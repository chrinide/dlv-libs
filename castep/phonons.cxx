
#include <cmath>
#include "../dlv/types.hxx"
#include "../dlv/constants.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/op_model.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_atoms.hxx"
#include "calcs.hxx"
#include "phonons.hxx"

DLV::operation *CASTEP::load_phonons::create(const char name[],
					     const char filename[],
					     const bool set_bonds,
					     char message[], const int_g mlen)
{
  message[0] = '\0';
  load_phonons *op = 0;
  DLV::string model_name = name_from_file(name, filename);
  if (DLV::check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (DLV::open_file_read(input, filename, message, mlen)) {
      int_g nfreq = 0;
      int_g nkvec = 0;
      int_g natoms = 0;
      real_g *mass;
      char units[64];
      DLV::model *structure = read_structure(model_name, input,
					     message, mlen, nfreq, nkvec,
					     natoms, mass, units);
      if (structure != 0) {
	op = new load_phonons(structure, filename);
	DLV::coord_type (*coords)[3] = new DLV::coord_type[natoms][3];
	structure->get_asym_atom_cart_coords(coords, natoms);
	DLV::data_object *data = 0;
	if (op->read_phonons(data, input, filename, nfreq, nkvec, natoms,
			     mass, coords, units, message, mlen)) {
	  attach_base(op);
	  op->attach_data(data);
	  if (set_bonds)
	    op->set_bond_all();
	} else {
	  delete op;
	  op = 0;
	}
	delete [] coords;
	delete [] mass;
      }
      input.close();
    }
  }
  return op;
}

DLV::string CASTEP::load_phonons::get_name() const
{
  return (get_model_name() + " - Load CASTEP phonon data");
}

DLV::model *CASTEP::load_phonons::read_structure(const DLV::string name,
						 std::ifstream &input,
						 char message[],
						 const int_g mlen,
						 int_g &nbranch,
						 int_g &nvectors,
						 int_g &natoms, real_g * &mass,
						 char units[])
{
  char line[256];
  // Get header block - todo loop until find it?
  input.getline(line, 256);
  char buff[128];
  sscanf(line, "%100s", buff);
  if (strcmp(buff, "BEGIN") != 0 && strcmp(buff, "begin") != 0) {
    strncpy(message, "Invalid phonon header", mlen);
    return 0;
  }
  // 'Number of ions'
  input >> line;
  input >> line;
  input >> line;
  input >> natoms;
  input.getline(line, 256);
  if (natoms < 1)
    strncpy(message, "No atoms found", mlen);
  else {
    // 'Number of branches'
    input >> line;
    input >> line;
    input >> line;
    input >> nbranch;
    input.getline(line, 256);
    if (nbranch < 1)
      strncpy(message, "No phonon frequencies found", mlen);
    else {
      // 'Number of wavevectors'
      input >> line;
      input >> line;
      input >> line;
      input >> nvectors;
      input.getline(line, 256);
      if (nvectors < 1)
	strncpy(message, "No k vectors found", mlen);
      else {
	// Read lattice
	units[0] = '\0';
	do {
	  input.getline(line, 256);
	  sscanf(line, "%s", buff);
	  if (strcmp(buff, "Frequencies") == 0) {
	    char tmp[128];
	    sscanf(line, "%s %s %s", buff, tmp, units);
	  }
	} while (strcmp(buff, "Unit") != 0);
	//input.getline(line, 256);
	real_l a[3];
	input.getline(line, 256);
	if (sscanf(line, "%lf %lf %lf", &a[0], &a[1], &a[2]) != 3)
	  strncpy(message, "Error reading a lattice vector", mlen - 1);
	else {
	  input.getline(line, 256);
	  real_l b[3];
	  if (sscanf(line, "%lf %lf %lf", &b[0], &b[1], &b[2]) != 3)
	    strncpy(message, "Error reading b lattice vector", mlen - 1);
	  else {
	    input.getline(line, 256);
	    real_l c[3];
	    if (sscanf(line, "%lf %lf %lf", &c[0], &c[1], &c[2]) != 3)
	      strncpy(message, "Error reading c lattice vector", mlen - 1);
	    else {
	      DLV::model *structure = DLV::model::create_atoms(name, 3);
	      if (structure == 0)
		strncpy(message, "Create model failed", mlen - 1);
	      else {
		DLVreturn_type ok = DLV_OK;
		if (!structure->set_primitive_lattice(a, b, c)) {
		  strncpy(message, "Error setting lattice vectors",
			  mlen - 1);
		  ok = DLV_ERROR;
		} else {
		  // Coordinates - 'Fractional coords...'
		  input.getline(line, 256);
		  // Read the atoms
		  DLV::coord_type coords[3];
		  char symbol[8];
		  mass = new real_g[natoms];
		  for (int_g i = 0; i < natoms; i++) {
		    input.getline(line, 256);
		    int_g j;
		    if (sscanf(line, "%d %lf %lf %lf %s %f", &j, &coords[0],
			       &coords[1], &coords[2], symbol,
			       &mass[i]) != 6) {
		      strncpy(message, "Error reading atoms", mlen - 1);
		      delete_local_array(mass);
		      ok = DLV_ERROR;
		      break;
		    } else {
		      DLV::atom my_atom =
			structure->add_fractional_atom(symbol, coords);
		      if (my_atom == 0) {
			strncpy(message, "Error adding atom to model", mlen);
			delete_local_array(mass);
			ok = DLV_ERROR;
			break;
		      }
		    }
		  }
		  // skip the 'end header block'
		  //if (ok == DLV_OK)
		  //  delete_local_array(mass);
		  input.getline(line, 256);
		}
		if (ok != DLV_OK) {
		  delete structure;
		  structure = 0;
		} else
		  structure->complete();
	      }
	      return structure;
	    }
	  }
	}
      }
    }
  }
  return 0;
}

bool CASTEP::load_phonons::read_phonons(DLV::data_object * &data,
					std::ifstream &input,
					const DLV::string id, const int_g nfreq,
					const int_g nkvectors,
					const int_g natoms, real_g mass[],
					DLV::coord_type c[][3], char units[],
					char message[], const int_g mlen)
{
  bool ok = true;
  // convert masses to the form we need
  for (int_g i = 0; i < natoms; i++) {
    //fprintf(stderr, "mass %f\n", mass[i]);
    mass[i] = real_g(1.0 / std::sqrt(mass[i]));
    //fprintf(stderr, "inv %f\n", mass[i]);
  }
  // Now read the eigenvectors.
  real_g (*vecs)[3] = new real_g[natoms][3];
  real_g (*phases)[3] = new real_g[natoms][3];
  real_g *freq = new real_g[nfreq];
  DLV::string *labels = new DLV::string[nfreq];
  real_g *intensities = new real_g[nfreq];
  real_g ka, kb, kc;
  real_g er, ei;
  const real_g tol = 0.001f;
  DLV::phonon_vectors *glyph = 0;
  char line[256];
  int_g nk = 0;
  do {
    input.getline(line, 256); // q-pt=
    if (input.eof()) {
      // Todo - if nk != nkvectors then reset in glyph
      break;
    }
    if (nk >= nkvectors) {
      strncpy(message, "Skipping unexpected phonon data", mlen);
      break;
    }
    //fprintf(stderr, "line is: %s\n", line);
    char buff[64];
    int_g tmp;
    if (sscanf(line, "%s %d %f %f %f", buff, &tmp, &ka, &kb, &kc) != 5) {
      strncpy(message, "Error reading k point", mlen);
      ok = false;
      break;
    }
    bool use_labels = true;
    bool has_intensities = false;
    for (int_g nm = 0; nm < nfreq; nm++) {
      input.getline(line, 256);
      int_g j;
      char info[16];
      real_g strength;
      int_g k = sscanf(line, "%d %f %16s %f", &j, &freq[nm], info, &strength);
      if (k == 4) {
	intensities[nm] = strength;
	has_intensities = true;
      }
      if (k >= 3)
	labels[nm] = info;
      else
	use_labels = false;
    }
    // Skip 'Phonon Eigenvectors'
    input.getline(line, 256);
    // Read this as 'Mode Ion X Y Z' in case fortran wraps lines
    input >> line; // Mode
    input >> line; // Ion
    input >> line; // X
    input >> line; // Y
    input >> line; // Z
    // Todo - maybe shouldn't assume mode, ion are sequential?
    // Read eigenvectors
    for (int_g nm = 0; nm < nfreq; nm++) {
      bool is_real = true;
      for (int_g i = 0; i < natoms; i++) {
	int_g atn;
	input >> atn; // mode
	input >> atn; // Ion
	input >> er;
	input >> ei;
	vecs[i][0] = er;
	phases[i][0] = ei;
	is_real = is_real && (ei < tol);
	input >> er;
	input >> ei;
	vecs[i][1] = er;
	phases[i][1] = ei;
	is_real = is_real && (ei < tol);
	input >> er;
	input >> ei;
	vecs[i][2] = er;
	phases[i][2] = ei;
	is_real = is_real && (ei < tol);
      }
      if (!is_real) {
	for (int_g i = 0; i < natoms; i++) {
	  er = vecs[i][0];
	  ei = phases[i][0];
	  vecs[i][0] = sqrt(er * er + ei * ei);
	  phases[i][0] = get_angle(er, ei);
	  er = vecs[i][1];
	  ei = phases[i][1];
	  vecs[i][1] = sqrt(er * er + ei * ei);
	  phases[i][1] = get_angle(er, ei);
	  er = vecs[i][2];
	  ei = phases[i][2];
	  vecs[i][2] = sqrt(er * er + ei * ei);
	  phases[i][2] = get_angle(er, ei);
	}
      }
      // Calculate displacements
      for (int_g i = 0; i < natoms; i++) {
	vecs[i][0] *= mass[i];
	vecs[i][1] *= mass[i];
	vecs[i][2] *= mass[i];
      }
      if (glyph == 0) {
	// Todo - vectors and intensities? (to use intensities)
	glyph = new DLV::phonon_vectors("CASTEP", id, this, nfreq, nkvectors);
	real_g (*coords)[3] = new real_g[natoms][3];
	for (int_g ix = 0; ix < natoms; ix++) {
	  coords[ix][0] = (real_g)c[ix][0];
	  coords[ix][1] = (real_g)c[ix][1];
	  coords[ix][2] = (real_g)c[ix][2];
	}
	glyph->set_grid(coords, natoms, false);
	data = glyph;
      }
      // Todo - use units
      if (use_labels) {
	if (is_real)
	  ok = glyph->add_data(nk, nm, ka, kb, kc, freq[nm],
			       labels[nm], vecs, natoms);
	else
	  ok = glyph->add_data(nk, nm, ka, kb, kc, freq[nm],
			       labels[nm], vecs, phases, natoms);
      } else {
	if (is_real)
	  ok = glyph->add_data(nk, nm, ka, kb, kc, freq[nm], vecs, natoms);
	else
	  ok = glyph->add_data(nk, nm, ka, kb, kc, freq[nm],
			       vecs, phases, natoms);
      }
      if (!ok) {
	strncpy(message, "Problem loading phonon data", mlen);
	break;
      }
    }
    if (!ok)
      break;
    // Tidy up for next q-pt.
    input.getline(line, 256);
    nk++;
  } while (!input.eof());
  delete [] intensities;
  delete [] labels;
  delete [] freq;
  delete [] vecs;
  return ok;
}

CASTEP::real_g CASTEP::load_phonons::get_angle(const real_g r, const real_g i)
{
  const real_g tol = 0.0001f;
  real_g a = 0.0;
  if (std::abs(r) < tol)
    a = real_g(DLV::pi / 2.0);
  else
    a = std::atan(std::abs(i) / std::abs(r));
  if (i < 0.0) {
    if (r < 0.0)
      a += (real_g)DLV::pi;
    else
      a = real_g(2.0 * DLV::pi - a);
  } else {
    if (r < 0.0)
      a = real_g(DLV::pi - a);
  }
  return a;
}

bool CASTEP::load_phonons::reload_data(DLV::data_object *data,
				       char message[], const int_g mlen)
{
  return true;
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, CASTEP::load_phonons *t,
				    const unsigned int file_version)
    {
      ::new(t)CASTEP::load_phonons(0, "recover");
    }

  }
}

template <class Archive>
void CASTEP::load_phonons::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::load_atom_model_op>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(CASTEP::load_phonons)

DLV_SUPPRESS_TEMPLATES(DLV::load_atom_model_op)

DLV_NORMAL_EXPLICIT(CASTEP::load_phonons)

#endif // DLV_USES_SERIALIZE
