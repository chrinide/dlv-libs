
#ifndef CASTEP_LOAD_PHONONS
#define CASTEP_LOAD_PHONONS

namespace CASTEP {

  class load_phonons : public DLV::load_atom_model_op {
  public:
    static operation *create(const char name[], const char filename[],
			     const bool set_bonds, char message[],
			     const int_g mlen);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

    // for serialization
    load_phonons(DLV::model *m, const char file[]);

  protected:

    DLV::string get_name() const;

  private:
    static DLV::model *read_structure(const DLV::string name,
				      std::ifstream &input, char message[],
				      const int_g mlen, int_g &nbranch,
				      int_g &nvectors, int_g &natoms,
				      real_g * &mass, char units[]);
    bool read_phonons(DLV::data_object * &data, std::ifstream &input,
		      const DLV::string id, const int_g nfreq,
		      const int_g nkvectors, const int_g natoms, real_g mass[],
		      DLV::coord_type c[][3], char units[],
		      char message[], const int_g mlen);
    static real_g get_angle(const real_g r, const real_g i);

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(CASTEP::load_phonons)
#endif // DLV_USES_SERIALIZE

inline CASTEP::load_phonons::load_phonons(DLV::model *m, const char file[])
  : load_atom_model_op(m, file)
{
}

#endif // CASTEP_LOAD_PHONONS
