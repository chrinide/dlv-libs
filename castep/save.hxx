
#ifndef CASTEP_SAVE_STRUCTURE
#define CASTEP_SAVE_STRUCTURE

namespace CASTEP {

  class save_structure : public DLV::save_model_op, public cell_file {
  public:
    static operation *create(const char filename[], const bool fractional,
			     char message[], const int_g mlen);

    // for serialization
    save_structure(const char file[]);

  protected:

    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(CASTEP::save_structure)
#endif // DLV_USES_SERIALIZE

inline CASTEP::save_structure::save_structure(const char file[])
  : save_model_op(file)
{
}

#endif // CASTEP_SAVE_STRUCTURE
