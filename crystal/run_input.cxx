
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_model.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/file.hxx"
#include "../dlv/job_setup.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "calcs.hxx"
#include "run_input.hxx"

DLV::string CRYSTAL::run_calc::serial_binary = "crystal";
DLV::string CRYSTAL::run_calc::parallel_binary = "Pcrystal";
DLV::string CRYSTAL::run_calc::mpp_binary = "MPPcrystal";

DLV::operation *CRYSTAL::run_calc::create(const char file[], const char str[],
					  const bool use_str,
					  const DLV::job_setup_data &job,
					  const bool mpp,
					  char message[], const int_g mlen)
{
  run_calc *op = 0;
  message[0] = '\0';
  if (use_str)
    op = new run_str_calc(file, str);
  else
    op = new run_calc(file);
  if (op == 0)
    strncpy(message, "Failed to allocate CRYSTAL::run_input operation",
	    mlen);
  else {
    attach_base(op);
    if (op->create_files(job.is_parallel(), job.is_local(), message, mlen))
      if (op->make_directory(message, mlen))
	op->write(op->get_infile_name(0), message, mlen);
    if (strlen(message) > 0) {
      // Don't delete once attached
      //delete op;
      //op = 0;
    } else {
      // Todo - mpp issues?
      op->execute(op->get_path(), op->get_serial_executable(),
		  op->get_parallel_executable(mpp), "", job, true,
		  false, "", message, mlen);
    }
  }
  return op;
}

DLV::string CRYSTAL::run_calc::get_name() const
{
  return ("CRYSTAL Run input calculation");
}

bool CRYSTAL::run_calc::has_structure() const
{
  return false;
}

bool CRYSTAL::run_str_calc::has_structure() const
{
  return true;
}

DLV::string CRYSTAL::run_calc::get_structure_file() const
{
  return "";
}

DLV::string CRYSTAL::run_str_calc::get_structure_file() const
{
  return structure_file;
}

bool CRYSTAL::run_calc::create_files(const bool is_parallel,
				     const bool is_local, char message[],
				     const int_g mlen)
{
  static char tag[] = "run";
  add_calc_error_file(tag, is_parallel, is_local);
  if (is_parallel)
    add_input_file(0, tag, "inp", "INPUT", is_local, false);
  else
    add_command_file(0, tag, "inp", is_local, false);
  add_structure_file(tag, is_local);
  add_log_file(tag, "out", is_local, true);
  add_sys_error_file(tag, "job", is_local, true);
  add_output_file(0, tag, "scf", "fort.98", is_local, true);
  add_output_file(1, tag, "runstr", "fort.34", is_local, false);
  return true;
}

void CRYSTAL::run_calc::add_structure_file(const char tag[],
					   const bool is_local)
{
  // dummy
}

void CRYSTAL::run_str_calc::add_structure_file(const char tag[],
					       const bool is_local)
{
  add_data_file(1, structure_file, "fort.34", is_local, true);
}

void CRYSTAL::run_calc::write(const DLV::string outfile,
			      char message[], const int_g mlen)
{
  std::ofstream output;
  std::ifstream datafile;
  if (DLV::open_file_read(datafile, filename.c_str(), message, mlen)) {
    if (DLV::open_file_write(output, outfile.c_str(), message, mlen)) {
      DLV::model *m = 0;
      int dim = 0;
      bool ok = true;
      bool finished = false;
      bool extprt_done = false;
      char buff[256];
      do {
	datafile.getline(buff, 256);
	if (datafile.eof())
	  finished = true;
	else if (strcmp(buff, "DLVINPUT") == 0) {
	  // EXTPRT keyword needs to apply to DLVINPUT, since it could
	  // contain an ATOMDISP
	  if (!has_structure())
	    strncpy(message,
		    "No structure file specified for EXTERNAL input", mlen);
	  else
	    strncpy(message, "Structure recovery not supported"
		    "- change DLVINPUT to EXTERNAL", mlen);
	  ok = false;
	} else if (strcmp(buff, "EXTERNAL") == 0) {
	  if (!has_structure()) {
	    // Todo - be smarter than this using COORPRT?
	    strncpy(message, "No structure file specified for EXTERNAL input",
		    mlen);
	    ok = false;
	  } else
	    m = read("Run CRYSTAL", get_structure_file().c_str(), false,
		     message, mlen);
	} else if (strcmp(buff, "OPTIMIZE") == 0 or
		   strcmp(buff, "OPTGEOM") == 0 or
		   strcmp(buff, "FREQCALC") == 0 or
		   strcmp(buff, "CPHF") == 0 or
		   strcmp(buff, "TDDFT") == 0 or
		   strncmp(buff, "END", 3) == 0) {
	  if (!extprt_done) {
	    output << "EXTPRT\n";
	    extprt_done = true;
	  }
	  output << buff;
	  output << '\n';
	} else if (strcmp(buff, "GUESSP") == 0 or
		   strcmp(buff, "GUESSF") == 0) {
	  strncpy(message, "Restarts not currently supported", mlen);
	  ok = false;
	} else if (strcmp(buff, "EXTPRT") != 0) {
	  output << buff;
	  output << '\n';
	}
	if (strcmp(buff, "MOLECULE") == 0)
	  dim = 0;
	else if (strcmp(buff, "POLYMER") == 0)
	  dim = 1;
	else if (strcmp(buff, "SLAB") == 0)
	  dim = 2;
	else if (strcmp(buff, "CRYSTAL") == 0)
	  dim = 3;
      } while (ok and !finished);
      if (ok) {
	if (m == 0) {
	  m = DLV::model::create_atoms("Run CRYSTAL", dim);
	  if (dim > 0)
	    m->set_lattice_parameters(1.0, 1.0, 1.0, 90.0, 90.0, 90.0);
	  m->complete();
	}
	attach_model(m);
      } else if (m != 0)
	delete m;
      output.close();
    }
    datafile.close();
  }
}

void CRYSTAL::run_calc::add_standard_data_objects()
{
  DLV::data_object *data = new DLV::atom_and_bond_data();
  if (data != 0) {
    attach_data(data);
#ifdef ENABLE_DLV_GRAPHICS
    render_data(data);
#endif // ENABLE_DLV_GRAPHICS
  }
}

bool CRYSTAL::run_calc::recover(const bool no_err, const bool log_ok,
				char message[], const int_g len)
{
  DLV::string id = get_job_id();
  bool ok = true;
  if (no_err) {
    // structure
    DLV::string label = "CRYSTAL run_input";
    DLV::model *m = read(label, get_outfile_name(1).c_str(),
			 false, message, len);
    ok = (m != 0);
    if (ok) {
      replace_model(m);
      //set_current();
    }
  }
  DLV::data_object *data = 0;
  if (log_ok) {
    // logfile
    DLV::string name = get_log_filename();
    if (no_err)
      data = new DLV::text_file(name, program_name, id,
				"run input output file");
    else
      data = new DLV::text_file(name, program_name, id,
				"run input(failed) output file");
    attach_data(data);
  }
  return ok;
}

bool CRYSTAL::run_calc::reload_data(DLV::data_object *data,
				    char message[], const int_g mlen)
{
  strncpy(message, "Reload CRYSTAL run input not implemented", mlen);
  return false;
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::run_calc *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::run_calc("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::run_str_calc *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::run_str_calc("recover", "str");
    }

  }
}

template <class Archive>
void CRYSTAL::run_calc::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<CRYSTAL::calc_geom>(*this);
  ar & filename;
}

template <class Archive>
void CRYSTAL::run_calc::load(Archive &ar, const unsigned int version)
{
  if (version == 0)
    ar & boost::serialization::base_object<CRYSTAL::calculate>(*this);
  else
    ar & boost::serialization::base_object<CRYSTAL::calc_geom>(*this);
  ar & filename;
}

template <class Archive>
void CRYSTAL::run_str_calc::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::run_calc>(*this);
  ar & structure_file;
}

BOOST_CLASS_VERSION(CRYSTAL::run_calc, 1)

BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::run_calc)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::run_str_calc)

DLV_SUPPRESS_TEMPLATES(CRYSTAL::calc_geom)

DLV_SPLIT_EXPLICIT(CRYSTAL::run_calc)
DLV_NORMAL_EXPLICIT(CRYSTAL::run_str_calc)

#endif // DLV_USES_SERIALIZE
