
#ifndef CRYSTAL_DOS_AND_BANDS
#define CRYSTAL_DOS_AND_BANDS

namespace CRYSTAL {

  class band_and_dos : public property_calc {
  public:
    static DLV::operation *create(const int_g version,
				  char message[], const int_g mlen);
    static bool reset_projections(const int_g ptype, char message[],
				  const int_g mlen);
    static bool add_projection(int_g &nproj, DLV::string &pname,
			       const int_g orbitals[], const int_g norbitals,
			       const bool all_atoms, const bool orb_atoms,
			       const bool orb_states, char message[],
			       const int_g mlen);
    static bool calculate(const int_g ndp, const int_g npoly, const int_g bmin,
			  const int_g bmax, const int_g nbp, const char path[],
			  const Density_Matrix &dm, const NewK &nk,
			  const int_g version, const DLV::job_setup_data &job,
			  const bool extern_job, const char extern_dir[],
			  char message[], const int_g mlen);
    static bool list_orbitals(int_g &nproj, DLV::string * &pname,
			      const bool all_atoms, const bool orb_atoms,
			      const bool orb_states, char message[],
			      const int_g mlen);

  protected:
    DLV::string get_name() const;
    virtual void set_params(const int_g np, const int_g npoly, const int_g bmin,
			    const int_g bmax, const int_g nbp,
			    const Density_Matrix &dm, const NewK &nk) = 0;
    virtual void add_dos_file(const char tag[], const bool is_local) = 0;
    bool create_files(const bool is_parallel, const bool is_local,
		      char message[], const int_g mlen);
    virtual void write(const char path[], const DLV::operation *op,
		       const DLV::model *const structure,
		       char message[], const int_g mlen) = 0;
    virtual int_g get_proj_type() const = 0;
    virtual void reset_projections(const int_g ptype) = 0;
    virtual bool add_projection(class wavefn_calc *wavefn,
				const DLV::model *const m, int_g &nproj,
				DLV::string &pname, const int_g orbitals[],
				const int_g norbitals, const bool all_atoms,
				const bool orb_atoms, const bool orb_states,
				const DLV::atom_integers *data, int_g &ptype,
				char message[], const int_g mlen) = 0;
    bool list_orbital_data(int_g &nproj, DLV::string * &pname,
			   const bool all_atoms, const bool orb_atoms,
			   const bool orb_states,
			   char message[], const int_g mlen);
    void atom_selections_changed();
    bool is_binary() const;

  private:
    bool binary_wvfn;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class band_and_dos_v5 : public band_and_dos, public band_file_v5,
			  public dos_data_v5, public band_data_v4  {
  public:
    band_and_dos_v5(const Density_Matrix &dm, const NewK &nk);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    void set_params(const int_g np, const int_g npoly, const int_g bmin,
		    const int_g bmax, const int_g nbp, const Density_Matrix &dm,
		    const NewK &nk);
    void add_dos_file(const char tag[], const bool is_local);
    void write(const char path[], const DLV::operation *op,
	       const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);
    int_g get_proj_type() const;
    void reset_projections(const int_g ptype);
    bool add_projection(class wavefn_calc *wavefn, const DLV::model *const m,
			int_g &nproj, DLV::string &pname,
			const int_g orbitals[], const int_g norbitals,
			const bool all_atoms, const bool orb_atoms,
			const bool orb_states, const DLV::atom_integers *data,
			int_g &ptype, char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class band_and_dos_v6 : public band_and_dos, public band_file_v5,
			  public dos_data_v6, public band_data_v6 {
  public:
    band_and_dos_v6(const Density_Matrix &dm, const NewK &nk);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    void set_params(const int_g np, const int_g npoly, const int_g bmin,
		    const int_g bmax, const int_g nbp, const Density_Matrix &dm,
		    const NewK &nk);
    void add_dos_file(const char tag[], const bool is_local);
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void write(const char path[], const DLV::operation *op,
	       const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);
    int_g get_proj_type() const;
    void reset_projections(const int_g ptype);
    bool add_projection(class wavefn_calc *wavefn, const DLV::model *const m,
			int_g &nproj, DLV::string &pname,
			const int_g orbitals[], const int_g norbitals,
			const bool all_atoms, const bool orb_atoms,
			const bool orb_states, const DLV::atom_integers *data,
			int_g &ptype, char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class band_and_dos_v7 : public band_and_dos, public band_file_v5,
			  public dos_data_v6, public band_data_v6 {
  public:
    band_and_dos_v7(const Density_Matrix &dm, const NewK &nk);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    void set_params(const int_g np, const int_g npoly, const int_g bmin,
		    const int_g bmax, const int_g nbp, const Density_Matrix &dm,
		    const NewK &nk);
    void add_dos_file(const char tag[], const bool is_local);
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void write(const char path[], const DLV::operation *op,
	       const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);
    int_g get_proj_type() const;
    void reset_projections(const int_g ptype);
    bool add_projection(class wavefn_calc *wavefn, const DLV::model *const m,
			int_g &nproj, DLV::string &pname,
			const int_g orbitals[], const int_g norbitals,
			const bool all_atoms, const bool orb_atoms,
			const bool orb_states, const DLV::atom_integers *data,
			int_g &ptype, char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(CRYSTAL::band_and_dos_v5)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::band_and_dos_v6)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::band_and_dos_v7)
#endif // DLV_USES_SERIALIZE

inline bool CRYSTAL::band_and_dos::is_binary() const
{
  return binary_wvfn;
}

inline CRYSTAL::band_and_dos_v5::band_and_dos_v5(const Density_Matrix &dm,
						 const NewK &nk)
  : property_data(dm, nk), dos_data_v5(dm, nk), band_data_v4(0, 0, 0, dm, nk)
{
}

inline CRYSTAL::band_and_dos_v6::band_and_dos_v6(const Density_Matrix &dm,
						 const NewK &nk)
  : property_data_v6(dm, nk), dos_data_v6(dm, nk),
    band_data_v6(0, 0, 0, dm, nk)
{
}

inline CRYSTAL::band_and_dos_v7::band_and_dos_v7(const Density_Matrix &dm,
						 const NewK &nk)
  : property_data_v6(dm, nk), dos_data_v6(dm, nk),
    band_data_v6(0, 0, 0, dm, nk)
{
}

#endif // CRYSTAL_DOS_AND_BANDS
