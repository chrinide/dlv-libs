
#ifndef CRYSTAL_WANNIER_FUNCTIONS
#define CRYSTAL_WANNIER_FUNCTIONS

namespace CRYSTAL {

  // Only v6 supports this?
  class wannier3D_file : public grid3D_v6 {
  protected:
    DLV::volume_data *create_volume(const char code[], const DLV::string id,
				    DLV::operation *op, const int_g nx,
				    const int_g ny, const int_g nz,
				    const real_g origin[3],
				    const real_g astep[3],
				    const real_g bstep[3],
				    const real_g cstep[3]) const;
    bool get_data(std::ifstream &input, DLV::volume_data *data,
		  DLV::operation *op, const int_g nx, const int_g ny,
		  const int_g nz, const char label[], char message[],
		  const int_g mlen);
  };

  class wannier3D_data : public property_data_v6 {
  protected:
    wannier3D_data(const int_g np, DLV::box *v, const int_g minb,
		   const int_g maxb, const int_g lvec, const bool sym,
		   const Density_Matrix &dm, const NewK &nk);

  protected:
    virtual void write_input(const DLV::string filename,
			     const DLV::operation *op,
			     const DLV::model *const structure, const bool spin,
			     const bool is_bin, char message[],
			     const int_g mlen);
    void write_input(const DLV::string filename, const DLV::operation *op,
		     const DLV::model *const structure,
		     const real_g grid_spacing, const bool set_points,
		     const real_g radius, const bool generate_grid,
		     const bool spin, const bool is_bin,
		     char message[], const int_g mlen);
    void write_grid(std::ofstream &output) const;
    void write_cmd_data(std::ofstream &output) const;
    virtual void write_printplo(std::ofstream &output, const int_g g) const;
    void write_data_sets(std::ofstream &output) const;

  private:
    int_g npoints;
    DLV::box *grid;
    int_g min_band;
    int_g max_band;
    int_g gstars;
    bool symmetrize;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class wannier3D_data_v6 : public wannier3D_data, public wannier3D_file {
  protected:
    wannier3D_data_v6(const int_g np, DLV::box *v, const int_g minb,
		      const int_g maxb, const int_g lvec, const bool sym,
		      const Density_Matrix &dm, const NewK &nk);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class wannier3D_data_v8 : public wannier3D_data, public wannier3D_file {
  protected:
    wannier3D_data_v8(const int_g np, DLV::box *v, const int_g minb,
		      const int_g maxb, const int_g lvec, const bool sym,
		      const real_g sp, const bool use_np, const real_g r,
		      const bool auto_grid,
		      const Density_Matrix &dm, const NewK &nk);

    void write_input(const DLV::string filename, const DLV::operation *op,
		     const DLV::model *const structure, const bool spin,
		     const bool is_bin, char message[], const int_g mlen);
    void write_printplo(std::ofstream &output, const int_g g) const;

  private:
    real_g grid_spacing;
    real_g radius;
    bool set_points;
    bool automatic_grid;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_wannier3D_data : public DLV::load_data_op {
  public:
    static operation *create(const char filename[], const int_g version,
			     char message[], const int_g mlen);

  protected:
    load_wannier3D_data(const char file[]);

    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_wannier3D_data_v6 : public load_wannier3D_data,
				 public wannier3D_file {
  public:
    static operation *create(const char filename[],
                             char message[], const int_g mlen);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

    // public for serialization
    load_wannier3D_data_v6(const char file[]);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class wannier3D_calc : public property_calc {
  public:
    static operation *create(const int_g np, const int_g grid,
			     const int_g minb, const int_g maxb, const bool sym,
			     const int_g lvec, const real_g spacing,
			     const bool use_np, const real_g r,
			     const bool auto_grid,
			     const Density_Matrix &dm, const NewK &nk,
			     const int_g version,
			     const DLV::job_setup_data &job,
			     const bool extern_job, const char extern_dir[],
			     char message[], const int_g mlen);

  protected:
    DLV::string get_name() const;

    bool create_files(const bool is_parallel, const bool is_local,
		      char message[], const int_g mlen);
    virtual void write(const DLV::string filename, const DLV::operation *op,
		       const DLV::model *const structure,
		       char message[], const int_g mlen) = 0;
    bool is_binary() const;

  private:
    bool binary_wvfn;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class wannier3D_calc_v6 : public wannier3D_calc, public wannier3D_data_v6 {
  public:
    wannier3D_calc_v6(const int_g np, DLV::box *v, const int_g minb,
		      const int_g maxb, const int_g lvec, const bool sym,
		      const Density_Matrix &dm, const NewK &nk);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void write(const DLV::string filename, const DLV::operation *op,
	       const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class wannier3D_calc_v8 : public wannier3D_calc, public wannier3D_data_v8 {
  public:
    wannier3D_calc_v8(const int_g np, DLV::box *v, const int_g minb,
		      const int_g maxb, const int_g lvec, const bool sym,
		      const real_g sp, const bool use_np, const real_g r,
		      const bool auto_grid, const Density_Matrix &dm,
		      const NewK &nk);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void write(const DLV::string filename, const DLV::operation *op,
	       const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  // v5+ supports this - but only implemented for v6+
  class wannier2D_file : public grid2D_v5 {
  protected:
    bool get_data(std::ifstream &input, DLV::surface_data *data,
		  const int_g nx, const int_g ny, const bool reverse,
		  const char label[], const int_g max_datasets,
		  char message[], const int_g mlen);
  };

  class wannier2D_data : public property_data_v6, public wannier2D_file {
  protected:
    wannier2D_data(const int_g np, DLV::plane *v, const int_g minb,
		   const int_g maxb, const int_g lvec, const bool sym,
		   const Density_Matrix &dm, const NewK &nk);

  protected:
    void write_input(const DLV::string filename, const DLV::operation *op,
		     const DLV::model *const structure, const bool spin,
		     const bool is_bin, char message[], const int_g mlen);
    void write_cmd_data(std::ofstream &output) const;

    int_g get_nbands() const;

  private:
    int_g npoints;
    DLV::plane *grid;
    int_g min_band;
    int_g max_band;
    int_g gstars;
    bool symmetrize;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_wannier2D_data : public DLV::load_data_op, public wannier2D_file {
  public:
    static operation *create(const char filename[], const int_g version,
			     char message[], const int_g mlen);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    load_wannier2D_data(const char file[]);

    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_wannier2D_data_v6 : public load_wannier2D_data {
  public:
    static operation *create(const char filename[],
                             char message[], const int_g mlen);

    //public for serialization
    load_wannier2D_data_v6(const char file[]);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class wannier2D_calc : public property_calc {
  public:
    static operation *create(const int_g np, const int_g grid,
			     const int_g minb, const int_g maxb, const bool sym,
			     const int_g lvec, const Density_Matrix &dm,
			     const NewK &nk, const int_g version,
			     const DLV::job_setup_data &job,
			     const bool extern_job, const char extern_dir[],
			     char message[], const int_g mlen);

  protected:
    DLV::string get_name() const;

    bool create_files(const bool is_parallel, const bool is_local,
		      char message[], const int_g mlen);
    virtual void write(const DLV::string filename, const DLV::operation *op,
		       const DLV::model *const structure,
		       char message[], const int_g mlen) = 0;
    bool is_binary() const;

  private:
    bool binary_wvfn;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class wannier2D_calc_v6 : public wannier2D_calc, public wannier2D_data {
  public:
    wannier2D_calc_v6(const int_g np, DLV::plane *v, const int_g minb,
		      const int_g maxb, const int_g lvec, const bool sym,
		      const Density_Matrix &dm, const NewK &nk);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void write(const DLV::string filename, const DLV::operation *op,
	       const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(CRYSTAL::load_wannier3D_data_v6)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::wannier3D_calc_v6)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::wannier3D_calc_v8)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::load_wannier2D_data_v6)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::wannier2D_calc_v6)
#endif // DLV_USES_SERIALIZE

inline CRYSTAL::wannier3D_data::wannier3D_data(const int_g np, DLV::box *v,
					       const int_g minb,
					       const int_g maxb,
					       const int_g lvec, const bool sym,
					       const Density_Matrix &dm,
					       const NewK &nk)
  : property_data_v6(dm, nk), npoints(np), grid(v), min_band(minb),
    max_band(maxb), gstars(lvec), symmetrize(sym)
{
}

inline CRYSTAL::wannier3D_data_v6::wannier3D_data_v6(const int_g np,
						     DLV::box *v,
						     const int_g minb,
						     const int_g maxb,
						     const int_g lvec,
						     const bool sym,
						     const Density_Matrix &dm,
						     const NewK &nk)
  : wannier3D_data(np, v, minb, maxb, lvec, sym, dm, nk)
{
}

inline CRYSTAL::wannier3D_data_v8::wannier3D_data_v8(const int_g np,
						     DLV::box *v,
						     const int_g minb,
						     const int_g maxb,
						     const int_g lvec,
						     const bool sym,
						     const real_g sp,
						     const bool use_np,
						     const real_g r,
						     const bool auto_grid,
						     const Density_Matrix &dm,
						     const NewK &nk)
  : wannier3D_data(np, v, minb, maxb, lvec, sym, dm, nk),
    grid_spacing(sp), radius(r), set_points(use_np), automatic_grid(auto_grid)
{
}

inline CRYSTAL::load_wannier3D_data::load_wannier3D_data(const char file[])
  : load_data_op(file)
{
}

inline
CRYSTAL::load_wannier3D_data_v6::load_wannier3D_data_v6(const char file[])
  : load_wannier3D_data(file)
{
}

inline bool CRYSTAL::wannier3D_calc::is_binary() const
{
  return binary_wvfn;
}

inline CRYSTAL::wannier3D_calc_v6::wannier3D_calc_v6(const int_g np,
						     DLV::box *v,
						     const int_g minb,
						     const int_g maxb,
						     const int_g lvec,
						     const bool sym,
						     const Density_Matrix &dm,
						     const NewK &nk)
  : wannier3D_data_v6(np, v, minb, maxb, lvec, sym, dm, nk)
{
}

inline CRYSTAL::wannier3D_calc_v8::wannier3D_calc_v8(const int_g np,
						     DLV::box *v,
						     const int_g minb,
						     const int_g maxb,
						     const int_g lvec,
						     const bool sym,
						     const real_g sp,
						     const bool use_np,
						     const real_g r,
						     const bool auto_grid,
						     const Density_Matrix &dm,
						     const NewK &nk)
  : wannier3D_data_v8(np, v, minb, maxb, lvec, sym,
		      sp, use_np, r, auto_grid, dm, nk)
{
}

inline CRYSTAL::wannier2D_data::wannier2D_data(const int_g np,
					       DLV::plane *v, const int_g minb,
					       const int_g maxb,
					       const int_g lvec, const bool sym,
					       const Density_Matrix &dm,
					       const NewK &nk)
  : property_data_v6(dm, nk), npoints(np), grid(v), min_band(minb),
    max_band(maxb), gstars(lvec), symmetrize(sym)
{
}

inline CRYSTAL::load_wannier2D_data::load_wannier2D_data(const char file[])
  : load_data_op(file)
{
}

inline
CRYSTAL::load_wannier2D_data_v6::load_wannier2D_data_v6(const char file[])
  : load_wannier2D_data(file)
{
}

inline bool CRYSTAL::wannier2D_calc::is_binary() const
{
  return binary_wvfn;
}

inline CRYSTAL::wannier2D_calc_v6::wannier2D_calc_v6(const int_g np,
						     DLV::plane *v,
						     const int_g minb,
						     const int_g maxb,
						     const int_g lvec,
						     const bool sym,
						     const Density_Matrix &dm,
						     const NewK &nk)
  : wannier2D_data(np, v, minb, maxb, lvec, sym, dm, nk)
{
}

inline CRYSTAL::int_g CRYSTAL::wannier2D_data::get_nbands() const
{
  return (max_band - min_band + 1);
}

#endif // CRYSTAL_WANNIER_FUNCTIONS
