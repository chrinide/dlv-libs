
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/constants.hxx"
#include "../dlv/math_fns.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_model.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/file.hxx"
#include "../dlv/job_setup.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_atoms.hxx"
#include "calcs.hxx"
#include "wavefn.hxx"

DLV::operation *CRYSTAL::wavefn_calc::create(const char file[],
					     const bool binary,
					     const bool new_view,
					     const bool new_str,
					     const bool set_bonds,
					     const bool do_tddft,
					     const char tddftfile[],
					     const DLV::job_setup_data &job,
					     const int_g version,
					     char message[], const int_g mlen)
{
  wavefn_calc *op = 0;
  message[0] = '\0';
  if (new_str) {
    if (file == 0 or strlen(file) == 0) {
      strncpy(message, "Empty filename", mlen);
      return 0;
    }
    if (version < CRYSTAL06)
      op = new wavefn_analyse(file, new_view, set_bonds, binary);
    else {
      if (do_tddft and version > CRYSTAL09)
	op = new wavefn_analyse_v8(file, tddftfile, new_view,
				   set_bonds, binary);
      else
	op = new wavefn_analyse_v6(file, new_view, set_bonds, binary);
    }
  } else {
    if (version < CRYSTAL06)
      op = new wavefn_calc(file);
    else
      op = new wavefn_calc_v6(file);
  }
  if (op == 0)
    strncpy(message, "Failed to allocate CRYSTAL::wavefn_calc operation",
	    mlen);
  else {
    if (op->create_files(false, job.is_local(), message, mlen))
      if (op->make_directory(message, mlen))
	op->write(op->get_infile_name(0), message, mlen);
    if (strlen(message) > 0) {
      delete op;
      op = 0;
    } else {
      if (new_str)
	op->attach_base_no_current();
      else
	op->attach();
      op->execute(op->get_path(), "properties", "", "", job, false,
		  false, "", message, mlen);
    }
  }
  return op;
}

bool CRYSTAL::wavefn_analyse::open_new_view() const
{
  return use_new_view;
}

DLV::string CRYSTAL::wavefn_calc::get_name() const
{
  DLV::string name = "CRYSTAL Wavefunction analysis - ";
  name += DLV::get_file_name(filename);
  return name;
}

void CRYSTAL::wavefn_calc_v6::add_calc_error_file(const DLV::string tag,
						  const bool is_parallel,
						  const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

void CRYSTAL::wavefn_analyse_v6::add_calc_error_file(const DLV::string tag,
						     const bool is_parallel,
						     const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

bool CRYSTAL::wavefn_calc::create_files(const bool is_parallel,
					const bool is_local,
					char message[], const int_g mlen)
{
  bool is_binary = false;
  if (find_current_wavefn(filename, is_binary, message, mlen)) {
    static char tag[] = "wavefn";
    static char input[] = "inp";
    add_calc_error_file(tag, is_parallel, is_local);
    if (is_parallel)
      add_input_file(0, tag, input, "INPUT", is_local, false);
    else
      add_command_file(0, tag, input, is_local, false);
    add_data_file(1, filename, "fort.98", is_local, true);
    add_log_file(tag, "out", is_local, true);
    add_sys_error_file(tag, "job", is_local, true);
    add_output_file(0, tag, "32", "fort.32", is_local, true);
    return true;
  } else
    return false;
}

bool CRYSTAL::wavefn_analyse::create_files(const bool is_parallel,
					   const bool is_local,
					   char message[], const int_g mlen)
{
  static char tag[] = "wavefn";
  static char input[] = "inp";
  add_calc_error_file(tag, is_parallel, is_local);
  if (is_parallel)
    add_input_file(0, tag, input, "INPUT", is_local, false);
  else
    add_command_file(0, tag, input, is_local, false);
  if (is_binary())
    add_data_file(1, get_filename(), "fort.9", is_local, true);
  else
    add_data_file(1, get_filename(), "fort.98", is_local, true);
  add_log_file(tag, "out", is_local, true);
  add_sys_error_file(tag, "job", is_local, true);
  add_output_file(0, tag, "32", "fort.32", is_local, true);
  add_output_file(1, tag, "str", "fort.34", is_local, true);
  return true;
}

void CRYSTAL::wavefn_calc::write(const DLV::string outfile,
				 char message[], const int_g mlen)
{
  std::ofstream output;
  if (DLV::open_file_write(output, outfile.c_str(), message, mlen)) {
    if (!binary)
      output << "RDFMWF\n";
    output << "INFO\n";
    // Although we don't need this when analysing, it simplifies the recovery.
    output << "EXTPRT\n";
    output << "END\n";
    output.close();
  }
}

void CRYSTAL::wavefn_analyse::add_standard_data_objects()
{
  DLV::data_object *data = new DLV::atom_and_bond_data();
  if (data != 0) {
    attach_data(data);
#ifdef ENABLE_DLV_GRAPHICS
    render_data(data);
#endif // ENABLE_DLV_GRAPHICS
  }
}

void CRYSTAL::wavefn_calc::attach_wavefn()
{
  DLV::data_object *data = new DLV::file_data(filename, program_name,
					      wavefn_label, binary);
  attach_data(data);
}

bool CRYSTAL::wavefn_calc::recover(const bool no_err, const bool log_ok,
				   char message[], const int_g len)
{
  DLV::string id = get_job_id();
  if (no_err) {
    //attach_wavefn(); - the scf already had it.
    return read_info(get_outfile_name(0), id, message, len);
  }
  return true;
}

bool CRYSTAL::wavefn_analyse::recover(const bool no_err, const bool log_ok,
				      char message[], const int_g len)
{
  DLV::string id = get_job_id();
  if (no_err) {
    DLV::string label = "CRYSTAL wavefn (";
    label += DLV::get_file_name(get_filename());
    label += ")";
    DLV::model *m = read(label, get_outfile_name(1).c_str(),
			 false, message, len);
    bool ok = (m != 0);
    if (ok) {
      attach_model(m);
      set_current();
      if (set_bonds)
	set_bond_all();
      //done by attach_base add_standard_data_objects();
      attach_wavefn();
      ok = read_info(get_outfile_name(0), id, message, len);
    }
    return ok;
  }
  return true;
}

bool CRYSTAL::wavefn_analyse_v8::recover(const bool no_err, const bool log_ok,
					 char message[], const int_g len)
{
  if (wavefn_analyse::recover(no_err, log_ok, message, len)) {
    if (tddft_file.length() > 0) {
      DLV::data_object *data = new DLV::file_data(tddft_file, program_name,
						  tddft_label);
      attach_data(data);
#ifdef ENABLE_DLV_GRAPHICS
      get_display_obj()->set_CRYSTAL_tddft();
#endif // ENABLE_DLV_GRAPHICS
    }
    return true;
  } else
    return false;
}

bool CRYSTAL::wavefn_calc::read_info(const DLV::string filename,
				     const DLV::string id, char message[],
				     const int_g mlen)
{
  std::ifstream info_file;
  bool ok = DLV::open_file_read(info_file, filename.c_str(), message, mlen);
  if (ok) {
    char buff[128];
    DLV::string text;
    for (int_g i = 0; i < 5; i++)
      info_file.getline(buff, 128);
    // level shifter
    info_file.getline(buff, 128);
    char label1[64];
    std::sscanf(buff, "%s", label1);
    bool use_fermi = true;
    if (strncmp(label1, "No", 2) != 0)
      use_fermi = false;
    // 2 spin lock lines
    info_file.getline(buff, 128);
    info_file.getline(buff, 128);
    // calculation type
    info_file.getline(buff, 128);
    char label2[64];
    std::sscanf(buff, "%*s %*s %*s %s %s", label1, label2);
    if (strncmp(label1, "UNRESTRICTED", 12) == 0 or
	strncmp(label2, "OPEN", 4) == 0)
      spin = true;
    // I should have been smarter in outputing the total energy
    info_file.getline(buff, 128);
    real_l total_e;
    if (std::sscanf(buff, "%*s %*s %*s %lf", &total_e) == 1) {
      DLV::real_data *obj = new DLV::real_data(program_name, id,
					       "Total Energy (H)", total_e);
      attach_data(obj);
    }
    info_file.getline(buff, 128);
    // Fermi energy data.
    int_g dummy;
    info_file >> dummy;
    real_l temp;
    info_file >> temp;
    if (dummy and use_fermi) {
      DLV::real_data *obj = new DLV::real_data(program_name, id,
					       fermi_label.c_str(),
					       temp * DLV::Hartree_to_eV);
      attach_data(obj);
      has_fermi = true;
    } else {
      DLV::real_data *obj = new DLV::real_data(program_name, id,
					       invalid_fermi_label.c_str(),
					       temp * DLV::Hartree_to_eV,
					       false);
      attach_data(obj);
    }
    info_file >> nshells;
    info_file >> norbitals;
    info_file >> electrons_per_cell;
    info_file >> core_electrons;
    info_file >> natoms;
    int_g k = 0;
    prim_atoms = new atom_data[natoms];
    shells = new shell_data[nshells];
    for (int_g i = 0; i < natoms; i++) {
      info_file >> dummy;
      info_file >> prim_atoms[i].atomic_number;
      info_file >> prim_atoms[i].basis_index;
      info_file >> temp;
      prim_atoms[i].coords[0] = temp * C09_bohr_to_angstrom;
      info_file >> temp;
      prim_atoms[i].coords[1] = temp * C09_bohr_to_angstrom;
      info_file >> temp;
      prim_atoms[i].coords[2] = temp * C09_bohr_to_angstrom;
      // Todo - find asymmetric atom index.
      info_file >> prim_atoms[i].nshells;
      prim_atoms[i].shell_index = k;
      for (int_g j = 0; j < prim_atoms[i].nshells; j++) {
        info_file >> shells[k].shell_type;
        k++;
      }
    }
    int_g spin_bands = 0;
    shrinka = 0;
    shrinkb = 0;
    shrinkc = 0;
    nkpoints = 0;
    kpoints = 0;
    info_file >> shrinka;
    if (!info_file.eof()) {
      info_file >> shrinkb;
      info_file >> shrinkc;
      info_file >> nkpoints;
      kpoints = new int_g [nkpoints][3]; 
      for (int_g i = 0; i < nkpoints; i++) {
	info_file >> kpoints[i][0];
	info_file >> kpoints[i][1];
	info_file >> kpoints[i][2];
      }
      if (spin) {
	info_file.getline(buff, 128);
	// spin
	info_file.getline(buff, 128);
	if (strncmp(buff, "Spin", 4) == 0) {
	  real_g tot_spin = 0.0;
	  std::sscanf(buff, "%*s %f", &tot_spin);
	  tot_spin = real_g(std::abs(tot_spin) / 2.0);
	  const real_g tol = 0.001f;
	  spin_bands = (int) DLV::truncate(tot_spin);
	  if (tot_spin > DLV::truncate(tot_spin) + tol)
	    spin_bands++;
	}
      }
    }
    info_file.close();
    // create property labels - check against DLV prim cell?
    DLV::atom_integers *labels = new DLV::atom_integers(program_name, id, this,
							prim_atom_label);
    real_g (*coords)[3] = new real_g[natoms][3];
    for (int_g i = 0; i < natoms; i++) {
      coords[i][0] = (real_g)prim_atoms[i].coords[0];
      coords[i][1] = (real_g)prim_atoms[i].coords[1];
      coords[i][2] = (real_g)prim_atoms[i].coords[2];
    }
    labels->set_grid(coords, natoms, false);
    int_g *data = new int_g[natoms];
    for (int_g i = 0; i < natoms; i++)
      data[i] = (i + 1);
    labels->add_data(data, "labels", false);
    // attach after data otherwise isn't listed correctly in UI
    attach_data(labels);
    spin_electrons = electrons_per_cell / 2 + spin_bands;
#ifdef ENABLE_DLV_GRAPHICS
    if (ok) {
      const DLV::model *const m = get_model();
      get_display_obj()->set_CRYSTAL_wavefn((core_electrons / 2) + 1,
					    spin_electrons,
					    norbitals, m->get_lattice_type(),
					    m->get_lattice_centring(),
					    spin, kpoints, nkpoints,
					    shrinka, shrinkb, shrinkc);
    }
#endif // ENABLE_DLV_GRAPHICS
  }
  return ok;
}

bool CRYSTAL::wavefn_calc::reload_data(DLV::data_object *data,
				       char message[], const int_g mlen)
{
  return true;
}

void CRYSTAL::wavefn_calc::count_atom_shell_type(const int_g atom, int_g &s,
						 int_g &p, int_g &sp,
						 int_g &d, int_g &f,
						 int_g &g) const
{
  int_g k = prim_atoms[atom].shell_index;
  for (int_g i = 0; i < prim_atoms[atom].nshells; i++) {
    switch (shells[k].shell_type) {
    case 0:
      s++;
      break;
    case 1:
      sp++;
      break;
    case 2:
      p++;
      break;
    case 3:
      d++;
      break;
    case 4:
      f++;
      break;
    case 5:
      g++;
      break;
    default:
      break;
    }
    k++;
  }
}

CRYSTAL::int_g CRYSTAL::wavefn_calc::find_s_shell(const int_g atom,
						  const int_g n) const
{
  int_g k = prim_atoms[atom].shell_index;
  int_g count = 0;
  for (int_g i = 0; i < prim_atoms[atom].nshells; i++) {
    if (shells[k].shell_type == 0) {
      if (count == n)
	return i;
      count++;
    }
    k++;
  }
  return -1;
}

CRYSTAL::int_g CRYSTAL::wavefn_calc::find_p_shell(const int_g atom,
						  const int_g n) const
{
  int_g k = prim_atoms[atom].shell_index;
  int_g count = 0;
  for (int_g i = 0; i < prim_atoms[atom].nshells; i++) {
    if (shells[k].shell_type == 2) {
      if (count == n)
	return i;
      count++;
    }
    k++;
  }
  return -1;
}

CRYSTAL::int_g CRYSTAL::wavefn_calc::find_sp_shell(const int_g atom,
						   const int_g n) const
{
  int_g k = prim_atoms[atom].shell_index;
  int_g count = 0;
  for (int_g i = 0; i < prim_atoms[atom].nshells; i++) {
    if (shells[k].shell_type == 1) {
      if (count == n)
	return i;
      count++;
    }
    k++;
  }
  return -1;
}

CRYSTAL::int_g CRYSTAL::wavefn_calc::find_d_shell(const int_g atom,
						  const int_g n) const
{
  int_g k = prim_atoms[atom].shell_index;
  int_g count = 0;
  for (int_g i = 0; i < prim_atoms[atom].nshells; i++) {
    if (shells[k].shell_type == 3) {
      if (count == n)
	return i;
      count++;
    }
    k++;
  }
  return -1;
}

CRYSTAL::int_g CRYSTAL::wavefn_calc::find_f_shell(const int_g atom,
						  const int_g n) const
{
  int_g k = prim_atoms[atom].shell_index;
  int_g count = 0;
  for (int_g i = 0; i < prim_atoms[atom].nshells; i++) {
    if (shells[k].shell_type == 4) {
      if (count == n)
	return i;
      count++;
    }
    k++;
  }
  return -1;
}

CRYSTAL::int_g CRYSTAL::wavefn_calc::find_g_shell(const int_g atom,
						  const int_g n) const
{
  int_g k = prim_atoms[atom].shell_index;
  int_g count = 0;
  for (int_g i = 0; i < prim_atoms[atom].nshells; i++) {
    if (shells[k].shell_type == 5) {
      if (count == n)
	return i;
      count++;
    }
    k++;
  }
  return -1;
}

CRYSTAL::int_g CRYSTAL::wavefn_calc::find_s_orbital(const int_g atom,
						    const int_g n) const
{
  int_g k = prim_atoms[atom].shell_index;
  int_g count = 0;
  int_g index = 0;
  for (int_g i = 0; i < prim_atoms[atom].nshells; i++) {
    switch (shells[k].shell_type) {
    case 0:
      if (count == n)
	return index;
      count++;
      index++;
      break;
    case 1:
      index += 4;
      break;
    case 2:
      index += 3;
      break;
    case 3:
      index += 5;
      break;
    case 4:
      index += 7;
      break;
    case 5:
      index += 9;
      break;
    default:
      index++;
      break;
    }
    k++;
  }
  return -1;
}

CRYSTAL::int_g CRYSTAL::wavefn_calc::find_p_orbital(const int_g atom,
						    const int_g n) const
{
  int_g k = prim_atoms[atom].shell_index;
  int_g count = 0;
  int_g index = 0;
  for (int_g i = 0; i < prim_atoms[atom].nshells; i++) {
    switch (shells[k].shell_type) {
    case 0:
      index++;
      break;
    case 1:
      index += 4;
      break;
    case 2:
      if (count == n)
	return index;
      count++;
      index += 3;
      break;
    case 3:
      index += 5;
      break;
    case 4:
      index += 7;
      break;
    case 5:
      index += 9;
      break;
    default:
      index++;
      break;
    }
    k++;
  }
  return -1;
}

CRYSTAL::int_g CRYSTAL::wavefn_calc::find_sp_orbital(const int_g atom,
						     const int_g n) const
{
  int_g k = prim_atoms[atom].shell_index;
  int_g count = 0;
  int_g index = 0;
  for (int_g i = 0; i < prim_atoms[atom].nshells; i++) {
    switch (shells[k].shell_type) {
    case 0:
      index++;
      break;
    case 1:
      if (count == n)
	return index;
      count++;
      index += 4;
      break;
    case 2:
      index += 3;
      break;
    case 3:
      index += 5;
      break;
    case 4:
      index += 7;
      break;
    case 5:
      index += 9;
      break;
    default:
      index++;
      break;
    }
    k++;
  }
  return -1;
}

CRYSTAL::int_g CRYSTAL::wavefn_calc::find_d_orbital(const int_g atom,
						    const int_g n) const
{
  int_g k = prim_atoms[atom].shell_index;
  int_g count = 0;
  int_g index = 0;
  for (int_g i = 0; i < prim_atoms[atom].nshells; i++) {
    switch (shells[k].shell_type) {
    case 0:
      index++;
      break;
    case 1:
      index += 4;
      break;
    case 2:
      index += 3;
      break;
    case 3:
      if (count == n)
	return index;
      count++;
      index += 5;
      break;
    case 4:
      index += 7;
      break;
    case 5:
      index += 9;
      break;
    default:
      index++;
      break;
    }
    k++;
  }
  return -1;
}

CRYSTAL::int_g CRYSTAL::wavefn_calc::find_f_orbital(const int_g atom,
						    const int_g n) const
{
  int_g k = prim_atoms[atom].shell_index;
  int_g count = 0;
  int_g index = 0;
  for (int_g i = 0; i < prim_atoms[atom].nshells; i++) {
    switch (shells[k].shell_type) {
    case 0:
      index++;
      break;
    case 1:
      index += 4;
      break;
    case 2:
      index += 3;
      break;
    case 3:
      index += 5;
      break;
    case 4:
      if (count == n)
	return index;
      count++;
      index += 7;
      break;
    case 5:
      index += 9;
      break;
    default:
      index++;
      break;
    }
    k++;
  }
  return -1;
}

CRYSTAL::int_g CRYSTAL::wavefn_calc::find_g_orbital(const int_g atom,
						    const int_g n) const
{
  int_g k = prim_atoms[atom].shell_index;
  int_g count = 0;
  int_g index = 0;
  for (int_g i = 0; i < prim_atoms[atom].nshells; i++) {
    switch (shells[k].shell_type) {
    case 0:
      index++;
      break;
    case 1:
      index += 4;
      break;
    case 2:
      index += 3;
      break;
    case 3:
      index += 5;
      break;
    case 4:
      index += 7;
      break;
    case 5:
      if (count == n)
	return index;
      count++;
      index += 9;
      break;
    default:
      index++;
      break;
    }
    k++;
  }
  return -1;
}

CRYSTAL::int_g
CRYSTAL::wavefn_calc::get_atom_basis_index(const int_g atom) const
{
  int_g index = 1;
  for (int_g i = 0; i < atom; i++) {
    int_g k = prim_atoms[i].shell_index;
    for (int_g j = 0; j < prim_atoms[i].nshells; j++) {
      switch (shells[k].shell_type) {
      case 0:
	index++;
	break;
      case 1:
	index += 4;
	break;
      case 2:
	index += 3;
	break;
      case 3:
	index += 5;
	break;
      case 4:
	index += 7;
	break;
      case 5:
	index += 9;
	break;
      default:
	index++;
	break;
      }
      k++;
    }
  }
  return index;
}

CRYSTAL::int_g CRYSTAL::wavefn_calc::get_s_or_sp_orbital(const int_g atom,
							 const int_g n) const
{
  int_g k = prim_atoms[atom].shell_index;
  int_g index = get_atom_basis_index(atom);
  int_g count = 0;
  for (int_g i = 0; i < prim_atoms[atom].nshells; i++) {
    switch (shells[k].shell_type) {
    case 0:
      if (count == n)
	return index;
      index++;
      count++;
      break;
    case 1:
      if (count == n)
	return index;
      index += 4;
      count++;
      break;
    case 2:
      index += 3;
      break;
    case 3:
      index += 5;
      break;
    case 4:
      index += 7;
      break;
    case 5:
      index += 9;
      break;
    default:
      index++;
      break;
    }
    k++;
  }
  return -1;
}

CRYSTAL::int_g CRYSTAL::wavefn_calc::get_p_or_sp_orbital(const int_g atom,
							 const int_g n) const
{
  int_g k = prim_atoms[atom].shell_index;
  int_g count = 1;
  int_g index = get_atom_basis_index(atom);
  for (int_g i = 0; i < prim_atoms[atom].nshells; i++) {
    switch (shells[k].shell_type) {
    case 0:
      index++;
      break;
    case 1:
      if (count == n)
	return index + 1;
      index += 4;
      count++;
      break;
    case 2:
      if (count == n)
	return index;
      index += 3;
      count++;
      break;
    case 3:
      index += 5;
      break;
    case 4:
      index += 7;
      break;
    case 5:
      index += 9;
      break;
    default:
      index++;
      break;
    }
    k++;
  }
  return -1;
}

CRYSTAL::int_g CRYSTAL::wavefn_calc::get_d_orbital(const int_g atom,
						   const int_g n) const
{
  int_g k = prim_atoms[atom].shell_index;
  int_g count = 2;
  int_g index = get_atom_basis_index(atom);
  for (int_g i = 0; i < prim_atoms[atom].nshells; i++) {
    switch (shells[k].shell_type) {
    case 0:
      index++;
      break;
    case 1:
      index += 4;
      break;
    case 2:
      index += 3;
      break;
    case 3:
      if (count == n)
	return index;
      index += 5;
      count++;
      break;
    case 4:
      index += 7;
      break;
    case 5:
      index += 9;
      break;
    default:
      index++;
      break;
    }
    k++;
  }
  return -1;
}

CRYSTAL::int_g CRYSTAL::wavefn_calc::get_f_orbital(const int_g atom,
						   const int_g n) const
{
  int_g k = prim_atoms[atom].shell_index;
  int_g count = 3;
  int_g index = get_atom_basis_index(atom);
  for (int_g i = 0; i < prim_atoms[atom].nshells; i++) {
    switch (shells[k].shell_type) {
    case 0:
      index++;
      break;
    case 1:
      index += 4;
      break;
    case 2:
      index += 3;
      break;
    case 3:
      index += 5;
      break;
    case 4:
      if (count == n)
	return index;
      index += 7;
      count++;
      break;
    case 5:
      index += 9;
      break;
    default:
      index++;
      break;
    }
    k++;
  }
  return -1;
}

CRYSTAL::int_g CRYSTAL::wavefn_calc::get_g_orbital(const int_g atom,
						   const int_g n) const
{
  int_g k = prim_atoms[atom].shell_index;
  int_g count = 4;
  int_g index = get_atom_basis_index(atom);
  for (int_g i = 0; i < prim_atoms[atom].nshells; i++) {
    switch (shells[k].shell_type) {
    case 0:
      index++;
      break;
    case 1:
      index += 4;
      break;
    case 2:
      index += 3;
      break;
    case 3:
      index += 5;
      break;
    case 4:
      index += 7;
      break;
    case 5:
      if (count == n)
	return index;
      index += 9;
      count++;
      break;
    default:
      index++;
      break;
    }
    k++;
  }
  return -1;
}

CRYSTAL::int_g CRYSTAL::wavefn_calc::get_max_n(const int_g atom,
					       const int_g l) const
{
  int_g k = prim_atoms[atom].shell_index;
  int_g count = l;
  for (int_g i = 0; i < prim_atoms[atom].nshells; i++) {
    switch (shells[k].shell_type) {
    case 0:
      if (l == 0)
	count++;
      break;
    case 1:
      if (l == 0 or l == 1)
	count++;
      break;
    case 2:
      if (l == 1)
	count++;
      break;
    case 3:
      if (l == 2)
	count++;
      break;
    case 4:
      if (l == 3)
	count++;
      break;
    case 5:
      if (l == 4)
	count++;
      break;
    default:
      break;
    }
    k++;
  }
  return count;
}

bool CRYSTAL::wavefn_analyse::is_geometry() const
{
  return true;
}

void CRYSTAL::wavefn_analyse::inherit_model()
{
  // Null op, we have a new structure
}

void CRYSTAL::wavefn_analyse::inherit_data()
{
  // Todo - I think this is possibly a null op, but see map_data
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::wavefn_calc *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::wavefn_calc("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::wavefn_calc_v6 *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::wavefn_calc_v6("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::wavefn_analyse *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::wavefn_analyse("recover", false, false, false);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::wavefn_analyse_v6 *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::wavefn_analyse_v6("recover", false, false, false);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::wavefn_analyse_v8 *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::wavefn_analyse_v8("recover", "", false, false, false);
    }

  }
}

template <class Archive>
void CRYSTAL::atom_data::serialize(Archive &ar, const unsigned int version)
{
  ar & atomic_number;
  ar & asym_index;
  ar & basis_index;
  ar & nshells;
  ar & shell_index;
  ar & coords[0];
  ar & coords[1];
      ar & coords[2];
}

template <class Archive>
void CRYSTAL::shell_data::serialize(Archive &ar, const unsigned int version)
{
  ar & shell_type;
}

template <class Archive>
void CRYSTAL::wavefn_calc::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<CRYSTAL::calculate>(*this);
  ar & filename;
  ar & nshells;
  ar & norbitals;
  ar & natoms;
  ar & electrons_per_cell;
  ar & core_electrons;
  ar & has_fermi;
  ar & spin;
  ar & spin_electrons;
  ar & nkpoints;
  ar & shrinka;
  ar & shrinkb;
  ar & shrinkc;
  for (int i = 0; i < natoms; i++)
    ar & prim_atoms[i];
  for (int i = 0; i < nshells; i++)
    ar & shells[i];
  for (int i = 0; i < nkpoints; i++) {
    ar & kpoints[i][0];
    ar & kpoints[i][1];
    ar & kpoints[i][2];
  }
  ar & binary;
  // Todo - rest of info should come from reload?
}

template <class Archive>
void CRYSTAL::wavefn_calc::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::calculate>(*this);
  ar & filename;
  ar & nshells;
  ar & norbitals;
  ar & natoms;
  ar & electrons_per_cell;
  ar & core_electrons;
  ar & has_fermi;
  ar & spin;
  ar & spin_electrons;
  ar & nkpoints;
  ar & shrinka;
  ar & shrinkb;
  ar & shrinkc;
  prim_atoms = new atom_data[natoms];
  for (int i = 0; i < natoms; i++)
    ar & prim_atoms[i];
  shells = new shell_data[nshells];
  for (int i = 0; i < nshells; i++)
    ar & shells[i];
  kpoints = new int[nkpoints][3];
  for (int i = 0; i < nkpoints; i++) {
    ar & kpoints[i][0];
    ar & kpoints[i][1];
    ar & kpoints[i][2];
  }
  if (version > 0)
    ar & binary;
  else
    binary = false;
  // Todo - rest of info should come from reload?
#ifdef ENABLE_DLV_GRAPHICS
  const DLV::model *const m = get_model();
  get_display_obj()->set_CRYSTAL_wavefn((core_electrons / 2) + 1,
					spin_electrons,
					norbitals, m->get_lattice_type(),
					m->get_lattice_centring(),
					spin, kpoints, nkpoints,
					shrinka, shrinkb, shrinkc);
#endif // ENABLE_DLV_GRAPHICS
}

template <class Archive>
void CRYSTAL::wavefn_calc_v6::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::wavefn_calc>(*this);
}

template <class Archive>
void CRYSTAL::wavefn_analyse::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::wavefn_calc>(*this);
  ar & use_new_view;
}

template <class Archive>
void CRYSTAL::wavefn_analyse_v6::serialize(Archive &ar,
					   const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::wavefn_analyse>(*this);
}

template <class Archive>
void CRYSTAL::wavefn_analyse_v8::serialize(Archive &ar,
					   const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::wavefn_analyse_v6>(*this);
  ar & tddft_file;
}

BOOST_CLASS_VERSION(CRYSTAL::wavefn_calc, 1)

BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::wavefn_calc)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::wavefn_calc_v6)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::wavefn_analyse)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::wavefn_analyse_v6)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::wavefn_analyse_v8)

DLV_SUPPRESS_TEMPLATES(CRYSTAL::calculate)

DLV_NORMAL_EXPLICIT(CRYSTAL::wavefn_calc)
DLV_NORMAL_EXPLICIT(CRYSTAL::wavefn_calc_v6)
DLV_NORMAL_EXPLICIT(CRYSTAL::wavefn_analyse)
DLV_NORMAL_EXPLICIT(CRYSTAL::wavefn_analyse_v6)
DLV_NORMAL_EXPLICIT(CRYSTAL::wavefn_analyse_v8)

#endif // DLV_USES_SERIALIZE
