
#ifndef CRYSTAL_RUN_INPUT
#define CRYSTAL_RUN_INPUT

namespace CRYSTAL {

  // v5+ only
  class run_calc : public calc_geom, public structure_file {
  public:
    static operation *create(const char file[], const char str[],
			     const bool use_str,
			     const DLV::job_setup_data &job, const bool mpp,
			     char message[], const int_g mlen);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

    // public for serialization
    run_calc(const char file[]);

  protected:
    DLV::string get_name() const;
    bool create_files(const bool is_parallel, const bool is_local,
		      char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

    virtual bool has_structure() const;
    virtual DLV::string get_structure_file() const;
    virtual void add_structure_file(const char tag[], const bool is_local);
    void write(const DLV::string outfile, char message[], const int_g mlen);
    void add_standard_data_objects();

    DLV::string get_serial_executable() const;
    DLV::string get_parallel_executable(const bool mpp = false) const;

  private:
    DLV::string filename;

    static DLV::string serial_binary;
    static DLV::string parallel_binary;
    static DLV::string mpp_binary;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class run_str_calc : public run_calc {
  public:
    run_str_calc(const char file[], const char str[]);

  protected:
    bool has_structure() const;
    DLV::string get_structure_file() const;
    void add_structure_file(const char tag[], const bool is_local);

  private:
    DLV::string structure_file;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(CRYSTAL::run_calc)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::run_str_calc)
#endif // DLV_USES_SERIALIZE

inline CRYSTAL::run_calc::run_calc(const char file[]) : filename(file)
{
}

inline CRYSTAL::run_str_calc::run_str_calc(const char file[], const char str[])
  : run_calc(file), structure_file(str)
{
}

inline DLV::string CRYSTAL::run_calc::get_serial_executable() const
{
  return serial_binary;
}

inline
DLV::string CRYSTAL::run_calc::get_parallel_executable(const bool mpp) const
{
  if (mpp)
    return mpp_binary;
  else
    return parallel_binary;
}

#endif // CRYSTAL_RUN_INPUT
