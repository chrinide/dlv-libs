
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_plots.hxx"
#include "plots.hxx"

bool CRYSTAL::plot25::read_plot(std::ifstream &input, DLV::plot_data *data,
				const DLV::int_g flag, const DLV::int_g npoints,
				char message[], const DLV::int_g mlen)
{
  DLV::real_g *plotdata = new DLV::real_g[npoints];
  for (DLV::int_g i = 0; i < npoints; i++)
    input >> plotdata[i];
  // Todo - what does flag do?
  data->add_plot(plotdata, npoints, false);
  return true;
}
