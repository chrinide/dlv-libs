
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/operation.hxx"
//#include "model.hxx"
#include "extern_gui.hxx"
#include "boost/python.hpp"

#define DLV_MODULE_NAME gui

BOOST_PYTHON_OPAQUE_SPECIALIZED_TYPE_ID(DLV::operation)

BOOST_PYTHON_MODULE(DLV_MODULE_NAME)
{
  using namespace boost::python;
  def("load", CRYSTAL::load_gui, boost::python::return_value_policy<boost::python::return_opaque_pointer>());
  def("save", CRYSTAL::save_gui, boost::python::return_value_policy<boost::python::return_opaque_pointer>());
}
