
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/constants.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_surf.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/calculation.hxx"
#include "calcs.hxx"
#include "props.hxx"
#include "slices.hxx"

CRYSTAL::grid2D::~grid2D()
{
  // Todo?
}

DLV::surface_data *CRYSTAL::grid2D::read_data(DLV::operation *op,
					      DLV::surface_data *s,
					      const char filename[],
					      const DLV::string id,
					      const char label[],
					      const int_g max_datasets,
					      char message[],
					      const int_g mlen,
					      const bool periodic)
{
  DLV::surface_data *data = 0;
  if (DLV::check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (DLV::open_file_read(input, filename, message, mlen)) {
      int_g nx = 0;
      int_g ny = 0;
      real_g origin[3];
      real_g astep[3];
      real_g bstep[3];
      bool reverse;
      if (get_grid(input, nx, ny, origin, astep, bstep, reverse,
		   message, mlen)) {
	if (s == 0) {
	  if (periodic)
	    data = new DLV::rspace_periodic_surface("CRYSTAL", id, op, nx, ny,
						    origin, astep, bstep);
	  else
	    data = new DLV::real_space_surface("CRYSTAL", id, op, nx, ny,
					       origin, astep, bstep);
	} else
	  data = s;
	if (!get_data(input, data, nx, ny, reverse, label, max_datasets,
		      message, mlen)) {
	  delete data;
	  data = 0;
	}
      }
      input.close();
    }
  }
  return data;
}

bool CRYSTAL::grid2D::get_grid(std::ifstream &input, int_g &nx, int_g &ny,
			       real_g origin[3], real_g astep[3],
			       real_g bstep[3], bool &reverse, char message[],
			       const int_g mlen)
{
  char line[128];
  input >> line;
  input >> nx;
  input >> ny;
  if (nx < 1 or ny < 1) {
    strncpy(message,
	    "Empty grid - is this really a CRYSTAL mapnet file?", mlen);
    return false;
  } else {
    input.getline(line,128);
    return read_grid(input, nx, ny, origin, astep, bstep,
		     reverse, message, mlen);
  }
}

bool CRYSTAL::grid2D::read_grid(std::ifstream &input, int_g &nx, int_g &ny,
				real_g origin[3], real_g astep[3],
				real_g bstep[3], bool &reverse,
				char message[], const int_g mlen)
{
  real_g a[3];
  input >> a[0];
  input >> a[1];
  input >> a[2];
  real_g b[3];
  input >> b[0];
  input >> b[1];
  input >> b[2];
  real_g c[3];
  input >> c[0];
  input >> c[1];
  input >> c[2];
  const bool au = false;
  if (au) {
    for (int_g i = 0; i < 3; i++) {
      a[i] = real_g(a[i] * C09_bohr_to_angstrom);
      b[i] = real_g(b[i] * C09_bohr_to_angstrom);
      c[i] = real_g(c[i] * C09_bohr_to_angstrom);
    }
  }
  // The AGkit gets upset when the origin is larger than the end point,
  // which results in an axis where the coordinate decreases.
  // This is an attempt to fix this.
  real_g lena = (a[0] * a[0] + a[1] * a[1] + a[2] * a[2]);
  real_g lenb = (b[0] * b[0] + b[1] * b[1] + b[2] * b[2]);
  if (lena > lenb) {
    reverse = true;
    for (int_g i = 0; i < 3; i++) {
      origin[i] = b[i];
      astep[i] = (a[i] - b[i]) / (real_g) (nx - 1);
      bstep[i] = (c[i] - b[i]) / (real_g) (ny - 1);
    }
  } else {
    reverse = false;
    for (int_g i = 0; i < 3; i++) {
      origin[i] = a[i];
      astep[i] = (b[i] - a[i]) / (real_g) (nx - 1);
      bstep[i] = (c[i] - b[i]) / (real_g) (ny - 1);
    }
  }
  return true;
}

bool CRYSTAL::grid2D_v4::get_data(std::ifstream &input,
				  DLV::surface_data *data, const int_g nx,
				  const int_g ny, const bool reverse,
				  const char label[], const int_g max_datasets,
				  char message[], const int_g mlen)
{
  char line[128];
  // clear line
  input.getline(line, 128);
  bool ok = true;
  if ((ok = read_data(input, data, nx, ny, reverse, label, message, mlen))) {
    if (max_datasets > 1) {
      char line[128];
      // Check for spin density.
      input.getline(line, 128);
      if (!input.eof()) {
	// There is a spin density also, skip extra info
	input.getline(line, 128);
	input.getline(line, 128);
	ok = read_data(input, data, nx, ny, reverse, "Spin Density",
		       message, mlen);
      }
    }
  }
  return ok;
}

bool CRYSTAL::grid2D_v5::get_data(std::ifstream &input,
				  DLV::surface_data *data, const int_g nx,
				  const int_g ny, const bool reverse,
				  const char label[], const int_g max_datasets,
				  char message[], const int_g mlen)
{
  int_g na;
  input >> na;
  int_g np;
  input >> np;
  // clear line
  //input.getline(line, 128);
  bool ok = true;
  if ((ok = read_data(input, data, nx, ny, reverse, label, message, mlen))) {
    if (max_datasets > 1) {
      char line[128];
      // Check for spin density.
      // skip atom info
      for (int_g i = 0; i < (na + np); i++)
	input.getline(line, 128);
      // Check for 1 more line - if there then spin density exists.
      input.getline(line, 128);
      if (!input.eof()) {
	// There is a spin density also, skip extra info
	input.getline(line, 128);
	input.getline(line, 128);
	ok = read_data(input, data, nx, ny, reverse, "Spin Density",
		       message, mlen);
      }
    }
  }
  return ok;
}

bool CRYSTAL::grid2D::read_data(std::ifstream &input, DLV::surface_data *data,
				const int_g nx, const int_g ny,
				const bool reverse, const char label[],
				char message[], const int_g mlen)
{
  int_g n = nx * ny;
  real_g *array = new real_g[n];
  if (array == 0) {
    strncpy(message, "Unable to allocate memory for data", mlen);
    return false;
  } else {
    // The really tricky bit is getting this right for the reversed axes.
    if (reverse) {
      for (int_g j = 0; j < ny; j++) {
	int_g k = j * nx;
	for (int_g i = nx - 1; i >= 0; i--)
	  input >> array[k + i];
      }
    } else {
      real_l temp; // wannier issues with out of range values.
      for (int_g i = 0; i < n; i++) {
	input >> temp;
	array[i] = (real_g) temp;
      }
    }
    data->add_data(array, label, false);
    // Get End of Line.
    char line[128];
    input.getline(line, 128);
    return true;
  }
}

void CRYSTAL::grid2D::write_mapnet(std::ofstream &output,
				   const DLV::plane *grid, const int_g np) const
{
  output << np << '\n';
  real_g o[3];
  real_g a[3];
  real_g b[3];
  grid->get_vertices(o, a, b);
  output << "COORDINA\n";
  output << a[0] << ' ' << a[1] << ' ' << a[2] << '\n';
  output << o[0] << ' ' << o[1] << ' ' << o[2] << '\n';
  output << b[0] << ' ' << b[1] << ' ' << b[2] << '\n';
  output << "END\n";
}
