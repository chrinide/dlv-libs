
#ifndef CRYSTAL_3D_DATA
#define CRYSTAL_3D_DATA

namespace CRYSTAL {

  class grid_3d_data {
  public:
    // serialization
    virtual ~grid_3d_data();

  protected:
    grid_3d_data(const bool do_cd, const int_g np, const int_g rs,
		 const int_g cell, const real_l xi, const real_l xa,
		 const real_l yi, const real_l ya, const real_l zi,
		 const real_l za);

  protected:
    void write_input(std::ofstream &output, const DLV::model *const structure);
    virtual void write_cell(std::ofstream &output, const int_g dims) const;
    virtual void write_command(std::ofstream &output) const;
    void write_points(std::ofstream &output) const;
    virtual void write_data_sets(std::ofstream &output) const;
    bool is_charge_calc() const;

  private:
    bool calc_charge;
    int_g npoints;
    bool use_range;
    bool conv_cell;
    real_l x_min;
    real_l x_max;
    real_l y_min;
    real_l y_max;
    real_l z_min;
    real_l z_max;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class grid_3d_data_v4 : public grid_3d_data, public grid3D_v4,
			  public property_data {
  protected:
    grid_3d_data_v4(const bool do_cd, const int_g np, const int_g rs,
		    const real_l xi, const real_l xa, const real_l yi,
		    const real_l ya, const real_l zi, const real_l za,
		    const Density_Matrix &dm, const NewK &nk);
    void write_cell(std::ofstream &output, const int_g dims) const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class grid_3d_data_v5 : public grid_3d_data, public grid3D_v5,
			  public property_data {
  protected:
    grid_3d_data_v5(const bool do_cd, const int_g np, const int_g rs,
		    const int_g cell, const real_l xi, const real_l xa,
		    const real_l yi, const real_l ya, const real_l zi,
		    const real_l za, const Density_Matrix &dm,
		    const NewK &nk);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class grid_3d_data_v6 : public grid_3d_data, public grid3D_v6,
			  public property_data_v6 {
  protected:
    grid_3d_data_v6(const bool do_cd, const bool do_pot, const int_g np,
		    const int_g rs, const int_g cell, const int_g tol,
		    const real_l xi, const real_l xa, const real_l yi,
		    const real_l ya, const real_l zi, const real_l za,
		    const Density_Matrix &dm, const NewK &nk);
    void write_command(std::ofstream &output) const;
    void write_data_sets(std::ofstream &output) const;

  private:
    bool calc_potential;
    int_g pot_itol;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class tddft_3d_data_v8 : public grid_3d_data, public grid3D_v6,
			   public property_data_v6 {
  protected:
    tddft_3d_data_v8(const int_g calc, const int_g exc, const int_g np,
		     const int_g rs, const int_g cell, const int_g tol,
		     const real_l xi, const real_l xa, const real_l yi,
		     const real_l ya, const real_l zi, const real_l za,
		     const Density_Matrix &dm, const NewK &nk);
    void write_command(std::ofstream &output) const;
    void write_data_sets(std::ofstream &output) const;

  private:
    int_g property;
    int_g excitation;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_3d_data : public DLV::load_data_op {
  public:
    static operation *create(const char filename[], const int_g version,
			     char message[], const int_g mlen);

  protected:
    load_3d_data(const char file[]);

    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_3d_data_v4 : public load_3d_data, public grid3D_v4 {
  public:
    static operation *create(const char filename[],
                             char message[], const int_g mlen);

    // public for serialization
    load_3d_data_v4(const char file[]);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_3d_data_v5 : public load_3d_data, public grid3D_v5 {
  public:
    static operation *create(const char filename[],
                             char message[], const int_g mlen);

    // public for serialization
    load_3d_data_v5(const char file[]);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_3d_data_v6 : public load_3d_data, public grid3D_v6 {
  public:
    static operation *create(const char filename[],
                             char message[], const int_g mlen);

    // public for serialization
    load_3d_data_v6(const char file[]);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class grid_3d_calc : public property_calc {
  public:
    static operation *create(const bool do_cd, const bool do_pot,
			     const int_g np, const int_g rs, const int_g cell,
			     const int_g tol, const real_l xi, const real_l xa,
			     const real_l yi, const real_l ya, const real_l zi,
			     const real_l za, const Density_Matrix &dm,
			     const NewK &nk, const int_g version,
			     const DLV::job_setup_data &job,
			     const bool extern_job, const char extern_dir[],
			     char message[], const int_g mlen);
    static operation *tddft(const int_g calc, const int_g exc,
			    const int_g np, const int_g rs, const int_g cell,
			    const int_g tol, const real_l xi, const real_l xa,
			    const real_l yi, const real_l ya, const real_l zi,
			    const real_l za, const Density_Matrix &dm,
			    const NewK &nk, const int_g version,
			    const DLV::job_setup_data &job,
			    const bool extern_job, const char extern_dir[],
			    char message[], const int_g mlen);

  protected:
    DLV::string get_name() const;

    bool create_files(const bool is_parallel, const bool is_local,
		      char message[], const int_g mlen);
    virtual void write(const DLV::string filename, const DLV::operation *op,
		       const DLV::model *const structure,
		       char message[], const int_g mlen) = 0;
    bool is_binary() const;
    virtual bool is_tddft() const;

  private:
    bool binary_wvfn;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class grid_3d_calc_v4 : public grid_3d_calc, public grid_3d_data_v4 {
  public:
    grid_3d_calc_v4(const bool do_cd, const int_g np, const int_g rs,
		    const real_l xi, const real_l xa, const real_l yi,
		    const real_l ya, const real_l zi, const real_l za,
		    const Density_Matrix &dm, const NewK &nk);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    void write(const DLV::string filename, const DLV::operation *op,
	       const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class grid_3d_calc_v5 : public grid_3d_calc, public grid_3d_data_v5 {
  public:
    grid_3d_calc_v5(const bool do_cd, const int_g np, const int_g rs,
		    const int_g cell, const real_l xi, const real_l xa,
		    const real_l yi, const real_l ya, const real_l zi,
		    const real_l za, const Density_Matrix &dm,
		    const NewK &nk);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    void write(const DLV::string filename, const DLV::operation *op,
	       const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class grid_3d_calc_v6 : public grid_3d_calc, public grid_3d_data_v6 {
  public:
    grid_3d_calc_v6(const bool do_cd, const bool do_pot, const int_g np,
		    const int_g rs, const int_g cell, const int_g tol,
		    const real_l xi, const real_l xa, const real_l yi,
		    const real_l ya, const real_l zi, const real_l za,
		    const Density_Matrix &dm, const NewK &nk);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void write(const DLV::string filename, const DLV::operation *op,
	       const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class tddft_3d_calc_v8 : public grid_3d_calc, public tddft_3d_data_v8 {
  public:
    tddft_3d_calc_v8(const int_g calc, const int_g exc, const int_g np,
		     const int_g rs, const int_g cell, const int_g tol,
		     const real_l xi, const real_l xa, const real_l yi,
		     const real_l ya, const real_l zi, const real_l za,
		     const Density_Matrix &dm, const NewK &nk);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void write(const DLV::string filename, const DLV::operation *op,
	       const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool is_tddft() const;
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(CRYSTAL::load_3d_data_v4)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::load_3d_data_v5)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::load_3d_data_v6)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::grid_3d_calc_v4)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::grid_3d_calc_v5)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::grid_3d_calc_v6)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::tddft_3d_calc_v8)
#endif // DLV_USES_SERIALIZE

inline CRYSTAL::grid_3d_data::grid_3d_data(const bool do_cd, const int_g np,
					   const int_g rs, const int_g cell,
					   const real_l xi, const real_l xa,
					   const real_l yi, const real_l ya,
					   const real_l zi, const real_l za)
  : calc_charge(do_cd), npoints(np), use_range(rs), conv_cell(cell), x_min(xi),
    x_max(xa), y_min(yi), y_max(ya), z_min(zi), z_max(za)
{
}

inline CRYSTAL::grid_3d_data_v4::grid_3d_data_v4(const bool do_cd,
						 const int_g np, const int_g rs,
						 const real_l xi,
						 const real_l xa,
						 const real_l yi,
						 const real_l ya,
						 const real_l zi,
						 const real_l za,
						 const Density_Matrix &dm,
						 const NewK &nk)
  : grid_3d_data(do_cd, np, rs, false, xi, xa, yi, ya, zi, za),
    property_data(dm, nk)
{    
}

inline CRYSTAL::grid_3d_data_v5::grid_3d_data_v5(const bool do_cd,
						 const int_g np,
						 const int_g rs,
						 const int_g cell,
						 const real_l xi,
						 const real_l xa,
						 const real_l yi,
						 const real_l ya,
						 const real_l zi,
						 const real_l za,
						 const Density_Matrix &dm,
						 const NewK &nk)
  : grid_3d_data(do_cd, np, rs, cell, xi, xa, yi, ya, zi, za),
    property_data(dm, nk)
{
}

inline CRYSTAL::grid_3d_data_v6::grid_3d_data_v6(const bool do_cd,
						 const bool do_pot,
						 const int_g np, const int_g rs,
						 const int_g cell,
						 const int_g tol,
						 const real_l xi,
						 const real_l xa,
						 const real_l yi,
						 const real_l ya,
						 const real_l zi,
						 const real_l za,
						 const Density_Matrix &dm,
						 const NewK &nk)
  : grid_3d_data(do_cd, np, rs, cell, xi, xa, yi, ya, zi, za),
    property_data_v6(dm, nk), calc_potential(do_pot), pot_itol(tol)
{
}

inline CRYSTAL::load_3d_data::load_3d_data(const char file[])
  : load_data_op(file)
{
}

inline CRYSTAL::load_3d_data_v4::load_3d_data_v4(const char file[])
  : load_3d_data(file)
{
}

inline CRYSTAL::load_3d_data_v5::load_3d_data_v5(const char file[])
  : load_3d_data(file)
{
}

inline CRYSTAL::load_3d_data_v6::load_3d_data_v6(const char file[])
  : load_3d_data(file)
{
}

inline bool CRYSTAL::grid_3d_calc::is_binary() const
{
  return binary_wvfn;
}

inline CRYSTAL::grid_3d_calc_v4::grid_3d_calc_v4(const bool do_cd,
						 const int_g np, const int_g rs,
						 const real_l xi,
						 const real_l xa,
						 const real_l yi,
						 const real_l ya,
						 const real_l zi,
						 const real_l za,
						 const Density_Matrix &dm,
						 const NewK &nk)
  : grid_3d_data_v4(do_cd, np, rs, xi, xa, yi, ya, zi, za, dm, nk)
{
}

inline CRYSTAL::grid_3d_calc_v5::grid_3d_calc_v5(const bool do_cd,
						 const int_g np, const int_g rs,
						 const int_g cell,
						 const real_l xi,
						 const real_l xa,
						 const real_l yi,
						 const real_l ya,
						 const real_l zi,
						 const real_l za,
						 const Density_Matrix &dm,
						 const NewK &nk)
  : grid_3d_data_v5(do_cd, np, rs, cell, xi, xa, yi, ya, zi, za, dm, nk)
{
}

inline CRYSTAL::grid_3d_calc_v6::grid_3d_calc_v6(const bool do_cd,
						 const bool do_pot,
						 const int_g np, const int_g rs,
						 const int_g cell,
						 const int_g tol,
						 const real_l xi,
						 const real_l xa,
						 const real_l yi,
						 const real_l ya,
						 const real_l zi,
						 const real_l za,
						 const Density_Matrix &dm,
						 const NewK &nk)
  : grid_3d_data_v6(do_cd, do_pot, np, rs, cell, tol,
		    xi, xa, yi, ya, zi, za, dm, nk)
{
}

inline bool CRYSTAL::grid_3d_data::is_charge_calc() const
{
  return calc_charge;
}

inline CRYSTAL::tddft_3d_data_v8::tddft_3d_data_v8(const int_g calc,
						   const int_g exc,
						   const int_g np,
						   const int_g rs,
						   const int_g cell,
						   const int_g tol,
						   const real_l xi,
						   const real_l xa,
						   const real_l yi,
						   const real_l ya,
						   const real_l zi,
						   const real_l za,
						   const Density_Matrix &dm,
						   const NewK &nk)
  : grid_3d_data(false, np, rs, cell, xi, xa, yi, ya, zi, za),
    property_data_v6(dm, nk), property(calc), excitation(exc)
{
}

inline CRYSTAL::tddft_3d_calc_v8::tddft_3d_calc_v8(const int_g calc,
						   const int_g exc,
						   const int_g np,
						   const int_g rs,
						   const int_g cell,
						   const int_g tol,
						   const real_l xi,
						   const real_l xa,
						   const real_l yi,
						   const real_l ya,
						   const real_l zi,
						   const real_l za,
						   const Density_Matrix &dm,
						   const NewK &nk)
  : tddft_3d_data_v8(calc, exc, np, rs, cell, tol,
		     xi, xa, yi, ya, zi, za, dm, nk)
{
}

#endif // CRYSTAL_3D_DATA
