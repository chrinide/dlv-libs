
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/constants.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_admin.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/data_atoms.hxx"
#include "../dlv/file.hxx"
#include "../dlv/job_setup.hxx"
#include "calcs.hxx"
#include "props.hxx"
#include "mulliken.hxx"
#include "../dlv/op_model.hxx"
#include "wavefn.hxx"

DLV::operation *CRYSTAL::mulliken_calc::create(const int_g n, const int_g r,
					       const Density_Matrix &dm,
					       const NewK &nk,
					       const int_g version,
					       const DLV::job_setup_data &job,
					       const bool extern_job,
					       const char extern_dir[],
					       char message[], const int_g mlen)
{
  mulliken_calc *op = 0;
  switch (version) {
  case CRYSTAL03:
    op = new mulliken_calc_v5(n, dm, nk);
    break;
  case CRYSTAL06:
    op = new mulliken_calc_v6(n, dm, nk);
    break;
  case CRYSTAL09:
  case CRYSTAL14:
  case CRYSTAL17:
  case CRYSTAL23:
  case CRYSTAL_DEV:
    op = new mulliken_calc_v7(n, (r == 1), dm, nk);
    break;
  default:
    strncpy(message, "BUG: unknown CRYSTAL version", mlen);
    break;
  }
  if (op == 0)
    strncpy(message, "Failed to allocate CRYSTAL::mulliken_calc operation",
	    mlen);
  else {
    message[0] = '\0';
    // Todo - is parallel relevant?
    op->attach();
    if (op->create_files(false, job.is_local(), message, mlen))
      if (op->make_directory(message, mlen))
	op->write(op->get_infile_name(0), op, op->get_model(), message, mlen);
    if (strlen(message) > 0) {
      // don't delete as already attached (so can find_wavefunction)
      // Todo - flag name as failed job?
      //delete op;
      //op = 0;
    } else {
      op->execute(op->get_path(), op->get_serial_executable(),
		  op->get_parallel_executable(), "", job, false,
		  extern_job, extern_dir, message, mlen);
    }
  }
  return op;
}

DLV::string CRYSTAL::mulliken_calc::get_name() const
{
  return ("CRYSTAL Mulliken population calculation");
}

DLV::operation *CRYSTAL::load_mulliken_data::create(const char filename[],
						    char message[],
						    const int_g mlen)
{
  load_mulliken_data *op = new load_mulliken_data(filename);
  DLV::data_object *data = op->read_data(op, 0, 0, filename, filename,
					 message, mlen);
  if (data == 0) {
    delete op;
    op = 0;
  } else {
    op->attach();
    op->attach_data(data);
  }
  return op;
}

DLV::string CRYSTAL::load_mulliken_data::get_name() const
{
  return ("Load CRYSTAL Mulliken population - " + get_filename());
}

void CRYSTAL::mulliken_calc_v6::add_calc_error_file(const DLV::string tag,
						    const bool is_parallel,
						    const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

void CRYSTAL::mulliken_calc_v7::add_calc_error_file(const DLV::string tag,
						    const bool is_parallel,
						    const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

bool CRYSTAL::mulliken_calc::create_files(const bool is_parallel,
					  const bool is_local, char message[],
					  const int_g mlen)
{
  DLV::string filename;
  if (find_wavefunction(filename, binary_wvfn, message, mlen)) {
    static char tag[] = "pop";
    static char input[] = "inp";
    add_calc_error_file(tag, is_parallel, is_local);
    if (is_parallel)
      add_input_file(0, tag, input, "INPUT", is_local, false);
    else
      add_command_file(0, tag, input, is_local, false);
    if (binary_wvfn)
      add_data_file(1, filename, "fort.9", is_local, true);
    else
      add_data_file(1, filename, "fort.98", is_local, true);
    add_log_file(tag, "out", is_local, true);
    add_sys_error_file(tag, "job", is_local, true);
    add_output_file(0, tag, "dat", "PPAN.DAT", is_local, true);
    return true;
  } else
    return false;
}

void CRYSTAL::mulliken_data::write_cmd(std::ofstream &output)
{
  output << "PPAN\n";
  output << neighbours;
  output << ' ';
  output << 0;
  output << '\n';
  output << "END\n";
}

void CRYSTAL::mulliken_data::write_cmd_v6(std::ofstream &output)
{
  output << "NEIGHBOR\n";
  output << neighbours << '\n';
  output << "PPAN\n";
  output << "END\n";
}

void
CRYSTAL::mulliken_data_v7::write_rotation(std::ofstream &output,
					  const DLV::model *const structure,
					  const DLV::atom_integers *data,
					  char message[], const int_g mlen)
{
  if (rotate) {
#ifdef ENABLE_DLV_GRAPHICS    
    int_g n = 0;
    int_g *prim_labels = 0;
    //int *types = 0;
    int_g (*shifts)[3] = 0;
    //DLV::atom *asym = 0;
    if (data->map_selected_atoms(structure, n, prim_labels, shifts)) {
      //delete [] asym;
      //int shifts[3][3];
      //DLV::coord_type p[3][3];
      //int atoms[3];
      //structure->get_selected_positions(p[0], p[1], p[2]);
      //structure->get_atom_shifts(p, shifts, types, atoms, 3);
      //delete [] types;
      output << "ROTREF\n";
      output << "ATOMS\n";
      output.width(5);
      // shifts are into cell 0, so cell they are in is -ve shift?
      output << prim_labels[2];
      output.width(5);
      output << -shifts[2][0];
      output.width(5);
      output << -shifts[2][1];
      output.width(5);
      output << -shifts[2][2] << '\n';
      output.width(5);
      output << prim_labels[1];
      output.width(5);
      output << -shifts[1][0];
      output.width(5);
      output << -shifts[1][1];
      output.width(5);
      output << -shifts[1][2] << '\n';
      output.width(5);
      output << prim_labels[0];
      output.width(5);
      output << -shifts[0][0];
      output.width(5);
      output << -shifts[0][1];
      output.width(5);
      output << -shifts[0][2] << '\n';
      delete [] shifts;
      delete [] prim_labels;
    } else {
      strncpy(message, "Failed to locate selections\n", mlen);
      return;
    }
#else
    strncpy(message, "Failed to locate selections\n", mlen);
    return;
 #endif // ENABLE_DLV_GRAPHICS
  }
}

void CRYSTAL::mulliken_calc_v5::write(const DLV::string filename,
				      const DLV::operation *op,
				      const DLV::model *const structure,
				      char message[], const int_g mlen)
{
  std::ofstream output;
  if (DLV::open_file_write(output, filename.c_str(), message, mlen)) {
    write_header(output, op, structure, is_binary());
    write_cmd(output);
    output.close();
  }
}

void CRYSTAL::mulliken_calc_v6::write(const DLV::string filename,
				      const DLV::operation *op,
				      const DLV::model *const structure,
				      char message[], const int_g mlen)
{
  std::ofstream output;
  if (DLV::open_file_write(output, filename.c_str(), message, mlen)) {
    write_header(output, op, structure, is_binary());
    write_cmd_v6(output);
    output.close();
  }
}

void CRYSTAL::mulliken_calc_v7::write(const DLV::string filename,
				      const DLV::operation *op,
				      const DLV::model *const structure,
				      char message[], const int_g mlen)
{
  const DLV::atom_integers *s = 0;
  if (has_rotate()) {
#ifdef ENABLE_DLV_GRAPHICS
    if (structure->get_number_selected_atoms() < 3) {
      strncpy(message, "3 atoms must be selected to specify rotation", mlen);
      return;
    }
#endif // ENABLE_DLV_GRAPHICS
    const DLV::data_object *data = find_data(prim_atom_label, program_name);
    if (data == 0) {
      strncpy(message, "Unable to locate primitive atom indices", mlen);
      return;
    }
    s = dynamic_cast<const DLV::atom_integers *>(data);
    if (s == 0) {
      strncpy(message, "Error locating primitive atom indices", mlen);
      return;
    }
  }
  prim = s;
  std::ofstream output;
  if (DLV::open_file_write(output, filename.c_str(), message, mlen)) {
    write_header(output, op, structure, is_binary());
    write_rotation(output, structure, s, message, mlen);
    write_cmd_v6(output);
    output.close();
  }
}

DLV::data_object *CRYSTAL::mulliken_file::read_data(DLV::operation *op,
						    wavefn_calc *wfn,
						    const DLV::atom_integers *c,
						    const char filename[],
						    const DLV::string id,
						    char message[],
						    const int_g mlen)
{
  // Todo - pseudo-pots will get labelling wrong.
  DLV::atom_reals *data = 0;
  std::ifstream data_file;
  bool ok = DLV::open_file_read(data_file, filename, message, mlen);
  if (ok) {
    char buff[128];
    do {
      data_file.getline(buff, 128);
    } while (buff[0] == '#');
    int_g nspin;
    int_g natoms;
    sscanf(buff, "%d %d", &nspin, &natoms);
    real_g (*coords)[3] = new real_g[natoms][3];
    real_g *total = new_local_array1(real_g, natoms);
    // Todo - get maxshells/maxorbitals
    real_g **shells = new_local_array1(real_g *, natoms);
    int_g *nshells = new_local_array1(int_g, natoms);
    real_g **orbitals = new_local_array1(real_g *, natoms);
    int_g *norbs = new_local_array1(int_g, natoms);
    real_g *new_data = new_local_array1(real_g, natoms);
    real_g *shell_data = 0;
    real_g *orbital_data = 0;
    data = new DLV::atom_reals(program_name, id, op, "Mulliken population");
    int_g max_shells = 0;
    int_g max_orbitals = 0;
    int_g max_s = 0;
    int_g max_p = 0;
    int_g max_sp = 0;
    int_g max_d = 0;
    int_g max_f = 0;
    int_g max_g = 0;
    for (int_g i = 0; i < nspin; i++) {
      for (int_g j = 0; j < natoms; j++) {
	int_g k;
	// iat nshell x y z
	data_file >> k;
	data_file >> nshells[j];
	if (i == 0) {
	  shells[j] = new_local_array1(real_g, nshells[j]);
	  if (nshells[j] > max_shells)
	    max_shells = nshells[j];
	}
	data_file >> coords[j][0];
	data_file >> coords[j][1];
	data_file >> coords[j][2];
	for (k = 0; k < 3; k++)
	  coords[j][k] *= C09_bohr_to_angstrom;
	// total charge and shell charges
	data_file >> total[j];
	for (k = 0; k < nshells[j]; k++)
	  data_file >> shells[j][k];
	// norbitals and orbital charges
	data_file >> norbs[j];
	if (i == 0) {
	  orbitals[j] = new_local_array1(real_g, norbs[j]);
	  if (norbs[j] > max_orbitals)
	    max_orbitals = norbs[j];
	}
	for (k = 0; k < norbs[j]; k++)
	  data_file >> orbitals[j][k];
      }
      char message[32];
      if (i == 0) {
	if (wfn != 0) {
	  for (int_g j = 0; j < natoms; j++) {
	    int_g s = 0;
	    int_g p = 0;
	    int_g sp = 0;
	    int_g d = 0;
	    int_g f = 0;
	    int_g g = 0;
	    wfn->count_atom_shell_type(j, s, p, sp, d, f, g);
	    if (s > max_s)
	      max_s = s;
	    if (p > max_p)
	      max_p = p;
	    if (sp > max_sp)
	      max_sp = sp;
	    if (d > max_d)
	      max_d = d;
	    if (f > max_f)
	      max_f = f;
	    if (g > max_g)
	      max_g = g;
	  }
	  max_shells = max_s + max_p + max_sp + max_d + max_f;
	  max_orbitals = max_s + 3 * max_p + 4 * max_sp + 5 * max_d
	    + 7 * max_f + 9 * max_g;
	}
	shell_data = new_local_array1(real_g, max_shells * natoms);
	for (int_g j = 0; j < max_shells * natoms; j++)
	  shell_data[j] = 0.0;
	orbital_data = new_local_array1(real_g, max_orbitals * natoms);
	for (int_g j = 0; j < max_orbitals * natoms; j++)
	  orbital_data[j] = 0.0;
	if (c == 0)
	  data->set_grid(coords, natoms, false);
	else
	  data->set_grid(c);
	strcpy(message, "charge");
      } else
	strcpy(message, "spin");
      char text[128];
      snprintf(text, 128, "total %ss", message);
      data->add_data(total, text, true);
      // set up shells array (linearised isn't ideal)
      int_g k = 0;
      for (int_g l = 0; l < max_shells; l++) {
	int_g index = 0;
	if (l < max_s) {
	  for (int_g j = 0; j < natoms; j++) {
	    index = wfn->find_s_shell(j, l);
	    if (index != -1)
	      shell_data[k + j] = shells[j][index];
	  }
	} else if (l < max_s + max_p) {
	  for (int_g j = 0; j < natoms; j++) {
	    index = wfn->find_p_shell(j, l - max_s);
	    if (index != -1)
	      shell_data[k + j] = shells[j][index];
	  }
	} else if (l < max_s + max_p + max_sp) {
	  for (int_g j = 0; j < natoms; j++) {
	    index = wfn->find_sp_shell(j, l - max_s - max_p);
	    if (index != -1)
	      shell_data[k + j] = shells[j][index];
	  }
	} else if (l < max_s + max_p + max_sp + max_d) {
	  for (int_g j = 0; j < natoms; j++) {
	    index = wfn->find_d_shell(j, l - max_s - max_p - max_sp);
	    if (index != -1)
	      shell_data[k + j] = shells[j][index];
	  }
	} else if (l < max_s + max_p + max_sp + max_d + max_f) {
	  for (int_g j = 0; j < natoms; j++) {
	    index = wfn->find_f_shell(j, l - max_s - max_p - max_sp - max_d);
	    if (index != -1)
	      shell_data[k + j] = shells[j][index];
	  }
	} else if (l < max_s + max_p + max_sp + max_d + max_f + max_g) {
	  for (int_g j = 0; j < natoms; j++) {
	    index = wfn->find_g_shell(j, l - max_s - max_p - max_sp - max_d - max_f);
	    if (index != -1)
	      shell_data[k + j] = shells[j][index];
	  }
	} else { // Todo - is this safe (g shells?)
	  for (int_g j = 0; j < natoms; j++)
	    shell_data[k + j] = shells[j][l];
	}
	k += natoms;
      }
      // save shells
      k = 0;
      for (int_g j = 0; j < max_shells; j++) {
	if (k < max_s)
	  snprintf(text, 128, " %1ds %s", k + 1, message);
	else if (k < max_s + max_p)
	  snprintf(text, 128, " %1dp %s", k - max_s + 2, message);
	else if (k < max_s + max_p + max_sp)
	  snprintf(text, 128, "%1dsp %s", k - max_s - max_p + 2, message);
	else if (k < max_s + max_p + max_sp + max_d)
	  snprintf(text, 128, " %1dd %s", k - max_s - max_p - max_sp + 3,
		   message);
	else if (k < max_s + max_p + max_sp + max_d + max_f)
	  snprintf(text, 128, " %1df %s",
		   k - max_s - max_p - max_sp - max_d + 4, message);
	else if (k < max_s + max_p + max_sp + max_d + max_f + max_g)
	  snprintf(text, 128, " %1dg %s",
		   k - max_s - max_p - max_sp - max_d + max_f + 5, message);
	else
	  snprintf(text, 128, " %1d shell %s",
		   k - max_s - max_p - max_sp - max_d - max_f, message);
	data->add_data(shell_data + j * natoms, text, true);
	k++;
      }
      // set up orbitals array (linearised isn't ideal)
      k = 0;
      for (int_g l = 0; l < max_shells; l++) {
	int_g index = 0;
	if (l < max_s) {
	  for (int_g j = 0; j < natoms; j++) {
	    index = wfn->find_s_orbital(j, l);
	    if (index != -1)
	      orbital_data[k + j] = orbitals[j][index];
	  }
	  k += natoms;
	} else if (l < max_s + max_p) {
	  for (int_g j = 0; j < natoms; j++) {
	    index = wfn->find_p_orbital(j, l - max_s);
	    if (index != -1) {
	      orbital_data[k + j] = orbitals[j][index];
	      orbital_data[k + natoms + j] = orbitals[j][index + 1];
	      orbital_data[k + 2 * natoms + j] = orbitals[j][index + 2];
	    }
	  }
	  k += 3 * natoms;
	} else if (l < max_s + max_p + max_sp) {
	  for (int_g j = 0; j < natoms; j++) {
	    index = wfn->find_sp_orbital(j, l - max_s - max_p);
	    if (index != -1) {
	      orbital_data[k + j] = orbitals[j][index];
	      orbital_data[k + natoms + j] = orbitals[j][index + 1];
	      orbital_data[k + 2 * natoms + j] = orbitals[j][index + 2];
	      orbital_data[k + 3 * natoms + j] = orbitals[j][index + 3];
	    }
	  }
	  k += 4 * natoms;
	} else if (l < max_s + max_p + max_sp + max_d) {
	  for (int_g j = 0; j < natoms; j++) {
	    index = wfn->find_d_orbital(j, l - max_s - max_p - max_sp);
	    if (index != -1) {
	      orbital_data[k + j] = orbitals[j][index];
	      orbital_data[k + natoms + j] = orbitals[j][index + 1];
	      orbital_data[k + 2 * natoms + j] = orbitals[j][index + 2];
	      orbital_data[k + 3 * natoms + j] = orbitals[j][index + 3];
	      orbital_data[k + 4 * natoms + j] = orbitals[j][index + 4];
	    }
	  }
	  k += 5 * natoms;
	} else if (l < max_s + max_p + max_sp + max_d + max_f) {
	  for (int_g j = 0; j < natoms; j++) {
	    index = wfn->find_f_orbital(j, l - max_s - max_p - max_sp - max_d);
	    if (index != -1) {
	      orbital_data[k + j] = orbitals[j][index];
	      orbital_data[k + natoms + j] = orbitals[j][index + 1];
	      orbital_data[k + 2 * natoms + j] = orbitals[j][index + 2];
	      orbital_data[k + 3 * natoms + j] = orbitals[j][index + 3];
	      orbital_data[k + 4 * natoms + j] = orbitals[j][index + 4];
	      orbital_data[k + 5 * natoms + j] = orbitals[j][index + 5];
	      orbital_data[k + 6 * natoms + j] = orbitals[j][index + 6];
	    }
	  }
	  k += 7 * natoms;
	} else if (l < max_s + max_p + max_sp + max_d + max_f + max_g) {
	  for (int_g j = 0; j < natoms; j++) {
	    index = wfn->find_g_orbital(j, l - max_s - max_p - max_sp - max_d - max_f);
	    if (index != -1) {
	      orbital_data[k + j] = orbitals[j][index];
	      orbital_data[k + natoms + j] = orbitals[j][index + 1];
	      orbital_data[k + 2 * natoms + j] = orbitals[j][index + 2];
	      orbital_data[k + 3 * natoms + j] = orbitals[j][index + 3];
	      orbital_data[k + 4 * natoms + j] = orbitals[j][index + 4];
	      orbital_data[k + 5 * natoms + j] = orbitals[j][index + 5];
	      orbital_data[k + 6 * natoms + j] = orbitals[j][index + 6];
	      orbital_data[k + 7 * natoms + j] = orbitals[j][index + 7];
	      orbital_data[k + 8 * natoms + j] = orbitals[j][index + 8];
	    }
	  }
	  k += 9 * natoms;
	} else { // Todo - is this safe (g shells?)
	  for (int_g j = 0; j < natoms; j++)
	    orbital_data[k + j] = orbitals[j][l];
	  k += natoms;
	}
      }
      // save orbitals
      k = 0;
      int_g l = 0;
      for (int_g j = 0; j < max_shells; j++) {
	if (k < max_s) {
	  snprintf(text, 128, "%1ds    %s", k + 1, message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	} else if (k < max_s + max_p) {
	  snprintf(text, 128, "%1dpx   %s", k - max_s + 2, message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	  snprintf(text, 128, "%1dpy   %s", k - max_s + 2, message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	  snprintf(text, 128, "%1dpz   %s", k - max_s + 2, message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	} else if (k < max_s + max_p + max_sp) {
	  snprintf(text, 128, "%1ds    %s", k - max_s - max_p + 2, message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	  snprintf(text, 128, "%1dpx   %s", k - max_s - max_p + 2, message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	  snprintf(text, 128, "%1dpy   %s", k - max_s - max_p + 2, message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	  snprintf(text, 128, "%1dpz   %s", k - max_s - max_p + 2, message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	} else if (k < max_s + max_p + max_sp + max_d) {
	  snprintf(text, 128, "%1ddz2  %s", k - max_s - max_p - max_sp + 3,
		   message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	  snprintf(text, 128, "%1ddxz  %s", k - max_s - max_p - max_sp + 3,
		   message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	  snprintf(text, 128, "%1ddyz  %s", k - max_s - max_p - max_sp + 3,
		   message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	  snprintf(text, 128, "%1ddx2  %s", k - max_s - max_p - max_sp + 3,
		   message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	  snprintf(text, 128, "%1ddxy  %s", k - max_s - max_p - max_sp + 3,
		   message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	} else if (k < max_s + max_p + max_sp + max_d + max_f) {
	  snprintf(text, 128, " %1dfz2z %s",
		   k - max_s - max_p - max_sp - max_d + 4, message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	  snprintf(text, 128, " %1dfz2x %s",
		   k - max_s - max_p - max_sp - max_d + 4, message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	  snprintf(text, 128, " %1dfz2y %s",
		   k - max_s - max_p - max_sp - max_d + 4, message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	  snprintf(text, 128, " %1dfx2z %s",
		   k - max_s - max_p - max_sp - max_d + 4, message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	  snprintf(text, 128, " %1dfxyz %s",
		   k - max_s - max_p - max_sp - max_d + 4, message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	  snprintf(text, 128, " %1dfx2y %s",
		   k - max_s - max_p - max_sp - max_d + 4, message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	  snprintf(text, 128, " %1dfy2y %s",
		   k - max_s - max_p - max_sp - max_d + 4, message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	} else if (k < max_s + max_p + max_sp + max_d + max_f + max_g) {
	  snprintf(text, 128, " %1dgx4   %s",
		   k - max_s - max_p - max_sp - max_d - max_f + 5, message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	  snprintf(text, 128, " %1dgz2xz %s",
		   k - max_s - max_p - max_sp - max_d - max_f + 5, message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	  snprintf(text, 128, " %1dgz2yz %s",
		   k - max_s - max_p - max_sp - max_d - max_f + 5, message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	  snprintf(text, 128, " %1dgx2z2 %s",
		   k - max_s - max_p - max_sp - max_d - max_f + 5, message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	  snprintf(text, 128, " %1dgz2xy %s",
		   k - max_s - max_p - max_sp - max_d - max_f + 5, message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	  snprintf(text, 128, " %1dgx2xz %s",
		   k - max_s - max_p - max_sp - max_d - max_f + 5, message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	  snprintf(text, 128, " %1dgx2yz %s",
		   k - max_s - max_p - max_sp - max_d - max_f + 5, message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	  snprintf(text, 128, " %1dgx4   %s",
		   k - max_s - max_p - max_sp - max_d - max_f + 5, message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	  snprintf(text, 128, " %1dgx2xy %s",
		   k - max_s - max_p - max_sp - max_d - max_f + 5, message);
	  data->add_data(orbital_data + l * natoms, text, true);
	  l++;
	}
	k++;
      }
      for (; l < max_orbitals; l++) {
	snprintf(text, 128, "%1d orbital %s",
		 k - max_s - max_p - max_sp - max_d - max_f - max_g, message);
	data->add_data(orbital_data + l * natoms, text, true);
      }
    }
    for (int_g i = 0; i < natoms; i++) {
      delete_local_array(orbitals[i]);
      delete_local_array(shells[i]);
    }
    delete_local_array(orbital_data);
    delete_local_array(shell_data);
    delete_local_array(new_data);
    delete_local_array(norbs);
    delete_local_array(orbitals);
    delete_local_array(nshells);
    delete_local_array(shells);
    delete_local_array(total);
    if (c != 0)
      delete [] coords;
    data_file.close();
  }
  return data;
}

bool CRYSTAL::mulliken_calc_v5::recover(const bool no_err, const bool log_ok,
					char message[], const int_g len)
{
  DLV::string id = get_job_id();
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi())
      read_logfile(name, id);
    DLV::data_object *file = 0;
    if (no_err)
      file = new DLV::text_file(name, program_name, id,
				"Mulliken output file");
    else
      file = new DLV::text_file(name, program_name, id,
				"Mulliken(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    DLV::operation *calc = find_parent(wavefn_label, program_name);
    if (calc == 0) {
      strncpy(message, "Failed to find wavefn op", len);
      return false;
    } else {
      wavefn_calc *wfn = dynamic_cast<CRYSTAL::wavefn_calc *>(calc);
      if (wfn == 0) {
	strncpy(message, "Failed to find wavefn", len);
	return false;
      } else {
	DLV::data_object *data = read_data(this, wfn, 0,
					   get_outfile_name(0).c_str(), id,
					   message, len);
	if (data == 0)
	  return false;
	else {
	  attach_data(data);
	}
      }
    }
  }
  return true;
}

bool CRYSTAL::mulliken_calc_v6::recover(const bool no_err, const bool log_ok,
					char message[], const int_g len)
{
  DLV::string id = get_job_id();
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi())
      read_logfile(name, id);
    DLV::data_object *file = 0;
    if (no_err)
      file = new DLV::text_file(name, program_name, id,
				"Mulliken output file");
    else
      file = new DLV::text_file(name, program_name, id,
				"Mulliken(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    DLV::operation *calc = find_parent(prim_atom_label, program_name);
    if (calc == 0) {
      strncpy(message, "Failed to find wavefn op", len);
      return false;
    } else {
      wavefn_calc *wfn = dynamic_cast<CRYSTAL::wavefn_calc *>(calc);
      if (wfn == 0) {
	strncpy(message, "Failed to find wavefn", len);
	return false;
      } else {
	DLV::data_object *data = read_data(this, wfn, 0,
					   get_outfile_name(0).c_str(), id,
					   message, len);
	if (data == 0)
	  return false;
	else {
	  attach_data(data);
	}
      }
    }
  }
  return true;
}

bool CRYSTAL::mulliken_calc_v7::recover(const bool no_err, const bool log_ok,
					char message[], const int_g len)
{
  DLV::string id = get_job_id();
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi())
      read_logfile(name, id);
    DLV::data_object *file = 0;
    if (no_err) {
      if (has_rotate())
	file = new DLV::text_file(name, program_name, id,
				  "Rotated Mulliken output file");
      else
	file = new DLV::text_file(name, program_name, id,
				  "Mulliken output file");
    } else
      file = new DLV::text_file(name, program_name, id,
				"Mulliken(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    DLV::operation *calc = find_parent(prim_atom_label, program_name);
    if (calc == 0) {
      strncpy(message, "Failed to find wavefn op", len);
      return false;
    } else {
      wavefn_calc *wfn = dynamic_cast<CRYSTAL::wavefn_calc *>(calc);
      if (wfn == 0) {
	strncpy(message, "Failed to find wavefn", len);
	return false;
      } else {
	const DLV::atom_integers *c = 0;
	if (has_rotate())
	  c = prim;
	DLV::data_object *data = read_data(this, wfn, c,
					   get_outfile_name(0).c_str(), id,
					   message, len);
	if (data == 0)
	  return false;
	else {
	  attach_data(data);
	}
      }
    }
  }
  return true;
}

bool CRYSTAL::mulliken_calc::reload_data(DLV::data_object *data,
				  char message[], const int_g mlen)
{
  //strncpy(message, "Todo - CRYSTAL mulliken calc reload not done", mlen);
  return true;
}

bool CRYSTAL::load_mulliken_data::reload_data(DLV::data_object *data,
					      char message[], const int_g mlen)
{
  //strncpy(message, "Todo - CRYSTAL mulliken load reload not done", mlen);
  return true;
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    CRYSTAL::load_mulliken_data *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::load_mulliken_data("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::mulliken_calc_v5 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::mulliken_calc_v5(0, d, n);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::mulliken_calc_v6 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::mulliken_calc_v6(0, d, n);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::mulliken_calc_v7 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::mulliken_calc_v7(0, false, d, n);
    }

  }
}

template <class Archive>
void CRYSTAL::mulliken_data::serialize(Archive &ar, const unsigned int version)
{
  ar & neighbours;
}

template <class Archive>
void CRYSTAL::mulliken_data_v5::serialize(Archive &ar,
					  const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::mulliken_data>(*this);
  ar & boost::serialization::base_object<CRYSTAL::property_data>(*this);
}

template <class Archive>
void CRYSTAL::mulliken_data_v6::serialize(Archive &ar,
					  const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::mulliken_data>(*this);
  ar & boost::serialization::base_object<CRYSTAL::property_data_v6>(*this);
}

template <class Archive>
void CRYSTAL::mulliken_data_v7::serialize(Archive &ar,
					  const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::mulliken_data>(*this);
  ar & boost::serialization::base_object<CRYSTAL::property_data_v6>(*this);
  ar & rotate;
}

template <class Archive>
void CRYSTAL::load_mulliken_data::serialize(Archive &ar,
					    const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::load_data_op>(*this);
}

template <class Archive>
void CRYSTAL::mulliken_calc::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::property_calc>(*this);
}

template <class Archive>
void CRYSTAL::mulliken_calc_v5::serialize(Archive &ar,
					  const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::mulliken_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::mulliken_data_v5>(*this);
}

template <class Archive>
void CRYSTAL::mulliken_calc_v6::serialize(Archive &ar,
					  const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::mulliken_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::mulliken_data_v6>(*this);
}

template <class Archive>
void CRYSTAL::mulliken_calc_v7::serialize(Archive &ar,
					  const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::mulliken_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::mulliken_data_v7>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::load_mulliken_data)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::mulliken_calc_v5)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::mulliken_calc_v6)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::mulliken_calc_v7)

DLV_SUPPRESS_TEMPLATES(DLV::load_data_op)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::property_data)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::property_data_v6)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::property_calc)

DLV_NORMAL_EXPLICIT(CRYSTAL::load_mulliken_data)
DLV_NORMAL_EXPLICIT(CRYSTAL::mulliken_calc_v5)
DLV_NORMAL_EXPLICIT(CRYSTAL::mulliken_calc_v6)
DLV_NORMAL_EXPLICIT(CRYSTAL::mulliken_calc_v7)

#endif // DLV_USES_SERIALIZE
