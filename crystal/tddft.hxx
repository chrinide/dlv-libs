
#ifndef CRYSTAL_TDDFT
#define CRYSTAL_TDDFT

namespace CRYSTAL {

  class tddft_plot_file {
  protected:
    class DLV::dos_plot *read_data(DLV::operation *op, DLV::dos_plot *plot,
				   const char filename[], const DLV::string id,
				   char message[], const int_g mlen);
  };

  class tddft_data : public tddft_plot_file {
  public:
    tddft_data();
    void set_params(const TDDFT &td);
    void write_tddft(std::ofstream &output) const;

  protected:
    bool do_scf() const;
    bool do_jdos() const;
    bool do_pes() const;

  private:
    TDDFT tddft;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class tddft_data_v8 : public scf_data_v8, public tddft_data {
  protected:
    void write_geom_commands(std::ofstream &output,
			     const DLV::model *const structure) const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_tddft_plot : public DLV::load_data_op, public tddft_plot_file {
  public:
    static DLV::operation *create(const char filename[], const int_g version,
				  char message[], const int_g mlen);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

    // public for serialization
    load_tddft_plot(const char file[]);

  protected:
    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class tddft_calc : public scf_calc { 
  public:
    tddft_calc();

  protected:
    DLV::string get_name() const;
    bool create_files(const bool is_parallel, const bool is_local,
		      char message[], const int_g mlen);

    virtual bool has_scf() const = 0;
    virtual bool has_jdos() const = 0;
    virtual bool has_pes() const = 0;
    void set_jdos(class DLV::dos_plot *d);
    void set_oas(class DLV::dos_plot *d);
    void set_pes(class DLV::dos_plot *d);
    class DLV::dos_plot *get_jdos();
    class DLV::dos_plot *get_oas();
    class DLV::dos_plot *get_pes();

  private:
    class DLV::dos_plot *jdos;
    class DLV::dos_plot *oas;
    class DLV::dos_plot *pes;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class tddft_calc_v8 : public tddft_calc, public tddft_data_v8 {
  public:
    tddft_calc_v8();

  protected:
    void set_params(const Hamiltonian &h, const Basis &b, const Kpoints &k,
		    const Tolerances &tol, const Convergence &c,
		    const Print &p, const Joboptions &j, const Optimise &opt,
		    const Phonon &ph, const Neb &n, const CPHF &cp,
		    const TDDFT &tddft, const bool a);
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void add_restart_file(const DLV::string tag, const bool is_local);
    void add_opt_restart_file(const DLV::string tag, const bool is_local);
    void add_optimiser_log(const DLV::string tag, const bool is_local);
    bool add_basis_set(const char filename[], const bool set_default,
		       int_g * &indices, int_g &value,
		       char message[], const int_g mlen);

    bool has_scf() const;
    bool has_jdos() const;
    bool has_pes() const;

    bool write(const DLV::string filename, const DLV::model *const structure,
	       char message[], const int_g mlen);
    void write_structure(const DLV::string filename,
			 const DLV::model *const structure,
			 char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(CRYSTAL::load_tddft_plot)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::tddft_calc)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::tddft_calc_v8)
#endif // DLV_USES_SERIALIZE

inline CRYSTAL::tddft_data::tddft_data()
  : tddft(true, false, true, true, true, 0, 1, 100, 100, 1e-6)
{
}

inline bool CRYSTAL::tddft_data::do_scf() const
{
  return tddft.scf_calc;
}

inline bool CRYSTAL::tddft_data::do_jdos() const
{
  return tddft.calc_jdos;
}

inline bool CRYSTAL::tddft_data::do_pes() const
{
  return tddft.calc_pes;
}

inline CRYSTAL::load_tddft_plot::load_tddft_plot(const char file[])
  : load_data_op(file)
{
}

inline CRYSTAL::tddft_calc::tddft_calc()
  : jdos(0), oas(0), pes(0)
{
}

inline CRYSTAL::tddft_calc_v8::tddft_calc_v8()
  : tddft_data_v8()
{
}

inline void CRYSTAL::tddft_calc::set_jdos(class DLV::dos_plot *d)
{
  jdos = d;
}

inline void CRYSTAL::tddft_calc::set_oas(class DLV::dos_plot *d)
{
  jdos = d;
}

inline void CRYSTAL::tddft_calc::set_pes(class DLV::dos_plot *d)
{
  jdos = d;
}

inline class DLV::dos_plot *CRYSTAL::tddft_calc::get_jdos()
{
  return jdos;
}

inline class DLV::dos_plot *CRYSTAL::tddft_calc::get_oas()
{
  return oas;
}

inline class DLV::dos_plot *CRYSTAL::tddft_calc::get_pes()
{
  return pes;
}

#endif // CRYSTAL_TDDFT
