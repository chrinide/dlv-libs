
#ifndef CRYSTAL_BLOCH_FUNCTIONS
#define CRYSTAL_BLOCH_FUNCTIONS

namespace CRYSTAL {

  class bloch_file : public grid3D_v6 {
  protected:
    bloch_file();
    DLV::volume_data *create_volume(const char code[], const DLV::string id,
				    DLV::operation *op, const int_g nx,
				    const int_g ny, const int_g nz,
				    const real_g origin[3],
				    const real_g astep[3],
				    const real_g bstep[3],
				    const real_g cstep[3]) const;
    bool get_data(std::ifstream &input, DLV::volume_data *data,
		  DLV::operation *op, const int_g nx, const int_g ny,
		  const int_g nz, const char label[], char message[],
		  const int_g mlen);

    virtual void attach_volume(DLV::volume_data *data) = 0;

  private:
    int_g nkpoints;
    int_g size;
    DLV::volume_data **data_items;

    void get_label(const char line[], char buff[], int_g &k1, int_g &k2,
		   int_g &k3, int_g &i1, int_g &i2, int_g &i3,
		   const bool periodic, bool &spin) const;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class bloch_data : public property_data_v6 {
  protected:
    bloch_data(const int_g np, DLV::box *v, const int_g minb,
	       const int_g maxb, const int_g nkp, const int_g *kp,
	       const Density_Matrix &dm, const NewK &nk);

  protected:
    void write_input(const DLV::string filename, const DLV::operation *op,
		     const DLV::model *const structure, const bool is_bin,
		     char message[], const int_g mlen);
    void write_grid(std::ofstream &output,
		    const DLV::model *const structure) const;
    void write_command(std::ofstream &output) const;
    void write_data_sets(std::ofstream &output) const;

  private:
    int_g npoints;
    DLV::box *grid;
    int_g min_band;
    int_g max_band;
    int_g nkpoints;
    int_g *kpoints;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class bloch_data_v6 : public bloch_data, public bloch_file {
  protected:
    bloch_data_v6(const int_g np, DLV::box *v, const int_g minb,
		  const int_g maxb, const int_g nkp, const int_g *kp,
		  const Density_Matrix &dm, const NewK &nk);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_bloch_data : public DLV::load_data_op {
  public:
    static operation *create(const char filename[], const int_g version,
			     char message[], const int_g mlen);

  protected:
    load_bloch_data(const char file[]);

    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_bloch_data_v6 : public load_bloch_data, public bloch_file {
  public:
    static operation *create(const char filename[],
                             char message[], const int_g mlen);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

    // public for serialization
    load_bloch_data_v6(const char file[]);

  protected:
    void attach_volume(DLV::volume_data *data);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class bloch_calc : public property_calc {
  public:
    static operation *create(const int_g np, const int_g grid,
			     const int_g minb, const int_g maxb,
			     const int_g nkp, const int_g *kp,
			     const Density_Matrix &dm, const NewK &nk,
			     const int_g version,
			     const DLV::job_setup_data &job,
			     const bool extern_job, const char extern_dir[],
			     char message[], const int_g mlen);

  protected:
    DLV::string get_name() const;

    bool create_files(const bool is_parallel, const bool is_local,
		      char message[], const int_g mlen);
    virtual void write(const DLV::string filename, const DLV::operation *op,
		       const DLV::model *const structure,
		       char message[], const int_g mlen) = 0;
    bool is_binary() const;

  private:
    bool binary_wvfn;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class bloch_calc_v6 : public bloch_calc, public bloch_data_v6 {
  public:
    bloch_calc_v6(const int_g np, DLV::box *v, const int_g minb,
		  const int_g maxb, const int_g nkp, const int_g *kp,
		  const Density_Matrix &dm, const NewK &nk);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void write(const DLV::string filename, const DLV::operation *op,
	       const DLV::model *const structure,
	       char message[], const int_g mlen);
    void attach_volume(DLV::volume_data *data);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(CRYSTAL::load_bloch_data_v6)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::bloch_calc_v6)
#endif // DLV_USES_SERIALIZE

inline CRYSTAL::bloch_file::bloch_file() : nkpoints(0), size(0), data_items(0)
{
}

inline CRYSTAL::bloch_data::bloch_data(const int_g np, DLV::box *v,
				       const int_g minb, const int_g maxb,
				       const int_g nkp, const int_g *kp,
				       const Density_Matrix &dm,
				       const NewK &nk)
  : property_data_v6(dm, nk), npoints(np), grid(v), min_band(minb),
    max_band(maxb), nkpoints(nkp), kpoints(new int_g[nkp])
{
  for (int_g i = 0; i < nkp; i++)
    kpoints[i] = kp[i];
}

inline CRYSTAL::bloch_data_v6::bloch_data_v6(const int_g np, DLV::box *v,
					     const int_g minb, const int_g maxb,
					     const int_g nkp, const int_g *kp,
					     const Density_Matrix &dm,
					     const NewK &nk)
  : bloch_data(np, v, minb, maxb, nkp, kp, dm, nk)
{
}

inline CRYSTAL::load_bloch_data::load_bloch_data(const char file[])
  : load_data_op(file)
{
}

inline CRYSTAL::load_bloch_data_v6::load_bloch_data_v6(const char file[])
  : load_bloch_data(file)
{
}

inline bool CRYSTAL::bloch_calc::is_binary() const
{
  return binary_wvfn;
}

inline CRYSTAL::bloch_calc_v6::bloch_calc_v6(const int_g np, DLV::box *v,
					     const int_g minb, const int_g maxb,
					     const int_g nkp, const int_g *kp,
					     const Density_Matrix &dm,
					     const NewK &nk)
  : bloch_data_v6(np, v, minb, maxb, nkp, kp, dm, nk)
{
}

#endif // CRYSTAL_BLOCH_FUNCTIONS
