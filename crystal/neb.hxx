
#ifndef CRYSTAL_NUDGED_ELASTIC_BAND
#define CRYSTAL_NUDGED_ELASTIC_BAND

namespace CRYSTAL {

//  class neb_file {
//  protected:
//      DLV::data_object *read_data(DLV::operation *op, const char filename[],
//				  const DLV::string id, char message[],
//				  const int_g mlen);
//  };

  class neb_data{  //: public neb_file{
  private:
    Neb data;
    DLV::operation *nebop1; //save operation instead???
    DLV::operation *nebop2;
    static DLV::operation *neb_editing;
    static const int_g frames_per_image = 10;
    bool *updated_images;
    DLV::operation **images;
  protected:
    void set_params(const Neb &p);
    Neb get_data() const;
    bool get_restart() const;
    DLV::string get_restart_file() const;
    DLV::string get_images_dir() const;
    int_g get_end_model() const;
    int_g get_natoms() const;
    int_g get_nimages() const;
    bool get_nocalc() const;
    void set_mapping(int_g maparray[], const int_g natoms);
    void get_mapping(int_g maparray[]) const;
    void set_nebop1(DLV::operation *op1);
    DLV::operation *get_nebop1() const;
    void set_nebop2(DLV::operation *op2);
    DLV::operation *get_nebop2() const;
    DLV::string optimiser_log() const;
    void set_images_arrays(const int_g n);
    void set_images(const int_g image, DLV::operation *op);
    void set_image_updated(const int_g image);
    const int_g get_frames_per_image();
  public:
    static const int_g max_frames = 5000;
    static DLV::operation *get_neb_editing();
    static void set_neb_editing(DLV::operation *op);
    DLV::operation *get_image_op(const int_g image);
    bool image_updated(const int_g image);
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class neb_data_v8 : public scf_data_v8, public neb_data {
  protected:
    void write_geom_commands(std::ofstream &output,
			     const DLV::model *const structure) const;
    void write_extern_label(std::ofstream &output) const;
    void write_section5(std::ofstream &output) const;
    virtual void write_neb(std::ofstream &output) const;
    DLV::string restart_opt_filename() const;
  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class neb_data_ini1 : public neb_data_v8{ //Creates images
  protected:
    void write_neb(std::ofstream &output) const;
  };
  class neb_data_ini2 : public neb_data_v8{ //Reads str files and writes fort.91
  protected:
    void write_neb(std::ofstream &output) const;
  };

  class neb_calc : public scf_geom {
  public:
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);
    static void reset_editing();  
    static void view_mapping_str1(const int_g dummy);
    static void view_mapping_str2( const int_g natoms, const int_g end_model, 
				   const int_g mapping[], const int_g dummy);
    virtual void view_mapping_str1() = 0;
    virtual void view_mapping_str2(const int_g natoms, const int_g end_model, 
				   const int_g mapping[]) = 0;
    static int_g map_neb_structures(int_g mapping[], const int_g natoms, 
				   const int_g end_model, const int_g dummy);
    virtual int_g map_neb_structures(int_g mapping[], const int_g natoms,
				    const int_g end_model) = 0;
    static DLV::operation *get_image_op(const int_g image, 
					const char images_dir[], 
					const bool bond_all, const int_g dummy);
    virtual DLV::operation *get_image_op(const int_g image, 
					 const char images_dir[],
					 const bool bond_all) = 0;
    static void set_images_arrays(const int_g n, const int_g dummy);
    virtual void set_images_arrays(const int_g n) = 0;
    static void save_image_str(const int_g image, DLV::operation *op,
			       const int_g dummy);
    virtual void save_image_str(const int_g image, DLV::operation *op) = 0;
    static DLV::operation *create_run1(const bool a, const Hamiltonian &h,
				       const Basis &b, const Kpoints &k,
				       const Tolerances &tol,
				       const Convergence &c, const Print &p,
				       const Joboptions &j,
				       const Optimise &opt, const Phonon &ph, 
				       const Neb &n,
				       const DLV::job_setup_data &job,
				       const bool extern_job,
				       const char extern_dir[],
				       DLV::operation *nebop2,
				       char dir_name[], char message[], 
				       const nat_g len);
    static DLV::operation *create_run2(const bool a, const Hamiltonian &h,
				       const Basis &b, const Kpoints &k,
				       const Tolerances &tol,
				       const Convergence &c,
				       const Print &p, const Joboptions &j,
				       const Optimise &opt, const Phonon &ph, 
				       const Neb &n,
				       const DLV::job_setup_data &job,
				       const bool extern_job,
				       const char extern_dir[],
				       DLV::operation *nebop2,
				       char dir_name[], char message[], 
				       const nat_g len);
    virtual DLV::operation *create_ini1(const bool a, const Hamiltonian &h,
					const Basis &b, const Kpoints &k,
					const Tolerances &tol,
					const Convergence &c,
					const Print &p, const Joboptions &j,
					const Optimise &opt, const Phonon &ph, 
					const Neb &n,
					const DLV::job_setup_data &job,
					const bool extern_job,
					const char extern_dir[],
					DLV::operation *nebop2,
					char dir_name[], char message[], 
					const nat_g len) = 0;
    virtual DLV::operation *create_ini2(const bool a, const Hamiltonian &h,
					const Basis &b, const Kpoints &k,
					const Tolerances &tol,
					const Convergence &c,
					const Print &p, const Joboptions &j,
					const Optimise &opt, const Phonon &ph, 
					const Neb &n,
					const DLV::job_setup_data &job,
					const bool extern_job,
					const char extern_dir[],
					DLV::operation *nebop2,
					char dir_name[], char message[], 
					const nat_g len) = 0;
    static void hide_atoms(const int_g end_model, const real_g dist, 
			   const int_g mapping[]);
    static void unhide_atoms(const int_g end_model);
    static void label_image(DLV::operation *op);
    static bool edit_atoms(const int_g end_model, int_g mapping[], 
			   char message[], const int_g mlen, const int_g dummy);
    virtual bool edit_atoms(const int_g end_model, int_g mapping[],
			    char message[], const int_g mlen) = 0;
    static DLV::string get_output_file_name(const int_g index);
  protected:
    DLV::string get_name() const;
    virtual bool create_files(const bool is_parallel, const bool is_local,
		      char message[], const int_g mlen) = 0;
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);
    virtual void add_optimiser_log(const DLV::string tag,
				   const bool is_local) = 0;
    void add_standard_data_objects();
  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class neb_calc_v8 : public neb_calc{
  protected:
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    bool edit_atoms(const int_g end_model, int_g mapping[], 
		    char message[], const int_g mlen);
    DLV::atom_integers *get_labels2();
    void set_labels2(DLV::operation *op2);
    DLV::operation *create_ini1(const bool a, const Hamiltonian &h,
				const Basis &b, const Kpoints &k,
				const Tolerances &tol, const Convergence &c,
				const Print &p, const Joboptions &j,
				const Optimise &opt, const Phonon &ph,
				const Neb &n, const DLV::job_setup_data &job,
				const bool extern_job, const char extern_dir[],
				DLV::operation *nebop2, char dir_name[],
				char message[], const nat_g len);
    DLV::operation *create_ini2(const bool a, const Hamiltonian &h,
				const Basis &b, const Kpoints &k,
				const Tolerances &tol, const Convergence &c,
				const Print &p, const Joboptions &j,
				const Optimise &opt, const Phonon &ph,
				const Neb &n, const DLV::job_setup_data &job,
				const bool extern_job, const char extern_dir[],
				DLV::operation *nebop2, char dir_name[],
				char message[], const nat_g len);
   virtual void set_nebop2(DLV::operation *op2) = 0;
   virtual void copy_images_from_master() = 0;
  private:
    DLV::atom_integers *labels2;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };


    class neb_calc_v8_final : public neb_calc_v8, public neb_data_v8{ 
    public:
      neb_calc_v8_final();
      bool reload_data(DLV::data_object *data, char message[],
		       const int_g mlen);      
      static DLV::operation *get_nebop2();
      static DLV::operation *get_neb_image_op(const int_g n);
    protected:
      bool get_NEB_plot_data(real_g dists[], real_g energies[], 
			     const int_g npts, DLV::string title,
			     DLV::string xlabel, DLV::string ylabel, 
			     char message[], const int_g mlen);
      bool get_NEB_trajectory(real_g p[][max_frames][3], 
			      DLV::coord_type coords_ts[][3],
			      const int_g ts_image, const int_g natoms, 
			      char message[], const int_g mlen);
      void set_params(const Hamiltonian &h, const Basis &b, const Kpoints &k,
		      const Tolerances &tol, const Convergence &c,
		      const Print &p, const Joboptions &j, const Optimise &opt,
		      const Phonon &ph, const Neb &n, const CPHF &cp,
		      const TDDFT &tddft, const bool a);  //unhappy
      bool add_basis_set(const char filename[], const bool set_default,
			 int_g * &indices, int_g &value,
			 char message[], const int_g mlen);
      void add_restart_file(const DLV::string tag, const bool is_local);
      void add_optimiser_log(const DLV::string tag, const bool is_local);
      bool create_files(const bool is_parallel, const bool is_local,
			char message[], const int_g mlen);
      bool write(const DLV::string filename, const DLV::model *const structure,
		 char message[], const int_g mlen);
      void write_structure(const DLV::string filename,
			   const DLV::model *const structure,
			   char message[], const int_g mlen);
      void write_2nd_structure(char message[], const int_g mlen);
      void add_opt_restart_file(const DLV::string tag, const bool is_local);
      bool recover(const bool no_err, const bool log_ok,
		   char message[], const int_g len);
      void view_mapping_str1();
      void view_mapping_str2(const int_g natoms, const int_g end_model, 
			     const int_g mapping[]);
      int_g map_neb_structures(int_g mapping[], const int_g natoms,
			      const int_g end_model);
      void set_images_arrays(const int_g n);
      DLV::operation *get_image_op(const int_g image, const char images_dir[],
				   const bool bond_all);
      void save_image_str(const int_g image, DLV::operation *op);
      void set_nebop2(DLV::operation *op2);
      void copy_images_from_master();
      int_g get_transition_image(char message[], const int_g len);
    private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE

    };


    class neb_calc_v8_ini1 : public neb_calc_v8, public neb_data_ini1 {
      //Creates images
    public:
	neb_calc_v8_ini1();
    protected:
      void set_params(const Hamiltonian &h, const Basis &b, const Kpoints &k,
		      const Tolerances &tol, const Convergence &c,
		      const Print &p, const Joboptions &j, const Optimise &opt,
		      const Phonon &ph, const Neb &n, const CPHF &cp,
		      const TDDFT &tddft, const bool a);  //unhappy
      bool add_basis_set(const char filename[], const bool set_default,
			 int_g * &indices, int_g &value,
			 char message[], const int_g mlen);
      void add_restart_file(const DLV::string tag, const bool is_local);
      void add_optimiser_log(const DLV::string tag, const bool is_local);
      bool create_files(const bool is_parallel, const bool is_local,
			char message[], const int_g mlen);
      bool write(const DLV::string filename, const DLV::model *const structure,
		 char message[], const int_g mlen);
      void write_structure(const DLV::string filename,
			   const DLV::model *const structure,
			   char message[], const int_g mlen);
      void write_2nd_structure(char message[], const int_g mlen);
      void add_opt_restart_file(const DLV::string tag, const bool is_local);
      bool recover(const bool no_err, const bool log_ok,
		   char message[], const int_g len);
      void view_mapping_str1();
      void view_mapping_str2(const int_g natoms, const int_g end_model, 
			     const int_g mapping[]);
      int_g map_neb_structures(int_g mapping[], const int_g natoms,
			      const int_g end_model);
      void set_images_arrays(const int_g n);
      DLV::operation *get_image_op(const int_g image, const char images_dir[],
				   const bool bond_all);
      void save_image_str(const int_g image, DLV::operation *op);
      void set_nebop2(DLV::operation *op2);
      void copy_images_from_master();
    private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
    };


    class neb_calc_v8_ini2 : public neb_calc_v8, public neb_data_ini2{ 
//Reads str files and writes fort.91
    public:
	neb_calc_v8_ini2();
    protected:
      void set_params(const Hamiltonian &h, const Basis &b, const Kpoints &k,
		      const Tolerances &tol, const Convergence &c,
		      const Print &p, const Joboptions &j, const Optimise &opt,
		      const Phonon &ph, const Neb &n, const CPHF &cp,
		      const TDDFT &tddft, const bool a);  //unhappy
      bool add_basis_set(const char filename[], const bool set_default,
			 int_g * &indices, int_g &value,
			 char message[], const int_g mlen);
      void add_restart_file(const DLV::string tag, const bool is_local);
      void add_optimiser_log(const DLV::string tag, const bool is_local);
      bool create_files(const bool is_parallel, const bool is_local,
			char message[], const int_g mlen);
      bool write(const DLV::string filename, const DLV::model *const structure,
		 char message[], const int_g mlen);
      void write_structure(const DLV::string filename,
			   const DLV::model *const structure,
			   char message[], const int_g mlen);
      void write_2nd_structure(char message[], const int_g mlen);
      void add_opt_restart_file(const DLV::string tag, const bool is_local);
      bool recover(const bool no_err, const bool log_ok, 
		   char message[], const int_g len);
      void view_mapping_str1();
      void view_mapping_str2(const int_g natoms,  const int_g end_model, 
			     const int_g mapping[]);
      int_g map_neb_structures(int_g mapping[], const int_g natoms,
			      const int_g end_model);
       void set_images_arrays(const int_g n);
      DLV::operation *get_image_op(const int_g image, const char images_dir[],
				   const bool bond_all);
      void save_image_str(const int_g image, DLV::operation *op);
      void set_nebop2(DLV::operation *op2);
      void copy_images_from_master();
    private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
    };
}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(CRYSTAL::neb_calc_v8_final)
#endif // DLV_USES_SERIALIZE

inline CRYSTAL::neb_calc_v8_final::neb_calc_v8_final()
  : neb_data_v8()
{
}

inline CRYSTAL::neb_calc_v8_ini1::neb_calc_v8_ini1()
  : neb_data_ini1()
{
}

inline CRYSTAL::neb_calc_v8_ini2::neb_calc_v8_ini2()
  : neb_data_ini2()
{
}

#endif // CRYSTAL_NUDGED_ELASTIC_BAND
