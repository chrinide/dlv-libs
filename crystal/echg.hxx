
#ifndef CRYSTAL_CHARGE_DENSITY_SLICE
#define CRYSTAL_CHARGE_DENSITY_SLICE

namespace CRYSTAL {

  class charge_d_data {
  public:
    // serialization
    virtual ~charge_d_data();

  protected:
    charge_d_data(const int_g np, DLV::plane *s);

    void write_input(std::ofstream &output);
    virtual void write_grid(std::ofstream &output) = 0;

    int_g get_npoints() const;
    const DLV::plane *get_vertices() const;

  private:
    int_g npoints;
    DLV::plane *grid;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class charge_d_data_v4 : public charge_d_data, public grid2D_v4,
			   public property_data {
  protected:
    charge_d_data_v4(const int_g np, DLV::plane *s,
		     const Density_Matrix &dm, const NewK &nk);
    void write_grid(std::ofstream &output);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class charge_d_data_v5 : public charge_d_data, public grid2D_v5,
			   public property_data {
  protected:
    charge_d_data_v5(const int_g np, DLV::plane *s,
		     const Density_Matrix &dm, const NewK &nk);
    void write_grid(std::ofstream &output);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class charge_d_data_v6 : public charge_d_data, public grid2D_v5,
			   public property_data_v6 {
  protected:
    charge_d_data_v6(const int_g np, DLV::plane *s,
		     const Density_Matrix &dm, const NewK &nk);
    void write_grid(std::ofstream &output);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_charge_d_slice : public DLV::load_data_op {
  public:
    static operation *create(const char filename[], const int_g version,
			     char message[], const int_g mlen);

  protected:
    load_charge_d_slice(const char file[]);

    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_charge_d_slice_v4 : public load_charge_d_slice, public grid2D_v4 {
  public:
    static operation *create(const char filename[],
			     char message[], const int_g mlen);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

    // public for serialization
    load_charge_d_slice_v4(const char file[]);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_charge_d_slice_v5 : public load_charge_d_slice, public grid2D_v5 {
  public:
    static operation *create(const char filename[],
			     char message[], const int_g mlen);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

    //public for serialization
    load_charge_d_slice_v5(const char file[]);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class charge_d_calc : public property_calc {
  public:
    static operation *create(const int_g np, const int_g selection,
			     const Density_Matrix &dm, const NewK &nk,
			     const int_g version,
			     const DLV::job_setup_data &job,
			     const bool extern_job, const char extern_dir[],
			     char message[], const int_g mlen);

  protected:
    DLV::string get_name() const;
    bool create_files(const bool is_parallel, const bool is_local,
		      char message[], const int_g mlen);
    virtual void write(const DLV::string filename, const DLV::operation *op,
		       const DLV::model *const structure,
		       char message[], const int_g mlen) = 0;
    bool is_binary() const;

  private:
    bool binary_wvfn;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class charge_d_calc_v4 : public charge_d_calc, public charge_d_data_v4 {
  public:
    charge_d_calc_v4(const int_g np, DLV::plane *s,
		     const Density_Matrix &dm, const NewK &nk);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    void write(const DLV::string filename, const DLV::operation *op,
	       const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class charge_d_calc_v5 : public charge_d_calc, public charge_d_data_v5 {
  public:
    charge_d_calc_v5(const int_g np, DLV::plane *s,
		     const Density_Matrix &dm, const NewK &nk);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    void write(const DLV::string filename, const DLV::operation *op,
	       const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class charge_d_calc_v6 : public charge_d_calc, public charge_d_data_v6 {
  public:
    charge_d_calc_v6(const int_g np, DLV::plane *s,
		     const Density_Matrix &dm, const NewK &nk);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void write(const DLV::string filename, const DLV::operation *op,
	       const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(CRYSTAL::load_charge_d_slice_v4)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::load_charge_d_slice_v5)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::charge_d_calc_v4)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::charge_d_calc_v5)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::charge_d_calc_v6)
#endif // DLV_USES_SERIALIZE

inline CRYSTAL::charge_d_data::charge_d_data(const int_g np, DLV::plane *s)
  : npoints(np), grid(s)
{
}

inline CRYSTAL::int_g CRYSTAL::charge_d_data::get_npoints() const
{
  return npoints;
}

inline const DLV::plane *CRYSTAL::charge_d_data::get_vertices() const
{
  return grid;
}

inline CRYSTAL::charge_d_data_v4::charge_d_data_v4(const int_g np,
						   DLV::plane *s,
						   const Density_Matrix &dm,
						   const NewK &nk)
  : charge_d_data(np, s), property_data(dm, nk)
{
}

inline CRYSTAL::charge_d_data_v5::charge_d_data_v5(const int_g np,
						   DLV::plane *s,
						   const Density_Matrix &dm,
						   const NewK &nk)
  : charge_d_data(np, s), property_data(dm, nk)
{
}

inline CRYSTAL::charge_d_data_v6::charge_d_data_v6(const int_g np,
						   DLV::plane *s,
						   const Density_Matrix &dm,
						   const NewK &nk)
  : charge_d_data(np, s), property_data_v6(dm, nk)
{
}

inline CRYSTAL::load_charge_d_slice::load_charge_d_slice(const char file[])
  : load_data_op(file)
{
}

inline
CRYSTAL::load_charge_d_slice_v4::load_charge_d_slice_v4(const char file[])
  : load_charge_d_slice(file)
{
}

inline
CRYSTAL::load_charge_d_slice_v5::load_charge_d_slice_v5(const char file[])
  : load_charge_d_slice(file)
{
}

inline bool CRYSTAL::charge_d_calc::is_binary() const
{
  return binary_wvfn;
}

inline CRYSTAL::charge_d_calc_v4::charge_d_calc_v4(const int_g np,
						   DLV::plane *s,
						   const Density_Matrix &dm,
						   const NewK &nk)
  : charge_d_data_v4(np, s, dm, nk)
{
}

inline CRYSTAL::charge_d_calc_v5::charge_d_calc_v5(const int_g np,
						   DLV::plane *s,
						   const Density_Matrix &dm,
						   const NewK &nk)
  : charge_d_data_v5(np, s, dm, nk)
{
}

inline CRYSTAL::charge_d_calc_v6::charge_d_calc_v6(const int_g np,
						   DLV::plane *s,
						   const Density_Matrix &dm,
						   const NewK &nk)
  : charge_d_data_v6(np, s, dm, nk)
{
}

#endif // CRYSTAL_CHARGE_DENSITY_SLICE
