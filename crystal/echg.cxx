
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_admin.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_surf.hxx"
#include "../dlv/job_setup.hxx"
#include "calcs.hxx"
#include "props.hxx"
#include "slices.hxx"
#include "echg.hxx"

CRYSTAL::charge_d_data::~charge_d_data()
{
}

DLV::operation *CRYSTAL::charge_d_calc::create(const int_g np,
					       const int_g selection,
					       const Density_Matrix &dm,
					       const NewK &nk,
					       const int_g version,
					       const DLV::job_setup_data &job,
					       const bool extern_job,
					       const char extern_dir[],
					       char message[], const int_g mlen)
{
  charge_d_calc *op = 0;
  message[0] = '\0';
  DLV::data_object *data = DLV::operation::find_plane(selection);
  if (data == 0)
    strncpy(message, "Failed to find plane", mlen);
  else {
    DLV::plane *surf = dynamic_cast<DLV::plane *>(data);
    if (surf == 0)
      strncpy(message, "BUG: selected grid is not a plane", mlen);
    else {
      bool parallel = false;
      switch (version) {
      case CRYSTAL98:
	op = new charge_d_calc_v4(np, surf, dm, nk);
	break;
      case CRYSTAL03:
	op = new charge_d_calc_v5(np, surf, dm, nk);
	break;
      case CRYSTAL06:
      case CRYSTAL09:
      case CRYSTAL14:
      case CRYSTAL17:
      case CRYSTAL23:
	op = new charge_d_calc_v6(np, surf, dm, nk);
	break;
      case CRYSTAL_DEV:
	// Todo - is this possible?
	op = new charge_d_calc_v6(np, surf, dm, nk);
	parallel = job.is_parallel();
	break;
      default:
	strncpy(message, "BUG: unknown CRYSTAL version", mlen);
	break;
      }
      if (op == 0) {
	if (strlen(message) == 0)
	  strncpy(message,
		  "Failed to allocate CRYSTAL::charge_d_calc operation", mlen);
      } else {
	op->attach();
	if (op->create_files(parallel, job.is_local(), message, mlen))
	  if (op->make_directory(message, mlen))
	    op->write(op->get_infile_name(0), op, op->get_model(),
		      message, mlen);
	if (strlen(message) > 0) {
	  // Don't delete once attached
	  //delete op;
	  //op = 0;
	} else {
	  op->execute(op->get_path(), op->get_serial_executable(),
		      op->get_parallel_executable(), "", job, parallel,
		      extern_job, extern_dir, message, mlen);
	}
      }
    }
  }
  return op;
}

DLV::string CRYSTAL::charge_d_calc::get_name() const
{
  return ("CRYSTAL Charge Density slice calculation");
}

DLV::operation *CRYSTAL::load_charge_d_slice::create(const char filename[],
					      const int_g version,
					      char message[], const int_g mlen)
{
  if (version == CRYSTAL98)
    return load_charge_d_slice_v4::create(filename, message, mlen);
  else
    return load_charge_d_slice_v5::create(filename, message, mlen);
}

DLV::string CRYSTAL::load_charge_d_slice::get_name() const
{
  return ("Load CRYSTAL charge density slice - " + get_filename());
}

DLV::operation *CRYSTAL::load_charge_d_slice_v4::create(const char filename[],
							char message[],
							const int_g mlen)
{
  load_charge_d_slice_v4 *op = new load_charge_d_slice_v4(filename);
  DLV::data_object *data = op->read_data(op, 0, filename, filename,
					 "Charge Density", 2,
					 message, mlen, false);
  if (data == 0) {
    delete op;
    op = 0;
  } else {
    op->attach();
    op->attach_data(data);
    if (data->get_number_data_sets() > 1)
      strncpy(message, "Second data set may not be correctly labelled", mlen);
  }
  return op;
}

DLV::operation *CRYSTAL::load_charge_d_slice_v5::create(const char filename[],
							char message[],
							const int_g mlen)
{
  load_charge_d_slice_v5 *op = new load_charge_d_slice_v5(filename);
  DLV::data_object *data = op->read_data(op, 0, filename, filename,
					 "Charge Density", 2,
					 message, mlen, false);
  if (data == 0) {
    delete op;
    op = 0;
  } else {
    op->attach();
    op->attach_data(data);
    if (data->get_number_data_sets() > 1)
      strncpy(message, "Second data set may not be correctly labelled", mlen);
  }
  return op;
}

void CRYSTAL::charge_d_calc_v6::add_calc_error_file(const DLV::string tag,
						    const bool is_parallel,
						    const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

bool CRYSTAL::charge_d_calc::create_files(const bool is_parallel,
					  const bool is_local, char message[],
					  const int_g mlen)
{
  DLV::string filename;
  if (find_wavefunction(filename, binary_wvfn, message, mlen)) {
    static char tag[] = "cd2";
    static char input[] = "inp";
    add_calc_error_file(tag, is_parallel, is_local);
    if (is_parallel)
      add_input_file(0, tag, input, "INPUT", is_local, false);
    else
      add_command_file(0, tag, input, is_local, false);
    if (binary_wvfn)
      add_data_file(1, filename, "fort.9", is_local, true);
    else
      add_data_file(1, filename, "fort.98", is_local, true);
    add_log_file(tag, "out", is_local, true);
    add_sys_error_file(tag, "job", is_local, true);
    add_output_file(0, tag, "dat", "fort.25", is_local, true);
    return true;
  } else
    return false;
}

void CRYSTAL::charge_d_data::write_input(std::ofstream &output)
{
  output << "ECHG\n";
  output << "0\n";
  write_grid(output);
  output << "END\n";
  output.close();
}

void CRYSTAL::charge_d_data_v4::write_grid(std::ofstream &output)
{
  write_mapnet(output, get_vertices(), get_npoints());
}

void CRYSTAL::charge_d_data_v5::write_grid(std::ofstream &output)
{
  write_mapnet(output, get_vertices(), get_npoints());
}

void CRYSTAL::charge_d_data_v6::write_grid(std::ofstream &output)
{
  write_mapnet(output, get_vertices(), get_npoints());
}

void CRYSTAL::charge_d_calc_v4::write(const DLV::string filename,
				      const DLV::operation *op,
				      const DLV::model *const structure,
				      char message[], const int_g mlen)
{
  std::ofstream output;
  if (DLV::open_file_write(output, filename.c_str(), message, mlen)) {
    write_header(output, op, structure, is_binary());
    write_input(output);
  }
}

void CRYSTAL::charge_d_calc_v5::write(const DLV::string filename,
				      const DLV::operation *op,
				      const DLV::model *const structure,
				      char message[], const int_g mlen)
{
  std::ofstream output;
  if (DLV::open_file_write(output, filename.c_str(), message, mlen)) {
    write_header(output, op, structure, is_binary());
    write_input(output);
  }
}

void CRYSTAL::charge_d_calc_v6::write(const DLV::string filename,
				      const DLV::operation *op,
				      const DLV::model *const structure,
				      char message[], const int_g mlen)
{
  std::ofstream output;
  if (DLV::open_file_write(output, filename.c_str(), message, mlen)) {
    write_header(output, op, structure, is_binary());
    write_input(output);
  }
}

bool CRYSTAL::charge_d_calc_v4::recover(const bool no_err, const bool log_ok,
					char message[], const int_g len)
{
  DLV::string id = get_job_id();
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi())
      read_logfile(name, id);
    DLV::data_object *file = 0;
    if (no_err)
      file = new DLV::text_file(name, program_name, id,
				"Charge2D output file");
    else
      file = new DLV::text_file(name, program_name, id,
				"Charge2D(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    DLV::data_object *data = read_data(this, 0, get_outfile_name(0).c_str(),
				       id, "Charge Density", 2,
				       message, len, false);
    if (data == 0)
      return false;
    else
      attach_data(data);
  }
  return true;
}

bool CRYSTAL::charge_d_calc_v5::recover(const bool no_err, const bool log_ok,
					char message[], const int_g len)
{
  DLV::string id = get_job_id();
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi())
      read_logfile(name, id);
    DLV::data_object *file = 0;
    if (no_err)
      file = new DLV::text_file(name, program_name, id,
				"Charge2D output file");
    else
      file = new DLV::text_file(name, program_name, id,
				"Charge2D(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    DLV::data_object *data = read_data(this, 0, get_outfile_name(0).c_str(),
				       id, "Charge Density", 2,
				       message, len, false);
    if (data == 0)
      return false;
    else
      attach_data(data);
  }
  return true;
}

bool CRYSTAL::charge_d_calc_v6::recover(const bool no_err, const bool log_ok,
					char message[], const int_g len)
{
  DLV::string id = get_job_id();
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi())
      read_logfile(name, id);
    DLV::data_object *file = 0;
    if (no_err)
      file = new DLV::text_file(name, program_name, id,
				"Charge2D output file");
    else
      file = new DLV::text_file(name, program_name, id,
				"Charge2D(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    DLV::data_object *data = read_data(this, 0, get_outfile_name(0).c_str(),
				       id, "Charge Density", 2,
				       message, len, false);
    if (data == 0)
      return false;
    else
      attach_data(data);
  }
  return true;
}

bool CRYSTAL::load_charge_d_slice_v4::reload_data(DLV::data_object *data,
						  char message[],
						  const int_g mlen)
{
  DLV::surface_data *v = dynamic_cast<DLV::surface_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload 2D charge density", mlen);
    return false;
  } else
    return (read_data(this, v, get_filename().c_str(),
		      "", "Charge Density", 2, message, mlen, false) != 0);
  return false;
}

bool CRYSTAL::load_charge_d_slice_v5::reload_data(DLV::data_object *data,
						  char message[],
						  const int_g mlen)
{
  DLV::surface_data *v = dynamic_cast<DLV::surface_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload 2D charge density", mlen);
    return false;
  } else
    return (read_data(this, v, get_filename().c_str(),
		      "", "Charge Density", 2, message, mlen, false) != 0);
  return false;
}

bool CRYSTAL::charge_d_calc_v4::reload_data(DLV::data_object *data,
					    char message[], const int_g mlen)
{
  DLV::surface_data *v = dynamic_cast<DLV::surface_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload 2D charge density", mlen);
    return false;
  } else
    return (read_data(this, v, get_outfile_name(0).c_str(),
		      "", "Charge Density", 2, message, mlen, false) != 0);
  return false;
}

bool CRYSTAL::charge_d_calc_v5::reload_data(DLV::data_object *data,
					    char message[], const int_g mlen)
{
  DLV::surface_data *v = dynamic_cast<DLV::surface_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload 2D charge density", mlen);
    return false;
  } else
    return (read_data(this, v, get_outfile_name(0).c_str(),
		      "", "Charge Density", 2, message, mlen, false) != 0);
  return false;
}

bool CRYSTAL::charge_d_calc_v6::reload_data(DLV::data_object *data,
					    char message[], const int_g mlen)
{
  DLV::surface_data *v = dynamic_cast<DLV::surface_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload 2D charge density", mlen);
    return false;
  } else
    return (read_data(this, v, get_outfile_name(0).c_str(),
		      "", "Charge Density", 2, message, mlen, false) != 0);
  return false;
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    CRYSTAL::load_charge_d_slice_v4 *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::load_charge_d_slice_v4("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    CRYSTAL::load_charge_d_slice_v5 *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::load_charge_d_slice_v5("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::charge_d_calc_v4 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::charge_d_calc_v4(0, 0, d, n);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::charge_d_calc_v5 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::charge_d_calc_v5(0, 0, d, n);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::charge_d_calc_v6 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::charge_d_calc_v6(0, 0, d, n);
    }

  }
}

template <class Archive>
void CRYSTAL::charge_d_data::serialize(Archive &ar, const unsigned int version)
{
  ar & npoints;
  ar & grid;
}

template <class Archive>
void CRYSTAL::charge_d_data_v4::serialize(Archive &ar,
					  const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::charge_d_data>(*this);
  ar & boost::serialization::base_object<CRYSTAL::property_data>(*this);
}

template <class Archive>
void CRYSTAL::charge_d_data_v5::serialize(Archive &ar,
					  const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::charge_d_data>(*this);
  ar & boost::serialization::base_object<CRYSTAL::property_data>(*this);
}

template <class Archive>
void CRYSTAL::charge_d_data_v6::serialize(Archive &ar,
					  const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::charge_d_data>(*this);
  ar & boost::serialization::base_object<CRYSTAL::property_data_v6>(*this);
}

template <class Archive>
void CRYSTAL::load_charge_d_slice::serialize(Archive &ar,
					     const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::load_data_op>(*this);
}

template <class Archive>
void CRYSTAL::load_charge_d_slice_v4::serialize(Archive &ar,
						const unsigned int version)
{
  ar & boost::serialization::base_object<load_charge_d_slice>(*this);
}

template <class Archive>
void CRYSTAL::load_charge_d_slice_v5::serialize(Archive &ar,
						const unsigned int version)
{
  ar & boost::serialization::base_object<load_charge_d_slice>(*this);
}

template <class Archive>
void CRYSTAL::charge_d_calc::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::property_calc>(*this);
}

template <class Archive>
void CRYSTAL::charge_d_calc_v4::serialize(Archive &ar,
					  const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::charge_d_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::charge_d_data_v4>(*this);
}

template <class Archive>
void CRYSTAL::charge_d_calc_v5::serialize(Archive &ar,
					  const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::charge_d_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::charge_d_data_v5>(*this);
}

template <class Archive>
void CRYSTAL::charge_d_calc_v6::serialize(Archive &ar,
					  const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::charge_d_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::charge_d_data_v6>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::load_charge_d_slice_v4)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::load_charge_d_slice_v5)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::charge_d_calc_v4)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::charge_d_calc_v5)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::charge_d_calc_v6)

DLV_SUPPRESS_TEMPLATES(DLV::load_data_op)
DLV_SUPPRESS_TEMPLATES(DLV::plane)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::property_data)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::property_data_v6)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::property_calc)

DLV_NORMAL_EXPLICIT(CRYSTAL::load_charge_d_slice_v4)
DLV_NORMAL_EXPLICIT(CRYSTAL::load_charge_d_slice_v5)
DLV_NORMAL_EXPLICIT(CRYSTAL::charge_d_calc_v4)
DLV_NORMAL_EXPLICIT(CRYSTAL::charge_d_calc_v5)
DLV_NORMAL_EXPLICIT(CRYSTAL::charge_d_calc_v6)

#endif // DLV_USES_SERIALIZE
