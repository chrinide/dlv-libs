
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/constants.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_vol.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/calculation.hxx"
#include "calcs.hxx"
#include "props.hxx"
#include "volumes.hxx"

CRYSTAL::grid3D::~grid3D()
{
}

DLV::volume_data *CRYSTAL::grid3D::read_data(DLV::operation *op,
					     const char filename[],
					     const DLV::string id,
					     char message[], const int_g mlen)
{
  DLV::volume_data *data = 0;
  if (DLV::check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (DLV::open_file_read(input, filename, message, mlen)) {
      char label[128];
      int_g nx = 0;
      int_g ny = 0;
      int_g nz = 0;
      real_g origin[3];
      real_g astep[3];
      real_g bstep[3];
      real_g cstep[3];
      if (get_grid(input, nx, ny, nz, origin, astep, bstep, cstep,
		   label, message, mlen)) {
	data = create_volume("CRYSTAL", id, op, nx, ny, nz, origin, astep,
			     bstep, cstep);
	if (!get_data(input, data, op, nx, ny, nz, label, message, mlen)) {
	  delete data;
	  data = 0;
	}
      }
      input.close();
    }
  }
  return data;
}

bool CRYSTAL::grid3D::read_data(DLV::operation *op, DLV::volume_data *data,
				const char filename[], char message[],
				const int_g mlen)
{
  bool ok = false;
  if (DLV::check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (DLV::open_file_read(input, filename, message, mlen)) {
      char label[128];
      int_g nx = 0;
      int_g ny = 0;
      int_g nz = 0;
      real_g origin[3];
      real_g astep[3];
      real_g bstep[3];
      real_g cstep[3];
      if (get_grid(input, nx, ny, nz, origin, astep, bstep, cstep,
		   label, message, mlen))
	if (get_data(input, data, op, nx, ny, nz, label, message, mlen))
	  ok = true;
      input.close();
    }
  }
  return ok;
}

DLV::volume_data *CRYSTAL::grid3D::create_volume(const char code[],
						 const DLV::string id,
						 DLV::operation *op,
						 const int_g nx, const int_g ny,
						 const int_g nz,
						 const real_g origin[3],
						 const real_g astep[3],
						 const real_g bstep[3],
						 const real_g cstep[3]) const
{
  return new DLV::rspace_periodic_volume("CRYSTAL", id, op, nx, ny, nz,
					 origin, astep, bstep, cstep);
}

bool CRYSTAL::grid3D::get_grid(std::ifstream &input, int_g &nx, int_g &ny,
			       int_g &nz, real_g origin[3], real_g astep[3],
			       real_g bstep[3], real_g cstep[3], char label[],
			       char message[], const int_g mlen)
{
  input >> nx;
  input >> ny;
  input >> nz;
  if (nx < 1 or ny < 1 or nz < 1) {
    strncpy(message,
	    "Empty grid - is this really a CRYSTAL volume file?", mlen);
    return false;
  } else
    return read_grid(input, origin, astep, bstep, cstep, message, mlen);
}

bool CRYSTAL::grid3D_v4::get_grid(std::ifstream &input, int_g &nx, int_g &ny,
				  int_g &nz, real_g origin[3], real_g astep[3],
				  real_g bstep[3], real_g cstep[3],
				  char label[], char message[],
				  const int_g mlen)
{
  input.getline(label, 128);
  return grid3D::get_grid(input, nx, ny, nz, origin, astep, bstep, cstep,
			  label, message, mlen);
}

bool CRYSTAL::grid3D::read_grid(std::ifstream &input, real_g origin[3],
				real_g astep[3], real_g bstep[3],
				real_g cstep[3], char message[],
				const int_g mlen)
{
  real_g o[3];
  input >> o[0];
  input >> o[1];
  input >> o[2];
  real_g da[3];
  input >> da[0];
  input >> da[1];
  input >> da[2];
  real_g db[3];
  input >> db[0];
  input >> db[1];
  input >> db[2];
  real_g dc[3];
  input >> dc[0];
  input >> dc[1];
  input >> dc[2];
  const real_g scale = get_grid_scale();
  for (int_g i = 0; i < 3; i++) {
    origin[i] = o[i] * scale;
    astep[i] = da[i] * scale;
    bstep[i] = db[i] * scale;
    cstep[i] = dc[i] * scale;
  }
  char buff[128];
  // Get end of line char after grid!
  input.getline(buff, 128);
  return true;
}

CRYSTAL::real_g CRYSTAL::grid3D::get_grid_scale() const
{
  return real_g(C09_bohr_to_angstrom);
}

bool CRYSTAL::grid3D::get_data_scale(real_g &scale) const
{
  return false;
}

bool CRYSTAL::grid3D_v4::get_data(std::ifstream &input, DLV::volume_data *data,
				  DLV::operation *op, const int_g nx,
				  const int_g ny, const int_g nz,
				  const char label[], char message[],
				  const int_g mlen)
{
  if (read_data(input, data, nx, ny, nz, 1, label, message, mlen)) {
    // Check for spin density - This will be the label
    char line[128];
    input.getline(line, 128);
    if (!input.eof()) {
      // There is a spin density also, skip grid repeat
      char text[256];
      input.getline(text, 256);
      input.getline(text, 256);
      input.getline(text, 256);
      input.getline(text, 256);
      input.getline(text, 256);
      return read_data(input, data, nx, ny, nz, 1, line, message, mlen);
    } else
      return true;
  } else
    return false;
}

bool CRYSTAL::grid3D_v5::get_data(std::ifstream &input, DLV::volume_data *data,
				  DLV::operation *op, const int_g nx,
				  const int_g ny, const int_g nz,
				  const char label[], char message[],
				  const int_g mlen)
{
  char line[128];
  input.getline(line, 128);
  if (read_data(input, data, nx, ny, nz, 1, line, message, mlen)) {
    // Check for spin density - This will be the label
    input.getline(line, 128);
    if (!input.eof())
      return read_data(input, data, nx, ny, nz, 1, line, message, mlen);
    else
      return true;
  } else
    return false;
}

bool CRYSTAL::grid3D_v6::get_data(std::ifstream &input, DLV::volume_data *data,
				  DLV::operation *op, const int_g nx,
				  const int_g ny, const int_g nz,
				  const char label[], char message[],
				  const int_g mlen)
{
  char line[128];
  input.getline(line, 128);
  char buff[128];
  int_g vec;
  process_label(line, buff, vec);
  bool get_data = true;
  bool ok = true;
  while (get_data) {
    if ((ok = read_data(input, data, nx, ny, nz, vec, buff, message, mlen))) {
      // Check for next data set - This will be the label
      input.getline(line, 128);
      if (input.eof())
	get_data = false;
      else
	process_label(line, buff, vec);
    } else
      get_data = false;
  }
  return ok;
}

void CRYSTAL::grid3D_v6::process_label(const char line[], char buff[],
				       int_g &v)
{
  v = 1;
  int_g ipos = 0;
  while (isspace(line[ipos]))
    ipos++;
  if (isdigit(line[ipos])) {
    strcpy(buff, line);
    v = atoi(&line[ipos]);
    ipos++;
    while (isspace(line[ipos]))
      ipos++;
  }
  strcpy(buff, &line[ipos]);
}

bool CRYSTAL::grid3D::read_data(std::ifstream &input, DLV::volume_data *data,
				const int_g nx, const int_g ny, const int_g nz,
				const int_g vec, const char label[],
				char message[], const int_g mlen)
{
  int_l n = (int_l)nx * (int_l)ny * (int_l)nz * (int_l)vec;
  real_g *array = new real_g[n];
  if (array == 0) {
    strncpy(message, "Unable to allocate memory for data", mlen);
    return false;
  } else {
    for (int_l i = 0; i < n; i++)
      input >> array[i];
    real_g scale = 1.0;
    if (get_data_scale(scale)) {
      for (int_l i = 0; i < n; i++)
	array[i] *= scale;
    }
    data->add_data(array, vec, label, false);
    // Get end of line char
    char line[128];
    input.getline(line, 128);
    return true;
  }
}

bool CRYSTAL::grid3D_v6::read_wvfn(std::ifstream &input, DLV::volume_data *data,
				   const int_g nx, const int_g ny,
				   const int_g nz, const char label[],
				   char message[], const int_g mlen)
{
  int_l n = (int_l)nx * (int_l)ny * (int_l)nz;
  real_g *a = new real_g[n];
  real_g *b = new real_g[n];
  if (a == 0 or b == 0) {
    strncpy(message, "Unable to allocate memory for data", mlen);
    return false;
  } else {
    for (int_l i = 0; i < n; i++) {
      input >> a[i];
      if (a[i] < 0.0)
	b[i] = real_g(DLV::pi);
      else
	b[i] = 0.0;
      // take square of magnitude for psi* psi
      a[i] = a[i] * a[i];
    }
    data->add_data(a, b, label, false);
    // Get end of line char
    char line[128];
    input.getline(line, 128);
    return true;
  }
}

bool CRYSTAL::grid3D_v6::read_wvfn(std::ifstream &input, DLV::volume_data *data,
				   const int_g nx, const int_g ny,
				   const int_g nz, const char label[],
				   const int_g k1, const int_g k2,
				   const int_g k3, const int_g s1,
				   const int_g s2, const int_g s3,
				   char message[], const int_g mlen)
{
  int_l n = (int_l)nx * (int_l)ny * (int_l)nz;
  real_g *a = new real_g[n];
  real_g *b = new real_g[n];
  if (a == 0 or b == 0) {
    strncpy(message, "Unable to allocate memory for data", mlen);
    return false;
  } else {
    for (int_l i = 0; i < n; i++) {
      input >> a[i];
      input >> b[i];
      // take square of magnitude for psi* phi
      a[i] = a[i] * a[i];
    }
    data->add_data(a, b, label, false);
    // Get end of line char
    char line[128];
    input.getline(line, 128);
    return true;
  }
}

// topond
bool CRYSTAL::grid3D_v8::read_topo_grid(std::ifstream &input, real_g origin[3],
					real_g astep[3], real_g bstep[3],
					real_g cstep[3],
					const DLV::coord_type a[3],
					const DLV::coord_type b[3],
					const DLV::coord_type c[3],
					const int_g nx, const int_g ny,
					const int_g nz, const int_g dims,
					char message[], const int_g mlen)
{
  real_g x[3];
  input >> x[0];
  input >> x[1];
  input >> x[2];
  real_g y[3];
  input >> y[0];
  input >> y[1];
  input >> y[2];
  real_g z[3];
  input >> z[0];
  input >> z[1];
  input >> z[2];
  const real_g scale = get_grid_scale();
  //for (int i = dims; i < 3; i++)
  //origin[i] *= scale;
  if (dims > 0) {
    origin[0] = x[0];
    // x[2] is the length of the primitive vector and not very useful
    real_g step = real_g(nx - 1);
    astep[0] = a[0] / step;
    astep[1] = a[1] / step;
    astep[2] = a[2] / step;
  } else {
    origin[0] = x[0] * scale;
    astep[0] = x[2] * scale;
    astep[1] = 0.0;
    astep[2] = 0.0;
  }
  if (dims > 1) {
    origin[1] = y[0];
    real_g step = real_g(ny - 1);
    bstep[0] = b[0] / step;
    bstep[1] = b[1] / step;
    bstep[2] = b[2] / step;
  } else {
    origin[1] = y[0] * scale;
    bstep[0] = 0.0;
    bstep[1] = y[2] * scale;
    bstep[2] = 0.0;
  }
  if (dims > 2) {
    origin[2] = z[0];
    real_g step = real_g(nz - 1);
    cstep[0] = c[0] / step;
    cstep[1] = c[1] / step;
    cstep[2] = c[2] / step;
  } else {
    origin[2] = z[0] * scale;
    cstep[0] = 0.0;
    cstep[1] = 0.0;
    cstep[2] = z[2] * scale;
  }
  char buff[128];
  // Get end of line char after grid!
  input.getline(buff, 128);
  return true;
}

bool CRYSTAL::grid3D_v8::get_topo_grid(std::ifstream &input, int_g &nx,
				       int_g &ny, int_g &nz, real_g origin[3],
				       real_g astep[3], real_g bstep[3],
				       real_g cstep[3],
				       const DLV::coord_type a[3],
				       const DLV::coord_type b[3],
				       const DLV::coord_type c[3],
				       const int_g dims,
				       char message[], const int_g mlen)
{
  input >> nx;
  input >> ny;
  input >> nz;
  if (nx < 1 or ny < 1 or nz < 1) {
    strncpy(message,
	    "Empty grid - is this really a CRYSTAL Topond3D file?", mlen);
    return false;
  } else
    return read_topo_grid(input, origin, astep, bstep, cstep, a, b, c,
			  nx, ny, nz, dims, message, mlen);
}

DLV::volume_data *CRYSTAL::grid3D_v8::read_topo_data(DLV::operation *op,
						     const char filename[],
						     const DLV::string id,
						     const DLV::coord_type a[3],
						     const DLV::coord_type b[3],
						     const DLV::coord_type c[3],
						     const int_g dims,
						     DLV::volume_data *data,
						     const char label[],
						     char message[],
						     const int_g mlen)
{
  if (DLV::check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (DLV::open_file_read(input, filename, message, mlen)) {
      int_g nx = 0;
      int_g ny = 0;
      int_g nz = 0;
      real_g origin[3];
      real_g astep[3];
      real_g bstep[3];
      real_g cstep[3];
      if (get_topo_grid(input, nx, ny, nz, origin, astep, bstep, cstep,
			a, b, c, dims, message, mlen)) {
	if (data == 0)
	  data = create_volume("CRYSTAL", id, op, nx, ny, nz, origin, astep,
			       bstep, cstep);
	if (!get_data(input, data, op, nx, ny, nz, label, message, mlen)) {
	  delete data;
	  data = 0;
	}
      }
      input.close();
    }
  }
  return data;
}

bool CRYSTAL::grid3D_v8::get_data(std::ifstream &input, DLV::volume_data *data,
				  DLV::operation *op, const int_g nx,
				  const int_g ny, const int_g nz,
				  const char label[], char message[],
				  const int_g mlen)
{
  return read_data(input, data, nx, ny, nz, 1, label, message, mlen);
  // Todo - used by reload?
  //return false;
}
