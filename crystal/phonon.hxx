
#ifndef CRYSTAL_PHONONS
#define CRYSTAL_PHONONS

namespace CRYSTAL {

  class phonon_file {
  protected:
    DLV::data_object *read_data(DLV::operation *op, const char filename[],
				const DLV::string id, char message[],
				const int_g mlen);
  };

  class phonon_data : public phonon_file {
  protected:
    void set_params(const Phonon &p);
    void write_phonons(std::ofstream &output) const;
    void write_phonons_v7(std::ofstream &output, const int_g model_type) const;

  private:
    Phonon data;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class phonon_data_v6 : public scf_data_v6, public phonon_data {
  protected:
    void write_geom_commands(std::ofstream &output,
			     const DLV::model *const structure) const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class phonon_data_v7 : public scf_data_v7, public phonon_data {
  protected:
    void write_geom_commands(std::ofstream &output,
			     const DLV::model *const structure) const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class phonon_data_v8 : public scf_data_v8, public phonon_data {
  protected:
    void write_geom_commands(std::ofstream &output,
			     const DLV::model *const structure) const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  // No changes between v6/v7
  class load_phonon_data : public DLV::load_data_op, public phonon_file {
  public:
    static DLV::operation *create(const char filename[],
				  char message[], const int_g mlen);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

    // public for serialization
    load_phonon_data(const char file[]);

  protected:
    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class phonon_calc : public scf_calc {
  public:
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    DLV::string get_name() const;
    bool create_files(const bool is_parallel, const bool is_local,
		      char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE

  };

  class phonon_calc_v6 : public phonon_calc, public phonon_data_v6 {
  public:
    phonon_calc_v6();

  protected:
    void set_params(const Hamiltonian &h, const Basis &b, const Kpoints &k,
		    const Tolerances &tol, const Convergence &c,
		    const Print &p, const Joboptions &j, const Optimise &opt,
		    const Phonon &ph, const Neb &n, const CPHF &cp,
		    const TDDFT &tddft, const bool a);
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void add_restart_file(const DLV::string tag, const bool is_local);
    void add_opt_restart_file(const DLV::string tag, const bool is_local);
    bool add_basis_set(const char filename[], const bool set_default,
		       int_g * &indices, int_g &value,
		       char message[], const int_g mlen);
    bool write(const DLV::string filename, const DLV::model *const structure,
	       char message[], const int_g mlen);
    void write_structure(const DLV::string filename,
			 const DLV::model *const structure,
			 char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE

  };

  class phonon_calc_v7 : public phonon_calc, public phonon_data_v7 {
  public:
    phonon_calc_v7();

  protected:
    void set_params(const Hamiltonian &h, const Basis &b, const Kpoints &k,
		    const Tolerances &tol, const Convergence &c,
		    const Print &p, const Joboptions &j, const Optimise &opt,
		    const Phonon &ph, const Neb &n, const CPHF &cp,
		    const TDDFT &tddft, const bool a);
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void add_restart_file(const DLV::string tag, const bool is_local);
    void add_opt_restart_file(const DLV::string tag, const bool is_local);
    bool add_basis_set(const char filename[], const bool set_default,
		       int_g * &indices, int_g &value,
		       char message[], const int_g mlen);
    bool write(const DLV::string filename, const DLV::model *const structure,
	       char message[], const int_g mlen);
    void write_structure(const DLV::string filename,
			 const DLV::model *const structure,
			 char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class phonon_calc_v8 : public phonon_calc, public phonon_data_v8 {
  public:
    phonon_calc_v8();

  protected:
    void set_params(const Hamiltonian &h, const Basis &b, const Kpoints &k,
		    const Tolerances &tol, const Convergence &c,
		    const Print &p, const Joboptions &j, const Optimise &opt,
		    const Phonon &ph, const Neb &n, const CPHF &cp,
		    const TDDFT &tddft, const bool a);
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void add_restart_file(const DLV::string tag, const bool is_local);
    void add_opt_restart_file(const DLV::string tag, const bool is_local);
    bool add_basis_set(const char filename[], const bool set_default,
		       int_g * &indices, int_g &value,
		       char message[], const int_g mlen);
    bool write(const DLV::string filename, const DLV::model *const structure,
	       char message[], const int_g mlen);
    void write_structure(const DLV::string filename,
			 const DLV::model *const structure,
			 char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(CRYSTAL::load_phonon_data)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::phonon_calc_v6)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::phonon_calc_v7)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::phonon_calc_v8)
#endif // DLV_USES_SERIALIZE

inline CRYSTAL::load_phonon_data::load_phonon_data(const char file[])
  : load_data_op(file)
{
}

inline CRYSTAL::phonon_calc_v6::phonon_calc_v6()
  : phonon_data_v6()
{
}

inline CRYSTAL::phonon_calc_v7::phonon_calc_v7()
  : phonon_data_v7()
{
}

inline CRYSTAL::phonon_calc_v8::phonon_calc_v8()
  : phonon_data_v8()
{
}

#endif // CRYSTAL_PHONONS
