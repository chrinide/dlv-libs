
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_admin.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/data_atoms.hxx"
#include "../dlv/job_setup.hxx"
#include "calcs.hxx"
#include "scf.hxx"
#include "phonon.hxx"

DLV::string CRYSTAL::phonon_calc::get_name() const
{
  return ("CRYSTAL Phonon calculation");
}

DLV::operation *CRYSTAL::load_phonon_data::create(const char filename[],
						  char message[],
						  const int_g mlen)
{
  load_phonon_data *op = new load_phonon_data(filename);
  DLV::data_object *data = op->read_data(op, filename, filename,
					 message, mlen);
  if (data == 0) {
    delete op;
    op = 0;
  } else {
    op->attach();
    op->attach_data(data);
  }
  return op;
}

DLV::string CRYSTAL::load_phonon_data::get_name() const
{
  return ("Load CRYSTAL Phonon vectors - " + get_filename());
}

DLV::data_object *CRYSTAL::phonon_file::read_data(DLV::operation *op,
						  const char filename[],
						  const DLV::string id,
						  char message[],
						  const int_g mlen)
{
  // Basically a copy of the GULP version.
  DLV::vectors_and_intensity *glyph = 0;
  if (DLV::check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (DLV::open_file_read(input, filename, message, mlen)) {
      int_g i, atn, n, nkp, nmodes;
      char line[256];
      bool ok = true;
      input >> n;
      if (n > 0) {
	// Read the atoms
	real_g (*coords)[3] = new real_g[n][3];
	for (i = 0; i < n; i++) {
	  // Ignore atomic number
	  // Todo - really should use this to check that the cell settings
	  // are the same, so the same atom types are at the coords.
	  input >> atn;
	  // These may be fragment coordinates.
	  input >> coords[i][0];
	  input >> coords[i][1];
	  input >> coords[i][2];
	}
	input >> nkp;
	input >> nmodes;
	// Now read the eigenvectors.
	real_g (*vecs)[3] = new_local_array2(real_g, n, 3);
	int_g nk, nm;
	real_g freq, ka, kb, kc;
	char mode_sym[16];
	for (nk = 0; nk < nkp; nk++) {
	  // clear to next line
	  input.getline(line, 256);
	  input >> line; // "K"
	  input >> line; // "point"
	  input >> line; // "at"
	  input >> ka;
	  input >> kb;
	  input >> kc;
	  for (nm = 0; nm < nmodes; nm++) {
	    // clear to next line
	    input.getline(line, 256);
	    input.getline(line, 256);
	    char misc[16]; // "Mode"
	    real_g intensity;
	    int_g ir_active;
	    int_g raman_active;
	    if (sscanf(line, "%s %d %f %s %f %d %d", misc, &atn, &freq,
		       mode_sym, &intensity, &ir_active, &raman_active) < 7) {
	      intensity = 0.0;
	      ir_active = 1;
	      raman_active = 1;
	    } else {
	      // 0 is active, 1 inactive
	      ir_active = !ir_active;
	      raman_active = !raman_active;
	    }
	    // Read eigenvectors
	    real_g temp;
	    // Currently throw away dummy imaginary parts
	    for (i = 0; i < n; i++) {
	      input >> vecs[i][0];
	      input >> temp;
	      input >> vecs[i][1];
	      input >> temp;
	      input >> vecs[i][2];
	      input >> temp;
	    }
	    if (glyph == 0) {
	      glyph = new DLV::vectors_and_intensity("CRYSTAL", id, op,
						     nmodes, nkp);
	      glyph->set_grid(coords, n, false);
	    }
	    ok = glyph->add_data(nk, nm, ka, kb, kc, freq, intensity,
				 ir_active, raman_active, mode_sym, vecs, n);
	    if (!ok)
	      break;
	  }
	  if (!ok)
	    break;
	}
	delete_local_array(vecs);
	//delete [] coords;
      }
      input.close();
    }
  }
  return glyph;
}

void CRYSTAL::phonon_data::set_params(const Phonon &p)
{
  data = p;
}

void CRYSTAL::phonon_calc_v6::set_params(const Hamiltonian &h, const Basis &b,
					 const Kpoints &k,
					 const Tolerances &tol,
					 const Convergence &c, const Print &p,
					 const Joboptions &j,
					 const Optimise &opt,
					 const Phonon &ph, const Neb &n, 
					 const CPHF &, const TDDFT &,
					 const bool a)
{
  scf_data::set_params(h, b, k, tol, c, p, j, a);
  phonon_data::set_params(ph);
}

void CRYSTAL::phonon_calc_v7::set_params(const Hamiltonian &h, const Basis &b,
					 const Kpoints &k,
					 const Tolerances &tol,
					 const Convergence &c, const Print &p,
					 const Joboptions &j,
					 const Optimise &opt,
					 const Phonon &ph, const Neb &n, 
					 const CPHF &, const TDDFT &,
					 const bool a)
{
  scf_data::set_params(h, b, k, tol, c, p, j, a);
  phonon_data::set_params(ph);
}

void CRYSTAL::phonon_calc_v8::set_params(const Hamiltonian &h, const Basis &b,
					 const Kpoints &k,
					 const Tolerances &tol,
					 const Convergence &c, const Print &p,
					 const Joboptions &j,
					 const Optimise &opt,
					 const Phonon &ph, const Neb &n, 
					 const CPHF &, const TDDFT &,
					 const bool a)
{
  scf_data::set_params(h, b, k, tol, c, p, j, a);
  phonon_data::set_params(ph);
}

bool CRYSTAL::phonon_calc_v6::add_basis_set(const char filename[],
					    const bool set_default,
					    int_g * &indices, int_g &value,
					    char message[], const int_g mlen)
{
  return scf_data::add_basis(filename, set_default, get_model(), indices,
			     value, message, mlen);
}

bool CRYSTAL::phonon_calc_v7::add_basis_set(const char filename[],
					    const bool set_default,
					    int_g * &indices, int_g &value,
					    char message[], const int_g mlen)
{
  return scf_data::add_basis(filename, set_default, get_model(), indices,
			     value, message, mlen);
}

bool CRYSTAL::phonon_calc_v8::add_basis_set(const char filename[],
					    const bool set_default,
					    int_g * &indices, int_g &value,
					    char message[], const int_g mlen)
{
  return scf_data::add_basis(filename, set_default, get_model(), indices,
			     value, message, mlen);
}

void CRYSTAL::phonon_calc_v6::add_calc_error_file(const DLV::string tag,
						  const bool is_parallel,
						  const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

void CRYSTAL::phonon_calc_v7::add_calc_error_file(const DLV::string tag,
						  const bool is_parallel,
						  const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

void CRYSTAL::phonon_calc_v8::add_calc_error_file(const DLV::string tag,
						  const bool is_parallel,
						  const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

void CRYSTAL::phonon_calc_v6::add_restart_file(const DLV::string tag,
					       const bool is_local)
{
  if (has_restart())
    add_data_file(1, restart_filename(), "fort.20", is_local, true);
}

void CRYSTAL::phonon_calc_v7::add_restart_file(const DLV::string tag,
					       const bool is_local)
{
  if (has_restart())
    add_data_file(1, restart_filename(), "fort.20", is_local, true);
}

void CRYSTAL::phonon_calc_v8::add_restart_file(const DLV::string tag,
					       const bool is_local)
{
  if (has_restart())
    add_data_file(1, restart_filename(), "fort.20", is_local, true);
}

void CRYSTAL::phonon_calc_v6::add_opt_restart_file(const DLV::string tag,
					       const bool is_local)
{
}

void CRYSTAL::phonon_calc_v7::add_opt_restart_file(const DLV::string tag,
					       const bool is_local)
{
}

void CRYSTAL::phonon_calc_v8::add_opt_restart_file(const DLV::string tag,
					       const bool is_local)
{
}

bool CRYSTAL::phonon_calc::create_files(const bool is_parallel,
					const bool is_local, char message[],
					const int_g mlen)
{
  static char tag[] = "phon";
  if (common_files(is_parallel, is_local, tag)) {
    add_output_file(1, tag, "phonlog", "SCFOUT.LOG", is_local, true);
    add_output_file(2, tag, "dlv", "phonons.dlv", is_local, true);
    add_output_file(3, tag, "20", "fort.20", is_local, true);
  } else
    return false;
  return true;
}

bool CRYSTAL::phonon_calc_v6::write(const DLV::string filename,
				    const DLV::model *const structure,
				    char message[], const int_g mlen)
{
  return write_input(filename, structure, message, mlen);
}

bool CRYSTAL::phonon_calc_v7::write(const DLV::string filename,
				    const DLV::model *const structure,
				    char message[], const int_g mlen)
{
  return write_input(filename, structure, message, mlen);
}

bool CRYSTAL::phonon_calc_v8::write(const DLV::string filename,
				    const DLV::model *const structure,
				    char message[], const int_g mlen)
{
  return write_input(filename, structure, message, mlen);
}

void CRYSTAL::phonon_calc_v6::write_structure(const DLV::string filename,
					     const DLV::model *const structure,
					      char message[], const int_g mlen)
{
  structure_file::write(filename, structure, get_c03_indices(), message, mlen);
}

void CRYSTAL::phonon_calc_v7::write_structure(const DLV::string filename,
					      const DLV::model *const structure,
					      char message[], const int_g mlen)
{
  structure_file::write(filename, structure, get_c03_indices(), message, mlen);
}

void CRYSTAL::phonon_calc_v8::write_structure(const DLV::string filename,
					      const DLV::model *const structure,
					      char message[], const int_g mlen)
{
  structure_file::write(filename, structure, get_c03_indices(), message, mlen);
}

void CRYSTAL::phonon_data::write_phonons(std::ofstream &output) const
{
  output << "FREQCALC\n";
  output << "STEPSIZE\n";
  output << data.step << '\n';
  if (data.deriv == 1) {
    output << "NUMDERIV\n";
    output << "2\n";
  }
  if (data.intensity)
    output << "INTENS\n";
  output << "DLVOUT\n";
  output << "END\n";
}

void CRYSTAL::phonon_data::write_phonons_v7(std::ofstream &output,
					    const int_g model_type) const
{
  if (data.dispersion and model_type > 0) {
    output << "SCELPHON\n";
    output << data.cell[0][0];
    if (model_type > 1) {
      output << " " << data.cell[0][1];
      if (model_type > 2)
	output << " " << data.cell[0][2];
    }
    output << '\n';
    if (model_type > 1) {
      output << data.cell[1][0] << " " << data.cell[1][1];
      if (model_type > 2)
	output << " " << data.cell[1][2];
      output << '\n';
    }
    if (model_type > 2) {
      output << data.cell[2][0] << " ";
      output << data.cell[2][1] << " ";
      output << data.cell[2][2] << '\n';
    }
  }
  output << "FREQCALC\n";
  output << "STEPSIZE\n";
  output << data.step << '\n';
  if (data.deriv == 1) {
    output << "NUMDERIV\n";
    output << "2\n";
  }
  if (data.intensity)
    output << "INTENS\n";
  if (data.dispersion and model_type > 0)
    output << "DISPERSI\n";
  output << "DLVOUT\n";
  output << "END\n";
}

void CRYSTAL::phonon_data_v6::write_geom_commands(std::ofstream &output,
						  const DLV::model *const) const
{
  write_phonons(output);
}

void CRYSTAL::phonon_data_v7::write_geom_commands(std::ofstream &output,
				    const DLV::model *const structure) const
{
  write_phonons_v7(output, structure->get_model_type());
}

void CRYSTAL::phonon_data_v8::write_geom_commands(std::ofstream &output,
				    const DLV::model *const structure) const
{
  write_phonons_v7(output, structure->get_model_type());
}

bool CRYSTAL::phonon_calc_v6::recover(const bool no_err, const bool log_ok,
				      char message[], const int_g len)
{
  DLV::string id = get_job_id();
  DLV::data_object *data = 0;
  if (log_ok) {
    // logfile
    DLV::string name = get_log_filename();
    if (no_err)
      data = new DLV::text_file(name, program_name, id,
				"Phonons output file");
    else
      data = new DLV::text_file(name, program_name, id,
				"Phonons(failed) output file");
    attach_data(data);
  }
  if (no_err) {
    // internal data logging
    DLV::string name = get_outfile_name(1);
    if (no_err)
      data = new DLV::text_file(name, program_name, id, "Phonons log file");
    else
      data = new DLV::text_file(name, program_name, id,
				"Phonons(failed) log file");
    attach_data(data);
    // phonons
    DLV::data_object *data = read_data(this, get_outfile_name(2).c_str(), id,
				       message, len);
    if (data == 0)
      return false;
    else
      attach_data(data);
  }
  return true;
}

bool CRYSTAL::phonon_calc_v7::recover(const bool no_err, const bool log_ok,
				      char message[], const int_g len)
{
  DLV::string id = get_job_id();
  DLV::data_object *data = 0;
  if (log_ok) {
    // logfile
    DLV::string name = get_log_filename();
    if (no_err)
      data = new DLV::text_file(name, program_name, id,
				"Phonons output file");
    else
      data = new DLV::text_file(name, program_name, id,
				"Phonons(failed) output file");
    attach_data(data);
  }
  if (no_err) {
    // internal data logging
    DLV::string name = get_outfile_name(1);
    if (no_err)
      data = new DLV::text_file(name, program_name, id, "Phonons log file");
    else
      data = new DLV::text_file(name, program_name, id,
				"Phonons(failed) log file");
    attach_data(data);
    // phonons
    DLV::data_object *data = read_data(this, get_outfile_name(2).c_str(), id,
				       message, len);
    if (data == 0)
      return false;
    else
      attach_data(data);
  }
  return true;
}

bool CRYSTAL::phonon_calc_v8::recover(const bool no_err, const bool log_ok,
				      char message[], const int_g len)
{
  DLV::string id = get_job_id();
  DLV::data_object *data = 0;
  if (log_ok) {
    // logfile
    DLV::string name = get_log_filename();
    if (no_err)
      data = new DLV::text_file(name, program_name, id,
				"Phonons output file");
    else
      data = new DLV::text_file(name, program_name, id,
				"Phonons(failed) output file");
    attach_data(data);
  }
  if (no_err) {
    // internal data logging
    DLV::string name = get_outfile_name(1);
    if (no_err)
      data = new DLV::text_file(name, program_name, id, "Phonons log file");
    else
      data = new DLV::text_file(name, program_name, id,
				"Phonons(failed) log file");
    attach_data(data);
    // phonons
    DLV::data_object *data = read_data(this, get_outfile_name(2).c_str(), id,
				       message, len);
    if (data == 0)
      return false;
    else
      attach_data(data);
  }
  return true;
}

bool CRYSTAL::phonon_calc::reload_data(DLV::data_object *data,
				       char message[], const int_g mlen)
{
  // we store phonon data, so this doesn't need to do anything
  return true;
}

bool CRYSTAL::load_phonon_data::reload_data(DLV::data_object *data,
					    char message[], const int_g mlen)
{
  // we store phonon data, so this doesn't need to do anything
  return true;
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::load_phonon_data *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::load_phonon_data("recover");
    }

  }
}

template <class Archive>
void CRYSTAL::phonon_data::serialize(Archive &ar, const unsigned int version)
{
  ar & data;
}

template <class Archive>
void CRYSTAL::phonon_data_v6::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::scf_data_v6>(*this);
  ar & boost::serialization::base_object<CRYSTAL::phonon_data>(*this);
}

template <class Archive>
void CRYSTAL::phonon_data_v7::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::scf_data_v7>(*this);
  ar & boost::serialization::base_object<CRYSTAL::phonon_data>(*this);
}

template <class Archive>
void CRYSTAL::phonon_data_v8::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::scf_data_v8>(*this);
  ar & boost::serialization::base_object<CRYSTAL::phonon_data>(*this);
}

template <class Archive>
void CRYSTAL::load_phonon_data::serialize(Archive &ar,
					  const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::load_data_op>(*this);
}

template <class Archive>
void CRYSTAL::phonon_calc::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::scf_calc>(*this);
}

template <class Archive>
void CRYSTAL::phonon_calc_v6::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::phonon_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::phonon_data_v6>(*this);
}

template <class Archive>
void CRYSTAL::phonon_calc_v7::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::phonon_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::phonon_data_v7>(*this);
}

template <class Archive>
void CRYSTAL::phonon_calc_v8::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::phonon_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::phonon_data_v8>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::load_phonon_data)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::phonon_calc_v6)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::phonon_calc_v7)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::phonon_calc_v8)

DLV_SUPPRESS_TEMPLATES(DLV::load_data_op)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::scf_data_v6)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::scf_data_v7)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::scf_data_v8)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::scf_calc)

DLV_NORMAL_EXPLICIT(CRYSTAL::load_phonon_data)
DLV_NORMAL_EXPLICIT(CRYSTAL::phonon_calc_v6)
DLV_NORMAL_EXPLICIT(CRYSTAL::phonon_calc_v7)
DLV_NORMAL_EXPLICIT(CRYSTAL::phonon_calc_v8)

#endif // DLV_USES_SERIALIZE
