
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_admin.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/data_atoms.hxx"
#include "calcs.hxx"
#include "props.hxx"
#include "eigvec.hxx"

DLV::operation *CRYSTAL::load_eigenvec_data::create(const char filename[],
						    char message[],
						    const int_g mlen)
{
  load_eigenvec_data *op = new load_eigenvec_data(filename);
  DLV::data_object *data = op->read_data(op, filename, message, mlen);
  if (data == 0) {
    delete op;
    op = 0;
  } else {
    op->attach();
    op->attach_data(data);
  }
  return op;
}

DLV::string CRYSTAL::load_eigenvec_data::get_name() const
{
  return ("Load CRYSTAL Eigenvectors - " + get_filename());
}

DLV::data_object *CRYSTAL::eigenvec_file::read_data(DLV::operation *op,
						    const char filename[],
						    char message[],
						    const int_g mlen)
{
  // Todo
  return 0;
}

bool CRYSTAL::load_eigenvec_data::reload_data(DLV::data_object *data,
					      char message[], const int_g mlen)
{
  strncpy(message, "Todo - CRYSTAL eigenvecs reload not done", mlen);
  return false;
}

#ifdef DLV_USES_SERIALIZE

template <class Archive>
void CRYSTAL::load_eigenvec_data::serialize(Archive &ar,
					    const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::load_data_op>(*this);
}

DLV_SUPPRESS_TEMPLATES(DLV::load_data_op)

#endif // DLV_USES_SERIALIZE
