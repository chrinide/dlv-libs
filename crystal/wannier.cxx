
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_admin.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_surf.hxx"
#include "../dlv/data_vol.hxx"
#include "../dlv/job_setup.hxx"
#include "calcs.hxx"
#include "props.hxx"
#include "slices.hxx"
#include "volumes.hxx"
#include "wannier.hxx"
#include "../dlv/op_model.hxx"
#include "wavefn.hxx"

DLV::operation *
CRYSTAL::wannier3D_calc::create(const int_g np, const int_g grid,
				const int_g minb, const int_g maxb,
				const bool sym, const int_g lvec,
				const real_g spacing, const bool use_np,
				const real_g r, const bool auto_grid,
				const Density_Matrix &dm, const NewK &nk,
				const int_g version,
				const DLV::job_setup_data &job,
				const bool extern_job, const char extern_dir[],
				char message[], const int_g mlen)
{
  wannier3D_calc *op = 0;
  message[0] = '\0';
  DLV::box *volume = 0;
  bool error = false;
  if (!auto_grid) {
    DLV::data_object *data = DLV::operation::find_3D_region(grid);
    if (data == 0) {
      strncpy(message, "Failed to find 3D region", mlen);
      error = true;
    } else {
      volume = dynamic_cast<DLV::box *>(data);
      if (volume == 0) {
	strncpy(message, "BUG: selected grid is not 3D region", mlen);
	error = true;
      }
    }
  }
  if (!error) {
    bool parallel = false;
    switch (version) {
    case CRYSTAL06:
    case CRYSTAL09:
      op = new wannier3D_calc_v6(np, volume, minb, maxb, lvec, sym, dm, nk);
      break;
    case CRYSTAL14:
    case CRYSTAL17:
    case CRYSTAL23:
      op = new wannier3D_calc_v8(np, volume, minb, maxb, lvec, sym, spacing,
				 use_np, r, auto_grid, dm, nk);
      break;
    case CRYSTAL_DEV:
      op = new wannier3D_calc_v8(np, volume, minb, maxb, lvec, sym, spacing,
				 use_np, r, auto_grid, dm, nk);
      parallel = job.is_parallel();
      break;
    default:
      strncpy(message, "BUG: unsupported CRYSTAL version", mlen);
      break;
    }
    if (op == 0) {
      if (strlen(message) == 0)
	strncpy(message,
		"Failed to allocate CRYSTAL::wannier3D_calc operation",
		mlen);
    } else {
      op->attach(); // attach to find wavefunction.
      if (op->create_files(parallel, job.is_local(), message, mlen))
	if (op->make_directory(message, mlen))
	  op->write(op->get_infile_name(0), op, op->get_model(),
		    message, mlen);
      if (strlen(message) > 0) {
	// Don't delete once attached
	//delete op;
	//op = 0;
      } else {
	op->execute(op->get_path(), op->get_serial_executable(),
		    op->get_parallel_executable(), "", job, parallel,
		    extern_job, extern_dir, message, mlen);
      }
    }
  }
  return op;
}

DLV::string CRYSTAL::wannier3D_calc::get_name() const
{
  return ("CRYSTAL Wannier 3D calculation");
}

DLV::operation *CRYSTAL::load_wannier3D_data::create(const char filename[],
						     const int_g version,
						     char message[],
						     const int_g mlen)
{
  switch (version) {
  case CRYSTAL06:
  case CRYSTAL09:
  case CRYSTAL14:
  case CRYSTAL17:
  case CRYSTAL23:
  case CRYSTAL_DEV:
    return load_wannier3D_data_v6::create(filename, message, mlen);
  default:
    strncpy(message, "BUG: unsupported CRYSTAL version", mlen);
  }
  return 0;
}

DLV::string CRYSTAL::load_wannier3D_data::get_name() const
{
  return ("Load CRYSTAL Wannier 3D volume - " + get_filename());
}

DLV::operation *CRYSTAL::load_wannier3D_data_v6::create(const char filename[],
							char message[],
							const int_g mlen)
{
  load_wannier3D_data_v6 *op = new load_wannier3D_data_v6(filename);
  DLV::data_object *data = op->read_data(op, filename, filename,
					 message, mlen);
  if (data == 0) {
    delete op;
    op = 0;
  } else {
    op->attach();
    op->attach_data(data);
  }
  return op;
}

void CRYSTAL::wannier3D_calc_v6::add_calc_error_file(const DLV::string tag,
						     const bool is_parallel,
						     const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

void CRYSTAL::wannier3D_calc_v8::add_calc_error_file(const DLV::string tag,
						     const bool is_parallel,
						     const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

bool CRYSTAL::wannier3D_calc::create_files(const bool is_parallel,
					   const bool is_local, char message[],
					   const int_g mlen)
{
  DLV::string filename;
  if (find_wavefunction(filename, binary_wvfn, message, mlen)) {
    static char tag[] = "wan3";
    static char input[] = "inp";
    add_calc_error_file(tag, is_parallel, is_local);
    if (is_parallel)
      add_input_file(0, tag, input, "INPUT", is_local, false);
    else
      add_command_file(0, tag, input, is_local, false);
    if (binary_wvfn)
      add_data_file(1, filename, "fort.9", is_local, true);
    else
      add_data_file(1, filename, "fort.98", is_local, true);
    add_log_file(tag, "out", is_local, true);
    add_sys_error_file(tag, "job", is_local, true);
    add_output_file(0, tag, "dat", "fort.31", is_local, true);
    return true;
  } else
    return false;
}

void CRYSTAL::wannier3D_data::write_input(const DLV::string filename,
					  const DLV::operation *op,
					  const DLV::model *const structure,
					  const bool spin, const bool is_bin,
					  char message[], const int_g mlen)
{
  std::ofstream output;
  if (DLV::open_file_write(output, filename.c_str(), message, mlen)) {
    write_header(output, op, structure, is_bin, true, false, false);
    //int dims = structure->get_number_of_periodic_dims();
    output << "LOCALWF\n";
    write_cmd_data(output);
    output << "NPOINTS\n";
    output << npoints;
    output << '\n';
    write_grid(output);
    write_data_sets(output);
    if (spin) {
      // output same data for other spin channel
      write_cmd_data(output);
      output << "NPOINTS\n";
      output << npoints;
      output << '\n';
      write_grid(output);
      write_data_sets(output);
    }
    output << "END\n";
    output.close();
  }
}

void CRYSTAL::wannier3D_data::write_input(const DLV::string filename,
					  const DLV::operation *op,
					  const DLV::model *const structure,
					  const real_g grid_spacing,
					  const bool set_points,
					  const real_g radius,
					  const bool generate_grid,
					  const bool spin, const bool is_bin,
					  char message[], const int_g mlen)
{
  std::ofstream output;
  if (DLV::open_file_write(output, filename.c_str(), message, mlen)) {
    write_header(output, op, structure, is_bin, true, false, false);
    //int dims = structure->get_number_of_periodic_dims();
    output << "LOCALWF\n";
    write_cmd_data(output);
    if (set_points) {
      output << "NPOINTS\n";
      output << npoints;
    } else {
      output << "STEPSIZE\n";
      output << grid_spacing;
    }
    output << '\n';
    if (generate_grid) {
      output << "RADIUS\n";
      output << radius;
      output << '\n';
    } else
      write_grid(output);
    write_data_sets(output);
    if (spin) {
      // output same data for other spin channel
      write_cmd_data(output);
      if (set_points) {
	output << "NPOINTS\n";
	output << npoints;
      } else {
	output << "STEPSIZE\n";
	output << grid_spacing;
      }
      output << '\n';
      if (generate_grid) {
	output << "RADIUS\n";
	output << grid_spacing;
	output << '\n';
      } else
	write_grid(output);
      write_data_sets(output);
    }
    output << "END\n";
    output.close();
  }
}

void CRYSTAL::wannier3D_data_v8::write_input(const DLV::string filename,
					     const DLV::operation *op,
					     const DLV::model *const structure,
					     const bool spin, const bool is_bin,
					     char message[], const int_g mlen)
{
  wannier3D_data::write_input(filename, op, structure, grid_spacing,
			      set_points, radius, automatic_grid,
			      spin, is_bin, message, mlen);
}

void CRYSTAL::wannier3D_data::write_grid(std::ofstream &output) const
{
  real_g o[3];
  real_g a[3];
  real_g b[3];
  real_g c[3];
  grid->get_points(o, a, b, c);
  output << "ORIGIN\n";
  output.width(10);
  output.precision(5);
  output << o[0];
  output.width(10);
  output.precision(5);
  output << o[1];
  output.width(10);
  output.precision(5);
  output << o[2] << '\n';
  output << "A_AXIS\n";
  output.width(10);
  output.precision(5);
  output << (a[0] - o[0]);
  output.width(10);
  output.precision(5);
  output << (a[1] - o[1]);
  output.width(10);
  output.precision(5);
  output<< (a[2] - o[2]) << '\n';
  output << "B_AXIS\n";
  output.width(10);
  output.precision(5);
  output << (b[0] - o[0]);
  output.width(10);
  output.precision(5);
  output << (b[1] - o[1]);
  output.width(10);
  output.precision(5);
  output << (b[2] - o[2]) << '\n';
  output << "C_AXIS\n";
  output.width(10);
  output.precision(5);
  output << (c[0] - o[0]);
  output.width(10);
  output.precision(5);
  output << (c[1] - o[1]);
  output.width(10);
  output.precision(5);
  output << (c[2] - o[2]) << '\n';
}

void CRYSTAL::wannier3D_data::write_printplo(std::ofstream &output,
					     const int_g g) const
{
  output << "PRINTPLO\n";
  output << "0 0 0 " << -g << '\n';
}

void CRYSTAL::wannier3D_data_v8::write_printplo(std::ofstream &output,
						const int_g g) const
{
  output << "PRINTPLO\n";
  output << "0 0 0 " << -g << '\n';
  output << "0 0.0 0 0\n";
}

void CRYSTAL::wannier3D_data::write_cmd_data(std::ofstream &output) const
{
  output << "INIFIBND\n";
  output.precision(5);
  output << min_band << ' ';
  output.precision(5);
  output << max_band << '\n';
  write_printplo(output, gstars);
  if (symmetrize) {
    output << "SYMMWANF\n";
    output << "END\n";
  }
  output << "END\n";
}

void CRYSTAL::wannier3D_data::write_data_sets(std::ofstream &output) const
{
  output << "AMPLITUDE\n";
  output << "END\n";
}

void CRYSTAL::wannier3D_calc_v6::write(const DLV::string filename,
				       const DLV::operation *op,
				       const DLV::model *const structure,
				       char message[], const int_g mlen)
{
  DLV::operation *calc = find_parent(prim_atom_label, program_name);
  bool spin = false;
  if (calc != 0) {
    wavefn_calc *wfn = dynamic_cast<CRYSTAL::wavefn_calc *>(calc);
    if (wfn != 0)
      spin = wfn->has_spin();
  }
  write_input(filename, op, structure, spin, is_binary(), message, mlen);
}

void CRYSTAL::wannier3D_calc_v8::write(const DLV::string filename,
				       const DLV::operation *op,
				       const DLV::model *const structure,
				       char message[], const int_g mlen)
{
  DLV::operation *calc = find_parent(prim_atom_label, program_name);
  bool spin = false;
  if (calc != 0) {
    wavefn_calc *wfn = dynamic_cast<CRYSTAL::wavefn_calc *>(calc);
    if (wfn != 0)
      spin = wfn->has_spin();
  }
  write_input(filename, op, structure, spin, is_binary(), message, mlen);
}

DLV::volume_data *CRYSTAL::wannier3D_file::create_volume(const char code[],
							 const DLV::string id,
							 DLV::operation *op,
							 const int_g nx,
							 const int_g ny,
							 const int_g nz,
							 const real_g origin[3],
							 const real_g astep[3],
							 const real_g bstep[3],
						   const real_g cstep[3]) const
{
  return new DLV::rspace_wavefunction("CRYSTAL", id, op, nx, ny, nz,
				      origin, astep, bstep, cstep);
}

bool CRYSTAL::wannier3D_file::get_data(std::ifstream &input,
				       DLV::volume_data *data,
				       DLV::operation *op, const int_g nx,
				       const int_g ny, const int_g nz,
				       const char label[], char message[],
				       const int_g mlen)
{
  char line[128];
  input.getline(line, 128);
  char buff[128];
  int_g vec;
  process_label(line, buff, vec);
  bool get_data = true;
  bool ok = true;
  while (get_data) {
    if ((ok = read_wvfn(input, data, nx, ny, nz, buff, message, mlen))) {
      // Check for next data set - This will be the label
      input.getline(line, 128);
      if (input.eof())
	get_data = false;
      else
	process_label(line, buff, vec);
    } else
      get_data = false;
  }
  return ok;
}

bool CRYSTAL::wannier3D_calc_v6::recover(const bool no_err, const bool log_ok,
					 char message[], const int_g len)
{
  DLV::string id = get_job_id();
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi())
      read_logfile(name, id);
    DLV::data_object *file = 0;
    if (no_err)
      file = new DLV::text_file(name, program_name, id,
				"Wannier3D output file");
    else
      file = new DLV::text_file(name, program_name, id,
				"Wannier3D(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    DLV::data_object *data = read_data(this, get_outfile_name(0).c_str(),
				       id, message, len);
    if (data == 0)
      return false;
    else
      attach_data(data);
  }
  return true;
}

bool CRYSTAL::wannier3D_calc_v8::recover(const bool no_err, const bool log_ok,
					 char message[], const int_g len)
{
  DLV::string id = get_job_id();
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi())
      read_logfile(name, id);
    DLV::data_object *file = 0;
    if (no_err)
      file = new DLV::text_file(name, program_name, id,
				"Wannier3D output file");
    else
      file = new DLV::text_file(name, program_name, id,
				"Wannier3D(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    DLV::data_object *data = read_data(this, get_outfile_name(0).c_str(),
				       id, message, len);
    if (data == 0)
      return false;
    else
      attach_data(data);
  }
  return true;
}

bool CRYSTAL::load_wannier3D_data_v6::reload_data(DLV::data_object *data,
						  char message[],
						  const int_g mlen)
{
  DLV::volume_data *v = dynamic_cast<DLV::volume_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload Bloch function", mlen);
    return false;
  } else
    return read_data(this, v, get_filename().c_str(), message, mlen);
}

bool CRYSTAL::wannier3D_calc_v6::reload_data(DLV::data_object *data,
					     char message[], const int_g mlen)
{
  DLV::volume_data *v = dynamic_cast<DLV::volume_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload Bloch function", mlen);
    return false;
  } else
    return read_data(this, v, get_outfile_name(0).c_str(), message, mlen);
}

bool CRYSTAL::wannier3D_calc_v8::reload_data(DLV::data_object *data,
					     char message[], const int_g mlen)
{
  DLV::volume_data *v = dynamic_cast<DLV::volume_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload Bloch function", mlen);
    return false;
  } else
    return read_data(this, v, get_outfile_name(0).c_str(), message, mlen);
}

DLV::operation *
CRYSTAL::wannier2D_calc::create(const int_g np, const int_g grid,
				const int_g minb, const int_g maxb,
				const bool sym, const int_g lvec,
				const Density_Matrix &dm,
				const NewK &nk, const int_g version,
				const DLV::job_setup_data &job,
				const bool extern_job, const char extern_dir[],
				char message[], const int_g mlen)
{
  wannier2D_calc *op = 0;
  message[0] = '\0';
  DLV::data_object *data = DLV::operation::find_plane(grid);
  if (data == 0)
    strncpy(message, "Failed to find plane", mlen);
  else {
    DLV::plane *surface = dynamic_cast<DLV::plane *>(data);
    if (surface == 0)
      strncpy(message, "BUG: selected grid is not a plane", mlen);
    else {
      bool parallel = false;
      switch (version) {
      case CRYSTAL06:
      case CRYSTAL09:
      case CRYSTAL14:
      case CRYSTAL17:
      case CRYSTAL23:
	op = new wannier2D_calc_v6(np, surface, minb, maxb, lvec, sym, dm, nk);
	break;
      case CRYSTAL_DEV:
	// Todo - is parallel sensible?
	op = new wannier2D_calc_v6(np, surface, minb, maxb, lvec, sym, dm, nk);
	parallel = job.is_parallel();
	break;
      default:
	strncpy(message, "BUG: unsupported CRYSTAL version", mlen);
	break;
      }
      if (op == 0) {
	if (strlen(message) == 0)
	  strncpy(message,
		  "Failed to allocate CRYSTAL::wannier2D_calc operation",
		  mlen);
      } else {
	op->attach(); // attach to find wavefunction.
	if (op->create_files(parallel, job.is_local(), message, mlen))
	  if (op->make_directory(message, mlen))
	    op->write(op->get_infile_name(0), op, op->get_model(),
		      message, mlen);
	if (strlen(message) > 0) {
	  delete op;
	  op = 0;
	} else {
	  op->execute(op->get_path(), op->get_serial_executable(),
		      op->get_parallel_executable(), "", job, parallel,
		      extern_job, extern_dir, message, mlen);
	}
      }
    }
  }
  return op;
}

DLV::string CRYSTAL::wannier2D_calc::get_name() const
{
  return ("CRYSTAL Wannier 2D calculation");
}

DLV::operation *CRYSTAL::load_wannier2D_data::create(const char filename[],
						     const int_g version,
						     char message[],
						     const int_g mlen)
{
  switch (version) {
  case CRYSTAL06:
  case CRYSTAL09:
  case CRYSTAL14:
  case CRYSTAL17:
  case CRYSTAL23:
  case CRYSTAL_DEV:
    return load_wannier2D_data_v6::create(filename, message, mlen);
  default:
    strncpy(message, "BUG: unsupported CRYSTAL version", mlen);
  }
  return 0;
}

DLV::string CRYSTAL::load_wannier2D_data::get_name() const
{
  return ("Load CRYSTAL Wannier 2D slice - " + get_filename());
}

DLV::operation *CRYSTAL::load_wannier2D_data_v6::create(const char filename[],
							char message[],
							const int_g mlen)
{
  load_wannier2D_data_v6 *op = new load_wannier2D_data_v6(filename);
  DLV::data_object *data = op->read_data(op, 0, filename, filename, "Wannier",
					 0, message, mlen, false);
  if (data == 0) {
    delete op;
    op = 0;
  } else {
    op->attach();
    op->attach_data(data);
  }
  return op;
}

void CRYSTAL::wannier2D_calc_v6::add_calc_error_file(const DLV::string tag,
						     const bool is_parallel,
						     const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

bool CRYSTAL::wannier2D_calc::create_files(const bool is_parallel,
					   const bool is_local, char message[],
					   const int_g mlen)
{
  DLV::string filename;
  if (find_wavefunction(filename, binary_wvfn, message, mlen)) {
    static char tag[] = "wan2";
    static char input[] = "inp";
    add_calc_error_file(tag, is_parallel, is_local);
    if (is_parallel)
      add_input_file(0, tag, input, "INPUT", is_local, false);
    else
      add_command_file(0, tag, input, is_local, false);
    if (binary_wvfn)
      add_data_file(1, filename, "fort.9", is_local, true);
    else
      add_data_file(1, filename, "fort.98", is_local, true);
    add_log_file(tag, "out", is_local, true);
    add_sys_error_file(tag, "job", is_local, true);
    add_output_file(0, tag, "dat", "fort.25", is_local, true);
    return true;
  } else
    return false;
}

void CRYSTAL::wannier2D_data::write_input(const DLV::string filename,
					  const DLV::operation *op,
					  const DLV::model *const structure,
					  const bool spin, const bool is_bin,
					  char message[], const int_g mlen)
{
  std::ofstream output;
  if (DLV::open_file_write(output, filename.c_str(), message, mlen)) {
    write_header(output, op, structure, is_bin, true, false, false);
    //int dims = structure->get_number_of_periodic_dims();
    output << "LOCALWF\n";
    write_cmd_data(output);
    int_g n = max_band - min_band + 1;
    output.width(5);
    output << n << '\n';
    for (int_g i = 1; i <= n; i++) {
      output << i << '\n';
      write_mapnet(output, grid, npoints);
    }
    if (spin) {
      // output same data for other spin channel
      write_cmd_data(output);
      output.width(5);
      output << n << '\n';
      for (int_g i = 1; i <= n; i++) {
	output << i << '\n';
	write_mapnet(output, grid, npoints);
      }
    }
    output << "END\n";
    output.close();
  }
}

void CRYSTAL::wannier2D_data::write_cmd_data(std::ofstream &output) const
{
  output << "INIFIBND\n";
  output.precision(5);
  output << min_band << ' ';
  output.precision(5);
  output << max_band << '\n';
  output << "PRINTPLO\n";
  output << "0 0 0 " << gstars << '\n';
  if (symmetrize) {
    output << "SYMMWANF\n";
    output << "END\n";
  }
  output << "END\n";
}

void CRYSTAL::wannier2D_calc_v6::write(const DLV::string filename,
				       const DLV::operation *op,
				       const DLV::model *const structure,
				       char message[], const int_g mlen)
{
  DLV::operation *calc = find_parent(wavefn_label, program_name);
  bool spin = false;
  if (calc != 0) {
    wavefn_calc *wfn = dynamic_cast<CRYSTAL::wavefn_calc *>(calc);
    if (wfn != 0)
      spin = wfn->has_spin();
  }
  write_input(filename, op, structure, spin, is_binary(), message, mlen);
}

bool CRYSTAL::wannier2D_file::get_data(std::ifstream &input,
				       DLV::surface_data *data, const int_g nx,
				       const int_g ny, const bool reverse,
				       const char label[],
				       const int_g max_datasets,
				       char message[], const int_g mlen)
{
  int_g na;
  input >> na;
  int_g np;
  input >> np;
  // clear line
  //input.getline(line, 128);
  char buff[128];
  bool ok = true;
  bool get_data = true;
  int_g count = 1;
  while (get_data) {
    if (max_datasets > 0 and count > max_datasets)
      snprintf(buff, 128, "%s amplitude beta - %1d", label,
	       count - max_datasets);
    else
      snprintf(buff, 128, "%s amplitude - %1d", label, count);
    if ((ok = read_data(input, data, nx, ny, reverse, buff, message, mlen))) {
      // Check for next data
      // skip atom info
      for (int_g i = 0; i < (na + np); i++)
	input.getline(buff, 128);
      // Check for 1 more line - if there then data exists.
      input.getline(buff, 128);
      if (input.eof()) {
	fprintf(stderr, "EOF\n");
	get_data = false;
      } else {
	// skip extra info
	input.getline(buff, 128);
	input.getline(buff, 128);
	if (max_datasets > 0 and count > max_datasets)
	  snprintf(buff, 128, "%s magnitude beta - %1d", label,
		   count - max_datasets);
	else
	  snprintf(buff, 128, "%s magnitude - %1d", label, count);
	if ((ok = read_data(input, data, nx, ny, reverse, buff,
			    message, mlen))) {
	  count++;
	  // Check for next data
	  // skip atom info
	  for (int_g i = 0; i < (na + np); i++)
	    input.getline(buff, 128);
	  // Check for 1 more line - if there then data exists.
	  input.getline(buff, 128);
	  if (input.eof())
	    get_data = false;
	  else {
	    // skip extra info
	    input.getline(buff, 128);
	    input.getline(buff, 128);
	  }
	} else
	  get_data = false;
      }
    } else
      get_data = false;
  }
  return ok;
}

bool CRYSTAL::wannier2D_calc_v6::recover(const bool no_err, const bool log_ok,
					 char message[], const int_g len)
{
  DLV::string id = get_job_id();
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi())
      read_logfile(name, id);
    DLV::data_object *file = 0;
    if (no_err)
      file = new DLV::text_file(name, program_name, id,
				"Wannier2D output file");
    else
      file = new DLV::text_file(name, program_name, id,
				"Wannier2D(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    DLV::data_object *data = read_data(this, 0, get_outfile_name(0).c_str(),
				       id, "Wannier", get_nbands(),
				       message, len, false);
    if (data == 0)
      return false;
    else
      attach_data(data);
  }
  return true;
}

bool CRYSTAL::load_wannier2D_data::reload_data(DLV::data_object *data,
					       char message[], const int_g mlen)
{
  DLV::surface_data *v = dynamic_cast<DLV::surface_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload 2D wannier", mlen);
    return false;
  } else
    return (read_data(this, v, get_filename().c_str(),
		      "", "Wannier", 0, message, mlen, false) != 0);
  return false;
}

bool CRYSTAL::wannier2D_calc_v6::reload_data(DLV::data_object *data,
					     char message[], const int_g mlen)
{
  DLV::surface_data *v = dynamic_cast<DLV::surface_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload 2D wannier", mlen);
    return false;
  } else
    return (read_data(this, v, get_outfile_name(0).c_str(),
		      "", "Wannier", get_nbands(), message, mlen, false) != 0);
  return false;
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    CRYSTAL::load_wannier3D_data_v6 *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::load_wannier3D_data_v6("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::wannier3D_calc_v6 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::wannier3D_calc_v6(0, 0, 0, 0, 0, false, d, n);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::wannier3D_calc_v8 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::wannier3D_calc_v8(0, 0, 0, 0, 0, false,
					 0.0, true, 0.0, true, d, n);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    CRYSTAL::load_wannier2D_data_v6 *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::load_wannier2D_data_v6("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::wannier2D_calc_v6 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::wannier2D_calc_v6(0, 0, 0, 0, 0, false, d, n);
    }

  }
}

template <class Archive>
void CRYSTAL::wannier3D_data::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::property_data_v6>(*this);
  ar & npoints;
  ar & grid;
  ar & min_band;
  ar & max_band;
  ar & gstars;
  ar & symmetrize;
}

template <class Archive>
void CRYSTAL::wannier3D_data_v6::serialize(Archive &ar,
					   const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::wannier3D_data>(*this);
}

template <class Archive>
void CRYSTAL::wannier3D_data_v8::serialize(Archive &ar,
					   const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::wannier3D_data>(*this);
  ar & grid_spacing;
  ar & radius;
  ar & set_points;
  ar & automatic_grid;
}

template <class Archive>
void CRYSTAL::load_wannier3D_data::serialize(Archive &ar,
					     const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::load_data_op>(*this);
}

template <class Archive>
void CRYSTAL::load_wannier3D_data_v6::serialize(Archive &ar,
						const unsigned int version)
{
  ar & boost::serialization::base_object<load_wannier3D_data>(*this);
}

template <class Archive>
void CRYSTAL::wannier3D_calc::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::property_calc>(*this);
}

template <class Archive>
void CRYSTAL::wannier3D_calc_v6::serialize(Archive &ar,
					   const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::wannier3D_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::wannier3D_data_v6>(*this);
}

template <class Archive>
void CRYSTAL::wannier3D_calc_v8::serialize(Archive &ar,
					   const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::wannier3D_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::wannier3D_data_v8>(*this);
}

template <class Archive>
void CRYSTAL::wannier2D_data::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::property_data_v6>(*this);
  ar & npoints;
  ar & grid;
  ar & min_band;
  ar & max_band;
  ar & gstars;
  ar & symmetrize;
}

template <class Archive>
void CRYSTAL::load_wannier2D_data::serialize(Archive &ar,
					     const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::load_data_op>(*this);
}

template <class Archive>
void CRYSTAL::load_wannier2D_data_v6::serialize(Archive &ar,
						const unsigned int version)
{
  ar & boost::serialization::base_object<load_wannier2D_data>(*this);
}

template <class Archive>
void CRYSTAL::wannier2D_calc::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::property_calc>(*this);
}

template <class Archive>
void CRYSTAL::wannier2D_calc_v6::serialize(Archive &ar,
					   const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::wannier2D_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::wannier2D_data>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::load_wannier3D_data_v6)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::wannier3D_calc_v6)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::wannier3D_calc_v8)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::load_wannier2D_data_v6)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::wannier2D_calc_v6)

DLV_SUPPRESS_TEMPLATES(DLV::load_data_op)
DLV_SUPPRESS_TEMPLATES(DLV::plane)
DLV_SUPPRESS_TEMPLATES(DLV::box)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::property_data_v6)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::property_calc)

DLV_NORMAL_EXPLICIT(CRYSTAL::load_wannier3D_data_v6)
DLV_NORMAL_EXPLICIT(CRYSTAL::wannier3D_calc_v6)
DLV_NORMAL_EXPLICIT(CRYSTAL::wannier3D_calc_v8)
DLV_NORMAL_EXPLICIT(CRYSTAL::load_wannier2D_data_v6)
DLV_NORMAL_EXPLICIT(CRYSTAL::wannier2D_calc_v6)

#endif // DLV_USES_SERIALIZE
