
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_model.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/op_admin.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/job_setup.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_atoms.hxx"
#include "../dlv/data_plots.hxx" // TD-DFT
#include "calcs.hxx"
#include "scf.hxx"
#include "scf_impl.hxx"
#include "phonon.hxx"
#include "neb.hxx"
#include "tddft.hxx"

DLV::string CRYSTAL::scf_calc::serial_binary = "crystal";
DLV::string CRYSTAL::scf_calc::parallel_binary = "Pcrystal";
DLV::string CRYSTAL::scf_calc::mpp_binary = "MPPcrystal";

DLV::operation *CRYSTAL::scf_calc::create(const int_g t, const int_g version,
					  char message[], const int_g mlen)
{
  scf_calc *op = 0;
  message[0] = '\0';
  switch (version) {
  case CRYSTAL98:
    switch (t) {
    case 0:
      op = new scf_calc_v4();
      break;
    default:
      strncpy(message, "BUG: CRYSTAL SCF task not supported", mlen);
      break;
    }
    break;
  case CRYSTAL03:
    switch (t) {
    case 0:
      op = new scf_calc_v5();
      break;
    case 1:
      op = new opt_calc_v5();
      break;
    default:
      strncpy(message, "BUG: CRYSTAL SCF task not supported", mlen);
      break;
    }
    break;
  case CRYSTAL06:
    switch (t) {
    case 0:
      op = new scf_calc_v6();
      break;
    case 1:
      op = new opt_calc_v6();
      break;
    case 2:
      op = new phonon_calc_v6();
      break;
    default:
      strncpy(message, "BUG: CRYSTAL SCF task not supported", mlen);
      break;
    }
    break;
  case CRYSTAL09:
    switch (t) {
    case 0:
      op = new scf_calc_v7();
      break;
    case 1:
      op = new opt_calc_v7();
      break;
    case 2:
      op = new phonon_calc_v7();
      break;
    case 4:
      op = new cphf_calc_v7();
      break;
    default:
      strncpy(message, "BUG: CRYSTAL SCF task not supported", mlen);
      break;
    }
    break;
  case CRYSTAL14:
  case CRYSTAL17:
  case CRYSTAL23:
  case CRYSTAL_DEV:
    switch (t) {
    case 0:
      op = new scf_calc_v8();
      break;
    case 1:
      op = new opt_calc_v8();
      break;
    case 2:
      op = new phonon_calc_v8();
      break;
    case 3:
      op = new neb_calc_v8_final();
      break;
    case 4:
      op = new cphf_calc_v8();
      break;
    case 5:
      op = new tddft_calc_v8();
      break;
    default:
      strncpy(message, "BUG: CRYSTAL SCF task not supported", mlen);
      break;
    }
    break;
  default:
    strncpy(message, "BUG: unknown CRYSTAL version", mlen);
    break;
  }
  if (op == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Failed to allocate CRYSTAL::scf_calc operation",
	      mlen);
  } else {
    message[0] = '\0';
    if (t == 1) { // opt
      op->attach_pending_copy_model("CRYSTAL opt");
      // otherwise toggle atom flags isn't available
      //op->render_r(message, mlen);
    }
    else if (t ==3){ // neb
      op->attach_pending_copy_model("CRYSTAL NEB");
    }
    else // scf, phonon cphf, or tddft
      op->attach_pending();
  }
  return op;
}

bool CRYSTAL::scf_calc::copy_basis(char message[], const int_g mlen)
{
  return false;
  /*DLV::operation *base = get_editing();
  scf_calc *op = dynamic_cast<scf_calc *>(base);
  if (op == 0) {
    strncpy(message, "BUG: object is not a SCF calculation", mlen);
    return false;
  }
  if (!op->show_basis) {
    op->toggle_atom_flags("SCF atomic basis sets");
    op->show_basis = true;
  }
  message[0] = '\0';
  // Look for an earlier SCF calculation
  operation *ptr = op->get_parent();
  scf_calc *basis = 0;
  What Todo about geometry changes?
  while (ptr != 0) {
    basis = dynamic_cast<scf_calc *>(ptr);
    if (basis != 0)
      break;
    ptr = ptr->get_parent();
  }
  if (basis == 0) {
    strncpy(message, "No basis found to inherit", mlen);
    return false;
  }
  bool ok = true;
  ok = op->copy_basis_sets();
  return ok;*/
}

bool CRYSTAL::scf_calc::add_basis(const char filename[],
				  const bool set_default, char message[],
				  const int_g mlen)
{
  DLV::operation *base = get_editing();
  scf_calc *op = dynamic_cast<scf_calc *>(base);
  if (op == 0) {
    strncpy(message, "BUG: object is not a SCF calculation", mlen);
    return false;
  }
  if (!op->show_basis) {
#ifdef ENABLE_DLV_GRAPHICS
    op->toggle_atom_flags("SCF atomic basis sets");
#endif // ENABLE_DLV_GRAPHICS
    op->show_basis = true;
  }
  message[0] = '\0';
  bool ok = true;
  int_g *indices = 0;
  int_g value = -1;
  ok = op->add_basis_set(filename, set_default, indices, value, message, mlen);
#ifdef ENABLE_DLV_GRAPHICS
  if (ok) {
    ok = op->set_atom_index_and_flags(indices, value, set_default,
				      message, mlen);
    if (ok)
      //op->set_selected_atom_flags(true);
      (void) op->deselect_atoms(message, mlen);
  }
#endif // ENABLE_DLV_GRAPHICS
  return ok;
}

bool CRYSTAL::scf_calc_v4::add_basis_set(const char filename[],
					 const bool set_default,
					 int_g * &indices, int_g &value,
					 char message[], const int_g mlen)
{
  return scf_data::add_basis(filename, set_default, get_model(), indices,
			     value, message, mlen);
}

bool CRYSTAL::scf_calc_v5::add_basis_set(const char filename[],
					 const bool set_default,
					 int_g * &indices, int_g &value,
					 char message[], const int_g mlen)
{
  return scf_data::add_basis(filename, set_default, get_model(), indices,
			     value, message, mlen);
}

bool CRYSTAL::scf_calc_v6::add_basis_set(const char filename[],
					 const bool set_default,
					 int_g * &indices, int_g &value,
					 char message[], const int_g mlen)
{
  return scf_data::add_basis(filename, set_default, get_model(), indices,
			     value, message, mlen);
}

bool CRYSTAL::scf_calc_v7::add_basis_set(const char filename[],
					 const bool set_default,
					 int_g * &indices, int_g &value,
					 char message[], const int_g mlen)
{
  return scf_data::add_basis(filename, set_default, get_model(), indices,
			     value, message, mlen);
}

bool CRYSTAL::scf_calc_v8::add_basis_set(const char filename[],
					 const bool set_default,
					 int_g * &indices, int_g &value,
					 char message[], const int_g mlen)
{
  return scf_data::add_basis(filename, set_default, get_model(), indices,
			     value, message, mlen);
}

bool CRYSTAL::cphf_calc_v7::add_basis_set(const char filename[],
					  const bool set_default,
					  int_g * &indices, int_g &value,
					  char message[], const int_g mlen)
{
  return scf_data::add_basis(filename, set_default, get_model(), indices,
			     value, message, mlen);
}

bool CRYSTAL::cphf_calc_v8::add_basis_set(const char filename[],
					  const bool set_default,
					  int_g * &indices, int_g &value,
					  char message[], const int_g mlen)
{
  return scf_data::add_basis(filename, set_default, get_model(), indices,
			     value, message, mlen);
}

bool CRYSTAL::opt_calc_v5::add_basis_set(const char filename[],
					 const bool set_default,
					 int_g * &indices, int_g &value,
					 char message[], const int_g mlen)
{
  return scf_data::add_basis(filename, set_default, get_model(), indices,
			     value, message, mlen);
}

bool CRYSTAL::opt_calc_v6::add_basis_set(const char filename[],
					 const bool set_default,
					 int_g * &indices, int_g &value,
					 char message[], const int_g mlen)
{
  return scf_data::add_basis(filename, set_default, get_model(), indices,
			     value, message, mlen);
}

bool CRYSTAL::opt_calc_v7::add_basis_set(const char filename[],
					 const bool set_default,
					 int_g * &indices, int_g &value,
					 char message[], const int_g mlen)
{
  return scf_data::add_basis(filename, set_default, get_model(), indices,
			     value, message, mlen);
}

bool CRYSTAL::opt_calc_v8::add_basis_set(const char filename[],
					 const bool set_default,
					 int_g * &indices, int_g &value,
					 char message[], const int_g mlen)
{
  return scf_data::add_basis(filename, set_default, get_model(), indices,
			     value, message, mlen);
}

bool CRYSTAL::scf_data::add_basis(const char filename[],
				  const bool set_default,
				  const DLV::model *const m,
				  int_g * &indices, int_g &value,
				  char message[], const int_g mlen)
{
  // Todo - set_default seems to be unused.
  if (basis_indices == 0) {
    natoms = m->get_number_of_asym_atoms();
    basis_indices = new int_g[natoms];
    for (int_g i = 0; i < natoms; i++)
      basis_indices[i] = -1;
  }
  bool pseudo = false;
  std::ifstream input;
  if (DLV::open_file_read(input, filename, message, mlen)) {
    char buff[128];
    input.getline(buff, 128);
    input.getline(buff, 128);
    char text[128];
    sscanf(buff, "%s", text);
    if (strcmp(text, "HAYWLC") == 0 or strcmp(text, "HAYWSC") == 0 or
	strcmp(text, "BARTHE") == 0 or strcmp(text, "DURAND") == 0 or
	strcmp(text, "INPUT") == 0)
      pseudo = true;
    input.close();
    basis_info data(filename, pseudo);
    value = (int)atom_bases.size();
    atom_bases.push_back(data);
    indices = basis_indices;
    return true;
  } else {
    strncpy(message, "Unable to find basis set file", mlen);
    return false;
  }
}

bool CRYSTAL::scf_calc::calculate(const bool a, const Hamiltonian &h,
				  const Basis &b, const Kpoints &k,
				  const Tolerances &tol, const Convergence &c,
				  const Print &p, const Joboptions &j,
				  const Optimise &opt, const Phonon &ph,
				  const Neb &n, const CPHF &cphf,
				  const TDDFT &tddft, const int_g version,
				  const DLV::job_setup_data &job,
				  const bool extern_job,
				  const char extern_dir[], const bool mpp,
				  char message[], const int_g mlen)
{
  // I could do this via a virtual in DLV::operation, but I don't want to
  // have to alter CCP3core to add CCP3calc options.
  DLV::operation *base = get_editing();
  scf_calc *op = dynamic_cast<scf_calc *>(base);
  if (op == 0) {
    strncpy(message, "BUG: object is not an SCF calculation", mlen);
    return false;
  }
  message[0] = '\0';
  bool parallel = false;
  bool use_mpp = false;
  if (version > CRYSTAL03) {
    use_mpp = mpp;
    if (version == CRYSTAL_DEV)
      parallel = job.is_parallel();
  }
  op->set_params(h, b, k, tol, c, p, j, opt, ph, n, cphf, tddft, a);
  (void) accept_pending();
  if (op->create_files(parallel, job.is_local(), message, mlen)) {
    if (op->make_directory(message, mlen)) {
      if (op->write(op->get_infile_name(0), op->get_model(), message, mlen)) {
	op->write_structure(op->get_infile_name(1), op->get_model(),
			    message, mlen);
	op->write_2nd_structure(message, mlen);
      }
    }
  }
  // This will trap warnings about a structure file with too many ops
  if (strlen(message) == 0) {
    op->execute(op->get_path(), op->get_serial_executable(),
		op->get_parallel_executable(use_mpp), "", job, true,
		extern_job, extern_dir, message, mlen);
    return true;
  } else
    return false;
}

DLV::string CRYSTAL::scf_calc::get_name() const
{
  return ("CRYSTAL SCF calculation");
}

DLV::string CRYSTAL::opt_calc::get_name() const
{
  return ("CRYSTAL structure optimisation calculation");
}

DLV::string CRYSTAL::cphf_calc::get_name() const
{
  return ("CRYSTAL CPHF/KS calculation");
}

void CRYSTAL::scf_data::set_params(const Hamiltonian &h, const Basis &b,
				   const Kpoints &k, const Tolerances &tol,
				   const Convergence &c, const Print &p,
				   const Joboptions &j, const bool a)
{
  hamiltonian = h;
  basis = b;
  kpoints = k;
  tolerances = tol;
  converge = c;
  print = p;
  options = j;
  analyse = a;
}

void CRYSTAL::scf_calc_v4::set_params(const Hamiltonian &h, const Basis &b,
				      const Kpoints &k, const Tolerances &tol,
				      const Convergence &c, const Print &p,
				      const Joboptions &j, const Optimise &opt,
				      const Phonon &ph, const Neb &n,
				      const CPHF &, const TDDFT &, const bool a)
{
  scf_data::set_params(h, b, k, tol, c, p, j, a);
}

void CRYSTAL::scf_calc_v5::set_params(const Hamiltonian &h, const Basis &b,
				      const Kpoints &k, const Tolerances &tol,
				      const Convergence &c, const Print &p,
				      const Joboptions &j, const Optimise &opt,
				      const Phonon &ph, const Neb &n,
				      const CPHF &, const TDDFT &, const bool a)
{
  scf_data::set_params(h, b, k, tol, c, p, j, a);
}

void CRYSTAL::scf_calc_v6::set_params(const Hamiltonian &h, const Basis &b,
				      const Kpoints &k, const Tolerances &tol,
				      const Convergence &c, const Print &p,
				      const Joboptions &j, const Optimise &opt,
				      const Phonon &ph, const Neb &n,
				      const CPHF &, const TDDFT &, const bool a)
{
  Hamiltonian hx = h;
  if (h.functional == Hamiltonian::pbesol0)
    hx.functional = Hamiltonian::soggaxc;
  scf_data::set_params(hx, b, k, tol, c, p, j, a);
}

void CRYSTAL::scf_calc_v7::set_params(const Hamiltonian &h, const Basis &b,
				      const Kpoints &k, const Tolerances &tol,
				      const Convergence &c, const Print &p,
				      const Joboptions &j, const Optimise &opt,
				      const Phonon &ph, const Neb &n,
				      const CPHF &, const TDDFT &, const bool a)
{
  scf_data::set_params(h, b, k, tol, c, p, j, a);
}

void CRYSTAL::scf_calc_v8::set_params(const Hamiltonian &h, const Basis &b,
				      const Kpoints &k, const Tolerances &tol,
				      const Convergence &c, const Print &p,
				      const Joboptions &j, const Optimise &opt,
				      const Phonon &ph, const Neb &n,
				      const CPHF &, const TDDFT &, const bool a)
{
  scf_data::set_params(h, b, k, tol, c, p, j, a);
}

void CRYSTAL::cphf_data::set_params(const CPHF &cp)
{
  cphf = cp;
}

void CRYSTAL::cphf_calc_v7::set_params(const Hamiltonian &h, const Basis &b,
				       const Kpoints &k, const Tolerances &tol,
				       const Convergence &c, const Print &p,
				       const Joboptions &j, const Optimise &opt,
				       const Phonon &ph, const Neb &n,
				       const CPHF &cp, const TDDFT &,
				       const bool a)
{
  scf_data::set_params(h, b, k, tol, c, p, j, a);
  cphf_data::set_params(cp);
}

void CRYSTAL::cphf_calc_v8::set_params(const Hamiltonian &h, const Basis &b,
				       const Kpoints &k, const Tolerances &tol,
				       const Convergence &c, const Print &p,
				       const Joboptions &j, const Optimise &opt,
				       const Phonon &ph, const Neb &n,
				       const CPHF &cp, const TDDFT &,
				       const bool a)
{
  scf_data::set_params(h, b, k, tol, c, p, j, a);
  cphf_data::set_params(cp);
}

void CRYSTAL::opt_data::set_params(const Optimise &opt)
{
  optimise = opt;
}

void CRYSTAL::opt_calc_v5::set_params(const Hamiltonian &h, const Basis &b,
				      const Kpoints &k, const Tolerances &tol,
				      const Convergence &c, const Print &p,
				      const Joboptions &j, const Optimise &opt,
				      const Phonon &ph, const Neb &n,
				      const CPHF &, const TDDFT &, const bool a)
{
  scf_data::set_params(h, b, k, tol, c, p, j, a);
  opt_data::set_params(opt);
}

void CRYSTAL::opt_calc_v6::set_params(const Hamiltonian &h, const Basis &b,
				      const Kpoints &k, const Tolerances &tol,
				      const Convergence &c, const Print &p,
				      const Joboptions &j, const Optimise &opt,
				      const Phonon &ph, const Neb &n,
				      const CPHF &, const TDDFT &, const bool a)
{
  scf_data_v6::set_params(h, b, k, tol, c, p, j, a);
  opt_data::set_params(opt);
}

void CRYSTAL::opt_calc_v7::set_params(const Hamiltonian &h, const Basis &b,
				      const Kpoints &k, const Tolerances &tol,
				      const Convergence &c, const Print &p,
				      const Joboptions &j, const Optimise &opt,
				      const Phonon &ph, const Neb &n,
				      const CPHF &, const TDDFT &, const bool a)
{
  scf_data::set_params(h, b, k, tol, c, p, j, a);
  opt_data::set_params(opt);
}

void CRYSTAL::opt_calc_v8::set_params(const Hamiltonian &h, const Basis &b,
				      const Kpoints &k, const Tolerances &tol,
				      const Convergence &c, const Print &p,
				      const Joboptions &j, const Optimise &opt,
				      const Phonon &ph, const Neb &n,
				      const CPHF &, const TDDFT &, const bool a)
{
  scf_data::set_params(h, b, k, tol, c, p, j, a);
  opt_data::set_params(opt);
}

void CRYSTAL::scf_calc_v6::add_calc_error_file(const DLV::string tag,
					       const bool is_parallel,
					       const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

void CRYSTAL::scf_calc_v7::add_calc_error_file(const DLV::string tag,
					       const bool is_parallel,
					       const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

void CRYSTAL::scf_calc_v8::add_calc_error_file(const DLV::string tag,
					       const bool is_parallel,
					       const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

void CRYSTAL::cphf_calc::add_calc_error_file(const DLV::string tag,
						const bool is_parallel,
						const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

void CRYSTAL::opt_calc_v6::add_calc_error_file(const DLV::string tag,
					       const bool is_parallel,
					       const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

void CRYSTAL::opt_calc_v7::add_calc_error_file(const DLV::string tag,
					       const bool is_parallel,
					       const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

void CRYSTAL::opt_calc_v8::add_calc_error_file(const DLV::string tag,
					       const bool is_parallel,
					       const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

void CRYSTAL::scf_calc_v4::add_restart_file(const DLV::string tag,
					    const bool is_local)
{
  if (has_restart())
    add_data_file(1, restart_filename(), "fort.20", is_local, true);
}

void CRYSTAL::scf_calc_v5::add_restart_file(const DLV::string tag,
					    const bool is_local)
{
  if (has_restart())
    add_data_file(1, restart_filename(), "fort.20", is_local, true);
}

void CRYSTAL::scf_calc_v6::add_restart_file(const DLV::string tag,
					    const bool is_local)
{
  if (has_restart())
    add_data_file(1, restart_filename(), "fort.20", is_local, true);
}

void CRYSTAL::scf_calc_v7::add_restart_file(const DLV::string tag,
					    const bool is_local)
{
  if (has_restart())
    add_data_file(1, restart_filename(), "fort.20", is_local, true);
}

void CRYSTAL::scf_calc_v8::add_restart_file(const DLV::string tag,
					    const bool is_local)
{
  if (has_restart())
    add_data_file(1, restart_filename(), "fort.20", is_local, true);
}

void CRYSTAL::cphf_calc_v7::add_restart_file(const DLV::string tag,
					     const bool is_local)
{
  if (has_restart())
    add_data_file(1, restart_filename(), "fort.20", is_local, true);
}

void CRYSTAL::cphf_calc_v8::add_restart_file(const DLV::string tag,
					     const bool is_local)
{
  if (has_restart())
    add_data_file(1, restart_filename(), "fort.20", is_local, true);
}

void CRYSTAL::opt_calc_v5::add_restart_file(const DLV::string tag,
					    const bool is_local)
{
  if (has_restart())
    add_data_file(1, restart_filename(), "fort.20", is_local, true);
}

void CRYSTAL::opt_calc_v6::add_restart_file(const DLV::string tag,
					    const bool is_local)
{
  if (has_restart())
    add_data_file(1, restart_filename(), "fort.20", is_local, true);
}

void CRYSTAL::opt_calc_v7::add_restart_file(const DLV::string tag,
					    const bool is_local)
{
  if (has_restart())
    add_data_file(1, restart_filename(), "fort.20", is_local, true);
}

void CRYSTAL::opt_calc_v8::add_restart_file(const DLV::string tag,
					    const bool is_local)
{
  if (has_restart())
    add_data_file(1, restart_filename(), "fort.20", is_local, true);
}

void CRYSTAL::scf_calc_v4::add_opt_restart_file(const DLV::string tag,
					     const bool is_local)
{
}
void CRYSTAL::scf_calc_v5::add_opt_restart_file(const DLV::string tag,
					     const bool is_local)
{
}
void CRYSTAL::scf_calc_v6::add_opt_restart_file(const DLV::string tag,
					     const bool is_local)
{
}

void CRYSTAL::scf_calc_v7::add_opt_restart_file(const DLV::string tag,
					     const bool is_local)
{
}

void CRYSTAL::scf_calc_v8::add_opt_restart_file(const DLV::string tag,
					     const bool is_local)
{
}

void CRYSTAL::cphf_calc::add_opt_restart_file(const DLV::string tag,
					      const bool is_local)
{
}

void CRYSTAL::opt_calc_v5::add_opt_restart_file(const DLV::string tag,
					     const bool is_local)
{
}

void CRYSTAL::opt_calc_v6::add_opt_restart_file(const DLV::string tag,
					     const bool is_local)
{
}

void CRYSTAL::opt_calc_v7::add_opt_restart_file(const DLV::string tag,
					     const bool is_local)
{
}

void CRYSTAL::opt_calc_v8::add_opt_restart_file(const DLV::string tag,
					     const bool is_local)
{
}

bool CRYSTAL::scf_calc::common_files(const bool is_parallel,
				     const bool is_local, const char tag[])
{
  static char input[] = "inp";
  add_calc_error_file(tag, is_parallel, is_local);
  if (is_parallel)
    add_input_file(0, tag, input, "INPUT", is_local, false);
  else
    add_command_file(0, tag, input, is_local, false);
  add_input_file(1, tag, "str", "fort.34", is_local, false);
  add_restart_file(tag, is_local);
  add_opt_restart_file(tag, is_local);
  add_log_file(tag, "out", is_local, true);
  add_sys_error_file(tag, "job", is_local, true);
  return true;
}

bool CRYSTAL::scf_calc::create_files(const bool is_parallel,
				     const bool is_local, char message[],
				     const int_g mlen)
{
  static char tag[] = "scf";
  if (common_files(is_parallel, is_local, tag))
    add_output_file(0, tag, "scf", "fort.98", is_local, true);
  else
    return false;
  return true;
}

void CRYSTAL::scf_calc::write_2nd_structure(char message[], const int_g mlen)
{
  return;
}


DLV::string CRYSTAL::opt_data::optimiser_log() const
{
  if (optimise.optimiser == Optimise::berny)
    return "SCFOUT.LOG";
  else
    return "LOG";
}

void CRYSTAL::opt_calc_v5::add_optimiser_log(const DLV::string tag,
					     const bool is_local)
{
  add_output_file(1, tag, "optlog", optimiser_log(), is_local, true);
}

void CRYSTAL::opt_calc_v6::add_optimiser_log(const DLV::string tag,
					     const bool is_local)
{
  add_output_file(1, tag, "optlog", optimiser_log(), is_local, true);
}

void CRYSTAL::opt_calc_v7::add_optimiser_log(const DLV::string tag,
					     const bool is_local)
{
  add_output_file(1, tag, "optlog", optimiser_log(), is_local, true);
}

void CRYSTAL::opt_calc_v8::add_optimiser_log(const DLV::string tag,
					     const bool is_local)
{
  add_output_file(1, tag, "optlog", optimiser_log(), is_local, true);
}

bool CRYSTAL::opt_calc::create_files(const bool is_parallel,
				     const bool is_local, char message[],
				     const int_g mlen)
{
  static char tag[] = "opt";
  if (common_files(is_parallel, is_local, tag)) {
    add_output_file(0, tag, "optstr", "fort.34", is_local, false);
    add_optimiser_log(tag, is_local);
    add_output_file(2, tag, "20", "fort.20", is_local, true);
  } else
    return false;
  return true;
}

bool CRYSTAL::scf_calc_v4::write(const DLV::string filename,
				 const DLV::model *const structure,
				 char message[], const int_g mlen)
{
  return write_input(filename, structure, message, mlen);
}

bool CRYSTAL::scf_calc_v5::write(const DLV::string filename,
				 const DLV::model *const structure,
				 char message[], const int_g mlen)
{
  return write_input(filename, structure, message, mlen);
}

bool CRYSTAL::scf_calc_v6::write(const DLV::string filename,
				 const DLV::model *const structure,
				 char message[], const int_g mlen)
{
  return write_input(filename, structure, message, mlen);
}

bool CRYSTAL::scf_calc_v7::write(const DLV::string filename,
				 const DLV::model *const structure,
				 char message[], const int_g mlen)
{
  return write_input(filename, structure, message, mlen);
}

bool CRYSTAL::scf_calc_v8::write(const DLV::string filename,
				 const DLV::model *const structure,
				 char message[], const int_g mlen)
{
  return write_input(filename, structure, message, mlen);
}

bool CRYSTAL::cphf_calc_v7::write(const DLV::string filename,
				  const DLV::model *const structure,
				  char message[], const int_g mlen)
{
  return write_input(filename, structure, message, mlen);
}

bool CRYSTAL::cphf_calc_v8::write(const DLV::string filename,
				  const DLV::model *const structure,
				  char message[], const int_g mlen)
{
  return write_input(filename, structure, message, mlen);
}

bool CRYSTAL::opt_calc_v5::write(const DLV::string filename,
				 const DLV::model *const structure,
				 char message[], const int_g mlen)
{
  return write_input(filename, structure, message, mlen);
}

bool CRYSTAL::opt_calc_v6::write(const DLV::string filename,
				 const DLV::model *const structure,
				 char message[], const int_g mlen)
{
  return write_input(filename, structure, message, mlen);
}

bool CRYSTAL::opt_calc_v7::write(const DLV::string filename,
				 const DLV::model *const structure,
				 char message[], const int_g mlen)
{
  return write_input(filename, structure, message, mlen);
}

bool CRYSTAL::opt_calc_v8::write(const DLV::string filename,
				 const DLV::model *const structure,
				 char message[], const int_g mlen)
{
  return write_input(filename, structure, message, mlen);
}

void CRYSTAL::scf_calc_v4::write_structure(const DLV::string filename,
					   const DLV::model *const structure,
					   char message[], const int_g mlen)
{
  structure_file::write(filename, structure, get_c03_indices(), message, mlen);
}

void CRYSTAL::scf_calc_v5::write_structure(const DLV::string filename,
					   const DLV::model *const structure,
					   char message[], const int_g mlen)
{
  structure_file::write(filename, structure, get_c03_indices(), message, mlen);
}

void CRYSTAL::scf_calc_v6::write_structure(const DLV::string filename,
					   const DLV::model *const structure,
					   char message[], const int_g mlen)
{
  structure_file::write(filename, structure, get_c03_indices(), message, mlen);
}

void CRYSTAL::scf_calc_v7::write_structure(const DLV::string filename,
					   const DLV::model *const structure,
					   char message[], const int_g mlen)
{
  structure_file::write(filename, structure, get_c03_indices(), message, mlen);
}

void CRYSTAL::scf_calc_v8::write_structure(const DLV::string filename,
					   const DLV::model *const structure,
					   char message[], const int_g mlen)
{
  structure_file::write(filename, structure, get_c03_indices(), message, mlen);
}

void CRYSTAL::cphf_calc_v7::write_structure(const DLV::string filename,
					    const DLV::model *const structure,
					    char message[], const int_g mlen)
{
  structure_file::write(filename, structure, get_c03_indices(), message, mlen);
}

void CRYSTAL::cphf_calc_v8::write_structure(const DLV::string filename,
					    const DLV::model *const structure,
					    char message[], const int_g mlen)
{
  structure_file::write(filename, structure, get_c03_indices(), message, mlen);
}

void CRYSTAL::opt_calc_v5::write_structure(const DLV::string filename,
					   const DLV::model *const structure,
					   char message[], const int_g mlen)
{
  structure_file::write(filename, structure, get_c03_indices(), message, mlen);
}

void CRYSTAL::opt_calc_v6::write_structure(const DLV::string filename,
					   const DLV::model *const structure,
					   char message[], const int_g mlen)
{
  structure_file::write(filename, structure, get_c03_indices(), message, mlen);
}

void CRYSTAL::opt_calc_v7::write_structure(const DLV::string filename,
					   const DLV::model *const structure,
					   char message[], const int_g mlen)
{
  structure_file::write(filename, structure, get_c03_indices(), message, mlen);
}

void CRYSTAL::opt_calc_v8::write_structure(const DLV::string filename,
					   const DLV::model *const structure,
					   char message[], const int_g mlen)
{
  structure_file::write(filename, structure, get_c03_indices(), message, mlen);
}

bool CRYSTAL::scf_data::setup_atom_bases(const DLV::model *const structure)
{
  if (basis_indices == 0) {
    natoms = structure->get_number_of_asym_atoms();
    basis_indices = new int_g[natoms];
    for (int_g i = 0; i < natoms; i++)
      basis_indices[i] = -1;
  }
  c03_indices = new int_g[natoms];
  int_g alle[100];
  int_g pseudo[100];
  for (int_g i = 0; i < 100; i++) {
    alle[i] = 0;
    pseudo[i] = 200;
  }
  int_g *atn = new_local_array1(int_g, natoms);
  structure->get_asym_atom_types(atn, natoms);
  if (atom_bases.size() > 0) {
    std::list<basis_info>::iterator ptr;
    int_g p = 0;
    for (ptr = atom_bases.begin(); ptr != atom_bases.end(); ++ptr ) {
      int_g j;
      for (j = 0; j < natoms; j++)
	if (basis_indices[j] == p)
	  break;
      if (j < natoms) {
	if (ptr->pseudo) {
	  c03_indices[j] = atn[j] + pseudo[atn[j]];
	  pseudo[atn[j]] += 100;
	} else {
	  c03_indices[j] = atn[j] + alle[atn[j]];
	  alle[atn[j]] += 100;
	}
	ptr->index = c03_indices[j];
	for (int_g k = j + 1; k < natoms; k++) {
	  if (basis_indices[k] == p and atn[k] == atn[j])
	    c03_indices[k] = c03_indices[j];
	}
      }
      p++;
    }
  }
  for (int_g i = 0; i < natoms; i++) {
    if (basis_indices[i] == -1) {
      c03_indices[i] = atn[i] + alle[atn[i]];
      basis.use_default = true;
    }
  }
  bool ok = true;
  for (int_g i = 0; i < natoms; i++) {
    if (alle[atn[i]] > 299 or (alle[atn[i]] > 199 and basis_indices[i] == -1))
      ok = false;
  }
  delete_local_array(atn);
  return ok;
}

bool CRYSTAL::scf_data::write_input(const DLV::string filename,
				    const DLV::model *const structure,
				    char message[], const int_g mlen)
{
  if (setup_atom_bases(structure)) {
    std::ofstream output;
    bool ok;
    if ((ok = DLV::open_file_write(output, filename.c_str(), message, mlen))) {
      output << "DLV generated input file for ";
      output << structure->get_model_name().c_str() << '\n';
      write_extern_label(output);
      write_geom_commands(output, structure);
      if (!basis.use_default or basis.default_basis < Basis::POB_DZVP)
	output << "END\n";
      if ((ok = write_basis(output, structure, message, mlen))) {
	write_controls(output);
	if (structure->get_number_of_periodic_dims() > 0)
	  write_shrink(output);
	write_scf(output, structure);
	output << "END\n";
	write_section5(output);
      }
      output.close();
    }
    return ok;
  } else {
    strncpy(message, "Too many all electron bases", mlen);
    return false;
  }
}

void CRYSTAL::scf_data_v4::write_extern_label(std::ofstream &output) const
{
  output << "EXTERNAL\n";
}

void CRYSTAL::scf_data_v5::write_extern_label(std::ofstream &output) const
{
  output << "DLVINPUT\n";
}

void CRYSTAL::scf_data_v6::write_extern_label(std::ofstream &output) const
{
  output << "DLVINPUT\n";
}

void
CRYSTAL::scf_data::write_geom_commands(std::ofstream &output,
				       const DLV::model *const structure) const
{
}

void CRYSTAL::cphf_data::write_cphf(std::ofstream &output) const
{
  output << "CPHF\n";
  output << "FMIXING\n";
  output << cphf.mixing << '\n';
  output << "MAXCYCLE\n";
  output << cphf.maxcycles << '\n';
  output << "TOLALPHA\n";
  output << cphf.tolalpha << '\n';
  output << "TOLUDIK\n";
  output << cphf.toludik << '\n';
  output << "END\n";
}

void CRYSTAL::cphf_data_v7::write_geom_commands(std::ofstream &output,
						const DLV::model *const) const
{
  write_cphf(output);
}

void CRYSTAL::cphf_data_v8::write_geom_commands(std::ofstream &output,
						const DLV::model *const) const
{
  write_cphf(output);
}

void CRYSTAL::opt_data::write_optimise(std::ofstream &output) const
{
  if (optimise.optimiser == Optimise::berny) {
    output << "OPTCOORD\n";
    output << "ETOL\n";
    output << optimise.etol << '\n';
    output << "XTOL\n";
    output << optimise.xtol << '\n';
  }
  else {
    output << "OPTIMIZE\n";
    output << "DOMIN\n";
    switch (optimise.opt_type) {
    case Optimise::cell_opt:
      output << "CELLOPT\n";
      break;
    default:
      output << "SYMMDIR\n";
      break;
    }
    output << "GSTEP\n";
    output << optimise.gtol << '\n';
    output << "ISCALE\n";
    output << optimise.iscale << '\n';
  }
  output << "GTOL\n";
  output << 1.5*optimise.gtol << '\n';
  output << "ETOL\n";
  output << optimise.etol << '\n';
  output << "XTOL\n";
  output << 1.5*optimise.xtol << '\n';
  output << "ENDOPT\n";
}

void CRYSTAL::opt_data::write_v6_optimise(std::ofstream &output) const
{
  if (optimise.optimiser == Optimise::berny) {
    output << "OPTGEOM\n";
    switch (optimise.opt_type) {
    case Optimise::cell_opt:
      output << "CELLONLY\n";
      break;
    case Optimise::atom_and_cell_opt:
      output << "FULLOPTG\n";
      break;
    default:
      break;
    }
    output << "TOLDEE\n";
    output << optimise.etol << '\n';
    output << "TOLDEG\n";
    output << 1.5*optimise.gtol << '\n';
    output << "TOLDEX\n";
    output << 1.5*optimise.xtol << '\n';
  }
  output << "ENDOPT\n";
}

void CRYSTAL::opt_data::write_v7_optimise(std::ofstream &output) const
{
  if (optimise.optimiser == Optimise::berny) {
    output << "OPTGEOM\n";
    switch (optimise.opt_type) {
    case Optimise::cell_opt:
      output << "CELLONLY\n";
      break;
    case Optimise::atom_and_cell_opt:
      output << "FULLOPTG\n";
      break;
    default:
      break;
    }
    output << "TOLDEE\n";
    output << optimise.etol << '\n';
    output << "TOLDEG\n";
    output << 1.5*optimise.gtol << '\n';
    output << "TOLDEX\n";
    output << 1.5*optimise.xtol << '\n';
  } else {
    output << "OPTIMIZE\n";
    output << "SYMMDIR\n";
    if(optimise.optimiser == Optimise::domin){
      output << "DOMIN\n";
      output << "ETOL\n";
      output << optimise.etol << '\n';
      output << "XTOL\n";
      output << optimise.xtol << '\n';
    }
    if(optimise.optimiser == Optimise::dlfind){
      output << "DLFIND\n";
      output << "NPOP\n";
      output << optimise.dlf_pop << '\n';
      output << "RADIUS\n";
      output << optimise.dlf_radius << '\n';
      output << "PO_GTOL\n";
      output << optimise.gtol << '\n';
      output << "PO_MAXCYCLE\n";
      output << optimise.dlf_cycles << '\n';
      if(optimise.dlf_type==Optimise::stoch) {
	output << "OPT_STOCH\n";
	if(optimise.dlf_sstype==Optimise::uniform)
	  output << "UNIFORM\n";
	if(optimise.dlf_sstype==Optimise::force_dir_bias)
	  output << "FORCE_DIR_BIAS\n";
	if(optimise.dlf_sstype==Optimise::force_bias)
	  output << "FORCE_BIAS\n";
	output <<"PO_RTOL\n";
	output << optimise.dlf_minradius <<"\n";
	output <<"CONTRACT\n";
	output << optimise.dlf_contractradius <<"\n";
	output <<"SCALEFACTOR\n";
	output << optimise.dlf_scalef <<"\n";
      }
      if(optimise.dlf_type==Optimise::ga) {
	output << "OPT_GA\n";
	output <<"NPOP_INIT\n";
	output << optimise.dlf_initpop <<"\n";
	output <<"MUTATION_RATE\n";
	output << optimise.dlf_mutation <<"\n";
	output <<"DEATH_RATE\n";
	output << optimise.dlf_death <<"\n";
	output << "NSAVE\n";
	output << optimise.dlf_nsaves << '\n';
  	output << "PO_RESET\n";
	output << optimise.dlf_reset << '\n';
      }
    }
  }
  output << "ENDOPT\n";
}

void CRYSTAL::opt_data::write_v8_optimise(std::ofstream &output) const
{
  if (optimise.optimiser == Optimise::berny) {
    output << "OPTGEOM\n";
    switch (optimise.opt_type) {
    case Optimise::cell_opt:
      output << "CELLONLY\n";
      break;
    case Optimise::atom_and_cell_opt:
      output << "FULLOPTG\n";
      break;
    default:
      output << "ATOMONLY\n";
      break;
    }
    output << "TOLDEE\n";
    output << optimise.etol << '\n';
    output << "TOLDEG\n";
    output << 1.5*optimise.gtol << '\n';
    output << "TOLDEX\n";
    output << 1.5*optimise.xtol << '\n';
  } else {
    output << "OPTIMIZE\n";
    output << "SYMMDIR\n";
    if(optimise.optimiser == Optimise::domin){
      output << "DOMIN\n";
      output << "ETOL\n";
      output << optimise.etol << '\n';
      output << "XTOL\n";
      output << optimise.xtol << '\n';
    }
    if(optimise.optimiser == Optimise::dlfind){
      output << "DLFIND\n";
      output << "NPOP\n";
      output << optimise.dlf_pop << '\n';
      output << "RADIUS\n";
      output << optimise.dlf_radius << '\n';
      output << "PO_GTOL\n";
      output << optimise.gtol << '\n';
      output << "PO_MAXCYCLE\n";
      output << optimise.dlf_cycles << '\n';
      if(optimise.dlf_type==Optimise::stoch) {
	output << "OPT_STOCH\n";
	if(optimise.dlf_sstype==Optimise::uniform)
	  output << "UNIFORM\n";
	if(optimise.dlf_sstype==Optimise::force_dir_bias)
	  output << "FORCE_DIR_BIAS\n";
	if(optimise.dlf_sstype==Optimise::force_bias)
	  output << "FORCE_BIAS\n";
	output <<"PO_RTOL\n";
	output << optimise.dlf_minradius <<"\n";
	output <<"CONTRACT\n";
	output << optimise.dlf_contractradius <<"\n";
	output <<"SCALEFACTOR\n";
	output << optimise.dlf_scalef <<"\n";
      }
      if(optimise.dlf_type==Optimise::ga) {
	output << "OPT_GA\n";
	output <<"NPOP_INIT\n";
	output << optimise.dlf_initpop <<"\n";
	output <<"MUTATION_RATE\n";
	output << optimise.dlf_mutation <<"\n";
	output <<"DEATH_RATE\n";
	output << optimise.dlf_death <<"\n";
	output << "NSAVE\n";
	output << optimise.dlf_nsaves << '\n';
  	output << "PO_RESET\n";
	output << optimise.dlf_reset << '\n';
      }
    }
  }
  output << "ENDOPT\n";
}

void CRYSTAL::opt_data_v5::write_geom_commands(std::ofstream &output,
					       const DLV::model *const) const
{
  write_optimise(output);
}

void CRYSTAL::opt_data_v6::write_geom_commands(std::ofstream &output,
					       const DLV::model *const) const
{
  write_v6_optimise(output);
}

void CRYSTAL::opt_data_v7::write_geom_commands(std::ofstream &output,
					       const DLV::model *const) const
{
  write_v7_optimise(output);
}

void CRYSTAL::opt_data_v8::write_geom_commands(std::ofstream &output,
					       const DLV::model *const) const
{
  write_v8_optimise(output);
}

bool CRYSTAL::scf_data::write_basis(std::ofstream &output,
				    const DLV::model *const structure,
				    char message[], const int_g mlen) const
{
  int_g *atn = new_local_array1(int_g, natoms);
  structure->get_asym_atom_types(atn, natoms);
  int_g *charges = new_local_array1(int_g, natoms);
  structure->get_asym_atom_charges(charges, natoms);
  bool *use_charge = new_local_array1(bool, natoms);
  for (int_g i = 0; i < natoms; i++)
    use_charge[i] = true;
  if (atom_bases.size() > 0) {
    std::list<basis_info>::const_iterator ptr;
    int_g p = 0;
    for (ptr = atom_bases.begin(); ptr != atom_bases.end(); ++ptr ) {
      int_g j;
      for (j = 0; j < natoms; j++)
	if (basis_indices[j] == p)
	  break;
      if (j < natoms) {
	if (ptr->pseudo)
	  write_pseudo_basis(output, ptr->name, ptr->index, atn[j],
			     charges[j], use_charge[j], message, mlen);
	else
	  write_alle_basis(output, ptr->name, ptr->index, atn[j],
			   charges[j], message, mlen);
      }
      p++;
    }
  }
  if (basis.use_default) {
    if (basis.default_basis > Basis::G621) {
      output << "BASISSET\n";
      switch (basis.default_basis) {
      case Basis::POB_DZVP:
	output << "POB-DZVP\n";
	break;
      case Basis::POB_DZVPP:
	output << "POB-DZVPP\n";
	break;
      case Basis::POB_TZVP:
	output << "POB-TZVP\n";
	break;
      case Basis::STO6G:
	output << "STO-6G\n";
	break;
      case Basis::G631ss:
	output << "6-31G**\n";
	break;
      case Basis::DEF2_TZVP:
	output << "DEF2-TZVP\n";
	break;
      default:
	break;
      }
    }
    bool done[100];
    for (int_g i = 0; i < 100; i++)
      done[i] = false;
    for (int_g j = 0; j < natoms; j++) {
      if (basis_indices[j] == -1 and !done[atn[j]]) {
	switch (basis.default_basis) {
	case Basis::STO3G:
	  write_sto3g(output, c03_indices[j], atn[j], charges[j]);
	  break;
	case Basis::G321:
	  write_n21g(output, c03_indices[j], atn[j], charges[j], 3);
	  break;
	case Basis::G621:
	  write_n21g(output, c03_indices[j], atn[j], charges[j], 6);
	  break;
	default:
	  break;
	}
	done[atn[j]] = true;
      }
    }
  }
  if (!basis.use_default or basis.default_basis < Basis::POB_DZVP) {
    output << "99 0\n";
    // Check for CHEMOD requirements
    bool *new_config = new_local_array1(bool, natoms);
    for (int_g i = 0; i < natoms; i++)
      new_config[i] = false;
    // count number required
    int_g count = 0;
    for (int_g i = 0; i < natoms; i++) {
      new_config[i] = true;
      for (int_g j = i + 1; j < natoms; j++) {
	if (!new_config[j] and atn[j] == atn[i] and
	    c03_indices[j] == c03_indices[i]) {
	  if (charges[j] != charges[i] and use_charge[i]) {
	    count++;
	    new_config[j] = true;
	  }
	}
      }
    }
    if (count > 0) {
      output << "CHEMOD\n";
      output << count << "\n";
      for (int_g i = 0; i < natoms; i++)
	new_config[i] = false;
      for (int_g i = 0; i < natoms; i++) {
	new_config[i] = true;
	for (int_g j = i + 1; j < natoms; j++) {
	  if (!new_config[j] and atn[j] == atn[i] and
	      c03_indices[j] == c03_indices[i]) {
	    if (charges[j] != charges[i] and use_charge[i]) {
	      output << j;
	      if (basis_indices[j] >= 0) {
		int_g p = 0;
		std::list<basis_info>::const_iterator ptr;
		for (ptr = atom_bases.begin(); p < basis_indices[j]; ++ptr );
		if (c03_indices[j] > 199)
		  write_pseudo_config(output, ptr->name, atn[j],
				      charges[j], message, mlen);
		else
		  write_alle_config(output, ptr->name, atn[j],
				    charges[j], message, mlen);
	      } else {
		switch (basis.default_basis) {
		case Basis::STO3G:
		  write_sto3g_config(output, atn[j], charges[j]);
		  break;
		default:
		  write_n21g_config(output, atn[j], charges[j]);
		  break;
		}
	      }
	      output << "\n";
	      new_config[j] = true;
	    }
	  }
	}
      }
    }
    delete_local_array(new_config);
    output << "END\n";
  }
  delete_local_array(use_charge);
  delete_local_array(charges);
  delete_local_array(atn);
  return true;
}

void CRYSTAL::scf_data::write_alle_basis(std::ofstream &output,
					 const DLV::string name,
					 const int_g index, const int_g atn,
					 const int_g charge, char message[],
					 const int_g mlen)
{
  std::ifstream file;
  if (DLV::open_file_read(file, name.c_str(), message, mlen)) {
    int_g n, ns = 1, np = 2, nd = 3, nf = 4, nshells, ng, btype, shelltype;
    real_g ne, scale;
    char buff[128];
    file >> n;
    file >> nshells;
    output << index << " " << nshells << "\n";
    // all electron
    int_g s1 = 0, s2 = 0, s3 = 0, s4 = 0, s5 = 0, s6 = 0, s7 = 0;
    int_g p2 = 0, p3 = 0, p4 = 0, p5 = 0, p6 = 0;
    int_g d3 = 0, d4 = 0, d5 = 0, d6 = 0;
    int_g f4 = 0, f5 = 0;
    get_configuration(atn, charge, s1, s2, s3, s4, s5, s6, s7, p2, p3,
		      p4, p5, p6, d3, d4, d5, d6, f4, f5);
    for (int_g i = 0; i < nshells; i++) {
      file >> btype;
      file >> shelltype;
      file >> ng;
      file >> ne;
      file >> scale;
      file.getline(buff, 128);
      output << btype << " " << shelltype << " " << ng << " ";
      // output number of electrons
      switch (shelltype) {
      case 0: // S shell
	write_s_electrons(s1, s2, s3, s4, s5, s6, s7, ns, output);
	break;
      case 1: // SP shell
	write_sp_electrons(s2 + p2, s3 + p3, s4 + p4, s5 + p5, s6 + p6,
			   np, output);
	ns++;
	break;
      case 2: // P shell
	write_p_electrons(p2, p3, p4, p5, p6, np, output);
	break;
      case 3: // D shell
	write_d_electrons(d3, d4, d5, d6, nd, output);
	break;
      case 4: // F shell
	write_f_electrons(f4, f5, nf, output);
	break;
      default:
	//DLVwarning("write_basis", "G shells not implemented");
	break;
      }
      output << " " << scale << "\n";
      for (int_g j = 0; j < ng; j++) {
	file.getline(buff, 128);
	output << buff << "\n";
      }
    }
    file.close();
  }
}

void CRYSTAL::scf_data::write_pseudo_basis(std::ofstream &output,
					   const DLV::string name,
					   const int_g index, const int_g atn,
					   const int_g charge, bool &use_charge,
					   char message[], const int_g mlen)
{
  std::ifstream file;
  if (DLV::open_file_read(file, name.c_str(), message, mlen)) {
    int_g n, nshells, ns = 1, np = 2, nd = 3, ng, btype, shelltype;
    real_g ne, scale;
    char buff[128];
    file >> n;
    file >> nshells;
    output << index << " " << nshells << "\n";
    file.getline(buff, 128);
    file.getline(buff, 128);
    if (strcmp(buff, "HAYWSC") == 0) {
      if (atn <= 36) {
	ns = 3;
	np = 3;
      } else if (atn <= 54) {
	ns = 4;
	np = 4;
	nd = 4;
      } else {
	ns = 5;
	np = 5;
	nd = 5;
      }
    } else if (strcmp(buff, "HAYWLC") == 0 || strcmp(buff, "BARTHE") == 0 ||
	       strcmp(buff, "DURAND") == 0) {
      if (atn <= 10)
	ns = 2;
      else if (atn <= 18) {
	ns = 3;
	np = 3;
      } else if (atn <= 36) {
	ns = 4;
	np = 4;
      } else if (atn <= 54) {
	ns = 5;
	np = 5;
	nd = 4;
      } else {
	ns = 6;
	np = 6;
	nd = 5;
      }
    } else {
      // STOLLP or INPUT
      use_charge = false;
      while (file.eof()) {
	file.getline(buff, 128);
	output << buff << "\n";
      }
      return;
    }
    output << buff << "\n";
    int_g s1 = 0, s2 = 0, s3 = 0, s4 = 0, s5 = 0, s6 = 0, s7 = 0;
    int_g p2 = 0, p3 = 0, p4 = 0, p5 = 0, p6 = 0;
    int_g d3 = 0, d4 = 0, d5 = 0, d6 = 0;
    int_g f4 = 0, f5 = 0;
    get_configuration(atn, charge, s1, s2, s3, s4, s5, s6, s7, p2, p3, p4, p5,
		      p6, d3, d4, d5, d6, f4, f5);
    for (int_g i = 0; i < nshells; i++) {
      file >> btype;
      file >> shelltype;
      file >> ng;
      file >> ne;
      file >> scale;
      // Get the end of line so we can just copy ng lines.
      file.getline(buff, 128);
      output << btype << " " << shelltype << " " << ng << " ";
      // output number of electrons
      switch (shelltype) {
      case 0: // S shell
	write_s_electrons(s1, s2, s3, s4, s5, s6, s7, ns, output);
	break;
      case 1: // SP shell
	write_sp_electrons(s2 + p2, s3 + p3, s4 + p4, s5 + p5, s6 + p6,
			   np, output);
	ns++;
	break;
      case 2: // P shell
	write_p_electrons(p2, p3, p4, p5, p6, np, output);
	break;
      case 3: // D shell
	write_d_electrons(d3, d4, d5, d6, nd, output);
	break;
      }
      output << " " << scale << "\n";
      for (int_g j = 0; j < ng; j++) {
	file.getline(buff, 128);
	output << buff << "\n";
      }
    }
  }
}

void CRYSTAL::scf_data::write_s_electrons(const int_g s1, const int_g s2,
					  const int_g s3, const int_g s4,
					  const int_g s5, const int_g s6,
					  const int_g s7, int_g &n,
					  std::ofstream &output)
{
  switch (n) {
  case 1:
    output << (real_g)s1;
    break;
  case 2:
    output << (real_g)s2;
    break;
  case 3:
    output << (real_g)s3;
    break;
  case 4:
    output << (real_g)s4;
    break;
  case 5:
    output << (real_g)s5;
    break;
  case 6:
    output << (real_g)s6;
    break;
  case 7:
    output << (real_g)s7;
    break;
  default:
    output << 0.0;
    break;
  }
  n++;
}

void CRYSTAL::scf_data::write_sp_electrons(const int_g sp2, const int_g sp3,
					   const int_g sp4, const int_g sp5,
					   const int_g sp6, int_g &n,
					   std::ofstream &output)
{
  switch (n) {
  case 2:
    output << (real_g)(sp2);
    break;
  case 3:
    output << (real_g)(sp3);
    break;
  case 4:
    output << (real_g)(sp4);
    break;
  case 5:
    output << (real_g)(sp5);
    break;
  case 6:
    output << (real_g)(sp6);
    break;
  default:
    output << 0.0;
    break;
  }
  n++;
}

void CRYSTAL::scf_data::write_p_electrons(const int_g p2, const int_g p3,
					  const int_g p4, const int_g p5,
					  const int_g p6, int_g &n,
					  std::ofstream &output)
{
  switch (n) {
  case 2:
    output << (real_g)p2;
    break;
  case 3:
    output << (real_g)p3;
    break;
  case 4:
    output << (real_g)p4;
    break;
  case 5:
    output << (real_g)p5;
    break;
  case 6:
    output << (real_g)p6;
    break;
  default:
    output << 0.0;
    break;
  }
  n++;
}

void CRYSTAL::scf_data::write_d_electrons(const int_g d3, const int_g d4,
					  const int_g d5, const int_g d6,
					  int_g &n, std::ofstream &output)
{
  switch (n) {
  case 3:
    output << (real_g)d3;
    break;
  case 4:
    output << (real_g)d4;
    break;
  case 5:
    output << (real_g)d5;
    break;
  default:
    output << 0.0;
    break;
  }
  n++;
}

void CRYSTAL::scf_data::write_f_electrons(const int_g f4, const int_g f5,
					  int_g &n, std::ofstream &output)
{
  switch (n) {
  case 4:
    output << (real_g)f4;
    break;
  case 5:
    output << (real_g)f5;
    break;
  default:
    output << 0.0;
    break;
  }
  n++;
}

void CRYSTAL::scf_data::write_sto3g(std::ofstream &output, const int_g index,
				    const int_g atn, const int_g charge)
{
  output << index << " ";
  if (atn <= 2)
    output << 1;
  else if (atn <= 10)
    output << 2;
  else if (atn <= 18)
    output << 3;
  else if (atn <= 20)
    output << 4;
  else if (atn <= 36)
    output << 5;
  else if (atn <= 38)
    output << 6;
  else if (atn <= 54)
    output << 7;
  output << '\n';
  int_g s1 = 0, s2 = 0, s3 = 0, s4 = 0, s5 = 0, s6 = 0, s7 = 0;
  int_g p2 = 0, p3 = 0, p4 = 0, p5 = 0, p6 = 0;
  int_g d3 = 0, d4 = 0, d5 = 0, d6 = 0;
  int_g f4 = 0, f5 = 0;
  get_configuration(atn, charge, s1, s2, s3, s4, s5, s6, s7, p2, p3, p4, p5,
		    p6, d3, d4, d5, d6, f4, f5);
  output << "1 0 3 " << (real_g)s1 << " 0.0\n";
  if (atn > 2) {
    output << "1 1 3 " << (real_g)(s2 + p2) << " 0.0\n";
    if (atn > 10) {
      output << "1 1 3 " << (real_g)(s3 + p3) << " 0.0\n";
      if (atn > 18) {
	output << "1 1 3 " << (real_g)(s4 + p4);
	output << " 0.0\n";
	if (atn > 20) {
	  output << "1 3 3 " << (real_g)d3 << " 0.0\n";
	  if (atn > 36) {
	    output << "1 1 3 " << (real_g)(s5 + p5);
	    output << " 0.0\n";
	    if (atn > 38)
	      output << "1 3 3 " << (real_g)d4 << " 0.0\n";
	  }
	}
      }
    }
  }
}

void CRYSTAL::scf_data::write_n21g(std::ofstream &output, const int_g index,
				   const int_g atn, const int_g charge,
				   const int_g ng)
{
  output << index << " ";
  if (atn <= 2)
    output << 2;
  else if (atn <= 10)
    output << 3;
  else if (atn <= 18)
    output << 4;
  else if (atn <= 20)
    output << 5;
  else if (atn <= 36)
    output << 7;
  else if (atn <= 38)
    output << 7;
  else if (atn <= 54)
    output << 9;
  output << "\n";
  int_g s1 = 0, s2 = 0, s3 = 0, s4 = 0, s5 = 0, s6 = 0, s7 = 0;
  int_g p2 = 0, p3 = 0, p4 = 0, p5 = 0, p6 = 0;
  int_g d3 = 0, d4 = 0, d5 = 0, d6 = 0;
  int_g f4 = 0, f5 = 0;
  get_configuration(atn, charge, s1, s2, s3, s4, s5, s6, s7, p2, p3, p4, p5,
		    p6, d3, d4, d5, d6, f4, f5);
  if (atn <= 2) {
    output << "2 0 2 " << (real_g)s1 << " 1.0\n";
    output << "2 0 1 0.0 1.0\n";
  } else {
    output << "2 0 " << ng << " " << (real_g)s1 << " 1.0\n";
    if (atn <= 10) {
      output << "2 1 2 " << (real_g)(s2 + p2) << " 1.0\n";
      output << "2 1 1 0.0 1.0\n";
    } else {
      output << "2 1 " << ng << " " << (real_g)(s2 + p2);
      output << " 1.0\n";
      if (atn <= 18) {
        output << "2 1 2 " << (real_g)(s3 + p3);
        output << " 1.0\n";
        output << "2 1 1 0.0 1.0\n";
      } else {
        output << "2 1 " << ng << " " << (real_g)(s3 + p3);
        output << " 1.0\n";
        if (atn <= 36) {
          output << "2 1 2 " << (real_g)(s4 + p4);
          output << " 1.0\n";
          output << "2 1 1 0.0 1.0\n";
          if (atn > 20) {
            output << "2 3 2 " << (real_g)d3 << " 1.0\n";
            output << "2 3 1 0.0 1.0\n";
          }
        } else {
          output << "2 1 " << ng << " " << (real_g)(s4 + p4);
          output << " 1.0\n";
          output << "2 3 " << ng << " " << (real_g)d3 << " 1.0\n";
          output << "2 1 2 " << (real_g)(s5 + p5);
          output << " 1.0\n";
          output << "2 1 1 0.0 1.0\n";
          if (atn > 38) {
            output << "2 3 2 " << (real_g)d4 << " 1.0\n";
            output << "2 3 1 0.0 1.0\n";
          }
        }
      }
    }
  }
}

void CRYSTAL::scf_data::write_alle_config(std::ofstream &output,
					  const DLV::string name,
					  const int_g atn, const int_g charge,
					  char message[], const int_g mlen)
{
  std::ifstream file;
  if (DLV::open_file_read(file, name.c_str(), message, mlen)) {
    int_g n, ns = 1, np = 2, nd = 3, nf = 4, nshells, ng, btype, shelltype;
    real_g ne, scale;
    char buff[128];
    file >> n;
    file >> nshells;
    // all electron
    int_g s1 = 0, s2 = 0, s3 = 0, s4 = 0, s5 = 0, s6 = 0, s7 = 0;
    int_g p2 = 0, p3 = 0, p4 = 0, p5 = 0, p6 = 0;
    int_g d3 = 0, d4 = 0, d5 = 0, d6 = 0;
    int_g f4 = 0, f5 = 0;
    get_configuration(atn, charge, s1, s2, s3, s4, s5, s6, s7, p2, p3,
		      p4, p5, p6, d3, d4, d5, d6, f4, f5);
    for (int_g i = 0; i < nshells; i++) {
      file >> btype;
      file >> shelltype;
      file >> ng;
      file >> ne;
      file >> scale;
      // Get the end of line so we can just copy ng lines.
      file.getline(buff, 128);
      // output number of electrons
      output << " ";
      switch (shelltype) {
      case 0: // S shell
	write_s_electrons(s1, s2, s3, s4, s5, s6, s7, ns, output);
	break;
      case 1: // SP shell
	write_sp_electrons(s2 + p2, s3 + p3, s4 + p4, s5 + p5, s6 + p6,
			   np, output);
	ns++;
	break;
      case 2: // P shell
	write_p_electrons(p2, p3, p4, p5, p6, np, output);
	break;
      case 3: // D shell
	write_d_electrons(d3, d4, d5, d6, nd, output);
	break;
      case 4: // F shell
	write_f_electrons(f4, f5, nf, output);
	break;
      default:
	break;
      }
      for (int_g j = 0; j < ng; j++)
        file.getline(buff, 128);
    }
    file.close();
  }
}

void CRYSTAL::scf_data::write_pseudo_config(std::ofstream &output,
					    const DLV::string name,
					    const int_g atn, const int_g charge,
					    char message[], const int_g mlen)
{
  std::ifstream file;
  if (DLV::open_file_read(file, name.c_str(), message, mlen)) {
    int_g n, ns = 1, np = 2, nd = 3, nshells, ng, btype, shelltype;
    real_g ne, scale;
    char buff[128];
    file >> n;
    file >> nshells;
    file.getline(buff, 128);
    file.getline(buff, 128);
    if (strcmp(buff, "HAYWSC") == 0) {
      if (atn <= 36) {
        ns = 3;
        np = 3;
      } else if (atn <= 54) {
        ns = 4;
        np = 4;
        nd = 4;
      } else {
        ns = 5;
        np = 5;
        nd = 5;
      }
    } else if (strcmp(buff, "HAYWLC") == 0 || strcmp(buff, "BARTHE") == 0 ||
               strcmp(buff, "DURAND") == 0) {
      if (atn <= 10)
        ns = 2;
      else if (atn <= 18) {
        ns = 3;
        np = 3;
      } else if (atn <= 36) {
        ns = 4;
        np = 4;
      } else if (atn <= 54) {
        ns = 5;
        np = 5;
        nd = 4;
      } else {
        ns = 6;
        np = 6;
        nd = 5;
      }
    }
    int_g s1 = 0, s2 = 0, s3 = 0, s4 = 0, s5 = 0, s6 = 0, s7 = 0;
    int_g p2 = 0, p3 = 0, p4 = 0, p5 = 0, p6 = 0;
    int_g d3 = 0, d4 = 0, d5 = 0, d6 = 0;
    int_g f4 = 0, f5 = 0;
    get_configuration(atn, charge, s1, s2, s3, s4, s5, s6, s7, p2, p3,
		      p4, p5, p6, d3, d4, d5, d6, f4, f5);
    for (int_g i = 0; i < nshells; i++) {
      file >> btype;
      file >> shelltype;
      file >> ng;
      file >> ne;
      file >> scale;
      // Get the end of line so we can just copy ng lines.
      file.getline(buff, 128);
      output << " ";
      // output number of electrons
      switch (shelltype) {
      case 0: // S shell
	write_s_electrons(s1, s2, s3, s4, s5, s6, s7, ns, output);
	break;
      case 1: // SP shell
	write_sp_electrons(s2 + p2, s3 + p3, s4 + p4, s5 + p5, s6 + p6,
			   np, output);
	ns++;
	break;
      case 2: // P shell
	write_p_electrons(p2, p3, p4, p5, p6, np, output);
	break;
      case 3: // D shell
	write_d_electrons(d3, d4, d5, d6, nd, output);
	break;
      }
      for (int_g j = 0; j < ng; j++)
        file.getline(buff, 128);
    }
    file.close();
  }
}

void CRYSTAL::scf_data::write_sto3g_config(std::ofstream &output,
					   const int_g atn, const int_g charge)
{
  int_g s1 = 0, s2 = 0, s3 = 0, s4 = 0, s5 = 0, s6 = 0, s7 = 0;
  int_g p2 = 0, p3 = 0, p4 = 0, p5 = 0, p6 = 0;
  int_g d3 = 0, d4 = 0, d5 = 0, d6 = 0;
  int_g f4 = 0, f5 = 0;
  get_configuration(atn, charge, s1, s2, s3, s4, s5, s6, s7, p2, p3, p4, p5,
		    p6, d3, d4, d5, d6, f4, f5);
  output << " " << (real_g)s1;
  if (atn > 2) {
    output << " " << (real_g)(s2 + p2);
    if (atn > 10) {
      output << " " << (real_g)(s3 + p3);
      if (atn > 18) {
        output << " " << (real_g)(s4 + p4);
        if (atn > 20) {
          output << " " << (real_g)d3;
          if (atn > 36) {
            output << " " << (real_g)(s5 + p5);
            if (atn > 38)
              output << " " << (real_g)d4;
          }
        }
      }
    }
  }
}

void CRYSTAL::scf_data::write_n21g_config(std::ofstream &output,
					  const int_g atn, const int_g charge)
{
  int_g s1 = 0, s2 = 0, s3 = 0, s4 = 0, s5 = 0, s6 = 0, s7 = 0;
  int_g p2 = 0, p3 = 0, p4 = 0, p5 = 0, p6 = 0;
  int_g d3 = 0, d4 = 0, d5 = 0, d6 = 0;
  int_g f4 = 0, f5 = 0;
  get_configuration(atn, charge, s1, s2, s3, s4, s5, s6, s7, p2, p3, p4, p5,
		    p6, d3, d4, d5, d6, f4, f5);
  if (atn <= 2)
    output << " " << (real_g)s1 << " 0.0";
  else {
    output << " " << (real_g)s1;
    if (atn <= 10)
      output << " " << (real_g)(s2 + p2) << " 0.0";
    else {
      output << " " << (real_g)(s2 + p2);
      if (atn <= 18)
        output << " " << (real_g)(s3 + p3) << " 0.0";
      else {
        output << " " << (real_g)(s3 + p3);
        if (atn <= 36) {
          output << " " << (real_g)(s4 + p4) << " 0.0";
          if (atn > 20)
            output << " " << (real_g)d3 << " 0.0";
        } else {
          output << " " << (real_g)(s4 + p4);
          output << " " << (real_g)d3;
          output << " " << (real_g)(s5 + p5) << " 0.0";
          if (atn > 38)
            output << " " << (real_g)d4 << " 0.0";
        }
      }
    }
  }
}

void CRYSTAL::scf_data::get_configuration(const int_g atn, const int_g charge,
					  int_g &s1, int_g &s2, int_g &s3,
					  int_g &s4, int_g &s5, int_g &s6,
					  int_g &s7, int_g &p2, int_g &p3,
					  int_g &p4, int_g &p5, int_g &p6,
					  int_g &d3, int_g &d4, int_g &d5,
					  int_g &d6, int_g &f4, int_g &f5)
{
  s1 = 0;
  s2 = 0;
  s3 = 0;
  s4 = 0;
  s5 = 0;
  s6 = 0;
  s7 = 0;
  p2 = 0;
  p3 = 0;
  p4 = 0;
  p5 = 0;
  p6 = 0;
  d3 = 0;
  d4 = 0;
  d5 = 0;
  d6 = 0;
  f4 = 0;
  f5 = 0;
  int_g ne = atn - charge;
  if (atn <= 2) {
    s1 = ne;
    if (s1 < 0)
      s1 = 0;
    else if ( s1 > 2)
      s1 = 2;
  } else {
    s1 = 2;
    if (atn <= 10) {
      if (atn <= 4) {
	s2 = ne - 2;
	if (s2 < 0)
	  s2 = 0;
	else if (s2 > 2) {
	  s2 = 2;
	  p2 = ne - 4;
	  if (p2 > 6)
	    p2 = 6;
	}
      } else {
	s2 = 2;
	p2 = ne - 4;
	if (p2 > 6)
	  p2 = 6;
	else if (p2 < 0) {
	  s2 = ne - 2;
	  p2 = 0;
	  if (s2 < 0)
	    s2 = 0;
	}
      }
    } else {
      s2 = 2;
      p2 = 6;
      if (atn <= 18) {
	if (atn <= 12) {
	  s3 = ne - 10;
	  if (s3 < 0)
	    s3 = 0;
	  else if (s3 > 2) {
	    s3 = 2;
	    p3 = ne - 12;
	    if (p3 > 6)
	      p3 = 6;
	  }
	} else {
	  s3 = 2;
	  p3 = ne - 12;
	  if (p3 > 6)
	    p3 = 6;
	  else if (p3 < 0) {
	    s3 = ne - 10;
	    p3 = 0;
	    if (s3 < 0)
	      s3 = 0;
	  }
	}
      } else {
	s3 = 2;
	p3 = 6;
	if (atn <= 36) {
	  if (atn <= 20) {
	    s4 = ne - 18;
	    if (s4 < 0)
	      s4 = 0;
	    else if (s4 > 2)
	      s4 = 2;
	  } else if (atn <= 30) {
	    s4 = 2;
	    d3 = atn - 20;
	    if (charge == 1)
	      s4 = 1;
	    else if (charge == 2)
	      s4 = 0;
	    else if (charge > 2) {
	      s4 = 0;
	      d3 = d3 - (charge - 2);
	    }
	    if (d3 < 0)
	      d3 = 0;
	    if (s4 > 0 and (d3 == 4 or d3 == 9)) {
	      d3++;
	      s4--;
	    }
	  } else {
	    s4 = 2;
	    d3 = 10;
	    p4 = ne - 30;
	    if (p4 > 6)
	      p4 = 6;
	    else if (p4 < 0) {
	      p4 = 0;
	      s4 = ne - 28;
	      if (s4 < 0)
		s4 = 0;
	    }
	  }
	} else {
	  s4 = 2;
	  d3 = 10;
	  p4 = 6;
	  if (atn <= 54) {
	    if (atn <= 38) {
	      s5 = ne - 36;
	      if (s5 < 0)
		s5 = 0;
	      else if (s5 > 2)
		s5 = 2;
	    } else if (atn <= 48) {
	      s5 = 2;
	      d4 = atn - 38;
	      if (charge == 1)
		s5 = 1;
	      else if (charge == 2)
		s5 = 0;
	      else if (charge > 2) {
		s5 = 0;
		d4 = d4 - (charge - 2);
	      }
	      if (d4 < 0)
		d4 = 0;
	      if (s5 > 0 and (d4 == 4 or d4 == 9)) {
		d4++;
		s5--;
	      }
	    } else {
	      s5 = 2;
	      d4 = 10;
	      p5 = ne - 48;
	      if (p5 > 6)
		p5 = 6;
	      else if (p5 < 0) {
		p5 = 0;
		s5 = ne - 46;
		if (s5 < 0)
		  s5 = 0;
	      }
	    }
	  } else {
	    s5 = 2;
	    d4 = 10;
	    p5 = 6;
	    if (atn <= 86) {
	      if (atn <= 56) {
		s6 = ne - 54;
		if (s6 < 0)
		  s6 = 0;
		else if (s6 > 2)
		  s6 = 2;
	      } else if (atn <= 70) {
		s6 = 2;
		f4 = atn - 56;
		if (charge == 1)
		  s6 = 1;
		else if (charge == 2)
		  s6 = 0;
		else if (charge > 2) {
		  s6 = 0;
		  f4 = f4 - (charge - 2);
		}
		if (f4 < 0)
		  f4 = 0;
		if (s6 > 0 and (f4 == 6 or f4 == 13)) {
		  f4++;
		  s6--;
		}
		if (atn == 57 and f4 == 1) {
		  f4 = 0;
		  d5 = 1;
		}
	      } else if (atn <= 80) {
		s6 = 2;
		f4 = 14;
		d5 = atn - 70;
		if (charge == 1)
		  s6 = 1;
		else if (charge == 2)
		  s6 = 0;
		else if (charge > 2) {
		  s6 = 0;
		  d5 = d5 - (charge - 2);
		}
		if (d5 < 0)
		  d5 = 0;
		if (s6 > 0 and (d5 == 4 or d5 == 9)) {
		  d5++;
		  s6--;
		}
	      } else {
		s6 = 2;
		f4 = 14;
		d5 = 10;
		p6 = ne - 80;
		if (p6 > 6)
		  p6 = 6;
		else if (p6 < 0) {
		  p6 = 0;
		  s6 = ne - 78;
		  if (s6 < 0)
		    s6 = 0;
		}
	      }
	    } else {
	      s6 = 2;
	      f4 = 14;
	      d5 = 10;
	      p6 = 6;
	      if (atn <= 88) {
		s7 = ne - 86;
		if (s7 < 0)
		  s7 = 0;
		else if (s7 > 2)
		  s7 = 2;
	      } else if (atn <= 102) {
		s7 = 2;
		f5 = atn - 56;
		if (charge == 1)
		  s7 = 1;
		else if (charge == 2)
		  s7 = 0;
		else if (charge > 2) {
		  s7 = 0;
		  f5 = f5 - (charge - 2);
		}
		if (f5 < 0)
		  f5 = 0;
		if (s7 > 0 and (f5 == 6 or f5 == 13)) {
		  f5++;
		  s7--;
		}
		if (atn == 89 and f5 == 1) {
		  f5 = 0;
		  d6 = 1;
		}
	      } else { // Up to 112
		s7 = 2;
		f5 = 14;
		d6 = atn - 102;
		if (charge == 1)
		  s7 = 1;
		else if (charge == 2)
		  s7 = 0;
		else if (charge > 2) {
		  s7 = 0;
		  d6 = d6 - (charge - 2);
		}
		if (d6 < 0)
		  d6 = 0;
		if (s7 > 0 and (d6 == 4 or d6 == 9)) {
		  d6++;
		  s7--;
		}
	      }
	    }
	  }
	}
      }
    }
  }
}

void CRYSTAL::scf_data::write_controls(std::ofstream &output) const
{
  if (hamiltonian.hamiltonian == Hamiltonian::Hartree_Fock) {
    switch (hamiltonian.options) {
    case Hamiltonian::restricted:
      output << "RHF\n";
      break;
    case Hamiltonian::ropen_shell:
      output << "ROHF\n";
      break;
    case Hamiltonian::unrestricted:
      output << "UHF\n";
      break;
    }
  } else {
    output << "DFT\n";
    if (hamiltonian.options == Hamiltonian::unrestricted)
      output << "SPIN\n";
    write_dft_functional(output);
    write_dft(output);
    output << "END\n";
  }
  output << "TOLINTEG\n";
  output << tolerances.itol1 << " ";
  output << tolerances.itol2 << " ";
  output << tolerances.itol3 << " ";
  output << tolerances.itol4 << " ";
  output << tolerances.itol5 << '\n';
  if (options.biesplit > 1) {
    output << "BIESPLIT\n";
    output << options.biesplit << '\n';
  }
  if (options.monsplit > 1) {
    output << "MONSPLIT\n";
    output << options.monsplit << '\n';
  }
}

void CRYSTAL::scf_data::write_dft_functional(std::ofstream &output) const
{
  switch (hamiltonian.functional) {
  case Hamiltonian::corr_exch:
    if (hamiltonian.correlation > Hamiltonian::no_correlation)
      output << "CORRELAT\n";
    write_correlation_fn(output);
    if (hamiltonian.exchange > Hamiltonian::hf_ex) {
      output << "EXCHANGE\n";
      write_exchange_fn(output);
    }
    if (hamiltonian.hybrid_function) {
      output << "HYBRID\n";
      output << hamiltonian.hybrid_mixing << '\n';
    }
    break;
  case Hamiltonian::b3lyp:
    output << "B3LYP\n";
    break;
  case Hamiltonian::b3pw:
    output << "B3PW\n";
    break;
  case Hamiltonian::pbe0:
    output << "PBE0\n";
    break;
  case Hamiltonian::pbesol0:
    output << "PBESOL0\n";
    break;
  case Hamiltonian::b1wc:
    output << "B1WC\n";
    break;
  case Hamiltonian::wc1lyp:
    output << "WC1LYP\n";
    break;
  case Hamiltonian::b97h:
    output << "B97H\n";
    break;
  case Hamiltonian::pbe0_third:
    output << "PBE0-13\n";
    break;
  case Hamiltonian::svwn:
    output << "SVWN\n";
    break;
  case Hamiltonian::pbexc:
    output << "PBEXC\n";
    break;
  case Hamiltonian::pbesolxc:
    output << "PBESOLXC\n";
    break;
  case Hamiltonian::soggaxc:
    output << "SOGGAXC\n";
    break;
  case Hamiltonian::hse06:
    output << "HSE06\n";
    break;
  case Hamiltonian::hsesol:
    output << "HSEsol\n";
    break;
  case Hamiltonian::hiss:
    output << "HISS\n";
    break;
  case Hamiltonian::rshxlda:
    output << "RSHXLDA\n";
    break;
  case Hamiltonian::wb97:
    output << "wB97\n";
    break;
  case Hamiltonian::wb97x:
    output << "wB97X\n";
    break;
  case Hamiltonian::lcwblyp:
    output << "LC-wBLYP\n";
    break;
  case Hamiltonian::lcwpbe:
    output << "LC-wPBE\n";
    break;
  case Hamiltonian::lcwpbesol:
    output << "LC-wPBEsol\n";
    break;
  case Hamiltonian::lcblyp:
    output << "LC-BLYP\n";
    break;
  case Hamiltonian::camb3lyp:
    output << "CAM-B3LYP\n";
    break;
  case Hamiltonian::scblyp:
    output << "SC-BLYP\n";
    break;
  case Hamiltonian::m06l:
    output << "M06L\n";
    break;
  case Hamiltonian::m05:
    output << "M05\n";
    break;
  case Hamiltonian::m06:
    output << "M06\n";
    break;
  case Hamiltonian::b2plyp:
    output << "B2PLYP\n";
    break;
  case Hamiltonian::b2gpplyp:
    output << "B2GPPLYP\n";
    break;
  case Hamiltonian::d3_blyp:
    output << "BLYP-D3\n";
    break;
  case Hamiltonian::d3_pbe:
    output << "PBE-D3\n";
    break;
  case Hamiltonian::d3_b3lyp:
    output << "B3LYP-D3\n";
    break;
  case Hamiltonian::d3_pbe0:
    output << "PBE0-D3\n";
    break;
  case Hamiltonian::d3_hse06:
    output << "HSE06-D3\n";
    break;
  case Hamiltonian::d3_hsesol:
    output << "HSEsol-D3\n";
    break;
  case Hamiltonian::d3_m06:
    output << "M06-D3\n";
    break;
  }
}

void CRYSTAL::scf_data::write_correlation_fn(std::ofstream &output) const
{
  switch (hamiltonian.correlation) {
  case Hamiltonian::pz_corr:
    output << "PZ\n";
    break;
  case Hamiltonian::pwlsd_corr:
    output << "PWLSD\n";
    break;
  case Hamiltonian::vwn_corr:
    output << "VWN\n";
    break;
  case Hamiltonian::vbh_corr:
    output << "VBH\n";
    break;
  case Hamiltonian::p86_corr:
    output << "P86\n";
    break;
  case Hamiltonian::pwgga_corr:
    output << "PWGGA\n";
    break;
  case Hamiltonian::lyp_corr:
    output << "LYP\n";
    break;
  case Hamiltonian::pbe_corr:
    output << "PBE\n";
    break;
  default:
    break;
  }
}

void CRYSTAL::scf_data::write_correlation_fn_v7(std::ofstream &output) const
{
  switch (hamiltonian.correlation) {
  case Hamiltonian::pz_corr:
    output << "PZ\n";
    break;
  case Hamiltonian::pwlsd_corr:
    output << "PWLSD\n";
    break;
  case Hamiltonian::vwn_corr:
    output << "VWN\n";
    break;
  case Hamiltonian::vbh_corr:
    output << "VBH\n";
    break;
  case Hamiltonian::p86_corr:
    output << "P86\n";
    break;
  case Hamiltonian::pwgga_corr:
    output << "PWGGA\n";
    break;
  case Hamiltonian::lyp_corr:
    output << "LYP\n";
    break;
  case Hamiltonian::pbe_corr:
    output << "PBE\n";
    break;
  case Hamiltonian::pbesol_corr:
    output << "PBESOL\n";
    break;
  case Hamiltonian::wl_corr:
    output << "WL\n";
    break;
  default:
    break;
  }
}

void CRYSTAL::scf_data::write_exchange_fn(std::ofstream &output) const
{
  switch (hamiltonian.exchange) {
  case Hamiltonian::lda_ex:
    output << "LDA\n";
    break;
  case Hamiltonian::vbh_ex:
    output << "VBH\n";
    break;
  case Hamiltonian::becke_ex:
    output << "BECKE\n";
    break;
  case Hamiltonian::pwgga_ex:
    output << "PWGGA\n";
    break;
  case Hamiltonian::pbe_ex:
    output << "PBE\n";
    break;
  default:
    break;
  }
}

void CRYSTAL::scf_data::write_exchange_v6(std::ofstream &output) const
{
  switch (hamiltonian.exchange) {
  case Hamiltonian::lda_ex:
    output << "LDA\n";
    break;
  case Hamiltonian::vbh_ex:
    output << "VBH\n";
    break;
  case Hamiltonian::becke_ex:
    output << "BECKE\n";
    break;
  case Hamiltonian::pwgga_ex:
    output << "PWGGA\n";
    break;
  case Hamiltonian::pbe_ex:
    output << "PBE\n";
    break;
  case Hamiltonian::wcgga_ex:
    output << "WCGGA\n";
    break;
  default:
    break;
  }
}

void CRYSTAL::scf_data::write_exchange_v7(std::ofstream &output) const
{
  switch (hamiltonian.exchange) {
  case Hamiltonian::lda_ex:
    output << "LDA\n";
    break;
  case Hamiltonian::vbh_ex:
    output << "VBH\n";
    break;
  case Hamiltonian::becke_ex:
    output << "BECKE\n";
    break;
  case Hamiltonian::pwgga_ex:
    output << "PWGGA\n";
    break;
  case Hamiltonian::pbe_ex:
    output << "PBE\n";
    break;
  case Hamiltonian::wcgga_ex:
    output << "WCGGA\n";
    break;
  case Hamiltonian::pbesol_ex:
    output << "PBESOL\n";
    break;
  case Hamiltonian::sogga_ex:
    output << "SOGGA\n";
    break;
  default:
    break;
  }
}

void CRYSTAL::scf_data::write_exchange_v8(std::ofstream &output) const
{
  switch (hamiltonian.exchange) {
  case Hamiltonian::lda_ex:
    output << "LDA\n";
    break;
  case Hamiltonian::vbh_ex:
    output << "VBH\n";
    break;
  case Hamiltonian::becke_ex:
    output << "BECKE\n";
    break;
  case Hamiltonian::pwgga_ex:
    output << "PWGGA\n";
    break;
  case Hamiltonian::pbe_ex:
    output << "PBE\n";
    break;
  case Hamiltonian::wcgga_ex:
    output << "WCGGA\n";
    break;
  case Hamiltonian::pbesol_ex:
    output << "PBESOL\n";
    break;
  case Hamiltonian::sogga_ex:
    output << "SOGGA\n";
    break;
  case Hamiltonian::mpw91_ex:
    output << "mPW91\n";
    break;
  default:
    break;
  }
}

void CRYSTAL::scf_data_v7::write_correlation_fn(std::ofstream &output) const
{
  write_correlation_fn_v7(output);
}

void CRYSTAL::scf_data_v8::write_correlation_fn(std::ofstream &output) const
{
  write_correlation_fn_v7(output);
}

void CRYSTAL::scf_data_v6::write_exchange_fn(std::ofstream &output) const
{
  write_exchange_v6(output);
}

void CRYSTAL::scf_data_v7::write_exchange_fn(std::ofstream &output) const
{
  write_exchange_v7(output);
}

void CRYSTAL::scf_data_v8::write_exchange_fn(std::ofstream &output) const
{
  write_exchange_v8(output);
}

void CRYSTAL::scf_data::write_dft_basis(std::ofstream &output) const
{
  switch (hamiltonian.aux_basis) {
  case Hamiltonian::aux_coarse:
    output << "BASIS\n";
    output << "1\n";
    break;
  case Hamiltonian::aux_medium:
    output << "BASIS\n";
    output << "2\n";
    break;
  case Hamiltonian::aux_fine:
    output << "BASIS\n";
    output << "3\n";
    break;
  default:
    break;
  }
}

void CRYSTAL::scf_data::write_dft_grid(std::ofstream &output) const
{
  switch (hamiltonian.aux_basis) {
  case Hamiltonian::aux_medium:
    output << "LGRID\n";
    break;
  case Hamiltonian::aux_fine:
    output << "XLGRID\n";
    break;
  case Hamiltonian::aux_extra_fine:
    output << "XXLGRID\n";
    break;
  default:
    break;
  }
}

void CRYSTAL::scf_data_v4::write_dft(std::ofstream &output) const
{
  write_dft_basis(output);
}

void CRYSTAL::scf_data_v5::write_dft(std::ofstream &output) const
{
  scf_data::write_v5_dft(output);
}

void CRYSTAL::scf_data_v6::write_dft(std::ofstream &output) const
{
  scf_data::write_v5_dft(output);
}

void CRYSTAL::scf_data::write_v5_dft(std::ofstream &output) const
{
  if (hamiltonian.numerical_dft) {
    output << "NUMERICA\n";
    if (options.calc_gradients or need_gradients())
      output << "NEWTON\n";
    write_dft_grid(output);
  } else {
    output << "FITTING\n";
    write_dft_basis(output);
  }
}

void CRYSTAL::scf_data_v4::write_controls(std::ofstream &output) const
{
  scf_data::write_controls(output);
  output << "END\n";
}

void CRYSTAL::scf_data_v5::write_controls(std::ofstream &output) const
{
  scf_data::write_controls(output);
  scf_data::write_v5_controls(output);
  output << "END\n";
}

void CRYSTAL::scf_data_v6::write_controls(std::ofstream &output) const
{
  scf_data::write_controls(output);
  scf_data::write_v5_controls(output);
}

void CRYSTAL::scf_data::write_v5_controls(std::ofstream &output) const
{
  if (options.direct) {
    output << "SCFDIR\n";
    if (!options.monodirect)
      output << "NOMONDIR\n";
  }
  /*if (options.semidirect) {
    output << "SEMIDIR\n";
    output << options.mem_buffs;
    if (options.direct)
      output << " 0\n";
    else
      output << " 1\n";
  }
  */
}

void CRYSTAL::scf_data::write_shrink(std::ofstream &output) const
{
  if (kpoints.asym) {
    output << "0 0 " << kpoints.isp << '\n';
    output << kpoints.is1 << " " << kpoints.is2;
    output << " " << kpoints.is3 << '\n';
  } else
    output << kpoints.is1 << " 0 " << kpoints.isp << '\n';
}

void CRYSTAL::scf_data::write_v6_shrink(std::ofstream &output) const
{
  output << "SHRINK\n";
  if (kpoints.asym) {
    output << "0 " << kpoints.isp << '\n';
    output << kpoints.is1 << " " << kpoints.is2;
    output << " " << kpoints.is3 << '\n';
  } else
    output << kpoints.is1 << " " << kpoints.isp << '\n';
}

void CRYSTAL::scf_data_v6::write_shrink(std::ofstream &output) const
{
  write_v6_shrink(output);
}

void CRYSTAL::scf_data::write_conv_type(std::ofstream &output) const
{
  // C06 etc shift by 1
  switch (tolerances.converge_method + 1) {
  case Tolerances::c_density_matrix:
    output << "TOLDEP\n";
    output << tolerances.density << '\n';
    break;
  default:
    output << "TOLDEE\n";
    output << tolerances.energy << '\n';
    break;
  }
}

void CRYSTAL::scf_data::write_conv_v4(std::ofstream &output) const
{
  output << "TOLSCF\n";
  output << tolerances.eigen << " " << tolerances.energy << '\n';
}

void CRYSTAL::scf_data::write_conv_v5(std::ofstream &output) const
{
  switch (tolerances.converge_method) {
  case Tolerances::c_energy_and_eig:
    output << "TOLSCF\n";
    output << tolerances.eigen << " " << tolerances.energy << '\n';
    break;
  case Tolerances::c_energy:
    output << "TOLDEE\n";
    output << tolerances.energy << '\n';
    break;
  case Tolerances::c_density_matrix:
    output << "TOLDEP\n";
    output << tolerances.density << '\n';
    break;
  }
}

void CRYSTAL::scf_data_v4::write_conv_type(std::ofstream &output) const
{
  write_conv_v4(output);
}

void CRYSTAL::scf_data_v5::write_conv_type(std::ofstream &output) const
{
  write_conv_v5(output);
}

void CRYSTAL::scf_data::write_no_shift(std::ofstream &output) const
{
}

void CRYSTAL::scf_data_v8::write_no_shift(std::ofstream &output) const
{
  output << "NOSHIFT\n";
}

void CRYSTAL::scf_data::write_scf(std::ofstream &output,
				  const DLV::model *const m) const
{
  write_conv_type(output);
  output << "MAXCYCLE\n";
  output << converge.maxcycles << '\n';
  output << "FMIXING\n";
  output << converge.mixing << '\n';
  if (converge.use_levshift) {
    output << "LEVSHIFT\n";
    output << converge.levshift;
    if (converge.lock_levshift)
      output << " 1\n";
    else
      output << " 0\n";
  } else
    write_no_shift(output);
  if (hamiltonian.options != Hamiltonian::restricted) {
    if (converge.spinlock) {
      output << "SPINLOCK\n";
      output << converge.spin << " " << converge.spin_cycles << '\n';
    }
  }
  if (converge.use_smearing) {
    output << "SMEAR\n";
    output << converge.fermi_smear << '\n';
  }
  if (hamiltonian.hamiltonian == Hamiltonian::Hartree_Fock and
      hamiltonian.options != Hamiltonian::restricted)
    if (print.exchange)
      output << "EXCHGENE\n";
  if (!is_optimise())
    if (print.mulliken)
      output << "PPAN\n";
}

void CRYSTAL::scf_data_v5::write_scf(std::ofstream &output,
				     const DLV::model *const m) const
{
  scf_data::write_scf(output, m);
  scf_data::write_v5_scf(output, m);
}

void CRYSTAL::scf_data_v6::write_scf(std::ofstream &output,
				     const DLV::model *const m) const
{
  scf_data::write_scf(output, m);
  scf_data::write_v5_scf(output, m);
}

void CRYSTAL::scf_data::write_v5_scf(std::ofstream &output,
				     const DLV::model *const m) const
{
  if (options.direct) {
    if (options.deltap) {
      output << "DELTAP\n";
      output << options.deltap_tol << '\n';
    }
  }
  switch (converge.method) {
  case Convergence::anderson:
    output << "ANDERSON\n";
    break;
  default:
    break;
  }
  if (options.calc_gradients or need_gradients())
    output << "GRADCAL\n";
  if (hamiltonian.spin) {
    int_g count = 0;
    int_g *spins = new_local_array1(int_g, natoms);
    m->get_asym_atom_spins(spins, natoms);
    // Count number of spins
    for (int_g i = 0; i < natoms; i++) {
      if (spins[i] != 0)
	count++;
      if (count > 0) {
	output << "ATOMSPIN\n";
	output << count << '\n';
	for (i = 0; i < natoms; i++) {
	  if (spins[i] != 0)
	    output << " " << (i + 1) << " " << spins[i] << "\n";
	}
      }
    }
    delete_local_array(spins);
  }
  if (options.fock_restart)
    output << "GUESSF\n";
  else if (options.density_restart)
    output << "GUESSP\n";
}

void CRYSTAL::scf_data::write_section5(std::ofstream &output) const
{
}

bool CRYSTAL::scf_data::need_gradients() const
{
  return false;
}

bool CRYSTAL::opt_data_v5::need_gradients() const
{
  return opt_data::use_gradients();
}

bool CRYSTAL::opt_data_v6::need_gradients() const
{
  return opt_data::use_gradients();
}

bool CRYSTAL::opt_data_v7::need_gradients() const
{
  return opt_data::use_gradients();
}

bool CRYSTAL::opt_data_v8::need_gradients() const
{
  return opt_data::use_gradients();
}

bool CRYSTAL::scf_data::is_optimise() const
{
  return false;
}

bool CRYSTAL::opt_data_v5::is_optimise() const
{
  return true;
}

bool CRYSTAL::opt_data_v6::is_optimise() const
{
  return true;
}

bool CRYSTAL::opt_data_v7::is_optimise() const
{
  return true;
}

bool CRYSTAL::opt_data_v8::is_optimise() const
{
  return true;
}

bool CRYSTAL::scf_calc::recover(const bool ok, const bool log_ok,
				char message[], const int_g len)
{
  DLV::string id = get_job_id();
  DLV::data_object *data = 0;
  if (log_ok) {
    // logfile
    DLV::string name = get_log_filename();
    if (ok)
      data = new DLV::text_file(name, program_name, id, "SCF output file");
    else
      data = new DLV::text_file(name, program_name, id,
				"SCF(failed) output file");
    attach_data(data);
  }
  if (ok) {
    DLV::string name = DLV::add_full_path(get_outfile_name(0));
    data = new DLV::file_data(name, program_name, wavefn_label);
    attach_data(data);
    //const DLV::model *const m = get_model();
#ifdef ENABLE_DLV_GRAPHICS
    get_display_obj()->set_CRYSTAL_scf();
#endif // ENABLE_DLV_GRAPHICS
  }
  return true;
}

bool CRYSTAL::cphf_calc_v7::recover(const bool ok, const bool log_ok,
				    char message[], const int_g len)
{
  DLV::string id = get_job_id();
  DLV::data_object *data = 0;
  if (log_ok) {
    // logfile
    DLV::string name = get_log_filename();
    if (ok)
      data = new DLV::text_file(name, program_name, id, "CPHF output file");
    else
      data = new DLV::text_file(name, program_name, id,
				"CPHF(failed) output file");
    attach_data(data);
  }
  if (ok) {
    DLV::string name = DLV::add_full_path(get_outfile_name(0));
    data = new DLV::file_data(name, program_name, wavefn_label);
    attach_data(data);
    //const DLV::model *const m = get_model();
#ifdef ENABLE_DLV_GRAPHICS
    get_display_obj()->set_CRYSTAL_scf();
#endif // ENABLE_DLV_GRAPHICS
  }
  return true;
}

bool CRYSTAL::cphf_calc_v8::recover(const bool ok, const bool log_ok,
				    char message[], const int_g len)
{
  DLV::string id = get_job_id();
  DLV::data_object *data = 0;
  if (log_ok) {
    // logfile
    DLV::string name = get_log_filename();
    if (ok)
      data = new DLV::text_file(name, program_name, id, "CPHF output file");
    else
      data = new DLV::text_file(name, program_name, id,
				"CPHF(failed) output file");
    attach_data(data);
  }
  if (ok) {
    DLV::string name = DLV::add_full_path(get_outfile_name(0));
    data = new DLV::file_data(name, program_name, wavefn_label);
    attach_data(data);
    //const DLV::model *const m = get_model();
#ifdef ENABLE_DLV_GRAPHICS
    get_display_obj()->set_CRYSTAL_scf();
#endif // ENABLE_DLV_GRAPHICS
  }
  return true;
}

// Todo - coomon with v6
bool CRYSTAL::opt_calc_v5::recover(const bool no_err, const bool log_ok,
				   char message[], const int_g len)
{
  DLV::string id = get_job_id();
  bool ok = true;
  if (no_err) {
    // structure
    DLV::string label = "CRYSTAL optimise";
    DLV::model *m = read(label, get_outfile_name(0).c_str(),
			 false, message, len);
    ok = (m != 0);
    if (ok) {
      replace_model(m);
      //set_current();
    }
  }
  DLV::data_object *data = 0;
  if (log_ok) {
    // logfile
    DLV::string name = get_log_filename();
    if (no_err)
      data = new DLV::text_file(name, program_name, id,
				"Optimise output file");
    else
      data = new DLV::text_file(name, program_name, id,
				"Optimise(failed) output file");
    attach_data(data);
  }
  if (no_err) {
    // internal data logging
    DLV::string name = get_outfile_name(1);
    if (no_err)
      data = new DLV::text_file(name, program_name, id, "Optimise log file");
    else
      data = new DLV::text_file(name, program_name, id,
				"Optimise(failed) log file");
    attach_data(data);
  }
  return ok;
}

bool CRYSTAL::opt_calc_v6::recover(const bool no_err, const bool log_ok,
				   char message[], const int_g len)
{
  DLV::string id = get_job_id();
  bool ok = true;
  if (no_err) {
    // structure
    DLV::string label = "CRYSTAL optimise";
    DLV::model *m = read(label, get_outfile_name(0).c_str(),
			 false, message, len);
    ok = (m != 0);
    if (ok) {
      replace_model(m);
      //set_current();
    }
  }
  DLV::data_object *data = 0;
  if (log_ok) {
    // logfile
    DLV::string name = get_log_filename();
    if (no_err)
      data = new DLV::text_file(name, program_name, id,
				"Optimise output file");
    else
      data = new DLV::text_file(name, program_name, id,
				"Optimise(failed) output file");
    attach_data(data);
  }
  if (no_err) {
    // internal data logging
    DLV::string name = get_outfile_name(1);
    if (no_err)
      data = new DLV::text_file(name, program_name, id, "Optimise log file");
    else
      data = new DLV::text_file(name, program_name, id,
				"Optimise(failed) log file");
    attach_data(data);
  }
  return ok;
}

bool CRYSTAL::opt_calc_v7::recover(const bool no_err, const bool log_ok,
				   char message[], const int_g len)
{
  DLV::string id = get_job_id();
  bool ok = true;
  if (no_err) {
    // structure
    DLV::string label = "CRYSTAL optimise";
    DLV::model *m = read(label, get_outfile_name(0).c_str(),
			 false, message, len);
    ok = (m != 0);
    if (ok) {
      replace_model(m);
      //set_current();
    }
  }
  DLV::data_object *data = 0;
  if (log_ok) {
    // logfile
    DLV::string name = get_log_filename();
    if (no_err)
      data = new DLV::text_file(name, program_name, id,
				"Optimise output file");
    else
      data = new DLV::text_file(name, program_name, id,
				"Optimise(failed) output file");
    attach_data(data);
  }
  if (no_err) {
    // internal data logging
    DLV::string name = get_outfile_name(1);
    if (no_err)
      data = new DLV::text_file(name, program_name, id, "Optimise log file");
    else
      data = new DLV::text_file(name, program_name, id,
				"Optimise(failed) log file");
    attach_data(data);
  }
  return ok;
}

bool CRYSTAL::opt_calc_v8::recover(const bool no_err, const bool log_ok,
				   char message[], const int_g len)
{
  DLV::string id = get_job_id();
  bool ok = true;
  if (no_err) {
    // structure
    DLV::string label = "CRYSTAL optimise";
    DLV::model *m = read(label, get_outfile_name(0).c_str(),
			 false, message, len);
    ok = (m != 0);
    if (ok) {
      replace_model(m);
      //set_current();
    }
  }
  DLV::data_object *data = 0;
  if (log_ok) {
    // logfile
    DLV::string name = get_log_filename();
    if (no_err)
      data = new DLV::text_file(name, program_name, id,
				"Optimise output file");
    else
      data = new DLV::text_file(name, program_name, id,
				"Optimise(failed) output file");
    attach_data(data);
  }
  if (no_err) {
    // internal data logging
    DLV::string name = get_outfile_name(1);
    if (no_err)
      data = new DLV::text_file(name, program_name, id, "Optimise log file");
    else
      data = new DLV::text_file(name, program_name, id,
				"Optimise(failed) log file");
    attach_data(data);
  }
  return ok;
}

void CRYSTAL::opt_calc::add_standard_data_objects()
{
  DLV::data_object *data = new DLV::atom_and_bond_data();
  if (data != 0) {
    attach_data(data);
#ifdef ENABLE_DLV_GRAPHICS
    render_data(data);
#endif // ENABLE_DLV_GRAPHICS
  }
}

bool CRYSTAL::scf_calc::reload_data(DLV::data_object *data,
				    char message[], const int_g mlen)
{
  strncpy(message, "Reload CRYSTAL SCF not implemented", mlen);
  return false;
}

bool CRYSTAL::opt_calc::reload_data(DLV::data_object *data,
				    char message[], const int_g mlen)
{
  strncpy(message, "Reload CRYSTAL optimisation not implemented", mlen);
  return false;
}

CRYSTAL::Hamiltonian::Hamiltonian()
  : hamiltonian(Hartree_Fock), options(restricted), spin(false),
    functional(corr_exch), correlation(no_correlation), exchange(hf_ex),
    aux_basis(aux_user), hybrid_function(false), hybrid_mixing(0),
    numerical_dft(true)
{
}

CRYSTAL::Hamiltonian::Hamiltonian(const int_g htype, const int_g hoption,
				  const bool use_spin, const int_g func,
				  const int_g corr, const int_g ex,
				  const int_g aux, const bool hybrid,
				  const int_g mix, const int_g num)
  : hamiltonian(Hartree_Fock), options(restricted), spin(false),
    functional(corr_exch), correlation(no_correlation), exchange(hf_ex),
    aux_basis(aux_user), hybrid_function(false), hybrid_mixing(0),
    numerical_dft(true)
{
  switch (htype) {
  case 0:
    hamiltonian = Hartree_Fock;
    switch (hoption) {
    case 0:
      options = restricted;
      break;
    case 1:
      options = ropen_shell;
      break;
    case 2:
      options = unrestricted;
      break;
    }
    break;
  case 1:
    hamiltonian = DFT;
    switch (hoption) {
    case 0:
      options = restricted;
      break;
    case 1:
      options = unrestricted;
      break;
    }
    break;
  }
  if (options != restricted)
    spin = use_spin;
  if (hamiltonian == DFT) {
    switch (func) {
    case 0:
      functional = corr_exch;
      switch (corr) {
      case 0:
        correlation = no_correlation;
        break;
      case 1:
        correlation = pz_corr;
        break;
      case 2:
        correlation = pwlsd_corr;
        break;
      case 3:
        correlation = vwn_corr;
        break;
      case 4:
        correlation = vbh_corr;
        break;
      case 5:
        correlation = p86_corr;
        break;
      case 6:
        correlation = pwgga_corr;
        break;
      case 7:
        correlation = lyp_corr;
        break;
      case 8:
        correlation = pbe_corr;
        break;
      case 9:
        correlation = pbesol_corr;
        break;
      case 10:
        correlation = wl_corr;
        break;
      }
      switch (ex) {
      case 0:
        exchange = hf_ex;
        break;
      case 1:
        exchange = lda_ex;
        break;
      case 2:
        exchange = vbh_ex;
        break;
      case 3:
        exchange = becke_ex;
        break;
      case 4:
       exchange = pwgga_ex;
        break;
      case 5:
        exchange = pbe_ex;
        break;
      case 6:
        exchange = wcgga_ex;
        break;
      case 7:
        exchange = pbesol_ex;
        break;
      case 8:
        exchange = sogga_ex;
        break;
      case 9:
        exchange = mpw91_ex;
        break;
      }
      hybrid_function = hybrid;
      if (hybrid)
        hybrid_mixing = mix;
      else
        hybrid_mixing = 0;
      break;
    case 1:
      functional = b3lyp;
      correlation = no_correlation;
      exchange = hf_ex;
      hybrid_function = false;
      hybrid_mixing = 0;
      break;
    case 2:
      functional = b3pw;
      correlation = no_correlation;
      exchange = hf_ex;
      hybrid_function = false;
      hybrid_mixing = 0;
      break;
    case 3:
      functional = pbe0;
      correlation = no_correlation;
      exchange = hf_ex;
      hybrid_function = false;
      hybrid_mixing = 0;
      break;
    case 4:
      functional = pbesol0;
      correlation = no_correlation;
      exchange = hf_ex;
      hybrid_function = false;
      hybrid_mixing = 0;
      break;
    case 5:
      functional = b1wc;
      correlation = no_correlation;
      exchange = hf_ex;
      hybrid_function = false;
      hybrid_mixing = 0;
      break;
    case 6:
      functional = wc1lyp;
      correlation = no_correlation;
      exchange = hf_ex;
      hybrid_function = false;
      hybrid_mixing = 0;
      break;
    case 7:
      functional = b97h;
      correlation = no_correlation;
      exchange = hf_ex;
      hybrid_function = false;
      hybrid_mixing = 0;
      break;
    case 8:
      functional = pbe0_third;
      correlation = no_correlation;
      exchange = hf_ex;
      hybrid_function = false;
      hybrid_mixing = 0;
      break;
    case 9:
      functional = svwn;
      correlation = no_correlation;
      exchange = hf_ex;
      hybrid_function = false;
      hybrid_mixing = 0;
      break;
    case 10:
      functional = pbexc;
      correlation = no_correlation;
      exchange = hf_ex;
      hybrid_function = false;
      hybrid_mixing = 0;
      break;
    case 11:
      functional = pbesolxc;
      correlation = no_correlation;
      exchange = hf_ex;
      hybrid_function = false;
      hybrid_mixing = 0;
      break;
    case 12:
      functional = soggaxc;
      correlation = no_correlation;
      exchange = hf_ex;
      hybrid_function = false;
      hybrid_mixing = 0;
      break;
    case 13:
      functional = hse06;
      correlation = no_correlation;
      exchange = hf_ex;
      hybrid_function = false;
      hybrid_mixing = 0;
      break;
    case 14:
      functional = hsesol;
      correlation = no_correlation;
      exchange = hf_ex;
      hybrid_function = false;
      hybrid_mixing = 0;
      break;
    case 15:
      functional = hiss;
      correlation = no_correlation;
      exchange = hf_ex;
      hybrid_function = false;
      hybrid_mixing = 0;
      break;
    case 16:
      functional = rshxlda;
      correlation = no_correlation;
      exchange = hf_ex;
      hybrid_function = false;
      hybrid_mixing = 0;
      break;
    case 17:
      functional = wb97;
      correlation = no_correlation;
      exchange = hf_ex;
      hybrid_function = false;
      hybrid_mixing = 0;
      break;
    case 18:
      functional = wb97x;
      correlation = no_correlation;
      exchange = hf_ex;
      hybrid_function = false;
      hybrid_mixing = 0;
      break;
    case 19:
      functional = lcwblyp;
      correlation = no_correlation;
      exchange = hf_ex;
      hybrid_function = false;
      hybrid_mixing = 0;
      break;
    case 20:
      functional = lcwpbe;
      correlation = no_correlation;
      exchange = hf_ex;
      hybrid_function = false;
      hybrid_mixing = 0;
      break;
    case 21:
      functional = lcwpbesol;
      correlation = no_correlation;
      exchange = hf_ex;
      hybrid_function = false;
      hybrid_mixing = 0;
      break;
    case 22:
      functional = lcblyp;
      correlation = no_correlation;
      exchange = hf_ex;
      hybrid_function = false;
      hybrid_mixing = 0;
      break;
    case 23:
      functional = camb3lyp;
      correlation = no_correlation;
      exchange = hf_ex;
      hybrid_function = false;
      hybrid_mixing = 0;
      break;
    case 24:
      functional = scblyp;
      correlation = no_correlation;
      exchange = hf_ex;
      hybrid_function = false;
      hybrid_mixing = 0;
      break;
    }
    switch (num) {
    case 0:
      numerical_dft = true;
      break;
    case 1:
      numerical_dft = false;
      break;
    }
    switch (aux) {
    case 0:
      aux_basis = aux_coarse;
      break;
    case 1:
      aux_basis = aux_medium;
      break;
    case 2:
      aux_basis = aux_fine;
      break;
    case 3:
      aux_basis = aux_extra_fine;
      break;
    }
  }
}

CRYSTAL::Basis::Basis() : default_basis(STO3G), use_default(false)
{
}

CRYSTAL::Basis::Basis(const int_g def)
  : default_basis(STO3G), use_default(false)
{
  switch (def) {
  case 1:
    default_basis = G321;
    break;
  case 2:
    default_basis = G621;
    break;
  case 3:
    default_basis = POB_DZVP;
    break;
  case 4:
    default_basis = POB_DZVPP;
    break;
  case 5:
    default_basis = POB_TZVP;
    break;
  case 6:
    default_basis = STO6G;
    break;
  case 7:
    default_basis = G631ss;
    break;
  case 8:
    default_basis = DEF2_TZVP;
    break;
  default:
    default_basis = STO3G;
    break;
  }
}

CRYSTAL::Tolerances::Tolerances()
  : itol1(6), itol2(6), itol3(6), itol4(6), itol5(12), energy(6),
    eigen(0), density(0), converge_method(c_energy)
{
}

CRYSTAL::Tolerances::Tolerances(const int_g t1, const int_g t2, const int_g t3,
				const int_g t4, const int_g t5, const int_g e,
				const int_g eig, const int_g dp,
				const int_g method)
  : itol1(t1), itol2(t2), itol3(t3), itol4(t4), itol5(t5), energy(0),
    eigen(0), density(0), converge_method(c_energy_and_eig)
{
  switch (method) {
  case 1:
    converge_method = c_energy;
    energy = e;
    eigen = 0;
    density = 0;
    break;
  case 2:
    converge_method = c_density_matrix;
    energy = 0;
    eigen = 0;
    density = dp;
    break;
  default:
    converge_method = c_energy_and_eig;
    energy = e;
    eigen = eig;
    density = 0;
    break;
  }
}

CRYSTAL::Convergence::Convergence()
  : method(linear_mixing), mixing(50), maxcycles(20), levshift(0),
    use_levshift(false), lock_levshift(false), use_smearing(false),
    fermi_smear(0.0), spinlock(false), spin(0), spin_cycles(0)
{
}

CRYSTAL::Convergence::Convergence(const int_g mthd, const int_g lmix,
				  const int_g mcycles, const bool lshift,
				  const int_g shift, const bool lock,
				  const bool smear, const real_g ftemp,
				  const bool spcalc, const bool slock,
				  const int_g sp, const int_g spcycles)
  : method(linear_mixing), mixing(lmix), maxcycles(mcycles), levshift(0),
    use_levshift(lshift), lock_levshift(false), use_smearing(smear),
    fermi_smear(0.0), spinlock(false), spin(0), spin_cycles(0)
{
  switch (mthd) {
  case 1:
    method = anderson;
    break;
  default:
    method = linear_mixing;
    break;
  }
  if (lshift) {
    levshift = shift;
    lock_levshift = lock;
  }
  if (smear)
    fermi_smear = ftemp;
  if (spcalc) {
    spinlock = slock;
    if (spinlock) {
      spin = sp;
      spin_cycles = spcycles;
    }
  }
}

CRYSTAL::Joboptions::Joboptions()
  : direct(true), monodirect(true), semidirect(false), deltap(false),
    fock_restart(false), density_restart(false), mem_buffs(0), deltap_tol(0),
    biesplit(1), monsplit(1), calc_gradients(false)
{
}

CRYSTAL::Joboptions::Joboptions(const bool bied, const bool monod,
				const bool sd, const int_g nbuff,
				const bool dp, const int_g tol,
				const int_g bies, const int_g mons,
				const bool grads, const int_g restart,
				const char file[])
  : direct(bied), monodirect(false), semidirect(sd), deltap(false),
    fock_restart(false), density_restart(false), mem_buffs(0), deltap_tol(0),
    biesplit(1), monsplit(1), calc_gradients(grads)
{
  if (bied) {
    monodirect = monod;
    if (monod)
      monsplit = 1;
    else
      monsplit = mons;
    if (dp) {
      deltap = dp;
      deltap_tol = tol;
    }
    biesplit = 1;
  } else {
    monodirect = false;
    deltap = false;
    deltap_tol = 0;
    biesplit = bies;
    monsplit = mons;
  }
  if (sd)
    mem_buffs = nbuff;
  switch (restart) {
  case 1:
    fock_restart = true;
    density_restart = false;
    break;
  case 2:
    fock_restart = false;
    density_restart = true;
    break;
  default:
    fock_restart = false;
    density_restart = false;
    break;
  }
  if (restart)
    restart_file = file;
}

CRYSTAL::scf_data::~scf_data()
{
  // Todo?
}

bool CRYSTAL::scf_geom::is_geometry() const
{
  return true;
}

void CRYSTAL::scf_geom::inherit_model()
{
  // Null op, we have a new structure
}

void CRYSTAL::scf_geom::inherit_data()
{
  // Todo - I think this is possibly a null op, but see map_data
}


#ifdef DLV_USES_SERIALIZE

template <class Archive>
void CRYSTAL::Hamiltonian::serialize(Archive &ar, const unsigned int version)
{
  ar & hamiltonian;
  ar & options;
  ar & spin;
  ar & functional;
  ar & correlation;
  ar & exchange;
  ar & aux_basis;
  ar & hybrid_function;
  ar & hybrid_mixing;
  ar & numerical_dft;
}

template <class Archive>
void CRYSTAL::Basis::serialize(Archive &ar, const unsigned int version)
{
  ar & default_basis;
  ar & use_default;
}

template <class Archive>
void CRYSTAL::Kpoints::serialize(Archive &ar, const unsigned int version)
{
  ar & is1;
  ar & is2;
  ar & is3;
  ar & isp;
  ar & asym;
}

template <class Archive>
void CRYSTAL::Tolerances::serialize(Archive &ar, const unsigned int version)
{
  ar & itol1;
  ar & itol2;
  ar & itol3;
  ar & itol4;
  ar & itol5;
  ar & energy;
  ar & eigen;
  ar & density;
  ar & converge_method;
}

template <class Archive>
void CRYSTAL::Convergence::serialize(Archive &ar, const unsigned int version)
{
  ar & method;
  ar & mixing;
  ar & maxcycles;
  ar & levshift;
  ar & use_levshift;
  ar & lock_levshift;
  ar & use_smearing;
  ar & fermi_smear;
  ar & spinlock;
  ar & spin;
  ar & spin_cycles;
}

template <class Archive>
void CRYSTAL::Print::serialize(Archive &ar, const unsigned int version)
{
  ar & mulliken;
  ar & exchange;
}

template <class Archive>
void CRYSTAL::Joboptions::serialize(Archive &ar, const unsigned int version)
{
  ar & direct;
  ar & monodirect;
  ar & semidirect;
  ar & deltap;
  ar & fock_restart;
  ar & density_restart;
  ar & mem_buffs;
  ar & deltap_tol;
  ar & biesplit;
  ar & monsplit;
  ar & calc_gradients;
  ar & restart_file;
}

template <class Archive>
void CRYSTAL::Optimise::serialize(Archive &ar, const unsigned int version)
{
  ar & etol;
  ar & xtol;
  ar & gtol;
  ar & iscale;
  ar & opt_type;
  ar & optimiser;
  ar & dlf_type;
  ar & dlf_sstype;
  ar & dlf_initpop;
  ar & dlf_pop;
  ar & dlf_radius;
  ar & dlf_minradius;
  ar & dlf_contractradius;
  ar & dlf_cycles;
  ar & dlf_nsaves;
  ar & dlf_mutation;
  ar & dlf_death;
  ar & dlf_scalef;
  ar & dlf_reset;
}

template <class Archive>
void CRYSTAL::Phonon::serialize(Archive &ar, const unsigned int version)
{
  ar & step;
  ar & deriv;
  ar & intensity;
  ar & dispersion;
  ar & cell;
}

template <class Archive>
void CRYSTAL::Neb::serialize(Archive &ar, const unsigned int version)
{
  ar & nimages;
  ar & iters;
  ar & climb;
  ar & start_ene;
  ar & final_ene;
  ar & separation;
  ar & optimiser;
  ar & convergence;
  ar & tol_energy;
  ar & tol_step;
  ar & tol_grad;
  ar & symmpath;
  ar & usestrsym;
  ar & adapt;
  ar & restart;
  ar & restart_file;
  ar & images_dir;
  ar & end_model;
  ar & mapping;
  ar & natoms;
  ar & nocalc;
}

template <class Archive>
void CRYSTAL::CPHF::serialize(Archive &ar, const unsigned int version)
{
  ar & mixing;
  ar & maxcycles;
  ar & tolalpha;
  ar & toludik;
}

template <class Archive>
void CRYSTAL::basis_info::serialize(Archive &ar, const unsigned int version)
{
  ar & name;
  //ar & atomic_number;
  //ar & basis_index;
  //ar & atn_index;
  ar & pseudo;
  //ar & new_config;
  //ar & def_basis;
  ar & index;
}

template <class Archive>
void CRYSTAL::TDDFT::serialize(Archive &ar, const unsigned int version)
{
  ar & scf_calc;
  ar & tamm_dancoff;
  ar & calc_jdos;
  ar & calc_pes;
  ar & non_direct;
  ar & diag_method;
  ar & diag_excitation;
  ar & diag_niterations;
  ar & diag_space;
  ar &diag_convergence;
}

template <class Archive>
void CRYSTAL::scf_data::serialize(Archive &ar, const unsigned int version)
{
  ar & hamiltonian;
  ar & basis;
  ar & kpoints;
  ar & tolerances;
  ar & converge;
  ar & print;
  ar & options;
  ar & natoms;
  // Todo basis?
  ar & wavefunction;
  ar & analyse;
}

template <class Archive>
void CRYSTAL::scf_data_v4::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::scf_data>(*this);
}

template <class Archive>
void CRYSTAL::scf_data_v5::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::scf_data>(*this);
}

template <class Archive>
void CRYSTAL::scf_data_v6::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::scf_data>(*this);
}

template <class Archive>
void CRYSTAL::scf_data_v7::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::scf_data_v6>(*this);
}

template <class Archive>
void CRYSTAL::scf_data_v8::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::scf_data_v7>(*this);
}

template <class Archive>
void CRYSTAL::scf_calc::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::calculate>(*this);
  ar & show_basis;
}

template <class Archive>
void CRYSTAL::scf_geom::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<scf_calc>(*this);
}

template <class Archive>
void CRYSTAL::scf_calc_v4::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<CRYSTAL::scf_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::scf_data_v4>(*this);
}

template <class Archive>
void CRYSTAL::scf_calc_v4::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::scf_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::scf_data_v4>(*this);
#ifdef ENABLE_DLV_GRAPHICS
  get_display_obj()->set_CRYSTAL_scf();
#endif // ENABLE_DLV_GRAPHICS
}

template <class Archive>
void CRYSTAL::scf_calc_v5::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<CRYSTAL::scf_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::scf_data_v5>(*this);
}

template <class Archive>
void CRYSTAL::scf_calc_v5::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::scf_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::scf_data_v5>(*this);
#ifdef ENABLE_DLV_GRAPHICS
  get_display_obj()->set_CRYSTAL_scf();
#endif // ENABLE_DLV_GRAPHICS
}

template <class Archive>
void CRYSTAL::scf_calc_v6::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<CRYSTAL::scf_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::scf_data_v6>(*this);
}

template <class Archive>
void CRYSTAL::scf_calc_v6::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::scf_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::scf_data_v6>(*this);
#ifdef ENABLE_DLV_GRAPHICS
  get_display_obj()->set_CRYSTAL_scf();
#endif // ENABLE_DLV_GRAPHICS
}

template <class Archive>
void CRYSTAL::scf_calc_v7::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<CRYSTAL::scf_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::scf_data_v7>(*this);
}

template <class Archive>
void CRYSTAL::scf_calc_v7::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::scf_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::scf_data_v7>(*this);
#ifdef ENABLE_DLV_GRAPHICS
  get_display_obj()->set_CRYSTAL_scf();
#endif // ENABLE_DLV_GRAPHICS
}

template <class Archive>
void CRYSTAL::scf_calc_v8::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<CRYSTAL::scf_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::scf_data_v8>(*this);
}

template <class Archive>
void CRYSTAL::scf_calc_v8::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::scf_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::scf_data_v8>(*this);
#ifdef ENABLE_DLV_GRAPHICS
  get_display_obj()->set_CRYSTAL_scf();
#endif // ENABLE_DLV_GRAPHICS
}

template <class Archive>
void CRYSTAL::cphf_data::serialize(Archive &ar, const unsigned int version)
{
  ar & cphf;
}

template <class Archive>
void CRYSTAL::cphf_data_v7::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::scf_data_v7>(*this);
  ar & boost::serialization::base_object<CRYSTAL::cphf_data>(*this);
}

template <class Archive>
void CRYSTAL::cphf_data_v8::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::scf_data_v8>(*this);
  ar & boost::serialization::base_object<CRYSTAL::cphf_data>(*this);
}

template <class Archive>
void CRYSTAL::cphf_calc::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::scf_calc>(*this);
}

template <class Archive>
void CRYSTAL::cphf_calc_v7::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<CRYSTAL::cphf_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::cphf_data_v7>(*this);
}

template <class Archive>
void CRYSTAL::cphf_calc_v7::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::cphf_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::cphf_data_v7>(*this);
#ifdef ENABLE_DLV_GRAPHICS
  get_display_obj()->set_CRYSTAL_scf();
#endif // ENABLE_DLV_GRAPHICS
}

template <class Archive>
void CRYSTAL::cphf_calc_v8::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<CRYSTAL::cphf_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::cphf_data_v8>(*this);
}

template <class Archive>
void CRYSTAL::cphf_calc_v8::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::cphf_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::cphf_data_v8>(*this);
#ifdef ENABLE_DLV_GRAPHICS
  get_display_obj()->set_CRYSTAL_scf();
#endif // ENABLE_DLV_GRAPHICS
}

template <class Archive>
void CRYSTAL::opt_data::serialize(Archive &ar, const unsigned int version)
{
  ar & optimise;
}

template <class Archive>
void CRYSTAL::opt_data_v5::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::scf_data_v5>(*this);
  ar & boost::serialization::base_object<CRYSTAL::opt_data>(*this);
}

template <class Archive>
void CRYSTAL::opt_data_v6::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::scf_data_v6>(*this);
  ar & boost::serialization::base_object<CRYSTAL::opt_data>(*this);
}

template <class Archive>
void CRYSTAL::opt_data_v7::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::scf_data_v7>(*this);
  ar & boost::serialization::base_object<CRYSTAL::opt_data>(*this);
}

template <class Archive>
void CRYSTAL::opt_data_v8::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::scf_data_v8>(*this);
  ar & boost::serialization::base_object<CRYSTAL::opt_data>(*this);
}

template <class Archive>
void CRYSTAL::opt_calc::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::scf_geom>(*this);
}

template <class Archive>
void CRYSTAL::opt_calc_v5::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::opt_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::opt_data_v5>(*this);
}

template <class Archive>
void CRYSTAL::opt_calc_v6::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::opt_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::opt_data_v6>(*this);
}

template <class Archive>
void CRYSTAL::opt_calc_v7::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::opt_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::opt_data_v7>(*this);
}

template <class Archive>
void CRYSTAL::opt_calc_v8::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::opt_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::opt_data_v8>(*this);
}

//BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::Phonon)
//BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::TDDFT)
//BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::Neb)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::scf_geom)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::scf_calc_v4)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::scf_calc_v5)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::scf_calc_v6)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::scf_calc_v7)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::scf_calc_v8)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::cphf_calc_v7)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::cphf_calc_v8)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::opt_calc_v5)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::opt_calc_v6)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::opt_calc_v7)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::opt_calc_v8)

DLV_SUPPRESS_TEMPLATES(CRYSTAL::calculate)

DLV_NORMAL_EXPLICIT(CRYSTAL::Phonon)
DLV_NORMAL_EXPLICIT(CRYSTAL::TDDFT)
//DLV_NORMAL_EXPLICIT(CRYSTAL::Neb)
DLV_SPLIT_EXPLICIT(CRYSTAL::scf_calc_v4)
DLV_SPLIT_EXPLICIT(CRYSTAL::scf_calc_v5)
DLV_SPLIT_EXPLICIT(CRYSTAL::scf_calc_v6)
DLV_SPLIT_EXPLICIT(CRYSTAL::scf_calc_v7)
DLV_SPLIT_EXPLICIT(CRYSTAL::scf_calc_v8)
DLV_SPLIT_EXPLICIT(CRYSTAL::cphf_calc_v7)
DLV_SPLIT_EXPLICIT(CRYSTAL::cphf_calc_v8)
DLV_NORMAL_EXPLICIT(CRYSTAL::opt_calc_v5)
DLV_NORMAL_EXPLICIT(CRYSTAL::opt_calc_v6)
DLV_NORMAL_EXPLICIT(CRYSTAL::opt_calc_v7)
DLV_NORMAL_EXPLICIT(CRYSTAL::opt_calc_v8)

#endif // DLV_USES_SERIALIZE
