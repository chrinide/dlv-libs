
#ifndef CRYSTAL_DENSITY_OF_STATES
#define CRYSTAL_DENSITY_OF_STATES

namespace CRYSTAL {

  const int_g max_projections = 64;

  // Todo - could be simplified
  class dos_file {
  };

  class dos_file_v4 : public dos_file, public plot25 {
  protected:
    class DLV::dos_plot *read_data(DLV::operation *op, DLV::electron_dos *plot,
				   const char filename[], const DLV::string id,
				   const DLV::string *labels, char message[],
				   const int_g mlen);
  };

  class dos_file_v5 : public dos_file {
  protected:
    class DLV::dos_plot *read_data(DLV::operation *op, DLV::electron_dos *plot,
				   const char filename[], const DLV::string id,
				   const DLV::string *labels,
				   const bool use_fermi, const real_g ef,
				   char message[], const int_g mlen);
  };

  class dos_data {
  public:
    // serialization
    virtual ~dos_data();

  protected:
    dos_data();

    void set_params(const int_g np, const int_g npoly, const int_g bmin,
		    const int_g bmax, const real_g emin, const real_g emax,
		    const bool use_e);
    void write_dos(std::ofstream &output, const DLV::operation *op,
		   const DLV::model *const structure);
    virtual void write_file_flag(std::ofstream &output) const = 0;
    void reset_projections(const int_g ptype, bool &toggle);
    bool add_projection(class wavefn_calc *wavefn, const DLV::model *const m,
			int_g &nproj, DLV::string &pname,
			const int_g orbitals[], const int_g norbitals,
			const bool all_atoms, const bool orb_atoms,
			const bool orb_states, const DLV::atom_integers *data,
			int_g &ptype, char message[], const int_g mlen);

    const DLV::string *get_labels() const;
    int_g get_proj_type() const;

  private:
    int_g proj_type;
    int_g nprojections;
    int_g npoints;
    int_g npolynomials;
    int_g band_min;
    int_g band_max;
    std::list<int> projections[max_projections];
    DLV::string labels[max_projections];
    real_g energy_min;
    real_g energy_max;
    bool use_energy;

    bool add_atom_projection(const DLV::model *const m, int_g &nproj,
			     DLV::string &pname, const bool all_atoms,
			     const DLV::atom_integers *data,
			     char message[], const int_g mlen);
    bool add_orbital_projection(class wavefn_calc *wavefn,
				const DLV::model *const m, int_g &nproj,
				DLV::string &pname, const int_g orbitals[],
				const int_g norbitals, const bool all_atoms,
				const bool expand_atoms,
				const bool expand_states,
				const DLV::atom_integers *data);

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class dos_data_v4 : public dos_data, public dos_file_v4,
		      virtual public property_data {
  protected:
    dos_data_v4(const Density_Matrix &dm, const NewK &nk);

    void set_params(const int_g np, const int_g npoly, const int_g bmin,
    		    const int_g bmax, const real_g emin, const real_g emax,
		    const bool use_e, const Density_Matrix &dm, const NewK &nk);
    void write_input(const DLV::string filename, const DLV::operation *op,
		     const DLV::model *const structure, const bool get_fermi,
		     const bool is_bin, char message[], const int_g mlen);
    void write_file_flag(std::ofstream &output) const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class dos_data_v5 : public dos_data, public dos_file_v5,
		      virtual public property_data {
  public: // for serialization
    dos_data_v5(const Density_Matrix &dm, const NewK &nk);

  protected:
    void set_params(const int_g np, const int_g npoly, const int_g bmin,
    		    const int_g bmax, const real_g emin, const real_g emax,
		    const bool use_e, const Density_Matrix &dm, const NewK &nk);
    void write_input(const DLV::string filename, const DLV::operation *op,
		     const DLV::model *const structure, const bool get_fermi,
		     const bool is_bin, char message[], const int_g mlen);
    void write_file_flag(std::ofstream &output) const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class dos_data_v6 : public dos_data, public dos_file_v5,
		      virtual public property_data_v6 {
  public: // for serialization
    dos_data_v6(const Density_Matrix &dm, const NewK &nk);

  protected:
    void set_params(const int_g np, const int_g npoly, const int_g bmin,
    		    const int_g bmax, const real_g emin, const real_g emax,
		    const bool use_e, const Density_Matrix &dm, const NewK &nk);
    void write_input(const DLV::string filename, const DLV::operation *op,
		     const DLV::model *const structure, const bool get_fermi,
		     const bool is_bin, char message[], const int_g mlen);
    void write_file_flag(std::ofstream &output) const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_dos_data : public DLV::load_data_op {
  public:
    static DLV::operation *create(const char filename[], const int_g version,
				  char message[], const int_g mlen);

  protected:
    load_dos_data(const char file[]);

    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_dos_v4 : public load_dos_data, public dos_file_v4 {
  public:
    static DLV::operation *create(const char filename[],
				  char message[], const int_g mlen);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

    //public for serialization
    load_dos_v4(const char file[]);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_dos_v5 : public load_dos_data, public dos_file_v5 {
  public:
    static DLV::operation *create(const char filename[],
				  char message[], const int_g mlen);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

    //public for serialization
    load_dos_v5(const char file[]);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_dos_v7 : public load_dos_data, public dos_file_v5 {
  public:
    static DLV::operation *create(const char filename[],
				  char message[], const int_g mlen);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

    //public for serialization
    load_dos_v7(const char file[]);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class dos_calc : public property_calc {
  public:
    static DLV::operation *create(const int_g version,
				  char message[], const int_g mlen);
    static bool reset_projections(const int_g ptype, char message[],
				  const int_g mlen);
    static bool add_projection(int_g &nproj, DLV::string &pname,
			       const int_g orbitals[], const int_g norbitals,
			       const bool all_atoms, const bool orb_atoms,
			       const bool orb_states, char message[],
			       const int_g mlen);
    static bool calculate(const int_g np, const int_g npoly, const int_g bmin,
			  const int_g bmax, const real_g emin,
			  const real_g emax, const bool use_e,
			  const Density_Matrix &dm, const NewK &nk,
			  const int_g version, const DLV::job_setup_data &job,
			  const bool extern_job, const char extern_dir[],
			  char message[], const int_g mlen);
    static bool list_orbitals(int_g &nproj, DLV::string * &pname,
			      const bool all_atoms, const bool orb_atoms,
			      const bool orb_states, char message[],
			      const int_g mlen);

  protected:
    DLV::string get_name() const;
    virtual void set_params(const int_g np, const int_g npoly, const int_g bmin,
			    const int_g bmax, const real_g emin,
			    const real_g emax, const bool use_e,
			    const Density_Matrix &dm, const NewK &nk) = 0;
    virtual void add_dos_file(const char tag[], const bool is_local) = 0;
    bool create_files(const bool is_parallel, const bool is_local,
		      char message[], const int_g mlen);
    virtual void write(const DLV::string filename, const DLV::operation *op,
		       const DLV::model *const structure,
		       char message[], const int_g mlen) = 0;
    virtual int_g get_proj_type() const = 0;
    virtual void reset_projections(const int_g ptype) = 0;
    virtual bool add_projection(class wavefn_calc *wavefn,
				const DLV::model *const m, int_g &nproj,
				DLV::string &pname, const int_g orbitals[],
				const int_g norbitals, const bool all_atoms,
				const bool orb_atoms, const bool orb_states,
				const DLV::atom_integers *data, int_g &ptype,
				char message[], const int_g mlen) = 0;
    friend class band_and_dos;
    bool list_orbital_data(int_g &nproj, DLV::string * &pname,
			   const bool all_atoms, const bool orb_atoms,
			   const bool orb_states, char message[],
			   const int_g mlen);
    static bool list_orbitals(class wavefn_calc *wavefn,
			      const DLV::model *const m,
			      const DLV::atom_integers *data, int_g &nproj,
			      DLV::string * &pname, const bool all_atoms,
			      const bool expand_atoms,
			      const bool expand_states);
    void atom_selections_changed();
    bool is_binary() const;

  private:
    bool binary_wvfn;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class dos_calc_v4 : public dos_calc, public dos_data_v4 {
  public:
    dos_calc_v4(const Density_Matrix &dm, const NewK &nk);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    void set_params(const int_g np, const int_g npoly, const int_g bmin,
		    const int_g bmax, const real_g emin, const real_g emax,
		    const bool use_e, const Density_Matrix &dm, const NewK &nk);
    void add_dos_file(const char tag[], const bool is_local);
    void write(const DLV::string filename, const DLV::operation *op,
	       const DLV::model *const structure,
	       char message[], const int_g mlen);
    int_g get_proj_type() const;
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);
    void reset_projections(const int_g ptype);
    bool add_projection(class wavefn_calc *wavefn, const DLV::model *const m,
			int_g &nproj, DLV::string &pname,
			const int_g orbitals[], const int_g norbitals,
			const bool all_atoms, const bool orb_atoms,
			const bool orb_states, const DLV::atom_integers *data,
			int_g &ptype, char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class dos_calc_v5 : public dos_calc, public dos_data_v5 {
  public:
    dos_calc_v5(const Density_Matrix &dm, const NewK &nk);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    void set_params(const int_g np, const int_g npoly, const int_g bmin,
		    const int_g bmax, const real_g emin, const real_g emax,
		    const bool use_e, const Density_Matrix &dm, const NewK &nk);
    void add_dos_file(const char tag[], const bool is_local);
    void write(const DLV::string filename, const DLV::operation *op,
	       const DLV::model *const structure,
	       char message[], const int_g mlen);
    int_g get_proj_type() const;
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);
    void reset_projections(const int_g ptype);
    bool add_projection(class wavefn_calc *wavefn, const DLV::model *const m,
			int_g &nproj, DLV::string &pname,
			const int_g orbitals[], const int_g norbitals,
			const bool all_atoms, const bool orb_atoms,
			const bool orb_states, const DLV::atom_integers *data,
			int_g &ptype, char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class dos_calc_v6 : public dos_calc, public dos_data_v6 {
  public:
    dos_calc_v6(const Density_Matrix &dm, const NewK &nk);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    void set_params(const int_g np, const int_g npoly, const int_g bmin,
		    const int_g bmax, const real_g emin, const real_g emax,
		    const bool use_e, const Density_Matrix &dm, const NewK &nk);
    void add_dos_file(const char tag[], const bool is_local);
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void write(const DLV::string filename, const DLV::operation *op,
	       const DLV::model *const structure,
	       char message[], const int_g mlen);
    int_g get_proj_type() const;
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);
    void reset_projections(const int_g ptype);
    bool add_projection(class wavefn_calc *wavefn, const DLV::model *const m,
			int_g &nproj, DLV::string &pname,
			const int_g orbitals[], const int_g norbitals,
			const bool all_atoms, const bool orb_atoms,
			const bool orb_states, const DLV::atom_integers *data,
			int_g &ptype, char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class dos_calc_v7 : public dos_calc, public dos_data_v6 {
  public:
    dos_calc_v7(const Density_Matrix &dm, const NewK &nk);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    void set_params(const int_g np, const int_g npoly, const int_g bmin,
		    const int_g bmax, const real_g emin, const real_g emax,
		    const bool use_e, const Density_Matrix &dm, const NewK &nk);
    void add_dos_file(const char tag[], const bool is_local);
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void write(const DLV::string filename, const DLV::operation *op,
	       const DLV::model *const structure,
	       char message[], const int_g mlen);
    int_g get_proj_type() const;
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);
    void reset_projections(const int_g ptype);
    bool add_projection(class wavefn_calc *wavefn, const DLV::model *const m,
			int_g &nproj, DLV::string &pname,
			const int_g orbitals[], const int_g norbitals,
			const bool all_atoms, const bool orb_atoms,
			const bool orb_states, const DLV::atom_integers *data,
			int_g &ptype, char message[], const int_g mlen);

  private:
    real_g shift;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(CRYSTAL::dos_data_v5)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::dos_data_v6)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::load_dos_v4)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::load_dos_v5)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::load_dos_v7)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::dos_calc_v4)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::dos_calc_v5)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::dos_calc_v6)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::dos_calc_v7)
#endif // DLV_USES_SERIALIZE

inline CRYSTAL::load_dos_data::load_dos_data(const char file[])
  : load_data_op(file)
{
}

inline CRYSTAL::load_dos_v4::load_dos_v4(const char file[])
  : load_dos_data(file)
{
}

inline CRYSTAL::load_dos_v5::load_dos_v5(const char file[])
  : load_dos_data(file)
{
}

inline CRYSTAL::load_dos_v7::load_dos_v7(const char file[])
  : load_dos_data(file)
{
}

inline CRYSTAL::dos_data::dos_data()
  : proj_type(0), nprojections(0), npoints(0),
    npolynomials(0), band_min(0), band_max(0)
{
}

inline void CRYSTAL::dos_data::set_params(const int_g np, const int_g npoly,
					  const int_g bmin, const int_g bmax,
					  const real_g emin, const real_g emax,
					  const bool use_e)
{
  npoints = np;
  npolynomials = npoly;
  band_min = bmin;
  band_max = bmax;
  energy_min = emin;
  energy_max = emax;
  use_energy = use_e;
}

inline const DLV::string *CRYSTAL::dos_data::get_labels() const
{
  return labels;
}

inline CRYSTAL::int_g CRYSTAL::dos_data::get_proj_type() const
{
  return proj_type;
}

inline void CRYSTAL::dos_data_v4::set_params(const int_g np, const int_g npoly,
					     const int_g bmin, const int_g bmax,
					     const real_g emin,
					     const real_g emax,
					     const bool use_e,
					     const Density_Matrix &dm,
					     const NewK &nk)
{
  dos_data::set_params(np, npoly, bmin, bmax, emin, emax, use_e);
  property_data::set_params(dm, nk);
}

inline void CRYSTAL::dos_data_v5::set_params(const int_g np, const int_g npoly,
					     const int_g bmin, const int_g bmax,
					     const real_g emin,
					     const real_g emax,
					     const bool use_e,
					     const Density_Matrix &dm,
					     const NewK &nk)
{
  dos_data::set_params(np, npoly, bmin, bmax, emin, emax, use_e);
  property_data::set_params(dm, nk);
}

inline void CRYSTAL::dos_data_v6::set_params(const int_g np, const int_g npoly,
					     const int_g bmin, const int_g bmax,
					     const real_g emin,
					     const real_g emax,
					     const bool use_e,
					     const Density_Matrix &dm,
					     const NewK &nk)
{
  dos_data::set_params(np, npoly, bmin, bmax, emin, emax, use_e);
  property_data_v6::set_params(dm, nk);
}

inline CRYSTAL::dos_data_v4::dos_data_v4(const Density_Matrix &dm,
					 const NewK &nk)
  : property_data(dm, nk)
{
}

inline CRYSTAL::dos_data_v5::dos_data_v5(const Density_Matrix &dm,
					 const NewK &nk)
  : property_data(dm, nk)
{
}

inline CRYSTAL::dos_data_v6::dos_data_v6(const Density_Matrix &dm,
					 const NewK &nk)
  : property_data_v6(dm, nk)
{
}

inline bool CRYSTAL::dos_calc::is_binary() const
{
  return binary_wvfn;
}

inline CRYSTAL::dos_calc_v4::dos_calc_v4(const Density_Matrix &dm,
					 const NewK &nk)
  : property_data(dm, nk), dos_data_v4(dm, nk)
{
}

inline CRYSTAL::dos_calc_v5::dos_calc_v5(const Density_Matrix &dm,
					 const NewK &nk)
  : property_data(dm, nk), dos_data_v5(dm, nk)
{
}

inline CRYSTAL::dos_calc_v6::dos_calc_v6(const Density_Matrix &dm,
					 const NewK &nk)
  : property_data_v6(dm, nk), dos_data_v6(dm, nk)
{
}

inline CRYSTAL::dos_calc_v7::dos_calc_v7(const Density_Matrix &dm,
					 const NewK &nk)
  : property_data_v6(dm, nk), dos_data_v6(dm, nk), shift(0.0)
{
}

#endif // CRYSTAL_DENSITY_OF_STATES
