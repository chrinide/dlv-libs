
#ifndef CRYSTAL_BAND_STRUCTURE
#define CRYSTAL_BAND_STRUCTURE

namespace CRYSTAL {

  class band_file {
  };

  class band_file_v4 : public band_file, public plot25 {
  protected:
    class DLV::panel_plot *read_data(DLV::operation *op,
				     class DLV::electron_bands *bands,
				     const char filename[],
				     const DLV::string id, const int_g band_min,
				     const int_g nkp,
				     const DLV::string klabels[],
				     char message[], const int_g mlen);

  private:
    static void get_data_info(std::ifstream &data_file, int_g &nblocks,
			      int_g &npts, int_g &nbands, bool &spin);
  };

  class band_file_v5 : public band_file {
  protected:
    class DLV::panel_plot *read_data(DLV::operation *op,
				     class DLV::electron_bands *bands,
				     const char filename[],
				     const DLV::string id,
				     const int_g band_min, const bool use_fermi,
				     const real_g ef, const int_g nkp,
				     const DLV::string klabels[],
				     char message[], const int_g mlen,
				     DLV::dos_plot *dos = 0);
  };

  struct kpoint_data {
    int_g denom;
    int_g point[3];
    DLV::string label;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class band_data {
  protected:
    band_data(const int_g np, const int_g bmin, const int_g bmax);

    void set_params(const int_g np, const int_g bmin, const int_g bmax);
    bool set_kpath(const char kpath[], char message[], const int_g mlen);
    int_g find_shrink() const;
    void write_band(std::ofstream &output, const DLV::operation *op,
		    const DLV::model *const structure);
    int_g get_min_band() const;
    int_g get_nkpoints() const;
    DLV::string get_klabel(const int_g index) const;
    real_g get_shift() const;
    void set_shift(const real_g s);

  private:
    int_g npoints;
    int_g band_min;
    int_g band_max;
    int_g nkpoints;
    kpoint_data *kpoints;
    real_g shift;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class band_data_v4 : public band_data, virtual public property_data {
  public: // for serialization
    band_data_v4(const int_g np, const int_g bmin, const int_g bmax,
		 const Density_Matrix &dm, const NewK &nk);

  protected:
    void write_input(const DLV::string filename, const char path[],
		     const DLV::operation *op,
		     const DLV::model *const structure, const bool get_fermi,
		     const bool is_bin, char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class band_data_v6 : public band_data, virtual public property_data_v6 {
  public: // for serialization
    band_data_v6(const int_g np, const int_g bmin, const int_g bmax,
		 const Density_Matrix &dm, const NewK &nk);

  protected:
    void write_input(const DLV::string filename, const char path[],
		     const DLV::operation *op,
		     const DLV::model *const structure, const bool get_fermi,
		     const bool is_bin, char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_band_data : public DLV::load_data_op {
  public:
    static DLV::operation *create(const char filename[], const int_g version,
				  char message[], const int_g mlen);

  protected:
    load_band_data(const char file[]);

    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_band_v4 : public load_band_data, public band_file_v4 {
  public:
    static DLV::operation *create(const char filename[],
				  char message[], const int_g mlen);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

    // public for serialization
    load_band_v4(const char file[]);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_band_v5 : public load_band_data, public band_file_v5 {
  public:
    static DLV::operation *create(const char filename[],
				  char message[], const int_g mlen);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

    // Public for serialization
    load_band_v5(const char file[]);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class band_calc : public property_calc {
  public:
    static DLV::operation *create(const int_g np, const int_g bmin,
				  const int_g bmax, const char path[],
				  const Density_Matrix &dm, const NewK &nk,
				  const int_g version,
				  const DLV::job_setup_data &job,
				  const bool extern_job,
				  const char extern_dir[],
				  char message[], const int_g mlen);

#ifdef ENABLE_DLV_GRAPHICS
    static DLV::operation *show_path(const char kpath[],
				     char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  protected:
    DLV::string get_name() const;
    virtual void add_band_file(const char tag[], const bool is_local) = 0;
    bool create_files(const bool is_parallel, const bool is_local,
		      char message[], const int_g mlen);
    virtual void write(const DLV::string filename, const char path[],
		       const DLV::operation *op,
		       const DLV::model *const structure,
		       char message[], const int_g mlen) = 0;
    bool is_binary() const;

  private:
    bool binary_wvfn;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class band_calc_v4 : public band_calc, public band_file_v4,
		       public band_data_v4 {
  public:
    band_calc_v4(const int_g np, const int_g bmin, const int_g bmax,
		 const Density_Matrix &dm, const NewK &nk);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    void add_band_file(const char tag[], const bool is_local);
    void write(const DLV::string filename, const char path[],
	       const DLV::operation *op, const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class band_calc_v5 : public band_calc, public band_file_v5,
		       public band_data_v4 {
  public:
    band_calc_v5(const int_g np, const int_g bmin, const int_g bmax,
		 const Density_Matrix &dm, const NewK &nk);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    void add_band_file(const char tag[], const bool is_local);
    void write(const DLV::string filename, const char path[],
	       const DLV::operation *op, const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class band_calc_v6 : public band_calc, public band_file_v5,
		       public band_data_v6 {
  public:
    band_calc_v6(const int_g np, const int_g bmin, const int_g bmax,
		 const Density_Matrix &dm, const NewK &nk);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    void add_band_file(const char tag[], const bool is_local);
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void write(const DLV::string filename, const char path[],
	       const DLV::operation *op, const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(CRYSTAL::band_data_v4)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::band_data_v6)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::load_band_v4)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::load_band_v5)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::band_calc_v4)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::band_calc_v5)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::band_calc_v6)
#endif // DLV_USES_SERIALIZE

inline CRYSTAL::band_data::band_data(const int_g np, const int_g bmin,
				     const int_g bmax)
  : npoints(np), band_min(bmin), band_max(bmax), nkpoints(0), kpoints(0)
{
}

inline CRYSTAL::band_data_v4::band_data_v4(const int_g np, const int_g bmin,
					   const int_g bmax,
					   const Density_Matrix &dm,
					   const NewK &nk)
  : property_data(dm, nk), band_data(np, bmin, bmax)
{
}

inline CRYSTAL::band_data_v6::band_data_v6(const int_g np, const int_g bmin,
					   const int_g bmax,
					   const Density_Matrix &dm,
					   const NewK &nk)
  : property_data_v6(dm, nk), band_data(np, bmin, bmax)
{
}

inline void CRYSTAL::band_data::set_params(const int_g np, const int_g bmin,
					   const int_g bmax)
{
  npoints = np;
  band_min = bmin;
  band_max = bmax;
}

inline CRYSTAL::real_g CRYSTAL::band_data::get_shift() const
{
  return shift;
}

inline void CRYSTAL::band_data::set_shift(const real_g s)
{
  shift = s;
}

inline CRYSTAL::int_g CRYSTAL::band_data::get_min_band() const
{
  return band_min;
}

inline CRYSTAL::int_g CRYSTAL::band_data::get_nkpoints() const
{
  return nkpoints;
}

inline DLV::string CRYSTAL::band_data::get_klabel(const int_g index) const
{
  return kpoints[index].label;
}

inline CRYSTAL::load_band_data::load_band_data(const char file[])
  : load_data_op(file)
{
}

inline CRYSTAL::load_band_v4::load_band_v4(const char file[])
  : load_band_data(file)
{
}

inline CRYSTAL::load_band_v5::load_band_v5(const char file[])
  : load_band_data(file)
{
}

inline bool CRYSTAL::band_calc::is_binary() const
{
  return binary_wvfn;
}

inline CRYSTAL::band_calc_v4::band_calc_v4(const int_g np, const int_g bmin,
					   const int_g bmax,
					   const Density_Matrix &dm,
					   const NewK &nk)
  : property_data(dm ,nk), band_data_v4(np, bmin, bmax, dm, nk)
{
}

inline CRYSTAL::band_calc_v5::band_calc_v5(const int_g np, const int_g bmin,
					   const int_g bmax,
					   const Density_Matrix &dm,
					   const NewK &nk)
  : property_data(dm, nk), band_data_v4(np, bmin, bmax, dm, nk)
{
}

inline CRYSTAL::band_calc_v6::band_calc_v6(const int_g np, const int_g bmin,
					   const int_g bmax,
					   const Density_Matrix &dm,
					   const NewK &nk)
  : property_data_v6(dm, nk), band_data_v6(np, bmin, bmax, dm, nk)
{
}

#endif // CRYSTAL_BAND_STRUCTURE
