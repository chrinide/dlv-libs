
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/constants.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_admin.hxx"
#include "../dlv/op_objs.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_plots.hxx"
#include "../dlv/job_setup.hxx"
#include "calcs.hxx"
#include "props.hxx"
#include "plots.hxx"
#include "band.hxx"

DLV::operation *CRYSTAL::band_calc::create(const int_g np, const int_g bmin,
					   const int_g bmax, const char path[],
					   const Density_Matrix &dm,
					   const NewK &nk,const int_g version,
					   const DLV::job_setup_data &job,
					   const bool extern_job,
					   const char extern_dir[],
					   char message[], const int_g mlen)
{
  band_calc *op = 0;
  message[0] = '\0';
  bool parallel = false;
  switch (version) {
  case CRYSTAL98:
    op = new band_calc_v4(np, bmin, bmax, dm, nk);
    break;
  case CRYSTAL03:
    op = new band_calc_v5(np, bmin, bmax, dm, nk);
    break;
  case CRYSTAL06:
  case CRYSTAL09:
  case CRYSTAL14:
  case CRYSTAL17:
  case CRYSTAL23:
    op = new band_calc_v6(np, bmin, bmax, dm, nk);
    break;
  case CRYSTAL_DEV:
    op = new band_calc_v6(np, bmin, bmax, dm, nk);
    parallel = job.is_parallel();
    break;
  default:
    strncpy(message, "BUG: unknown CRYSTAL version", mlen);
    break;
  }
  if (op == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Failed to allocate CRYSTAL::band_calc operation",
	      mlen);
  } else {
    message[0] = '\0';
    op->attach();
    if (op->create_files(parallel, job.is_local(), message, mlen))
      if (op->make_directory(message, mlen))
	op->write(op->get_infile_name(0), path, op, op->get_model(),
		  message, mlen);
    if (strlen(message) > 0) {
      // Don't delete once attached
      //delete op;
      //op = 0;
    } else {
      op->execute(op->get_path(), op->get_serial_executable(),
		  op->get_parallel_executable(), "", job, parallel,
		  extern_job, extern_dir, message, mlen);
    }
  }
  return op;
}

#ifdef ENABLE_DLV_GRAPHICS

DLV::operation *CRYSTAL::band_calc::show_path(const char kpath[],
					      char message[], const int_g mlen)
{
  // copy of set_kpath
  int_g i = 0, j, k, slash, space, eol, l, ll, n = 0, p, pt[3], d[3];
  DLV::string path = kpath, line, sub;
  char buff[128];

  l = (int)path.length();
  while ((i = (int)path.find('\n', i + 1)) < l) {
    if (i < 0)
      break;
    j = i;
    n++;
  }
  if (j < (l - 1))
    n++;
  if (n < 2) {
    strncpy(message, "Insufficient k points found", mlen);
    return 0;
  }
  if (n >= 20) {
    strncpy(message, "Too many k points", mlen);
    return 0;
  }
  real_g kpoints[20][3];
  DLV::string labels[20];
  j = 0;
  for (i = 0; i < n; i++) {
    eol = (int)path.find('\n', j);
    if (eol < 0 or eol > l)
      eol = l;
    line = path.substr(j, (eol - j));
    k = (int)line.find_first_not_of(' ', 0);
    p = 0;
    ll = (int)line.length();
    while (k < ll and p < 3) {
      space = (int)line.find(' ', k);
      slash = (int)line.find('/', k);
      sub = line.substr(k, (space - k));
      strcpy(buff, sub.c_str());
      // Todo - better error checking.
      if (slash < space and slash > 0)
	sscanf(buff, "%d/%d", &pt[p], &d[p]);
      else {
	sscanf(buff, "%d", &pt[p]);
	d[p] = 1;
      }
      k = space + 1;
      while (k < ll and line[k] == ' ')
	k++;
      p++;
    }
    if (p == 3) {
      kpoints[i][0] = real_g(pt[0]) /  real_g(d[0]);
      kpoints[i][1] = real_g(pt[1]) /  real_g(d[1]);
      kpoints[i][2] = real_g(pt[2]) /  real_g(d[2]);
      // Now get the label
      if (k < ll) {
	while (line[k] == ' ')
	  k++;
	labels[i] = line.substr(k, (ll - k));
      } else
	labels[i] = "";
    } else {
      strncpy(message, "Unable to interpret k point", mlen);
      return 0;
    }
    j = eol + 1;
  }
  DLV::op_line *op = new DLV::op_line("k-space path");
  op->create_path(kpoints, labels, n, true, message, mlen);
  return op;
}

#endif // ENABLE_DLV_GRAPHICS

DLV::string CRYSTAL::band_calc::get_name() const
{
  return ("CRYSTAL Band Structure calculation");
}

DLV::operation *CRYSTAL::load_band_data::create(const char filename[],
					       const int_g version,
					       char message[], const int_g mlen)
{
  switch (version) {
  case CRYSTAL98:
    return load_band_v4::create(filename, message, mlen);
  case CRYSTAL03:
  case CRYSTAL06:
  case CRYSTAL09:
  case CRYSTAL14:
  case CRYSTAL17:
  case CRYSTAL23:
  case CRYSTAL_DEV:
    return load_band_v5::create(filename, message, mlen);
  default:
    strncpy(message, "BUG: unknown CRYSTAL version", mlen);
  }
  return 0;
}

DLV::string CRYSTAL::load_band_data::get_name() const
{
  return ("Load CRYSTAL Band structure - " + get_filename());
}

DLV::operation *CRYSTAL::load_band_v4::create(const char filename[],
					      char message[],
					      const int_g mlen)
{
  load_band_v4 *op = new load_band_v4(filename);
  DLV::data_object *data = op->read_data(op, 0, filename, filename,
					 1, 0, 0, message, mlen);
  if (data == 0) {
    delete op;
    op = 0;
  } else {
    op->attach();
    op->attach_data(data);
  }
  return op;
}

DLV::operation *CRYSTAL::load_band_v5::create(const char filename[],
					      char message[],
					      const int_g mlen)
{
  load_band_v5 *op = new load_band_v5(filename);
  DLV::data_object *data = op->read_data(op, 0, filename, filename, 1,
					 false, 0.0, 0, 0, message, mlen);
  if (data == 0) {
    delete op;
    op = 0;
  } else {
    op->attach();
    op->attach_data(data);
  }
  return op;
}

void CRYSTAL::band_file_v4::get_data_info(std::ifstream &data_file,
					  int_g &nblocks, int_g &npts,
					  int_g &nbands, bool &spin)
{
  int_g i, n, nlines, a1, a2, a3, b1, b2, b3, c1, c2, c3, d1, d2, d3;
  char buff[128];

  nblocks = 0;
  npts = 0;
  nbands = 0;
  spin = false;
  // first line
  data_file >> buff;
  data_file >> nbands;
  data_file >> npts;
  data_file.getline(buff, 128);
  // Line 2, skip
  data_file.getline(buff, 128);
  // Line 3 - k points
  data_file >> a1;
  data_file >> a2;
  data_file >> a3;
  data_file >> b1;
  data_file >> b2;
  data_file >> b3;
  data_file.getline(buff, 128);
  n = npts;
  do {
    nblocks++;
    // Skip the lines of data.
    nlines = (nbands * n) / 6;
    if ((nbands * n) % 6 != 0)
      nlines++;
    for (i = 0; i < nlines; i++)
      data_file.getline(buff, 128);
    data_file >> buff;
    if (!data_file.eof()) {
      // Number of bands doesn't change.
      data_file >> i;
      data_file >> n;
      data_file.getline(buff, 128);
      // Line 2, skip
      data_file.getline(buff, 128);
      // Line 3 - k points
      data_file >> c1;
      data_file >> c2;
      data_file >> c3;
      data_file >> d1;
      data_file >> d2;
      data_file >> d3;
      data_file.getline(buff, 128);
      if (a1 == c1 && a2 == c2 && a3 == c3 &&
	  b1 == d1 && b2 == d2 && b3 == d3) {
	spin = true;
	nbands *= 2;
	n = 0;
      } else
	npts += n;
    } else
      n = 0;
  } while (n != 0);
}

DLV::panel_plot *CRYSTAL::band_file_v4::read_data(DLV::operation *op,
						  DLV::electron_bands *bands,
						  const char filename[],
						  const DLV::string id,
						  const int_g band_min,
						  const int_g nkp,
						  const DLV::string klabels[],
						  char message[],
						  const int_g mlen)
{
  DLV::electron_bands *data = 0;
  if (DLV::check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (DLV::open_file_read(input, filename, message, mlen)) {
      int_g nblocks;
      int_g npts;
      int_g nbands;
      bool spin;
      get_data_info(input, nblocks, npts, nbands, spin);
      input.clear();
      input.seekg(0);
      real_g **plots = new_local_array1(real_g *, nbands);
      for (int_g i = 0; i < nbands; i++)
	plots[i] = new real_g[npts];
      real_g *grid = new real_g[npts];
      real_g *xspecial = new real_g[nblocks + 1];
      xspecial[0] = 0.0;
      if (spin)
	nbands /= 2;
      grid[0] = 0.0;
      int_g n = 0;
      char line[128];
      for (int_g i = 0; i < nblocks; i++) {
	// first line
	input >> line;
	int_g j;
	input >> j;
	int_g size;
	input >> size;
	real_g temp;
	input >> temp;
	real_g step;
	input >> step;
	input.getline(line, 128);
	// Lines 2+3, skip
	input.getline(line, 128);
	input.getline(line, 128);
	for (j = 0; j < size; j++) {
	  for (int_g k = 0; k < nbands; k++) {
	    input >> plots[k][n + j];
	    plots[k][n + j] *= real_g(DLV::Hartree_to_eV);
	  }
	}
	if (n != 0)
	  grid[n] = grid[n - 1] + step;
	for (j = 1; j < size; j++)
	  grid[n + j] = grid[n] + ((real_g) j) * step;
	n += size;
	xspecial[i + 1] = grid[n - 1];
      }
      int_g nplots = nbands;
      if (spin)
	nplots += nbands;
      if (bands == 0)
	data = new DLV::electron_bands("CRYSTAL", id, op,
				       nplots, nblocks, spin);
      else
	data = bands;
      data->set_grid(grid, npts, false);
      data->set_xpoints(xspecial, nblocks + 1, false);
      if (nkp == (nblocks + 1)) {
	for (int_g i = 0; i < nkp; i++) {
	  // Special AVS Gamma character, lower case is \r2129 for gamma.
	  if (klabels[i] == "G") {
	    DLV::string gamma = "\r2029";
	    data->set_xlabel(gamma.c_str(), i);
	  } else
	    data->set_xlabel(klabels[i].c_str(), i); 
	}
      } // else we don't have labels.
      for (int_g i = 0; i < nbands; i++)
	data->add_plot(plots[i], i, false);
      if (spin) {
	n = 0;
	for (int_g i = 0; i < nblocks; i++) {
	  // first line
	  input >> line;
	  int_g j;
	  input >> j;
	  int_g size;
	  input >> size;
	  input.getline(line, 128);
	  // Lines 2+3, skip
	  input.getline(line, 128);
	  input.getline(line, 128);
	  for (j = 0; j < size; j++) {
	    for (int_g k = 0; k < nbands; k++) {
	      input >> plots[nbands + k][n + j];
	      plots[nbands + k][n + j] *= real_g(DLV::Hartree_to_eV);
	    }
	  }
	  n += size;
	}
	for (int_g i = 0; i < nbands; i++)
	  data->add_plot(plots[nbands + i], i + nbands, false);
      }
      delete_local_array(plots);
      // Now set up the headers.
      for (int_g i = 0; i < nbands; i++) {
	if (spin)
	  snprintf(line, 128, "Band (%1d) - Alpha", band_min + i);
	else
	  snprintf(line, 128, "Band (%1d)", band_min + i);
	data->set_plot_label(line, i);
      }
      if (spin) {
	for (int_g i = 0; i < nbands; i++) {
	  snprintf(line, 128, "Band (%1d) - Beta", band_min + i);
	  data->set_plot_label(line, nbands + i);
	}
      }
      data->set_yaxis_label("Energy (eV)");
      data->set_yunits("eV");
      input.close();
    }
  }
  return data;
}

DLV::panel_plot *CRYSTAL::band_file_v5::read_data(DLV::operation *op,
						  DLV::electron_bands *bands,
						  const char filename[],
						  const DLV::string id,
						  const int_g band_min,
						  const bool use_fermi,
						  const real_g ef,
						  const int_g nkp,
						  const DLV::string klabels[],
						  char message[],
						  const int_g mlen,
						  DLV::dos_plot *dos)
{
  DLV::electron_bands *data = 0;
  if (DLV::check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (DLV::open_file_read(input, filename, message, mlen)) {
      char line[128];
      input.getline(line, 128);
      if (line[0] != '#')
	strncpy(message, "Invalid header to band structure file", mlen);
      else {
	int_g nk;
	int_g nbands;
	int_g nspin;
	// # NKPT nk NBND nbands NSPIN nspin
	sscanf(line + 1, "%*s %d %*s %d %*s %d", &nk, &nbands, &nspin);
	real_g **plots = new_local_array1(real_g *, nbands);
	for (int_g i = 0; i < nbands; i++)
	  plots[i] = new real_g[nk];
	real_g *grid = new real_g[nk];
	input.getline(line, 128);
	// # NPANEL n
	int_g nblocks;
	sscanf(line + 1, "%*s %d", &nblocks);
	real_g *xspecial = new real_g[nblocks + 1];
	int_g positions[32];
	char labels[32][64];
	for (int_g i = 0; i <= nblocks; i++) {
	  // # index label
	  input.getline(line, 128);
	  sscanf(line + 1, "%d %s", &positions[i], labels[i]);
	}
	for (int_g l = 0; l < nspin; l++) {
	  // Skip xmgr commands
	  do {
	    input >> line;
	    if (line[0] == '@' or line[0] == '#')
	      input.getline(line, 128);
	    else
	      break;
	  } while (1);
	  // Need to process first line differently
	  grid[0] = (real_g)atof(line);
	  for (int_g k = 0; k < nbands; k++) {
	    input >> plots[k][0];
	    plots[k][0] *= real_g(DLV::Hartree_to_eV);
	  }
	  for (int_g j = 1; j < nk; j++) {
	    input >> grid[j];
	    for (int_g k = 0; k < nbands; k++) {
	      input >> plots[k][j];
	      plots[k][j] *= real_g(DLV::Hartree_to_eV);
	    }
	  }
	  // Tidy end of line
	  input.getline(line, 128);
	  // reset fermi energy
	  if (use_fermi) {
	    for (int_g i = 0; i < nbands; i++)
	      for (int_g j = 0; j < nk; j++)
		plots[i][j] += ef;
	  }
	  if (data == 0) {
	    int_g nplots = nbands;
	    if (nspin == 2)
	      nplots += nbands;
	    if (bands == 0) {
	      if (dos == 0)
		data = new DLV::electron_bands("CRYSTAL", id, op, nplots,
					       nblocks, (nspin == 2));
	      else
		data = new DLV::electron_bdos("CRYSTAL", id, op, nplots,
					      nblocks, (nspin == 2), dos);
	    } else
	      data = bands;
	    data->set_grid(grid, nk, false);
	  }
	  bool copy_data = (l == 0 and nspin == 2);
	  for (int_g i = 0; i < nbands; i++)
	    data->add_plot(plots[i], l * nbands + i, copy_data);
	  // Now set up the headers.
	  char text[128];
	  for (int_g j = 0; j < nbands; j++) {
	    if (nspin > 1) {
	      if (l == 0)
		snprintf(text, 128, "Band (%1d) - Alpha", band_min + j);
	      else
		snprintf(text, 128, "Band (%1d) - Beta", band_min + j);
	    } else
	      snprintf(text, 128, "Band (%1d)", band_min + j);
	    data->set_plot_label(text, l * nbands + j);
	  }
	}
	delete_local_array(plots);
	for (int_g i = 0; i <= nblocks; i++)
	  xspecial[i] = grid[positions[i] - 1];
	data->set_xpoints(xspecial, nblocks + 1, false);
	if (nkp == (nblocks + 1)) {
	  for (int_g i = 0; i < nkp; i++) {
	    // Special AVS Gamma character, lower case is \r2129 for gamma.
	    if (klabels[i] == "G") {
	      DLV::string gamma = "\r2029";
	      data->set_xlabel(gamma.c_str(), i);
	    } else
	      data->set_xlabel(klabels[i].c_str(), i); 
	  }
	} else {
	  // Use labels from file
	  for (int_g i = 0; i <= nblocks; i++)
	    data->set_xlabel(labels[i], i);
	}
	data->set_yaxis_label("Energy (eV)");
	data->set_yunits("eV");
      }
      input.close();
    }
  }
  return data;
}

bool CRYSTAL::band_data::set_kpath(const char kpath[],
				   char message[], const int_g mlen)
{
  int_g i = 0, j, k, slash, space, eol, l, ll, n = 0, p, pt[3], d[3];
  DLV::string path = kpath, line, sub;
  char buff[128];

  l = (int)path.length();
  while ((i = (int)path.find('\n', i + 1)) < l) {
    if (i < 0)
      break;
    j = i;
    n++;
  }
  if (j < (l - 1))
    n++;
  if (n < 2) {
    strncpy(message, "Insufficient k points found", mlen);
    return false;
  }
  nkpoints = n;
  kpoints = new kpoint_data[n];
  j = 0;
  for (i = 0; i < n; i++) {
    eol = (int)path.find('\n', j);
    if (eol < 0 or eol > l)
      eol = l;
    line = path.substr(j, (eol - j));
    k = (int)line.find_first_not_of(' ', 0);
    p = 0;
    ll = (int)line.length();
    while (k < ll and p < 3) {
      space = (int)line.find(' ', k);
      slash = (int)line.find('/', k);
      sub = line.substr(k, (space - k));
      strcpy(buff, sub.c_str());
      // Todo - better error checking.
      if (slash < space and slash > 0)
	sscanf(buff, "%d/%d", &pt[p], &d[p]);
      else {
	sscanf(buff, "%d", &pt[p]);
	d[p] = 1;
      }
      k = space + 1;
      while (k < ll and line[k] == ' ')
	k++;
      p++;
    }
    if (p == 3) {
      // Find the common denominator and set values.
      // Almost a copy of find_shrink.
      int_g fst = 0;
      int_g fac = 1;
      int_g a;
      for (a = 0; a < 3; a++) {
        fac *= d[a];
        if (fst < d[a])
          fst = d[a];
      }
      for (a = fst; a < fac; a++) {
	int_g b;
        for (b = 0; b < 3; b++) {
          if ((a % d[b]) != 0)
            break;
        }
        if (b == 3)
          break;
      }
      kpoints[i].denom = a;
      kpoints[i].point[0] = pt[0] * a / d[0];
      kpoints[i].point[1] = pt[1] * a / d[1];
      kpoints[i].point[2] = pt[2] * a / d[2];
      // Now get the label
      if (k < ll) {
	while (line[k] == ' ')
	  k++;
	kpoints[i].label = line.substr(k, (ll - k));
      } else
	kpoints[i].label = "";
    } else {
      strncpy(message, "Unable to interpret k point", mlen);
      return false;
    }
    j = eol + 1;
  }
  return true;
}

CRYSTAL::int_g CRYSTAL::band_data::find_shrink() const
{
  int_g i, j, fst = 0, fac = 1;

  for (i = 0; i < nkpoints; i++) {
    fac *= kpoints[i].denom;
    if (fst < kpoints[i].denom)
      fst = kpoints[i].denom;
  }
  for (i = fst; i < fac; i++) {
    for (j = 0; j < nkpoints; j++) {
      if ((i % kpoints[j].denom) != 0)
        break;
    }
    if (j == nkpoints)
      return i;
  }
  return fac;
}

void CRYSTAL::band_calc_v4::add_band_file(const char tag[],
					  const bool is_local)
{
  add_output_file(0, tag, "dat", "fort.25", is_local, true);
}

void CRYSTAL::band_calc_v5::add_band_file(const char tag[],
					  const bool is_local)
{
  add_output_file(0, tag, "dat", "BAND.DAT", is_local, true);
}

void CRYSTAL::band_calc_v6::add_band_file(const char tag[],
					  const bool is_local)
{
  add_output_file(0, tag, "dat", "BAND.DAT", is_local, true);
}

void CRYSTAL::band_calc_v6::add_calc_error_file(const DLV::string tag,
						const bool is_parallel,
						const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

bool CRYSTAL::band_calc::create_files(const bool is_parallel,
				      const bool is_local, char message[],
				      const int_g mlen)
{
  DLV::string filename;
  if (find_wavefunction(filename, binary_wvfn, message, mlen)) {
    static char tag[] = "band";
    static char input[] = "inp";
    add_calc_error_file(tag, is_parallel, is_local);
    if (is_parallel)
      add_input_file(0, tag, input, "INPUT", is_local, false);
    else
      add_command_file(0, tag, input, is_local, false);
    if (binary_wvfn)
      add_data_file(1, filename, "fort.9", is_local, true);
    else
      add_data_file(1, filename, "fort.98", is_local, true);
    add_log_file(tag, "out", is_local, true);
    add_sys_error_file(tag, "job", is_local, true);
    add_band_file(tag, is_local);
    return true;
  } else
    return false;
}

void CRYSTAL::band_data::write_band(std::ofstream &output,
				    const DLV::operation *op,
				    const DLV::model *const structure)
{
  output << "BAND\n";
  output << "DLV generated Band Structure calculation\n";
  int_g shrink = find_shrink();
  output << (nkpoints - 1);
  output << " " << shrink;
  output << " " << npoints;
  output << " " << band_min;
  output << " " << band_max;
  output << " 1 0\n";
  for (int_g i = 1; i < nkpoints; i++) {
    output << (kpoints[i - 1].point[0] * shrink / kpoints[i - 1].denom);
    output << " " << (kpoints[i - 1].point[1]
		      * shrink / kpoints[i - 1].denom);
    output << " " << (kpoints[i - 1].point[2]
		      * shrink / kpoints[i - 1].denom);
    output << " " << (kpoints[i].point[0] * shrink / kpoints[i].denom);
    output << " " << (kpoints[i].point[1] * shrink / kpoints[i].denom);
    output << " " << (kpoints[i].point[2] * shrink / kpoints[i].denom);
    output << '\n';
  }
}

void CRYSTAL::band_data_v4::write_input(const DLV::string filename,
					const char path[],
					const DLV::operation *op,
					const DLV::model *const structure,
					const bool get_fermi, const bool is_bin,
					char message[], const int_g mlen)
{
  if (set_kpath(path, message, mlen)) {
    std::ofstream output;
    if (DLV::open_file_write(output, filename.c_str(), message, mlen)) {
      write_header(output, op, structure, is_bin, false, get_fermi);
      write_band(output, op, structure);
      output << "END\n";
      output.close();
    }
  }
}

void CRYSTAL::band_data_v6::write_input(const DLV::string filename,
					const char path[],
					const DLV::operation *op,
					const DLV::model *const structure,
					const bool get_fermi, const bool is_bin,
					char message[], const int_g mlen)
{
  if (set_kpath(path, message, mlen)) {
    std::ofstream output;
    if (DLV::open_file_write(output, filename.c_str(), message, mlen)) {
      write_header(output, op, structure, is_bin, false, get_fermi);
      write_band(output, op, structure);
      output << "END\n";
      output.close();
    }
  }
}

void CRYSTAL::band_calc_v4::write(const DLV::string filename,
				  const char path[], const DLV::operation *op,
				  const DLV::model *const structure,
				  char message[], const int_g mlen)
{
  write_input(filename, path, op, structure, !has_fermi_energy(),
	      is_binary(), message, mlen);
}

void CRYSTAL::band_calc_v5::write(const DLV::string filename,
				  const char path[], const DLV::operation *op,
				  const DLV::model *const structure,
				  char message[], const int_g mlen)
{
  write_input(filename, path, op, structure, !has_fermi_energy(),
	      is_binary(), message, mlen);
}

void CRYSTAL::band_calc_v6::write(const DLV::string filename,
				  const char path[], const DLV::operation *op,
				  const DLV::model *const structure,
				  char message[], const int_g mlen)
{
  write_input(filename, path, op, structure, !has_fermi_energy(),
	      is_binary(), message, mlen);
}

bool CRYSTAL::band_calc_v4::recover(const bool no_err, const bool log_ok,
				    char message[], const int_g len)
{
  DLV::string id = get_job_id();
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi())
      read_logfile(name, id);
    DLV::data_object *file = 0;
    if (no_err)
      file = new DLV::text_file(name, program_name, id, "Band output file");
    else
      file = new DLV::text_file(name, program_name, id,
				"Band(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    int_g nk = get_nkpoints();
    DLV::string *labels = 0;
    if (nk > 0) {
      labels = new DLV::string[nk];
      for (int_g i = 0; i < nk; i++)
	labels[i] = get_klabel(i);
    }
    DLV::panel_plot *data = read_data(this, 0, get_outfile_name(0).c_str(), id,
				      get_min_band(), nk, labels,
				      message, len);
    if (data == 0)
      return false;
    else {
      real_l ef;
      if (find_fermi_energy(ef, message, len)) {
	real_g value = real_g(ef);
	data->set_ypoints(&value, 1);
	bool insulator = false;
	DLV::string fermi;
	if (find_state_type(insulator, message, len)) {
	  if (insulator)
	    fermi = "HOMO";
	  else
	    fermi = "Ef";
	}
	data->set_ylabel(fermi.c_str(), 0);
      }
      attach_data(data);
    }
  }
  return true;
}

bool CRYSTAL::band_calc_v5::recover(const bool no_err, const bool log_ok,
				    char message[], const int_g len)
{
  DLV::string id = get_job_id();
  bool no_fermi = true;
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi()) {
      read_logfile(name, id);
      no_fermi = false;
    }
    DLV::data_object *file = 0;
    if (no_err)
      file = new DLV::text_file(name, program_name, id, "Band output file");
    else
      file = new DLV::text_file(name, program_name, id,
				"Band(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    real_l ef;
    bool use_fermi;
    real_g value = 0.0;
    if ((use_fermi = find_fermi_energy(ef, message, len)))
      value = real_g(ef);
    real_l shift = 0.0;
    if (no_fermi)
      if (find_invalid_fermi(shift, message, len))
	value = real_g(shift);
    set_shift(value);
    int_g nk = get_nkpoints();
    DLV::string *labels = 0;
    if (nk > 0) {
      labels = new DLV::string[nk];
      for (int_g i = 0; i < nk; i++)
	labels[i] = get_klabel(i);
    }
    DLV::panel_plot *data = read_data(this, 0, get_outfile_name(0).c_str(), id,
				      get_min_band(), true, value, nk,
				      labels, message, len);
    if (nk > 0)
      delete [] labels;
    if (data == 0)
      return false;
    else {
      if (use_fermi) {
	value = real_g(ef);
	data->set_ypoints(&value, 1);
	bool insulator = false;
	DLV::string fermi;
	if (find_state_type(insulator, message, len)) {
	  if (insulator)
	    fermi = "HOMO";
	  else
	    fermi = "Ef";
	}
	data->set_ylabel(fermi.c_str(), 0);
      }
      attach_data(data);
    }
  }
  return true;
}

//Todo - this is (so far) just a copy of v5
bool CRYSTAL::band_calc_v6::recover(const bool no_err, const bool log_ok,
				    char message[], const int_g len)
{
  DLV::string id = get_job_id();
  bool no_fermi = true;
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi()) {
      read_logfile(name, id);
      no_fermi = false;
    }
    DLV::data_object *file = 0;
    if (no_err)
      file = new DLV::text_file(name, program_name, id, "Band output file");
    else
      file = new DLV::text_file(name, program_name, id,
				"Band(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    real_l ef;
    bool use_fermi;
    real_g value = 0.0;
    if ((use_fermi = find_fermi_energy(ef, message, len)))
      value = real_g(ef);
    real_l shift = 0.0;
    if (no_fermi)
      if (find_invalid_fermi(shift, message, len))
	value = real_g(shift);
    set_shift(value);
    int_g nk = get_nkpoints();
    DLV::string *labels = 0;
    if (nk > 0) {
      labels = new DLV::string[nk];
      for (int_g i = 0; i < nk; i++)
	labels[i] = get_klabel(i);
    }
    DLV::panel_plot *data = read_data(this, 0, get_outfile_name(0).c_str(), id,
				      get_min_band(), true, value,
				      nk, labels, message, len);
    if (nk > 0)
      delete [] labels;
    if (data == 0)
      return false;
    else {
      if (use_fermi) {
	value = real_g(ef);
	data->set_ypoints(&value, 1);
	bool insulator = false;
	DLV::string fermi;
	if (find_state_type(insulator, message, len)) {
	  if (insulator)
	    fermi = "HOMO";
	  else
	    fermi = "Ef";
	}
	data->set_ylabel(fermi.c_str(), 0);
      }
      attach_data(data);
    }
  }
  return true;
}

bool CRYSTAL::load_band_v4::reload_data(DLV::data_object *data,
					char message[], const int_g mlen)
{
  DLV::electron_bands *v = dynamic_cast<DLV::electron_bands *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload Bands", mlen);
    return false;
  } else
    return (read_data(this, v, get_filename().c_str(),
		      get_filename().c_str(), 1, 0, 0, message, mlen) != 0);
  return false;
}

bool CRYSTAL::load_band_v5::reload_data(DLV::data_object *data,
					char message[], const int_g mlen)
{
  DLV::electron_bands *v = dynamic_cast<DLV::electron_bands *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload Bands", mlen);
    return false;
  } else
    return (read_data(this, v, get_filename().c_str(), get_filename().c_str(),
		      1, false, 0.0, 0, 0, message, mlen) != 0);
  return false;
}

bool CRYSTAL::band_calc_v4::reload_data(DLV::data_object *data,
					char message[], const int_g mlen)
{
  DLV::electron_bands *v = dynamic_cast<DLV::electron_bands *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload Bands", mlen);
    return false;
  } else {
    int_g nk = get_nkpoints();
    DLV::string *labels = 0;
    if (nk > 0) {
      labels = new DLV::string[nk];
      for (int_g i = 0; i < nk; i++)
	labels[i] = get_klabel(i);
    }
    bool ok = (read_data(this, v, get_outfile_name(0).c_str(), "",
			 get_min_band(), nk, labels, message, mlen) != 0);
    if (nk > 0)
      delete [] labels;
    return ok;
  }
  return false;
}

bool CRYSTAL::band_calc_v5::reload_data(DLV::data_object *data,
					char message[], const int_g mlen)
{
  DLV::electron_bands *v = dynamic_cast<DLV::electron_bands *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload Bands", mlen);
    return false;
  } else {
    int_g nk = get_nkpoints();
    DLV::string *labels = 0;
    if (nk > 0) {
      labels = new DLV::string[nk];
      for (int_g i = 0; i < nk; i++)
	labels[i] = get_klabel(i);
    }
    bool ok = (read_data(this, v, get_outfile_name(0).c_str(), "",
			 get_min_band(), true, get_shift(), nk, labels,
			 message, mlen) != 0);
    if (nk > 0)
      delete [] labels;
    return ok;
  }
  return false;
}

bool CRYSTAL::band_calc_v6::reload_data(DLV::data_object *data,
					char message[], const int_g mlen)
{
  DLV::electron_bands *v = dynamic_cast<DLV::electron_bands *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload Bands", mlen);
    return false;
  } else {
    int_g nk = get_nkpoints();
    DLV::string *labels = 0;
    if (nk > 0) {
      labels = new DLV::string[nk];
      for (int_g i = 0; i < nk; i++)
	labels[i] = get_klabel(i);
    }
    bool ok = (read_data(this, v, get_outfile_name(0).c_str(), "",
			 get_min_band(), true, get_shift(), nk, labels,
			 message, mlen) != 0);
    if (nk > 0)
      delete [] labels;
    return ok;
  }
  return false;
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::band_data_v4 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::band_data_v4(0, 0, 0, d, n);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::band_data_v6 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::band_data_v6(0, 0, 0, d, n);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::load_band_v4 *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::load_band_v4("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::load_band_v5 *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::load_band_v5("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::band_calc_v4 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::band_calc_v4(0, 0, 0, d, n);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::band_calc_v5 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::band_calc_v5(0, 0, 0, d, n);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::band_calc_v6 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::band_calc_v6(0, 0, 0, d, n);
    }

  }
}

template <class Archive>
void CRYSTAL::kpoint_data::serialize(Archive &ar, const unsigned int version)
{
  ar & denom;
  ar & point;
  ar & label;
}

template <class Archive>
void CRYSTAL::band_data::save(Archive &ar, const unsigned int version) const
{
  ar & npoints;
  ar & band_min;
  ar & band_max;
  ar & nkpoints;
  for (int_g i = 0; i < nkpoints; i++)
    ar & kpoints[i];
  ar & shift;
}

template <class Archive>
void CRYSTAL::band_data::load(Archive &ar, const unsigned int version)
{
  ar & npoints;
  ar & band_min;
  ar & band_max;
  ar & nkpoints;
  kpoints = new kpoint_data[nkpoints];
  for (int_g i = 0; i < nkpoints; i++)
    ar & kpoints[i];
  ar & shift;
}

template <class Archive>
void CRYSTAL::band_data_v4::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::band_data>(*this);
  ar & boost::serialization::base_object<CRYSTAL::property_data>(*this);
}

template <class Archive>
void CRYSTAL::band_data_v6::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::band_data>(*this);
  ar & boost::serialization::base_object<CRYSTAL::property_data_v6>(*this);
}

template <class Archive>
void CRYSTAL::load_band_data::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::load_data_op>(*this);
}

template <class Archive>
void CRYSTAL::load_band_v4::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<load_band_data>(*this);
}

template <class Archive>
void CRYSTAL::load_band_v5::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<load_band_data>(*this);
}

template <class Archive>
void CRYSTAL::band_calc::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::property_calc>(*this);
}

template <class Archive>
void CRYSTAL::band_calc_v4::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::band_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::band_data_v4>(*this);
}

template <class Archive>
void CRYSTAL::band_calc_v5::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::band_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::band_data_v4>(*this);
}

template <class Archive>
void CRYSTAL::band_calc_v6::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::band_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::band_data_v6>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::band_data_v4)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::band_data_v6)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::load_band_v4)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::load_band_v5)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::band_calc_v4)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::band_calc_v5)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::band_calc_v6)

DLV_SUPPRESS_TEMPLATES(DLV::load_data_op)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::property_data)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::property_data_v6)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::property_calc)

DLV_NORMAL_EXPLICIT(CRYSTAL::band_data_v4)
DLV_NORMAL_EXPLICIT(CRYSTAL::band_data_v6)
DLV_NORMAL_EXPLICIT(CRYSTAL::load_band_v4)
DLV_NORMAL_EXPLICIT(CRYSTAL::load_band_v5)
DLV_NORMAL_EXPLICIT(CRYSTAL::band_calc_v4)
DLV_NORMAL_EXPLICIT(CRYSTAL::band_calc_v5)
DLV_NORMAL_EXPLICIT(CRYSTAL::band_calc_v6)

#endif // DLV_USES_SERIALIZE
