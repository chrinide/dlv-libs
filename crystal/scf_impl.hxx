
#ifndef CRYSTAL_SCF_PRIVATE_TYPES
#define CRYSTAL_SCF_PRIVATE_TYPES

namespace CRYSTAL {

  class cphf_data {
  protected:
    void set_params(const CPHF &cp);
    void write_cphf(std::ofstream &output) const;

  private:
    CPHF cphf;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class cphf_data_v7 : public scf_data_v7, public cphf_data {
  protected:
    void write_geom_commands(std::ofstream &output,
			     const DLV::model *const structure) const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class cphf_data_v8 : public scf_data_v8, public cphf_data {
  protected:
    void write_geom_commands(std::ofstream &output,
			     const DLV::model *const structure) const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class opt_data {
  protected:
    bool use_gradients() const;
    void set_params(const Optimise &opt);
    DLV::string optimiser_log() const;
    void write_optimise(std::ofstream &output) const;
    void write_v6_optimise(std::ofstream &output) const;
    void write_v7_optimise(std::ofstream &output) const;
    void write_v8_optimise(std::ofstream &output) const;

  private:
    Optimise optimise;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class opt_data_v5 : public scf_data_v5, public opt_data {
  protected:
    bool need_gradients() const;
    bool is_optimise() const;
    void write_geom_commands(std::ofstream &output,
			     const DLV::model *const structure) const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class opt_data_v6 : public scf_data_v6, public opt_data {
  protected:
    bool need_gradients() const;
    bool is_optimise() const;
    void write_geom_commands(std::ofstream &output,
			     const DLV::model *const structure) const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class opt_data_v7 : public scf_data_v7, public opt_data {
  protected:
    bool need_gradients() const;
    bool is_optimise() const;
    void write_geom_commands(std::ofstream &output,
			     const DLV::model *const structure) const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class opt_data_v8 : public scf_data_v8, public opt_data {
  protected:
    bool need_gradients() const;
    bool is_optimise() const;
    void write_geom_commands(std::ofstream &output,
			     const DLV::model *const structure) const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class scf_calc_v4 : public scf_calc, public scf_data_v4 {
  public:
    scf_calc_v4();

  protected:
    void set_params(const Hamiltonian &h, const Basis &b, const Kpoints &k,
		    const Tolerances &tol, const Convergence &c,
		    const Print &p, const Joboptions &j, const Optimise &opt,
		    const Phonon &ph, const Neb &n, const CPHF &cp,
		    const TDDFT &tddft, const bool a);
    void add_restart_file(const DLV::string tag, const bool is_local);
    void add_opt_restart_file(const DLV::string tag, const bool is_local);
    bool add_basis_set(const char filename[], const bool set_default,
		       int_g * &indices, int_g &value,
		       char message[], const int_g mlen);
    bool write(const DLV::string filename, const DLV::model *const structure,
	       char message[], const int_g mlen);
    void write_structure(const DLV::string filename,
			 const DLV::model *const structure,
			 char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class scf_calc_v5 : public scf_calc, public scf_data_v5 {
  public:
    scf_calc_v5();

  protected:
    void set_params(const Hamiltonian &h, const Basis &b, const Kpoints &k,
		    const Tolerances &tol, const Convergence &c,
		    const Print &p, const Joboptions &j, const Optimise &opt,
		    const Phonon &ph, const Neb &n, const CPHF &cp,
		    const TDDFT &tddft, const bool a);
    void add_restart_file(const DLV::string tag, const bool is_local);
    void add_opt_restart_file(const DLV::string tag, const bool is_local);
    bool add_basis_set(const char filename[], const bool set_default,
		       int_g * &indices, int_g &value,
		       char message[], const int_g mlen);
    bool write(const DLV::string filename, const DLV::model *const structure,
	       char message[], const int_g mlen);
    void write_structure(const DLV::string filename,
			 const DLV::model *const structure,
			 char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class scf_calc_v6 : public scf_calc, public scf_data_v6 {
  public:
    scf_calc_v6();

  protected:
    void set_params(const Hamiltonian &h, const Basis &b, const Kpoints &k,
		    const Tolerances &tol, const Convergence &c,
		    const Print &p, const Joboptions &j, const Optimise &opt,
		    const Phonon &ph, const Neb &n, const CPHF &cp,
		    const TDDFT &tddft, const bool a);
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void add_restart_file(const DLV::string tag, const bool is_local);
    void add_opt_restart_file(const DLV::string tag, const bool is_local);
    bool add_basis_set(const char filename[], const bool set_default,
		       int_g * &indices, int_g &value,
		       char message[], const int_g mlen);
    bool write(const DLV::string filename, const DLV::model *const structure,
	       char message[], const int_g mlen);
    void write_structure(const DLV::string filename,
			 const DLV::model *const structure,
			 char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class scf_calc_v7 : public scf_calc, public scf_data_v7 {
  public:
    scf_calc_v7();

  protected:
    void set_params(const Hamiltonian &h, const Basis &b, const Kpoints &k,
		    const Tolerances &tol, const Convergence &c,
		    const Print &p, const Joboptions &j, const Optimise &opt,
		    const Phonon &ph, const Neb &n, const CPHF &cp,
		    const TDDFT &tddft, const bool a);
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void add_restart_file(const DLV::string tag, const bool is_local);
    void add_opt_restart_file(const DLV::string tag, const bool is_local);
    bool add_basis_set(const char filename[], const bool set_default,
		       int_g * &indices, int_g &value,
		       char message[], const int_g mlen);
    bool write(const DLV::string filename, const DLV::model *const structure,
	       char message[], const int_g mlen);
    void write_structure(const DLV::string filename,
			 const DLV::model *const structure,
			 char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class scf_calc_v8 : public scf_calc, public scf_data_v8 {
  public:
    scf_calc_v8();

  protected:
    void set_params(const Hamiltonian &h, const Basis &b, const Kpoints &k,
		    const Tolerances &tol, const Convergence &c,
		    const Print &p, const Joboptions &j, const Optimise &opt,
		    const Phonon &ph, const Neb &n, const CPHF &cp,
		    const TDDFT &tddft, const bool a);
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void add_restart_file(const DLV::string tag, const bool is_local);
    void add_opt_restart_file(const DLV::string tag, const bool is_local);
    bool add_basis_set(const char filename[], const bool set_default,
		       int_g * &indices, int_g &value,
		       char message[], const int_g mlen);
    bool write(const DLV::string filename, const DLV::model *const structure,
	       char message[], const int_g mlen);
    void write_structure(const DLV::string filename,
			 const DLV::model *const structure,
			 char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class cphf_calc : public scf_calc {
  public:
    cphf_calc();

  protected:
    DLV::string get_name() const;
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void add_opt_restart_file(const DLV::string tag, const bool is_local);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class cphf_calc_v7 : public cphf_calc, public cphf_data_v7 {
  public:
    cphf_calc_v7();

  protected:
    void set_params(const Hamiltonian &h, const Basis &b, const Kpoints &k,
		    const Tolerances &tol, const Convergence &c,
		    const Print &p, const Joboptions &j, const Optimise &opt,
		    const Phonon &ph, const Neb &n, const CPHF &cp,
		    const TDDFT &tddft, const bool a);
    void add_restart_file(const DLV::string tag, const bool is_local);
    bool add_basis_set(const char filename[], const bool set_default,
		       int_g * &indices, int_g &value,
		       char message[], const int_g mlen);
    bool write(const DLV::string filename, const DLV::model *const structure,
	       char message[], const int_g mlen);
    void write_structure(const DLV::string filename,
			 const DLV::model *const structure,
			 char message[], const int_g mlen);

    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class cphf_calc_v8 : public cphf_calc, public cphf_data_v8 {
  public:
    cphf_calc_v8();

  protected:
    void set_params(const Hamiltonian &h, const Basis &b, const Kpoints &k,
		    const Tolerances &tol, const Convergence &c,
		    const Print &p, const Joboptions &j, const Optimise &opt,
		    const Phonon &ph, const Neb &n, const CPHF &cp,
		    const TDDFT &tddft, const bool a);
    void add_restart_file(const DLV::string tag, const bool is_local);
    bool add_basis_set(const char filename[], const bool set_default,
		       int_g * &indices, int_g &value,
		       char message[], const int_g mlen);
    bool write(const DLV::string filename, const DLV::model *const structure,
	       char message[], const int_g mlen);
    void write_structure(const DLV::string filename,
			 const DLV::model *const structure,
			 char message[], const int_g mlen);

    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class opt_calc : public scf_geom {
  public:
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    DLV::string get_name() const;
    bool create_files(const bool is_parallel, const bool is_local,
		      char message[], const int_g mlen);
    virtual void add_optimiser_log(const DLV::string tag,
				   const bool is_local) = 0;
    void add_standard_data_objects();

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class opt_calc_v5 : public opt_calc, public opt_data_v5 {
  public:
    opt_calc_v5();

  protected:
    void set_params(const Hamiltonian &h, const Basis &b, const Kpoints &k,
		    const Tolerances &tol, const Convergence &c,
		    const Print &p, const Joboptions &j, const Optimise &opt,
		    const Phonon &ph, const Neb &n, const CPHF &cp,
		    const TDDFT &tddft, const bool a);
    void add_restart_file(const DLV::string tag, const bool is_local);
    void add_opt_restart_file(const DLV::string tag, const bool is_local);
    void add_optimiser_log(const DLV::string tag, const bool is_local);
    bool add_basis_set(const char filename[], const bool set_default,
		       int_g * &indices, int_g &value,
		       char message[], const int_g mlen);
    bool write(const DLV::string filename, const DLV::model *const structure,
	       char message[], const int_g mlen);
    void write_structure(const DLV::string filename,
			 const DLV::model *const structure,
			 char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class opt_calc_v6 : public opt_calc, public opt_data_v6 {
  public:
    opt_calc_v6();

  protected:
    void set_params(const Hamiltonian &h, const Basis &b, const Kpoints &k,
		    const Tolerances &tol, const Convergence &c,
		    const Print &p, const Joboptions &j, const Optimise &opt,
		    const Phonon &ph, const Neb &n, const CPHF &cp,
		    const TDDFT &tddft, const bool a);
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void add_restart_file(const DLV::string tag, const bool is_local);
    void add_opt_restart_file(const DLV::string tag, const bool is_local);
    void add_optimiser_log(const DLV::string tag, const bool is_local);
    bool add_basis_set(const char filename[], const bool set_default,
		       int_g * &indices, int_g &value,
		       char message[], const int_g mlen);
    bool write(const DLV::string filename, const DLV::model *const structure,
	       char message[], const int_g mlen);
    void write_structure(const DLV::string filename,
			 const DLV::model *const structure,
			 char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class opt_calc_v7 : public opt_calc, public opt_data_v7 {
  public:
    opt_calc_v7();

  protected:
    void set_params(const Hamiltonian &h, const Basis &b, const Kpoints &k,
		    const Tolerances &tol, const Convergence &c,
		    const Print &p, const Joboptions &j, const Optimise &opt,
		    const Phonon &ph, const Neb &n, const CPHF &cp,
		    const TDDFT &tddft, const bool a);
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void add_restart_file(const DLV::string tag, const bool is_local);
    void add_opt_restart_file(const DLV::string tag, const bool is_local);
    void add_optimiser_log(const DLV::string tag, const bool is_local);
    bool add_basis_set(const char filename[], const bool set_default,
		       int_g * &indices, int_g &value,
		       char message[], const int_g mlen);
    bool write(const DLV::string filename, const DLV::model *const structure,
	       char message[], const int_g mlen);
    void write_structure(const DLV::string filename,
			 const DLV::model *const structure,
			 char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class opt_calc_v8 : public opt_calc, public opt_data_v8 {
  public:
    opt_calc_v8();

  protected:
    void set_params(const Hamiltonian &h, const Basis &b, const Kpoints &k,
		    const Tolerances &tol, const Convergence &c,
		    const Print &p, const Joboptions &j, const Optimise &opt,
		    const Phonon &ph, const Neb &n, const CPHF &cp,
		    const TDDFT &tddft, const bool a);
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void add_restart_file(const DLV::string tag, const bool is_local);
    void add_opt_restart_file(const DLV::string tag, const bool is_local);
    void add_optimiser_log(const DLV::string tag, const bool is_local);
    bool add_basis_set(const char filename[], const bool set_default,
		       int_g * &indices, int_g &value,
		       char message[], const int_g mlen);
    bool write(const DLV::string filename, const DLV::model *const structure,
	       char message[], const int_g mlen);
    void write_structure(const DLV::string filename,
			 const DLV::model *const structure,
			 char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(CRYSTAL::scf_calc_v4)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::scf_calc_v5)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::scf_calc_v6)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::scf_calc_v7)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::scf_calc_v8)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::cphf_calc_v7)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::cphf_calc_v8)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::opt_calc_v5)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::opt_calc_v6)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::opt_calc_v7)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::opt_calc_v8)
#endif // DLV_USES_SERIALIZE

inline CRYSTAL::scf_calc_v4::scf_calc_v4()
  : scf_data_v4()
{
}

inline CRYSTAL::scf_calc_v5::scf_calc_v5()
  : scf_data_v5()
{
}

inline CRYSTAL::scf_calc_v6::scf_calc_v6()
  : scf_data_v6()
{
}

inline CRYSTAL::scf_calc_v7::scf_calc_v7()
  : scf_data_v7()
{
}

inline CRYSTAL::scf_calc_v8::scf_calc_v8()
  : scf_data_v8()
{
}

inline CRYSTAL::cphf_calc::cphf_calc()
  : scf_calc()
{
}

inline CRYSTAL::cphf_calc_v7::cphf_calc_v7()
  : cphf_data_v7()
{
}

inline CRYSTAL::cphf_calc_v8::cphf_calc_v8()
  : cphf_data_v8()
{
}

inline CRYSTAL::opt_calc_v5::opt_calc_v5()
  : opt_data_v5()
{
}

inline CRYSTAL::opt_calc_v6::opt_calc_v6()
  : opt_data_v6()
{
}

inline CRYSTAL::opt_calc_v7::opt_calc_v7()
  : opt_data_v7()
{
}

inline CRYSTAL::opt_calc_v8::opt_calc_v8()
  : opt_data_v8()
{
}

inline bool CRYSTAL::opt_data::use_gradients() const
{
  return (optimise.optimiser == Optimise::domin and
	  optimise.opt_type == Optimise::atom_opt);
}

#endif // CRYSTAL_SCF_PRIVATE_TYPES
