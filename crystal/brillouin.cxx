
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/constants.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_admin.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_vol.hxx"
#include "../dlv/job_setup.hxx"
#include "calcs.hxx"
#include "props.hxx"
#include "volumes.hxx"
#include "brillouin.hxx"

DLV::operation *CRYSTAL::ek_3d_calc::create(const int_g minb, const int_g maxb,
					    const int_g gilat,
					    const Density_Matrix &dm,
					    const NewK &nk,
					    const int_g version,
					    const DLV::job_setup_data &job,
					    const bool extern_job,
					    const char extern_dir[],
					    char message[], const int_g mlen)
{
  ek_3d_calc *op = 0;
  message[0] = '\0';
  bool parallel = false;
  switch (version) {
  case CRYSTAL06:
  case CRYSTAL09:
  case CRYSTAL14:
  case CRYSTAL17:
  case CRYSTAL23:
    op = new ek_3d_calc_v6(minb, maxb, gilat, dm, nk);
    break;
  case CRYSTAL_DEV:
    op = new ek_3d_calc_v6(minb, maxb, gilat, dm, nk);
    parallel = job.is_parallel();
    break;
  default:
    strncpy(message, "BUG: unsupported CRYSTAL version", mlen);
    break;
  }
  if (op == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Failed to allocate CRYSTAL::ek_3d_calc operation",
	      mlen);
  } else {
    op->attach(); // attach to find wavefunction.
    if (op->create_files(parallel, job.is_local(), message, mlen))
      if (op->make_directory(message, mlen))
	op->write(op->get_infile_name(0), op, op->get_model(),
		  message, mlen);
    if (strlen(message) > 0) {
      // Don't delete once attached
      //delete op;
      //op = 0;
    } else {
      op->execute(op->get_path(), op->get_serial_executable(),
		  op->get_parallel_executable(), "", job, parallel,
		  extern_job, extern_dir, message, mlen);
    }
  }
  return op;
}

DLV::string CRYSTAL::ek_3d_calc::get_name() const
{
  return ("CRYSTAL E(k) volume calculation");
}

DLV::operation *CRYSTAL::load_ek_3d_data::create(const char filename[],
						 const int_g version,
						 char message[],
						 const int_g mlen)
{
  switch (version) {
  case CRYSTAL06:
  case CRYSTAL09:
  case CRYSTAL14:
  case CRYSTAL17:
  case CRYSTAL23:
  case CRYSTAL_DEV:
    return load_ek_3d_data_v6::create(filename, message, mlen);
  default:
    strncpy(message, "BUG: unsupported CRYSTAL version", mlen);
  }
  return 0;
}

DLV::string CRYSTAL::load_ek_3d_data::get_name() const
{
  return ("Load CRYSTAL E(k) volume - " + get_filename());
}

DLV::operation *CRYSTAL::load_ek_3d_data_v6::create(const char filename[],
						    char message[],
						    const int_g mlen)
{
  load_ek_3d_data_v6 *op = new load_ek_3d_data_v6(filename);
  DLV::data_object *data = op->read_data(op, filename, filename,message, mlen);
  if (data == 0) {
    delete op;
    op = 0;
  } else {
    op->attach();
    op->attach_data(data);
  }
  return op;
}

void CRYSTAL::ek_3d_calc_v6::add_calc_error_file(const DLV::string tag,
						 const bool is_parallel,
						 const bool is_local)
{
  add_job_error_file(tag, "err", "ERROR", is_local, true);
}

bool CRYSTAL::ek_3d_calc::create_files(const bool is_parallel,
				       const bool is_local, char message[],
				       const int_g mlen)
{
  DLV::string filename;
  if (find_wavefunction(filename, binary_wvfn, message, mlen)) {
    static char tag[] = "ek";
    static char input[] = "inp";
    add_calc_error_file(tag, is_parallel, is_local);
    if (is_parallel)
      add_input_file(0, tag, input, "INPUT", is_local, false);
    else
      add_command_file(0, tag, input, is_local, false);
    if (binary_wvfn)
      add_data_file(1, filename, "fort.9", is_local, true);
    else
      add_data_file(1, filename, "fort.98", is_local, true);
    add_log_file(tag, "out", is_local, true);
    add_sys_error_file(tag, "job", is_local, true);
    add_output_file(0, tag, "dat", "fort.35", is_local, true);
    return true;
  } else
    return false;
}

void CRYSTAL::ek_3d_data::write_input(const DLV::string filename,
				      const DLV::operation *op,
				      const DLV::model *const structure,
				      const bool is_bin, char message[],
				      const int_g mlen)
{
  std::ofstream output;
  if (DLV::open_file_write(output, filename.c_str(), message, mlen)) {
    write_header(output, op, structure, is_bin, true, false, false);
    write_command(output);
    write_data_sets(output);
    output << "END\n";
    output.close();
  }
}

void CRYSTAL::ek_3d_data::write_command(std::ofstream &output) const
{
  output << "DLV_BAND\n";
}

void CRYSTAL::ek_3d_data::write_data_sets(std::ofstream &output) const
{
  output.precision(5);
  output << min_band << ' ';
  output << max_band << ' ';
  output << shrink << '\n';
}

void CRYSTAL::ek_3d_calc_v6::write(const DLV::string filename,
				    const DLV::operation *op,
				    const DLV::model *const structure,
				    char message[], const int_g mlen)
{
  write_input(filename, op, structure, is_binary(), message, mlen);
}

DLV::volume_data *CRYSTAL::brillouin_file::create_volume(const char code[],
							 const DLV::string id,
							 DLV::operation *op,
							 const int_g nx,
							 const int_g ny,
							 const int_g nz,
							 const real_g origin[3],
							 const real_g astep[3],
							 const real_g bstep[3],
						   const real_g cstep[3]) const
{
  return new DLV::k_space_volume("CRYSTAL", id, op, nx, ny, nz,
				 origin, astep, bstep, cstep);
}

CRYSTAL::real_g CRYSTAL::brillouin_file::get_grid_scale() const
{
  return real_g(1.0 / C09_bohr_to_angstrom);
}

bool CRYSTAL::brillouin_file::get_data_scale(real_g &scale) const
{
  scale = real_g(DLV::Hartree_to_eV);
  return true;
}

bool CRYSTAL::ek_3d_calc_v6::recover(const bool no_err, const bool log_ok,
				     char message[], const int_g len)
{
  DLV::string id = get_job_id();
  if (log_ok) {
    DLV::string name = get_log_filename();
    if (recover_fermi())
      read_logfile(name, id);
    DLV::data_object *file = 0;
    if (no_err)
      file = new DLV::text_file(name, program_name, id, "E(k) output file");
    else
      file = new DLV::text_file(name, program_name, id,
				"E(k)(failed) output file");
    attach_data(file);
  }
  if (no_err) {
    DLV::data_object *data = read_data(this, get_outfile_name(0).c_str(),
				       id, message, len);
    if (data == 0)
      return false;
    else
      attach_data(data);
  }
  return true;
}

bool CRYSTAL::load_ek_3d_data_v6::reload_data(DLV::data_object *data,
					      char message[], const int_g mlen)
{
  DLV::volume_data *v = dynamic_cast<DLV::volume_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload E(k) grid", mlen);
    return false;
  } else
    return read_data(this, v, get_filename().c_str(), message, mlen);
}

bool CRYSTAL::ek_3d_calc_v6::reload_data(DLV::data_object *data,
					 char message[], const int_g mlen)
{
  DLV::volume_data *v = dynamic_cast<DLV::volume_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload E(k) grid", mlen);
    return false;
  } else
    return read_data(this, v, get_outfile_name(0).c_str(), message, mlen);
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    CRYSTAL::load_ek_3d_data_v6 *t,
				    const unsigned int file_version)
    {
      ::new(t)CRYSTAL::load_ek_3d_data_v6("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, CRYSTAL::ek_3d_calc_v6 *t,
				    const unsigned int file_version)
    {
      CRYSTAL::Density_Matrix d(0, 0.0, 0.0, 0, 0);
      CRYSTAL::NewK n(0, 0, 0, 0, false, false, false, false);
      ::new(t)CRYSTAL::ek_3d_calc_v6(0, 0, 0, d, n);
    }

  }
}

template <class Archive>
void CRYSTAL::ek_3d_data::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<CRYSTAL::property_data_v6>(*this);
  ar & min_band;
  ar & max_band;
  ar & shrink;
}

template <class Archive>
void CRYSTAL::ek_3d_data::load(Archive &ar, const unsigned int version)
{
  if (version == 0)
    ar & boost::serialization::base_object<CRYSTAL::property_data>(*this);
  else
    ar & boost::serialization::base_object<CRYSTAL::property_data_v6>(*this);
  ar & min_band;
  ar & max_band;
  ar & shrink;
}

template <class Archive>
void CRYSTAL::ek_3d_data_v6::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::ek_3d_data>(*this);
}

template <class Archive>
void CRYSTAL::load_ek_3d_data::serialize(Archive &ar,
					 const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::load_data_op>(*this);
}

template <class Archive>
void CRYSTAL::load_ek_3d_data_v6::serialize(Archive &ar,
					    const unsigned int version)
{
  ar & boost::serialization::base_object<load_ek_3d_data>(*this);
}

template <class Archive>
void CRYSTAL::ek_3d_calc::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::property_calc>(*this);
}

template <class Archive>
void CRYSTAL::ek_3d_calc_v6::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<CRYSTAL::ek_3d_calc>(*this);
  ar & boost::serialization::base_object<CRYSTAL::ek_3d_data_v6>(*this);
}

BOOST_CLASS_VERSION(CRYSTAL::ek_3d_data, 1)

BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::load_ek_3d_data_v6)
BOOST_CLASS_EXPORT_IMPLEMENT(CRYSTAL::ek_3d_calc_v6)

DLV_SUPPRESS_TEMPLATES(DLV::load_data_op)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::property_data_v6)
DLV_SUPPRESS_TEMPLATES(CRYSTAL::property_calc)

DLV_NORMAL_EXPLICIT(CRYSTAL::load_ek_3d_data_v6)
DLV_NORMAL_EXPLICIT(CRYSTAL::ek_3d_calc_v6)

#endif // DLV_USES_SERIALIZE
