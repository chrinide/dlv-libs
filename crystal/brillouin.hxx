
#ifndef CRYSTAL_FERMI_SURFACE
#define CRYSTAL_FERMI_SURFACE

namespace CRYSTAL {

  // v6? only
  class brillouin_file : public grid3D_v6 {
  protected:
    DLV::volume_data *create_volume(const char code[], const DLV::string id,
				    DLV::operation *op, const int_g nx,
				    const int_g ny, const int_g nz,
				    const real_g origin[3],
				    const real_g astep[3],
				    const real_g bstep[3],
				    const real_g cstep[3]) const;
    real_g get_grid_scale() const;
    bool get_data_scale(real_g &scale) const;
  };

  class ek_3d_data : public property_data_v6 {
  protected:
    ek_3d_data(const int_g minb, const int_g maxb, const int_g gilat,
	       const Density_Matrix &dm, const NewK &nk);

  protected:
    void write_input(const DLV::string filename, const DLV::operation *op,
		     const DLV::model *const structure,
		     const bool is_bin, char message[], const int_g mlen);
    void write_command(std::ofstream &output) const;
    void write_data_sets(std::ofstream &output) const;

  private:
    int_g min_band;
    int_g max_band;
    int_g shrink;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class ek_3d_data_v6 : public ek_3d_data, public brillouin_file {
  protected:
    ek_3d_data_v6(const int_g minb, const int_g maxb, const int_g gilat,
		  const Density_Matrix &dm, const NewK &nk);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_ek_3d_data : public DLV::load_data_op {
  public:
    static operation *create(const char filename[], const int_g version,
			     char message[], const int_g mlen);

  protected:
    load_ek_3d_data(const char file[]);

    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_ek_3d_data_v6 : public load_ek_3d_data, public brillouin_file {
  public:
    static operation *create(const char filename[],
                             char message[], const int_g mlen);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

    // public for serialization
    load_ek_3d_data_v6(const char file[]);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class ek_3d_calc : public property_calc {
  public:
    static operation *create(const int_g minb, const int_g maxb,
			     const int_g gilat, const Density_Matrix &dm,
			     const NewK &nk, const int_g version,
			     const DLV::job_setup_data &job,
			     const bool extern_job, const char extern_dir[],
			     char message[], const int_g mlen);

  protected:
    DLV::string get_name() const;

    bool create_files(const bool is_parallel, const bool is_local,
		      char message[], const int_g mlen);
    virtual void write(const DLV::string filename, const DLV::operation *op,
		       const DLV::model *const structure,
		       char message[], const int_g mlen) = 0;
    bool is_binary() const;

  private:
    bool binary_wvfn;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class ek_3d_calc_v6 : public ek_3d_calc, public ek_3d_data_v6 {
  public:
    ek_3d_calc_v6(const int_g minb, const int_g maxb, const int_g gilat,
		  const Density_Matrix &dm, const NewK &nk);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    void add_calc_error_file(const DLV::string tag, const bool is_parallel,
			     const bool is_local);
    void write(const DLV::string filename, const DLV::operation *op,
	       const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(CRYSTAL::load_ek_3d_data_v6)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::ek_3d_calc_v6)
#endif // DLV_USES_SERIALIZE

inline CRYSTAL::ek_3d_data::ek_3d_data(const int_g minb, const int_g maxb,
				       const int_g gilat,
				       const Density_Matrix &dm,
				       const NewK &nk)
  : property_data_v6(dm, nk), min_band(minb), max_band(maxb), shrink(gilat)
{
}

inline CRYSTAL::ek_3d_data_v6::ek_3d_data_v6(const int_g minb, const int_g maxb,
					     const int_g gilat,
					     const Density_Matrix &dm,
					     const NewK &nk)
  : ek_3d_data(minb, maxb, gilat, dm, nk)
{
}

inline CRYSTAL::load_ek_3d_data::load_ek_3d_data(const char file[])
  : load_data_op(file)
{
}

inline CRYSTAL::load_ek_3d_data_v6::load_ek_3d_data_v6(const char file[])
  : load_ek_3d_data(file)
{
}

inline bool CRYSTAL::ek_3d_calc::is_binary() const
{
  return binary_wvfn;
}

inline CRYSTAL::ek_3d_calc_v6::ek_3d_calc_v6(const int_g minb, const int_g maxb,
					     const int_g gilat,
					     const Density_Matrix &dm,
					     const NewK &nk)
  : ek_3d_data_v6(minb, maxb, gilat, dm, nk)
{
}

#endif // CRYSTAL_FERMI_SURFACE
