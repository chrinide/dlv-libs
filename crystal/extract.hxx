
#ifndef CRYSTAL_EXTRACT_STRUCTURE
#define CRYSTAL_EXTRACT_STRUCTURE

namespace CRYSTAL {

  class extract_calc : public calc_geom, public structure_file {
  public:
    static operation *create(const char file[], const char str[],
			     const bool use_str, const bool new_view,
			     const bool set_bonds,
			     const DLV::job_setup_data &job,
			     char message[], const int_g mlen);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);
    bool open_new_view() const;

    //public for serialization
    extract_calc(const char file[], const bool new_view, const bool sb);

  protected:
    DLV::string get_name() const;
    bool create_files(const bool is_parallel, const bool is_local,
		      char message[], const int_g mlen);
    void add_standard_data_objects();
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);

    virtual bool has_structure() const;
    virtual void add_structure_file(const bool is_local);
    void write(const DLV::string outfile, char message[], const int_g mlen);

  private:
    DLV::string filename;
    bool use_new_view;
    bool set_bonds;

    bool read_data(const DLV::string filename, const DLV::string id,
		   char message[], const int_g mlen);

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class extract_str : public extract_calc {
  public:
    extract_str(const char file[], const char str[], const bool new_view,
		const bool sb);

  protected:
    bool has_structure() const;
    void add_structure_file(const bool is_local);

  private:
    DLV::string structure_file;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(CRYSTAL::extract_calc)
BOOST_CLASS_EXPORT_KEY(CRYSTAL::extract_str)
#endif // DLV_USES_SERIALIZE

inline CRYSTAL::extract_calc::extract_calc(const char file[],
					   const bool new_view, const bool sb)
  : filename(file), use_new_view(new_view), set_bonds(sb)
{
}

inline CRYSTAL::extract_str::extract_str(const char file[], const char str[],
					 const bool new_view, const bool sb)
  : extract_calc(file, new_view, sb), structure_file(str)
{
}

#endif // CRYSTAL_EXTRACT_STRUCTURE
