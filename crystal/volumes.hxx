
#ifndef CRYSTAL_VOLUME_DATA
#define CRYSTAL_VOLUME_DATA

namespace CRYSTAL {

  // interface classes for 3D grids
  class grid3D {
  protected:
    virtual ~grid3D();
    DLV::volume_data *read_data(DLV::operation *op, const char filename[],
				const DLV::string id, char message[],
				const int_g mlen);
    bool read_data(DLV::operation *op, DLV::volume_data *data,
		   const char filename[], char message[], const int_g mlen);
    virtual bool get_grid(std::ifstream &input, int_g &nx, int_g &ny, int_g &nz,
			  real_g origin[3], real_g astep[3], real_g bstep[3],
			  real_g cstep[3], char label[],
			  char message[], const int_g mlen);
    virtual DLV::volume_data *create_volume(const char code[],
					    const DLV::string id,
					    DLV::operation *op, const int_g nx,
					    const int_g ny, const int_g nz,
					    const real_g origin[3],
					    const real_g astep[3],
					    const real_g bstep[3],
					    const real_g cstep[3]) const;
    virtual bool get_data(std::ifstream &input, DLV::volume_data *data,
			  DLV::operation *op, const int_g nx, const int_g ny,
			  const int_g nz, const char label[], char message[],
			  const int_g mlen) = 0;
    bool read_grid(std::ifstream &input, real_g origin[3], real_g astep[3],
		   real_g bstep[3], real_g cstep[3], char message[],
		   const int_g mlen);
    bool read_data(std::ifstream &input, DLV::volume_data *data,
		   const int_g nx, const int_g ny, const int_g nz,
		   const int_g vec, const char label[],
		   char message[], const int_g mlen);
    virtual real_g get_grid_scale() const;
    virtual bool get_data_scale(real_g &scale) const;
  };

  class grid3D_v4 : public grid3D {
  protected:
    bool get_grid(std::ifstream &input, int_g &nx, int_g &ny, int_g &nz,
		  real_g origin[3], real_g astep[3], real_g bstep[3],
		  real_g cstep[3], char label[],
		  char message[], const int_g mlen);
    bool get_data(std::ifstream &input, DLV::volume_data *data,
		  DLV::operation *op, const int_g nx, const int_g ny,
		  const int_g nz, const char label[], char message[],
		  const int_g mlen);
  };

  class grid3D_v5 : public grid3D {
  protected:
    bool get_data(std::ifstream &input, DLV::volume_data *data,
		  DLV::operation *op, const int_g nx, const int_g ny,
		  const int_g nz, const char label[], char message[],
		  const int_g mlen);
  };

  class grid3D_v6 : public grid3D {
  protected:
    bool get_data(std::ifstream &input, DLV::volume_data *data,
		  DLV::operation *op, const int_g nx, const int_g ny,
		  const int_g nz, const char label[], char message[],
		  const int_g mlen);
    bool read_wvfn(std::ifstream &input, DLV::volume_data *data,
		   const int_g nx, const int_g ny, const int_g nz,
		   const char label[], char message[], const int_g mlen);
    bool read_wvfn(std::ifstream &input, DLV::volume_data *data,
		   const int_g nx, const int_g ny, const int_g nz,
		   const char label[], const int_g k1, const int_g k2,
		   const int_g k3, const int_g s1, const int_g s2,
		   const int_g s3, char message[], const int_g mlen);
    void process_label(const char line[], char buff[], int_g &v);
  };

  class grid3D_v8 : public grid3D {
  protected:
    bool get_data(std::ifstream &input, DLV::volume_data *data,
		  DLV::operation *op, const int_g nx, const int_g ny,
		  const int_g nz, const char label[], char message[],
		  const int_g mlen);
    bool get_topo_grid(std::ifstream &input, int_g &nx, int_g &ny,
		       int_g &nz, real_g origin[3], real_g astep[3],
		       real_g bstep[3], real_g cstep[3],
		       const DLV::coord_type a[3], const DLV::coord_type b[3],
		       const DLV::coord_type c[3], const int_g dims,
		       char message[], const int_g mlen);
    bool read_topo_grid(std::ifstream &input, real_g origin[3], real_g astep[3],
			real_g bstep[3], real_g cstep[3],
			const DLV::coord_type a[3], const DLV::coord_type b[3],
			const DLV::coord_type c[3], const int_g nx,
			const int_g ny, const int_g nz, const int_g dims,
			char message[], const int_g mlen);
    DLV::volume_data *read_topo_data(DLV::operation *op, const char filename[],
				     const DLV::string id,
				     const DLV::coord_type a[3],
				     const DLV::coord_type b[3],
				     const DLV::coord_type c[3],
				     const int_g dims, DLV::volume_data *data,
				     const char label[],
				     char message[], const int_g mlen);
  };

}

#endif // CRYSTAL_VOLUME_DATA
