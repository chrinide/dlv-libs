
#ifndef GULP_EXECUTE_PROG
#define GULP_EXECUTE_PROG

namespace GULP {

  // Common data for calculations.
  class control_data {
  public:
    control_data(const bool prop, const bool fe, const int_g phonons,
		 const int_g sh1, const int_g sh2, const int_g sh3,
		 const bool usep, real_g p, const bool uset, real_g t,
		 const bool eig, const int_g n, const char *path);

    bool property_calc;
    bool free_energy_calc;
    enum phonon_type { no_phonons, phonon_DOS, phonon_disp } phonon_calc;
    int_g kshrinka, kshrinkb, kshrinkc;
    real_g pressure;
    real_g temperature;
    bool set_temperature, set_pressure;
    bool phonon_eigenvectors;
    int_g band_npoints;
    DLV::string band_path;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class optimize_data {
  public:
    optimize_data(const int_g c);

    enum constraint_type {
      no_constraint, constant_pressure, constant_volume
    } calc_constraint;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class moldyn_data {
  public:
    moldyn_data(const int_g en, const real_g step, const int_g eq,
		const int_g pd, const int_g sample, const real_g therm,
		const real_g baro, const int_g ts);

    enum ensemble_type { ensemble_nve, ensemble_nvt, ensemble_npt } ensemble;
    real_g timestep;
    int_g equilibrate_steps;
    int_g production_steps;
    int_g sample_steps;
    int_g trajectory_steps;
    real_g thermostat;
    real_g barostat;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class potential_lib {
  public:
    potential_lib(const bool s, DLV::string l);

    bool output_shells;
    DLV::string library_name;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  struct kpoint_data {
    real_g point[3];
    DLV::string label;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  // Todo - lets see how much of this we actually use
  class calculation_data : public structure_file {
  public:
    // Single point data, common to all - should it be?
    calculation_data(const control_data &c, const potential_lib &p);
    virtual ~calculation_data();

  protected:
    virtual bool use_structure() const;
    virtual bool use_trajectory() const;
    bool calc_phonon_DOS() const;
    bool calc_phonon_dispersion() const;
    bool calc_phonon_vectors() const;

    bool set_kpath(const char kpath[], char message[], const int_g mlen);

    void write_input(DLV::string filename, const DLV::string tag,
		     const DLV::model *const structure,
		     char message[], const int_g mlen);
    virtual void write_controls(std::ofstream &output) const;
    void write_final_controls(std::ofstream &output,
			      char message[], const int_g mlen);
    void write_potentials(std::ofstream &output) const;
    void write_file_data(std::ofstream &output, const DLV::string name) const;

    int_g get_num_kpoints() const;
    DLV::string get_kpoint_label(const int_g n) const;

    DLV::string get_potential_file() const;

  private:
    control_data controls;
    potential_lib potentials;
    kpoint_data *kpoints;
    int_g nkpoints;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class optimization_data : public virtual calculation_data {
  public:
    optimization_data(const control_data &c, const optimize_data &o,
		      const potential_lib &p);

  protected:
    bool use_structure() const;

    void write_controls(std::ofstream &output) const;

  private:
    optimize_data opt_data;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class molecular_dynamics_data : public virtual calculation_data {
  public:
    molecular_dynamics_data(const control_data &c, const moldyn_data &m,
			    const potential_lib &p);

  protected:
    bool use_structure() const;
    bool use_trajectory() const;

    void write_controls(std::ofstream &output) const;

  private:
    moldyn_data md_data;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  // Todo - this structure seems unnecessarily complicated - improve it?
  class calculate : public DLV::batch_calc, public property_files,
		    virtual public calculation_data {
  public:
    static operation *create(const int_g calc_type, const control_data &c,
			     const optimize_data &o, const moldyn_data &m,
			     const potential_lib &p,
			     const DLV::job_setup_data &job,
			     const bool extern_job, const char extern_dir[],
			     char message[], const int_g mlen);

    calculate(const control_data &c, const potential_lib &p);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    DLV::string get_name() const;
    bool create_files(const bool is_parallel, const bool is_local,
		      char message[], const int_g mlen);
    bool setup_files(const char tag[], const bool is_parallel,
		     const bool is_local, char message[], const int_g mlen);
    virtual void write(const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);
    bool no_errors(char message[], const int_g len);

    DLV::string get_serial_executable() const;
    DLV::string get_parallel_executable() const;
    DLV::string get_path() const;

  private:
    static DLV::string serial_binary;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class calc_geom : public calculate  {
  public:
    bool is_geometry() const;

    calc_geom(const control_data &c, const potential_lib &p);

  protected:
    void inherit_model();
    void inherit_data();
    void add_standard_data_objects();

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class optimization_calc : public calc_geom, public optimization_data {
  public:
    optimization_calc(const control_data &c, const optimize_data &o,
		      const potential_lib &p);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    DLV::string get_name() const;
    bool create_files(const bool is_parallel, const bool is_local,
		      char message[], const int_g mlen);
    void write(const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);
    void fixup_job();

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class molecular_dynamics_calc : public calc_geom,
				  public molecular_dynamics_data {
  public:
    molecular_dynamics_calc(const control_data &c, const moldyn_data &m,
			    const potential_lib &p);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    DLV::string get_name() const;
    bool create_files(const bool is_parallel, const bool is_local,
		      char message[], const int_g mlen);
    void write(const DLV::model *const structure,
	       char message[], const int_g mlen);
    bool recover(const bool no_err, const bool log_ok,
		 char message[], const int_g len);
    void fixup_job();
    void set_data(DLV::time_plot *d1, DLV::time_plot *d2,
		  DLV::time_plot *d3, DLV::md_trajectory *d4);

  private:
    DLV::time_plot *t1;
    DLV::time_plot *t2;
    DLV::time_plot *t3;
    DLV::md_trajectory *md;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(GULP::optimization_calc)
BOOST_CLASS_EXPORT_KEY(GULP::molecular_dynamics_calc)
#endif // DLV_USES_SERIALIZE

inline GULP::control_data::control_data(const bool prop, const bool fe,
					const int_g phonons, const int_g sh1,
					const int_g sh2, const int_g sh3,
					const bool usep, real_g p,
					const bool uset, real_g t,
					const bool eig, const int_g n,
					const char *path)
  : property_calc(prop), free_energy_calc(fe), pressure(p), temperature(t),
    set_temperature(uset), set_pressure(usep)
{
  switch (phonons) {
  case 1:
    phonon_calc = phonon_DOS;
    kshrinka = sh1;
    kshrinkb = sh2;
    kshrinkc = sh3;
    phonon_eigenvectors = false;
    break;
  case 2:
    phonon_calc = phonon_disp;
    band_npoints = n;
    band_path = path;
    phonon_eigenvectors = eig;
    break;
  default:
    phonon_calc = no_phonons;
    phonon_eigenvectors = false;
    break;
  }
}

inline GULP::optimize_data::optimize_data(const int_g c)
{
  switch (c) {
  case 0:
    calc_constraint = constant_pressure;
    break;
  case 1:
    calc_constraint = constant_volume;
    break;
  default:
    calc_constraint = no_constraint;
    break;
  }
}

inline GULP::moldyn_data::moldyn_data(const int_g en, const real_g step,
				      const int_g eq, const int_g pd,
				      const int_g sample, const real_g therm,
				      const real_g baro, const int_g ts)
  : timestep(step), equilibrate_steps(eq), production_steps(pd),
    sample_steps(sample), trajectory_steps(ts), thermostat(therm),
    barostat(baro)
{
  switch (en) {
  case 0:
    ensemble = ensemble_nve;
    break;
  case 1:
    ensemble = ensemble_nvt;
    break;
  case 2:
    ensemble = ensemble_npt;
    break;
  }
}

inline GULP::potential_lib::potential_lib(const bool s, DLV::string l)
  : output_shells(s), library_name(l)
{
}

inline GULP::calculation_data::calculation_data(const control_data &c,
						const potential_lib &p)
  : controls(c), potentials(p)
{
}

inline GULP::optimization_data::optimization_data(const control_data &c,
						  const optimize_data &o,
						  const potential_lib &p)
  : calculation_data(c, p), opt_data(o)
{
}

inline
GULP::molecular_dynamics_data::molecular_dynamics_data(const control_data &c,
						       const moldyn_data &m,
						       const potential_lib &p)
  : calculation_data(c, p), md_data(m)
{
}

inline GULP::calculate::calculate(const control_data &c,
				  const potential_lib &p)
  : calculation_data(c, p)
{
}

inline GULP::calc_geom::calc_geom(const control_data &c,
				  const potential_lib &p)
  :  calculation_data(c, p), calculate(c, p)
{
}

inline GULP::optimization_calc::optimization_calc(const control_data &c,
						  const optimize_data &o,
						  const potential_lib &p)
  : calculation_data(c, p), calc_geom(c, p), optimization_data(c, o, p)
{
}

inline
GULP::molecular_dynamics_calc::molecular_dynamics_calc(const control_data &c,
						       const moldyn_data &m,
						       const potential_lib &p)
  : calculation_data(c, p), calc_geom(c, p), molecular_dynamics_data(c, m, p),
    t1(0), t2(0), t3(0), md(0)
{
}

inline bool GULP::calculation_data::calc_phonon_DOS() const
{
  return (controls.phonon_calc == control_data::phonon_DOS);
}

inline bool GULP::calculation_data::calc_phonon_dispersion() const
{
  return (controls.phonon_calc == control_data::phonon_disp);
}

inline bool GULP::calculation_data::calc_phonon_vectors() const
{
  return controls.phonon_eigenvectors;
}

inline DLV::string GULP::calculation_data::get_potential_file() const
{
  return potentials.library_name;
}

inline GULP::int_g GULP::calculation_data::get_num_kpoints() const
{
  return nkpoints;
}

inline DLV::string GULP::calculation_data::get_kpoint_label(const int_g n) const
{
  return kpoints[n].label;
}

inline DLV::string GULP::calculate::get_serial_executable() const
{
  return serial_binary;
}

inline DLV::string GULP::calculate::get_parallel_executable() const
{
  return "";
}

inline void GULP::molecular_dynamics_calc::set_data(DLV::time_plot *d1,
						    DLV::time_plot *d2,
						    DLV::time_plot *d3,
						    DLV::md_trajectory *d4)
{
  t1 = d1;
  t2 = d2;
  t3 = d3;
  md = d4;
}

#endif // GULP_EXECUTE_PROG
