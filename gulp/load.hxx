
#ifndef GULP_LOAD_PROPERTIES
#define GULP_LOAD_PROPERTIES

namespace GULP {

  class load_phonon_bands : public DLV::load_data_op, public property_files {
  public:
    static operation *create(const char filename[],
			     char message[], const int_g mlen);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

    // for serialization
    load_phonon_bands(const char file[]);

  protected:
    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_phonon_DOS : public DLV::load_data_op, public property_files {
  public:
    static operation *create(const char filename[],
			     char message[], const int_g mlen);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

    // for serialization
    load_phonon_DOS(const char file[]);

  protected:
    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_phonon_vectors : public DLV::load_data_op, public property_files {
  public:
    static operation *create(const char filename[],
			     char message[], const int_g mlen);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

    // for serialization
    load_phonon_vectors(const char file[]);

  protected:
    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_MD_trajectory : public DLV::load_atom_model_op,
			     public property_files {
  public:
    static operation *create(const char filename[],
			     char message[], const int_g mlen);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

    // for serialization
    load_MD_trajectory(DLV::model *m, const char file[]);

  protected:
    DLV::string get_name() const;
    void set_data(DLV::time_plot *d1, DLV::time_plot *d2,
		  DLV::time_plot *d3, DLV::md_trajectory *d4);

  private:
    DLV::time_plot *t1;
    DLV::time_plot *t2;
    DLV::time_plot *t3;
    DLV::md_trajectory *md;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(GULP::load_phonon_bands)
BOOST_CLASS_EXPORT_KEY(GULP::load_phonon_DOS)
BOOST_CLASS_EXPORT_KEY(GULP::load_phonon_vectors)
BOOST_CLASS_EXPORT_KEY(GULP::load_MD_trajectory)
#endif // DLV_USES_SERIALIZE

inline GULP::load_phonon_bands::load_phonon_bands(const char file[])
  : load_data_op(file)
{
}

inline GULP::load_phonon_DOS::load_phonon_DOS(const char file[])
  : load_data_op(file)
{
}

inline GULP::load_phonon_vectors::load_phonon_vectors(const char file[])
  : load_data_op(file)
{
}

inline GULP::load_MD_trajectory::load_MD_trajectory(DLV::model *m,
						    const char file[])
  : load_atom_model_op(m, file), t1(0), t2(0), t3(0), md(0)
{
}

inline void GULP::load_MD_trajectory::set_data(DLV::time_plot *d1,
					       DLV::time_plot *d2,
					       DLV::time_plot *d3,
					       DLV::md_trajectory *d4)
{
  t1 = d1;
  t2 = d2;
  t3 = d3;
  md = d4;
}

#endif // GULP_LOAD_PROPERTIES
