
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_admin.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/op_model.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_plots.hxx"
#include "../dlv/data_atoms.hxx"
#include "../dlv/file.hxx"
#include "../dlv/job_setup.hxx"
#include "calcs.hxx"
#include "execute.hxx"

DLV::string GULP::calculate::serial_binary = "gulp";

#include "../graphics/calculations.hxx"

DLV::string GULP::calculate::get_path() const
{
  DLV::string path = DLV::get_code_path("GULP");
  DLV::string name = path;
  if (path[path.length() - 1] != DIR_SEP_CHR)
    name += DIR_SEP_CHR;
  DLV::string exec = name + serial_binary;
  if (DLV::file_exists(exec.c_str()))
    return name;
  exec = name + "Exe/" + serial_binary;
  if (DLV::file_exists(exec.c_str()))
    return (name + "Exe/");
  exec = name + "Src/" + serial_binary;
  if (DLV::file_exists(exec.c_str()))
    return (name + "Src/");
  // Todo
  return path;
}

DLV::operation *GULP::calculate::create(const int_g calc_type,
					const control_data &c,
					const optimize_data &o,
					const moldyn_data &m,
					const potential_lib &p,
					const DLV::job_setup_data &job,
					const bool extern_job,
					const char extern_dir[],
					char message[], const int_g mlen)
{
  calculate *op = 0;
  switch (calc_type) {
  case 0:
    op = new calculate(c, p);
    break;
  case 1:
    op = new optimization_calc(c, o, p);
    break;
  case 2:
    op = new molecular_dynamics_calc(c, m, p);
    break;
  }
  if (op == 0)
    strncpy(message, "Failed to allocate GULP::calculate operation", mlen);
  else {
    message[0] = '\0';
    // Todo - parallel
    if (op->create_files(false, job.is_local(), message, mlen))
      if (op->make_directory(message, mlen))
	op->write(get_current_model(), message, mlen);
    if (strlen(message) > 0) {
      delete op;
      op = 0;
    } else {
      if (calc_type == 0)
	op->attach();
      else
	op->attach_no_current();
      op->execute(op->get_path(), op->get_serial_executable(),
		  op->get_parallel_executable(), "", job, false,
		  extern_job, extern_dir, message, mlen);
    }
  }
  return op;
}

GULP::calculation_data::~calculation_data()
{
  // Todo ?
}

DLV::string GULP::calculate::get_name() const
{
  return ("GULP single point calc");
}

DLV::string GULP::optimization_calc::get_name() const
{
  return ("GULP structure optimization");
}

DLV::string GULP::molecular_dynamics_calc::get_name() const
{
  return ("GULP molecular dynamics run");
}

void GULP::calculate::write(const DLV::model *const structure,
			    char message[], const int_g mlen)
{
  write_input(get_infile_name(0), "", structure, message, mlen);
}

void GULP::optimization_calc::write(const DLV::model *const structure,
				    char message[], const int_g mlen)
{
  write_input(get_infile_name(0), "GULP_opt", structure, message, mlen);
}

void GULP::molecular_dynamics_calc::write(const DLV::model *const structure,
					  char message[], const int_g mlen)
{
  write_input(get_infile_name(0), "GULP_md", structure, message, mlen);
}

bool GULP::calculation_data::use_structure() const
{
  return false;
}

bool GULP::optimization_data::use_structure() const
{
  return true;
}

bool GULP::molecular_dynamics_data::use_structure() const
{
  return true;
}

bool GULP::calculation_data::use_trajectory() const
{
  return false;
}

bool GULP::molecular_dynamics_data::use_trajectory() const
{
  return true;
}

void GULP::calculation_data::write_input(DLV::string filename,
					 const DLV::string tag,
					 const DLV::model *const structure,
					 char message[], const int_g mlen)
{
  std::ofstream output;
  if (DLV::open_file_write(output, filename.c_str(), message, mlen)) {
    write_controls(output);
    output << "title\n";
    output << "DLV generated input file for ";
    DLV::string model_name = structure->get_model_name();
    output << model_name.c_str() << '\n';
    output << "end\n";
    write_data(output, structure, potentials.output_shells);
    write_final_controls(output, message, mlen);
    write_potentials(output);
    DLV::string name;
    if (tag.length() > 0)
      name = tag;
    else {
      if (model_name.length() > 0) {
	// make name safe
	for (nat_g i = 0; i < model_name.length(); i++) {
	  if (model_name[i] == ' ')
	    name += '_';
	  else if (isalnum(model_name[i]))
	    name += model_name[i];
	  else if (model_name[i] == '(')
	    break;
	}
	if (name[name.length() - 1] != '_')
	  name += '_';
      }
    }
    write_file_data(output, name);
    output.close();
  }
}

void GULP::calculation_data::write_controls(std::ofstream &output) const
{
  if (controls.property_calc)
    output << "property ";
  if (controls.phonon_calc != control_data::no_phonons)
    output << "phonon ";
  if (controls.free_energy_calc)
    output << "free_energy ";
  output << '\n';
  if (controls.set_pressure) {
    output << "pressure\n";
    output << controls.pressure << '\n';
  }
  if (controls.set_temperature) {
    output << "temperature\n";
    output << controls.temperature << '\n';
  }
}

void GULP::optimization_data::write_controls(std::ofstream &output) const
{
  output << "optimise ";
  switch (opt_data.calc_constraint) {
  case optimize_data::constant_pressure:
    output << "conp ";
    break;
  case optimize_data::constant_volume:
    output << "conv ";
    break;
  default:
    break;
  }
  calculation_data::write_controls(output);
}

void GULP::molecular_dynamics_data::write_controls(std::ofstream &output) const
{
  output << "md ";
  if (md_data.ensemble == moldyn_data::ensemble_npt)
    output << "conp ";    
  else
    output << "conv ";    
  calculation_data::write_controls(output);
  output << "ensemble ";
  switch (md_data.ensemble) {
  case moldyn_data::ensemble_nve:
    output << "nve";
    break;
  case moldyn_data::ensemble_nvt:
    output << "nvt " << md_data.thermostat;
    break;
  case moldyn_data::ensemble_npt:
    output << "npt " << md_data.thermostat << " " << md_data.barostat;
    break;
  }
  output << '\n';
  output << "timestep " << md_data.timestep << " fs\n";
  output << "equilibration ";
  output << (real_g) md_data.equilibrate_steps * md_data.timestep;
  output << " fs\n";
  output << "production ";
  output << (real_g) md_data.production_steps * md_data.timestep;
  output << " fs\n";
  output << "sample ";
  output << (real_g) md_data.sample_steps * md_data.timestep;
  output << " fs\n";
  output << "write ";
  output << (real_g) md_data.trajectory_steps * md_data.timestep;
  output << " fs\n";
}

void GULP::calculation_data::write_final_controls(std::ofstream &output,
						  char message[],
						  const int_g mlen)
{
  switch (controls.phonon_calc) {
  case control_data::phonon_DOS:
    output << "shrink ";
    output << controls.kshrinka << " " << controls.kshrinkb << " ";
    output << controls.kshrinkc << '\n';
    break;
  case control_data::phonon_disp:
    if (set_kpath(controls.band_path.c_str(), message, mlen)) {
      output << "dispersion ";
      output << nkpoints - 1 << " ";
      output << (controls.band_npoints) / (nkpoints - 1) << '\n';
      for (int_g i = 0; i < nkpoints - 1; i++) {
	output << kpoints[i].point[0] << " ";
	output << kpoints[i].point[1] << " ";
	output << kpoints[i].point[2] << " to ";
	output << kpoints[i + 1].point[0] << " ";
	output << kpoints[i + 1].point[1] << " ";
	output << kpoints[i + 1].point[2] << '\n';
      }
    }
    break;
  default:
    break;
  }
}

void GULP::calculation_data::write_potentials(std::ofstream &output) const
{
  if (potentials.library_name.length() > 0)
    output << "library " << DLV::get_file_name(potentials.library_name).c_str()
	   << '\n';
}

void GULP::calculation_data::write_file_data(std::ofstream &output,
					     const DLV::string name) const
{
  if (use_structure())
    output << "output str " << name.c_str() << '\n';
  if (controls.phonon_calc != control_data::no_phonons) {
    output << "output phon " << name.c_str() << '\n';
    if (controls.phonon_eigenvectors)
      output << "output eig " << name.c_str() << '\n';
  }
  if (use_trajectory())
    //DLV::string tjname = name;
    //tjname += ".trg";
    output << "output trajectory ascii " << name.c_str() << '\n';
}

// Copied from XSTAL band code.
bool GULP::calculation_data::set_kpath(const char kpath[],
				       char message[], const int_g mlen)
{
  int_g i = 0, j, k, slash, space, eol, l, ll, n = 0, p, pt[3], d[3];
  DLV::string path = kpath, line, sub;
  char buff[128];

  l = path.length();
  while ((i = path.find('\n', i + 1)) < l) {
    if (i < 0)
      break;
    j = i;
    n++;
  }
  if (j < (l - 1))
    n++;
  if (n < 2) {
    strncpy(message, "Insufficient k points found", mlen);
    return false;
  }
  nkpoints = n;
  kpoints = new kpoint_data[n];
  j = 0;
  for (i = 0; i < n; i++) {
    eol = path.find('\n', j);
    if (eol < 0 or eol > l)
      eol = l;
    line = path.substr(j, (eol - j));
    k = line.find_first_not_of(' ', 0);
    p = 0;
    ll = line.length();
    while (k < ll and p < 3) {
      space = line.find(' ', k);
      slash = line.find('/', k);
      sub = line.substr(k, (space - k));
      strcpy(buff, sub.c_str());
      // Todo - better error checking.
      if (slash < space and slash > 0)
	sscanf(buff, "%d/%d", &pt[p], &d[p]);
      else {
	sscanf(buff, "%d", &pt[p]);
	d[p] = 1;
      }
      k = space + 1;
      while (k < ll and line[k] == ' ')
	k++;
      p++;
    }
    if (p == 3) {
      kpoints[i].point[0] = (real_g) pt[0] / (real_g) d[0];
      kpoints[i].point[1] = (real_g) pt[1] / (real_g) d[1];
      kpoints[i].point[2] = (real_g) pt[2] / (real_g) d[2];
      // Now get the label
      if (k < ll) {
	while (line[k] == ' ')
	  k++;
	kpoints[i].label = line.substr(k, (ll - k));
      } else
	kpoints[i].label = "";
    } else {
      strncpy(message, "Unable to interpret k point", mlen);
      return false;
    }
    j = eol + 1;
  }
  return true;
}

bool GULP::calculate::setup_files(const char tag[], const bool is_parallel,
				  const bool is_local, char message[],
				  const int_g mlen)
{
  //if (is_parallel)
  //  add_input_file(0, tag, input, "INPUT", is_local, false);
  //  else
  add_command_file(0, tag, "ginp", is_local, false);
  if (get_potential_file().length() > 0)
    add_data_file(1, get_potential_file(), is_local, true);
  add_log_file(tag, "out", is_local, true);
  add_sys_error_file(tag, "err", is_local, true);
  int_g filenum = 0;
  if (use_structure()) {
    add_output_file(filenum, tag, "str", is_local, true);
    filenum++;
  }
  if (use_trajectory()) {
    add_output_file(filenum, tag, "trg", is_local, true);
    filenum++;
  }
  if (calc_phonon_DOS()) {
    add_output_file(filenum, tag, "dens", is_local, true);
    filenum++;
  }
  if (calc_phonon_dispersion()) {
    add_output_file(filenum, tag, "disp", is_local, true);
    filenum++;
  }
  if (calc_phonon_vectors()) {
    add_output_file(filenum, tag, "eig", is_local, true);
    filenum++;
  }
  return true;
}

bool GULP::calculate::create_files(const bool is_parallel,
				   const bool is_local, char message[],
				   const int_g mlen)
{
  return setup_files("", is_parallel, is_local, message, mlen);
}

bool GULP::optimization_calc::create_files(const bool is_parallel,
					   const bool is_local, char message[],
					   const int_g mlen)
{
  return setup_files("GULP_opt", is_parallel, is_local, message, mlen);
}

bool GULP::molecular_dynamics_calc::create_files(const bool is_parallel,
						 const bool is_local,
						 char message[],
						 const int_g mlen)
{
  return setup_files("GULP_md", is_parallel, is_local, message, mlen);
}

bool GULP::calculate::recover(const bool no_err, const bool log_ok,
			      char message[], const int_g len)
{
  DLV::string id = get_job_id();
  DLV::data_object *data = 0;
  if (log_ok) {
    // logfile
    DLV::string name = get_log_filename();
    if (no_err)
      data = new DLV::text_file(name, "GULP", id, "Single point output file");
    else
      data = new DLV::text_file(name, "GULP", id,
				"Single point(failed) output file");
    attach_data(data);
  }
  if (no_err) {
    int_g filenum = 0;
    if (use_structure())
      filenum++;
    DLV::data_object *data = 0;
    if (calc_phonon_DOS()) {
      data = read_phonon_DOS(this, 0, get_outfile_name(filenum).c_str(),
			     id, message, len);
      attach_data(data);
      filenum++;
    }
    if (calc_phonon_dispersion()) {
      int_g nk = get_num_kpoints();
      DLV::string *labels = new DLV::string[nk];
      for (int_g i = 0; i < nk; i++)
	labels[i] = get_kpoint_label(i);
      data = read_phonon_bands(this, 0, get_outfile_name(filenum).c_str(), id,
			       nk, labels, message, len);
      delete [] labels;
      attach_data(data);
      filenum++;
    }
    if (calc_phonon_vectors()) {
      data = read_phonon_vectors(this, get_outfile_name(filenum).c_str(),
				 id, message, len);
      attach_data(data);
      filenum++;
    }
  }
  return true;
}

bool GULP::optimization_calc::recover(const bool no_err, const bool log_ok,
				      char message[], const int_g len)
{
  DLV::string id = get_job_id();
  DLV::data_object *data = 0;
  if (log_ok) {
    // logfile
    DLV::string name = get_log_filename();
    if (no_err)
      data = new DLV::text_file(name, "GULP", id, "Optimisation output file");
    else
      data = new DLV::text_file(name, "GULP", id,
				"Optimisation(failed) output file");
    attach_data(data);
  }
  if (no_err) {
    int_g filenum = 0;
    DLV::model *structure = read("GULP opt", get_outfile_name(filenum).c_str(),
				 message, len);
    filenum++;
    if (structure != 0) {
      attach_model(structure);
      DLV::data_object *data = 0;
      if (calc_phonon_DOS()) {
	data = read_phonon_DOS(this, 0, get_outfile_name(filenum).c_str(),
			       id, message, len);
	attach_data(data);
	filenum++;
      }
      if (calc_phonon_dispersion()) {
	int_g nk = get_num_kpoints();
	DLV::string *labels = new DLV::string[nk];
	for (int_g i = 0; i < nk; i++)
	  labels[i] = get_kpoint_label(i);
	data = read_phonon_bands(this, 0, get_outfile_name(filenum).c_str(), id,
				 nk, labels, message, len);
	delete [] labels;
	attach_data(data);
	filenum++;
      }
      if (calc_phonon_vectors()) {
	data = read_phonon_vectors(this, get_outfile_name(filenum).c_str(),
				   id, message, len);
	attach_data(data);
	filenum++;
      }
    }
  } else {
    // Need a model to attach for failed job
    DLV::model *str = get_parent()->get_model();
    attach_model(str->duplicate_model(str->get_model_name() + " failed opt"));
  }
  set_current();
  return true;
}

bool GULP::molecular_dynamics_calc::recover(const bool no_err,
					    const bool log_ok,
					    char message[], const int_g len)
{
  DLV::string id = get_job_id();
  DLV::data_object *data = 0;
  if (log_ok) {
    // logfile
    DLV::string name = get_log_filename();
    if (no_err)
      data = new DLV::text_file(name, "GULP", id, "MD output file");
    else
      data = new DLV::text_file(name, "GULP", id,
				"MD(failed) output file");
    attach_data(data);
  }
  if (no_err) {
    int_g filenum = 0;
    DLV::model *structure = read("GULP md", get_outfile_name(filenum).c_str(),
				 message, len);
    filenum++;
    if (structure != 0) {
      DLV::data_object *data = 0;
      if (calc_phonon_DOS()) {
	data = read_phonon_DOS(this, 0, get_outfile_name(filenum).c_str(),
			       id, message, len);
	attach_data(data);
	filenum++;
      }
      if (calc_phonon_dispersion()) {
	int_g nk = get_num_kpoints();
	DLV::string *labels = new DLV::string[nk];
	for (int_g i = 0; i < nk; i++)
	  labels[i] = get_kpoint_label(i);
	data = read_phonon_bands(this, 0, get_outfile_name(filenum).c_str(), id,
				 nk, labels, message, len);
	delete [] labels;
	attach_data(data);
	filenum++;
      }
      if (calc_phonon_vectors()) {
	data = read_phonon_vectors(this, get_outfile_name(filenum).c_str(),
				   id, message, len);
	attach_data(data);
	filenum++;
      }
      DLV::time_plot *d1 = 0;
      DLV::time_plot *d2 = 0;
      DLV::time_plot *d3 = 0;
      DLV::md_trajectory *d4 = 0;
      if (read_MD_trajectory(this, d1, d2, d3, d4,
			     get_outfile_name(filenum).c_str(), id,
			     structure, true, message, len)) {
	set_data(d1, d2, d3, d4);
	attach_data(d1);
	attach_data(d2);
	attach_data(d3);
	attach_data(d4);
      }
    }
    attach_model(structure);
    set_current();
  }
  return true;
}

void GULP::optimization_calc::fixup_job()
{
  // Need a model to attach for failed job
  DLV::model *str = get_parent()->get_model();
  attach_model(str->duplicate_model(str->get_model_name() + " failed MD"));
}

void GULP::molecular_dynamics_calc::fixup_job()
{
  // Need a model to attach for failed job
  DLV::model *str = get_parent()->get_model();
  attach_model(str->duplicate_model(str->get_model_name() + " failed MD"));
}

bool GULP::calculate::reload_data(DLV::data_object *data,
				  char message[], const int_g mlen)
{
  int_g filenum = 0;
  if (use_structure())
    filenum++;
  if (calc_phonon_DOS()) {
    DLV::phonon_dos *v = dynamic_cast<DLV::phonon_dos *>(data);
    if (v != 0)
      return (read_phonon_DOS(this, v, get_outfile_name(filenum).c_str(),
			      "", message, mlen) != 0);
    filenum++;
  }
  if (calc_phonon_dispersion())  {
    DLV::phonon_bands *v = dynamic_cast<DLV::phonon_bands *>(data);
    if (v != 0)
      return (read_phonon_bands(this, v, get_outfile_name(filenum).c_str(),
				"", 0, 0, message, mlen) != 0);
    filenum++;
  }
  strncpy(message, "Incorrect data object to reload phonons", mlen);
  return false;
}

bool GULP::optimization_calc::reload_data(DLV::data_object *data,
					  char message[], const int_g mlen)
{
  int_g filenum = 0;
  filenum++;
  if (calc_phonon_DOS()) {
    DLV::phonon_dos *v = dynamic_cast<DLV::phonon_dos *>(data);
    if (v != 0)
      return (read_phonon_DOS(this, v, get_outfile_name(filenum).c_str(),
			      "", message, mlen) != 0);
    filenum++;
  }
  if (calc_phonon_dispersion())  {
    DLV::phonon_bands *v = dynamic_cast<DLV::phonon_bands *>(data);
    if (v != 0)
      return (read_phonon_bands(this, v, get_outfile_name(filenum).c_str(),
				"", 0, 0, message, mlen) != 0);
    filenum++;
  }
  strncpy(message, "Incorrect data object to reload phonons", mlen);
  return false;
}

bool GULP::molecular_dynamics_calc::reload_data(DLV::data_object *data,
						char message[],
						const int_g mlen)
{
  int_g filenum = 0;
  filenum++;
  if (calc_phonon_DOS()) {
    DLV::phonon_dos *v = dynamic_cast<DLV::phonon_dos *>(data);
    if (v != 0)
      return (read_phonon_DOS(this, v, get_outfile_name(filenum).c_str(),
			      "", message, mlen) != 0);
    filenum++;
  }
  if (calc_phonon_dispersion())  {
    DLV::phonon_bands *v = dynamic_cast<DLV::phonon_bands *>(data);
    if (v != 0)
      return (read_phonon_bands(this, v, get_outfile_name(filenum).c_str(),
				"", 0, 0, message, mlen) != 0);
    filenum++;
  }
  if (calc_phonon_vectors())
    filenum++;
  if (read_MD_trajectory(this, t1, t2, t3, md,
			 get_outfile_name(filenum).c_str(),
			 "", get_current_model(),
			 false, message, mlen) != 0)
    return true;
  strncpy(message, "Incorrect data object to reload md trajectory", mlen);
  return false;
}

bool GULP::calculate::no_errors(char message[], const int_g len)
{
  if (strlen(message) > 0)
    strcat(message, "\n");
  DLV::string name = get_sys_error_file();
  if (DLV::check_filename(name.c_str(), message, len)) {
    std::ifstream error_file;
    if (DLV::open_file_read(error_file, name.c_str(), message, len)) {
      if (!error_file.eof()) {
	char buff[256];
	error_file.getline(buff, 256);
	if (strlen(buff) > 0 and strncmp(buff, "FORTRAN STOP", 13) != 0) {
	  strncat(message, buff, len - strlen(message) - 1);
	  return false;
	}
      }
      error_file.close();
    }
    return true; // empty file -> no errors
  }
  return false;
}

bool GULP::calc_geom::is_geometry() const
{
  return true;
}

void GULP::calc_geom::inherit_model()
{
  // Null op, we have a new structure
}

void GULP::calc_geom::inherit_data()
{
  // Todo - I think this is possibly a null op, but see map_data
}

void GULP::calc_geom::add_standard_data_objects()
{
  DLV::data_object *data = new DLV::atom_and_bond_data();
  if (data != 0) {
    attach_data(data);
#ifdef ENABLE_DLV_GRAPHICS
    render_data(data);
#endif // ENABLE_DLV_GRAPHICS
  }
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, GULP::calculate *t,
				    const unsigned int file_version)
    {
      GULP::control_data c(false, false, 0, 0, 0, 0, false, 0.0, false,
			   0.0, false, 0, 0);
      GULP::potential_lib p(false, "recover");
      ::new(t)GULP::calculate(c, p);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, GULP::calc_geom *t,
				    const unsigned int file_version)
    {
      GULP::control_data c(false, false, 0, 0, 0, 0, false, 0.0, false,
			   0.0, false, 0, 0);
      GULP::potential_lib p(false, "recover");
      ::new(t)GULP::calc_geom(c, p);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, GULP::optimization_calc *t,
				    const unsigned int file_version)
    {
      GULP::control_data c(false, false, 0, 0, 0, 0, false, 0.0, false,
			   0.0, false, 0, 0);
      GULP::optimize_data o(0);
      GULP::potential_lib p(false, "recover");
      ::new(t)GULP::optimization_calc(c, o, p);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    GULP::molecular_dynamics_calc *t,
				    const unsigned int file_version)
    {
      GULP::control_data c(false, false, 0, 0, 0, 0, false, 0.0, false,
			   0.0, false, 0, 0);
      GULP::moldyn_data m(0, 0.0, 0, 0, 0, 0.0, 0.0, 0);
      GULP::potential_lib p(false, "recover");
      ::new(t)GULP::molecular_dynamics_calc(c, m, p);
    }

  }
}

/*
namespace boost {
  namespace detail {

    // I hope these are right, its the contained calculation_data that
    // is virtual not the bits that also use it. Todo - check
    template <> struct is_virtual_base_of_impl2<GULP::optimization_data,
						GULP::optimization_calc> {
      static const bool value = false;
    };

    template <> struct is_virtual_base_of_impl2<GULP::molecular_dynamics_data,
						GULP::molecular_dynamics_calc> {
      static const bool value = false;
    };

  }
}
*/

template <class Archive>
void GULP::control_data::serialize(Archive &ar, const unsigned int version)
{
  ar & property_calc;
  ar & free_energy_calc;
  ar & phonon_calc;
  ar & kshrinka;
  ar & kshrinkb;
  ar & kshrinkc;
  ar & pressure;
  ar & temperature;
  ar & set_temperature;
  ar & set_pressure;
  ar & band_npoints;
  ar & band_path;
}

template <class Archive>
void GULP::optimize_data::serialize(Archive &ar, const unsigned int version)
{
  ar & calc_constraint;
}

template <class Archive>
void GULP::moldyn_data::serialize(Archive &ar, const unsigned int version)
{
  ar & ensemble;
  ar & timestep;
  ar & equilibrate_steps;
  ar & production_steps;
  ar & sample_steps;
  ar & trajectory_steps;
  ar & thermostat;
  ar & barostat;
}

template <class Archive>
void GULP::potential_lib::serialize(Archive &ar, const unsigned int version)
{
  ar & output_shells;
  ar & library_name;
}

template <class Archive>
void GULP::kpoint_data::serialize(Archive &ar, const unsigned int version)
{
  ar & point;
  ar & label;
}

template <class Archive>
void GULP::calculation_data::serialize(Archive &ar, const unsigned int version)
{
  ar & controls;
  ar & potentials;
  ar & kpoints; // Todo - loop to save all of them?
  ar & nkpoints;
}

template <class Archive>
void GULP::optimization_data::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<GULP::calculation_data>(*this);
  ar & opt_data;
}

template <class Archive>
void GULP::molecular_dynamics_data::serialize(Archive &ar,
					      const unsigned int version)
{
  ar & boost::serialization::base_object<GULP::calculation_data>(*this);
  ar & md_data;
}

template <class Archive>
void GULP::calculate::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::batch_calc>(*this);
  ar & boost::serialization::base_object<GULP::calculation_data>(*this);
}

template <class Archive>
void GULP::calc_geom::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<calculate>(*this);
}

template <class Archive>
void GULP::optimization_calc::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<GULP::calc_geom>(*this);
  ar & boost::serialization::base_object<GULP::optimization_data>(*this);
}

template <class Archive>
void GULP::molecular_dynamics_calc::serialize(Archive &ar,
					      const unsigned int version)
{
  ar & boost::serialization::base_object<GULP::calc_geom>(*this);
  ar & 
    boost::serialization::base_object<GULP::molecular_dynamics_data>(*this);
  ar & t1;
  ar & t2;
  ar & t3;
  ar & md;
}

BOOST_CLASS_EXPORT_IMPLEMENT(GULP::calculate)
BOOST_CLASS_EXPORT_IMPLEMENT(GULP::calc_geom)
BOOST_CLASS_EXPORT_IMPLEMENT(GULP::optimization_calc)
BOOST_CLASS_EXPORT_IMPLEMENT(GULP::molecular_dynamics_calc)

DLV_SUPPRESS_TEMPLATES(DLV::batch_calc)
DLV_SUPPRESS_TEMPLATES(DLV::md_trajectory)
DLV_SUPPRESS_TEMPLATES(DLV::time_plot)

DLV_NORMAL_EXPLICIT(GULP::calculate)
DLV_NORMAL_EXPLICIT(GULP::calc_geom)
DLV_NORMAL_EXPLICIT(GULP::optimization_calc)
DLV_NORMAL_EXPLICIT(GULP::molecular_dynamics_calc)

#endif // DLV_USES_SERIALIZE
