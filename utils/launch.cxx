
#include "windows.h"
#include <string.h>

int main(int argc, char *argv[])
{
  static char args[1024];
  int i;
  BOOL bret = FALSE;
  STARTUPINFO si = {0};
  PROCESS_INFORMATION pi = {0};

  args[0] = '\0';
  for (i = 1; i < argc; i++) {
    strcat(args, argv[i]);
    strcat(args, " ");
  }
  si.cb = sizeof(si);
  si.dwFlags = STARTF_USESTDHANDLES;
  si.hStdInput = GetStdHandle(STD_INPUT_HANDLE);
  si.hStdOutput = GetStdHandle(STD_OUTPUT_HANDLE);
  si.hStdError = GetStdHandle(STD_ERROR_HANDLE);

  bret = CreateProcess(NULL, args, NULL, NULL,
		       TRUE, 0, NULL, NULL, &si, &pi);
  if (bret) {
    WaitForSingleObject(pi.hProcess, INFINITE);
    CloseHandle(pi.hProcess);
    CloseHandle(pi.hThread);
    return 0;
  }
  return 1;
}
