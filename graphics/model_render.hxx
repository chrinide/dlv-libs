
#ifndef DLV_MODEL_RENDERER
#define DLV_MODEL_RENDERER

#ifdef DLV_USES_AVS_GRAPHICS
// Forward declarations
class CCP3_Renderers_Model_Model_R;
class CCP3_Renderers_Model_Model_K;
class CCP3_Renderers_Model_Outline;
class CCP3_Renderers_Model_Shell;
#endif // AVS_GRAPHICS

#ifdef DLV_USES_VTK_GRAPHICS
// Forward declare - or just include vtk/render_base?
namespace CCP3 {

  class model_atoms;
  class model_kspace;
  class model_outline;
  class model_shells;

}

#endif // DLV_USES_VTK_GRAPHICS

namespace DLV {

  class model_display_obj {
  public:
    virtual ~model_display_obj();
  };

  template <class render_t> class rspace_model_templ :
    public model_display_obj {
  public:
    rspace_model_templ(render_parent *p);
    ~rspace_model_templ();
    class toolkit_obj get_ui_obj() const;

    void deactivate_atom_flags();
    void activate_atom_flags(const string label);
    void toggle_atom_flags(const string label);

    void copy_settings(const rspace_model_templ *parent);
    void get_lattice_data(bool &draw, bool &labels) const;
    void set_lattice(float ipoints[][3], float fpoints[][3], const int n);
    void set_lattice_labels(float points[][3], const char *labels[],
			    const int n);
    void detach_lattice();
    void detach_lattice_labels();
    void get_atom_data(bool &draw, float &scale, int &select,
		       bool &use_flags, int &flag_type) const;
    int get_atom_group_index() const;
    int get_selection_symmetry() const;
    void set_selection_symmetry(const int s);
    void draw_opaque_atoms(float coords[][3], float colours[][3],
			   float radii[], const int natoms, const int nframes);
    void draw_transparent_atoms(float coords[][3], float colours[][3],
				float radii[], const int natoms,
				const int nframes);
    void draw_edit_atoms(float coords[][3], float colours[][3],
			 float radii[], const int natoms, const int nframes);
    void detach_opaque_atoms();
    void detach_transparent_atoms();
    void detach_edit_atoms();
    void detach_all_atoms();
    void get_bond_data(bool &draw, bool &polyhedra, float &overlap,
		       bool &tubes, float &radius, int &subdiv) const;
    void draw_opaque_bonds(float coords[][3], float colours[][3],
			   const long nbonds, long connects[], const long nc,
			   const int nframes, const bool tubes,
			   const int frame);
    void draw_transparent_bonds(float coords[][3], float colours[][3],
				const long nbonds, long connects[],
				const long nc, const int nframes,
				const bool tubes, const int frame);
    void draw_edit_bonds(float coords[][3], float colours[][3],
			 const long nbonds, long connects[], const long nc,
			 const int nframes, const bool tubes, const int frame);
    void detach_opaque_bonds();
    void detach_transparent_bonds();
    void detach_edit_bonds();
    void detach_all_bonds();
    void draw_polyhedra(float coords[][3], float colours[][3],
			const long nverts, long connects[], const long nc,
			const int nframes, const int frame);
    void detach_polyhedra();
    void attach_editor(const bool v, const toolkit_obj &t, float matrix[4][4],
		       float translate[3], float centre[3], char message[],
		       const int mlen) const;

  private:
    render_t *obj;
    bool lattice_attached;
    bool lattice_labels;
    bool opaque_atoms;
    bool transparent_atoms;
    bool edit_atoms;
    bool opaque_line_bonds;
    bool transparent_line_bonds;
    bool edit_line_bonds;
    bool opaque_tube_bonds;
    bool transparent_tube_bonds;
    bool edit_tube_bonds;
    bool polyhedra;

    void detach_atoms();
    void detach_line_bonds();
    void detach_tube_bonds();

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  template <class render_t> class kspace_model_templ :
    public model_display_obj {
  public:
    kspace_model_templ(render_parent *p);
    ~kspace_model_templ();
    class toolkit_obj get_ui_obj() const;

    void get_lattice_data(bool &draw, bool &labels) const;
    void set_lattice(float vertices[][3], const int nv, long connects[],
		     const long nc);
    void detach_lattice();

  private:
    render_t *obj;
    bool lattice_attached;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  template <class render_t> class outline_model_templ :
    public model_display_obj {
  public:
    outline_model_templ(render_parent *p);
    ~outline_model_templ();
    class toolkit_obj get_ui_obj() const;

    void set_data(float vertices[][3], const int nvertices, int planes[],
		  const int nplanes, float iradius[], float oradius[],
		  float dmin[], float dmax[], int orientation[],
		  const int ncylinders, float radii[], const int nspheres);

  private:
    render_t *obj;
    bool data_set;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  template <class render_t> class shell_model_templ : public model_display_obj {
  public:
    shell_model_templ(render_parent *p);
    ~shell_model_templ();
    class toolkit_obj get_ui_obj() const;

    void draw(const float colour[3], const int nshells,
	      const float radii[], const float shell_colours[][3]);

  private:
    render_t *obj;
    bool is_drawn;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

#ifdef DLV_USES_AVS_GRAPHICS
  typedef rspace_model_templ<CCP3_Renderers_Model_Model_R> model_display_r;
  typedef kspace_model_templ<CCP3_Renderers_Model_Model_K> model_display_k;
  typedef outline_model_templ<CCP3_Renderers_Model_Outline> model_outline_r;
  typedef shell_model_templ<CCP3_Renderers_Model_Shell> model_shell_r;
#elif defined(DLV_USES_VTK_GRAPHICS)
  typedef rspace_model_templ<CCP3::model_atoms> model_display_r;
  typedef kspace_model_templ<CCP3::model_kspace> model_display_k;
  typedef outline_model_templ<CCP3::model_outline> model_outline_r;
  typedef shell_model_templ<CCP3::model_shells> model_shell_r;
#else
  typedef rspace_model_templ<void> model_display_r;
  typedef kspace_model_templ<void> model_display_k;
  typedef outline_model_templ<void> model_outline_r;
  typedef shell_model_templ<void> model_shell_r;
#endif // ! AVS_GRAPHICS

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(DLV::model_display_r)
BOOST_CLASS_EXPORT_KEY(DLV::model_display_k)
BOOST_CLASS_EXPORT_KEY(DLV::model_outline_r)
BOOST_CLASS_EXPORT_KEY(DLV::model_shell_r)
#endif // DLV_USES_SERIALIZE

#endif // DLV_MODEL_RENDERER
