
#ifndef DLV_AVS_EVENTS
#define DLV_AVS_EVENTS

namespace DLV {

  extern void attach_event(const int socket, void (*fn)(char *), void *ptr);
  extern void detach_event(const int socket, void (*fn)(char *), void *ptr);

}

#endif // DLV_AVS_EVENTS
