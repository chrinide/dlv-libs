
#ifndef DLV_EDIT_OBJECT
#define DLV_EDIT_OBJECT

namespace DLV {

  class edited_obj {
  public:
    edited_obj(const bool sp);
    virtual ~edited_obj();

    static edited_obj *create_orthoslice(const render_parent *parent,
					 const class drawable_obj *draw,
					 const bool kspace, char message[],
					 const int mlen);
    static edited_obj *create_slice(const render_parent *parent,
				    const class drawable_obj *draw,
				    const bool kspace, char message[],
				    const int mlen);
    static edited_obj *create_clamp(const render_parent *parent,
				    const class drawable_obj *draw,
				    const int component, const bool kspace,
				    char message[], const int mlen);
    static edited_obj *create_crop(const render_parent *parent,
				   const class drawable_obj *draw,
				   const bool kspace, char message[],
				   const int mlen);
    static edited_obj *create_cut(const render_parent *parent,
				  const class drawable_obj *draw,
				  const bool kspace, char message[],
				  const int mlen);
    static edited_obj *create_downsize(const render_parent *parent,
				       const class drawable_obj *draw,
				       const bool kspace, char message[],
				       const int mlen);
    static edited_obj *create_extension(const render_parent *parent,
					const class drawable_obj *draw,
					const bool kspace, const int x,
					const int y, const int z,
					char message[], const int mlen);
    static edited_obj *create_math(const render_parent *parent,
				   const class drawable_obj *draw,
				   const int component, const bool kspace,
				   char message[], const int mlen);
    static edited_obj *create_vol_render(const render_parent *parent,
					 const class drawable_obj *draw,
					 const bool kspace, char message[],
					 const int mlen);
    static edited_obj *create_dos_shift(const render_parent *parent,
					const class drawable_obj *draw,
					const bool kspace, char message[],
					const int mlen);
    static edited_obj *create_band_shift(const render_parent *parent,
					 const class drawable_obj *draw,
					 const bool kspace, char message[],
					 const int mlen);
    static edited_obj *create_bdos_shift(const render_parent *parent,
					 const class drawable_obj *draw,
					 const bool kspace, char message[],
					 const int mlen);
    static edited_obj *create_spectrum(const render_parent *parent,
				       const int index, char message[],
				       const int mlen);
    static edited_obj *create_phonon_traj(const render_parent *parent,
					  const int index, char message[],
					  const int mlen);

    void update_edit_list(const render_parent *parent, const string name,
			  const int index);
    virtual void attach_params() const = 0;
    class drawable_obj *get_drawable() const;

    virtual bool update_spectrum(float grid[], float data[], const int n,
				 const string name, char message[],
				 const int mlen);
    virtual bool update_slice(const class drawable_obj *draw, char message[],
			      const int mlen);
    virtual bool update_cut(const class drawable_obj *draw, char message[],
			    const int mlen);
    virtual bool update_extension(const int na, const int nb, const int nc,
				  const float o[3], const float a[3],
				  const float b[3], const float c[3],
				  const int onx, const int ony,
				  const int onz, float *origdata[],
				  const int vdims[], const int ndata,
				  char message[], const int mlen);
    virtual bool update_math(const class drawable_obj *draw, const int cmpt,
			     const int index, char message[], const int mlen);
    virtual bool update_volr(const float o[3], const float a[3],
			     const float b[3], const float c[3],
			     const int nx, const int ny,
			     const int nz, float *origdata[],
			     const int vdims[], const int ndata,
			     char message[], const int mlen);
    virtual bool update_shift(const float shift, char message[],
			      const int mlen);

    virtual void reload_data(class drawable_obj *draw) = 0;

    bool is_k_space() const;

  protected:
    virtual int get_type() const = 0;
    void set_drawable(class drawable_obj *d);
    class drawable_obj *replace_edit(class toolkit_obj &id);

  private:
    class drawable_obj *draw;
    bool reciprocal_space;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_SERIALIZATION_ASSUME_ABSTRACT(DLV::edited_obj)
BOOST_CLASS_EXPORT_KEY(DLV::edited_obj)
#endif // DLV_USES_SERIALIZE

inline DLV::edited_obj::edited_obj(const bool sp)
  : draw(0), reciprocal_space(sp)
{
}

inline class DLV::drawable_obj *DLV::edited_obj::get_drawable() const
{
  return draw;
}

inline void DLV::edited_obj::set_drawable(class drawable_obj *d)
{
  draw = d;
}

inline bool DLV::edited_obj::is_k_space() const
{
  return reciprocal_space;
}

#endif // DLV_EDIT_OBJECT
