
#ifndef DLV_TOOLKIT_OBJ
#define DLV_TOOLKIT_OBJ

#ifdef DLV_USES_AVS_GRAPHICS
#  include <avs/omx.hxx>
typedef OMobj_id tool_id;
#elif defined(DLV_USES_VTK_GRAPHICS)
typedef void *tool_id;
#  define OMnull_obj nullptr
#else
typedef int tool_id;
const int OMnull_obj = 0;
#endif

namespace DLV {

  class toolkit_obj {
  public:
    toolkit_obj();
    toolkit_obj(const tool_id id);

    tool_id get_id() const;

  private:
    tool_id obj;
  };

}

inline DLV::toolkit_obj::toolkit_obj() : obj(OMnull_obj)
{
}

inline DLV::toolkit_obj::toolkit_obj(const tool_id id) : obj(id)
{
}

inline tool_id DLV::toolkit_obj::get_id() const
{
  return obj;
}

#endif // DLV_TOOLKIT_OBJ
