
#ifdef ENABLE_DLV_GRAPHICS
#  if defined(DLV_USES_AVS_GRAPHICS) || defined(DLV_USES_VTK_GRAPHICS)
#    error "This dummy graphics interface should only be used when no graphics library is defined"
#  endif // Check for AVS/VTK
#else
#  error "ENABLE DLV_GRAPHICS should be defined when building this dummy graphics interface"
#endif // GRAPHICS check

#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#include "../graphics/toolkit_obj.hxx"
#include "../graphics/render_base.hxx"
#include "../graphics/model_render.hxx"

DLV::model_display_obj::~model_display_obj()
{
}

template <class render_t>
DLV::rspace_model_templ<render_t>::~rspace_model_templ()
{
  //delete obj;
}

template <class render_t>
DLV::kspace_model_templ<render_t>::~kspace_model_templ()
{
  //delete obj;
}

template <class render_t>
DLV::outline_model_templ<render_t>::~outline_model_templ()
{
  //delete obj;
}

template <class render_t>
DLV::shell_model_templ<render_t>::~shell_model_templ()
{
  //delete obj;
}

template <class render_t>
DLV::rspace_model_templ<render_t>::rspace_model_templ(render_parent *p)
  : lattice_attached(false), lattice_labels(false), opaque_atoms(false),
    transparent_atoms(false), edit_atoms(false), opaque_line_bonds(false),
    transparent_line_bonds(false), edit_line_bonds(false),
    opaque_tube_bonds(false), transparent_tube_bonds(false),
    edit_tube_bonds(false), polyhedra(false)
{
  obj = nullptr;
}

template <class render_t>
DLV::kspace_model_templ<render_t>::kspace_model_templ(render_parent *p)
  : lattice_attached(false)
{
  obj = nullptr;
}

template <class render_t> void
DLV::rspace_model_templ<render_t>::copy_settings(const rspace_model_templ *parent)
{
}

template <class render_t>
DLV::toolkit_obj DLV::rspace_model_templ<render_t>::get_ui_obj() const
{
  return toolkit_obj();
}

template <class render_t>
DLV::toolkit_obj DLV::kspace_model_templ<render_t>::get_ui_obj() const
{
  return toolkit_obj();
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::deactivate_atom_flags()
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::activate_atom_flags(const string label)
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::toggle_atom_flags(const string label)
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::get_lattice_data(bool &draw,
							 bool &labels) const
{
  draw = false;
  labels = false;
}

template <class render_t>
void DLV::kspace_model_templ<render_t>::get_lattice_data(bool &draw,
							 bool &labels) const
{
  draw = false;
  labels = false;
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::set_lattice(float ipoints[][3],
						    float fpoints[][3],
						    const int n)
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::set_lattice_labels(float points[][3],
							   const char *labels[],
							   const int n)
{
}

template <class render_t>
void DLV::kspace_model_templ<render_t>::set_lattice(float vertices[][3],
						    const int nv,
						    long connects[],
						    const long nc)
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_lattice()
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_lattice_labels()
{
}

template <class render_t>
void DLV::kspace_model_templ<render_t>::detach_lattice()
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::get_atom_data(bool &draw, float &scale,
						      int &select,
						      bool &use_flags,
						      int &flag_type) const
{
  draw = false;
  scale = false;
  select = 0;
  // Todo?
  use_flags = false;
  flag_type = 0;
}

template <class render_t>
int DLV::rspace_model_templ<render_t>::get_atom_group_index() const
{
  return 0;
}

template <class render_t>
int DLV::rspace_model_templ<render_t>::get_selection_symmetry() const
{
  return 0;
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::set_selection_symmetry(const int s)
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::draw_opaque_atoms(float coords[][3],
							  float colours[][3],
							  float radii[],
							  const int natoms,
							  const int nframes)
{
}

template <class render_t> void
DLV::rspace_model_templ<render_t>::draw_transparent_atoms(float coords[][3],
							  float colours[][3],
							  float radii[],
							  const int natoms,
							  const int nframes)
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::draw_edit_atoms(float coords[][3],
							float colours[][3],
							float radii[],
							const int natoms,
							const int nframes)
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_atoms()
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_opaque_atoms()
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_transparent_atoms()
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_edit_atoms()
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_all_atoms()
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::get_bond_data(bool &draw,
						      bool &polyhedra,
						      float &overlap,
						      bool &tubes,
						      float &radius,
						      int &subdiv) const
{
  draw = false;
  polyhedra = false;
  overlap = 1.0;
  tubes = false;
  radius = 1.0;
  subdiv = 16;
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::draw_opaque_bonds(float coords[][3],
							  float colours[][3],
							  const long nbonds,
							  long connects[],
							  const long nc,
							  const int nframes,
							  const bool tubes,
							  const int frame)
{
}

template <class render_t> void
DLV::rspace_model_templ<render_t>::draw_transparent_bonds(float coords[][3],
							  float colours[][3],
							  const long nbonds,
							  long connects[],
							  const long nc,
							  const int nframes,
							  const bool tubes,
							  const int frame)
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::draw_edit_bonds(float coords[][3],
							float colours[][3],
							const long nbonds,
							long connects[],
							const long nc,
							const int nframes,
							const bool tubes,
							const int frame)
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_line_bonds()
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_tube_bonds()
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_opaque_bonds()
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_transparent_bonds()
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_edit_bonds()
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_all_bonds()
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::draw_polyhedra(float coords[][3],
						       float colours[][3],
						       const long nverts,
						       long connects[],
						       const long nc,
						       const int nframes,
						       const int frame)
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_polyhedra()
{
}

template <class render_t>
DLV::outline_model_templ<render_t>::outline_model_templ(render_parent *p)
  : data_set(false)
{
  obj = nullptr;
}

template <class render_t>
DLV::toolkit_obj DLV::outline_model_templ<render_t>::get_ui_obj() const
{
  return toolkit_obj();
}

template <class render_t>
void DLV::outline_model_templ<render_t>::set_data(float vertices[][3],
						  const int nvertices,
						  int planes[],
						  const int nplanes,
						  float iradius[],
						  float oradius[],
						  float dmin[], float dmax[],
						  int orientation[],
						  const int ncylinders,
						  float radii[],
						  const int nspheres)
{
}

template <class render_t>
DLV::shell_model_templ<render_t>::shell_model_templ(render_parent *p)
  : is_drawn(false)
{
  obj = nullptr;
}

template <class render_t>
DLV::toolkit_obj DLV::shell_model_templ<render_t>::get_ui_obj() const
{
  return toolkit_obj();
}

template <class render_t>
void DLV::shell_model_templ<render_t>::draw(const float colour[3],
					    const int nshells,
					    const float radii[],
					    const float shell_colours[][3])
{
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::attach_editor(const bool v,
						      const toolkit_obj &t,
						      float matrix[4][4],
						      float translate[3],
						      float centre[3],
						      char message[],
						      const int mlen) const
{
}

template class DLV::rspace_model_templ<void>;
template class DLV::kspace_model_templ<void>;
template class DLV::outline_model_templ<void>;
template class DLV::shell_model_templ<void>;

#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::model_display_r *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::model_display_r(DLV::render_parent::get_serialize_obj());
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::model_display_k *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::model_display_k(DLV::render_parent::get_serialize_obj());
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::model_outline_r *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::model_outline_r(DLV::render_parent::get_serialize_obj());
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::model_shell_r *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::model_shell_r(DLV::render_parent::get_serialize_obj());
    }

  }
}

template <class render_t> template <class Archive>
void DLV::rspace_model_templ<render_t>::save(Archive &ar,
					     const unsigned int version) const
{
}

template <class render_t> template <class Archive>
void DLV::rspace_model_templ<render_t>::load(Archive &ar,
					     const unsigned int version)
{
}

template <class render_t> template <class Archive>
void DLV::kspace_model_templ<render_t>::save(Archive &ar,
					     const unsigned int version) const
{
}

template <class render_t> template <class Archive>
void DLV::kspace_model_templ<render_t>::load(Archive &ar,
					     const unsigned int version)
{
}

template <class render_t> template <class Archive>
void DLV::outline_model_templ<render_t>::save(Archive &ar,
					      const unsigned int version) const
{
}

template <class render_t> template <class Archive>
void DLV::outline_model_templ<render_t>::load(Archive &ar,
					      const unsigned int version)
{
}

template <class render_t> template <class Archive>
void DLV::shell_model_templ<render_t>::save(Archive &ar,
					    const unsigned int version) const
{
  // Todo
}

template <class render_t> template <class Archive>
void DLV::shell_model_templ<render_t>::load(Archive &ar,
					   const unsigned int version)
{
  // Todo - obj needs to exist!
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::model_display_r)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::model_display_k)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::model_outline_r)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::model_shell_r)

#endif // DLV_USES_SERIALIZE
