
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#include "../dlv/constants.hxx"
#include "../graphics/toolkit_obj.hxx"
#include "../graphics/render_base.hxx"
#include "../graphics/drawable.hxx"

DLV::drawable_obj::~drawable_obj()
{
}

DLV::drawable_obj *
DLV::drawable_obj::create_lengths_angles(const render_parent *parent,
					 const string formula,
					 char message[], const int mlen)
{
  return nullptr;
}

DLV::drawable_obj *
DLV::drawable_obj::create_text_value(const render_parent *parent,
				     const string value, char message[],
				     const int mlen)
{
  return nullptr;
}

DLV::drawable_obj *
DLV::drawable_obj::create_real_value(const render_parent *parent,
				     const double value, char message[],
				     const int mlen)
{
  return nullptr;
}

DLV::drawable_obj *
DLV::drawable_obj::create_text_file(const render_parent *parent,
				    const string name, char message[],
				    const int mlen)
{
  return nullptr;
}

DLV::drawable_obj *
DLV::drawable_obj::create_text_buff(const render_parent *parent,
				    char message[], const int mlen)
{
  return nullptr;
}

DLV::drawable_obj *
DLV::drawable_obj::create_text_field(const render_parent *parent,
				     float coords[][3], int **data,
				     const string labels[], const int ndata,
				     const int natoms, char message[],
				     const int mlen)
{
  return nullptr;
}

DLV::drawable_obj *
DLV::drawable_obj::create_text_field(const render_parent *parent,
				     float coords[][3], float **data,
				     const string labels[], const int ndata,
				     const int natoms, char message[],
				     const int mlen)
{
  return nullptr;
}

DLV::drawable_obj *
DLV::drawable_obj::create_vector_field(const render_parent *parent,
				       float coords[][3], float data[][3],
				       const int natoms, const int ncmpts,
				       char message[], const int mlen)
{
  return nullptr;
}

DLV::drawable_obj *
DLV::drawable_obj::create_volume(const render_parent *parent,
				 const float origin[3], const float astep[3],
				 const float bstep[3], const float cstep[3],
				 float *data[], const int nx, const int ny,
				 const int nz, const int dims[],
				 const string labels[], const int ndata,
				 const bool kspace, const bool use_ignore,
				 const float ignore, char message[],
				 const int mlen)
{
  return nullptr;
}

DLV::drawable_obj *
DLV::drawable_obj::create_real_slice(const render_parent *parent,
				     const float origin[3],
				     const float astep[3],
				     const float bstep[3],
				     float *data[], const int nx, const int ny,
				     const string labels[], const int ndata,
				     const bool use_ignore, const float ignore,
				     char message[], const int mlen)
{
  return nullptr;
}

DLV::drawable_obj *
DLV::drawable_obj::create_dos_data(const render_parent *parent,
				   float grid[], const int npoints,
				   float *data[], const string labels[],
				   const int ngraphs, const bool spin,
				   const float xpoints[],
				   const string xlabels[], const int nx,
				   const float yrange[],
				   char message[], const int mlen)
{
  return nullptr;
}

DLV::drawable_obj *
DLV::drawable_obj::create_multi_grid_data(const render_parent *parent,
					  float *multi_grids[], 
					  const int ngrids, int *select_grid,
					  int *grid_sizes,
					  const int npoints,
					  float *data[], const string labels[],
					  const int ngraphs, const bool spin,
					  const float xpoints[],
					  const string xlabels[], const int nx,
					  const float yrange[],
					  char message[], const int mlen)
{
  return nullptr;
}

DLV::drawable_obj *
DLV::drawable_obj::create_panel_data(const render_parent *parent,
				     float grid[], const int npoints,
				     float *data[], const string labels[],
				     const int ngraphs, const bool spin,
				     const float xpoints[],
				     const string xlabels[], const int nx,
				     const float ypoints[],
				     const string ylabels[], const int ny,
				     const float xrange[],
				     const float yrange[],
				     char message[], const int mlen)
{
  return nullptr;
}

DLV::drawable_obj *
DLV::drawable_obj::create_bdos_data(const render_parent *parent,
				    float grid[], const int npoints,
				    float *data[], const string labels[],
				    const int ngraphs, const bool spin,
				    const float xpoints[],
				    const string xlabels[], const int nx,
				    const float ypoints[],
				    const string ylabels[], const int ny,
				    const float xrange[], const float yrange[],
				    float dgrid[], const int dnpoints,
				    float *ddata[], const int dngraphs,
				    char message[], const int mlen)
{
  return nullptr;
}

DLV::drawable_obj *
DLV::drawable_obj::create_real_wavefn(const render_parent *parent,
				      const float origin[3],
				      const float astep[3],
				      const float bstep[3],
				      const float cstep[3],
				      float *data[], float *phases[],
				      const int nx, const int ny,
				      const int nz, const string labels[],
				      const int ndata, const bool use_ignore,
				      const float ignore,
				      char message[], const int mlen)
{
  return nullptr;
}

DLV::drawable_obj *
DLV::drawable_obj::create_bloch_wavefn(const render_parent *parent,
				       const float origin[3],
				       const float astep[3],
				       const float bstep[3],
				       const float cstep[3],
				       float *data[], float *phases[],
				       const int nx, const int ny,
				       const int nz, const string labels[],
				       const int ndata,
				       const int knum[], const int kden[],
				       const bool use_ignore,
				       const float ignore,
				       char message[], const int mlen)
{
  return nullptr;
}

DLV::drawable_obj *
DLV::drawable_obj::create_box_data(const render_parent *parent,
				   const bool kspace, char message[],
				   const int mlen)
{
  return nullptr;
}

DLV::drawable_obj *
DLV::drawable_obj::create_sphere_data(const render_parent *parent,
				      const bool kspace, char message[],
				      const int mlen)
{
  return nullptr;
}

DLV::drawable_obj *
DLV::drawable_obj::create_plane_data(const render_parent *parent,
				     const bool kspace, char message[],
				     const int mlen)
{
  return nullptr;
}

DLV::drawable_obj *
DLV::drawable_obj::create_line_data(const render_parent *parent,
				    const bool kspace, char message[],
				    const int mlen)
{
  return nullptr;
}

DLV::drawable_obj *
DLV::drawable_obj::create_point_data(const render_parent *parent,
				     const bool kspace, char message[],
				     const int mlen)
{
  return nullptr;
}

DLV::drawable_obj *
DLV::drawable_obj::create_wulff_data(const render_parent *parent,
				     float (*vertices)[3], const int nverts,
				     float (*colours)[3], float (*normals)[3],
				     long connects[], const long nconnects,
				     float (*pos)[3], string labels[],
				     int nodes[], const int nnodes,
				     float (*lverts)[3], int nlverts,
				     long lines[], const long nlines,
				     char message[], const int mlen)
{
  return nullptr;
}

DLV::drawable_obj *
DLV::drawable_obj::create_leed_data(const render_parent *parent,
				    float (*points)[2], const int npoints,
				    float (*domains)[4], int ndomains,
				    int *colours, char message[],
				    const int mlen)
{
  return nullptr;
}

void DLV::drawable_obj::reload_data(const render_parent *parent, float grid[],
				    const int npoints, float *data[],
				    const string labels[], const int ngraphs,
				    const bool spin, const float xpoints[],
				    const string xlabels[], const int nx,
				    const float yrange[], char message[],
				    const int mlen)
{
  // dummy
}

DLV::drawable_obj *DLV::drawable_obj::duplicate(const toolkit_obj &id) const
{
  // dummy
  return nullptr;
}

void DLV::drawable_obj::update_atoms_and_bonds(const int natoms,
					       const double x, const double y,
					       const double z,
					       const double length,
					       const double x2,
					       const double y2,
					       const double z2,
					       const double angle,
					       const char sym1[],
					       const char sym2[],
					       const char sym3[],
					       const float r1, const float r2,
					       const float r3, const char [])
{
}

void DLV::drawable_obj::update_formula(const string f)
{
}

void DLV::drawable_obj::update_selected_atom(const int index)
{
}

void DLV::drawable_obj::update_text_value(const string value)
{
}

void DLV::drawable_obj::update_text_field(float coords[][3], int **array,
					  const string labels[],
					  const int ndata, const int natoms,
					  char message[], const int mlen)
{
}

void DLV::drawable_obj::update_text_field(float coords[][3], float **array,
					  const string labels[],
					  const int ndata, const int natoms,
					  char message[], const int mlen)
{
}

void DLV::drawable_obj::update_vector_field(float coords[][3],
					    float array[][3], const int natoms,
					    const int cmpt, char message[],
					    const int mlen)
{
}

void DLV::drawable_obj::update_box(const float o[3], const float a[3],
				   const float b[3], const float c[3])
{
}

void DLV::drawable_obj::update_sphere(const float o[3], const float radius)
{
}

void DLV::drawable_obj::update_plane(const float o[3], const float a[3],
				     const float b[3])
{
}

void DLV::drawable_obj::update_line(const float a[3], const float b[3])
{
}

void DLV::drawable_obj::update_point(const float point[3])
{
}

void DLV::drawable_obj::update_leed(float (*points)[2], const int npoints,
				    float (*domains)[4], const int ndomains,
				    int *colours)
{
}

void DLV::drawable_obj::reload_data(const render_parent *parent, float *multi_grids[],
				    const int ngrids, int *grid_sizes,
				    const int npoints, float *data[],
				    int *select_grid,
				    const string labels[], const int ngraphs,
				    const bool spin, const float xpoints[],
				    const string xlabels[], const int nx,
				    const float yrange[], char message[],
				    const int mlen)
{
  // dummy
}

void DLV::drawable_obj::reload_data(const render_parent *parent, float grid[],
				    const int npoints, float *data[],
				    const string labels[], const int ngraphs,
				    const bool spin, const float xpoints[],
				    const string xlabels[], const int nx,
				    const float ypoints[],
				    const string ylabels[], const int ny,
				    const float xrange[], const float yrange[],
				    char message[], const int mlen)
{
  // Dummy
}

void DLV::drawable_obj::reload_data(const render_parent *parent, float grid[],
				    const int npoints, float *data[],
				    const string labels[], const int ngraphs,
				    const bool spin, const float xpoints[],
				    const string xlabels[], const int nx,
				    const float ypoints[],
				    const string ylabels[], const int ny,
				    const float xrange[], const float yrange[],
				    float dgrid[], const int dnpoints,
				    float *ddata[], const int dngraphs,
				    char message[], const int mlen)
{
  // dummy
}

void DLV::drawable_obj::reload_data(const render_parent *parent,
				    const float origin[3],
				    const float astep[3], const float bstep[3],
				    float *data[], const int nx, const int ny,
				    const string labels[], const int ndata,
				    const bool use_ignore, const float ignore,
				    char message[], const int mlen)
{
  // dummy
}

void DLV::drawable_obj::reload_data(const render_parent *parent,
				    const float origin[3],
				    const float astep[3], const float bstep[3],
				    const float cstep[3], float *data[],
				    const int nx, const int ny,
				    const int nz, const int dims[],
				    const string labels[], const int ndata,
				    const bool kspace, const bool use_ignore,
				    const float ignore, char message[],
				    const int mlen)
{
  // dummy
}

void DLV::drawable_obj::reload_wavefn(const render_parent *parent,
				      const float origin[3],
				      const float astep[3],
				      const float bstep[3],
				      const float cstep[3], float *data[],
				      float *phases[], const int nx,
				      const int ny, const int nz,
				      const string labels[], const int ndata,
				      const bool use_ignore, const float ignore,
				      char message[], const int mlen)
{
}

void DLV::drawable_obj::reload_bloch(const render_parent *parent,
				     const float origin[3],
				     const float astep[3], const float bstep[3],
				     const float cstep[3], float *data[],
				     float *phases[], const int nx,
				     const int ny, const int nz,
				     const string labels[], const int ndata,
				     const int knum[], const int kden[],
				     const bool use_ignore, const float ignore,
				     char message[], const int mlen)
{
  // dummy
}

void DLV::drawable_obj::expand_text_list(const int n)
{
}

void DLV::drawable_obj::add_text_line(const char line[], const int index,
				      const bool append)
{
}

bool DLV::drawable_obj::volume(const float origin[3], const float astep[3],
			       const float bstep[3], const float cstep[3],
			       float *datavals[], const int nx, const int ny,
			       const int nz, const int vdims[],
			       const string labels[], const int ndata,
			       const bool use_ignore, const float ignore,
			       char message[], const int mlen)
{
  return false;
}

bool DLV::drawable_obj::volume(const float origin[3], const float astep[3],
			       const float bstep[3], const float cstep[3],
			       float *mag[], float *phase[], const int nx,
			       const int ny, const int nz,
			       const string labels[], const int ndata,
			       const bool use_ignore, const float ignore,
			       char message[], const int mlen)
{
  return false;
}

bool DLV::drawable_obj::volume(const float origin[3], const float astep[3],
			       const float bstep[3], const float cstep[3],
			       float *mag[], float *phase[], const int nx,
			       const int ny, const int nz,
			       const string labels[], const int ndata,
			       const int knum[], const int kden[],
			       const bool use_ignore, const float ignore,
			       char message[], const int mlen)
{
  return false;
}

bool DLV::drawable_obj::slice(const float origin[3], const float astep[3],
			      const float bstep[3], float *datavals[],
			      const int nx, const int ny,
			      const string labels[], const int ndata,
			      const bool use_ignore, const float ignore,
			      char message[], const int mlen)
{
  return false;
}

bool DLV::drawable_obj::plot(float grid[], const int npoints,
			     float *plots[], const string labels[],
			     const int ngraphs, const bool spin,
			     const float xpoints[], const string xlabels[],
			     const int nx, const float yrange[],
			     char message[], const int mlen)
{
  return false;
}

bool DLV::drawable_obj::plot(float *multi_grids[], const int ngrids,
			     int *select_grid, int *grid_sizes,
			     const int npoints, float *plots[],
			     const string labels[], const int ngraphs,
			     const bool spin, const float xpoints[],
			     const string xlabels[], const int nx,
			     const float yrange[], char message[],
			     const int mlen)
{
  return false;
}

bool DLV::drawable_obj::plot(float grid[], const int npoints,
			     float *plots[], const string labels[],
			     const int ngraphs, const bool spin,
			     const float xpoints[], const string xlabels[],
			     const int nx, const float ypoints[],
			     const string ylabels[], const int ny,
			     const float xrange[], const float yrange[],
			     char message[], const int mlen)
{
  return false;
}

#ifdef DLV_USES_SERIALIZE

template <class Archive>
void DLV::drawable_obj::serialize(Archive &ar, const unsigned int version)
{
}

//BOOST_CLASS_EXPORT_IMPLEMENTATION(DLV::drawable_obj)

#endif // DLV_USES_SERIALIZE
