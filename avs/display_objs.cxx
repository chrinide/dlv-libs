
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#include "../graphics/toolkit_obj.hxx"
#include "../graphics/render_base.hxx"
#include "../graphics/drawable.hxx"
#include "../graphics/display_objs.hxx"
#include "avs/src/express/points.hxx"
#include "avs/src/express/lines.hxx"
#include "avs/src/express/plots.hxx"
#include "avs/src/express/planes.hxx"
#include "avs/src/express/volumes.hxx"
#include "display_impl.hxx"

namespace DLV {

  OMobj_id find_property(const render_parent *parent, const char name[],
			 const bool kspace);
  void get_graph_font(char name[], const int n);

}

DLV::display_obj::~display_obj()
{
}

OMobj_id DLV::find_property(const render_parent *parent, const char name[],
			    const bool kspace)
{
  OMobj_id space = OMnull_obj;
  if (kspace)
    space = parent->get_kspace().get_id();
  else
    space = parent->get_rspace().get_id();
  OMobj_id id = OMfind_str_subobj(space, name, OM_OBJ_RW);
  if (OMis_null_obj(id)) {
    CCP3_Renderers_Base_props *obj
      = new CCP3_Renderers_Base_props(space, name);
    // Attach to list of display objects
    OMobj_id out = OMfind_str_subobj(space, "obj", OM_OBJ_RW);
    OMadd_obj_ref(out, obj->GroupObject.obj.obj_id(), 0);
    // set name
    //Todo OMset_str_val(obj->GroupObject.Top.name.obj_id(), name);
    id = obj->obj_id();
    // foreground colours?
    float c[3];
    parent->get_inverted_background(c);
    float *ptr =
      (float *) obj->GroupObject.Props.col.ret_array_ptr(OM_GET_ARRAY_WR);
    ptr[0] = c[0];
    ptr[1] = c[0];
    ptr[2] = c[2];
    obj->GroupObject.Props.col.free_array(ptr);
  }
  return id;
}

void DLV::get_graph_font(char name[], const int n)
{
  static OMobj_id obj = OMnull_obj;
  if (OMis_null_obj(obj))
    obj = OMfind_str_subobj(OMinst_obj, "DLV.DLVcore.preferences.AGFont",
			    OM_OBJ_RD);
  OMget_str_val(obj, &name, n);
} 

void DLV::display_obj::transform_point(float p[3], const float mat[4][4],
				       const float xlate[3],
				       const float centre[3])
{
  // Best guess based on AVS V experiments.
  float v[3];
  v[0] = (((mat[0][0] * (p[0] - centre[0]) + mat[1][0] * (p[1] - centre[1])
	    + mat[2][0] * (p[2] - centre[2])) * mat[3][3]) + xlate[0]);
  v[1] = (((mat[0][1] * (p[0] - centre[0]) + mat[1][1] * (p[1] - centre[1])
	    + mat[2][1] * (p[2] - centre[2])) * mat[3][3]) + xlate[1]);
  v[2] = (((mat[0][2] * (p[0] - centre[0]) + mat[1][2] * (p[1] - centre[1])
	    + mat[2][2] * (p[2] - centre[2])) * mat[3][3]) + xlate[2]);
  p[0] = v[0];
  p[1] = v[1];
  p[2] = v[2];
}

void DLV::display_obj::display_3D()
{
// dummy function
}

void DLV::display_atom_text_data::display_3D()
{
  obj->params.in_viewer = 1;
}

void DLV::display_obj::undisplay_3D()
{
  // dummy function
}
void DLV::display_atom_text_data::undisplay_3D()
{
  obj->params.in_viewer = 0;
}

void DLV::display_obj::update_display_list(const render_parent *parent)
{
  parent->add_display_list(name, get_type(), get_index());
}

void DLV::display_obj::update_display_list(const render_parent *parent,
					   const string name)
{
  parent->add_display_list(name, get_type(), get_index());
}

DLV::display_obj *
DLV::display_obj::create_text_display(const render_parent *parent,
				      const class drawable_obj *draw,
				      const string name, const int index,
				      char message[], const int mlen)
{
  OMobj_id id = find_property(parent, "Points", false);
  display_text_data *obj = new display_text_data(id, index, name);
  if (obj == 0)
    strncpy(message, "Unable to create text info", mlen);
  else
    obj->attach_data(draw, name);
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_real_display(const render_parent *parent,
				      const class drawable_obj *draw,
				      const string name, const int index,
				      char message[], const int mlen)
{
  OMobj_id id = find_property(parent, "Points", false);
  display_real_data *obj = new display_real_data(id, index, name);
  if (obj == 0)
    strncpy(message, "Unable to create real scalar info", mlen);
  else
    obj->attach_data(draw, name);
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_file_view(const render_parent *parent,
				   const class drawable_obj *draw,
				   const string name, const int index,
				   char message[], const int mlen)
{
  // Todo - doesn't belong in points?
  OMobj_id id = find_property(parent, "Points", false);
  display_file_view *obj = new display_file_view(id, index, name);
  if (obj == 0)
    strncpy(message, "Unable to create text file info", mlen);
  else
    obj->attach_data(draw, name);
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_text_view(const render_parent *parent,
				   const class drawable_obj *draw,
				   const string name, const int index,
				   char message[], const int mlen)
{
  // Todo - doesn't belong in points?
  OMobj_id id = find_property(parent, "Points", false);
  display_text_view *obj = new display_text_view(id, index, name);
  if (obj == 0)
    strncpy(message, "Unable to create text info", mlen);
  else
    obj->attach_data(draw, name);
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_bond_text(const render_parent *parent,
				   const drawable_obj *draw,
				   const string name, const int index,
				   char message[], const int mlen)
{
  OMobj_id id = find_property(parent, "Points", false);
  display_atom_bond_data *obj = new display_atom_bond_data(id, index, name);
  if (obj == 0)
    strncpy(message, "Unable to create atom and bond info", mlen);
  else
    obj->attach_data(draw, name, id);
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_atom_text(const render_parent *parent,
				   const class drawable_obj *draw,
				   const int component, const string name,
				   const int index, char message[],
				   const int mlen)
{
  OMobj_id id = find_property(parent, "Points", false);
  display_atom_text_data *obj = new display_atom_text_data(id, index, name);
  if (obj == 0)
    strncpy(message, "Unable to create atom text info", mlen);
  else
    obj->attach_data(draw, component, name, id);
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_atom_vectors(const render_parent *parent,
				      const class drawable_obj *draw,
				      const int component, const string name,
				      const int index, char message[],
				      const int mlen)
{
  OMobj_id id = find_property(parent, "Points", false);
  display_atom_vector_data *obj = new display_atom_vector_data(id, index,
							       name);
  if (obj == 0)
    strncpy(message, "Unable to create atom vector info", mlen);
  else
    obj->attach_data(draw, component, name, id);
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_phonon_anim(const render_parent *parent,
				     const class drawable_obj *draw,
				     const string name, const int index,
				     char message[], const int mlen)
{
  OMobj_id id = find_property(parent, "Points", false);
  display_atom_trajectory *obj = new display_atom_trajectory(id, index, name);
  if (obj == 0)
    strncpy(message, "Unable to create atom trajectory", mlen);
  //else
  //obj->attach_data(draw, component, name, id);
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_isosurface(const render_parent *parent,
				    const drawable_obj *draw,
				    const bool is_periodic, const bool kspace,
				    const int component, const string name,
				    const int index, char message[],
				    const int mlen)
{
  OMobj_id id = find_property(parent, "Volumes", kspace);
  isosurface *obj = 0;
  if (is_periodic and !kspace)
    obj = new periodic_isosurface(id, index, kspace, name);
  else
    obj = new isosurface(id, index, kspace, name);
  if (obj == 0)
    strncpy(message, "Unable to create isosurface", mlen);
  else {
    obj->attach_data(draw, component, name, id);
    obj->attach_transforms(parent);
  }
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_iso_wavefn(const render_parent *parent,
				    const drawable_obj *draw,
				    const bool is_periodic, const bool kspace,
				    const int component, const int ndata,
				    const string name, const int index,
				    const int sa, const int sb, const int sc,
				    char message[], const int mlen)
{
  OMobj_id id = find_property(parent, "Volumes", kspace);
  iso_wavefn *obj = 0;
  if (!kspace) {
    if (is_periodic)
      obj = new iso_bloch(id, index, sa, sb, sc, kspace, name);
    else
      obj = new iso_wavefn(id, index, kspace, name);
  } // Todo?
  if (obj == 0)
    strncpy(message, "Unable to create isosurface", mlen);
  else {
    obj->attach_data(draw, component, ndata, name, id);
    obj->attach_transforms(parent);
  }
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_cells(const render_parent *parent,
			       const drawable_obj *draw,
			       const bool is_periodic, const bool kspace,
			       const int component, const string name,
			       const int index, char message[],
			       const int mlen)
{
  OMobj_id id = find_property(parent, "Volumes", kspace);
  cells_obj *obj = 0;
  if (is_periodic and !kspace)
    obj = new periodic_cells(id, index, kspace, name);
  else
    obj = new cells_obj(id, index, kspace, name);
  if (obj == 0)
    strncpy(message, "Unable to create cells", mlen);
  else {
    obj->attach_data(draw, component, name, id);
    obj->attach_transforms(parent);
  }
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_spheres(const render_parent *parent,
				 const drawable_obj *draw,
				 const bool is_periodic, const bool kspace,
				 const int component, const string name,
				 const int index, char message[],
				 const int mlen)
{
  OMobj_id id = find_property(parent, "Volumes", kspace);
  spheres *obj = 0;
  if (is_periodic and !kspace)
    obj = new periodic_spheres(id, index, kspace, name);
  else
    obj = new spheres(id, index, kspace, name);
  if (obj == 0)
    strncpy(message, "Unable to create spheres", mlen);
  else {
    obj->attach_data(draw, component, name, id);
    obj->attach_transforms(parent);
  }
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_volume(const render_parent *parent,
				 const drawable_obj *draw,
				 const bool is_periodic, const bool kspace,
				 const int component, const string name,
				 const int index, char message[],
				 const int mlen)
{
  OMobj_id id = find_property(parent, "Volumes", kspace);
  volume_view *obj = 0;
  //if (is_periodic and !kspace)
  //  obj = new periodic_spheres(id, index, kspace, name);
  //else
  obj = new volume_view(id, index, kspace, name);
  if (obj == 0)
    strncpy(message, "Unable to create volume render", mlen);
  else {
    obj->attach_data(draw, component, name, id);
    obj->attach_transforms(parent);
  }
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_hedgehog(const render_parent *parent,
				  const drawable_obj *draw,
				  const bool is_periodic, const bool kspace,
				  const int component, const string name,
				  const int index, char message[],
				  const int mlen)
{
  OMobj_id id = find_property(parent, "Volumes", kspace);
  hedgehog *obj = 0;
  if (is_periodic and !kspace)
    obj = new periodic_hedgehog(id, index, kspace, name);
  else
    obj = new hedgehog(id, index, kspace, name);
  if (obj == 0)
    strncpy(message, "Unable to create vectors", mlen);
  else {
    obj->attach_data(draw, component, name, id);
    obj->attach_transforms(parent);
  }
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_streamlines(const render_parent *parent,
				     const drawable_obj *draw,
				     const bool is_periodic, const bool kspace,
				     const int component, const string name,
				     const int index, char message[],
				     const int mlen)
{
  OMobj_id id = find_property(parent, "Volumes", kspace);
  streamlines *obj = 0;
  if (is_periodic and !kspace)
    obj = new periodic_streamlines(id, index, kspace, name);
  else
    obj = new streamlines(id, index, kspace, name);
  if (obj == 0)
    strncpy(message, "Unable to create isosurface", mlen);
  else {
    obj->attach_data(draw, component, name, id);
    obj->attach_transforms(parent);
  }
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_ag_contour(const render_parent *parent,
				    const drawable_obj *draw,
				    const bool is_periodic, const bool kspace,
				    const int component, const string name,
				    const int index, char message[],
				    const int mlen)
{
  OMobj_id id = find_property(parent, "Planes", kspace);
  ag_contours *obj = 0;
  if (is_periodic and !kspace)
    obj = new periodic_ag_contours(id, index, kspace, name);
  else
    obj = new ag_contours(id, index, kspace, name);
  if (obj == 0)
    strncpy(message, "Unable to create AGisolines", mlen);
  else {
    obj->set_background(parent);
    obj->set_font();
    obj->attach_data(draw, component, name, id, parent->get_rspace().get_id(),
		     parent->get_name());
    obj->attach_transforms(parent);
  }
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_contour(const render_parent *parent,
				 const drawable_obj *draw,
				 const bool is_periodic, const bool kspace,
				 const int component, const string name,
				 const int index, char message[],
				 const int mlen)
{
  OMobj_id id = find_property(parent, "Planes", kspace);
  contours *obj = 0;
  if (is_periodic and !kspace)
    obj = new periodic_contours(id, index, kspace, name);
  else
    obj = new contours(id, index, kspace, name);
  if (obj == 0)
    strncpy(message, "Unable to create isolines", mlen);
  else {
    obj->set_background(parent);
    obj->attach_data(draw, component, name, id, parent->get_rspace().get_id(),
		     parent->get_name());
    obj->attach_transforms(parent);
  }
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_pn_contour(const render_parent *parent,
				    const drawable_obj *draw,
				    const bool is_periodic, const bool kspace,
				    const int component, const string name,
				    const int index, char message[],
				    const int mlen)
{
  OMobj_id id = find_property(parent, "Planes", kspace);
  pn_contours *obj = 0;
  if (is_periodic and !kspace)
    obj = new periodic_pn_contours(id, index, kspace, name);
  else
    obj = new pn_contours(id, index, kspace, name);
  if (obj == 0)
    strncpy(message, "Unable to create P/N isolines", mlen);
  else {
    obj->set_background(parent);
    obj->attach_data(draw, component, name, id, parent->get_rspace().get_id(),
		     parent->get_name());
    obj->attach_transforms(parent);
  }
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_solid_ct(const render_parent *parent,
				  const drawable_obj *draw,
				  const bool is_periodic, const bool kspace,
				  const int component, const string name,
				  const int index, char message[],
				  const int mlen)
{
  OMobj_id id = find_property(parent, "Planes", kspace);
  solid_contours *obj = 0;
  if (is_periodic and !kspace)
    obj = new periodic_solid_contours(id, index, kspace, name);
  else
    obj = new solid_contours(id, index, kspace, name);
  if (obj == 0)
    strncpy(message, "Unable to create Solid contour", mlen);
  else {
    obj->set_background(parent);
    obj->attach_data(draw, component, name, id, parent->get_rspace().get_id(),
		     parent->get_name());
    obj->attach_transforms(parent);
  }
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_shaded_ct(const render_parent *parent,
				   const drawable_obj *draw,
				   const bool is_periodic, const bool kspace,
				   const int component, const string name,
				   const int index, char message[],
				   const int mlen)
{
  OMobj_id id = find_property(parent, "Planes", kspace);
  shaded_contours *obj = 0;
  if (is_periodic and !kspace)
    obj = new periodic_shaded_contours(id, index, kspace, name);
  else
    obj = new shaded_contours(id, index, kspace, name);
  if (obj == 0)
    strncpy(message, "Unable to create Shaded contour", mlen);
  else {
    obj->set_background(parent);
    obj->attach_data(draw, component, name, id, parent->get_rspace().get_id(),
		     parent->get_name());
    obj->attach_transforms(parent);
  }
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_surface_plot(const render_parent *parent,
				      const drawable_obj *draw,
				      const bool is_periodic,
				      const bool kspace,
				      const int component, const string name,
				      const int index, char message[],
				      const int mlen)
{
  OMobj_id id = find_property(parent, "Planes", kspace);
  surface_plot *obj = 0;
  if (is_periodic and !kspace)
    obj = new periodic_surface_plot(id, index, kspace, name);
  else
    obj = new surface_plot(id, index, kspace, name);
  if (obj == 0)
    strncpy(message, "Unable to create Surface Plot", mlen);
  else {
    obj->attach_data(draw, component, name, id, parent->get_rspace().get_id());
    obj->attach_transforms(parent);
  }
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_dos_plot(const render_parent *parent,
				  const class drawable_obj *draw,
				  const string name, const int index,
				  const string xaxis, const string yaxis,
				  const int ngraphs, const int type,
				  char message[], const int mlen)
{
  OMobj_id id = find_property(parent, "Plots", false);
  plot_obj *obj;
  if(type==0)
    obj = new display_dos(id, index, name);
  else if(type==1)
    obj = new display_rod1d(id, index, name);
  else if(type==2)
    obj = new display_rod2d(id, index, name);
  else
    obj = 0;
  if (obj == 0)
    strncpy(message, "Unable to create DOS plot", mlen);
  else {
    bool visible = type!=2;
    obj->set_background(parent);
    obj->set_font();
    obj->attach_data(draw, name, xaxis, yaxis, ngraphs, id,
		     parent->get_name(), visible);
  }
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_panel_plot(const render_parent *parent,
				    const class drawable_obj *draw,
				    const string name, const int index,
				    const string xaxis, const string yaxis,
				    const int ngraphs, char message[],
				    const int mlen, const bool split_spins)
{
  OMobj_id id = find_property(parent, "Plots", false);
  plot_obj *obj = 0;
  if (split_spins)
    obj = new display_bspin(id, index, name);
  else
    obj = new display_band(id, index, name);
  if (obj == 0)
    strncpy(message, "Unable to create Band plot", mlen);
  else {
    obj->set_background(parent);
    obj->set_font();
    obj->attach_data(draw, name, xaxis, yaxis, ngraphs, id,
		     parent->get_name());
  }
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_3D_region(const render_parent *parent,
				   const drawable_obj *draw,
				   const string name, const int index,
				   const bool kspace, char message[],
				   const int mlen)
{
  OMobj_id id = find_property(parent, "Volumes", kspace);
  display_3D_region *obj = new display_3D_region(id, index, kspace, name);
  if (obj == 0)
    strncpy(message, "Unable to create 3D region", mlen);
  else
    obj->attach_data(draw, name, id);
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_sphere_obj(const render_parent *parent,
				    const drawable_obj *draw,
				    const string name, const int index,
				    const bool kspace, char message[],
				    const int mlen)
{
  OMobj_id id = find_property(parent, "Volumes", kspace);
  display_sphere_obj *obj = new display_sphere_obj(id, index, kspace, name);
  if (obj == 0)
    strncpy(message, "Unable to create sphere", mlen);
  else
    obj->attach_data(draw, name, id);
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_plane_obj(const render_parent *parent,
				   const drawable_obj *draw,
				   const string name, const int index,
				   const bool kspace, char message[],
				   const int mlen)
{
  OMobj_id id = find_property(parent, "Planes", kspace);
  display_plane *obj = new display_plane(id, index, kspace, name);
  if (obj == 0)
    strncpy(message, "Unable to create plane", mlen);
  else
    obj->attach_data(draw, name, id);
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_line_obj(const render_parent *parent,
				  const drawable_obj *draw,
				  const string name, const int index,
				  const bool kspace, char message[],
				  const int mlen)
{
  OMobj_id id = find_property(parent, "Lines", kspace);
  display_line *obj = new display_line(id, index, kspace, name);
  if (obj == 0)
    strncpy(message, "Unable to create line", mlen);
  else
    obj->attach_data(draw, name, id);
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_point_obj(const render_parent *parent,
				   const drawable_obj *draw,
				   const string name, const int index,
				   const bool kspace, char message[],
				   const int mlen)
{
  OMobj_id id = find_property(parent, "Points", kspace);
  display_point *obj = new display_point(id, index, kspace, name);
  if (obj == 0)
    strncpy(message, "Unable to create point", mlen);
  else
    obj->attach_data(draw, name, id);
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_wulff_region(const render_parent *parent,
				      const drawable_obj *draw,
				      const string name, const int index,
				      char message[], const int mlen)
{
  OMobj_id id = find_property(parent, "Volumes", false);
  display_wulff_region *obj = new display_wulff_region(id, index, name);
  if (obj == 0)
    strncpy(message, "Unable to create wulff region", mlen);
  else {
    obj->set_colours(parent);
    obj->attach_data(draw, name, id);
  }
  return obj;
}

DLV::display_obj *
DLV::display_obj::create_leed_pattern(const render_parent *parent,
				      const drawable_obj *draw,
				      const string name, const int index,
				      char message[], const int mlen)
{
  OMobj_id id = find_property(parent, "Plots", false);
  display_leed_pattern *obj = new display_leed_pattern(id, index, name);
  if (obj == 0)
    strncpy(message, "Unable to create leed pattern", mlen);
  else {
    //obj->set_background(parent); Its set to white - Todo?
    obj->attach_data(draw, name, id, parent->get_name());
  }
  return obj;
}

void DLV::display_obj::update_streamlines(const class drawable_obj *draw,
					  char message[], const int mlen)
{
  // Dummy
}

DLV::iso_wavefn::iso_wavefn(CCP3_Renderers_Volumes_iso_wavefn_base *d,
			    const int i, const int sa, const int sb,
			    const int sc, const bool sp, const string n)
  : display_obj(i, n, sp), obj(d)
{
  obj->sa = sa;
  obj->sb = sb;
  obj->sc = sc;
}

DLV::display_text_data::~display_text_data()
{
  delete obj;
}

DLV::display_real_data::~display_real_data()
{
  delete obj;
}

DLV::display_file_view::~display_file_view()
{
  delete obj;
}

DLV::display_text_view::~display_text_view()
{
  delete obj;
}

DLV::display_atom_bond_data::~display_atom_bond_data()
{
  delete obj;
}

DLV::display_atom_text_data::~display_atom_text_data()
{
  delete obj;
}

DLV::display_atom_vector_data::~display_atom_vector_data()
{
  delete obj;
}

DLV::display_atom_trajectory::~display_atom_trajectory()
{
  delete obj;
}

DLV::isosurface::~isosurface()
{
  delete obj;
}

DLV::iso_wavefn::~iso_wavefn()
{
  delete obj;
}

DLV::cells_obj::~cells_obj()
{
  delete obj;
}

DLV::spheres::~spheres()
{
  delete obj;
}

DLV::volume_view::~volume_view()
{
  delete obj;
}

DLV::hedgehog::~hedgehog()
{
  delete obj;
}

DLV::streamlines::~streamlines()
{
  delete obj;
}

DLV::ag_contours::~ag_contours()
{
  delete obj;
}

DLV::contours::~contours()
{
  delete obj;
}

DLV::pn_contours::~pn_contours()
{
  delete obj;
}

DLV::solid_contours::~solid_contours()
{
  delete obj;
}

DLV::shaded_contours::~shaded_contours()
{
  delete obj;
}

DLV::surface_plot::~surface_plot()
{
  delete obj;
}

DLV::plot_obj::~plot_obj()
{
  delete obj;
}

DLV::display_3D_region::~display_3D_region()
{
  delete obj;
}

DLV::display_sphere_obj::~display_sphere_obj()
{
  delete obj;
}

DLV::display_plane::~display_plane()
{
  delete obj;
}

DLV::display_line::~display_line()
{
  delete obj;
}

DLV::display_point::~display_point()
{
  delete obj;
}

DLV::display_wulff_region::~display_wulff_region()
{
  delete obj;
}

DLV::display_leed_pattern::~display_leed_pattern()
{
  delete obj;
}

int DLV::display_text_data::get_type() const
{
  return (int)DISP_text;
}

int DLV::display_real_data::get_type() const
{
  return (int)DISP_real;
}

int DLV::display_file_view::get_type() const
{
  return (int)DISP_file;
}

int DLV::display_text_view::get_type() const
{
  return (int)DISP_file;
}

int DLV::display_atom_bond_data::get_type() const
{
  return (int)DISP_bond_text;
}

int DLV::display_atom_text_data::get_type() const
{
  return (int)DISP_bond_text;
}

int DLV::display_atom_vector_data::get_type() const
{
  return (int)DISP_atom_vectors;
}

int DLV::display_atom_trajectory::get_type() const
{
  return (int)DISP_traj;
}

int DLV::isosurface::get_type() const
{
  return (int)DISP_isosurface;
}

int DLV::iso_wavefn::get_type() const
{
  return (int)DISP_iso_wavefn;
}

int DLV::cells_obj::get_type() const
{
  return (int)DISP_cells;
}

int DLV::spheres::get_type() const
{
  return (int)DISP_spheres;
}

int DLV::volume_view::get_type() const
{
  return (int)DISP_volume;
}

int DLV::hedgehog::get_type() const
{
  return (int)DISP_hedgehog;
}

int DLV::streamlines::get_type() const
{
  return (int)DISP_streamlines;
}

int DLV::ag_contours::get_type() const
{
  return (int)DISP_AGisolines;
}

int DLV::contours::get_type() const
{
  return (int)DISP_isolines;
}

int DLV::pn_contours::get_type() const
{
  return (int)DISP_PNisolines;
}

int DLV::solid_contours::get_type() const
{
  return (int)DISP_solid;
}

int DLV::shaded_contours::get_type() const
{
  return (int)DISP_shaded;
}

int DLV::surface_plot::get_type() const
{
  return (int)DISP_surface;
}

int DLV::plot_obj::get_type() const
{
  return (int)DISP_plot;
}

int DLV::display_3D_region::get_type() const
{
  return (int)DISP_box;
}

int DLV::display_sphere_obj::get_type() const
{
  return (int)DISP_sphere_obj;
}

int DLV::display_plane::get_type() const
{
  return (int)DISP_plane;
}

int DLV::display_line::get_type() const
{
  return (int)DISP_line;
}

int DLV::display_point::get_type() const
{
  return (int)DISP_point;
}

int DLV::display_wulff_region::get_type() const
{
  return (int)DISP_wulff;
}

int DLV::display_leed_pattern::get_type() const
{
  return (int)DISP_leed;
}

void DLV::display_wulff_region::set_colours(const render_parent *parent)
{
  float c[3];
  parent->get_inverted_background(c);
  obj->params.red = c[0];
  obj->params.green = c[1];
  obj->params.blue = c[2];
}

void DLV::plot_obj::set_font()
{
  char name[128];
  get_graph_font(name, 128);
  obj->Font = name;
}

void DLV::ag_contours::set_font()
{
  char name[128];
  get_graph_font(name, 128);
  obj->contour2D.Font = name;
}

void DLV::plot_obj::set_background(const render_parent *parent)
{
  float c[3];
  parent->get_background(c);
  float *ptr = (float *)
    obj->DLV2Dscene.View.View.back_col.ret_array_ptr(OM_GET_ARRAY_WR);
  ptr[0] = c[0];
  ptr[1] = c[0];
  ptr[2] = c[2];
  obj->DLV2Dscene.View.View.back_col.free_array(ptr);
}

void DLV::display_leed_pattern::set_background(const render_parent *parent)
{
  float c[3];
  parent->get_background(c);
  float *ptr = (float *)
    obj->DLV2Dscene.View.View.back_col.ret_array_ptr(OM_GET_ARRAY_WR);
  ptr[0] = c[0];
  ptr[1] = c[0];
  ptr[2] = c[2];
  obj->DLV2Dscene.View.View.back_col.free_array(ptr);
}

void DLV::ag_contours::set_background(const render_parent *parent)
{
  float c[3];
  parent->get_background(c);
  float *ptr = (float *)
    obj->contour2D.DLV2Dscene.View.View.back_col.ret_array_ptr(OM_GET_ARRAY_WR);
  ptr[0] = c[0];
  ptr[1] = c[0];
  ptr[2] = c[2];
  obj->contour2D.DLV2Dscene.View.View.back_col.free_array(ptr);
}

void DLV::contours::set_background(const render_parent *parent)
{
  float c[3];
  parent->get_background(c);
  float *ptr = (float *)
    obj->contour2D.DLV2Dscene.View.View.back_col.ret_array_ptr(OM_GET_ARRAY_WR);
  ptr[0] = c[0];
  ptr[1] = c[0];
  ptr[2] = c[2];
  obj->contour2D.DLV2Dscene.View.View.back_col.free_array(ptr);
}

void DLV::pn_contours::set_background(const render_parent *parent)
{
  float c[3];
  parent->get_background(c);
  float *ptr = (float *)
    obj->contour2D.DLV2Dscene.View.View.back_col.ret_array_ptr(OM_GET_ARRAY_WR);
  ptr[0] = c[0];
  ptr[1] = c[0];
  ptr[2] = c[2];
  obj->contour2D.DLV2Dscene.View.View.back_col.free_array(ptr);
}

void DLV::solid_contours::set_background(const render_parent *parent)
{
  float c[3];
  parent->get_background(c);
  float *ptr = (float *)
    obj->contour2D.DLV2Dscene.View.View.back_col.ret_array_ptr(OM_GET_ARRAY_WR);
  ptr[0] = c[0];
  ptr[1] = c[0];
  ptr[2] = c[2];
  obj->contour2D.DLV2Dscene.View.View.back_col.free_array(ptr);
}

void DLV::shaded_contours::set_background(const render_parent *parent)
{
  float c[3];
  parent->get_background(c);
  float *ptr = (float *)
    obj->contour2D.DLV2Dscene.View.View.back_col.ret_array_ptr(OM_GET_ARRAY_WR);
  ptr[0] = c[0];
  ptr[1] = c[0];
  ptr[2] = c[2];
  obj->contour2D.DLV2Dscene.View.View.back_col.free_array(ptr);
}

void DLV::display_text_data::attach_data(const drawable_obj *draw,
					 const string name)
{
  // Set name of display object
  OMset_str_val(obj->name.obj_id(), name.c_str());
  // attach display output
  //OMobj_id out = OMfind_str_subobj(parent, "GroupObject.child_objs",
  //			   OM_OBJ_RW);
  //OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
  // attach data
  OMset_obj_ref(obj->data.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::display_real_data::attach_data(const drawable_obj *draw,
					 const string name)
{
  // Set name of display object
  OMset_str_val(obj->name.obj_id(), name.c_str());
  // attach data
  OMset_obj_ref(obj->data.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::display_file_view::attach_data(const drawable_obj *draw,
					 const string name)
{
  // Set name of display object
  OMset_str_val(obj->name.obj_id(), name.c_str());
  // attach display output
  //OMobj_id out = OMfind_str_subobj(parent, "GroupObject.child_objs",
  //			   OM_OBJ_RW);
  //OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
  // attach data
  OMset_obj_ref(obj->data.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::display_text_view::attach_data(const drawable_obj *draw,
					 const string name)
{
  // Set name of display object
  OMset_str_val(obj->name.obj_id(), name.c_str());
  // attach display output
  //OMobj_id out = OMfind_str_subobj(parent, "GroupObject.child_objs",
  //			   OM_OBJ_RW);
  //OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
  // attach data
  OMset_obj_ref(obj->data.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::display_atom_bond_data::attach_data(const drawable_obj *draw,
					      const string name,
					      const OMobj_id parent)
{
  // Set name of display object
  OMset_str_val(obj->name.obj_id(), name.c_str());
  // attach display output
  OMobj_id out = OMfind_str_subobj(parent, "GroupObject.child_objs",
				   OM_OBJ_RW);
  OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
  // attach data
  OMset_obj_val(obj->data.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::display_atom_text_data::attach_data(const drawable_obj *draw,
					      const int component,
					      const string name,
					      const OMobj_id parent)
{
  obj->params.component = component;
  // Set name of display object
  OMset_str_val(obj->name.obj_id(), name.c_str());
  // attach display output
  OMobj_id out = OMfind_str_subobj(parent, "GroupObject.child_objs",
				   OM_OBJ_RW);
  OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
  // attach data
  OMset_obj_ref(obj->glyph_field.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::display_atom_vector_data::attach_data(const drawable_obj *draw,
						const int component,
						const string name,
						const OMobj_id parent)
{
  obj->component = component;
  // Set name of display object
  OMset_str_val(obj->name.obj_id(), name.c_str());
  // attach display output
  OMobj_id out = OMfind_str_subobj(parent, "GroupObject.child_objs",
				   OM_OBJ_RW);
  OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
  // attach data
  OMset_obj_ref(obj->glyph_field.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::isosurface::attach_data(const drawable_obj *draw,
				  const int component, const string name,
				  const OMobj_id parent)
{
  // Set name of display object
  if (name.length() > 120) {
    char str[128];
    strncpy(str, name.c_str(), 120);
    str[100] = '\0';
    OMset_str_val(obj->name.obj_id(), str);
  } else
    OMset_str_val(obj->name.obj_id(), name.c_str());
  // attach display output
  OMobj_id out = OMfind_str_subobj(parent, "GroupObject.child_objs",
				   OM_OBJ_RW);
  OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
  // attach field
  OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
  // set data values
  obj->IsoParam.iso_component = component;
  obj->IsoParam.colour_data = component;
}

void DLV::iso_wavefn::attach_data(const drawable_obj *draw,
				  const int component, const int ndata,
				  const string name, const OMobj_id parent)
{
  // Set name of display object
  OMset_str_val(obj->name.obj_id(), name.c_str());
  // attach display output
  OMobj_id out = OMfind_str_subobj(parent, "GroupObject.child_objs",
				   OM_OBJ_RW);
  OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
  // attach field
  OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
  // set data values
  obj->IsoParam.iso_component = component;
  obj->IsoParam.colour_data = ndata + component;
}

void DLV::cells_obj::attach_data(const drawable_obj *draw,
				 const int component, const string name,
				 const OMobj_id parent)
{
  // Set name of display object
  OMset_str_val(obj->name.obj_id(), name.c_str());
  // attach display output
  OMobj_id out = OMfind_str_subobj(parent, "GroupObject.child_objs",
				   OM_OBJ_RW);
  OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
  // attach field
  OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
  // set data values
  obj->ContourParam.contour_comp = component;
}

void DLV::spheres::attach_data(const drawable_obj *draw,
			       const int component, const string name,
			       const OMobj_id parent)
{
  // Set name of display object
  OMset_str_val(obj->name.obj_id(), name.c_str());
  // attach display output
  OMobj_id out = OMfind_str_subobj(parent, "GroupObject.child_objs",
				   OM_OBJ_RW);
  OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
  // attach field
  OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
  // set data values
  obj->param.component = component;
}

void DLV::volume_view::attach_data(const drawable_obj *draw,
				     const int component, const string name,
				     const OMobj_id parent)
{
  // Set name of display object
  OMset_str_val(obj->name.obj_id(), name.c_str());
  // attach display output
  OMobj_id out = OMfind_str_subobj(parent, "GroupObject.child_objs",
				   OM_OBJ_RW);
  OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
  // attach field
  OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
  // set data values
  //obj->param.component = component;
}

void DLV::hedgehog::attach_data(const drawable_obj *draw,
				const int component, const string name,
				const OMobj_id parent)
{
  // Todo - ?
  // Set name of display object
  OMset_str_val(obj->name.obj_id(), name.c_str());
  // attach display output
  OMobj_id out = OMfind_str_subobj(parent, "GroupObject.child_objs",
				   OM_OBJ_RW);
  OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
  // attach field
  OMset_obj_ref(obj->glyph_field.obj_id(), draw->get_id().get_id(), 0);
  // set data values
  obj->component = component;
}

void DLV::streamlines::attach_data(const drawable_obj *draw,
				   const int component, const string name,
				   const OMobj_id parent)
{
  // Todo ?
  // Set name of display object
  OMset_str_val(obj->name.obj_id(), name.c_str());
  // attach display output
  OMobj_id out = OMfind_str_subobj(parent, "GroupObject.child_objs",
				   OM_OBJ_RW);
  OMadd_obj_ref(out, obj->obj.obj_id(), 0);
  // attach field
  OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
  // set data values
  obj->StreamParam.component = component;
}

void DLV::ag_contours::attach_data(const drawable_obj *draw,
				   const int component, const string name,
				   const OMobj_id parent,
				   const OMobj_id space, const char *model)
{
  // Set name of display object
  OMset_str_val(obj->name.obj_id(), name.c_str());
  // attach display output
  OMobj_id out = OMfind_str_subobj(parent, "GroupObject.child_objs",
				   OM_OBJ_RW);
  OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
  // attach field
  OMset_obj_ref(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
  // set data values
  obj->IsoParam.contour_comp = component;
  // attach 2D replication
  out = OMfind_str_subobj(space, "common_data", OM_OBJ_RW);
  OMset_obj_ref(obj->context.obj_id(), out, 0);
  // Attach name to 2D view
  OMset_str_val(obj->contour2D.DLV2Dscene.Model_Name.obj_id(), model);
}

void DLV::contours::attach_data(const drawable_obj *draw,
				const int component, const string name,
				const OMobj_id parent,
				const OMobj_id space, const char *model)
{
  // Set name of display object
  OMset_str_val(obj->name.obj_id(), name.c_str());
  // attach display output
  OMobj_id out = OMfind_str_subobj(parent, "GroupObject.child_objs",
				   OM_OBJ_RW);
  OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
  // attach field
  OMset_obj_ref(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
  // set data values
  obj->IsoParam.contour_comp = component;
  // attach 2D replication
  //out = OMfind_str_subobj(space, "common_data", OM_OBJ_RW);
  //OMset_obj_ref(obj->context.obj_id(), out, 0);
  // Attach name to 2D view
  OMset_str_val(obj->contour2D.DLV2Dscene.Model_Name.obj_id(), model);
}

void DLV::pn_contours::attach_data(const drawable_obj *draw,
				   const int component, const string name,
				   const OMobj_id parent,
				   const OMobj_id space, const char *model)
{
  // Set name of display object
  OMset_str_val(obj->name.obj_id(), name.c_str());
  // attach display output
  OMobj_id out = OMfind_str_subobj(parent, "GroupObject.child_objs",
				   OM_OBJ_RW);
  OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
  // attach field
  OMset_obj_ref(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
  // set data values
  obj->IsoParam.contour_comp = component;
  // attach 2D replication
  //out = OMfind_str_subobj(space, "common_data", OM_OBJ_RW);
  //OMset_obj_ref(obj->context.obj_id(), out, 0);
  // Attach name to 2D view
  OMset_str_val(obj->contour2D.DLV2Dscene.Model_Name.obj_id(), model);
}

void DLV::solid_contours::attach_data(const drawable_obj *draw,
				      const int component, const string name,
				      const OMobj_id parent,
				      const OMobj_id space, const char *model)
{
  // Set name of display object
  OMset_str_val(obj->name.obj_id(), name.c_str());
  // attach display output
  OMobj_id out = OMfind_str_subobj(parent, "GroupObject.child_objs",
				   OM_OBJ_RW);
  OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
  // attach field
  OMset_obj_ref(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
  // set data values
  obj->ContourParam.contour_comp = component;
  // attach 2D replication
  //out = OMfind_str_subobj(space, "common_data", OM_OBJ_RW);
  //OMset_obj_ref(obj->context.obj_id(), out, 0);
  // Attach name to 2D view
  OMset_str_val(obj->contour2D.DLV2Dscene.Model_Name.obj_id(), model);
}

void DLV::shaded_contours::attach_data(const drawable_obj *draw,
				       const int component, const string name,
				       const OMobj_id parent,
				       const OMobj_id space, const char *model)
{
  // Set name of display object
  OMset_str_val(obj->name.obj_id(), name.c_str());
  // attach display output
  OMobj_id out = OMfind_str_subobj(parent, "GroupObject.child_objs",
				   OM_OBJ_RW);
  OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
  // attach field
  OMset_obj_ref(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
  // set data values
  obj->ContourParam.contour_comp = component;
  // attach 2D replication
  //out = OMfind_str_subobj(space, "common_data", OM_OBJ_RW);
  //OMset_obj_ref(obj->context.obj_id(), out, 0);
  // Attach name to 2D view
  OMset_str_val(obj->contour2D.DLV2Dscene.Model_Name.obj_id(), model);
}

void DLV::surface_plot::attach_data(const drawable_obj *draw,
				    const int component, const string name,
				    const OMobj_id parent,
				    const OMobj_id space)
{
  // Set name of display object
  OMset_str_val(obj->name.obj_id(), name.c_str());
  // attach display output
  OMobj_id out = OMfind_str_subobj(parent, "GroupObject.child_objs",
				   OM_OBJ_RW);
  OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
  // attach field
  OMset_obj_ref(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
  // set data values
  obj->SurfPlotParam.component = component;
  // attach 2D replication
  //out = OMfind_str_subobj(space, "common_data", OM_OBJ_RW);
  //OMset_obj_ref(obj->context.obj_id(), out, 0);
}

void DLV::plot_obj::attach_data(const drawable_obj *draw, const string name,
				const string xaxis, const string yaxis,
				const int ngraphs, const OMobj_id parent,
				const char *model, const bool visible)
{
  // Set up default visibility of plot lines.
  OMset_array_size(obj->parameters.plot_vis.obj_id(), ngraphs);
  int *ptr = (int *) obj->parameters.plot_vis.ret_array_ptr(OM_GET_ARRAY_WR);
  if (ptr != 0) {
    if(visible) 
      for (int i = 0; i < ngraphs; i++)
	ptr[i] = 1;
    else
      for (int i = 0; i < ngraphs; i++)
	ptr[i] = 0;
    obj->parameters.plot_vis.free_array(ptr);
  }
  // Set name of display object
  OMset_str_val(obj->title.obj_id(), name.c_str());
  // attach field
  OMset_obj_ref(obj->in.obj_id(), draw->get_id().get_id(), 0);
  // set data values
  obj->xlabel = xaxis.c_str();
  obj->ylabel = yaxis.c_str();
  // Attach name to 2D view
  OMset_str_val(obj->DLV2Dscene.Model_Name.obj_id(), model);
}

void DLV::display_3D_region::attach_data(const drawable_obj *draw,
					 const string name,
					 const OMobj_id parent)
{
  // Set name of display object
  OMset_str_val(obj->name.obj_id(), name.c_str());
  // attach display output
  OMobj_id out = OMfind_str_subobj(parent, "GroupObject.child_objs",
				   OM_OBJ_RW);
  OMadd_obj_ref(out, obj->obj.obj_id(), 0);
  // attach data
  OMset_obj_val(obj->data.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::display_sphere_obj::attach_data(const drawable_obj *draw,
					  const string name,
					  const OMobj_id parent)
{
  // Set name of display object
  OMset_str_val(obj->name.obj_id(), name.c_str());
  // attach display output
  OMobj_id out = OMfind_str_subobj(parent, "GroupObject.child_objs",
				   OM_OBJ_RW);
  OMadd_obj_ref(out, obj->obj.obj_id(), 0);
  // attach data
  OMset_obj_val(obj->data.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::display_plane::attach_data(const drawable_obj *draw,
				     const string name,
				     const OMobj_id parent)
{
  // Set name of display object
  OMset_str_val(obj->name.obj_id(), name.c_str());
  // attach display output
  OMobj_id out = OMfind_str_subobj(parent, "GroupObject.child_objs",
				   OM_OBJ_RW);
  OMadd_obj_ref(out, obj->obj.obj_id(), 0);
  // attach data
  OMset_obj_val(obj->data.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::display_line::attach_data(const drawable_obj *draw,
				    const string name,
				    const OMobj_id parent)
{
  // Set name of display object
  OMset_str_val(obj->name.obj_id(), name.c_str());
  // attach display output
  OMobj_id out = OMfind_str_subobj(parent, "GroupObject.child_objs",
				   OM_OBJ_RW);
  OMadd_obj_ref(out, obj->obj.obj_id(), 0);
  // attach data
  OMset_obj_val(obj->data.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::display_point::attach_data(const drawable_obj *draw,
				     const string name,
				     const OMobj_id parent)
{
  // Set name of display object
  OMset_str_val(obj->name.obj_id(), name.c_str());
  // attach display output
  OMobj_id out = OMfind_str_subobj(parent, "GroupObject.child_objs",
				   OM_OBJ_RW);
  OMadd_obj_ref(out, obj->obj.obj_id(), 0);
  // attach data
  OMset_obj_val(obj->data.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::display_wulff_region::attach_data(const drawable_obj *draw,
					    const string name,
					    const OMobj_id parent)
{
  // Set name of display object
  OMset_str_val(obj->name.obj_id(), name.c_str());
  // attach display output
  OMobj_id out = OMfind_str_subobj(parent, "GroupObject.child_objs",
				   OM_OBJ_RW);
  OMadd_obj_ref(out, obj->obj.obj_id(), 0);
  // attach data
  OMset_obj_val(obj->data.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::display_leed_pattern::attach_data(const drawable_obj *draw,
					    const string name,
					    const OMobj_id parent,
					    const char *model)
{
  // Set name of display object
  //OMset_str_val(obj->name.obj_id(), name.c_str());
  // attach display output
  //OMobj_id out = OMfind_str_subobj(parent, "GroupObject.child_objs",
  //			   OM_OBJ_RW);
  //OMadd_obj_ref(out, obj->obj.obj_id(), 0);
  // attach data
  OMset_obj_ref(obj->data.obj_id(), draw->get_id().get_id(), 0);
  // Attach name to 2D view
  OMset_str_val(obj->DLV2Dscene.Model_Name.obj_id(), model);
}

void DLV::display_text_data::reattach_data(const drawable_obj *draw,
					   const render_parent *parent)
{
  if (!is_rendered()) {
    // Todo
    OMset_obj_ref(obj->data.obj_id(), draw->get_id().get_id(), 0);
    set_rendered();
  }
}

void DLV::display_real_data::reattach_data(const drawable_obj *draw,
					   const render_parent *parent)
{
  if (!is_rendered()) {
    OMset_obj_ref(obj->data.obj_id(), draw->get_id().get_id(), 0);
    set_rendered();
  }
}

void DLV::display_file_view::reattach_data(const drawable_obj *draw,
					   const render_parent *parent)
{
  if (!is_rendered()) {
    // Todo
    OMset_obj_ref(obj->data.obj_id(), draw->get_id().get_id(), 0);
    set_rendered();
  }
}

void DLV::display_text_view::reattach_data(const drawable_obj *draw,
					   const render_parent *parent)
{
  if (!is_rendered()) {
    // Todo
    OMset_obj_ref(obj->data.obj_id(), draw->get_id().get_id(), 0);
    set_rendered();
  }
}

void DLV::display_atom_bond_data::reattach_data(const drawable_obj *draw,
						const render_parent *parent)
{
  if (!is_rendered()) {
    // Set name of display object
    OMset_str_val(obj->name.obj_id(), get_name().c_str());
    // attach display output
    DLV::render_parent *px = DLV::render_parent::get_serialize_obj();
    OMobj_id p = DLV::find_property(px, "Points", false);
    OMobj_id out = OMfind_str_subobj(p, "GroupObject.child_objs",
				     OM_OBJ_RW);
    OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
    OMset_obj_val(obj->data.obj_id(), draw->get_id().get_id(), 0);
    set_rendered();
  }
}

void DLV::display_atom_text_data::reattach_data(const drawable_obj *draw,
						const render_parent *parent)
{
  if (!is_rendered()) {
    DLV::render_parent *px = DLV::render_parent::get_serialize_obj();
    OMobj_id p = DLV::find_property(px, "Points", false);
    OMobj_id out = OMfind_str_subobj(p, "GroupObject.child_objs",
				     OM_OBJ_RW);
    OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
    OMset_obj_ref(obj->glyph_field.obj_id(), draw->get_id().get_id(), 0);
    set_rendered();
  }
}

void DLV::display_atom_vector_data::reattach_data(const drawable_obj *draw,
						  const render_parent *parent)
{
  if (!is_rendered()) {
    // Set name of display object
    OMset_str_val(obj->name.obj_id(), get_name().c_str());
    DLV::render_parent *px = DLV::render_parent::get_serialize_obj();
    OMobj_id p = DLV::find_property(px, "Points", false);
    OMobj_id out = OMfind_str_subobj(p, "GroupObject.child_objs",
				     OM_OBJ_RW);
    OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
    OMset_obj_ref(obj->glyph_field.obj_id(), draw->get_id().get_id(), 0);
    set_rendered();
  }
}

void DLV::isosurface::reattach_data(const drawable_obj *draw,
				    const render_parent *parent)
{
  if (!is_rendered()) {
    // Set name of display object
    OMset_str_val(obj->name.obj_id(), get_name().c_str());
    // attach display output
    OMobj_id p = DLV::find_property(parent, "Volumes", is_k_space());
    OMobj_id out = OMfind_str_subobj(p, "GroupObject.child_objs",
				     OM_OBJ_RW);
    OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
    OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
    attach_transforms(parent);
    obj->IsoParam.colour_data = colour_comp;
    set_rendered();
  }
}

void DLV::iso_wavefn::reattach_data(const drawable_obj *draw,
				    const render_parent *parent)
{
  if (!is_rendered()) {
    // Set name of display object
    OMset_str_val(obj->name.obj_id(), get_name().c_str());
    // attach display output
    //DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
    OMobj_id p = DLV::find_property(parent, "Volumes", is_k_space());
    OMobj_id out = OMfind_str_subobj(p, "GroupObject.child_objs",
				     OM_OBJ_RW);
    OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
    // attach field
    OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
    attach_transforms(parent);
    set_rendered();
  }
}

void DLV::cells_obj::reattach_data(const drawable_obj *draw,
				   const render_parent *parent)
{
  if (!is_rendered()) {
    // Todo
    OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
    set_rendered();
  }
}

void DLV::spheres::reattach_data(const drawable_obj *draw,
				 const render_parent *parent)
{
  if (!is_rendered()) {
    // Todo
    OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
    set_rendered();
  }
}

void DLV::volume_view::reattach_data(const drawable_obj *draw,
				       const render_parent *parent)
{
  if (!is_rendered()) {
    // Todo
    OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
    set_rendered();
  }
}

void DLV::hedgehog::reattach_data(const drawable_obj *draw,
				  const render_parent *parent)
{
  if (!is_rendered()) {
    OMset_str_val(obj->name.obj_id(), get_name().c_str());
    OMobj_id p = DLV::find_property(parent, "Volumes", is_k_space());
    OMobj_id out = OMfind_str_subobj(p, "GroupObject.child_objs",
				     OM_OBJ_RW);
    OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
    // attach field
    OMset_obj_ref(obj->glyph_field.obj_id(), draw->get_id().get_id(), 0);
    attach_transforms(parent);
    set_rendered();
  }
}

void DLV::streamlines::reattach_data(const drawable_obj *draw,
				     const render_parent *parent)
{
  if (!is_rendered()) {
    OMset_str_val(obj->name.obj_id(), get_name().c_str());
    // attach display output
    OMobj_id p = DLV::find_property(parent, "Volumes", is_k_space());
    OMobj_id out = OMfind_str_subobj(p, "GroupObject.child_objs",
				     OM_OBJ_RW);
    OMadd_obj_ref(out, obj->obj.obj_id(), 0);
    // attach field
    OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
    attach_transforms(parent);
    set_rendered();
  }
}

void DLV::ag_contours::reattach_data(const drawable_obj *draw,
				     const render_parent *parent)
{
  if (!is_rendered()) {
    OMobj_id p = DLV::find_property(parent, "Planes", is_k_space());
    OMobj_id out = OMfind_str_subobj(p, "GroupObject.child_objs",
				     OM_OBJ_RW);
    OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
    // attach 2D replication
    OMobj_id space = parent->get_rspace().get_id();
    out = OMfind_str_subobj(space, "common_data", OM_OBJ_RW);
    OMset_obj_ref(obj->context.obj_id(), out, 0);
    OMset_obj_ref(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
    attach_transforms(parent);
    set_rendered();
  }
}

void DLV::contours::reattach_data(const drawable_obj *draw,
				  const render_parent *parent)
{
  if (!is_rendered()) {
    OMobj_id p = DLV::find_property(parent, "Planes", is_k_space());
    OMobj_id out = OMfind_str_subobj(p, "GroupObject.child_objs",
				     OM_OBJ_RW);
    OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
    // attach 2D replication
    //OMobj_id space = parent->get_rspace().get_id();
    //out = OMfind_str_subobj(space, "common_data", OM_OBJ_RW);
    //OMset_obj_ref(obj->context.obj_id(), out, 0);
    OMset_obj_ref(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
    attach_transforms(parent);
    set_rendered();
  }
}

void DLV::pn_contours::reattach_data(const drawable_obj *draw,
				     const render_parent *parent)
{
  if (!is_rendered()) {
    OMobj_id p = DLV::find_property(parent, "Planes", is_k_space());
    OMobj_id out = OMfind_str_subobj(p, "GroupObject.child_objs",
				     OM_OBJ_RW);
    OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
    // attach 2D replication
    //OMobj_id space = parent->get_rspace().get_id();
    //out = OMfind_str_subobj(space, "common_data", OM_OBJ_RW);
    //OMset_obj_ref(obj->context.obj_id(), out, 0);
    OMset_obj_ref(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
    attach_transforms(parent);
    set_rendered();
  }
}

void DLV::solid_contours::reattach_data(const drawable_obj *draw,
					const render_parent *parent)
{
  if (!is_rendered()) {
    OMobj_id p = DLV::find_property(parent, "Planes", is_k_space());
    OMobj_id out = OMfind_str_subobj(p, "GroupObject.child_objs",
				     OM_OBJ_RW);
    OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
    // attach 2D replication
    //OMobj_id space = parent->get_rspace().get_id();
    //out = OMfind_str_subobj(space, "common_data", OM_OBJ_RW);
    //OMset_obj_ref(obj->context.obj_id(), out, 0);
    OMset_obj_ref(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
    attach_transforms(parent);
    set_rendered();
  }
}

void DLV::shaded_contours::reattach_data(const drawable_obj *draw,
					 const render_parent *parent)
{
  if (!is_rendered()) {
    OMobj_id p = DLV::find_property(parent, "Planes", is_k_space());
    OMobj_id out = OMfind_str_subobj(p, "GroupObject.child_objs",
				     OM_OBJ_RW);
    OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
    // attach 2D replication
    //OMobj_id space = parent->get_rspace().get_id();
    //out = OMfind_str_subobj(space, "common_data", OM_OBJ_RW);
    //OMset_obj_ref(obj->context.obj_id(), out, 0);
    OMset_obj_ref(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
    attach_transforms(parent);
    set_rendered();
  }
}

void DLV::surface_plot::reattach_data(const drawable_obj *draw,
				      const render_parent *parent)
{
  if (!is_rendered()) {
    OMobj_id p = DLV::find_property(parent, "Planes", is_k_space());
    OMobj_id out = OMfind_str_subobj(p, "GroupObject.child_objs",
				     OM_OBJ_RW);
    OMadd_obj_ref(out, obj->out_obj.obj_id(), 0);
    OMset_obj_ref(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
    attach_transforms(parent);
    set_rendered();
  }
}

void DLV::plot_obj::reattach_data(const drawable_obj *draw,
				  const render_parent *parent)
{
  if (!is_rendered()) {
    OMset_obj_ref(obj->in.obj_id(), draw->get_id().get_id(), 0);
    set_rendered();
  }
}

void DLV::display_3D_region::reattach_data(const drawable_obj *draw,
					   const render_parent *parent)
{
  if (!is_rendered()) {
    OMobj_id p = DLV::find_property(parent, "Volumes", is_k_space());
    OMobj_id out = OMfind_str_subobj(p, "GroupObject.child_objs",
				     OM_OBJ_RW);
    OMadd_obj_ref(out, obj->obj.obj_id(), 0);
    OMset_obj_val(obj->data.obj_id(), draw->get_id().get_id(), 0);
    set_rendered();
  }
}

void DLV::display_sphere_obj::reattach_data(const drawable_obj *draw,
					    const render_parent *parent)
{
  if (!is_rendered()) {
    OMobj_id p = DLV::find_property(parent, "Volumes", is_k_space());
    OMobj_id out = OMfind_str_subobj(p, "GroupObject.child_objs",
				     OM_OBJ_RW);
    OMadd_obj_ref(out, obj->obj.obj_id(), 0);
    OMset_obj_val(obj->data.obj_id(), draw->get_id().get_id(), 0);
    set_rendered();
  }
}

void DLV::display_plane::reattach_data(const drawable_obj *draw,
				       const render_parent *parent)
{
  if (!is_rendered()) {
    OMobj_id p = DLV::find_property(parent, "Planes", is_k_space());
    OMobj_id out = OMfind_str_subobj(p, "GroupObject.child_objs",
				     OM_OBJ_RW);
    OMadd_obj_ref(out, obj->obj.obj_id(), 0);
    OMset_obj_val(obj->data.obj_id(), draw->get_id().get_id(), 0);
    set_rendered();
  }
}

void DLV::display_line::reattach_data(const drawable_obj *draw,
				      const render_parent *parent)
{
  if (!is_rendered()) {
    // Todo
    OMset_obj_val(obj->data.obj_id(), draw->get_id().get_id(), 0);
    set_rendered();
  }
}

void DLV::display_point::reattach_data(const drawable_obj *draw,
				       const render_parent *parent)
{
  if (!is_rendered()) {
    // Todo
    OMset_obj_val(obj->data.obj_id(), draw->get_id().get_id(), 0);
    set_rendered();
  }
}

void DLV::display_wulff_region::reattach_data(const drawable_obj *draw,
					      const render_parent *parent)
{
  if (!is_rendered()) {
    // Todo
    OMset_obj_val(obj->data.obj_id(), draw->get_id().get_id(), 0);
    set_rendered();
  }
}

void DLV::display_leed_pattern::reattach_data(const drawable_obj *draw,
					      const render_parent *parent)
{
  if (!is_rendered()) {
    // Todo
    OMset_obj_ref(obj->data.obj_id(), draw->get_id().get_id(), 0);
    set_rendered();
  }
}

void DLV::display_atom_trajectory::reattach_data(const drawable_obj *draw,
						 const render_parent *parent)
{
  if (!is_rendered()) {
    // Todo
    set_rendered();
  }
}

void DLV::isosurface::attach_transforms(const render_parent *parent)
{
}

void DLV::periodic_isosurface::attach_transforms(const render_parent *parent)
{
  CCP3_Renderers_Volumes_isosurface_rep *ptr =
    static_cast<CCP3_Renderers_Volumes_isosurface_rep *>(get_obj());
  OMobj_id space = parent->get_rspace().get_id();
  OMobj_id id = OMfind_str_subobj(space, "ntransforms", OM_OBJ_RW);
  OMset_obj_ref(ptr->n.obj_id(), id, 0);
  id = OMfind_str_subobj(space, "transforms", OM_OBJ_RW);
  OMset_obj_ref(ptr->Xform.obj_id(), id, 0);
}

void DLV::iso_wavefn::attach_transforms(const render_parent *parent)
{
}

void DLV::iso_bloch::attach_transforms(const render_parent *parent)
{
  CCP3_Renderers_Volumes_iso_wavefn_base *ptr = get_obj();
  OMobj_id space = parent->get_rspace().get_id();
  OMobj_id id = OMfind_str_subobj(space, "common_data", OM_OBJ_RW);
  OMobj_id n = OMfind_str_subobj(id, "na", OM_OBJ_RW);
  OMset_obj_val(ptr->na.obj_id(), n, 0);
  n = OMfind_str_subobj(id, "nb", OM_OBJ_RW);
  OMset_obj_val(ptr->nb.obj_id(), n, 0);
  n = OMfind_str_subobj(id, "nc", OM_OBJ_RW);
  OMset_obj_val(ptr->nc.obj_id(), n, 0);
}

void DLV::cells_obj::attach_transforms(const render_parent *parent)
{
}

void DLV::periodic_cells::attach_transforms(const render_parent *parent)
{
  CCP3_Renderers_Volumes_shrink_cells_rep *ptr =
    static_cast<CCP3_Renderers_Volumes_shrink_cells_rep *>(get_obj());
  OMobj_id space = parent->get_rspace().get_id();
  OMobj_id id = OMfind_str_subobj(space, "ntransforms", OM_OBJ_RW);
  OMset_obj_ref(ptr->n.obj_id(), id, 0);
  id = OMfind_str_subobj(space, "transforms", OM_OBJ_RW);
  OMset_obj_ref(ptr->Xform.obj_id(), id, 0);
}

void DLV::spheres::attach_transforms(const render_parent *parent)
{
}

void DLV::volume_view::attach_transforms(const render_parent *parent)
{
}

void DLV::periodic_spheres::attach_transforms(const render_parent *parent)
{
  CCP3_Renderers_Volumes_spheres_rep *ptr =
    static_cast<CCP3_Renderers_Volumes_spheres_rep *>(get_obj());
  OMobj_id space = parent->get_rspace().get_id();
  OMobj_id id = OMfind_str_subobj(space, "ntransforms", OM_OBJ_RW);
  OMset_obj_ref(ptr->n.obj_id(), id, 0);
  id = OMfind_str_subobj(space, "transforms", OM_OBJ_RW);
  OMset_obj_ref(ptr->Xform.obj_id(), id, 0);

}

void DLV::hedgehog::attach_transforms(const render_parent *parent)
{
}

void DLV::periodic_hedgehog::attach_transforms(const render_parent *parent)
{
  // Todo
}

void DLV::streamlines::attach_transforms(const render_parent *parent)
{
}

void DLV::periodic_streamlines::attach_transforms(const render_parent *parent)
{
  // Todo
}

void DLV::ag_contours::attach_transforms(const render_parent *parent)
{
}

void DLV::periodic_ag_contours::attach_transforms(const render_parent *parent)
{
  CCP3_Renderers_Planes_AGisoline_rep *ptr =
    static_cast<CCP3_Renderers_Planes_AGisoline_rep *>(get_obj());
  OMobj_id space = parent->get_rspace().get_id();
  OMobj_id id = OMfind_str_subobj(space, "ntransforms", OM_OBJ_RW);
  OMset_obj_ref(ptr->n.obj_id(), id, 0);
  id = OMfind_str_subobj(space, "transforms", OM_OBJ_RW);
  OMset_obj_ref(ptr->Xform.obj_id(), id, 0);

}

void DLV::contours::attach_transforms(const render_parent *parent)
{
}

void DLV::periodic_contours::attach_transforms(const render_parent *parent)
{
  CCP3_Renderers_Planes_isoline_rep *ptr =
    static_cast<CCP3_Renderers_Planes_isoline_rep *>(get_obj());
  OMobj_id space = parent->get_rspace().get_id();
  OMobj_id id = OMfind_str_subobj(space, "ntransforms", OM_OBJ_RW);
  OMset_obj_ref(ptr->n.obj_id(), id, 0);
  id = OMfind_str_subobj(space, "transforms", OM_OBJ_RW);
  OMset_obj_ref(ptr->Xform.obj_id(), id, 0);

}

void DLV::pn_contours::attach_transforms(const render_parent *parent)
{
}

void DLV::periodic_pn_contours::attach_transforms(const render_parent *parent)
{
  CCP3_Renderers_Planes_disoline_rep *ptr =
    static_cast<CCP3_Renderers_Planes_disoline_rep *>(get_obj());
  OMobj_id space = parent->get_rspace().get_id();
  OMobj_id id = OMfind_str_subobj(space, "ntransforms", OM_OBJ_RW);
  OMset_obj_ref(ptr->n.obj_id(), id, 0);
  id = OMfind_str_subobj(space, "transforms", OM_OBJ_RW);
  OMset_obj_ref(ptr->Xform.obj_id(), id, 0);

}

void DLV::solid_contours::attach_transforms(const render_parent *parent)
{
}

void
DLV::periodic_solid_contours::attach_transforms(const render_parent *parent)
{
  CCP3_Renderers_Planes_solid_contour_rep *ptr =
    static_cast<CCP3_Renderers_Planes_solid_contour_rep *>(get_obj());
  OMobj_id space = parent->get_rspace().get_id();
  OMobj_id id = OMfind_str_subobj(space, "ntransforms", OM_OBJ_RW);
  OMset_obj_ref(ptr->n.obj_id(), id, 0);
  id = OMfind_str_subobj(space, "transforms", OM_OBJ_RW);
  OMset_obj_ref(ptr->Xform.obj_id(), id, 0);

}

void DLV::shaded_contours::attach_transforms(const render_parent *parent)
{
}

void
DLV::periodic_shaded_contours::attach_transforms(const render_parent *parent)
{
  CCP3_Renderers_Planes_shaded_contour_rep *ptr =
    static_cast<CCP3_Renderers_Planes_shaded_contour_rep *>(get_obj());
  OMobj_id space = parent->get_rspace().get_id();
  OMobj_id id = OMfind_str_subobj(space, "ntransforms", OM_OBJ_RW);
  OMset_obj_ref(ptr->n.obj_id(), id, 0);
  id = OMfind_str_subobj(space, "transforms", OM_OBJ_RW);
  OMset_obj_ref(ptr->Xform.obj_id(), id, 0);

}

void DLV::surface_plot::attach_transforms(const render_parent *parent)
{
}

void DLV::periodic_surface_plot::attach_transforms(const render_parent *parent)
{
  CCP3_Renderers_Planes_surf_plot_rep *ptr =
    static_cast<CCP3_Renderers_Planes_surf_plot_rep *>(get_obj());
  OMobj_id space = parent->get_rspace().get_id();
  OMobj_id id = OMfind_str_subobj(space, "ntransforms", OM_OBJ_RW);
  OMset_obj_ref(ptr->n.obj_id(), id, 0);
  id = OMfind_str_subobj(space, "transforms", OM_OBJ_RW);
  OMset_obj_ref(ptr->Xform.obj_id(), id, 0);

}

void DLV::display_text_data::attach_params() const
{
  static OMobj_id visible = OMnull_obj;
  static OMobj_id params = OMnull_obj;
  if (OMis_null_obj(visible)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.textUI.params", OM_OBJ_RW);
  }
  OMset_obj_ref(visible, obj->visible.obj_id(), 0);
  OMset_obj_ref(params, obj->params.obj_id(), 0);
}

void DLV::display_real_data::attach_params() const
{
  static OMobj_id visible = OMnull_obj;
  static OMobj_id params = OMnull_obj;
  if (OMis_null_obj(params)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.realUI.params", OM_OBJ_RW);
  }
  OMset_obj_ref(visible, obj->visible.obj_id(), 0);
  OMset_obj_ref(params, obj->params.obj_id(), 0);
}

void DLV::display_file_view::attach_params() const
{
  static OMobj_id visible = OMnull_obj;
  if (OMis_null_obj(visible)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
  }
  OMset_obj_ref(visible, obj->visible.obj_id(), 0);
  //OMset_obj_ref(params, obj->params.obj_id(), 0);
}

void DLV::display_text_view::attach_params() const
{
  static OMobj_id visible = OMnull_obj;
  if (OMis_null_obj(visible)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
  }
  OMset_obj_ref(visible, obj->visible.obj_id(), 0);
  //OMset_obj_ref(params, obj->params.obj_id(), 0);
}

void DLV::display_atom_bond_data::attach_params() const
{
  static OMobj_id visible = OMnull_obj;
  static OMobj_id params = OMnull_obj;
  if (OMis_null_obj(params)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.bondsUI.params", OM_OBJ_RW);
  }
  OMset_obj_ref(visible, obj->visible.obj_id(), 0);
  OMset_obj_ref(params, obj->params.obj_id(), 0);
}

void DLV::display_atom_text_data::attach_params() const
{
  static OMobj_id visible = OMnull_obj;
  static OMobj_id params = OMnull_obj;
  if (OMis_null_obj(params)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.bondsUI.params", OM_OBJ_RW);
  }
  OMset_obj_ref(visible, obj->visible.obj_id(), 0);
  OMset_obj_ref(params, obj->params.obj_id(), 0);
}

void DLV::display_atom_vector_data::attach_params() const
{
  static OMobj_id visible = OMnull_obj;
  static OMobj_id params = OMnull_obj;
  static OMobj_id field = OMnull_obj;
  if (OMis_null_obj(visible)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.AtomVectorUI.scale",
			OM_OBJ_RW);
    field =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.AtomVectorUI.use_colour",
			OM_OBJ_RW);
  }
  OMset_obj_ref(visible, obj->visible.obj_id(), 0);
  OMset_obj_val(params, obj->scale.obj_id(), 0);
  OMset_obj_val(field, obj->colour.obj_id(), 0);
}

void DLV::display_atom_trajectory::attach_params() const
{
  static OMobj_id visible = OMnull_obj;
  //static OMobj_id params = OMnull_obj;
  //static OMobj_id field = OMnull_obj;
  if (OMis_null_obj(visible)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
    /*params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.AtomVectorUI.scale",
			OM_OBJ_RW);
    field =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.AtomVectorUI.use_colour",
			OM_OBJ_RW);*/
  }
  OMset_obj_ref(visible, obj->visible.obj_id(), 0);
  //OMset_obj_ref(params, obj->scale.obj_id(), 0);
  //OMset_obj_ref(field, obj->colour.obj_id(), 0);
}

void DLV::isosurface::attach_params() const
{
  static OMobj_id visible = OMnull_obj;
  static OMobj_id params = OMnull_obj;
  static OMobj_id field = OMnull_obj;
  static OMobj_id threads = OMnull_obj;
  if (OMis_null_obj(visible)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.IsoUI.param", OM_OBJ_RW);
    field =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.IsoUI.in_fld", OM_OBJ_RW);
    threads =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.nthreads", OM_OBJ_RW);
  }
  OMset_obj_ref(field, obj->in_field.obj_id(), 0);
  OMset_obj_ref(params, obj->IsoParam.obj_id(), 0);
  OMset_obj_ref(visible, obj->visible.obj_id(), 0);
  OMset_obj_ref(obj->nthreads.obj_id(), threads, 0);
}

void DLV::iso_wavefn::attach_params() const
{
  static OMobj_id visible = OMnull_obj;
  static OMobj_id params = OMnull_obj;
  static OMobj_id field = OMnull_obj;
  static OMobj_id threads = OMnull_obj;
  if (OMis_null_obj(visible)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.IsoWavefnUI.param", OM_OBJ_RW);
    field =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.IsoWavefnUI.in_fld",
			OM_OBJ_RW);
    threads =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.nthreads", OM_OBJ_RW);
  }
  OMset_obj_ref(field, obj->in_field.obj_id(), 0);
  OMset_obj_ref(params, obj->IsoParam.obj_id(), 0);
  OMset_obj_ref(visible, obj->visible.obj_id(), 0);
  OMset_obj_ref(obj->nthreads.obj_id(), threads, 0);
}

void DLV::cells_obj::attach_params() const
{
  static OMobj_id visible = OMnull_obj;
  static OMobj_id params = OMnull_obj;
  static OMobj_id cparams = OMnull_obj;
  static OMobj_id field = OMnull_obj;
  if (OMis_null_obj(visible)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.ShrinkUI.param", OM_OBJ_RW);
    cparams =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.ShrinkUI.cparam", OM_OBJ_RW);
    field =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.ShrinkUI.in_fld", OM_OBJ_RW);
  }
  OMset_obj_ref(visible, obj->visible.obj_id(), 0);
  OMset_obj_ref(cparams, obj->ContourParam.obj_id(), 0);
  OMset_obj_ref(params, obj->shrink_cells.ShrinkParam.obj_id(), 0);
  OMset_obj_ref(field, obj->in_field.obj_id(), 0);
}

void DLV::spheres::attach_params() const
{
  static OMobj_id visible = OMnull_obj;
  static OMobj_id params = OMnull_obj;
  static OMobj_id field = OMnull_obj;
  if (OMis_null_obj(visible)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.SphereUI.param", OM_OBJ_RW);
    field =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.SphereUI.in_fld", OM_OBJ_RW);
  }
  OMset_obj_ref(visible, obj->visible.obj_id(), 0);
  OMset_obj_ref(params, obj->param.obj_id(), 0);
  OMset_obj_ref(field, obj->in_field.obj_id(), 0);
}

void DLV::volume_view::attach_params() const
{
  static OMobj_id visible = OMnull_obj;
  static OMobj_id params = OMnull_obj;
  //static OMobj_id field = OMnull_obj;
  if (OMis_null_obj(visible)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.volume_rUI.params", OM_OBJ_RW);
    //field =
    //  OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
    //		".displayUI.data_panels.volume_rUI.in_fld", OM_OBJ_RW);
  }
  OMset_obj_ref(visible, obj->visible.obj_id(), 0);
  OMset_obj_ref(params, obj->param.obj_id(), 0);
  //OMset_obj_ref(field, obj->in_field.obj_id(), 0);
}

void DLV::hedgehog::attach_params() const
{
  // Todo
  static OMobj_id visible = OMnull_obj;
  static OMobj_id params = OMnull_obj;
  static OMobj_id field = OMnull_obj;
  if (OMis_null_obj(visible)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.SphereUI.param", OM_OBJ_RW);
    field =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.SphereUI.in_fld", OM_OBJ_RW);
  }
  OMset_obj_ref(visible, obj->visible.obj_id(), 0);
  //TodoOMset_obj_ref(params, obj->param.obj_id(), 0);
  OMset_obj_ref(field, obj->glyph_field.obj_id(), 0);
}

void DLV::streamlines::attach_params() const
{
  // Todo
  static OMobj_id visible = OMnull_obj;
  static OMobj_id params = OMnull_obj;
  static OMobj_id field = OMnull_obj;
  static OMobj_id threads = OMnull_obj;
  if (OMis_null_obj(visible)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.StreamlineUI.param",
			OM_OBJ_RW);
    field =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.StreamlineUI.in_fld",
			OM_OBJ_RW);
    threads =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.nthreads", OM_OBJ_RW);
  }
  OMset_obj_ref(visible, obj->visible.obj_id(), 0);
  OMset_obj_ref(params, obj->StreamParam.obj_id(), 0);
  OMset_obj_ref(field, obj->in_field.obj_id(), 0);
  OMset_obj_ref(obj->nthreads.obj_id(), threads, 0);
}

void DLV::ag_contours::attach_params() const
{
  static OMobj_id visible = OMnull_obj;
  static OMobj_id visible2D = OMnull_obj;
  static OMobj_id params = OMnull_obj;
  static OMobj_id field = OMnull_obj;
  if (OMis_null_obj(visible)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.AGisolineUI.param", OM_OBJ_RW);
    field =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.AGisolineUI.in_fld",
			OM_OBJ_RW);
    visible2D =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.AGisolineUI.visible2D",
			OM_OBJ_RW);
  }
  OMset_obj_ref(visible, obj->visible3D.obj_id(), 0);
  OMset_obj_ref(params, obj->IsoParam.obj_id(), 0);
  OMset_obj_ref(field, obj->in_field.obj_id(), 0);
  OMset_obj_ref(visible2D, obj->visible2D.obj_id(), 0);
}

void DLV::contours::attach_params() const
{
  static OMobj_id visible = OMnull_obj;
  static OMobj_id visible2D = OMnull_obj;
  static OMobj_id params = OMnull_obj;
  static OMobj_id field = OMnull_obj;
  if (OMis_null_obj(visible)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.isolineUI.param", OM_OBJ_RW);
    field =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.isolineUI.in_fld",
			OM_OBJ_RW);
    visible2D =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.isolineUI.visible2D",
			OM_OBJ_RW);
  }
  OMset_obj_ref(visible, obj->visible3D.obj_id(), 0);
  OMset_obj_ref(params, obj->IsoParam.obj_id(), 0);
  OMset_obj_ref(field, obj->in_field.obj_id(), 0);
  OMset_obj_ref(visible2D, obj->visible2D.obj_id(), 0);
}

void DLV::pn_contours::attach_params() const
{
  static OMobj_id visible = OMnull_obj;
  static OMobj_id visible2D = OMnull_obj;
  static OMobj_id params = OMnull_obj;
  static OMobj_id field = OMnull_obj;
  if (OMis_null_obj(visible)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.PNisolineUI.param", OM_OBJ_RW);
    field =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.PNisolineUI.in_fld",
			OM_OBJ_RW);
    visible2D =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.PNisolineUI.visible2D",
			OM_OBJ_RW);
  }
  OMset_obj_ref(visible, obj->visible3D.obj_id(), 0);
  OMset_obj_ref(params, obj->IsoParam.obj_id(), 0);
  OMset_obj_ref(field, obj->in_field.obj_id(), 0);
  OMset_obj_ref(visible2D, obj->visible2D.obj_id(), 0);
}

void DLV::solid_contours::attach_params() const
{
  static OMobj_id visible = OMnull_obj;
  static OMobj_id visible2D = OMnull_obj;
  static OMobj_id params = OMnull_obj;
  static OMobj_id field = OMnull_obj;
  if (OMis_null_obj(visible)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.solidUI.param", OM_OBJ_RW);
    field =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.solidUI.in_fld",
			OM_OBJ_RW);
    visible2D =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.solidUI.visible2D",
			OM_OBJ_RW);
  }
  OMset_obj_ref(visible, obj->visible3D.obj_id(), 0);
  OMset_obj_ref(params, obj->ContourParam.obj_id(), 0);
  OMset_obj_ref(field, obj->in_field.obj_id(), 0);
  OMset_obj_ref(visible2D, obj->visible2D.obj_id(), 0);
}

void DLV::shaded_contours::attach_params() const
{
  static OMobj_id visible = OMnull_obj;
  static OMobj_id visible2D = OMnull_obj;
  static OMobj_id params = OMnull_obj;
  static OMobj_id field = OMnull_obj;
  if (OMis_null_obj(visible)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.shadedUI.param", OM_OBJ_RW);
    field =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.shadedUI.in_fld",
			OM_OBJ_RW);
    visible2D =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.shadedUI.visible2D",
			OM_OBJ_RW);
  }
  OMset_obj_ref(visible, obj->visible3D.obj_id(), 0);
  OMset_obj_ref(params, obj->ContourParam.obj_id(), 0);
  OMset_obj_ref(field, obj->in_field.obj_id(), 0);
  OMset_obj_ref(visible2D, obj->visible2D.obj_id(), 0);
}

void DLV::surface_plot::attach_params() const
{
  static OMobj_id visible = OMnull_obj;
  static OMobj_id params = OMnull_obj;
  static OMobj_id field = OMnull_obj;
  if (OMis_null_obj(visible)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.surfplotUI.plot_data",
			OM_OBJ_RW);
    field =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.surfplotUI.in_fld",
			OM_OBJ_RW);
  }
  OMset_obj_ref(visible, obj->visible.obj_id(), 0);
  OMset_obj_ref(params, obj->SurfPlotParam.obj_id(), 0);
  OMset_obj_ref(field, obj->in_field.obj_id(), 0);
}

void DLV::plot_obj::attach_params() const
{
  static OMobj_id visible = OMnull_obj;
  static OMobj_id params = OMnull_obj;
  //static OMobj_id field = OMnull_obj;
  if (OMis_null_obj(visible)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.PlotUI.params", OM_OBJ_RW);
    //field =
    //  OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
    //		".displayUI.data_panels.PlotUI.in_fld",
    //		OM_OBJ_RW);
  }
  OMset_obj_ref(visible, obj->visible.obj_id(), 0);
  OMset_obj_ref(params, obj->parameters.obj_id(), 0);
  //OMset_obj_ref(field, obj->in.obj_id(), 0);
}

void DLV::display_3D_region::attach_params() const
{
  static OMobj_id visible = OMnull_obj;
  static OMobj_id params = OMnull_obj;
  if (OMis_null_obj(params)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.boxUI.params", OM_OBJ_RW);
  }
  OMset_obj_ref(visible, obj->visible.obj_id(), 0);
  OMset_obj_ref(params, obj->params.obj_id(), 0);
}

void DLV::display_sphere_obj::attach_params() const
{
  static OMobj_id visible = OMnull_obj;
  static OMobj_id params = OMnull_obj;
  if (OMis_null_obj(params)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.SphUI.params", OM_OBJ_RW);
  }
  OMset_obj_ref(visible, obj->visible.obj_id(), 0);
  OMset_obj_ref(params, obj->params.obj_id(), 0);
}

void DLV::display_plane::attach_params() const
{
  static OMobj_id visible = OMnull_obj;
  static OMobj_id params = OMnull_obj;
  if (OMis_null_obj(params)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.planeUI.params", OM_OBJ_RW);
  }
  OMset_obj_ref(visible, obj->visible.obj_id(), 0);
  OMset_obj_ref(params, obj->params.obj_id(), 0);
}

void DLV::display_line::attach_params() const
{
  static OMobj_id visible = OMnull_obj;
  static OMobj_id params = OMnull_obj;
  if (OMis_null_obj(params)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.lineUI.params", OM_OBJ_RW);
  }
  OMset_obj_ref(visible, obj->visible.obj_id(), 0);
  OMset_obj_ref(params, obj->params.obj_id(), 0);
}

void DLV::display_point::attach_params() const
{
  static OMobj_id visible = OMnull_obj;
  static OMobj_id params = OMnull_obj;
  if (OMis_null_obj(params)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.pointUI.params", OM_OBJ_RW);
  }
  OMset_obj_ref(visible, obj->visible.obj_id(), 0);
  OMset_obj_ref(params, obj->params.obj_id(), 0);
}

void DLV::display_wulff_region::attach_params() const
{
  static OMobj_id visible = OMnull_obj;
  static OMobj_id params = OMnull_obj;
  if (OMis_null_obj(params)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.WulffUI.params", OM_OBJ_RW);
  }
  OMset_obj_ref(visible, obj->visible.obj_id(), 0);
  OMset_obj_ref(params, obj->params.obj_id(), 0);
}

void DLV::display_leed_pattern::attach_params() const
{
  static OMobj_id visible = OMnull_obj;
  static OMobj_id params = OMnull_obj;
  if (OMis_null_obj(params)) {
    visible =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.visible", OM_OBJ_RW);
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_display"
			".displayUI.data_panels.leedUI.params", OM_OBJ_RW);
  }
  OMset_obj_ref(visible, obj->visible.obj_id(), 0);
  OMset_obj_ref(params, obj->params.obj_id(), 0);
}

bool DLV::display_obj::attach_editor(const bool v, const toolkit_obj &t,
				     float matrix[4][4], float translate[3],
				     float centre[3], char message[],
				     const int mlen) const
{
  strncpy(message, "BUG: attempt to attach non-editable object", mlen);
  return false;
}

bool DLV::display_3D_region::attach_editor(const bool v, const toolkit_obj &t,
					   float matrix[4][4],
					   float translate[3],
					   float centre[3], char message[],
					   const int mlen) const
{
  if (v) {
    OMset_obj_val(t.get_id(), obj->obj.obj_id(), 0);
    OMset_int_val(obj->region3D.Top.xform_mode.obj_id(), 0); //Normal
  } else {
    OMset_obj_val(t.get_id(), OMnull_obj, 0);
    // recover transformation
    float (*m)[4] =
      (float (*)[4])obj->region3D.Xform.mat.ret_array_ptr(OM_GET_ARRAY_RW);
    for (int i = 0; i < 4; i++)
      for (int j = 0; j < 4; j++)
	matrix[i][j] = m[i][j];
    for (int i = 0; i < 4; i++) {
      for (int j = 0; j < 4; j++)
	m[i][j] = 0.0;
      m[i][i] = 1.0;
    }
    obj->region3D.Xform.mat.free_array(m);
    float *vec =
      (float *)obj->region3D.Xform.xlate.ret_array_ptr(OM_GET_ARRAY_RW);
    for (int i = 0; i < 3; i++)
      translate[i] = vec[i];
    for (int i = 0; i < 3; i++)
      vec[i] = 0.0;
    obj->region3D.Xform.xlate.free_array(vec);
    vec = (float *)obj->region3D.Xform.center.ret_array_ptr(OM_GET_ARRAY_RW);
    for (int i = 0; i < 3; i++)
      centre[i] = vec[i];
    for (int i = 0; i < 3; i++)
      vec[i] = 0.0;
    obj->region3D.Xform.center.free_array(vec);
    OMset_int_val(obj->region3D.Top.xform_mode.obj_id(), 1); //Parent
  }
  return true;
}

bool DLV::display_plane::attach_editor(const bool v, const toolkit_obj &t,
				       float matrix[4][4], float translate[3],
				       float centre[3], char message[],
				       const int mlen) const
{
  if (v) {
    OMset_obj_val(t.get_id(), obj->obj.obj_id(), 0);
    OMset_int_val(obj->quad_mesh.DataObject.Obj.xform_mode.obj_id(), 0);
    //Normal
  } else {
    OMset_obj_val(t.get_id(), OMnull_obj, 0);
    // recover transformation
    float (*m)[4] =
      (float (*)[4])obj->quad_mesh.out.xform.mat.ret_array_ptr(OM_GET_ARRAY_RW);
    for (int i = 0; i < 4; i++)
      for (int j = 0; j < 4; j++)
	matrix[i][j] = m[i][j];
    for (int i = 0; i < 4; i++) {
      for (int j = 0; j < 4; j++)
	m[i][j] = 0.0;
      m[i][i] = 1.0;
    }
    obj->quad_mesh.out.xform.mat.free_array(m);
    float *vec =
      (float *)obj->quad_mesh.out.xform.xlate.ret_array_ptr(OM_GET_ARRAY_RW);
    for (int i = 0; i < 3; i++)
      translate[i] = vec[i];
    for (int i = 0; i < 3; i++)
      vec[i] = 0.0;
    obj->quad_mesh.out.xform.xlate.free_array(vec);
    vec =
      (float *)obj->quad_mesh.out.xform.center.ret_array_ptr(OM_GET_ARRAY_RW);
    for (int i = 0; i < 3; i++)
      centre[i] = vec[i];
    for (int i = 0; i < 3; i++)
      vec[i] = 0.0;
    obj->quad_mesh.out.xform.center.free_array(vec);
    OMset_int_val(obj->quad_mesh.DataObject.Obj.xform_mode.obj_id(), 1);
    //Parent
  }
  return true;
}

void DLV::streamlines::update_streamlines(const class drawable_obj *draw,
					  char message[], const int mlen)
{
  OMset_obj_ref(obj->in_probe.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::display_obj::update_data(const render_parent *parent,
				   const class drawable_obj *draw,
				   const string name, 
				   const string xaxis, const string yaxis,
				   const int ngraphs, const bool visible,
				   char message[], const int mlen)
{
  strncpy(message, "BUG: update_data is a dummy function", mlen - 1);
}

void DLV::display_rod1d::update_data(const render_parent *parent,
				    const class drawable_obj *draw,
				    const string name, 
				    const string xaxis, const string yaxis,
				    const int ngraphs, const bool visible,
				    char message[], const int mlen)
{
  attach_data(draw, name, xaxis, yaxis, ngraphs, OMnull_obj,
	      parent->get_name(), visible);    
}

void DLV::display_rod2d::update_data(const render_parent *parent,
				    const class drawable_obj *draw,
				    const string name, 
				    const string xaxis, const string yaxis,
				    const int ngraphs, const bool visible,
				    char message[], const int mlen)
{
  attach_data(draw, name, xaxis, yaxis, ngraphs, OMnull_obj,
	      parent->get_name(), visible);    
}

void DLV::display_obj::turn_off_visible()
{
}

void DLV::display_atom_trajectory::turn_off_visible()
{
  obj->visible = 0;
}

#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    // Don't think we can partially specialise a function?
    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::display_atom_bond_data *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    DLV::display_atom_bond_data *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::display_atom_bond_data(DLV::find_property(p, "Points",
							     false), i, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::display_text_data *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    DLV::display_text_data *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::display_text_data(DLV::find_property(p, "Points",
							false), i, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::display_real_data *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    DLV::display_real_data *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::display_real_data(DLV::find_property(p, "Points",
							false), i, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::display_file_view *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    DLV::display_file_view *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::display_file_view(DLV::find_property(p, "Points",
							false), i, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::display_text_view *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    DLV::display_text_view *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::display_text_view(DLV::find_property(p, "Points",
							false), i, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::display_atom_text_data *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    DLV::display_atom_text_data *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::display_atom_text_data(DLV::find_property(p, "Points",
							     false), i, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::display_atom_vector_data *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    DLV::display_atom_vector_data *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
     ::new(t)DLV::display_atom_vector_data(DLV::find_property(p, "Points",
							      false), i, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::display_atom_trajectory *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    DLV::display_atom_trajectory *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::display_atom_trajectory(DLV::find_property(p, "Points",
							      false), i, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::isosurface *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::isosurface *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::isosurface(DLV::find_property(p, "Volumes", sp), i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::periodic_isosurface *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    DLV::periodic_isosurface *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::periodic_isosurface(DLV::find_property(p, "Volumes", sp),
				       i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::iso_wavefn *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::iso_wavefn *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::iso_wavefn(DLV::find_property(p, "Volumes", sp), i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::iso_bloch *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::iso_bloch *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::iso_bloch(DLV::find_property(p, "Volumes", sp),
			     i, 0, 0, 0, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::cells_obj *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::cells_obj *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::cells_obj(DLV::find_property(p, "Volumes", sp), i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::periodic_cells *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::periodic_cells *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::periodic_cells(DLV::find_property(p, "Volumes", sp),
				  i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::spheres *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();	
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::spheres *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::spheres(DLV::find_property(p, "Volumes", sp), i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::volume_view *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();	
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::volume_view *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::volume_view(DLV::find_property(p, "Volumes", sp),
				 i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::periodic_spheres *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();	
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::periodic_spheres *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::periodic_spheres(DLV::find_property(p, "Volumes", sp),
				    i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::hedgehog *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::hedgehog *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::hedgehog(DLV::find_property(p, "Volumes", sp), i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::periodic_hedgehog *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::periodic_hedgehog *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::periodic_hedgehog(DLV::find_property(p, "Volumes", sp),
				     i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::streamlines *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::streamlines *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::streamlines(DLV::find_property(p, "Volumes", sp),
			       i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::periodic_streamlines *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::periodic_streamlines *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::periodic_streamlines(DLV::find_property(p, "Volumes", sp),
					i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::ag_contours *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::ag_contours *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::ag_contours(DLV::find_property(p, "Planes", sp), i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::periodic_ag_contours *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::periodic_ag_contours *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::periodic_ag_contours(DLV::find_property(p, "Planes", sp),
					i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::contours *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::contours *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::contours(DLV::find_property(p, "Planes", sp), i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::periodic_contours *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::periodic_contours *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::periodic_contours(DLV::find_property(p, "Planes", sp),
				     i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::pn_contours *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::pn_contours *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::pn_contours(DLV::find_property(p, "Planes", sp), i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::periodic_pn_contours *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    DLV::periodic_pn_contours *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::periodic_pn_contours(DLV::find_property(p, "Planes", sp),
					i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::solid_contours *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::solid_contours *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::solid_contours(DLV::find_property(p, "Planes", sp),
				  i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::periodic_solid_contours *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    DLV::periodic_solid_contours *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::periodic_solid_contours(DLV::find_property(p, "Planes", sp),
					   i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::shaded_contours *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::shaded_contours *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::shaded_contours(DLV::find_property(p, "Planes", sp),
				   i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::periodic_shaded_contours *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    DLV::periodic_shaded_contours *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::periodic_shaded_contours(DLV::find_property(p, "Planes",
							       sp), i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::surface_plot *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::surface_plot *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::surface_plot(DLV::find_property(p, "Planes", sp),
				i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::periodic_surface_plot *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::periodic_surface_plot *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::periodic_surface_plot(DLV::find_property(p, "Planes", sp),
					 i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::display_dos *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    DLV::display_dos *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::display_dos(DLV::find_property(p, "Plots", false), i, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::display_band *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    DLV::display_band *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::display_band(DLV::find_property(p, "Plots", false), i, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::display_3D_region *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    DLV::display_3D_region *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::display_3D_region(DLV::find_property(p, "Volumes",
							sp), i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::display_sphere_obj *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    DLV::display_sphere_obj *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::display_sphere_obj(DLV::find_property(p, "Volumes",
							 sp), i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::display_plane *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    DLV::display_plane *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::display_plane(DLV::find_property(p, "Planes", sp),
				 i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::display_line *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    DLV::display_line *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::display_line(DLV::find_property(p, "Lines", sp), i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::display_point *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    DLV::display_point *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::display_point(DLV::find_property(p, "Points", sp),
				 i, sp, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::display_wulff_region *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    DLV::display_wulff_region *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::display_wulff_region(DLV::find_property(p, "Volumes",
							   false), i, "");
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::display_leed_pattern *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    DLV::display_leed_pattern *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::display_leed_pattern(DLV::find_property(p, "Plots",
							   false), i, "");
    }

  }
}

template <class Archive>
void DLV::display_obj::save(Archive &ar, const unsigned int version) const
{
  ar & name;
}

template <class Archive>
void DLV::display_obj::load(Archive &ar, const unsigned int version)
{
  ar & name;
  rendered = false;
}

template <class Archive>
void DLV::display_atom_bond_data::save(Archive &ar,
				       const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i = (int)obj->params.in_viewer;
  ar & i;
  i = (int)obj->params.precision;
  ar & i;
}

template <class Archive>
void DLV::display_atom_bond_data::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i;
  ar & i;
  obj->params.in_viewer = i;
  ar & i;
  obj->params.precision = i;
  obj->visible = 0;
}

template <class Archive>
void DLV::display_text_data::save(Archive &ar,
				  const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
}

template <class Archive>
void DLV::display_text_data::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  obj->visible = 0;
}

template <class Archive>
void DLV::display_real_data::save(Archive &ar,
				  const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i = (int)obj->params.precision;
  ar & i;
  string str = (char *)obj->name;
  ar & str;
}

template <class Archive>
void DLV::display_real_data::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i;
  ar & i;
  obj->params.precision = i;
  string str;
  ar & str;
  obj->name = str.c_str();
  obj->visible = 0;
}

template <class Archive>
void DLV::display_file_view::save(Archive &ar,
				  const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
}

template <class Archive>
void DLV::display_file_view::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  obj->visible = 0;
}

template <class Archive>
void DLV::display_text_view::save(Archive &ar,
				  const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
}

template <class Archive>
void DLV::display_text_view::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  obj->visible = 0;
}

template <class Archive>
void DLV::display_atom_text_data::save(Archive &ar,
				       const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i = (int)obj->params.component;
  ar & i;
  i = (int)obj->params.in_viewer;
  ar & i;
  string str = (char *)obj->name;
  ar & str;
}

template <class Archive>
void DLV::display_atom_text_data::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i;
  ar & i;
  obj->params.component = i;
  ar & i;
  obj->params.in_viewer = i;
  string str;
  ar & str;
  obj->name = str.c_str();
  obj->visible = 0;
}

template <class Archive>
void DLV::display_atom_vector_data::save(Archive &ar,
					 const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
  float f = (float)obj->scale;
  ar & f;
  int i = (int)obj->component;
  ar & i;
}

template <class Archive>
void DLV::display_atom_vector_data::load(Archive &ar,
					 const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  float f;
  ar & f;
  obj->scale = f;
  int i;
  ar & i;
  obj->component = i;
  obj->visible = 0;
}

template <class Archive>
void DLV::display_atom_trajectory::save(Archive &ar,
					const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
  // Todo
}

template <class Archive>
void DLV::display_atom_trajectory::load(Archive &ar,
					const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  obj->visible = 0;
}

template <class Archive>
void DLV::isosurface::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i = (int)obj->IsoParam.iso_component;
  ar & i;
  float f = (float)obj->IsoParam.iso_level;
  ar & f;
  i = (int)obj->IsoParam.color;
  ar & i;
  i = (int)obj->IsoParam.colour_data;
  ar & i;
  //ar & (int)obj->IsoParam.datamap;
  f = (float)obj->IsoParam.min_colour_val;
  ar & f;
  f = (float)obj->IsoParam.max_colour_val;
  ar & f;
  i = (int)obj->IsoParam.complex;
  ar & i;
}

template <class Archive>
void DLV::isosurface::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i;
  ar & i;
  obj->IsoParam.iso_component = i;
  float f;
  ar & f;
  obj->IsoParam.iso_level = f;
  ar & i;
  obj->IsoParam.color = i;
  ar & i;
  colour_comp = i;
  obj->IsoParam.colour_data = i;
  //ar & obj->IsoParam.datamap;
  ar & f;
  obj->IsoParam.min_colour_val = f;
  ar & f;
  obj->IsoParam.max_colour_val = f;
  ar & i;
  obj->IsoParam.complex = i;
  obj->visible = 0;
}

template <class Archive>
void DLV::iso_wavefn::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i = (int)obj->IsoParam.iso_component;
  ar & i;
  float f = (float)obj->IsoParam.iso_level;
  ar & f;
  i = (int)obj->IsoParam.colour_data;
  ar & i;
  i = (int)obj->IsoParam.complex;
  ar & i;
  i = (int)obj->sa;
  ar & i;
  i = (int)obj->sb;
  ar & i;
  i = (int)obj->sc;
  ar & i;
}

template <class Archive>
void DLV::iso_wavefn::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i;
  ar & i;
  obj->IsoParam.iso_component = i;
  float f;
  ar & f;
  obj->IsoParam.iso_level = f;
  ar & i;
  obj->IsoParam.colour_data = i;
  ar & i;
  obj->IsoParam.complex = i;
  ar & i;
  obj->sa = i;
  ar & i;
  obj->sb = i;
  ar & i;
  obj->sc = i;
  obj->visible = 0;
}

template <class Archive>
void DLV::cells_obj::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i = (int)obj->ContourParam.contour_comp;
  ar & i;
  i = (int)obj->ContourParam.ncontours;
  ar & i;
  float f = (float)obj->ContourParam.level_min;
  ar & f;
  f = (float)obj->ContourParam.level_max;
  ar & f;
  i = (int)obj->ContourParam.color;
  ar & i;
  i = (int)obj->ContourParam.map;
  ar & i;
}

template <class Archive>
void DLV::cells_obj::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i;
  ar & i;
  obj->ContourParam.contour_comp = i;
  ar & i;
  obj->ContourParam.ncontours = i;
  float f;
  ar & f;
  obj->ContourParam.level_min = f;
  ar & f;
  obj->ContourParam.level_max = f;
  ar & i;
  obj->ContourParam.color = i;
  ar & i;
  obj->ContourParam.map = i;
  obj->visible = 0;
}

template <class Archive>
void DLV::spheres::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i = (int)obj->param.component;
  ar & i;
  i = (int)obj->param.mag_comp;
  ar & i;
  i = (int)obj->param.map;
  ar & i;
  i = (int)obj->param.mag_map;
  ar & i;
  float f = (float)obj->param.scale;
  ar & f;
}

template <class Archive>
void DLV::spheres::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i;
  ar & i;
  obj->param.component = i;
  ar & i;
  obj->param.mag_comp = i;
  ar & i;
  obj->param.map = i;
  ar & i;
  obj->param.mag_map = i;
  float f;
  ar & f;
  obj->param.scale = f;
  obj->visible = 0;
}

template <class Archive>
void DLV::volume_view::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i = (int)obj->param.surface;
  ar & i;
  i = (int)obj->param.volume;
  ar & i;
  i = (int)obj->param.voxel;
  ar & i;
  i = (int)obj->param.ray_algo;
  ar & i;
  i = (int)obj->param.ray_norm;
  ar & i;
  i = (int)obj->param.fat_ray;
  ar & i;
  float f = (float)obj->param.absorb;
  ar & f;
  f = (float)obj->param.emit;
  ar & f;
  i = (int)obj->param.alt;
  ar & i;
}

template <class Archive>
void DLV::volume_view::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i;
  ar & i;
  obj->param.surface = i;
  ar & i;
  obj->param.volume = i;
  ar & i;
  obj->param.voxel = i;
  ar & i;
  obj->param.ray_algo = i;
  ar & i;
  obj->param.ray_norm = i;
  ar & i;
  obj->param.fat_ray = i;
  float f;
  ar & f;
  obj->param.absorb = f;
  ar & f;
  obj->param.emit = f;
  ar & i;
  obj->param.alt = i;
  ar & i;
  obj->visible = 0;
}

template <class Archive>
void DLV::hedgehog::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i = (int)obj->component;
  ar & i;
  float f = (float)obj->scale;
  ar & f;
}

template <class Archive>
void DLV::hedgehog::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i;
  ar & i;
  obj->component = i;
  float f;
  ar & f;
  obj->scale = f;
  obj->visible = 0;
}

template <class Archive>
void DLV::streamlines::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i;
  i = (int)obj->StreamParam.component;
  ar & i;
  i = (int)obj->StreamParam.order;
  ar & i;
  i = (int)obj->StreamParam.forw_back;
  ar & i;
  i = (int)obj->StreamParam.nseg;
  ar & i;
  i = (int)obj->StreamParam.max_seg;
  ar & i;
  i = (int)obj->StreamParam.color;
  ar & i;
  i = (int)obj->StreamParam.plane;
  ar & i;
  float f;
  f = (float)obj->StreamParam.min_vel;
  ar & f;
}

template <class Archive>
void DLV::streamlines::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i;
  ar & i;
  obj->StreamParam.component = i;
  ar & i;
  obj->StreamParam.order = i;
  ar & i;
  obj->StreamParam.forw_back = i;
  ar & i;
  obj->StreamParam.nseg = i;
  ar & i;
  obj->StreamParam.max_seg = i;
  ar & i;
  obj->StreamParam.color = i;
  ar & i;
  obj->StreamParam.plane = i;
  float f;
  ar & f;
  obj->StreamParam.min_vel = f;
  obj->visible = 0;
}

template <class Archive>
void DLV::ag_contours::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i = (int)obj->IsoParam.contour_comp;
  ar & i;
  i = (int)obj->IsoParam.ncontours;
  ar & i;
  float f = (float)obj->IsoParam.level_min;
  ar & f;
  f = (float)obj->IsoParam.level_max;
  ar & f;
  i = (int)obj->IsoParam.color;
  ar & i;
  i = (int)obj->IsoParam.log;
  ar & i;
  string n = (char *)obj->name;
  ar & n;
  n = (char *)obj->contour2D.DLV2Dscene.Model_Name;
  ar & n;
  n = (char *)obj->contour2D.Font;
  ar & n;
}

template <class Archive>
void DLV::ag_contours::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i;
  ar & i;
  obj->IsoParam.contour_comp = i;
  ar & i;
  obj->IsoParam.ncontours = i;
  float f;
  ar & f;
  obj->IsoParam.level_min = f;
  ar & f;
  obj->IsoParam.level_max = f;
  ar & i;
  obj->IsoParam.color = i;
  ar & i;
  obj->IsoParam.log = i;
  string n;
  ar & n;
  obj->name = n.c_str();
  ar & n;
  obj->contour2D.DLV2Dscene.Model_Name = n.c_str();
  if (version > 0) {
    ar & n;
    obj->contour2D.Font = n.c_str();
  }
  obj->visible3D = 0;
  obj->visible2D = 0;
  DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
  set_background(p);
}

template <class Archive>
void DLV::contours::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i = (int)obj->IsoParam.contour_comp;
  ar & i;
  i = (int)obj->IsoParam.ncontours;
  ar & i;
  float f = (float)obj->IsoParam.level_min;
  ar & f;
  f = (float)obj->IsoParam.level_max;
  ar & f;
  i = (int)obj->IsoParam.color;
  ar & i;
  i = (int)obj->IsoParam.log;
  ar & i;
  string n = (char *)obj->name;
  ar & n;
  n = (char *)obj->contour2D.DLV2Dscene.Model_Name;
  ar & n;
}

template <class Archive>
void DLV::contours::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i;
  ar & i;
  obj->IsoParam.contour_comp = i;
  ar & i;
  obj->IsoParam.ncontours = i;
  float f;
  ar & f;
  obj->IsoParam.level_min = f;
  ar & f;
  obj->IsoParam.level_max = f;
  ar & i;
  obj->IsoParam.color = i;
  ar & i;
  obj->IsoParam.log = i;
  string n;
  ar & n;
  obj->name = n.c_str();
  ar & n;
  obj->contour2D.DLV2Dscene.Model_Name = n.c_str();
  obj->visible3D = 0;
  obj->visible2D = 0;
  DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
  set_background(p);
}

template <class Archive>
void DLV::pn_contours::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i = (int)obj->IsoParam.contour_comp;
  ar & i;
  i = (int)obj->IsoParam.ncontours;
  ar & i;
  float f = (float)obj->IsoParam.level_min;
  ar & f;
  f = (float)obj->IsoParam.level_max;
  ar & f;
  i = (int)obj->IsoParam.color;
  ar & i;
  i = (int)obj->IsoParam.log;
  ar & i;
  string n = (char *)obj->name;
  ar & n;
  n = (char *)obj->contour2D.DLV2Dscene.Model_Name;
  ar & n;
}

template <class Archive>
void DLV::pn_contours::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i;
  ar & i;
  obj->IsoParam.contour_comp = i;
  ar & i;
  obj->IsoParam.ncontours = i;
  float f;
  ar & f;
  obj->IsoParam.level_min = f;
  ar & f;
  obj->IsoParam.level_max = f;
  ar & i;
  obj->IsoParam.color = i;
  ar & i;
  obj->IsoParam.log = i;
  string n;
  ar & n;
  obj->name = n.c_str();
  ar & n;
  obj->contour2D.DLV2Dscene.Model_Name = n.c_str();
  obj->visible3D = 0;
  obj->visible2D = 0;
  DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
  set_background(p);
}

template <class Archive>
void DLV::solid_contours::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i = (int)obj->ContourParam.contour_comp;
  ar & i;
  i = (int)obj->ContourParam.ncontours;
  ar & i;
  float f = (float)obj->ContourParam.level_min;
  ar & f;
  f = (float)obj->ContourParam.level_max;
  ar & f;
  i = (int)obj->ContourParam.color;
  ar & i;
  i = (int)obj->ContourParam.colour_data;
  ar & i;
  string n = (char *)obj->name;
  ar & n;
  n = (char *)obj->contour2D.DLV2Dscene.Model_Name;
  ar & n;
}

template <class Archive>
void DLV::solid_contours::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i;
  ar & i;
  obj->ContourParam.contour_comp = i;
  ar & i;
  obj->ContourParam.ncontours = i;
  float f;
  ar & f;
  obj->ContourParam.level_min = f;
  ar & f;
  obj->ContourParam.level_max = f;
  ar & i;
  obj->ContourParam.color = i;
  ar & i;
  obj->ContourParam.colour_data = i;
  string n;
  ar & n;
  obj->name = n.c_str();
  ar & n;
  obj->contour2D.DLV2Dscene.Model_Name = n.c_str();
  obj->visible3D = 0;
  obj->visible2D = 0;
  DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
  set_background(p);
}

template <class Archive>
void DLV::shaded_contours::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i = (int)obj->ContourParam.contour_comp;
  ar & i;
  i = (int)obj->ContourParam.ncontours;
  ar & i;
  float f = (float)obj->ContourParam.level_min;
  ar & f;
  f = (float)obj->ContourParam.level_max;
  ar & f;
  i = (int)obj->ContourParam.color;
  ar & i;
  i = (int)obj->ContourParam.log;
  ar & i;
  string n = (char *)obj->name;
  ar & n;
  n = (char *)obj->contour2D.DLV2Dscene.Model_Name;
  ar & n;
}

template <class Archive>
void DLV::shaded_contours::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i;
  ar & i;
  obj->ContourParam.contour_comp = i;
  ar & i;
  obj->ContourParam.ncontours = i;
  float f;
  ar & f;
  obj->ContourParam.level_min = f;
  ar & f;
  obj->ContourParam.level_max = f;
  ar & i;
  obj->ContourParam.color = i;
  ar & i;
  obj->ContourParam.log = i;
  string n;
  ar & n;
  obj->name = n.c_str();
  ar & n;
  obj->contour2D.DLV2Dscene.Model_Name = n.c_str();
  obj->visible3D = 0;
  obj->visible2D = 0;
  DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
  set_background(p);
}

template <class Archive>
void DLV::surface_plot::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i = (int)obj->SurfPlotParam.component;
  ar & i;
  float f = (float)obj->SurfPlotParam.scale;
  ar & f;
  f = (float)obj->SurfPlotParam.offset;
  ar & f;
  string n = (char *)obj->name;
  ar & n;
}

template <class Archive>
void DLV::surface_plot::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i;
  ar & i;
  obj->SurfPlotParam.component = i;
  float f;
  ar & f;
  obj->SurfPlotParam.scale = f;
  ar & f;
  obj->SurfPlotParam.offset = f;
  string n;
  ar & n;
  obj->name = n.c_str();
  obj->visible = 0;
}

template <class Archive>
void DLV::plot_obj::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i = (int)obj->parameters.plot_select;
  ar & i;
  string str = (char *)obj->title;
  ar & str;
  str = (char *)obj->xlabel;
  ar & str;
  str = (char *)obj->ylabel;
  ar & str;
  str = (char *)obj->DLV2Dscene.Model_Name;
  ar & str;
  float f = (float)obj->parameters.width;
  ar & f;
  str = (char *)obj->Font;
  ar & str;
  // Todo plot_vis?
}

template <class Archive>
void DLV::plot_obj::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i;
  ar & i;
  obj->parameters.plot_select = i;
  string str;
  ar & str;
  obj->title = str.c_str();
  ar & str;
  obj->xlabel = str.c_str();
  ar & str;
  obj->ylabel = str.c_str();
  ar & str;
  obj->DLV2Dscene.Model_Name = str.c_str();
  if (version > 0) {
    float f;
    ar & f;
    obj->parameters.width = f;
    if (version > 1) {
      ar & str;
      obj->Font = str.c_str();
    }
    // Todo plot_vis?
  }
  obj->visible = 0;
  DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
  set_background(p);
}

template <class Archive>
void DLV::display_3D_region::save(Archive &ar,
				  const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i = (int)obj->params.lines;
  ar & i;
  i = (int)obj->params.planes;
  ar & i;
  float f = (float)obj->params.red;
  ar & f;
  f = (float)obj->params.green;
  ar & f;
  f = (float)obj->params.blue;
  ar & f;
  f = (float)obj->params.opacity;
  ar & f;
}

template <class Archive>
void DLV::display_3D_region::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i;
  ar & i;
  obj->params.lines = i;
  ar & i;
  obj->params.planes = i;
  float f;
  ar & f;
  obj->params.red = f;
  ar & f;
  obj->params.green = f;
  ar & f;
  obj->params.blue = f;
  ar & f;
  obj->params.opacity = f;
  obj->visible = 0;
}

template <class Archive>
void DLV::display_sphere_obj::save(Archive &ar,
				   const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i = (int)obj->params.subdivisions;
  ar & i;
  //i = (int)obj->params.planes;
  //ar & i;
  float f = (float)obj->params.red;
  ar & f;
  f = (float)obj->params.green;
  ar & f;
  f = (float)obj->params.blue;
  ar & f;
  f = (float)obj->params.opacity;
  ar & f;
}

template <class Archive>
void DLV::display_sphere_obj::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i;
  ar & i;
  obj->params.subdivisions = i;
  //ar & i;
  //obj->params.planes = i;
  float f;
  ar & f;
  obj->params.red = f;
  ar & f;
  obj->params.green = f;
  ar & f;
  obj->params.blue = f;
  ar & f;
  obj->params.opacity = f;
  obj->visible = 0;
}

template <class Archive>
void DLV::display_plane::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
  float f = (float)obj->params.red;
  ar & f;
  f = (float)obj->params.green;
  ar & f;
  f = (float)obj->params.blue;
  ar & f;
  f = (float)obj->params.opacity;
  ar & f;
}

template <class Archive>
void DLV::display_plane::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  float f;
  ar & f;
  obj->params.red = f;
  ar & f;
  obj->params.green = f;
  ar & f;
  obj->params.blue = f;
  ar & f;
  obj->params.opacity = f;
  obj->visible = 0;
}

template <class Archive>
void DLV::display_line::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
  float f = (float)obj->params.red;
  ar & f;
  f = (float)obj->params.green;
  ar & f;
  f = (float)obj->params.blue;
  ar & f;
}

template <class Archive>
void DLV::display_line::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  float f;
  ar & f;
  obj->params.red = f;
  ar & f;
  obj->params.green = f;
  ar & f;
  obj->params.blue = f;
  obj->visible = 0;
}

template <class Archive>
void DLV::display_point::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
  float f = (float)obj->params.red;
  ar & f;
  f = (float)obj->params.green;
  ar & f;
  f = (float)obj->params.blue;
  ar & f;
}

template <class Archive>
void DLV::display_point::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  float f;
  ar & f;
  obj->params.red = f;
  ar & f;
  obj->params.green = f;
  ar & f;
  obj->params.blue = f;
  obj->visible = 0;
}

template <class Archive>
void DLV::display_wulff_region::save(Archive &ar,
				     const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i = (int)obj->params.lines;
  ar & i;
  i = (int)obj->params.planes;
  ar & i;
  float f = (float)obj->params.red;
  ar & f;
  f = (float)obj->params.green;
  ar & f;
  f = (float)obj->params.blue;
  ar & f;
  f = (float)obj->params.opacity;
  ar & f;
}

template <class Archive>
void DLV::display_wulff_region::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i;
  ar & i;
  obj->params.lines = i;
  ar & i;
  obj->params.planes = i;
  float f;
  ar & f;
  obj->params.red = f;
  ar & f;
  obj->params.green = f;
  ar & f;
  obj->params.blue = f;
  ar & f;
  obj->params.opacity = f;
  obj->visible = 0;
}

template <class Archive>
void DLV::display_leed_pattern::save(Archive &ar,
				     const unsigned int version) const
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i = (int)obj->params.domains;
  ar & i;
}

template <class Archive>
void DLV::display_leed_pattern::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<display_obj>(*this);
  int i;
  ar & i;
  obj->params.domains = i;
  obj->visible = 0;
}

BOOST_CLASS_VERSION(DLV::plot_obj, 2)
BOOST_CLASS_VERSION(DLV::ag_contours, 1)

BOOST_CLASS_EXPORT_GUID(DLV::display_atom_bond_data,
			"DLV::display_atom_bond_data")
BOOST_CLASS_EXPORT_GUID(DLV::display_text_data, "DLV::display_text_data")
BOOST_CLASS_EXPORT_GUID(DLV::display_real_data, "DLV::display_real_data")
BOOST_CLASS_EXPORT_GUID(DLV::display_file_view, "DLV::display_file_view")
BOOST_CLASS_EXPORT_GUID(DLV::display_text_view, "DLV::display_text_view")
BOOST_CLASS_EXPORT_GUID(DLV::display_atom_text_data,
			"DLV::display_atom_text_data")
BOOST_CLASS_EXPORT_GUID(DLV::display_atom_vector_data,
			"DLV::display_atom_vector_data")
BOOST_CLASS_EXPORT_GUID(DLV::display_atom_trajectory,
			"DLV::display_atom_trajectory")
BOOST_CLASS_EXPORT_GUID(DLV::isosurface, "DLV::isosurface")
BOOST_CLASS_EXPORT_GUID(DLV::periodic_isosurface, "DLV::periodic_isosurface")
BOOST_CLASS_EXPORT_GUID(DLV::iso_wavefn, "DLV::iso_wavefn")
BOOST_CLASS_EXPORT_GUID(DLV::iso_bloch, "DLV::iso_bloch")
BOOST_CLASS_EXPORT_GUID(DLV::cells_obj, "DLV::cells_obj")
BOOST_CLASS_EXPORT_GUID(DLV::periodic_cells, "DLV::periodic_cells")
BOOST_CLASS_EXPORT_GUID(DLV::spheres, "DLV::spheres")
BOOST_CLASS_EXPORT_GUID(DLV::periodic_spheres, "DLV::periodic_spheres")
BOOST_CLASS_EXPORT_GUID(DLV::hedgehog, "DLV::hedgehog")
BOOST_CLASS_EXPORT_GUID(DLV::periodic_hedgehog, "DLV::periodic_hedgehog")
BOOST_CLASS_EXPORT_GUID(DLV::streamlines, "DLV::streamlines")
BOOST_CLASS_EXPORT_GUID(DLV::periodic_streamlines, "DLV::periodic_streamlines")
BOOST_CLASS_EXPORT_GUID(DLV::ag_contours, "DLV::ag_contours")
BOOST_CLASS_EXPORT_GUID(DLV::periodic_ag_contours, "DLV::periodic_ag_contours")
BOOST_CLASS_EXPORT_GUID(DLV::contours, "DLV::contours")
BOOST_CLASS_EXPORT_GUID(DLV::periodic_contours, "DLV::periodic_contours")
BOOST_CLASS_EXPORT_GUID(DLV::pn_contours, "DLV::pn_contours")
BOOST_CLASS_EXPORT_GUID(DLV::periodic_pn_contours, "DLV::periodic_pn_contours")
BOOST_CLASS_EXPORT_GUID(DLV::solid_contours, "DLV::solid_contours")
BOOST_CLASS_EXPORT_GUID(DLV::periodic_solid_contours,
			"DLV::periodic_solid_contours")
BOOST_CLASS_EXPORT_GUID(DLV::shaded_contours, "DLV::shaded_contours")
BOOST_CLASS_EXPORT_GUID(DLV::periodic_shaded_contours,
			"DLV::periodic_shaded_contours")
BOOST_CLASS_EXPORT_GUID(DLV::surface_plot, "DLV::surface_plot")
BOOST_CLASS_EXPORT_GUID(DLV::periodic_surface_plot,
			"DLV::periodic_surface_plot")
BOOST_CLASS_EXPORT_GUID(DLV::display_dos, "DLV::display_dos")
BOOST_CLASS_EXPORT_GUID(DLV::display_band, "DLV::display_band")
BOOST_CLASS_EXPORT_GUID(DLV::display_3D_region, "DLV::display_3D_region")
BOOST_CLASS_EXPORT_GUID(DLV::display_sphere_obj, "DLV::display_sphere_obj")
BOOST_CLASS_EXPORT_GUID(DLV::display_plane, "DLV::display_plane")
BOOST_CLASS_EXPORT_GUID(DLV::display_line, "DLV::display_line")
BOOST_CLASS_EXPORT_GUID(DLV::display_point, "DLV::display_point")
BOOST_CLASS_EXPORT_GUID(DLV::display_wulff_region, "DLV::display_wulff_region")
BOOST_CLASS_EXPORT_GUID(DLV::display_leed_pattern, "DLV::display_leed_pattern")

#endif // DLV_USES_SERIALIZE
