
#include "avs/omx.hxx"
#include "../graphics/excurv.hxx"

void EXCURV::set_parameters(const char data[])
{
  static OMobj_id str = OMnull_obj;
  if (OMis_null_obj(str))
    str = OMfind_str_subobj(OMinst_obj,
                            "DLV.calculations.EXCURV.execute.refine_data",
                            OM_OBJ_RW);
  OMset_str_val(str, data);
}

void EXCURV::set_error(const char message[], const bool expt, const bool pot)
{
  static OMobj_id errstr = OMnull_obj;
  static OMobj_id expterr = OMnull_obj;
  static OMobj_id poterr = OMnull_obj;
  if (OMis_null_obj(errstr)) {
    OMobj_id obj = OMfind_str_subobj(OMinst_obj,
				     "DLV.calculations.EXCURV.execute",
				     OM_OBJ_RW);

    errstr = OMfind_str_subobj(obj, "error_string", OM_OBJ_RW);
    expterr = OMfind_str_subobj(obj, "expt_error", OM_OBJ_RW);
    poterr = OMfind_str_subobj(obj, "pot_error", OM_OBJ_RW);
  }
  if (expt)
    OMset_int_val(expterr, 1);
  if (pot)
    OMset_int_val(poterr, 1);
  OMset_str_val(errstr, message);
}

void EXCURV::clear_error(const bool expt, const bool pot)
{
  static OMobj_id errstr = OMnull_obj;
  static OMobj_id expterr = OMnull_obj;
  static OMobj_id poterr = OMnull_obj;
  if (OMis_null_obj(errstr)) {
    OMobj_id obj = OMfind_str_subobj(OMinst_obj,
				     "DLV.calculations.EXCURV.execute",
				     OM_OBJ_RW);

    errstr = OMfind_str_subobj(obj, "error_string", OM_OBJ_RW);
    expterr = OMfind_str_subobj(obj, "expt_error", OM_OBJ_RW);
    poterr = OMfind_str_subobj(obj, "pot_error", OM_OBJ_RW);
  }
  if (expt)
    OMset_int_val(expterr, 1);
  if (pot)
    OMset_int_val(poterr, 1);
  OMset_str_val(errstr, "");
}
