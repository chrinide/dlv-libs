
#ifdef ENABLE_DLV_GRAPHICS
#  if defined(DLV_USES_VTK_GRAPHICS)
#    error "This graphics interface should not be built when using the VTK graphics library"
#  elif !defined(DLV_USES_AVS_GRAPHICS)
#    error "DLV_USES_AVS_GRAPHICS should also be defined when building this graphics interface"
#  endif // Check for AVS/VTK
#else
#  error "ENABLE DLV_GRAPHICS and DLV_USES_AVS_GRAPHICS should be defined when building this graphics interface"
#endif // GRAPHICS check

#include <cmath>
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#include "../graphics/toolkit_obj.hxx"
#include "../graphics/render_base.hxx"
#include "avs/src/express/rbase.hxx"
#include "avs/src/express/scenes.hxx"

#ifdef WIN32
#  define snprintf _snprintf
#endif // WIN32

template <> DLV::toolkit_obj *DLV::render_parent::parent = 0;
template <> DLV::render_parent *DLV::render_parent::serialize_obj = 0;

template <class scene_t, class parent_t>
DLV::render_parent_templ<scene_t, parent_t>::render_parent_templ()
{
  if (parent == 0) {
    OMobj_id id = OMfind_str_subobj(OMinst_obj, "DLV.Display_Objs", OM_OBJ_RW);
    parent = new toolkit_obj(id);
  }
}

template <class scene_t, class parent_t>
DLV::render_parent_templ<scene_t, parent_t>::~render_parent_templ()
{
}

template <class scene_t, class parent_t, class atoms_t>
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::~render_atoms_templ()
{
  // Todo delete obj;
}

template <class scene_t, class parent_t, class outline_t>
DLV::render_outline_templ<scene_t, parent_t, outline_t>::~render_outline_templ()
{
  // Todo delete obj;
}

template <class scene_t, class parent_t, class shells_t>
DLV::render_shells_templ<scene_t, parent_t, shells_t>::~render_shells_templ()
{
  // Todo delete obj;
}

template <class scene_t, class parent_t, class atoms_t>
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::render_atoms_templ(const char name[])
{
  obj = new atoms_t(render_parent_templ<scene_t, parent_t>::get_parent()->get_id(), name);
  obj->model_name = name;
  // Copy the default values for the common and render settings.
  OMset_obj_val(obj->r_space.common_data.obj_id(),
		OMfind_str_subobj(render_parent_templ<scene_t, parent_t>::get_parent()->get_id(),
				  "default_data.common", OM_OBJ_RD), 0);
  OMset_obj_val(obj->r_space.props.obj_id(),
		OMfind_str_subobj(render_parent_templ<scene_t, parent_t>::get_parent()->get_id(),
				  "default_data.props", OM_OBJ_RD), 0);
  OMset_obj_val(obj->background.obj_id(),
		OMfind_str_subobj(render_parent_templ<scene_t, parent_t>::get_parent()->get_id(),
				  "default_data.background", OM_OBJ_RD), 0);
}

template <class scene_t, class parent_t, class outline_t>
DLV::render_outline_templ<scene_t, parent_t, outline_t>::render_outline_templ(const char name[])
{
  obj = new outline_t(render_parent_templ<scene_t, parent_t>::get_parent()->get_id(), name);
  obj->model_name = name;
  OMset_obj_val(obj->background.obj_id(),
		OMfind_str_subobj(render_parent_templ<scene_t, parent_t>::get_parent()->get_id(),
				  "default_data.background", OM_OBJ_RD), 0);
  OMset_obj_val(obj->r_space.common_data.obj_id(),
		OMfind_str_subobj(render_parent_templ<scene_t, parent_t>::get_parent()->get_id(),
				  "default_data.common", OM_OBJ_RD), 0);
}

template <class scene_t, class parent_t, class shells_t>
DLV::render_shells_templ<scene_t, parent_t, shells_t>::render_shells_templ(const char name[])
{
  obj = new shells_t(render_parent_templ<scene_t, parent_t>::get_parent()->get_id(), name);
  obj->model_name = name;
  OMset_obj_val(obj->background.obj_id(),
		OMfind_str_subobj(render_parent_templ<scene_t, parent_t>::get_parent()->get_id(),
				  "default_data.background", OM_OBJ_RD), 0);
}

template <class scene_t, class parent_t>
char *DLV::render_parent_templ<scene_t, parent_t>::get_name() const
{
  parent_t *obj = get_object();
  return (char *)obj->model_name;
}

template <class scene_t, class parent_t>
DLV::toolkit_obj *DLV::render_parent_templ<scene_t, parent_t>::get_parent() const
{
  return parent;
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::get_background(float c[3]) const
{
  parent_t *obj = get_object();
  c[0] = (float) obj->background.red;
  c[1] = (float) obj->background.green;
  c[2] = (float) obj->background.blue;
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::get_inverted_background(float c[3]) const
{
  parent_t *obj = get_object();
  c[0] = (float) obj->background.red;
  c[1] = (float) obj->background.green;
  c[2] = (float) obj->background.blue;
  const float tol = 0.02;
  if (std::abs(c[0] - c[1]) < tol and std::abs(c[0] - c[2]) < tol) {
    // probably a grey
    if (c[0] > 0.5 or c[1] > 0.5 or c[2] > 0.5) {
      // light background so use black
      c[0] = 0.0;
      c[1] = 0.0;
      c[2] = 0.0;
    } else {
      c[0] = 1.0;
      c[1] = 1.0;
      c[2] = 1.0;
    }
  } else {
    //coloured background so invert
    c[0] = 1.0 - c[0];
    c[1] = 1.0 - c[1];
    c[2] = 1.0 - c[2];
  }
}

template <class scene_t, class parent_t, class atoms_t>
atoms_t *DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::get_atom_obj() const
{
  return obj;
}

template <class scene_t, class parent_t, class atoms_t>
parent_t *DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::get_object() const
{
  return obj;
}

template <class scene_t, class parent_t, class outline_t>
parent_t *DLV::render_outline_templ<scene_t, parent_t, outline_t>::get_object() const
{
  return obj;
}

template <class scene_t, class parent_t, class shells_t>
parent_t *DLV::render_shells_templ<scene_t, parent_t, shells_t>::get_object() const
{
  return obj;
}

template <class scene_t, class parent_t> DLVreturn_type
DLV::render_parent_templ<scene_t, parent_t>::attach_k3D(scene_t *view)
{
  // BUG
  return DLV_ERROR;
}

template <class scene_t, class parent_t, class atoms_t> DLVreturn_type
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::attach_r3D(scene_t *view)
{
  //int ok = OMset_obj_ref(view->Top.Xform, obj->Xform.obj_id(), 0);
  int ok = OMset_obj_ref(view->Top.Xform.mat.obj_id(),
			 obj->Xform.mat.obj_id(), 0);
  if (ok == OM_STAT_SUCCESS) {
    ok = OMset_obj_ref(view->Top.Top.props, obj->Props.obj_id(), 0);
    if (ok == OM_STAT_SUCCESS) {
      ok = OMset_obj_ref(view->View.View.back_col, obj->back_col.obj_id(), 0);
      if (ok == OM_STAT_SUCCESS) {
	ok = OMset_obj_ref(view->Top.child_objs, obj->r_obj.obj_id(), 0);
	if (ok == OM_STAT_SUCCESS) {
	  ok = OMset_obj_val(view->edit_obj, obj->r_edit_obj.obj_id(), 0);
	  if (ok == OM_STAT_SUCCESS) {
	    OMobj_id a = view->View.ViewUI.PickInteractor.state.obj_id();
	    OMobj_id b = obj->r_space.event.obj_id();
	    ok = OMset_obj_ref(a, b, 0);
	  }
	}
      }
    }
  }
  if (ok == OM_STAT_SUCCESS)
    return DLV_OK;
  else
    return DLV_ERROR;
}

template <class scene_t, class parent_t, class atoms_t> DLVreturn_type
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::attach_k3D(scene_t *view)
{
  //int ok = OMset_obj_ref(view->Top.Xform, obj->Xform.obj_id(), 0);
  int ok = OMset_obj_ref(view->Top.Xform.mat.obj_id(),
			 obj->Xform.mat.obj_id(), 0);
  if (ok == OM_STAT_SUCCESS) {
    ok = OMset_obj_ref(view->Top.Top.props, obj->Props.obj_id(), 0);
    if (ok == OM_STAT_SUCCESS) {
      ok = OMset_obj_ref(view->View.View.back_col, obj->back_col.obj_id(), 0);
      if (ok == OM_STAT_SUCCESS) {
	ok = OMset_obj_ref(view->Top.child_objs, obj->k_obj.obj_id(), 0);
	if (ok == OM_STAT_SUCCESS)
	  ok = OMset_obj_val(view->edit_obj, obj->k_edit_obj.obj_id(), 0);
      }
    }
  }
  if (ok == OM_STAT_SUCCESS)
    return DLV_OK;
  else
    return DLV_ERROR;
}

template <class scene_t, class parent_t> DLVreturn_type
DLV::render_parent_templ<scene_t, parent_t>::detach_k3D(scene_t *view)
{
  // BUG
  return DLV_ERROR;
}

template <class scene_t, class parent_t, class atoms_t> DLVreturn_type
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::detach_r3D(scene_t *view)
{
  int ok = OMdel_obj_ref(view->Top.child_objs, obj->r_obj.obj_id(), 0);
  if (ok == OM_STAT_SUCCESS) {
    ok = OMdel_obj_ref(view->View.View.back_col, obj->back_col.obj_id(), 0);
    if (ok == OM_STAT_SUCCESS) {
      ok = OMdel_obj_ref(view->Top.Top.props, obj->Props.obj_id(), 0);
      if (ok == OM_STAT_SUCCESS) {
	ok = OMdel_obj_ref(view->Top.Xform.mat.obj_id(),
			   obj->Xform.mat.obj_id(), 0);
	if (ok == OM_STAT_SUCCESS) {
	  OMobj_id a = view->View.ViewUI.PickInteractor.state.obj_id();
	  OMobj_id b = obj->r_space.event.obj_id();
	  ok = OMdel_obj_ref(a, b, 0);
	}
      }
    }
  }
  if (ok == OM_STAT_SUCCESS)
    return DLV_OK;
  else
    return DLV_ERROR;
}

template <class scene_t, class parent_t, class atoms_t> DLVreturn_type
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::detach_k3D(scene_t *view)
{
  int ok = OMdel_obj_ref(view->Top.child_objs, obj->k_obj.obj_id(), 0);
  if (ok == OM_STAT_SUCCESS) {
    ok = OMdel_obj_ref(view->View.View.back_col, obj->back_col.obj_id(), 0);
    if (ok == OM_STAT_SUCCESS) {
      ok = OMdel_obj_ref(view->Top.Top.props, obj->Props.obj_id(), 0);
      if (ok == OM_STAT_SUCCESS)
	ok = OMdel_obj_ref(view->Top.Xform.mat.obj_id(),
			   obj->Xform.mat.obj_id(), 0);
    }
  }
  if (ok == OM_STAT_SUCCESS)
    return DLV_OK;
  else
    return DLV_ERROR;
}

// Todo - some of this is common to render_atoms
template <class scene_t, class parent_t, class outline_t> DLVreturn_type
DLV::render_outline_templ<scene_t, parent_t, outline_t>::attach_r3D(scene_t *view)
{
  int ok = OMset_obj_ref(view->Top.Xform.mat.obj_id(),
			 obj->Xform.mat.obj_id(), 0);
  if (ok == OM_STAT_SUCCESS) {
    ok = OMset_obj_ref(view->Top.Top.props, obj->Props.obj_id(), 0);
    if (ok == OM_STAT_SUCCESS) {
      ok = OMset_obj_ref(view->View.View.back_col, obj->back_col.obj_id(), 0);
      if (ok == OM_STAT_SUCCESS) {
	ok = OMset_obj_ref(view->Top.child_objs, obj->r_obj.obj_id(), 0);
	if (ok == OM_STAT_SUCCESS)
	  ok = OMset_obj_val(view->edit_obj, obj->r_edit_obj.obj_id(), 0);
      }
    }
  }
  if (ok == OM_STAT_SUCCESS)
    return DLV_OK;
  else
    return DLV_ERROR;
}

template <class scene_t, class parent_t, class outline_t> DLVreturn_type
DLV::render_outline_templ<scene_t, parent_t, outline_t>::detach_r3D(scene_t *view)
{
  int ok = OMdel_obj_ref(view->Top.child_objs, obj->r_obj.obj_id(), 0);
  if (ok == OM_STAT_SUCCESS) {
    ok = OMset_obj_ref(view->View.View.back_col, obj->back_col.obj_id(), 0);
    if (ok == OM_STAT_SUCCESS) {
      ok = OMdel_obj_ref(view->Top.Top.props, obj->Props.obj_id(), 0);
      if (ok == OM_STAT_SUCCESS)
	ok = OMdel_obj_ref(view->Top.Xform.mat, obj->Xform.mat.obj_id(), 0);
    }
  }
  if (ok == OM_STAT_SUCCESS)
    return DLV_OK;
  else
    return DLV_ERROR;
}

template <class scene_t, class parent_t, class shells_t> DLVreturn_type
DLV::render_shells_templ<scene_t, parent_t, shells_t>::attach_r3D(scene_t *view)
{
  int ok = OMset_obj_ref(view->Top.Xform.mat, obj->Xform.mat.obj_id(), 0);
  if (ok == OM_STAT_SUCCESS) {
    ok = OMset_obj_ref(view->Top.Top.props, obj->Props.obj_id(), 0);
    if (ok == OM_STAT_SUCCESS) {
      ok = OMset_obj_ref(view->View.View.back_col, obj->back_col.obj_id(), 0);
      if (ok == OM_STAT_SUCCESS) {
	ok = OMset_obj_ref(view->Top.child_objs, obj->r_obj.obj_id(), 0);
	if (ok == OM_STAT_SUCCESS)
	  ok = OMset_obj_val(view->edit_obj, obj->r_edit_obj.obj_id(), 0);
      }
    }
  }
  if (ok == OM_STAT_SUCCESS)
    return DLV_OK;
  else
    return DLV_ERROR;
}

template <class scene_t, class parent_t, class shells_t> DLVreturn_type
DLV::render_shells_templ<scene_t, parent_t, shells_t>::detach_r3D(scene_t *view)
{
  int ok = OMdel_obj_ref(view->Top.child_objs, obj->r_obj.obj_id(), 0);
  if (ok == OM_STAT_SUCCESS) {
    ok = OMset_obj_ref(view->View.View.back_col, obj->back_col.obj_id(), 0);
    if (ok == OM_STAT_SUCCESS) {
      ok = OMdel_obj_ref(view->Top.Top.props, obj->Props.obj_id(), 0);
      if (ok == OM_STAT_SUCCESS)
	ok = OMdel_obj_ref(view->Top.Xform.mat, obj->Xform.mat.obj_id(), 0);
    }
  }
  if (ok == OM_STAT_SUCCESS)
    return DLV_OK;
  else
    return DLV_ERROR;
}

template <class scene_t, class parent_t, class atoms_t>
DLV::toolkit_obj DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::get_rspace() const
{
  return toolkit_obj(obj->r_space.obj_id());
}

template <class scene_t, class parent_t, class atoms_t>
DLV::toolkit_obj DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::get_kspace() const
{
  return toolkit_obj(obj->k_space.obj_id());
}

template <class scene_t, class parent_t, class outline_t>
DLV::toolkit_obj DLV::render_outline_templ<scene_t, parent_t, outline_t>::get_rspace() const
{
  return toolkit_obj(obj->r_space.obj_id());
}

template <class scene_t, class parent_t, class outline_t>
DLV::toolkit_obj DLV::render_outline_templ<scene_t, parent_t, outline_t>::get_kspace() const
{
  return toolkit_obj();
}

template <class scene_t, class parent_t, class shells_t>
DLV::toolkit_obj DLV::render_shells_templ<scene_t, parent_t, shells_t>::get_rspace() const
{
  return toolkit_obj(obj->r_space.obj_id());
}

template <class scene_t, class parent_t, class shells_t>
DLV::toolkit_obj DLV::render_shells_templ<scene_t, parent_t, shells_t>::get_kspace() const
{
  return toolkit_obj();
}

template <class scene_t, class parent_t, class atoms_t>
DLV::toolkit_obj DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::get_redit() const
{
  return toolkit_obj(obj->r_edit_obj.obj_id());
}

template <class scene_t, class parent_t, class atoms_t>
DLV::toolkit_obj DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::get_kedit() const
{
  return toolkit_obj(obj->k_edit_obj.obj_id());
}

template <class scene_t, class parent_t, class outline_t>
DLV::toolkit_obj DLV::render_outline_templ<scene_t, parent_t, outline_t>::get_redit() const
{
  return toolkit_obj(obj->r_edit_obj.obj_id());
}

template <class scene_t, class parent_t, class outline_t>
DLV::toolkit_obj DLV::render_outline_templ<scene_t, parent_t, outline_t>::get_kedit() const
{
  return toolkit_obj();
}

template <class scene_t, class parent_t, class shells_t>
DLV::toolkit_obj DLV::render_shells_templ<scene_t, parent_t, shells_t>::get_redit() const
{
  return toolkit_obj(obj->r_edit_obj.obj_id());
}

template <class scene_t, class parent_t, class shells_t>
DLV::toolkit_obj DLV::render_shells_templ<scene_t, parent_t, shells_t>::get_kedit() const
{
  return toolkit_obj();
}

template <class scene_t, class parent_t>
DLVreturn_type DLV::render_parent_templ<scene_t, parent_t>::attach_kui(toolkit_obj &k_ui) const
{
  // BUG
  return DLV_ERROR;
}

template <class scene_t, class parent_t, class atoms_t>
DLVreturn_type DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::attach_rui(toolkit_obj &r_ui) const
{
  static OMobj_id common_ui = OMnull_obj;
  static OMobj_id structure_ui = OMnull_obj;
  static OMobj_id display_ui = OMnull_obj;
  static OMobj_id edit_ui = OMnull_obj;
  static OMobj_id props_ui = OMnull_obj;
  static OMobj_id crystal = OMnull_obj;
  static OMobj_id crystalselect = OMnull_obj;
  static OMobj_id rodselect = OMnull_obj;
  static OMobj_id onetep = OMnull_obj;
  static OMobj_id edit_model = OMnull_obj;
  static OMobj_id loop_ui = OMnull_obj;
  static OMobj_id groups_ui = OMnull_obj;
  if (OMis_null_obj(common_ui)) {
    common_ui =
      OMfind_str_subobj(OMinst_obj,
			"DLV.app_window.ModelUI.menu_display.atoms.common",
			OM_OBJ_RW);
    structure_ui =
      OMfind_str_subobj(OMinst_obj,
			"DLV.app_window.ModelUI.menu_display.atoms.rspace",
			OM_OBJ_RW);
    props_ui =
      OMfind_str_subobj(OMinst_obj,
			"DLV.app_window.ModelUI.menu_display.atoms.props",
			OM_OBJ_RW);
    groups_ui =
      OMfind_str_subobj(OMinst_obj,
			"DLV.app_window.ModelUI.menu_display.atoms.groups",
			OM_OBJ_RW);
    edit_model =
      OMfind_str_subobj(OMinst_obj,
			"DLV.app_window.ModelUI.menu_edit",
			OM_OBJ_RW);
    loop_ui = OMfind_str_subobj(OMinst_obj, "DLV.app_window.ModelUI.Loop",
				OM_OBJ_RW);
    display_ui =
      OMfind_str_subobj(OMinst_obj,
			"DLV.app_window.DataUI.menu_display.displayUI",
			OM_OBJ_RW);
    edit_ui =
      OMfind_str_subobj(OMinst_obj,
			"DLV.app_window.DataUI.menu_edit.editUI",
			OM_OBJ_RW);
    OMobj_id x = OMfind_str_subobj(OMinst_obj,
				   "DLV.calculations.programs._CRYSTAL",
				   OM_OBJ_RD);
    int stuff;
    if (OMget_int_val(x, &stuff) == OM_STAT_SUCCESS) {
      if (stuff == 1) {
	crystal = OMfind_str_subobj(OMinst_obj,
				    "DLV.calculations.CRYSTAL.Properties.wvfn",
				    OM_OBJ_RW);
	crystalselect = OMfind_str_subobj(OMinst_obj,
				    "DLV.calculations.CRYSTAL.nselections",
					  OM_OBJ_RW);
      }
    }
    x = OMfind_str_subobj(OMinst_obj, "DLV.calculations.programs.ROD",
			  OM_OBJ_RD);
    if (OMget_int_val(x, &stuff) == OM_STAT_SUCCESS) {
      if (stuff == 1)
	rodselect = OMfind_str_subobj(OMinst_obj,
				      "DLV.calculations.ROD.Run_Rod.runrodUI"
				      ".RodModelTabUI.nselections",
				      OM_OBJ_RW);
    }
    x = OMfind_str_subobj(OMinst_obj, "DLV.calculations.programs.ONETEP",
			  OM_OBJ_RD);
    if (OMget_int_val(x, &stuff) == OM_STAT_SUCCESS) {
      if (stuff == 1)
	onetep = OMfind_str_subobj(OMinst_obj,
			      "DLV.calculations.ONETEP.SCF_calc.wavefn_set",
				   OM_OBJ_RW);
    }
  }
  // push/pop retains atom updates within attach so we can discard.
  OMpush_ctx(common_ui, OM_STATE_PROG, 0, 0);
  OMset_obj_ref(common_ui, obj->r_space.common_data.obj_id(), 0);
  OMpop_ctx(common_ui);
  //OMpush_ctx(props_ui, OM_STATE_PROG, 0, 0);
  OMset_obj_ref(props_ui, obj->r_space.props.obj_id(), 0);
  //OMpop_ctx(props_ui);
  OMpush_ctx(r_ui.get_id(), OM_STATE_PROG, 0, 0);
  OMset_obj_ref(structure_ui, r_ui.get_id(), 0);
  OMpop_ctx(r_ui.get_id());
  // atom groups
  OMset_obj_ref(groups_ui, obj->atom_groups.obj_id(), 0);
  // loop
  OMobj_id id = OMfind_str_subobj(loop_ui, "end", OM_OBJ_RW);
  OMset_obj_ref(id, obj->r_space.end_frame.obj_id(), 0);
  id = OMfind_str_subobj(loop_ui, "count", OM_OBJ_RW);
  OMset_obj_ref(id, obj->r_space.cur_frame.obj_id(), 0);
  // data links
  id = OMfind_str_subobj(display_ui, "data_list", OM_OBJ_RW);
  OMset_obj_ref(id, obj->data_objs.obj_id(), 0);
  id = OMfind_str_subobj(display_ui, "data_object", OM_OBJ_RW);
  OMset_obj_ref(id, obj->data_selection.obj_id(), 0);  
  id = OMfind_str_subobj(display_ui, "display_list", OM_OBJ_RW);
  OMset_obj_ref(id, obj->display_objs.obj_id(), 0);  
  id = OMfind_str_subobj(display_ui, "display_object", OM_OBJ_RW);
  OMset_obj_ref(id, obj->display_selection.obj_id(), 0);  
  // edit links
  id = OMfind_str_subobj(edit_ui, "data_list", OM_OBJ_RW);
  OMset_obj_ref(id, obj->editable_objs.obj_id(), 0);
  id = OMfind_str_subobj(edit_ui, "data_object", OM_OBJ_RW);
  OMset_obj_ref(id, obj->editable_selection.obj_id(), 0);
  id = OMfind_str_subobj(edit_ui, "edit_list", OM_OBJ_RW);
  OMset_obj_ref(id, obj->edits_objs.obj_id(), 0);
  id = OMfind_str_subobj(edit_ui, "edit_object", OM_OBJ_RW);
  OMset_obj_ref(id, obj->edits_selection.obj_id(), 0);
  id = OMfind_str_subobj(edit_model, "nselections", OM_OBJ_RW);
  OMset_obj_val(id, obj->r_space.nselected_atoms.obj_id(), 0);
  if (!OMis_null_obj(crystal))
    OMset_obj_ref(crystal, obj->CRYSTAL_Wvfn.obj_id(), 0);
  if (!OMis_null_obj(crystalselect))
    OMset_obj_val(crystalselect, obj->r_space.nselected_atoms.obj_id(), 0);
  if (!OMis_null_obj(rodselect))
    OMset_obj_val(rodselect, obj->r_space.nselected_atoms.obj_id(), 0);
  if (!OMis_null_obj(onetep))
    OMset_obj_ref(onetep, obj->ONETEP_Wvfn.scf_done.obj_id(), 0);
  // display links done when first object is added
  return DLV_OK;
}

template <class scene_t, class parent_t, class atoms_t>
DLVreturn_type DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::attach_kui(toolkit_obj &k_ui) const
{
  static OMobj_id structure_ui = OMnull_obj;
  static OMobj_id props_ui = OMnull_obj;
  static OMobj_id display_ui = OMnull_obj;
  static OMobj_id edit_ui = OMnull_obj;
  static OMobj_id crystal = OMnull_obj;
  if (OMis_null_obj(structure_ui)) {
    structure_ui =
      OMfind_str_subobj(OMinst_obj,
			"DLV.app_window.ModelUI.menu_display.atoms.kspace",
			OM_OBJ_RW);
    props_ui =
      OMfind_str_subobj(OMinst_obj,
			"DLV.app_window.ModelUI.menu_display.atoms.props",
			OM_OBJ_RW);
    display_ui =
      OMfind_str_subobj(OMinst_obj,
			"DLV.app_window.DataUI.menu_display.displayUI",
			OM_OBJ_RW);
    edit_ui =
      OMfind_str_subobj(OMinst_obj,
			"DLV.app_window.DataUI.menu_edit.editUI",
			OM_OBJ_RW);
    OMobj_id x = OMfind_str_subobj(OMinst_obj,
				   "DLV.calculations.programs._CRYSTAL",
				   OM_OBJ_RD);
    int stuff;
    if (OMget_int_val(x, &stuff) == OM_STAT_SUCCESS) {
      if (stuff == 1)
	crystal = OMfind_str_subobj(OMinst_obj,
				    "DLV.calculations.CRYSTAL.Properties.wvfn",
				    OM_OBJ_RW);
    }
  }
  OMset_obj_ref(structure_ui, k_ui.get_id(), 0);  
  // Todo - if we allow r/k space panels to appear at same time for
  // different modesl this will break!
  OMset_obj_ref(props_ui, obj->r_space.props.obj_id(), 0);  
  // data links
  OMobj_id id = OMfind_str_subobj(display_ui, "data_list", OM_OBJ_RW);
  OMset_obj_ref(id, obj->data_objs.obj_id(), 0);
  id = OMfind_str_subobj(display_ui, "data_object", OM_OBJ_RW);
  OMset_obj_ref(id, obj->data_selection.obj_id(), 0);  
  id = OMfind_str_subobj(display_ui, "display_list", OM_OBJ_RW);
  OMset_obj_ref(id, obj->display_objs.obj_id(), 0);  
  id = OMfind_str_subobj(display_ui, "display_object", OM_OBJ_RW);
  OMset_obj_ref(id, obj->display_selection.obj_id(), 0);  
  // edit links
  id = OMfind_str_subobj(edit_ui, "data_list", OM_OBJ_RW);
  OMset_obj_ref(id, obj->editable_objs.obj_id(), 0);
  id = OMfind_str_subobj(edit_ui, "data_object", OM_OBJ_RW);
  OMset_obj_ref(id, obj->editable_selection.obj_id(), 0);
  id = OMfind_str_subobj(edit_ui, "edit_list", OM_OBJ_RW);
  OMset_obj_ref(id, obj->edits_objs.obj_id(), 0);
  id = OMfind_str_subobj(edit_ui, "edit_object", OM_OBJ_RW);
  OMset_obj_ref(id, obj->edits_selection.obj_id(), 0);
  if (!OMis_null_obj(crystal))
    OMset_obj_ref(crystal, obj->CRYSTAL_Wvfn.obj_id(), 0);
  return DLV_OK;
}

template <class scene_t, class parent_t, class outline_t>
DLVreturn_type DLV::render_outline_templ<scene_t, parent_t, outline_t>::attach_rui(toolkit_obj &r_ui) const
{
  static OMobj_id data_ui = OMnull_obj;
  static OMobj_id display_ui = OMnull_obj;
  static OMobj_id edit_ui = OMnull_obj;
  static OMobj_id common_ui = OMnull_obj;
  if (OMis_null_obj(data_ui)) {
    data_ui =
      OMfind_str_subobj(OMinst_obj,
			"DLV.app_window.ModelUI.menu_display.outline.data",
			OM_OBJ_RW);
    display_ui =
      OMfind_str_subobj(OMinst_obj,
			"DLV.app_window.DataUI.menu_display.displayUI",
			OM_OBJ_RW);
    edit_ui =
      OMfind_str_subobj(OMinst_obj,
			"DLV.app_window.DataUI.menu_edit.editUI",
			OM_OBJ_RW);
    common_ui =
      OMfind_str_subobj(OMinst_obj,
			"DLV.app_window.ModelUI.menu_display.outline.common",
			OM_OBJ_RW);
  }
  OMset_obj_ref(data_ui, r_ui.get_id(), 0);  
  OMpush_ctx(common_ui, OM_STATE_PROG, 0, 0);
  OMset_obj_ref(common_ui, obj->r_space.common_data.obj_id(), 0);
  OMpop_ctx(common_ui); 
 // data links - common to all? No data?
  OMobj_id id = OMfind_str_subobj(display_ui, "data_list", OM_OBJ_RW);
  OMset_obj_ref(id, obj->data_objs.obj_id(), 0);
  id = OMfind_str_subobj(display_ui, "data_object", OM_OBJ_RW);
  OMset_obj_ref(id, obj->data_selection.obj_id(), 0);  
  id = OMfind_str_subobj(display_ui, "display_list", OM_OBJ_RW);
  OMset_obj_ref(id, obj->display_objs.obj_id(), 0);  
  id = OMfind_str_subobj(display_ui, "display_object", OM_OBJ_RW);
  OMset_obj_ref(id, obj->display_selection.obj_id(), 0);  
  // edit links
  id = OMfind_str_subobj(edit_ui, "data_list", OM_OBJ_RW);
  OMset_obj_ref(id, obj->editable_objs.obj_id(), 0);
  id = OMfind_str_subobj(edit_ui, "data_object", OM_OBJ_RW);
  OMset_obj_ref(id, obj->editable_selection.obj_id(), 0);
  id = OMfind_str_subobj(edit_ui, "edit_list", OM_OBJ_RW);
  OMset_obj_ref(id, obj->edits_objs.obj_id(), 0);
  id = OMfind_str_subobj(edit_ui, "edit_object", OM_OBJ_RW);
  OMset_obj_ref(id, obj->edits_selection.obj_id(), 0);
  // Todo - delete CRYSTAL/ONETEP links?
  return DLV_OK;
}

template <class scene_t, class parent_t, class shells_t>
DLVreturn_type DLV::render_shells_templ<scene_t, parent_t, shells_t>::attach_rui(toolkit_obj &r_ui) const
{
  static OMobj_id data_ui = OMnull_obj;
  static OMobj_id display_ui = OMnull_obj;
  if (OMis_null_obj(data_ui)) {
    data_ui =
      OMfind_str_subobj(OMinst_obj,
			"DLV.app_window.ModelUI.menu_display.shell.data",
			OM_OBJ_RW);
    display_ui =
      OMfind_str_subobj(OMinst_obj,
			"DLV.app_window.DataUI.menu_display.displayUI",
			OM_OBJ_RW);
  }
  OMset_obj_ref(data_ui, r_ui.get_id(), 0);  
  // data links - common to all? No data?
  OMobj_id id = OMfind_str_subobj(display_ui, "data_list", OM_OBJ_RW);
  OMset_obj_ref(id, obj->data_objs.obj_id(), 0);
  id = OMfind_str_subobj(display_ui, "data_object", OM_OBJ_RW);
  OMset_obj_ref(id, obj->data_selection.obj_id(), 0);  
  // display links done when first object is added
  return DLV_OK;
}

template <class scene_t, class parent_t, class atoms_t>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::copy_settings(render_parent_templ<scene_t, parent_t> *p, const bool copy_cell,
				      const bool copy_wavefn)
{
  render_atoms *ptr = static_cast<render_atoms *>(p);
  OMset_obj_val(obj->background.obj_id(), ptr->obj->background.obj_id(), 0);
  OMset_obj_val(obj->Xform.obj_id(), ptr->obj->Xform.obj_id(), 0);
  //OMset_obj_val(obj->Props.obj_id(), ptr->obj->Props.obj_id(), 0);
  // set is dangerous as there seem to be refs involved, so copy explicitly
  {
    OMset_obj_val(obj->Props.col.obj_id(), ptr->obj->Props.col.obj_id(), 0);
    OMset_obj_val(obj->Props.hi1_col.obj_id(),
		  ptr->obj->Props.hi1_col.obj_id(), 0);
    OMset_obj_val(obj->Props.hi2_col.obj_id(),
		  ptr->obj->Props.hi2_col.obj_id(), 0);
    OMset_obj_val(obj->Props.material.obj_id(),
		  ptr->obj->Props.material.obj_id(), 0);
    OMset_obj_val(obj->Props.spec_col.obj_id(),
		  ptr->obj->Props.trans.obj_id(), 0);
    OMset_obj_val(obj->Props.trans.obj_id(),
		  ptr->obj->Props.draw_mode.obj_id(), 0);
    OMset_obj_val(obj->Props.draw_mode.obj_id(),
		  ptr->obj->Props.line_width.obj_id(), 0);
    OMset_obj_val(obj->Props.line_width.obj_id(),
		  ptr->obj->Props.line_width.obj_id(), 0);
    OMset_obj_val(obj->Props.line_style.obj_id(),
		  ptr->obj->Props.line_style.obj_id(), 0);
    OMset_obj_val(obj->Props.line_aa.obj_id(),
		  ptr->obj->Props.line_aa.obj_id(), 0);
    OMset_obj_val(obj->Props.dir_pt_size.obj_id(),
		  ptr->obj->Props.dir_pt_size.obj_id(), 0);
    OMset_obj_val(obj->Props.subdiv.obj_id(),
		  ptr->obj->Props.subdiv.obj_id(), 0);
    OMset_obj_val(obj->Props.jitter.obj_id(),
		  ptr->obj->Props.jitter.obj_id(), 0);
    OMset_obj_val(obj->Props.cull.obj_id(),
		  ptr->obj->Props.cull.obj_id(), 0);
    OMset_obj_val(obj->Props.voxel_interp.obj_id(),
		  ptr->obj->Props.voxel_interp.obj_id(), 0);
    OMset_obj_val(obj->Props.ray_algo.obj_id(),
		  ptr->obj->Props.ray_algo.obj_id(), 0);
    OMset_obj_val(obj->Props.ray_norm.obj_id(),
		  ptr->obj->Props.ray_norm.obj_id(), 0);
    OMset_obj_val(obj->Props.fat_ray.obj_id(),
		  ptr->obj->Props.fat_ray.obj_id(), 0);
    OMset_obj_val(obj->Props.sfp_absorb.obj_id(),
		  ptr->obj->Props.sfp_absorb.obj_id(), 0);
    OMset_obj_val(obj->Props.sfp_emit.obj_id(),
		  ptr->obj->Props.sfp_emit.obj_id(), 0);
    OMset_obj_val(obj->Props.font.obj_id(),
		  ptr->obj->Props.font.obj_id(), 0);
    OMset_obj_val(obj->Props.inherit.obj_id(),
		  ptr->obj->Props.inherit.obj_id(), 0);
  }
  OMset_obj_val(obj->r_space.props.obj_id(),
		ptr->obj->r_space.props.obj_id(), 0);
  obj->r_space.common_data.centre_cell =
    ptr->obj->r_space.common_data.centre_cell;
  obj->r_space.common_data.conventional_cell =
    ptr->obj->r_space.common_data.conventional_cell;
  obj->r_space.common_data.tolerance = ptr->obj->r_space.common_data.tolerance;
  if (copy_cell) {
    obj->r_space.common_data.na = ptr->obj->r_space.common_data.na;
    obj->r_space.common_data.nb = ptr->obj->r_space.common_data.nb;
    obj->r_space.common_data.nc = ptr->obj->r_space.common_data.nc;
  }
  if (copy_wavefn) {
    OMset_obj_val(obj->CRYSTAL_Wvfn.obj_id(),
		  ptr->obj->CRYSTAL_Wvfn.obj_id(), 0);
    OMset_obj_val(obj->ONETEP_Wvfn.obj_id(),
		  ptr->obj->ONETEP_Wvfn.obj_id(), 0);
  }
  // anything else?
}

template <class scene_t, class parent_t, class outline_t>
void DLV::render_outline_templ<scene_t, parent_t, outline_t>::copy_settings(render_parent_templ<scene_t, parent_t> *p, const bool copy_cell,
					const bool)
{
  render_outline *ptr = static_cast<render_outline *>(p);
  OMset_obj_val(obj->background.obj_id(), ptr->obj->background.obj_id(), 0);
  OMset_obj_val(obj->Props.obj_id(), ptr->obj->Props.obj_id(), 0);
  obj->r_space.common_data.centre_cell =
    ptr->obj->r_space.common_data.centre_cell;
  obj->r_space.common_data.conventional_cell =
    ptr->obj->r_space.common_data.conventional_cell;
  obj->r_space.common_data.tolerance = ptr->obj->r_space.common_data.tolerance;
  if (copy_cell) {
    obj->r_space.common_data.na = ptr->obj->r_space.common_data.na;
    obj->r_space.common_data.nb = ptr->obj->r_space.common_data.nb;
    obj->r_space.common_data.nc = ptr->obj->r_space.common_data.nc;
  }
}

template <class scene_t, class parent_t, class shells_t>
void DLV::render_shells_templ<scene_t, parent_t, shells_t>::copy_settings(render_parent_templ<scene_t, parent_t> *p, const bool, const bool)
{
  render_shells *ptr = static_cast<render_shells *>(p);
  OMset_obj_val(obj->background.obj_id(), ptr->obj->background.obj_id(), 0);
  OMset_obj_val(obj->Props.obj_id(), ptr->obj->Props.obj_id(), 0);
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_data_size(const int n)
{
  OMset_array_size(get_object()->data_objs.obj_id(), n);
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::select_data_object(const int n)
{
  OMset_int_val(get_object()->data_selection.obj_id(), n);
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_edit_size(const int n)
{
  OMset_array_size(get_object()->editable_objs.obj_id(), n);
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_data_label(const string label, const int n)
{
  get_object()->data_objs[n].label.set_str_val(label.c_str());
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_edit_label(const string label, const int n)
{
  get_object()->editable_objs[n].label.set_str_val(label.c_str());
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_sub_data_size(const int s, const int n)
{
  parent_t *obj = get_object();
  OMset_array_size(obj->data_objs[n].data_list.obj_id(), s);
  OMset_array_size(obj->data_objs[n].vector.obj_id(), s);
  if (s == 1)
    obj->data_objs[n].selection = 0;
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_sub_edit_size(const int s, const int n)
{
  parent_t *obj = get_object();
  OMset_array_size(obj->editable_objs[n].data_list.obj_id(), s);
  OMset_array_size(obj->editable_objs[n].vector.obj_id(), s);
  if (s == 1)
    obj->editable_objs[n].selection = 0;
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_sub_data_label(const string label,
					    const int s, const int n)
{
  parent_t *obj = get_object();
  OMset_str_array_val(obj->data_objs[n].data_list.obj_id(), s, label.c_str());
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_sub_edit_label(const string label,
					    const int s, const int n)
{
  parent_t *obj = get_object();
  OMset_str_array_val(obj->editable_objs[n].data_list.obj_id(), s,
		      label.c_str());
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_display_type(const int v, const int n)
{
  parent_t *obj = get_object();
  obj->data_objs[n].type_data.set_int_val(v);
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_edit_type(const int v, const int n)
{
  parent_t *obj = get_object();
  obj->editable_objs[n].type_data.set_int_val(v);
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_sub_data_vector(const bool v,
					     const int s, const int n)
{
  parent_t *obj = get_object();
  int *ptr = (int *)obj->data_objs[n].vector.ret_array_ptr(OM_GET_ARRAY_WR,
							   0, 0);
  ptr[s] = (int)v;
  obj->data_objs[n].vector.free_array(ptr);
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_sub_edit_vector(const bool v,
					     const int s, const int n)
{
  parent_t *obj = get_object();
  int *ptr = (int *)obj->editable_objs[n].vector.ret_array_ptr(OM_GET_ARRAY_WR,
							       0, 0);
  ptr[s] = (int)v;
  obj->editable_objs[n].vector.free_array(ptr);
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::add_display_list(const string name,
					  const int id, const int index) const
{
  parent_t *obj = get_object();
  int n = obj->display_size;
  int i = n + 1;
  OMpush_ctx(obj->display_selection.obj_id(), OM_STATE_PROG, 0, 0);
  obj->display_size = i;
  OMset_array_size(obj->display_objs.obj_id(), i);
  obj->display_objs[n].label = name.c_str();
  obj->display_objs[n].display_type = id;
  obj->display_objs[n].index = index;
  // clear any widget updates that may attempt to reset display_selection
  if (n == 0) {
    // display
    static OMobj_id display_ui = OMnull_obj;
    if (OMis_null_obj(display_ui))
      display_ui =
	OMfind_str_subobj(OMinst_obj,
			  "DLV.app_window.DataUI.menu_display.displayUI",
			  OM_OBJ_RW);
    OMobj_id id = OMfind_str_subobj(display_ui, "display_list", OM_OBJ_RW);
    OMset_obj_ref(id, obj->display_objs.obj_id(), 0);
    id = OMfind_str_subobj(display_ui, "display_object", OM_OBJ_RW);
    OMset_obj_ref(id, obj->display_selection.obj_id(), 0);  
  }
  OMpop_ctx(obj->display_selection.obj_id());
  obj->display_selection = n;
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::reset_display_list(const int object) const
{
  parent_t *obj = get_object();
  int n = obj->display_size;
  // Object is not the array index, its the objects index value
  int k = 0;
  int i;
  for (i = 0; i < n; i++) {
    int j = obj->display_objs[i].index;
    if (j == object) {
      k = i;
      break;
    }
  }
  OMpush_ctx(obj->display_selection.obj_id(), OM_STATE_PROG, 0, 0);
  for (; i < n - 1; i++) {
    obj->display_objs[i].label = (char *)obj->display_objs[i + 1].label;
    obj->display_objs[i].display_type =
      (int)obj->display_objs[i + 1].display_type;
    obj->display_objs[i].index = (int)obj->display_objs[i + 1].index;
  }
  obj->display_size = n - 1;
  OMset_array_size(obj->display_objs.obj_id(), n - 1);
  // clear any widget updates that may attempt to reset display_selection
  OMpop_ctx(obj->display_selection.obj_id());
  if (k > 0)
    obj->display_selection = k - 1;
  else
    obj->display_selection = 0;
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::reset_display_list(const int first,
					    const int last) const
{
  parent_t *obj = get_object();
  int n = obj->display_size;
  // Object is not the array index, its the objects index value
  int k = 0;
  int i;
  for (i = 0; i < n; i++) {
    int j = obj->display_objs[i].index;
    if (j == first) {
      k = i;
      break;
    }
  }
  for (; i < n; i++) {
    int j = obj->display_objs[i].index;
    if (j == last)
      break;
  }
  int diff = last - first + 1;
  int l = k;
  OMpush_ctx(obj->display_selection.obj_id(), OM_STATE_PROG, 0, 0);
  for (i = i + 1; i < n; i++) {
    obj->display_objs[l].label = (char *)obj->display_objs[i].label;
    obj->display_objs[l].display_type =
      (int)obj->display_objs[i].display_type;
    obj->display_objs[l].index = (int)obj->display_objs[i].index;
    l++;
  }
  obj->display_size = n - diff;
  OMset_array_size(obj->display_objs.obj_id(), n - diff);
  // clear any widget updates that may attempt to reset display_selection
  OMpop_ctx(obj->display_selection.obj_id());
  if (k > 0)
    obj->display_selection = k - 1;
  else
    obj->display_selection = 0;
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::empty_display_list() const
{
  parent_t *obj = get_object();
  OMpush_ctx(obj->display_selection.obj_id(), OM_STATE_PROG, 0, 0);
  obj->display_size = 0;
  OMset_array_size(obj->display_objs.obj_id(), 0);
  //obj->display_objs[n].label = name.c_str();
  //obj->display_objs[n].display_type = id;
  // clear any widget updates that may attempt to reset display_selection
  OMpop_ctx(obj->display_selection.obj_id());
  OMset_obj_val(obj->display_selection.obj_id(), OMnull_obj, 0);
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::show_data_panel()
{
  static OMobj_id panel = OMnull_obj;
  if (OMis_null_obj(panel))
    panel = OMfind_str_subobj(OMinst_obj,
			      "DLV.app_window.DataUI.menu_display.panel_open",
			      OM_OBJ_RW);
  OMset_int_val(panel, 1);
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::stop_animation()
{
  static OMobj_id runf = OMnull_obj;
  static OMobj_id runb = OMnull_obj;
  if (OMis_null_obj(runf)) {
   runf = OMfind_str_subobj(OMinst_obj, "DLV.app_window.ModelUI.Loop.run",
			      OM_OBJ_RW);
   runb = OMfind_str_subobj(OMinst_obj, "DLV.app_window.ModelUI.Loop.run_back",
			      OM_OBJ_RW);
  }
  OMset_int_val(runf, 0);
  OMset_int_val(runb, 0);
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::show_animate_panel()
{
  static OMobj_id panel = OMnull_obj;
  if (OMis_null_obj(panel))
    panel = OMfind_str_subobj(OMinst_obj,
			      "DLV.app_window.ModelUI.Loop.visible",
			      OM_OBJ_RW);
  OMset_int_val(panel, 1);
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::add_edit_list(const string name,
				       const int id, const int index) const
{
  parent_t *obj = get_object();
  int n = obj->edits_size;
  int i = n + 1;
  OMpush_ctx(obj->edits_selection.obj_id(), OM_STATE_PROG, 0, 0);
  obj->edits_size = i;
  OMset_array_size(obj->edits_objs.obj_id(), i);
  obj->edits_objs[n].label = name.c_str();
  obj->edits_objs[n].display_type = id;
  obj->edits_objs[n].index = index;
  // clear any widget updates that may attempt to reset display_selection
  OMpop_ctx(obj->edits_selection.obj_id());
  obj->edits_selection = n;
  if (n == 0) {
    // display
    static OMobj_id display_ui = OMnull_obj;
    if (OMis_null_obj(display_ui))
      display_ui =
	OMfind_str_subobj(OMinst_obj,
			  "DLV.app_window.DataUI.menu_edit.editUI",
			  OM_OBJ_RW);
    OMobj_id id = OMfind_str_subobj(display_ui, "edit_list", OM_OBJ_RW);
    OMset_obj_ref(id, obj->edits_objs.obj_id(), 0);
    id = OMfind_str_subobj(display_ui, "edit_object", OM_OBJ_RW);
    OMset_obj_ref(id, obj->edits_selection.obj_id(), 0);
  }
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::reset_edit_list(const int object) const
{
  parent_t *obj = get_object();
  int n = obj->edits_size;
  OMpush_ctx(obj->edits_selection.obj_id(), OM_STATE_PROG, 0, 0);
  for (int i = object; i < n - 1; i++) {
    obj->edits_objs[i].label = (char *)obj->edits_objs[i + 1].label;
    obj->edits_objs[i].display_type = (int)obj->edits_objs[i + 1].display_type;
    obj->edits_objs[i].index = (int)obj->edits_objs[i + 1].index;
  }
  obj->edits_size = n - 1;
  OMset_array_size(obj->edits_objs.obj_id(), n - 1);
  // clear any widget updates that may attempt to reset display_selection
  OMpop_ctx(obj->edits_selection.obj_id());
  if (object > 0)
    obj->edits_selection = object - 1;
  else
    obj->edits_selection = 0;
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::empty_edit_list() const
{
  parent_t *obj = get_object();
  OMpush_ctx(obj->edits_selection.obj_id(), OM_STATE_PROG, 0, 0);
  obj->edits_size = 0;
  OMset_array_size(obj->edits_objs.obj_id(), 0);
  //obj->display_objs[n].label = name.c_str();
  //obj->display_objs[n].display_type = id;
  // clear any widget updates that may attempt to reset display_selection
  OMpop_ctx(obj->edits_selection.obj_id());
  OMset_obj_val(obj->edits_selection.obj_id(), OMnull_obj, 0);
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_atom_group_size(const int)
{
  // a bug
}

template <class scene_t, class parent_t, class atoms_t>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::set_atom_group_size(const int size)
{
  OMset_array_size(obj->atom_groups.obj_id(), size);
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_atom_group_name(const string, const int)
{
  // a bug
}

template <class scene_t, class parent_t, class atoms_t>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::set_atom_group_name(const string name,
					       const int index)
{
  OMset_str_array_val(obj->atom_groups.obj_id(), index, name.c_str());
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::get_cell_data(int &, int &, int &,
								bool &, bool &,
								bool &, double &) const
{
  // a bug
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_transforms(const int n,
					const float transforms[][3])
{
  // a bug
}

template <class scene_t, class parent_t, class atoms_t>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::get_cell_data(int &na, int &nb, int &nc,
									bool &centre_cell,
									bool &conv_cell,
									bool &edges,
									double &tol) const
{
  na = (int)obj->r_space.common_data.na;
  nb = (int)obj->r_space.common_data.nb;
  nc = (int)obj->r_space.common_data.nc;
  centre_cell = (bool)obj->r_space.common_data.centre_cell;
  conv_cell = (int(obj->r_space.common_data.conventional_cell) == 1);
  edges = (cell_type < 2);
  tol = (double)obj->r_space.common_data.tolerance;
}

template <class scene_t, class parent_t, class atoms_t>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::set_transforms(const int n,
				       const float transforms[][3])
{
  OMset_array_size(obj->r_space.transforms.obj_id(), n);
  for (int i = 0; i < n; i++) {
    float *ptr = (float *)
      obj->r_space.transforms[i].xform.xlate.ret_array_ptr(OM_GET_ARRAY_WR);
    if (ptr != 0) {
      ptr[0] = transforms[i][0];
      ptr[1] = transforms[i][1];
      ptr[2] = transforms[i][2];
      obj->r_space.transforms[i].xform.xlate.free_array(ptr);
    }
    // else HELP!
  }
}

template <class scene_t, class parent_t, class outline_t>
void DLV::render_outline_templ<scene_t, parent_t, outline_t>::get_cell_data(int &na, int &nb, int &nc,
				      bool &centre_cell,
				      int &cell_type, double &tol) const
{
  na = (int)obj->r_space.common_data.na;
  nb = (int)obj->r_space.common_data.nb;
  nc = (int)obj->r_space.common_data.nc;
  centre_cell = (bool)obj->r_space.common_data.centre_cell;
  cell_type = (int)obj->r_space.common_data.conventional_cell;
  tol = (double)obj->r_space.common_data.tolerance;
}

template <class scene_t, class parent_t, class outline_t>
void DLV::render_outline_templ<scene_t, parent_t, outline_t>::set_transforms(const int n,
				       const float transforms[][3])
{
  OMset_array_size(obj->r_space.transforms.obj_id(), n);
  for (int i = 0; i < n; i++) {
    float *ptr = (float *)
      obj->r_space.transforms[i].xform.xlate.ret_array_ptr(OM_GET_ARRAY_WR);
    if (ptr != 0) {
      ptr[0] = transforms[i][0];
      ptr[1] = transforms[i][1];
      ptr[2] = transforms[i][2];
      obj->r_space.transforms[i].xform.xlate.free_array(ptr);
    }
    // else HELP!
  }
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_CRYSTAL_wavefn(const int valence_min,
					    const int valence_max,
					    const int nbands,
					    const int lattice,
					    const int centre, const bool spin,
					    const int kpoints[][3],
					    const int nkpoints, const int sa,
					    const int sb, const int sc)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_CRYSTAL_scf()
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_CRYSTAL_tddft()
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_ONETEP_scf()
{
}

template <class scene_t, class parent_t, class atoms_t>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::set_CRYSTAL_wavefn(const int valence_min,
					   const int valence_max,
					   const int nbands,
					   const int lattice,
					   const int centre, const bool spin,
					   const int kpoints[][3],
					   const int nkpoints, const int sa,
					   const int sb, const int sc)
{
  obj->CRYSTAL_Wvfn.nbands = nbands;
  obj->CRYSTAL_Wvfn.vband_min = valence_min;
  obj->CRYSTAL_Wvfn.vband_max = valence_max;
  obj->CRYSTAL_Wvfn.lattice = lattice;
  obj->CRYSTAL_Wvfn.centre = centre;
  obj->CRYSTAL_Wvfn.wavefn_set = 1;
  obj->CRYSTAL_Wvfn.spin = (int)spin;
  obj->CRYSTAL_Wvfn.n_kpoints = nkpoints;
  char temp[32];
  for (int i = 0; i < nkpoints; i++) {
    string label;
    if (kpoints[i][0] == 0)
      label += "0  ";
    else {
      int num = kpoints[i][0];
      int den = sa;
      int divisor = 1;
      for (int j = 2; j <= num; j++) {
	if (num % j == 0 and den % j == 0)
	  divisor = j;
      }
      num /= divisor;
      den /= divisor;
      snprintf(temp, 32, "%1d/%1d", num, den);
      label += temp;
    }
    label += " ";
    if (kpoints[i][1] == 0)
      label += "0  ";
    else {
      int num = kpoints[i][1];
      int den = sa;
      int divisor = 1;
      for (int j = 2; j <= num; j++) {
	if (num % j == 0 and den % j == 0)
	  divisor = j;
      }
      num /= divisor;
      den /= divisor;
      snprintf(temp, 32, "%1d/%1d", num, den);
      label += temp;
    }
    label += " ";
    if (kpoints[i][2] == 0)
      label += "0  ";
    else {
      int num = kpoints[i][2];
      int den = sa;
      int divisor = 1;
      for (int j = 2; j <= num; j++) {
	if (num % j == 0 and den % j == 0)
	  divisor = j;
      }
      num /= divisor;
      den /= divisor;
      snprintf(temp, 32, "%1d/%1d", num, den);
      label += temp;
    }
    OMset_str_array_val(obj->CRYSTAL_Wvfn.kpoint_labels.obj_id(),
			i, label.c_str());
  }
}

template <class scene_t, class parent_t, class atoms_t>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::set_CRYSTAL_scf()
{
  obj->CRYSTAL_Wvfn.scf_done = 1;
}

template <class scene_t, class parent_t, class atoms_t>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::set_CRYSTAL_tddft()
{
  obj->CRYSTAL_Wvfn.tddft_done = 1;
}

template <class scene_t, class parent_t, class atoms_t>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::set_ONETEP_scf()
{
  obj->ONETEP_Wvfn.scf_done = 1;
}

template class DLV::render_parent_templ<CCP3_Viewers_Scenes_DLV3Dscene,
					CCP3_Renderers_Base_parent>;
template class DLV::render_atoms_templ<CCP3_Viewers_Scenes_DLV3Dscene,
				       CCP3_Renderers_Base_parent,
				       CCP3_Renderers_Base_atoms>;
template class DLV::render_outline_templ<CCP3_Viewers_Scenes_DLV3Dscene,
					 CCP3_Renderers_Base_parent,
					 CCP3_Renderers_Base_outline>;
template class DLV::render_shells_templ<CCP3_Viewers_Scenes_DLV3Dscene,
					CCP3_Renderers_Base_parent,
					CCP3_Renderers_Base_shells>;

#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::render_atoms *t,
				    const unsigned int file_version)
    {
      DLV::string name = t->get_name();
      ar << name;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::render_atoms *t,
				    const unsigned int file_version)
    {
      DLV::string name;
      ar >> name;
      ::new(t)DLV::render_atoms(name.c_str());
      t->set_serialize_obj();
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::render_outline *t,
				    const unsigned int file_version)
    {
      DLV::string name = t->get_name();
      ar << name;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::render_outline *t,
				    const unsigned int file_version)
    {
      DLV::string name;
      ar >> name;
      ::new(t)DLV::render_outline(name.c_str());
      t->set_serialize_obj();
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::render_shells *t,
				    const unsigned int file_version)
    {
      DLV::string name = t->get_name();
      ar << name;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::render_shells *t,
				    const unsigned int file_version)
    {
      DLV::string name;
      ar >> name;
      ::new(t)DLV::render_shells(name.c_str());
      t->set_serialize_obj();
    }

  }
}

template <class scene_t, class parent_t> template <class Archive>
void DLV::render_parent_templ<scene_t, parent_t>::serialize(Archive &ar, const unsigned int version)
{
}

template <class scene_t, class parent_t, class atoms_t> template <class Archive>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<render_parent>(*this);
  int i = (int)obj->r_space.common_data.na;
  ar << i;
  i = (int)obj->r_space.common_data.nb;
  ar << i;
  i = (int)obj->r_space.common_data.nc;
  ar << i;
  i = (int)obj->r_space.common_data.centre_cell;
  ar << i;
  i = (int)obj->r_space.common_data.conventional_cell;
  ar << i;
  float f = (float)obj->r_space.common_data.tolerance;
  ar << f;
  // Todo - involves writing arrays obj->Xform.mat/xlate/center
}

template <class scene_t, class parent_t, class atoms_t> template <class Archive>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<render_parent>(*this);
  int i;
  ar >> i;
  obj->r_space.common_data.na = i;
  ar >> i;
  obj->r_space.common_data.nb = i;
  ar >> i;
  obj->r_space.common_data.nc = i;
  ar >> i;
  obj->r_space.common_data.centre_cell = i;
  ar >> i;
  obj->r_space.common_data.conventional_cell = i;
  float f;
  ar >> f;
  obj->r_space.common_data.tolerance = f;
  // Todo - involves reading arrays
}

template <class scene_t, class parent_t, class outline_t> template <class Archive>
void DLV::render_outline_templ<scene_t, parent_t, outline_t>::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<render_parent>(*this);
  // Todo
}

template <class scene_t, class parent_t, class outline_t> template <class Archive>
void DLV::render_outline_templ<scene_t, parent_t, outline_t>::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<render_parent>(*this);
  // Todo - obj needs to exist!
}

template <class scene_t, class parent_t, class shells_t> template <class Archive>
void DLV::render_shells_templ<scene_t, parent_t, shells_t>::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<render_parent>(*this);
  // Todo
}

template <class scene_t, class parent_t, class shells_t> template <class Archive>
void DLV::render_shells_templ<scene_t, parent_t, shells_t>::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<render_parent>(*this);
  // Todo - obj needs to exist!
}

BOOST_CLASS_EXPORT_GUID(DLV::render_atoms, "DLV::render_atoms")
BOOST_CLASS_EXPORT_GUID(DLV::render_outline, "DLV::render_outline")
BOOST_CLASS_EXPORT_GUID(DLV::render_shells, "DLV::render_shells")

#endif // DLV_USES_SERIALIZE
