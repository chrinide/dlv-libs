
#ifndef DLV_DISPLAY_PRIVATE
#define DLV_DISPLAY_PRIVATE

namespace DLV {

  class display_text_data : public display_obj {
  public:
    display_text_data(const OMobj_id id, const int i, const string n);
    ~display_text_data();

    void attach_data(const drawable_obj *draw, const string name);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);

  protected:
    int get_type() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Points_text_message *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class display_real_data : public display_obj {
  public:
    display_real_data(const OMobj_id id, const int i, const string n);
    ~display_real_data();

    void attach_data(const drawable_obj *draw, const string name);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);

  protected:
    int get_type() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Points_real_scalar *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class display_file_view : public display_obj {
  public:
    display_file_view(const OMobj_id id, const int i, const string n);
    ~display_file_view();

    void attach_data(const drawable_obj *draw, const string name);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);

  protected:
    int get_type() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Points_file_viewer *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class display_text_view : public display_obj {
  public:
    display_text_view(const OMobj_id id, const int i, const string n);
    ~display_text_view();

    void attach_data(const drawable_obj *draw, const string name);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);

  protected:
    int get_type() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Points_buffer_view *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class display_atom_bond_data : public display_obj {
  public:
    display_atom_bond_data(const OMobj_id id, const int i, const string n);
    ~display_atom_bond_data();

    void attach_data(const drawable_obj *draw, const string name,
		     const OMobj_id parent);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);

  protected:
    int get_type() const;
    //CCP3_Renderers_Points_atom_bond_text *get_obj() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Points_atom_bond_text *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class display_atom_text_data : public display_obj {
  public:
    display_atom_text_data(const OMobj_id id, const int i, const string n);
    ~display_atom_text_data();

    void attach_data(const drawable_obj *draw, const int component,
		     const string name, const OMobj_id parent);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);
    void display_3D();
    void undisplay_3D();
  protected:
    int get_type() const;
    //CCP3_Renderers_Points_atom_bond_text *get_obj() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Points_labels *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class display_atom_vector_data : public display_obj {
  public:
    display_atom_vector_data(const OMobj_id id, const int i, const string n);
    ~display_atom_vector_data();

    void attach_data(const drawable_obj *draw, const int component,
		     const string name, const OMobj_id parent);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);

  protected:
    int get_type() const;
    //CCP3_Renderers_Points_atom_bond_text *get_obj() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Points_vector_glyphs *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class display_atom_trajectory : public display_obj {
  public:
    display_atom_trajectory(const OMobj_id id, const int i, const string n);
    ~display_atom_trajectory();

    void turn_off_visible();
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);

  protected:
    int get_type() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Points_trajectory *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  // Todo - the volume stuff would seem to have some common options
  class isosurface : public display_obj {
  public:
    isosurface(const OMobj_id id, const int i, const bool sp, const string n);
    ~isosurface();

    void attach_data(const drawable_obj *draw, const int component,
		     const string name, const OMobj_id parent);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);
    virtual void attach_transforms(const render_parent *parent);

  protected:
    isosurface(CCP3_Renderers_Volumes_isosurface_base *d,
	       const int i, const bool sp, const string n);

    int get_type() const;
    CCP3_Renderers_Volumes_isosurface_base *get_obj() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Volumes_isosurface_base *obj;
    int colour_comp; // value from serialize::load seems to get lost

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class periodic_isosurface : public isosurface {
  public:
    periodic_isosurface(const OMobj_id id, const int i,
			const bool sp, const string n);

    void attach_transforms(const render_parent *parent);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<DLV::isosurface>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

  class iso_wavefn : public display_obj {
  public:
    iso_wavefn(const OMobj_id id, const int i, const bool sp, const string n);
    ~iso_wavefn();

    void attach_data(const drawable_obj *draw, const int component,
		     const int ndata, const string name,
		     const OMobj_id parent);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);
    virtual void attach_transforms(const render_parent *parent);

  protected:
    iso_wavefn(CCP3_Renderers_Volumes_iso_wavefn_base *d, const int i,
	       const int sa, const int sb, const int sc, const bool sp,
	       const string n);

    int get_type() const;
    CCP3_Renderers_Volumes_iso_wavefn_base *get_obj() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Volumes_iso_wavefn_base *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class iso_bloch : public iso_wavefn {
  public:
    iso_bloch(const OMobj_id id, const int i, const int sa, const int sb,
	      const int sc, const bool sp, const string n);

    void attach_transforms(const render_parent *parent);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<DLV::iso_wavefn>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

  class cells_obj : public display_obj {
  public:
    cells_obj(const OMobj_id id, const int i, const bool sp, const string n);
    ~cells_obj();

    void attach_data(const drawable_obj *draw, const int component,
		     const string name, const OMobj_id parent);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);
    virtual void attach_transforms(const render_parent *parent);

  protected:
    cells_obj(CCP3_Renderers_Volumes_shrink_cells_base *d,
	      const int i, const bool sp, const string n);

    int get_type() const;
    CCP3_Renderers_Volumes_shrink_cells_base *get_obj() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Volumes_shrink_cells_base *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class periodic_cells : public cells_obj {
  public:
    periodic_cells(const OMobj_id id, const int i,
		   const bool sp, const string n);

    void attach_transforms(const render_parent *parent);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<DLV::cells_obj>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

  class spheres : public display_obj {
  public:
    spheres(const OMobj_id id, const int i, const bool sp, const string n);
    ~spheres();

    void attach_data(const drawable_obj *draw, const int component,
		     const string name, const OMobj_id parent);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);
    virtual void attach_transforms(const render_parent *parent);

  protected:
    spheres(CCP3_Renderers_Volumes_spheres_base *d,
	    const int i, const bool sp, const string n);

    int get_type() const;
    CCP3_Renderers_Volumes_spheres_base *get_obj() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Volumes_spheres_base *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class periodic_spheres : public spheres {
  public:
    periodic_spheres(const OMobj_id id, const int i,
		     const bool sp, const string n);

    void attach_transforms(const render_parent *parent);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<DLV::spheres>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

  class volume_view : public display_obj {
  public:
    volume_view(const OMobj_id id, const int i, const bool sp,
		  const string n);
    ~volume_view();

    void attach_data(const drawable_obj *draw, const int component,
		     const string name, const OMobj_id parent);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);
    virtual void attach_transforms(const render_parent *parent);

  protected:
    volume_view(CCP3_Renderers_Volumes_volume_render *d,
		const int i, const bool sp, const string n);

    int get_type() const;
    CCP3_Renderers_Volumes_volume_render *get_obj() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Volumes_volume_render *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class ag_contours : public display_obj {
  public:
    ag_contours(const OMobj_id id, const int i, const bool sp, const string n);
    ~ag_contours();

    void attach_data(const drawable_obj *draw, const int component,
		     const string name, const OMobj_id parent,
		     const OMobj_id space, const char *model);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);
    virtual void attach_transforms(const render_parent *parent);
    void set_background(const render_parent *parent);
    void set_font();

  protected:
    ag_contours(CCP3_Renderers_Planes_AGisoline_base *d,
		const int i, const bool sp, const string n);

    int get_type() const;
    CCP3_Renderers_Planes_AGisoline_base *get_obj() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Planes_AGisoline_base *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class periodic_ag_contours : public ag_contours {
  public:
    periodic_ag_contours(const OMobj_id id, const int i,
			 const bool sp, const string n);

    void attach_transforms(const render_parent *parent);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<DLV::ag_contours>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

  class contours : public display_obj {
  public:
    contours(const OMobj_id id, const int i, const bool sp, const string n);
    ~contours();

    void attach_data(const drawable_obj *draw, const int component,
		     const string name, const OMobj_id parent,
		     const OMobj_id space, const char *model);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);
    virtual void attach_transforms(const render_parent *parent);
    void set_background(const render_parent *parent);

  protected:
    contours(CCP3_Renderers_Planes_isoline_base *d,
	     const int i, const bool sp, const string n);

    int get_type() const;
    CCP3_Renderers_Planes_isoline_base *get_obj() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Planes_isoline_base *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class periodic_contours : public contours {
  public:
    periodic_contours(const OMobj_id id, const int i,
		      const bool sp, const string n);

    void attach_transforms(const render_parent *parent);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<DLV::contours>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

  class pn_contours : public display_obj {
  public:
    pn_contours(const OMobj_id id, const int i, const bool sp, const string n);
    ~pn_contours();

    void attach_data(const drawable_obj *draw, const int component,
		     const string name, const OMobj_id parent,
		     const OMobj_id space, const char *model);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);
    virtual void attach_transforms(const render_parent *parent);
    void set_background(const render_parent *parent);

  protected:
    pn_contours(CCP3_Renderers_Planes_disoline_base *d,
		const int i, const bool sp, const string n);

    int get_type() const;
    CCP3_Renderers_Planes_disoline_base *get_obj() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Planes_disoline_base *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class periodic_pn_contours : public pn_contours {
  public:
    periodic_pn_contours(const OMobj_id id, const int i,
			 const bool sp, const string n);

    void attach_transforms(const render_parent *parent);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<DLV::pn_contours>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

  class solid_contours : public display_obj {
  public:
    solid_contours(const OMobj_id id, const int i,
		   const bool sp, const string n);
    ~solid_contours();

    void attach_data(const drawable_obj *draw, const int component,
		     const string name, const OMobj_id parent,
		     const OMobj_id space, const char *model);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);
    virtual void attach_transforms(const render_parent *parent);
    void set_background(const render_parent *parent);

  protected:
    solid_contours(CCP3_Renderers_Planes_solid_contour_base *d,
		   const int i, const bool sp, const string n);

    int get_type() const;
    CCP3_Renderers_Planes_solid_contour_base *get_obj() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Planes_solid_contour_base *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class periodic_solid_contours : public solid_contours {
  public:
    periodic_solid_contours(const OMobj_id id, const int i,
			    const bool sp, const string n);

    void attach_transforms(const render_parent *parent);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<DLV::solid_contours>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

  class shaded_contours : public display_obj {
  public:
    shaded_contours(const OMobj_id id, const int i,
		    const bool sp, const string n);
    ~shaded_contours();

    void attach_data(const drawable_obj *draw, const int component,
		     const string name, const OMobj_id parent,
		     const OMobj_id space, const char *model);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);
    virtual void attach_transforms(const render_parent *parent);
    void set_background(const render_parent *parent);

  protected:
    shaded_contours(CCP3_Renderers_Planes_shaded_contour_base *d,
		    const int i, const bool sp, const string n);

    int get_type() const;
    CCP3_Renderers_Planes_shaded_contour_base *get_obj() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Planes_shaded_contour_base *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class periodic_shaded_contours : public shaded_contours {
  public:
    periodic_shaded_contours(const OMobj_id id, const int i,
			     const bool sp, const string n);

    void attach_transforms(const render_parent *parent);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<DLV::shaded_contours>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

  class surface_plot : public display_obj {
  public:
    surface_plot(const OMobj_id id, const int i,
		 const bool sp, const string n);
    ~surface_plot();

    void attach_data(const drawable_obj *draw, const int component,
		     const string name, const OMobj_id parent,
		     const OMobj_id space);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);
    virtual void attach_transforms(const render_parent *parent);

  protected:
    surface_plot(CCP3_Renderers_Planes_surf_plot_base *d,
		 const int i, const bool sp, const string n);

    int get_type() const;
    CCP3_Renderers_Planes_surf_plot_base *get_obj() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Planes_surf_plot_base *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class periodic_surface_plot : public surface_plot {
  public:
    periodic_surface_plot(const OMobj_id id, const int i,
			  const bool sp, const string n);

    void attach_transforms(const render_parent *parent);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<DLV::surface_plot>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

  class plot_obj : public display_obj {
  public:
    ~plot_obj();
    void attach_data(const drawable_obj *draw, const string name,
		     const string xaxis, const string yaxis,
		     const int ngraphs, const OMobj_id parent,
		     const char *model, const bool visible = true);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);

    void set_background(const render_parent *parent);
    void set_font();

  protected:
    plot_obj(CCP3_Renderers_Plots_plot_base *d, const int i, const string n);

    int get_type() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Plots_plot_base *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class display_dos : public plot_obj {
  public:
    display_dos(const OMobj_id id, const int i, const string n);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<DLV::plot_obj>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

  class display_rod1d : public plot_obj {
  public:
    display_rod1d(const OMobj_id id, const int i, const string n);
    void update_data(const render_parent *parent,
		const class drawable_obj *draw,
		const string name,
		const string xaxis, const string yaxis,
		const int ngraphs, const bool visible,
		char message[], const int mlen);
  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<DLV::plot_obj>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

  class display_rod2d : public plot_obj {
  public:
    display_rod2d(const OMobj_id id, const int i, const string n);
    void update_data(const render_parent *parent,
		const class drawable_obj *draw,
		const string name,
		const string xaxis, const string yaxis,
		const int ngraphs, const bool visible,
		char message[], const int mlen);
  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<DLV::plot_obj>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

  class display_band : public plot_obj {
  public:
    display_band(const OMobj_id id, const int i, const string n);

  protected:
    display_band(CCP3_Renderers_Plots_plot_base *d,
		 const int i, const string n);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<DLV::plot_obj>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

  class display_bspin : public display_band {
  public:
    display_bspin(const OMobj_id id, const int i, const string n);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<DLV::display_band>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

  class hedgehog : public display_obj {
  public:
    hedgehog(const OMobj_id id, const int i, const bool sp, const string n);
    ~hedgehog();

    void attach_data(const drawable_obj *draw, const int component,
		     const string name, const OMobj_id parent);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);
    virtual void attach_transforms(const render_parent *parent);

  protected:
    hedgehog(CCP3_Renderers_Volumes_hedgehog_base *d,
	     const int i, const bool sp, const string n);

    int get_type() const;
    CCP3_Renderers_Volumes_hedgehog_base *get_obj() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Volumes_hedgehog_base *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class periodic_hedgehog : public hedgehog {
  public:
    periodic_hedgehog(const OMobj_id id, const int i,
		      const bool sp, const string n);

    void attach_transforms(const render_parent *parent);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<DLV::hedgehog>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

  class streamlines : public display_obj {
  public:
    streamlines(const OMobj_id id, const int i, const bool sp, const string n);
    ~streamlines();

    void attach_data(const drawable_obj *draw, const int component,
		     const string name, const OMobj_id parent);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);
    virtual void attach_transforms(const render_parent *parent);
    void update_streamlines(const class drawable_obj *draw,
			    char message[], const int mlen);

  protected:
    streamlines(CCP3_Renderers_Volumes_streamline_base *d,
		const int i, const bool sp, const string n);

    int get_type() const;
    CCP3_Renderers_Volumes_streamline_base *get_obj() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Volumes_streamline_base *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class periodic_streamlines : public streamlines {
  public:
    periodic_streamlines(const OMobj_id id, const int i,
			 const bool sp, const string n);

    void attach_transforms(const render_parent *parent);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<DLV::streamlines>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

  class display_3D_region : public display_obj {
  public:
    display_3D_region(const OMobj_id id, const int i,
		      const bool sp, const string n);
    ~display_3D_region();

    void attach_data(const drawable_obj *draw, const string name,
		     const OMobj_id parent);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);
    bool attach_editor(const bool v, const class toolkit_obj &t,
		       float matrix[4][4], float translate[3],
		       float centre[3], char message[], const int mlen) const;

  protected:
    int get_type() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Volumes_region3D *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class display_sphere_obj : public display_obj {
  public:
    display_sphere_obj(const OMobj_id id, const int i,
		       const bool sp, const string n);
    ~display_sphere_obj();

    void attach_data(const drawable_obj *draw, const string name,
		     const OMobj_id parent);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);

  protected:
    int get_type() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Volumes_sphere *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class display_plane : public display_obj {
  public:
    display_plane(const OMobj_id id, const int i,
		  const bool sp, const string n);
    ~display_plane();

    void attach_data(const drawable_obj *draw, const string name,
		     const OMobj_id parent);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);
    bool attach_editor(const bool v, const class toolkit_obj &t,
		       float matrix[4][4], float translate[3],
		       float centre[3], char message[], const int mlen) const;

  protected:
    int get_type() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Planes_plane *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class display_line : public display_obj {
  public:
    display_line(const OMobj_id id, const int i,
		 const bool sp, const string n);
    ~display_line();

    void attach_data(const drawable_obj *draw, const string name,
		     const OMobj_id parent);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);

  protected:
    int get_type() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Lines_line *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class display_point : public display_obj {
  public:
    display_point(const OMobj_id id, const int i,
		  const bool sp, const string n);
    ~display_point();

    void attach_data(const drawable_obj *draw, const string name,
		     const OMobj_id parent);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);

  protected:
    int get_type() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Points_point *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class display_wulff_region : public display_obj {
  public:
    display_wulff_region(const OMobj_id id, const int i, const string n);
    ~display_wulff_region();

    void set_colours(const render_parent *parent);
    void attach_data(const drawable_obj *draw, const string name,
		     const OMobj_id parent);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);

  protected:
    int get_type() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Volumes_wulff_plot *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class display_leed_pattern : public display_obj {
  public:
    display_leed_pattern(const OMobj_id id, const int i, const string n);
    ~display_leed_pattern();

    void attach_data(const drawable_obj *draw, const string name,
		     const OMobj_id parent, const char *model);
    void reattach_data(const class drawable_obj *draw,
		       const render_parent *parent);
    void set_background(const render_parent *parent);

  protected:
    int get_type() const;
    void attach_params() const;

  private:
    CCP3_Renderers_Plots_LEED_pattern *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

}

inline DLV::display_text_data::display_text_data(OMobj_id id, const int i,
						 const string n)
  : display_obj(i, n, false), obj(new CCP3_Renderers_Points_text_message(id))
{
}

inline DLV::display_real_data::display_real_data(OMobj_id id, const int i,
						 const string n)
  : display_obj(i, n, false), obj(new CCP3_Renderers_Points_real_scalar(id))
{
}

inline DLV::display_file_view::display_file_view(OMobj_id id, const int i,
						 const string n)
  : display_obj(i, n, false), obj(new CCP3_Renderers_Points_file_viewer(id))
{
}

inline DLV::display_text_view::display_text_view(OMobj_id id, const int i,
						 const string n)
  : display_obj(i, n, false), obj(new CCP3_Renderers_Points_buffer_view(id))
{
}

inline DLV::display_atom_bond_data::display_atom_bond_data(OMobj_id id,
							   const int i,
							   const string n)
  : display_obj(i, n, false), obj(new CCP3_Renderers_Points_atom_bond_text(id))
{
}

inline DLV::display_atom_text_data::display_atom_text_data(OMobj_id id,
							   const int i,
							   const string n)
  : display_obj(i, n, false), obj(new CCP3_Renderers_Points_labels(id))
{
}

inline DLV::display_atom_vector_data::display_atom_vector_data(OMobj_id id,
							       const int i,
							       const string n)
  : display_obj(i, n, false), obj(new CCP3_Renderers_Points_vector_glyphs(id))
{
}

inline DLV::display_atom_trajectory::display_atom_trajectory(OMobj_id id,
							     const int i,
							     const string n)
  : display_obj(i, n, false), obj(new CCP3_Renderers_Points_trajectory(id))
{
}

inline DLV::isosurface::isosurface(OMobj_id id, const int i, const bool sp,
				   const string n)
  : display_obj(i, n, sp), obj(new CCP3_Renderers_Volumes_isosurface(id))
{
}

inline DLV::isosurface::isosurface(CCP3_Renderers_Volumes_isosurface_base *d,
				   const int i, const bool sp, const string n)
  : display_obj(i, n, sp), obj(d)
{
}

inline DLV::periodic_isosurface::periodic_isosurface(OMobj_id id, const int i,
						     const bool sp,
						     const string n)
  : isosurface(new CCP3_Renderers_Volumes_isosurface_rep(id), i, sp, n)
{
}

inline CCP3_Renderers_Volumes_isosurface_base *DLV::isosurface::get_obj() const
{
  return obj;
}

inline DLV::iso_wavefn::iso_wavefn(OMobj_id id, const int i, const bool sp,
				   const string n)
  : display_obj(i, n, sp), obj(new CCP3_Renderers_Volumes_iso_wavefn(id))
{
}

inline DLV::iso_bloch::iso_bloch(OMobj_id id, const int i, const int sa,
				 const int sb, const int sc,
				 const bool sp, const string n)
  : iso_wavefn(new CCP3_Renderers_Volumes_iso_wavefn_rep(id),
	       i, sa, sb, sc, sp, n)
{
}

inline CCP3_Renderers_Volumes_iso_wavefn_base *DLV::iso_wavefn::get_obj() const
{
  return obj;
}

inline DLV::cells_obj::cells_obj(OMobj_id id, const int i,
				 const bool sp, const string n)
  : display_obj(i, n, sp), obj(new CCP3_Renderers_Volumes_shrink_cells(id))
{
}

inline DLV::cells_obj::cells_obj(CCP3_Renderers_Volumes_shrink_cells_base *d,
				 const int i, const bool sp, const string n)
  : display_obj(i, n, sp), obj(d)
{
}

inline DLV::periodic_cells::periodic_cells(OMobj_id id, const int i,
					   const bool sp, const string n)
  : cells_obj(new CCP3_Renderers_Volumes_shrink_cells_rep(id), i, sp, n)
{
}

inline CCP3_Renderers_Volumes_shrink_cells_base *
DLV::cells_obj::get_obj() const
{
  return obj;
}

inline DLV::spheres::spheres(OMobj_id id, const int i, const bool sp,
			     const string n)
  : display_obj(i, n, sp), obj(new CCP3_Renderers_Volumes_spheres(id))
{
}

inline DLV::spheres::spheres(CCP3_Renderers_Volumes_spheres_base *d,
			     const int i, const bool sp, const string n)
  : display_obj(i, n, sp), obj(d)
{
}

inline DLV::periodic_spheres::periodic_spheres(OMobj_id id, const int i,
					       const bool sp, const string n)
  : spheres(new CCP3_Renderers_Volumes_spheres_rep(id), i, sp, n)
{
}

inline CCP3_Renderers_Volumes_spheres_base *DLV::spheres::get_obj() const
{
  return obj;
}

inline DLV::volume_view::volume_view(OMobj_id id, const int i,
				     const bool sp, const string n)
  : display_obj(i, n, sp), obj(new CCP3_Renderers_Volumes_volume_render(id))
{
}

inline DLV::volume_view::volume_view(CCP3_Renderers_Volumes_volume_render *d,
				     const int i, const bool sp, const string n)
  : display_obj(i, n, sp), obj(d)
{
}

inline CCP3_Renderers_Volumes_volume_render *DLV::volume_view::get_obj() const
{
  return obj;
}

inline DLV::hedgehog::hedgehog(OMobj_id id, const int i, const bool sp,
			       const string n)
  : display_obj(i, n, sp), obj(new CCP3_Renderers_Volumes_hedgehog(id))
{
}

inline DLV::hedgehog::hedgehog(CCP3_Renderers_Volumes_hedgehog_base *d,
			       const int i, const bool sp, const string n)
  : display_obj(i, n, sp), obj(d)
{
}

inline DLV::periodic_hedgehog::periodic_hedgehog(OMobj_id id, const int i,
						 const bool sp, const string n)
  : hedgehog(new CCP3_Renderers_Volumes_hedgehog_rep(id), i, sp, n)
{
}

inline CCP3_Renderers_Volumes_hedgehog_base *DLV::hedgehog::get_obj() const
{
  return obj;
}

inline DLV::streamlines::streamlines(OMobj_id id, const int i, const bool sp,
				     const string n)
  : display_obj(i, n, sp), obj(new CCP3_Renderers_Volumes_streamlines(id))
{
}

inline DLV::streamlines::streamlines(CCP3_Renderers_Volumes_streamline_base *d,
				     const int i, const bool sp,
				     const string n)
  : display_obj(i, n, sp), obj(d)
{
}

inline
DLV::periodic_streamlines::periodic_streamlines(OMobj_id id, const int i,
						const bool sp, const string n)
  : streamlines(new CCP3_Renderers_Volumes_streamlines_rep(id), i, sp, n)
{
}

inline CCP3_Renderers_Volumes_streamline_base *
DLV::streamlines::get_obj() const
{
  return obj;
}

inline DLV::ag_contours::ag_contours(OMobj_id id, const int i,
				     const bool sp, const string n)
  : display_obj(i, n, sp), obj(new CCP3_Renderers_Planes_AGisoline(id))
{
}

inline DLV::ag_contours::ag_contours(CCP3_Renderers_Planes_AGisoline_base *d,
				     const int i, const bool sp,
				     const string n)
  : display_obj(i, n, sp), obj(d)
{
}

inline
DLV::periodic_ag_contours::periodic_ag_contours(OMobj_id id, const int i,
						const bool sp, const string n)
  : ag_contours(new CCP3_Renderers_Planes_AGisoline_rep(id), i, sp, n)
{
}

inline CCP3_Renderers_Planes_AGisoline_base *DLV::ag_contours::get_obj() const
{
  return obj;
}

inline DLV::contours::contours(OMobj_id id, const int i, const bool sp,
			       const string n)
  : display_obj(i, n, sp), obj(new CCP3_Renderers_Planes_isoline(id))
{
}

inline DLV::contours::contours(CCP3_Renderers_Planes_isoline_base *d,
			       const int i, const bool sp, const string n)
  : display_obj(i, n, sp), obj(d)
{
}

inline DLV::periodic_contours::periodic_contours(OMobj_id id, const int i,
						 const bool sp, const string n)
  : contours(new CCP3_Renderers_Planes_isoline_rep(id), i, sp, n)
{
}

inline CCP3_Renderers_Planes_isoline_base *DLV::contours::get_obj() const
{
  return obj;
}

inline DLV::pn_contours::pn_contours(OMobj_id id, const int i,
				     const bool sp, const string n)
  : display_obj(i, n, sp), obj(new CCP3_Renderers_Planes_disoline(id))
{
}

inline DLV::pn_contours::pn_contours(CCP3_Renderers_Planes_disoline_base *d,
				     const int i, const bool sp,
				     const string n)
  : display_obj(i, n, sp), obj(d)
{
}

inline
DLV::periodic_pn_contours::periodic_pn_contours(OMobj_id id, const int i,
						const bool sp, const string n)
  : pn_contours(new CCP3_Renderers_Planes_disoline_rep(id), i, sp, n)
{
}

inline CCP3_Renderers_Planes_disoline_base *DLV::pn_contours::get_obj() const
{
  return obj;
}

inline DLV::solid_contours::solid_contours(OMobj_id id, const int i,
					   const bool sp, const string n)
  : display_obj(i, n, sp), obj(new CCP3_Renderers_Planes_solid_contour(id))
{
}

inline
DLV::solid_contours::solid_contours(CCP3_Renderers_Planes_solid_contour_base
				    *d, const int i, const bool sp,
				    const string n)
  : display_obj(i, n, sp), obj(d)
{
}

DLV::periodic_solid_contours::periodic_solid_contours(OMobj_id id, const int i,
						      const bool sp,
						      const string n)
  : solid_contours(new CCP3_Renderers_Planes_solid_contour_rep(id), i, sp, n)
{
}

inline CCP3_Renderers_Planes_solid_contour_base *
DLV::solid_contours::get_obj() const
{
  return obj;
}

inline DLV::shaded_contours::shaded_contours(OMobj_id id, const int i,
					     const bool sp, const string n)
  : display_obj(i, n, sp), obj(new CCP3_Renderers_Planes_shaded_contour(id))
{
}

inline
DLV::shaded_contours::shaded_contours(
		      CCP3_Renderers_Planes_shaded_contour_base *d,
		      const int i, const bool sp, const string n)
  : display_obj(i, n, sp), obj(d)
{
}

inline
DLV::periodic_shaded_contours::periodic_shaded_contours(OMobj_id id,
							const int i,
							const bool sp,
							const string n)
  : shaded_contours(new CCP3_Renderers_Planes_shaded_contour_rep(id), i, sp, n)
{
}

inline CCP3_Renderers_Planes_shaded_contour_base *
DLV::shaded_contours::get_obj() const
{
  return obj;
}

inline DLV::surface_plot::surface_plot(OMobj_id id, const int i,
				       const bool sp, const string n)
  : display_obj(i, n, sp), obj(new CCP3_Renderers_Planes_surf_plot(id))
{
}

inline
DLV::surface_plot::surface_plot(CCP3_Renderers_Planes_surf_plot_base *d,
				const int i, const bool sp, const string n)
  : display_obj(i, n, sp), obj(d)
{
}

inline
DLV::periodic_surface_plot::periodic_surface_plot(OMobj_id id, const int i,
						  const bool sp,
						  const string n)
  : surface_plot(new CCP3_Renderers_Planes_surf_plot_rep(id), i, sp, n)
{
}

inline CCP3_Renderers_Planes_surf_plot_base *DLV::surface_plot::get_obj() const
{
  return obj;
}

inline DLV::plot_obj::plot_obj(CCP3_Renderers_Plots_plot_base *d,
			       const int i, const string n)
  : display_obj(i, n, false), obj(d)
{
}

inline DLV::display_dos::display_dos(OMobj_id id, const int i, const string n)
  : plot_obj(new CCP3_Renderers_Plots_plot_dos(id), i, n)
{
}

inline DLV::display_rod1d::display_rod1d(OMobj_id id, const int i, const string n)
  : plot_obj(new CCP3_Renderers_Plots_plot_rod1d(id), i, n)
{
}

inline DLV::display_rod2d::display_rod2d(OMobj_id id, const int i, const string n)
  : plot_obj(new CCP3_Renderers_Plots_plot_rod2d(id), i, n)
{
}

inline DLV::display_band::display_band(OMobj_id id, const int i,
				       const string n)
  : plot_obj(new CCP3_Renderers_Plots_plot_band(id), i, n)
{
}

inline DLV::display_band::display_band(CCP3_Renderers_Plots_plot_base *d,
				       const int i, const string n)
  : plot_obj(d, i, n)
{
}

inline DLV::display_bspin::display_bspin(OMobj_id id, const int i,
					 const string n)
  : display_band(new CCP3_Renderers_Plots_plot_bspin(id), i, n)
{
}

inline DLV::display_3D_region::display_3D_region(OMobj_id id,
						 const int i, const bool sp,
						 const string n)
  : display_obj(i, n, sp), obj(new CCP3_Renderers_Volumes_region3D(id))
{
}

inline DLV::display_sphere_obj::display_sphere_obj(OMobj_id id,
						   const int i, const bool sp,
						   const string n)
  : display_obj(i, n, sp), obj(new CCP3_Renderers_Volumes_sphere(id))
{
}

inline DLV::display_plane::display_plane(OMobj_id id, const int i,
					 const bool sp, const string n)
  : display_obj(i, n, sp), obj(new CCP3_Renderers_Planes_plane(id))
{
}

inline DLV::display_line::display_line(OMobj_id id, const int i,
				       const bool sp, const string n)
  : display_obj(i, n, sp), obj(new CCP3_Renderers_Lines_line(id))
{
}

inline DLV::display_point::display_point(OMobj_id id, const int i,
					 const bool sp, const string n)
  : display_obj(i, n, sp), obj(new CCP3_Renderers_Points_point(id))
{
}

inline DLV::display_wulff_region::display_wulff_region(OMobj_id id,
						       const int i,
						       const string n)
  : display_obj(i, n, false), obj(new CCP3_Renderers_Volumes_wulff_plot(id))
{
}

inline DLV::display_leed_pattern::display_leed_pattern(OMobj_id id,
						       const int i,
						       const string n)
  : display_obj(i, n, false), obj(new CCP3_Renderers_Plots_LEED_pattern(id))
{
}

#endif // DLV_DISPLAY_PRIVATE
