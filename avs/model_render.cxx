
#ifdef ENABLE_DLV_GRAPHICS
#  if defined(DLV_USES_VTK_GRAPHICS)
#    error "This graphics interface should not be built when using the VTK graphics library"
#  elif !defined(DLV_USES_AVS_GRAPHICS)
#    error "DLV_USES_AVS_GRAPHICS should also be defined when building this graphics interface"
#  endif // Check for AVS/VTK
#else
#  error "ENABLE DLV_GRAPHICS and DLV_USES_AVS_GRAPHICS should be defined when building this graphics interface"
#endif // GRAPHICS check

#include "avs/omx.hxx"
#include "../fld/Xfld.h"
#include "avs/src/express/model.hxx"
#include "src/dlv/types.hxx"
#include "src/dlv/boost_lib.hxx"
#include "../graphics/toolkit_obj.hxx"
#include "../graphics/render_base.hxx"
#include "../graphics/model_render.hxx"

DLV::model_display_obj::~model_display_obj()
{
}

template <class render_t>
DLV::rspace_model_templ<render_t>::~rspace_model_templ()
{
  delete obj;
}

template <class render_t>
DLV::kspace_model_templ<render_t>::~kspace_model_templ()
{
  delete obj;
}

template <class render_t>
DLV::outline_model_templ<render_t>::~outline_model_templ()
{
  delete obj;
}

template <class render_t>
DLV::shell_model_templ<render_t>::~shell_model_templ()
{
  delete obj;
}

template <class render_t>
DLV::rspace_model_templ<render_t>::rspace_model_templ(render_parent *p)
  : lattice_attached(false), lattice_labels(false), opaque_atoms(false),
    transparent_atoms(false), edit_atoms(false), opaque_line_bonds(false),
    transparent_line_bonds(false), edit_line_bonds(false),
    opaque_tube_bonds(false), transparent_tube_bonds(false),
    edit_tube_bonds(false), polyhedra(false)
{
  toolkit_obj space = p->get_rspace();
  obj = new CCP3_Renderers_Model_Model_R(space.get_id(), "Model");
  // Make in/out links to parent objects
  OMobj_id out = OMfind_str_subobj(space.get_id(), "obj", OM_OBJ_RW);
  OMadd_obj_ref(out, obj->GroupObject.obj.obj_id(), 0);
  OMadd_obj_ref(out, obj->EditAtoms.obj.obj_id(), 0);
  out = OMfind_str_subobj(space.get_id(), "props", OM_OBJ_RW);
  OMadd_obj_ref(obj->props.obj_id(), out, 0);
  out = OMfind_str_subobj(space.get_id(), "nselected_atoms", OM_OBJ_RW);
  OMadd_obj_ref(out, obj->atoms.opaque.select_atom.nselects.obj_id(), 0);
  out = OMfind_str_subobj(space.get_id(), "nframes", OM_OBJ_RW);
  OMadd_obj_ref(out, obj->nframes.obj_id(), 0);
  out = OMfind_str_subobj(space.get_id(), "end_frame", OM_OBJ_RW);
  OMadd_obj_ref(out, obj->end_index.obj_id(), 0);
  out = OMfind_str_subobj(space.get_id(), "cur_frame", OM_OBJ_RW);
  OMadd_obj_ref(out, obj->cur_frame.obj_id(), 0);
  out = OMfind_str_subobj(space.get_id(), "event", OM_OBJ_RW);
  //if (OMis_null_obj(out))
  //  fprintf(stderr, "Couldn't find object\n");
  OMset_obj_ref(obj->event.obj_id(), out, 0);
  // Copy the default values for the settings.
  OMobj_id data = OMfind_str_subobj(OMinst_obj,
				    "DLV.Display_Objs.default_data.rspace",
				    OM_OBJ_RW);
  OMset_obj_val(obj->options.obj_id(), data, 0);
  // foreground colours?
  float c[3];
  p->get_inverted_background(c);
  float *ptr =
    (float *) obj->GroupObject.Props.col.ret_array_ptr(OM_GET_ARRAY_WR);
  ptr[0] = c[0];
  ptr[1] = c[0];
  ptr[2] = c[2];
  obj->GroupObject.Props.col.free_array(ptr);
}

template <class render_t>
DLV::kspace_model_templ<render_t>::kspace_model_templ(render_parent *p)
  : lattice_attached(false)
{
  toolkit_obj space = p->get_kspace();
  obj = new CCP3_Renderers_Model_Model_K(space.get_id(), "Model");
  // Make in/out links to parent objects
  OMobj_id out = OMfind_str_subobj(space.get_id(), "obj", OM_OBJ_RW);
  OMadd_obj_ref(out, obj->GroupObject.obj.obj_id(), 0);
  out = OMfind_str_subobj(space.get_id(), "props", OM_OBJ_RW);
  OMadd_obj_ref(obj->props.obj_id(), out, 0);
  // Copy the default values for the settings.
  OMobj_id data = OMfind_str_subobj(OMinst_obj,
				    "DLV.Display_Objs.default_data.kspace",
				    OM_OBJ_RW);
  OMset_obj_val(obj->options.obj_id(), data, 0);
  // foreground colours?
  float c[3];
  p->get_inverted_background(c);
  float *ptr =
    (float *) obj->GroupObject.Props.col.ret_array_ptr(OM_GET_ARRAY_WR);
  ptr[0] = c[0];
  ptr[1] = c[0];
  ptr[2] = c[2];
  obj->GroupObject.Props.col.free_array(ptr);
}

template <class render_t> void
DLV::rspace_model_templ<render_t>::copy_settings(const rspace_model_templ *parent)
{
  OMset_obj_val(obj->options.obj_id(), parent->obj->options.obj_id(), 0);
}

template <class render_t>
DLV::toolkit_obj DLV::rspace_model_templ<render_t>::get_ui_obj() const
{
  return toolkit_obj(obj->options.obj_id());
}

template <class render_t>
DLV::toolkit_obj DLV::kspace_model_templ<render_t>::get_ui_obj() const
{
  return toolkit_obj(obj->options.obj_id());
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::deactivate_atom_flags()
{
  obj->options.atoms.flag_data_available = 0;
  obj->options.atoms.use_flag_data = 0;
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::activate_atom_flags(const string label)
{
    obj->options.atoms.flag_data_available = 1;
    obj->options.atoms.use_flag_data = 1;
    OMset_str_val(obj->options.atoms.flag_data_label.obj_id(), label.c_str());
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::toggle_atom_flags(const string label)
{
  int use = (int)obj->options.atoms.flag_data_available;
  if (use == 1) {
    obj->options.atoms.flag_data_available = 0;
    obj->options.atoms.use_flag_data = 0;
  } else {
    obj->options.atoms.flag_data_available = 1;
    obj->options.atoms.use_flag_data = 1;
    OMset_str_val(obj->options.atoms.flag_data_label.obj_id(), label.c_str());
  }
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::get_lattice_data(bool &draw,
							 bool &labels) const
{
  draw = (bool)obj->options.lattice.draw;
  labels = (bool)obj->options.lattice.label;
}

template <class render_t>
void DLV::kspace_model_templ<render_t>::get_lattice_data(bool &draw,
							 bool &labels) const
{
  draw = (bool)obj->options.lattice.draw;
  labels = (bool)obj->options.lattice.label;
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::set_lattice(float ipoints[][3],
					  float fpoints[][3], const int n)
{
  obj->draw_lattice.nlines = n;
  xp_long size = 3 * n;
  int ok = obj->draw_lattice.initial_coords.set_array(OM_TYPE_FLOAT, ipoints,
						 size, OM_SET_ARRAY_COPY);
  ok = obj->draw_lattice.final_coords.set_array(OM_TYPE_FLOAT, fpoints,
						size, OM_SET_ARRAY_COPY);
  if (!lattice_attached) {
    OMadd_obj_ref(obj->GroupObject.child_objs.obj_id(),
		  obj->draw_lattice.obj.obj_id(), 0);
    lattice_attached = true;
  }
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::set_lattice_labels(float points[][3],
						 const char *labels[],
						 const int n)
{
  obj->draw_lattice.lattice_labels.nlabels = n;
  xp_long size = 3 * n;
  int ok =
    obj->draw_lattice.lattice_labels.coords.set_array(OM_TYPE_FLOAT,
						      points, size,
						      OM_SET_ARRAY_COPY);
  for (int i = 0; i < n; i++)
    ok = OMset_str_array_val(obj->draw_lattice.lattice_labels.text.obj_id(),
			     i, labels[i]);
  if (!lattice_labels) {
    OMadd_obj_ref(obj->draw_lattice.GroupObject.child_objs.obj_id(),
    		  obj->draw_lattice.lattice_labels.labels.obj.obj_id(), 0);
    lattice_labels = true;
  }
}

template <class render_t>
void DLV::kspace_model_templ<render_t>::set_lattice(float vertices[][3],
						    const int nv,
						    long connects[],
						    const long nc)
{
  obj->draw_lattice.nvertices = nv;
  xp_long size = 3 * nv;
  int ok = obj->draw_lattice.vertices.set_array(OM_TYPE_FLOAT, vertices,
						size, OM_SET_ARRAY_COPY);
  size = nc;
  // Todo - possibly dangerous assumption about long vs xp_long?
  ok = obj->draw_lattice.connects.set_array(OM_TYPE_LONG, connects,
					    size, OM_SET_ARRAY_COPY);
  if (!lattice_attached) {
    OMadd_obj_ref(obj->GroupObject.child_objs.obj_id(),
		  obj->draw_lattice.obj.obj_id(), 0);
    lattice_attached = true;
  }
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_lattice()
{
  if (lattice_attached) {
    OMdel_obj_ref(obj->GroupObject.child_objs.obj_id(),
    		  obj->draw_lattice.obj.obj_id(), 0);
    lattice_attached = false;
  }
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_lattice_labels()
{
  if (lattice_labels) {
    OMdel_obj_ref(obj->draw_lattice.GroupObject.child_objs.obj_id(),
    		  obj->draw_lattice.lattice_labels.labels.obj.obj_id(), 0);
    lattice_labels = false;
  }
}

template <class render_t>
void DLV::kspace_model_templ<render_t>::detach_lattice()
{
  if (lattice_attached) {
    OMdel_obj_ref(obj->GroupObject.child_objs.obj_id(),
		  obj->draw_lattice.obj.obj_id(), 0);
    lattice_attached = false;
  }
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::get_atom_data(bool &draw, float &scale,
						      int &select,
						      bool &use_flags,
						      int &flag_type) const
{
  draw = (bool)obj->options.atoms.draw;
  scale = (float)obj->options.atoms.scale;
  select = (int)obj->options.atoms.selection_method;
  // Todo?
  use_flags = (bool)obj->options.atoms.use_flag_data;
  flag_type = (int)obj->options.atoms.flag_method;
}

template <class render_t>
int DLV::rspace_model_templ<render_t>::get_atom_group_index() const
{
  return (int)obj->options.atoms.object_groups;
}

template <class render_t>
int DLV::rspace_model_templ<render_t>::get_selection_symmetry() const
{
  /*if ((int)obj->options.atoms.select_symmetry_copies == 1)
    return 0;
  else if ((int)obj->options.atoms.select_translation_copies == 1)
    return 1;
  else
  return 2;*/
  return (int)obj->options.atoms.symmetry_copies;
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::set_selection_symmetry(const int s)
{
  // This is ugly but gets around an issue with the toolbar
  //obj->options.atoms.symmetry_copies = s;
  OMobj_id obj = OMnull_obj;
  if (s == 1)
    obj = OMfind_str_subobj(OMinst_obj, "DLV.app_window.ModelUI.menu_display"
			    ".atoms.renderUI.atomUI.select_translations.set",
			    OM_OBJ_RW);
  else if (s == 2)
    obj = OMfind_str_subobj(OMinst_obj, "DLV.app_window.ModelUI.menu_display"
			    ".atoms.renderUI.atomUI.select_atom.set",
			    OM_OBJ_RW);
  else
    obj = OMfind_str_subobj(OMinst_obj, "DLV.app_window.ModelUI.menu_display"
			    ".atoms.renderUI.atomUI.select_symmetry.set",
			    OM_OBJ_RW);
  OMset_int_val(obj, 1);
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::draw_opaque_atoms(float coords[][3],
							  float colours[][3],
							  float radii[],
							  const int natoms,
							  const int nframes)
{
  obj->atoms.nframes = nframes;
  obj->atoms.nopaque = (xp_long)natoms;
  xp_long size = 3 * nframes * natoms;
  int ok = obj->atoms.opaque.sequence.set_array(OM_TYPE_FLOAT, coords,
						size, OM_SET_ARRAY_COPY);
  size = 3 * natoms;
  ok = obj->atoms.opaque.colours.set_array(OM_TYPE_FLOAT, colours,
					   size, OM_SET_ARRAY_COPY);
  size = natoms;
  ok = obj->atoms.opaque.radii.set_array(OM_TYPE_FLOAT, radii,
					 size, OM_SET_ARRAY_COPY);
  if (!opaque_atoms) {
    OMadd_obj_ref(obj->atoms.GroupObject.child_objs.obj_id(),
    		  obj->atoms.opaque.obj.obj_id(), 0);
    opaque_atoms = true;
    if (!transparent_atoms)
      OMadd_obj_ref(obj->GroupObject.child_objs.obj_id(),
		    obj->atoms.GroupObject.obj.obj_id(), 0);
  }
}

template <class render_t> void
DLV::rspace_model_templ<render_t>::draw_transparent_atoms(float coords[][3],
							  float colours[][3],
							  float radii[],
							  const int natoms,
							  const int nframes)
{
  obj->atoms.nframes = nframes;
  obj->atoms.ntransparent = (xp_long)natoms;
  xp_long size = 3 * nframes * natoms;
  int ok = obj->atoms.transparent.sequence.set_array(OM_TYPE_FLOAT, coords,
						     size, OM_SET_ARRAY_COPY);
  size = 3 * natoms;
  ok = obj->atoms.transparent.colours.set_array(OM_TYPE_FLOAT, colours,
						size, OM_SET_ARRAY_COPY);
  size = natoms;
  ok = obj->atoms.transparent.radii.set_array(OM_TYPE_FLOAT, radii,
					      size, OM_SET_ARRAY_COPY);
  if (!transparent_atoms) {
    // Todo - I don't understand why this doesn't work
    ok = OMadd_obj_ref(obj->atoms.GroupObject.child_objs.obj_id(),
    		       obj->atoms.transparent.obj.obj_id(), 0);
    transparent_atoms = true;
    if (!opaque_atoms)
      OMadd_obj_ref(obj->GroupObject.child_objs.obj_id(),
		    obj->atoms.GroupObject.obj.obj_id(), 0);
  }
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::draw_edit_atoms(float coords[][3],
							float colours[][3],
							float radii[],
							const int natoms,
							const int nframes)
{
  obj->atoms.nframes = nframes;
  obj->atoms.nedit = (xp_long)natoms;
  xp_long size = 3 * nframes * natoms;
  int ok = obj->atoms.edit.sequence.set_array(OM_TYPE_FLOAT, coords,
					      size, OM_SET_ARRAY_COPY);
  size = 3 * natoms;
  ok = obj->atoms.edit.colours.set_array(OM_TYPE_FLOAT, colours,
					 size, OM_SET_ARRAY_COPY);
  size = natoms;
  ok = obj->atoms.edit.radii.set_array(OM_TYPE_FLOAT, radii,
				       size, OM_SET_ARRAY_COPY);
  if (!edit_atoms) {
    OMadd_obj_ref(obj->EditAtoms.child_objs.obj_id(),
    		  obj->atoms.edit.obj.obj_id(), 0);
    edit_atoms = true;
    //if (!transparent_atoms and !opaque_atoms)
    // OMadd_obj_ref(obj->GroupObject.child_objs.obj_id(),
    //	    obj->atoms.GroupObject.obj.obj_id(), 0);
  }
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_atoms()
{
  if (!opaque_atoms and !transparent_atoms)
    OMdel_obj_ref(obj->GroupObject.child_objs.obj_id(),
		  obj->atoms.GroupObject.obj.obj_id(), 0);
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_opaque_atoms()
{
  if (opaque_atoms) {
    obj->atoms.nopaque = 0;
    OMdel_obj_ref(obj->atoms.GroupObject.child_objs.obj_id(),
		  obj->atoms.opaque.obj.obj_id(), 0);
    opaque_atoms = false;
    if (!transparent_atoms and !edit_atoms)
      detach_atoms();      
  }
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_transparent_atoms()
{
  if (transparent_atoms) {
    obj->atoms.ntransparent = 0;
    OMdel_obj_ref(obj->atoms.GroupObject.child_objs.obj_id(),
		  obj->atoms.transparent.obj.obj_id(), 0);
    transparent_atoms = false;
    if (!opaque_atoms and !edit_atoms)
      detach_atoms();      
  }
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_edit_atoms()
{
  if (edit_atoms) {
    obj->atoms.nedit = 0;
    OMdel_obj_ref(obj->EditAtoms.child_objs.obj_id(),
		  obj->atoms.edit.obj.obj_id(), 0);
    edit_atoms = false;
    //if (!transparent_atoms and !opaque_atoms)
    // detach_atoms();      
  }
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_all_atoms()
{
  detach_opaque_atoms();
  detach_transparent_atoms();
  detach_edit_atoms();
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::get_bond_data(bool &draw,
						      bool &polyhedra,
						      float &overlap,
						      bool &tubes,
						      float &radius,
						      int &subdiv) const
{
  draw = (bool)obj->options.bonds.draw;
  polyhedra = (bool)obj->options.bonds.polyhedra;
  overlap = (float)obj->options.bonds.overlap;
  tubes = (bool)obj->options.bonds.tubes;
  radius = (float)obj->options.bonds.tube_radius;
  subdiv = (int)obj->options.bonds.tube_subdiv;
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::draw_opaque_bonds(float coords[][3],
							  float colours[][3],
							  const long nbonds,
							  long connects[],
							  const long nc,
							  const int nframes,
							  const bool tubes,
							  const int frame)
{
  int ok;
  if (tubes) {
    if (frame == 0)
      obj->tube_bonds.nframes = nframes;
    // Todo - don't instance/deinstance, just set max number with no array?
    obj->tube_bonds.nopaque[frame].n = (xp_long)nbonds;
    obj->tube_bonds.opaque.sequence[frame].ndata = nbonds;
    if (nbonds == 0) {
      ok = obj->tube_bonds.opaque.sequence[frame].coords.set_array_size(0);
      ok = obj->tube_bonds.opaque.sequence[frame].colours.set_array_size(0);
      ok =
	obj->tube_bonds.opaque.sequence[frame].connects.set_array_size(0);
    } else {
      xp_long size = 3 * nbonds;
      ok =
	obj->tube_bonds.opaque.sequence[frame].coords.set_array(OM_TYPE_FLOAT,
								coords, size,
							   OM_SET_ARRAY_COPY);
      ok =
	obj->tube_bonds.opaque.sequence[frame].colours.set_array(OM_TYPE_FLOAT,
								 colours, size,
							   OM_SET_ARRAY_COPY);
      size = nc;
      ok =
	obj->tube_bonds.opaque.sequence[frame].connects.set_array(OM_TYPE_LONG,
								  connects,
								  size,
							    OM_SET_ARRAY_COPY);
    }
    if (!opaque_tube_bonds) {
      OMadd_obj_ref(obj->tube_bonds.GroupObject.child_objs.obj_id(),
      		    obj->tube_bonds.opaque.obj.obj_id(), 0);
      opaque_tube_bonds = true;
      if (!transparent_tube_bonds)
	OMadd_obj_ref(obj->GroupObject.child_objs.obj_id(),
		      obj->tube_bonds.GroupObject.obj.obj_id(), 0);
    }
  } else {
    if (frame == 0)
      obj->line_bonds.nframes = nframes;
    obj->line_bonds.nopaque[frame].n = (xp_long)nbonds;
    obj->line_bonds.opaque.sequence[frame].ndata = nbonds;
    if (nbonds == 0) {
      ok = obj->line_bonds.opaque.sequence[frame].coords.set_array_size(0);
      ok = obj->line_bonds.opaque.sequence[frame].colours.set_array_size(0);
      ok =
	obj->line_bonds.opaque.sequence[frame].connects.set_array_size(0);
    } else {
      xp_long size = 3 * nbonds;
      ok =
	obj->line_bonds.opaque.sequence[frame].coords.set_array(OM_TYPE_FLOAT,
								coords, size,
							   OM_SET_ARRAY_COPY);
      ok =
	obj->line_bonds.opaque.sequence[frame].colours.set_array(OM_TYPE_FLOAT,
								 colours, size,
							OM_SET_ARRAY_COPY);
      size = nc;
      ok =
	obj->line_bonds.opaque.sequence[frame].connects.set_array(OM_TYPE_LONG,
								  connects,
								  size,
							   OM_SET_ARRAY_COPY);
    }
    if (!opaque_line_bonds) {
      OMadd_obj_ref(obj->line_bonds.GroupObject.child_objs.obj_id(),
		    obj->line_bonds.opaque.obj.obj_id(), 0);
      opaque_line_bonds = true;
      if (!transparent_line_bonds)
	OMadd_obj_ref(obj->GroupObject.child_objs.obj_id(),
		      obj->line_bonds.GroupObject.obj.obj_id(), 0);
    }
  }
}

template <class render_t> void
DLV::rspace_model_templ<render_t>::draw_transparent_bonds(float coords[][3],
							  float colours[][3],
							  const long nbonds,
							  long connects[],
							  const long nc,
							  const int nframes,
							  const bool tubes,
							  const int frame)
{
  int ok;
  int i = frame;
  if (tubes) {
    if (frame == 0)
      obj->tube_bonds.nframes = nframes;
    obj->tube_bonds.ntransparent[frame].n = (xp_long)nbonds;
    obj->tube_bonds.transparent.sequence[frame].ndata = nbonds;
    if (nbonds == 0) {
      ok =
	obj->tube_bonds.transparent.sequence[frame].coords.set_array_size(0);
      ok =
	obj->tube_bonds.transparent.sequence[frame].colours.set_array_size(0);
      ok =
	obj->tube_bonds.transparent.sequence[frame].connects.set_array_size(0);
    } else {
      xp_long size = 3 * nbonds;
      ok =
	obj->tube_bonds.transparent.sequence[i].coords.set_array(OM_TYPE_FLOAT,
								 coords, size,
						       OM_SET_ARRAY_COPY);
      ok =
       obj->tube_bonds.transparent.sequence[i].colours.set_array(OM_TYPE_FLOAT,
								 colours, size,
							OM_SET_ARRAY_COPY);
      size = nc;
      ok =
      obj->tube_bonds.transparent.sequence[i].connects.set_array(OM_TYPE_LONG,
								 connects,
								 size,
						     OM_SET_ARRAY_COPY);
    }
    if (!transparent_tube_bonds) {
      OMadd_obj_ref(obj->tube_bonds.GroupObject.child_objs.obj_id(),
		    obj->tube_bonds.transparent.obj.obj_id(), 0);
      transparent_tube_bonds = true;
      if (!opaque_tube_bonds)
	OMadd_obj_ref(obj->GroupObject.child_objs.obj_id(),
		      obj->tube_bonds.GroupObject.obj.obj_id(), 0);
    }
  } else {
    if (frame == 0)
      obj->line_bonds.nframes = nframes;
    obj->line_bonds.ntransparent[frame].n = (xp_long)nbonds;
    obj->line_bonds.transparent.sequence[frame].ndata = nbonds;
    if (nbonds == 0) {
      ok =
	obj->line_bonds.transparent.sequence[frame].coords.set_array_size(0);
      ok =
	obj->line_bonds.transparent.sequence[frame].colours.set_array_size(0);
      ok =
     obj->line_bonds.transparent.sequence[frame].connects.set_array_size(0);
    } else {
      xp_long size = 3 * nbonds;
      ok =
	obj->line_bonds.transparent.sequence[i].coords.set_array(OM_TYPE_FLOAT,
								 coords, size,
						      OM_SET_ARRAY_COPY);
      ok =
       obj->line_bonds.transparent.sequence[i].colours.set_array(OM_TYPE_FLOAT,
								 colours, size,
							  OM_SET_ARRAY_COPY);
      size = nc;
      ok =
       obj->line_bonds.transparent.sequence[i].connects.set_array(OM_TYPE_LONG,
								  connects,
								  size,
							   OM_SET_ARRAY_COPY);
    }
    if (!transparent_line_bonds) {
      OMadd_obj_ref(obj->line_bonds.GroupObject.child_objs.obj_id(),
		    obj->line_bonds.transparent.obj.obj_id(), 0);
      transparent_line_bonds = true;
      if (!opaque_line_bonds)
	OMadd_obj_ref(obj->GroupObject.child_objs.obj_id(),
		      obj->line_bonds.GroupObject.obj.obj_id(), 0);
    }
  }
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::draw_edit_bonds(float coords[][3],
							float colours[][3],
							const long nbonds,
							long connects[],
							const long nc,
							const int nframes,
							const bool tubes,
							const int frame)
{
  int ok;
  if (tubes) {
    if (frame == 0)
      obj->tube_bonds.nframes = nframes;
    obj->tube_bonds.nedit[frame].n = (xp_long)nbonds;
    obj->tube_bonds.edit.sequence[frame].ndata = nbonds;
    if (nbonds == 0) {
      ok = obj->tube_bonds.edit.sequence[frame].coords.set_array_size(0);
      ok = obj->tube_bonds.edit.sequence[frame].colours.set_array_size(0);
      ok = obj->tube_bonds.edit.sequence[frame].connects.set_array_size(0);
    } else {
      xp_long size = 3 * nbonds;
      ok = obj->tube_bonds.edit.sequence[frame].coords.set_array(OM_TYPE_FLOAT,
								 coords, size,
							   OM_SET_ARRAY_COPY);
      ok =
	obj->tube_bonds.edit.sequence[frame].colours.set_array(OM_TYPE_FLOAT,
							       colours, size,
							   OM_SET_ARRAY_COPY);
      size = nc;
      ok =
       obj->tube_bonds.edit.sequence[frame].connects.set_array(OM_TYPE_LONG,
							       connects,
							       size,
							    OM_SET_ARRAY_COPY);
    }
    if (!edit_tube_bonds) {
      OMadd_obj_ref(obj->EditAtoms.child_objs.obj_id(),
		    obj->tube_bonds.edit.obj.obj_id(), 0);
      edit_tube_bonds = true;
      //if (!transparent_tube_bonds and !opaque_tube_bonds)
      //	OMadd_obj_ref(obj->GroupObject.child_objs.obj_id(),
      //	      obj->tube_bonds.GroupObject.obj.obj_id(), 0);
    }
  } else {
    if (frame == 0)
      obj->line_bonds.nframes = nframes;
    obj->line_bonds.nedit[frame].n = (xp_long)nbonds;
    obj->line_bonds.edit.sequence[frame].ndata = nbonds;
    if (nbonds == 0) {
      ok = obj->line_bonds.edit.sequence[frame].coords.set_array_size(0);
      ok = obj->line_bonds.edit.sequence[frame].colours.set_array_size(0);
      ok = obj->line_bonds.edit.sequence[frame].connects.set_array_size(0);
    } else {
      xp_long size = 3 * nbonds;
      ok = obj->line_bonds.edit.sequence[frame].coords.set_array(OM_TYPE_FLOAT,
								 coords, size,
							    OM_SET_ARRAY_COPY);
      ok =
	obj->line_bonds.edit.sequence[frame].colours.set_array(OM_TYPE_FLOAT,
							       colours, size,
							   OM_SET_ARRAY_COPY);
      size = nc;
      ok =
       obj->line_bonds.edit.sequence[frame].connects.set_array(OM_TYPE_LONG,
							       connects,
							       size,
							   OM_SET_ARRAY_COPY);
    }
    if (!edit_line_bonds) {
      OMadd_obj_ref(obj->EditAtoms.child_objs.obj_id(),
		    obj->line_bonds.edit.obj.obj_id(), 0);
      edit_line_bonds = true;
      //if (!transparent_line_bonds and !opaque_line_bonds)
      //OMadd_obj_ref(obj->GroupObject.child_objs.obj_id(),
      //	      obj->line_bonds.GroupObject.obj.obj_id(), 0);
    }
  }
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_line_bonds()
{
  if (!opaque_line_bonds and !transparent_line_bonds)
    OMdel_obj_ref(obj->GroupObject.child_objs.obj_id(),
		  obj->line_bonds.GroupObject.obj.obj_id(), 0);
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_tube_bonds()
{
  if (!opaque_tube_bonds and !transparent_tube_bonds)
    OMdel_obj_ref(obj->GroupObject.child_objs.obj_id(),
		  obj->tube_bonds.GroupObject.obj.obj_id(), 0);
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_opaque_bonds()
{
  if (opaque_line_bonds) {
    OMdel_obj_ref(obj->line_bonds.GroupObject.child_objs.obj_id(),
		  obj->line_bonds.opaque.obj.obj_id(), 0);
    opaque_line_bonds = false;
    if (!transparent_line_bonds and !edit_line_bonds)
      this->detach_line_bonds();      
  } else if (opaque_tube_bonds) {
    OMdel_obj_ref(obj->tube_bonds.GroupObject.child_objs.obj_id(),
		  obj->tube_bonds.opaque.obj.obj_id(), 0);
    opaque_tube_bonds = false;
    if (!transparent_tube_bonds and !edit_tube_bonds)
      this->detach_tube_bonds();      
  }
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_transparent_bonds()
{
  if (transparent_line_bonds) {
    OMdel_obj_ref(obj->line_bonds.GroupObject.child_objs.obj_id(),
		  obj->line_bonds.transparent.obj.obj_id(), 0);
    transparent_line_bonds = false;
    if (!opaque_line_bonds and !edit_line_bonds)
      this->detach_line_bonds();
  } else if (transparent_tube_bonds) {
    OMdel_obj_ref(obj->tube_bonds.GroupObject.child_objs.obj_id(),
		  obj->tube_bonds.transparent.obj.obj_id(), 0);
    transparent_tube_bonds = false;
    if (!opaque_tube_bonds and !edit_tube_bonds)
      this->detach_tube_bonds();
  }
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_edit_bonds()
{
  if (edit_line_bonds) {
    OMdel_obj_ref(obj->EditAtoms.child_objs.obj_id(),
		  obj->line_bonds.edit.obj.obj_id(), 0);
    edit_line_bonds = false;
    //if (!transparent_line_bonds and !opaque_line_bonds)
    //detach_line_bonds();      
  } else if (edit_tube_bonds) {
    OMdel_obj_ref(obj->EditAtoms.child_objs.obj_id(),
		  obj->tube_bonds.edit.obj.obj_id(), 0);
    edit_tube_bonds = false;
    //if (!transparent_tube_bonds and !opaque_tube_bonds)
    //detach_tube_bonds();      
  }
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_all_bonds()
{
  detach_opaque_bonds();
  detach_transparent_bonds();
  detach_edit_bonds();
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::draw_polyhedra(float coords[][3],
						       float colours[][3],
						       const long nverts,
						       long connects[],
						       const long nc,
						       const int nframes,
						       const int frame)
{
  int ok;
  if (frame == 0)
    obj->polyhedra.nframes = nframes;
  // Todo - don't instance/deinstance, just set max number with no array?
  //obj->polyhedra.nopaque[frame].n = (xp_long)nverts;
  obj->polyhedra.opaque.sequence[frame].ndata = nverts;
  if (nverts == 0) {
    ok = obj->polyhedra.opaque.sequence[frame].coords.set_array_size(0);
    ok = obj->polyhedra.opaque.sequence[frame].colours.set_array_size(0);
    ok =
      obj->polyhedra.opaque.sequence[frame].connects.set_array_size(0);
  } else {
    xp_long size = 3 * nverts;
    ok =
      obj->polyhedra.opaque.sequence[frame].coords.set_array(OM_TYPE_FLOAT,
							     coords, size,
							  OM_SET_ARRAY_COPY);
    ok =
      obj->polyhedra.opaque.sequence[frame].colours.set_array(OM_TYPE_FLOAT,
							      colours, size,
							   OM_SET_ARRAY_COPY);
    size = nc;
    ok =
      obj->polyhedra.opaque.sequence[frame].connects.set_array(OM_TYPE_LONG,
							       connects,
							       size,
							   OM_SET_ARRAY_COPY);
  }
  if (!polyhedra) {
    //OMadd_obj_ref(obj->polyhedra.GroupObject.child_objs.obj_id(),
    //	  obj->polyhedra.opaque.obj.obj_id(), 0);
    OMadd_obj_ref(obj->GroupObject.child_objs.obj_id(),
		  obj->polyhedra.GroupObject.obj.obj_id(), 0);
    polyhedra = true;
  }
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::detach_polyhedra()
{
  if (polyhedra) {
    OMdel_obj_ref(obj->GroupObject.child_objs.obj_id(),
		  obj->polyhedra.GroupObject.obj.obj_id(), 0);
    polyhedra = false;
  }
}

template <class render_t>
DLV::outline_model_templ<render_t>::outline_model_templ(render_parent *p)
  : data_set(false)
{
  toolkit_obj space = p->get_rspace();
  obj = new CCP3_Renderers_Model_Outline(space.get_id(), "Model");
  // Make in/out links to parent objects
  OMobj_id ntr = OMfind_str_subobj(space.get_id(), "ntransforms", OM_OBJ_RW);
  OMadd_obj_ref(obj->n.obj_id(), ntr, 0);
  OMobj_id xtr = OMfind_str_subobj(space.get_id(), "transforms", OM_OBJ_RW);
  OMadd_obj_ref(obj->Xform.obj_id(), xtr, 0);
  // foreground colours?
  float c[3];
  p->get_inverted_background(c);
  float *ptr =
    (float *) obj->GroupObject.Props.col.ret_array_ptr(OM_GET_ARRAY_WR);
  ptr[0] = c[0];
  ptr[1] = c[0];
  ptr[2] = c[2];
  obj->GroupObject.Props.col.free_array(ptr);
  OMobj_id out = OMfind_str_subobj(space.get_id(), "obj", OM_OBJ_RW);
  OMadd_obj_ref(out, obj->GroupObject.obj.obj_id(), 0);
}

template <class render_t>
DLV::toolkit_obj DLV::outline_model_templ<render_t>::get_ui_obj() const
{
  return toolkit_obj(obj->data.obj_id());
}

template <class render_t>
void DLV::outline_model_templ<render_t>::set_data(float vertices[][3],
						  const int nvertices,
						  int planes[],
						  const int nplanes,
						  float iradius[],
						  float oradius[],
						  float dmin[], float dmax[],
						  int orientation[],
						  const int ncylinders,
						  float radii[],
						  const int nspheres)
{
  if (!data_set) {
    // Todo - be more efficient?
    obj->n_vertices = nvertices;
    obj->n_cylinders = ncylinders;
    obj->n_spheres = nspheres;
    if (nplanes > 0) {
      xp_long size = 3 * nvertices;
      int ok = obj->vertices.set_array(OM_TYPE_FLOAT, vertices,
				       size, OM_SET_ARRAY_COPY);
      size = nplanes;
      ok = obj->nodes.set_array(OM_TYPE_INT, planes, size, OM_SET_ARRAY_COPY);
      size = 0;
      for (int i = 0; i < nplanes; i++)
	size += planes[i];
      xp_long *connects = new xp_long[size];
      for (int i = 0; i < nvertices; i++)
	connects[i] = i;
      ok = obj->connects.set_array(OM_TYPE_LONG, connects,
				   size, OM_SET_ARRAY_COPY);
      delete [] connects;
      size = 2 * nvertices;
      connects = new xp_long[size];
      int k = 0;
      int count = 0;
      for (int i = 0; i < nplanes; i++) {
	for (int j = 0; j < planes[i]; j++) {
	  connects[k] = count + j;
	  connects[k + 1] = count + (j + 1) % planes[i];
	  k += 2;
	}
	count += planes[i];
      }
      ok = obj->lines.set_array(OM_TYPE_LONG, connects,
				size, OM_SET_ARRAY_COPY);
      delete [] connects;
    }
    if (ncylinders > 0) {
      xp_long size = ncylinders;
      int ok = obj->rmin.set_array(OM_TYPE_FLOAT, iradius,
				   size, OM_SET_ARRAY_COPY);
      ok = obj->rmax.set_array(OM_TYPE_FLOAT, oradius,
			       size, OM_SET_ARRAY_COPY);
      ok = obj->zmin.set_array(OM_TYPE_FLOAT, dmin,
			       size, OM_SET_ARRAY_COPY);
      ok = obj->zmax.set_array(OM_TYPE_FLOAT, dmax,
			       size, OM_SET_ARRAY_COPY);
      ok = obj->orientation.set_array(OM_TYPE_INT, orientation,
				      size, OM_SET_ARRAY_COPY);
    }
    if (nspheres > 0) {
      xp_long size = nspheres;
      int ok = obj->rsph.set_array(OM_TYPE_FLOAT, radii,
				   size, OM_SET_ARRAY_COPY);
    }
    data_set = true;
  }
}

template <class render_t>
DLV::shell_model_templ<render_t>::shell_model_templ(render_parent *p)
  : is_drawn(false)
{
  toolkit_obj space = p->get_rspace();
  obj = new CCP3_Renderers_Model_Shell(space.get_id(), "Model");
  // Make in/out links to parent objects
  OMobj_id out = OMfind_str_subobj(space.get_id(), "obj", OM_OBJ_RW);
  OMadd_obj_ref(out, obj->GroupObject.obj.obj_id(), 0);
  // foreground colours?
  float c[3];
  p->get_inverted_background(c);
  float *ptr =
    (float *) obj->GroupObject.Props.col.ret_array_ptr(OM_GET_ARRAY_WR);
  ptr[0] = c[0];
  ptr[1] = c[0];
  ptr[2] = c[2];
  obj->GroupObject.Props.col.free_array(ptr);
}

template <class render_t>
DLV::toolkit_obj DLV::shell_model_templ<render_t>::get_ui_obj() const
{
  return toolkit_obj(obj->subdivisions.obj_id());
}

template <class render_t>
void DLV::shell_model_templ<render_t>::draw(const float colour[3],
					    const int nshells,
					    const float radii[],
					    const float shell_colours[][3])
{
  obj->nshells = nshells;
  obj->central_data.red = colour[0];
  obj->central_data.green = colour[1];
  obj->central_data.blue = colour[2];
  for (int i = 0; i < nshells; i++) {
    obj->shell_data[i].radius = radii[i];
    obj->shell_data[i].red = shell_colours[i][0];
    obj->shell_data[i].green = shell_colours[i][1];
    obj->shell_data[i].blue = shell_colours[i][2];
  }
  if (nshells > 0 and !is_drawn) {
    //OMadd_obj_ref(obj->GroupObject.child_objs.obj_id(),
    //	  obj->shells.obj.obj_id(), 0);
    OMlink_objs(obj->shells.child_objs.obj_id(), obj->mlink.obj_id());
    is_drawn = true;
  }
}

template <class render_t>
void DLV::rspace_model_templ<render_t>::attach_editor(const bool v,
						      const toolkit_obj &t,
						      float matrix[4][4],
						      float translate[3],
						      float centre[3],
						      char message[],
						      const int mlen) const
{
  if (v) {
    // Todo - reset transforms?
    OMset_obj_val(t.get_id(), obj->EditAtoms.obj.obj_id(), 0);
    OMset_int_val(obj->EditAtoms.Top.xform_mode.obj_id(), 0); //Normal
  } else {
    OMset_obj_val(t.get_id(), OMnull_obj, 0);
    // recover transformation
    float (*m)[4] =
      (float (*)[4])obj->EditAtoms.Xform.mat.ret_array_ptr(OM_GET_ARRAY_RW);
    for (int i = 0; i < 4; i++)
      for (int j = 0; j < 4; j++)
	matrix[i][j] = m[i][j];
    for (int i = 0; i < 4; i++) {
      for (int j = 0; j < 4; j++)
	m[i][j] = 0.0;
      m[i][i] = 1.0;
    }
    obj->EditAtoms.Xform.mat.free_array(m);
    float *vec =
      (float *)obj->EditAtoms.Xform.xlate.ret_array_ptr(OM_GET_ARRAY_RW);
    for (int i = 0; i < 3; i++)
      translate[i] = vec[i];
    for (int i = 0; i < 3; i++)
      vec[i] = 0.0;
    obj->EditAtoms.Xform.xlate.free_array(vec);
    vec = (float *)obj->EditAtoms.Xform.center.ret_array_ptr(OM_GET_ARRAY_RW);
    for (int i = 0; i < 3; i++)
      centre[i] = vec[i];
    for (int i = 0; i < 3; i++)
      vec[i] = 0.0;
    obj->EditAtoms.Xform.center.free_array(vec);
    OMset_int_val(obj->EditAtoms.Top.xform_mode.obj_id(), 1); //Parent
  }
}

template class DLV::rspace_model_templ<CCP3_Renderers_Model_Model_R>;
template class DLV::kspace_model_templ<CCP3_Renderers_Model_Model_K>;
template class DLV::outline_model_templ<CCP3_Renderers_Model_Outline>;
template class DLV::shell_model_templ<CCP3_Renderers_Model_Shell>;

#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::model_display_r *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::model_display_r(DLV::render_parent::get_serialize_obj());
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::model_display_k *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::model_display_k(DLV::render_parent::get_serialize_obj());
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::model_outline_r *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::model_outline_r(DLV::render_parent::get_serialize_obj());
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::model_shell_r *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::model_shell_r(DLV::render_parent::get_serialize_obj());
    }

  }
}

template <class render_t> template <class Archive>
void DLV::rspace_model_templ<render_t>::save(Archive &ar,
					     const unsigned int version) const
{
  int i = (int)obj->options.atoms.draw;
  ar << i;
  float f = (float)obj->options.atoms.scale;
  ar << f;
  //ar & (float)obj->options.atoms.tolerance;
  i = (int)obj->options.atoms.selection_method;
  ar << i;
  i = (int)obj->options.atoms.label_selects;
  ar << i;
  i = (int)obj->options.atoms.object_groups;
  ar << i;
  i = (int)obj->options.atoms.symmetry_copies;
  ar << i;
  //ar & (float)obj->props.spheres.opacity;
  //ar & (int)obj->props.spheres.subdiv;
  i = (int)obj->options.lattice.draw;
  ar << i;
  i = (int)obj->options.lattice.label;
  ar << i;
  i = (int)obj->options.bonds.draw;
  ar << i;
  i = (int)obj->options.bonds.polyhedra;
  ar << i;
  i = (int)obj->options.bonds.tubes;
  ar << i;
  f = (float)obj->options.bonds.overlap;
  ar << f;
  f = (float)obj->options.bonds.tube_radius;
  ar << f;
  i = (int)obj->options.bonds.tube_subdiv;
  ar << i;
  // Todo - polyhedra
}

template <class render_t> template <class Archive>
void DLV::rspace_model_templ<render_t>::load(Archive &ar,
					     const unsigned int version)
{
  int i;
  ar >> i;
  obj->options.atoms.draw = i;
  float f;
  ar >> f;
  obj->options.atoms.scale = f;
  //ar & obj->options.atoms.tolerance;
  ar >> i;
  obj->options.atoms.selection_method = i;
  ar >> i;
  obj->options.atoms.label_selects = i;
  ar >> i;
  obj->options.atoms.object_groups = i;
  ar >> i;
  obj->options.atoms.symmetry_copies = i;
  //ar & obj->options.atoms.opacity;
  //ar & obj->options.atoms.sphere_subdiv;
  ar >> i;
  obj->options.lattice.draw = i;
  ar >> i;
  obj->options.lattice.label = i;
  ar >> i;
  obj->options.bonds.draw = i;
  ar >> i;
  obj->options.bonds.polyhedra = i;
  ar >> i;
  obj->options.bonds.tubes = i;
  ar >> f;
  obj->options.bonds.overlap = f;
  ar >> f;
  obj->options.bonds.tube_radius = f;
  ar >> i;
  obj->options.bonds.tube_subdiv = i;
  // Todo - polyhedra
}

template <class render_t> template <class Archive>
void DLV::kspace_model_templ<render_t>::save(Archive &ar,
					     const unsigned int version) const
{
  int i = (int)obj->options.lattice.draw;
  ar << i;
  i = (int)obj->options.lattice.label;
  ar << i;
}

template <class render_t> template <class Archive>
void DLV::kspace_model_templ<render_t>::load(Archive &ar,
					     const unsigned int version)
{
  int i;
  ar >> i;
  obj->options.lattice.draw = i;
  ar >> i;
  obj->options.lattice.label = i;
}

template <class render_t> template <class Archive>
void DLV::outline_model_templ<render_t>::save(Archive &ar,
					      const unsigned int version) const
{
  int i = (int)obj->data.draw_lines;
  ar << i;
  i = (int)obj->data.draw_surface;
  ar << i;
  float f = (float)obj->data.opacity;
  ar << f;
  f = (float)obj->data.red;
  ar << f;
  f = (float)obj->data.green;
  ar << f;
  f = (float)obj->data.blue;
  ar << f;
}

template <class render_t> template <class Archive>
void DLV::outline_model_templ<render_t>::load(Archive &ar,
					      const unsigned int version)
{
  int i;
  ar >> i;
  obj->data.draw_lines = i;
  ar >> i;
  obj->data.draw_surface = i;
  float f;
  ar >> f;
  obj->data.opacity = f;
  ar >> f;
  obj->data.red = f;
  ar >> f;
  obj->data.green = f;
  ar >> f;
  obj->data.blue = f;
}

template <class render_t> template <class Archive>
void DLV::shell_model_templ<render_t>::save(Archive &ar,
					    const unsigned int version) const
{
  // Todo
}

template <class render_t> template <class Archive>
void DLV::shell_model_templ<render_t>::load(Archive &ar,
					   const unsigned int version)
{
  // Todo - obj needs to exist!
}

BOOST_CLASS_EXPORT_GUID(DLV::model_display_r, "DLV::model_display_r")
BOOST_CLASS_EXPORT_GUID(DLV::model_display_k, "DLV::model_display_k")
BOOST_CLASS_EXPORT_GUID(DLV::model_outline_r, "DLV::model_outline_r")
BOOST_CLASS_EXPORT_GUID(DLV::model_shell_r, "DLV::model_shell_r")

#endif // DLV_USES_SERIALIZE
