
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#include "../graphics/toolkit_obj.hxx"
#include "../graphics/render_base.hxx"
#include "../graphics/drawable.hxx"
#include "../graphics/edit_objs.hxx"
#include "avs/src/express/edits.hxx"
#include "edit_impl.hxx"

namespace DLV {

  OMobj_id find_parent(const render_parent *parent, const bool kspace);

}

DLV::edited_obj::~edited_obj()
{
  delete draw;
}

OMobj_id DLV::find_parent(const render_parent *parent, const bool kspace)
{
  OMobj_id space = OMnull_obj;
  if (kspace)
    space = parent->get_kspace().get_id();
  else
    space = parent->get_rspace().get_id();
  OMobj_id id = OMfind_str_subobj(space, "Edits", OM_OBJ_RW);
  return id;
}

void DLV::edited_obj::update_edit_list(const render_parent *parent,
				       const string name, const int index)
{
  parent->add_edit_list(name, get_type(), index);
}

DLV::edited_obj *
DLV::edited_obj::create_orthoslice(const render_parent *parent,
				   const drawable_obj *draw, const bool kspace,
				   char message[], const int mlen)
{
  OMobj_id id = find_parent(parent, kspace);
  orthoslice *edit = new orthoslice(id, kspace);
  if (edit == 0)
    strncpy(message, "Unable to create orthoslice", mlen);
  else
    edit->attach_data(draw);
  return edit;
}

DLV::edited_obj *
DLV::edited_obj::create_slice(const render_parent *parent,
			      const drawable_obj *draw, const bool kspace,
			      char message[], const int mlen)
{
  OMobj_id id = find_parent(parent, kspace);
  slice *edit = new slice(id, kspace);
  if (edit == 0)
    strncpy(message, "Unable to create slice", mlen);
  else
    edit->attach_data(draw);
  return edit;
}

DLV::edited_obj *
DLV::edited_obj::create_clamp(const render_parent *parent,
			      const drawable_obj *draw,
			      const int component, const bool kspace,
			      char message[], const int mlen)
{
  OMobj_id id = find_parent(parent, kspace);
  clamp *edit = new clamp(id, kspace);
  if (edit == 0)
    strncpy(message, "Unable to create clamp", mlen);
  else
    edit->attach_data(draw, component);
  return edit;
}

DLV::edited_obj *
DLV::edited_obj::create_crop(const render_parent *parent,
			     const drawable_obj *draw, const bool kspace,
			     char message[], const int mlen)
{
  OMobj_id id = find_parent(parent, kspace);
  crop *edit = new crop(id, kspace);
  if (edit == 0)
    strncpy(message, "Unable to create crop", mlen);
  else
    edit->attach_data(draw);
  return edit;
}

DLV::edited_obj *
DLV::edited_obj::create_cut(const render_parent *parent,
			    const drawable_obj *draw, const bool kspace,
			    char message[], const int mlen)
{
  OMobj_id id = find_parent(parent, kspace);
  cut *edit = new cut(id, kspace);
  if (edit == 0)
    strncpy(message, "Unable to create cut", mlen);
  else
    edit->attach_data(draw);
  return edit;
}

DLV::edited_obj *
DLV::edited_obj::create_downsize(const render_parent *parent,
				 const drawable_obj *draw, const bool kspace,
				 char message[], const int mlen)
{
  OMobj_id id = find_parent(parent, kspace);
  downsize *edit = new downsize(id, kspace);
  if (edit == 0)
    strncpy(message, "Unable to create downsize", mlen);
  else
    edit->attach_data(draw);
  return edit;
}

DLV::edited_obj *
DLV::edited_obj::create_extension(const render_parent *parent,
				  const drawable_obj *draw, const bool kspace,
				  const int x, const int y, const int z,
				  char message[], const int mlen)
{
  OMobj_id id = find_parent(parent, kspace);
  extension *edit = new extension(id, x, y, z, kspace);
  if (edit == 0)
    strncpy(message, "Unable to create data extension", mlen);
  else
    edit->attach_data(draw);
  return edit;
}

DLV::edited_obj *
DLV::edited_obj::create_math(const render_parent *parent,
			     const drawable_obj *draw, const int component,
			     const bool kspace, char message[], const int mlen)
{
  OMobj_id id = find_parent(parent, kspace);
  data_math *edit = new data_math(id, kspace);
  if (edit == 0)
    strncpy(message, "Unable to create data_math", mlen);
  else
    edit->attach_data(draw, component);
  return edit;
}

DLV::edited_obj *
DLV::edited_obj::create_vol_render(const render_parent *parent,
				   const drawable_obj *draw, const bool kspace,
				   char message[], const int mlen)
{
  OMobj_id id = find_parent(parent, kspace);
  volume_render *edit = new volume_render(id, kspace);
  if (edit == 0)
    strncpy(message, "Unable to create volume render", mlen);
  else
    edit->attach_data(draw);
  return edit;
}

DLV::edited_obj *
DLV::edited_obj::create_spectrum(const render_parent *parent, const int index,
				 char message[], const int mlen)
{
  OMobj_id id = find_parent(parent, false);
  phonon_spectrum *edit = new phonon_spectrum(id, index);
  if (edit == 0)
    strncpy(message, "Unable to create spectrum", mlen);
  else
    edit->attach_data();
  return edit;
}

DLV::edited_obj *
DLV::edited_obj::create_phonon_traj(const render_parent *parent,
				    const int index, char message[],
				    const int mlen)
{
  OMobj_id id = find_parent(parent, false);
  phonon_traj *edit = new phonon_traj(id, index);
  if (edit == 0)
    strncpy(message, "Unable to create trajectory", mlen);
  else
    edit->attach_data();
  return edit;
}

DLV::edited_obj *
DLV::edited_obj::create_dos_shift(const render_parent *parent,
				  const drawable_obj *draw,
				  const bool kspace, char message[],
				  const int mlen)
{
  OMobj_id id = find_parent(parent, kspace);
  dos_shift *edit = new dos_shift(id, kspace);
  if (edit == 0)
    strncpy(message, "Unable to create shift of DOS", mlen);
  else
    edit->attach_data(draw);
  return edit;
}

DLV::edited_obj *
DLV::edited_obj::create_band_shift(const render_parent *parent,
				   const drawable_obj *draw,
				   const bool kspace, char message[],
				   const int mlen)
{
  OMobj_id id = find_parent(parent, kspace);
  band_shift *edit = new band_shift(id, kspace);
  if (edit == 0)
    strncpy(message, "Unable to create shift of Bands", mlen);
  else
    edit->attach_data(draw);
  return edit;
}

DLV::edited_obj *
DLV::edited_obj::create_bdos_shift(const render_parent *parent,
				   const drawable_obj *draw,
				   const bool kspace, char message[],
				   const int mlen)
{
  OMobj_id id = find_parent(parent, kspace);
  bdos_shift *edit = new bdos_shift(id, kspace);
  if (edit == 0)
    strncpy(message, "Unable to create shift of Bands and DOS", mlen);
  else
    edit->attach_data(draw);
  return edit;
}

DLV::orthoslice::~orthoslice()
{
  delete obj;
}

DLV::slice::~slice()
{
  delete obj;
}

DLV::clamp::~clamp()
{
  delete obj;
}

DLV::crop::~crop()
{
  delete obj;
}

DLV::cut::~cut()
{
  delete obj;
}

DLV::downsize::~downsize()
{
  delete obj;
}

DLV::extension::~extension()
{
  delete field;
  delete obj;
}

DLV::data_math::~data_math()
{
  delete obj;
}

DLV::volume_render::~volume_render()
{
  delete obj;
}

DLV::phonon_spectrum::~phonon_spectrum()
{
  delete obj;
}

DLV::phonon_traj::~phonon_traj()
{
  delete obj;
}

DLV::dos_shift::~dos_shift()
{
  delete obj;
}

DLV::band_shift::~band_shift()
{
  delete obj;
}

DLV::bdos_shift::~bdos_shift()
{
  delete obj;
}

int DLV::orthoslice::get_type() const
{
  return (int)EDIT_orthoslice;
}

int DLV::slice::get_type() const
{
  return (int)EDIT_slice;
}

int DLV::clamp::get_type() const
{
  return (int)EDIT_clamp;
}

int DLV::crop::get_type() const
{
  return (int)EDIT_crop;
}

int DLV::cut::get_type() const
{
  return (int)EDIT_cut;
}

int DLV::downsize::get_type() const
{
  return (int)EDIT_downsize;
}

int DLV::extension::get_type() const
{
  return (int)EDIT_extend;
}

int DLV::data_math::get_type() const
{
  return (int)EDIT_math;
}

int DLV::volume_render::get_type() const
{
  return (int)EDIT_vol;
}

int DLV::phonon_spectrum::get_type() const
{
  return (int)EDIT_intens;
}

int DLV::phonon_traj::get_type() const
{
  return (int)EDIT_eigenvec;
}

int DLV::dos_shift::get_type() const
{
  return (int)EDIT_plot;
}

int DLV::band_shift::get_type() const
{
  return (int)EDIT_plot;
}

int DLV::bdos_shift::get_type() const
{
  return (int)EDIT_plot;
}

void DLV::orthoslice::attach_data(const drawable_obj *draw)
{
  // setup drawable
  toolkit_obj t(obj->out_fld.obj_id());
  drawable_obj *out = drawable_obj::create_edit(t);
  set_drawable(out);
  // attach field
  OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::slice::attach_data(const drawable_obj *draw)
{
  // setup drawable
  toolkit_obj t(obj->out_fld.obj_id());
  drawable_obj *out = drawable_obj::create_edit(t);
  set_drawable(out);
  // attach field
  OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::clamp::attach_data(const drawable_obj *draw, const int component)
{
  // setup drawable
  toolkit_obj t(obj->out_fld.obj_id());
  drawable_obj *out = drawable_obj::create_edit(t);
  set_drawable(out);
  // attach field
  OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
  obj->ClampParam.vector = component;
}

void DLV::crop::attach_data(const drawable_obj *draw)
{
  // setup drawable
  toolkit_obj t(obj->out_fld.obj_id());
  drawable_obj *out = drawable_obj::create_edit(t);
  set_drawable(out);
  // attach field
  OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::cut::attach_data(const drawable_obj *draw)
{
  // setup drawable
  toolkit_obj t(obj->out_fld.obj_id());
  drawable_obj *out = drawable_obj::create_edit(t);
  set_drawable(out);
  // attach field
  OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::downsize::attach_data(const drawable_obj *draw)
{
  // setup drawable
  toolkit_obj t(obj->out_fld.obj_id());
  drawable_obj *out = drawable_obj::create_edit(t);
  set_drawable(out);
  // attach field
  OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::extension::attach_data(const drawable_obj *draw)
{
  // setup drawable
  toolkit_obj t(obj->out_fld.obj_id());
  drawable_obj *out = drawable_obj::create_edit(t);
  set_drawable(out);
  // create field - copy drawable one
  toolkit_obj tk(obj->obj_id());
  field = draw->duplicate(tk);
  //OMset_obj_val(obj->Field.obj_id(), draw->get_id().get_id(), 0);
  OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
  OMset_obj_val(obj->out_fld.obj_id(), field->get_id().get_id(), 0);
  // Todo?
  //nx = ;
}

void DLV::data_math::attach_data(const drawable_obj *draw, const int component)
{
  // setup drawable
  toolkit_obj t(obj->out_fld.obj_id());
  drawable_obj *out = drawable_obj::create_edit(t);
  set_drawable(out);
  // attach field
  OMset_obj_val(obj->in_field1.obj_id(), draw->get_id().get_id(), 0);
  obj->params.cmp1 = component;
}

void DLV::volume_render::attach_data(const drawable_obj *draw)
{
  // setup drawable
  toolkit_obj t(obj->out_fld.obj_id());
  drawable_obj *out = drawable_obj::create_edit(t);
  set_drawable(out);
  // attach field
  //Todo OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::phonon_spectrum::attach_data()
{
  // setup drawable
  toolkit_obj t(obj->field.obj_id());
  drawable_obj *out = drawable_obj::create_edit(t);
  set_drawable(out);
  obj->SpectrumData.index = index;
}

void DLV::phonon_traj::attach_data()
{
  // setup drawable
  //toolkit_obj t(obj->field.obj_id());
  //drawable_obj *out = drawable_obj::create_edit(t);
  //set_drawable(out);
  obj->PhononData.index = index;
}

void DLV::dos_shift::attach_data(const drawable_obj *draw)
{
  // setup drawable
  toolkit_obj t(obj->out_dos.obj_id());
  drawable_obj *out = drawable_obj::create_edit(t);
  set_drawable(out);
  // create field - copy drawable one
  OMset_obj_val(obj->dos_data.obj_id(), draw->get_id().get_id(), 0);
  OMset_obj_ref(obj->in_data.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::band_shift::attach_data(const drawable_obj *draw)
{
  // setup drawable
  toolkit_obj t(obj->out_data.obj_id());
  drawable_obj *out = drawable_obj::create_edit(t);
  set_drawable(out);
  // create field - copy drawable one
  OMset_obj_val(obj->band_data.obj_id(), draw->get_id().get_id(), 0);
  OMset_obj_ref(obj->in_data.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::bdos_shift::attach_data(const drawable_obj *draw)
{
  // setup drawable
  toolkit_obj t(obj->out_data.obj_id());
  drawable_obj *out = drawable_obj::create_edit(t);
  set_drawable(out);
  // create field - copy drawable one
  OMset_obj_val(obj->band_data.obj_id(), draw->get_id().get_id(), 0);
  OMset_obj_ref(obj->in_data.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::orthoslice::attach_params() const
{
  static OMobj_id params = OMnull_obj;
  static OMobj_id field = OMnull_obj;
  if (OMis_null_obj(params)) {
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_edit"
			".editUI.edit_panels.OrthoSliceUI.param", OM_OBJ_RW);
    field =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_edit"
			".editUI.edit_panels.OrthoSliceUI.in_fld", OM_OBJ_RW);
  }
  OMset_obj_ref(field, obj->in_field.obj_id(), 0);
  OMset_obj_ref(params, obj->OrthoSliceParam.obj_id(), 0);
}

void DLV::slice::attach_params() const
{
  static OMobj_id params = OMnull_obj;
  static OMobj_id field = OMnull_obj;
  if (OMis_null_obj(params)) {
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_edit"
			".editUI.edit_panels.SliceUI.param", OM_OBJ_RW);
    field =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_edit"
			".editUI.edit_panels.SliceUI.in_fld", OM_OBJ_RW);
  }
  OMset_obj_ref(field, obj->in_field.obj_id(), 0);
  OMset_obj_ref(params, obj->SliceParam.obj_id(), 0);
}

void DLV::clamp::attach_params() const
{
  static OMobj_id params = OMnull_obj;
  static OMobj_id field = OMnull_obj;
  if (OMis_null_obj(params)) {
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_edit"
			".editUI.edit_panels.ClampUI.param", OM_OBJ_RW);
    field =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_edit"
			".editUI.edit_panels.ClampUI.in_fld", OM_OBJ_RW);
  }
  OMset_obj_ref(field, obj->in_field.obj_id(), 0);
  OMset_obj_ref(params, obj->ClampParam.obj_id(), 0);
}

void DLV::crop::attach_params() const
{
  static OMobj_id params = OMnull_obj;
  static OMobj_id field = OMnull_obj;
  if (OMis_null_obj(params)) {
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_edit"
			".editUI.edit_panels.CropUI.param", OM_OBJ_RW);
    field =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_edit"
			".editUI.edit_panels.CropUI.in_fld", OM_OBJ_RW);
  }
  OMset_obj_ref(field, obj->in_field.obj_id(), 0);
  OMset_obj_ref(params, obj->CropParam.obj_id(), 0);
}

void DLV::cut::attach_params() const
{
  static OMobj_id params = OMnull_obj;
  static OMobj_id field = OMnull_obj;
  if (OMis_null_obj(params)) {
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_edit"
			".editUI.edit_panels.CutUI.param", OM_OBJ_RW);
    field =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_edit"
			".editUI.edit_panels.CutUI.in_fld", OM_OBJ_RW);
  }
  OMset_obj_ref(field, obj->in_field.obj_id(), 0);
  OMset_obj_ref(params, obj->CutParam.obj_id(), 0);
}

void DLV::downsize::attach_params() const
{
  static OMobj_id params = OMnull_obj;
  static OMobj_id field = OMnull_obj;
  if (OMis_null_obj(params)) {
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_edit"
			".editUI.edit_panels.DownsizeUI.param", OM_OBJ_RW);
    field =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_edit"
			".editUI.edit_panels.DownsizeUI.in_fld", OM_OBJ_RW);
  }
  OMset_obj_ref(field, obj->in_field.obj_id(), 0);
  OMset_obj_ref(params, obj->DownsizeParam.obj_id(), 0);
}

void DLV::extension::attach_params() const
{
  static OMobj_id params = OMnull_obj;
  //static OMobj_id field = OMnull_obj;
  if (OMis_null_obj(params)) {
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_edit"
			".editUI.edit_panels.ExtensionUI.params", OM_OBJ_RW);
    //field =
    //OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_edit"
    //		".editUI.edit_panels.ExtensionUI.in_fld", OM_OBJ_RW);
  }
  //Todo OMset_obj_ref(field, obj->in_field.obj_id(), 0);
  OMset_obj_ref(params, obj->params.obj_id(), 0);
}

void DLV::data_math::attach_params() const
{
  static OMobj_id params = OMnull_obj;
  //static OMobj_id field = OMnull_obj;
  if (OMis_null_obj(params)) {
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_edit"
			".editUI.edit_panels.DataMathUI.param", OM_OBJ_RW);
    //field =
    //OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_edit"
    //		".editUI.edit_panels.DataMathUI.in_fld", OM_OBJ_RW);
  }
  OMset_obj_ref(params, obj->params.obj_id(), 0);
}

void DLV::volume_render::attach_params() const
{
  static OMobj_id params = OMnull_obj;
  static OMobj_id field = OMnull_obj;
  if (OMis_null_obj(params)) {
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_edit"
			".editUI.edit_panels.VolRUI.param", OM_OBJ_RW);
    field =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_edit"
			".editUI.edit_panels.VolRUI.in_fld", OM_OBJ_RW);
  }
  //Todo OMset_obj_ref(field, obj->in_field.obj_id(), 0);
  //Todo OMset_obj_ref(params, obj->OrthoSliceParam.obj_id(), 0);
}

void DLV::phonon_spectrum::attach_params() const
{
  static OMobj_id params = OMnull_obj;
  if (OMis_null_obj(params)) {
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_edit"
			".editUI.edit_panels.SpectraUI.params", OM_OBJ_RW);
  }
  OMset_obj_ref(params, obj->SpectrumData.obj_id(), 0);
}

void DLV::phonon_traj::attach_params() const
{
  static OMobj_id params = OMnull_obj;
  if (OMis_null_obj(params)) {
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_edit"
			".editUI.edit_panels.PhononUI.params", OM_OBJ_RW);
  }
  OMset_obj_ref(params, obj->PhononData.obj_id(), 0);
}

void DLV::dos_shift::attach_params() const
{
  static OMobj_id params = OMnull_obj;
  if (OMis_null_obj(params)) {
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_edit"
			".editUI.edit_panels.EnergyUI.params", OM_OBJ_RW);
  }
  OMset_obj_ref(params, obj->params.obj_id(), 0);
}

void DLV::band_shift::attach_params() const
{
  static OMobj_id params = OMnull_obj;
  if (OMis_null_obj(params)) {
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_edit"
			".editUI.edit_panels.EnergyUI.params", OM_OBJ_RW);
  }
  OMset_obj_ref(params, obj->params.obj_id(), 0);
}

void DLV::bdos_shift::attach_params() const
{
  static OMobj_id params = OMnull_obj;
  if (OMis_null_obj(params)) {
    params =
      OMfind_str_subobj(OMinst_obj, "DLV.app_window.DataUI.menu_edit"
			".editUI.edit_panels.EnergyUI.params", OM_OBJ_RW);
  }
  OMset_obj_ref(params, obj->params.obj_id(), 0);
}

bool DLV::edited_obj::update_spectrum(float grid[], float data[],
				      const int n, const string name,
				      char message[], const int mlen)
{
  strncpy(message, "BUG: not a spectrum edit", mlen);
  return false;
}

bool DLV::phonon_spectrum::update_spectrum(float grid[], float data[],
					   const int n, const string name,
					   char message[], const int mlen)
{
  obj->field.field.nnodes = n;
  obj->field.field.nnode_data = 1;
  obj->field.field.nspace = 1;
  obj->field.field.coordinates.veclen = 1;
  xp_long s = n;
  int ok = obj->field.field.coordinates.values.set_array(OM_TYPE_FLOAT, grid,
							 s, OM_SET_ARRAY_COPY);
  if (ok != OM_STAT_SUCCESS) {
    strncpy(message, "Unable to set spectrum coordinates array", mlen);
    return false;
  }
  obj->field.field.node_data[0].veclen = 1;
  obj->field.field.node_data[0].labels = name.c_str();
  obj->field.field.node_data[0].id = 0;
  ok = obj->field.field.node_data[0].values.set_array(OM_TYPE_FLOAT, data, s,
						      OM_SET_ARRAY_COPY);
  if (ok != OM_STAT_SUCCESS) {
    strncpy(message, "Unable to set spectrum data array", mlen);
    return false;
  }
  return true;
}

bool DLV::edited_obj::update_slice(const class drawable_obj *draw,
				   char message[], const int mlen)
{
  strncpy(message, "BUG: not a slice edit", mlen);
  return false;
}

bool DLV::slice::update_slice(const class drawable_obj *draw,
			      char message[], const int mlen)
{
  OMset_obj_ref(obj->in_plane.obj_id(), draw->get_id().get_id(), 0);
  return true;
}

bool DLV::edited_obj::update_cut(const class drawable_obj *draw,
				 char message[], const int mlen)
{
  strncpy(message, "BUG: not a cut edit", mlen);
  return false;
}

bool DLV::cut::update_cut(const class drawable_obj *draw,
			  char message[], const int mlen)
{
  OMset_obj_ref(obj->in_plane.obj_id(), draw->get_id().get_id(), 0);
  return true;
}

bool DLV::edited_obj::update_extension(const int na, const int nb,
				       const int nc, const float o[3],
				       const float a[3], const float b[3],
				       const float c[3], const int onx,
				       const int ony, const int onz,
				       float *origdata[],
				       const int vdims[], const int ndata,
				       char message[], const int mlen)
{
  strncpy(message, "BUG: not an extension edit", mlen);
  return false;
}

bool DLV::extension::update_extension(const int na, const int nb,
				      const int nc, const float o[3],
				      const float a[3], const float b[3],
				      const float c[3], const int onx,
				      const int ony, const int onz,
				      float *origdata[],
				      const int vdims[], const int ndata,
				      char message[], const int mlen)
{
  if ((na != incx) || (nb != incy) || (nc != incz)) {
    int diffa = na - incx;
    int diffb = nb - incy;
    int diffc = nc - incz;
    nx += 2 * diffa;
    ny += 2 * diffb;
    nz += 2 * diffc;
    // from in_field?
    float origin[3];
    origin[0] = o[0] - na * a[0] - nb * b[0] - nc * c[0];
    origin[1] = o[1] - na * a[1] - nb * b[1] - nc * c[1];
    origin[2] = o[2] - na * a[2] - nb * b[2] - nc * c[2];
    // Now adjust the data and the field that uses it - from drawable
    incx = na;
    incy = nb;
    incz = nc;
    int sx = 0;
    int sy = 0;
    int sz = 0;
    int ex = onx;
    int ey = ony;
    int ez = onz;
    if (na < 0) {
      sx = - na;
      ex = onx + na;
    }
    if (nb < 0) {
      sy = - nb;
      ey = ony + nb;
    }
    if (nc < 0) {
      sz = - nc;
      ez = onz + nc;
    }
    float *data[100];
    for (int l = 0; l < ndata; l++) {
      data[l] = new float[nx * ny * nz];
      if (data[l] == 0) {
	strncpy(message, "Unable to allocate memory for extension data", mlen);
	return false;
      } else {
	// Copy the base data
	for (int k = sz; k < ez; k++) {
	  int ki = k * ony;
	  int kj = (k + incz) * ny;
	  for (int j = sy; j < ey; j++) {
	    int ji = (ki + j) * onx;
	    int jj = (kj + j + incy) * nx;
	    for (int i = sx; i < ex; i++) {
	      data[l][jj + incx + i] = origdata[l][ji + i];
	    }
	  }
	}
	// Expand the rest of the data
	for (int k = sz; k < ez; k++) {
	  int ki = k * ony;
	  int kj = (k + incz) * ny;
	  // x faces
	  for (int j = sy; j < ey; j++) {
	    int ji = (ki + j) * onx;
	    int jj = (kj + j + incy) * nx;
	    for (int i = 0; i < incx; i++) {
	      data[l][jj + i] = origdata[l][ji + (onx - incx - 1 + i)];
	      data[l][jj + onx + incx + i] = origdata[l][ji + i + 1];
	    }
	  }
	  // y faces
	  for (int j = 0; j < incy; j++) {
	    int ji = (ki + (ony - incy - 1 + j)) * onx;
	    int jj = (kj + j) * nx;
	    for (int i = sx; i < ex; i++) {
	      data[l][jj + incx + i] = origdata[l][ji + i];
	    }
	    // x,y corners
	    for (int i = 0; i < incx; i++) {
	      data[l][jj + i] = origdata[l][ji + (onx - incx - 1 + i)];
	      data[l][jj + onx + incx + i] = origdata[l][ji + i + 1];
	    }
	    ji = (ki + j + 1) * onx;
	    jj = (kj + ony + incy + j) * nx;
	    for (int i = sx; i < ex; i++) {
	      data[l][jj + incx + i] = origdata[l][ji + i];
	    }
	    // x,y corners
	    for (int i = 0; i < incx; i++) {
	      data[l][jj + i] = origdata[l][ji + (onx - incx - 1 + i)];
	      data[l][jj + onx + incx + i] = origdata[l][ji + i + 1];
	    }
	  }
	}
	// z faces
	for (int k = 0; k < incz; k++) {
	  // z lower
	  int ki = (onz - incz - 1 + k) * ony;
	  int kj = k * ny;
	  for (int j = sy; j < ey; j++) {
	    int ji = (ki + j) * onx;
	    int jj = (kj + j + incy) * nx;
	    for (int i = sx; i < ex; i++) {
	      data[l][jj + incx + i] = origdata[l][ji + i];
	    }
	    // x,z corners
	    for (int i = 0; i < incx; i++) {
	      data[l][jj + i] = origdata[l][ji + (onx - incx - 1 + i)];
	      data[l][jj + onx + incx + i] = origdata[l][ji + i + 1];
	    }
	  }
	  // y,z faces
	  for (int j = 0; j < incy; j++) {
	    int ji = (ki + (ony - incy - 1 + j)) * onx;
	    int jj = (kj + j) * nx;
	    for (int i = sx; i < ex; i++) {
	      data[l][jj + incx + i] = origdata[l][ji + i];
	    }
	    // x,y,z corners
	    for (int i = 0; i < incx; i++) {
	      data[l][jj + i] = origdata[l][ji + (onx - incx - 1 + i)];
	      data[l][jj + onx + incx + i] = origdata[l][ji + i + 1];
	    }
	    ji = (ki + j + 1) * onx;
	    jj = (kj + ony + incy + j) * nx;
	    for (int i = sx; i < ex; i++) {
	      data[l][jj + incx + i] = origdata[l][ji + i];
	    }
	    // x,y,z corners
	    for (int i = 0; i < incx; i++) {
	      data[l][jj + i] = origdata[l][ji + (onx - incx - 1 + i)];
	      data[l][jj + onx + incx + i] = origdata[l][ji + i + 1];
	    }
	  }
	  // z upper
	  ki = (k + 1) * ony;
	  kj = (onz + incz + k) * ny;
	  for (int j = sy; j < ey; j++) {
	    int ji = (ki + j) * onx;
	    int jj = (kj + j + incy) * nx;
	    for (int i = sx; i < ex; i++) {
	      data[l][jj + incx + i] = origdata[l][ji + i];
	    }
	    // x,z corners
	    for (int i = 0; i < incx; i++) {
	      data[l][jj + i] = origdata[l][ji + (onx - incx - 1 + i)];
	      data[l][jj + onx + incx + i] = origdata[l][ji + i + 1];
	    }
	  }
	  // y,z faces
	  for (int j = 0; j < incy; j++) {
	    int ji = (ki + (ony - incy - 1 + j)) * onx;
	    int jj = (kj + j) * nx;
	    for (int i = sx; i < ex; i++) {
	      data[l][jj + incx + i] = origdata[l][ji + i];
	    }
	    // x,y,z corners
	    for (int i = 0; i < incx; i++) {
	      data[l][jj + i] = origdata[l][ji + (onx - incx - 1 + i)];
	      data[l][jj + onx + incx + i] = origdata[l][ji + i + 1];
	    }
	    ji = (ki + j + 1) * onx;
	    jj = (kj + ony + incy + j) * nx;
	    for (int i = sx; i < ex; i++) {
	      data[l][jj + incx + i] = origdata[l][ji + i];
	    }
	    // x,y,z corners
	    for (int i = 0; i < incx; i++) {
	      data[l][jj + i] = origdata[l][ji + (onx - incx - 1 + i)];
	      data[l][jj + onx + incx + i] = origdata[l][ji + i + 1];
	    }
	  }
	}
      }
    }
    // Update the field
    field->volume(origin, a, b, c, data, nx, ny, nz, vdims, 0,
		  ndata, false, 0.0, message, mlen);
    for (int i = 0; i < ndata; i++)
      if (data[i] != 0)
	delete [] data[i];
  }
  return true;
}

bool DLV::edited_obj::update_math(const class drawable_obj *draw,
				  const int cmpt, const int index,
				  char message[], const int mlen)
{
  strncpy(message, "BUG: not a data math edit", mlen);
  return false;
}

bool DLV::data_math::update_math(const class drawable_obj *draw,
				 const int cmpt, const int index,
				 char message[], const int mlen)
{
  switch (index) {
  case 0:
    OMset_obj_val(obj->in_field2.obj_id(), draw->get_id().get_id(), 0);
    obj->params.cmp2 = cmpt;
    break;
  case 1:
    OMset_obj_val(obj->in_field3.obj_id(), draw->get_id().get_id(), 0);
    obj->params.cmp3 = cmpt;
    break;
  case 2:
    OMset_obj_val(obj->in_field4.obj_id(), draw->get_id().get_id(), 0);
    obj->params.cmp4 = cmpt;
    break;
  }
  return true;
}

bool DLV::edited_obj::update_volr(const float o[3], const float a[3],
				  const float b[3], const float c[3],
				  const int nx, const int ny, const int nz,
				  float *origdata[],
				  const int vdims[], const int ndata,
				  char message[], const int mlen)
{
  strncpy(message, "BUG: not a volume render edit", mlen);
  return false;
}

bool DLV::volume_render::update_volr(const float o[3],
				     const float a[3], const float b[3],
				     const float c[3], const int nx,
				     const int ny, const int nz,
				     float *origdata[],
				     const int vdims[], const int ndata,
				     char message[], const int mlen)
{
  obj->field.ndim = 3;
  xp_long *dims = (xp_long *) obj->field.dims.ret_array_ptr(OM_GET_ARRAY_WR);
  if (dims == 0) {
    strncpy(message, "Unable to allocate volume array dimensions", mlen);
    return false;
  }
  dims[0] = nx;
  dims[1] = ny;
  dims[2] = nz;
  obj->field.dims.free_array(dims);
  xp_long s = nx * ny * nz;
  obj->field.nnodes = s;
  //obj->field.nspace = 3;
  // set the points.
  float points[2][3];
  points[0][0] = o[0];
  points[0][1] = o[1];
  points[0][2] = o[2];
  points[1][0] = o[0] + ((float) (nx - 1)) * a[0];
  points[1][1] = o[1] + ((float) (ny - 1)) * b[1];
  points[1][2] = o[2] + ((float) (nz - 1)) * c[2];
  obj->field.points.set_array(OM_TYPE_FLOAT, points, 6, OM_SET_ARRAY_COPY);
  int nscalar = 0;
  for (int i = 0; i < ndata; i++) {
    if (vdims[i] == 1)
      nscalar++;
  }
  obj->field.nnode_data = nscalar;
  nscalar = 0;
  for (int i = 0; i < ndata; i++) {
    if (vdims[i] == 1) {
      obj->field.node_data[nscalar].veclen = 1;
      float mind = 1e10f, maxd = -1e10f;
      for (xp_long j = 0; j < s; j++) {
	if (origdata[i][j] < mind)
	  mind = origdata[i][j];
	if (origdata[i][j] > maxd)
	  maxd = origdata[i][j];
      }
      s = nx * ny * nz;
      unsigned char *vals = (unsigned char *)
	obj->field.node_data[nscalar].values.ret_typed_array_ptr(
							   OM_GET_ARRAY_WR,
							   OM_TYPE_BYTE,
							   &s);
      if (vals == 0) {
	strncpy(message, "Unable to allocate data array", mlen);
	return false;
      }
      float range = maxd - mind;
      for (xp_long j = 0; j < s; j++) {
	vals[j] = (unsigned char) (255 * (origdata[i][j] - mind) / range);
      }
      obj->field.node_data[nscalar].values.free_array(vals);
      nscalar++;
    }
  }
  return true;
}

bool DLV::edited_obj::update_shift(const float shift, char message[],
				   const int mlen)
{
  strncpy(message, "BUG: not an energy shift", mlen);
  return false;
}

bool DLV::dos_shift::update_shift(const float shift, char message[],
				  const int mlen)
{
  // shift data
  int n = obj->in_data.field.npoints;
  float *data =
    (float *)obj->in_data.field.points.ret_array_ptr(OM_GET_ARRAY_RD);
  float *update =
    (float *)obj->dos_data.field.points.ret_array_ptr(OM_GET_ARRAY_WR);
  for (int i = 0; i < n; i++)
    update[i] = data[i] + shift;
  obj->dos_data.field.points.free_array(update);
  obj->in_data.field.points.free_array(data);
  // seem to also need to mess with coords
  data =
    (float *)obj->in_data.field.coordinates.values.ret_array_ptr(OM_GET_ARRAY_RD);
  update =
    (float *)obj->dos_data.field.coordinates.values.ret_array_ptr(OM_GET_ARRAY_WR);
  for (int i = 0; i < n; i++)
    update[i] = data[i] + shift;
  obj->dos_data.field.coordinates.values.free_array(update);
  obj->in_data.field.coordinates.values.free_array(data);
  // shift lines
  int nx = obj->in_data.x.nnode_data;
  for (int i = 0; i < nx; i++) {
    data =
    (float *)obj->in_data.x.node_data[i].values.ret_array_ptr(OM_GET_ARRAY_RD);
    update =
  (float *)obj->dos_data.x.node_data[i].values.ret_array_ptr(OM_GET_ARRAY_WR);
    update[0] = data[0] + shift;
    update[1] = data[1] + shift;
    obj->dos_data.x.node_data[i].values.free_array(update);
    obj->in_data.x.node_data[i].values.free_array(data);
  }
  return true;
}

bool DLV::band_shift::update_shift(const float shift, char message[],
				   const int mlen)
{
  // shift data
  int nodes = obj->in_data.field.nnode_data;
  for (int j = 0; j < nodes; j++) {
    int n = obj->in_data.field.node_data[j].nvals;
    float *data =
      (float *)obj->in_data.field.node_data[j].values.ret_array_ptr(OM_GET_ARRAY_RD);
    float *update =
    (float *)obj->band_data.field.node_data[j].values.ret_array_ptr(OM_GET_ARRAY_WR);
    float emin = 1e10;
    float emax = -1e10;
    for (int i = 0; i < n; i++) {
      update[i] = data[i] + shift;
      if (update[i] > emax)
	emax = update[i];
      if (update[i] < emin)
	emin = update[i];
    }
    obj->band_data.field.node_data[j].values.free_array(update);
    obj->in_data.field.node_data[j].values.free_array(data);
    obj->band_data.field.node_data[j].min = emin;
    obj->band_data.field.node_data[j].max = emax;
  }
  // shift lines
  int ny = obj->in_data.y.nnode_data;
  for (int i = 0; i < ny; i++) {
    float *data =
    (float *)obj->in_data.y.node_data[i].values.ret_array_ptr(OM_GET_ARRAY_RD);
    float *update =
  (float *)obj->band_data.y.node_data[i].values.ret_array_ptr(OM_GET_ARRAY_WR);
    update[0] = data[0] + shift;
    update[1] = data[1] + shift;
    obj->band_data.y.node_data[i].min = update[0];
    obj->band_data.y.node_data[i].max = update[1];
    obj->band_data.y.node_data[i].values.free_array(update);
    obj->in_data.y.node_data[i].values.free_array(data);
  }
  // shift scale
  float *data =
    (float *)obj->in_data.x.points.ret_array_ptr(OM_GET_ARRAY_RD);
  float *update =
    (float *)obj->band_data.x.points.ret_array_ptr(OM_GET_ARRAY_WR);
  update[0] = data[0] + shift;
  update[1] = data[1] + shift;
  obj->band_data.x.points.free_array(update);
  obj->in_data.x.points.free_array(data);
  data =
    (float *)obj->in_data.x.coordinates.values.ret_array_ptr(OM_GET_ARRAY_RD);
  update =
   (float *)obj->band_data.x.coordinates.values.ret_array_ptr(OM_GET_ARRAY_WR);
  update[0] = data[0] + shift;
  update[1] = data[1] + shift;
  obj->band_data.x.coordinates.min = update[0];
  obj->band_data.x.coordinates.max = update[1];
  obj->band_data.x.coordinates.values.free_array(update);
  obj->in_data.x.coordinates.values.free_array(data);
  return true;
}

bool DLV::bdos_shift::update_shift(const float shift, char message[],
				   const int mlen)
{
  //fprintf(stderr, "Shift is %f\n", shift);
  // shift data
  int nodes = obj->in_data.field.nnode_data;
  for (int j = 0; j < nodes; j++) {
    int n = obj->in_data.field.node_data[j].nvals;
    float *data =
      (float *)obj->in_data.field.node_data[j].values.ret_array_ptr(OM_GET_ARRAY_RD);
    float *update =
    (float *)obj->band_data.field.node_data[j].values.ret_array_ptr(OM_GET_ARRAY_WR);
    float emin = 1e10;
    float emax = -1e10;
    for (int i = 0; i < n; i++) {
      update[i] = data[i] + shift;
      if (update[i] > emax)
	emax = update[i];
      if (update[i] < emin)
	emin = update[i];
    }
    obj->band_data.field.node_data[j].values.free_array(update);
    obj->in_data.field.node_data[j].values.free_array(data);
    obj->band_data.field.node_data[j].min = emin;
    obj->band_data.field.node_data[j].max = emax;
  }
  // shift lines
  int ny = obj->in_data.y.nnode_data;
  for (int i = 0; i < ny; i++) {
    float *data =
    (float *)obj->in_data.y.node_data[i].values.ret_array_ptr(OM_GET_ARRAY_RD);
    float *update =
  (float *)obj->band_data.y.node_data[i].values.ret_array_ptr(OM_GET_ARRAY_WR);
    update[0] = data[0] + shift;
    update[1] = data[1] + shift;
    obj->band_data.y.node_data[i].values.free_array(update);
    obj->in_data.y.node_data[i].values.free_array(data);
    obj->band_data.y.node_data[i].min = update[0];
    obj->band_data.y.node_data[i].max = update[1];
  }
  // shift scale
  int nx = obj->in_data.x.npoints;
  float *data =
    (float *)obj->in_data.x.points.ret_array_ptr(OM_GET_ARRAY_RD);
  float *update =
    (float *)obj->band_data.x.points.ret_array_ptr(OM_GET_ARRAY_WR);
  for (int i = 0; i < nx; i++)
    update[i] = data[i] + shift;
  obj->band_data.x.points.free_array(update);
  obj->in_data.x.points.free_array(data);
  data =
    (float *)obj->in_data.x.coordinates.values.ret_array_ptr(OM_GET_ARRAY_RD);
  update =
    (float *)obj->band_data.x.coordinates.values.ret_array_ptr(OM_GET_ARRAY_WR);
  float emin = 1e10;
  float emax = -1e10;
  for (int i = 0; i < nx; i++) {
    update[i] = data[i] + shift;
    if (update[i] > emax)
      emax = update[i];
    if (update[i] < emin)
      emin = update[i];
  }
  obj->band_data.x.coordinates.min = emin;
  obj->band_data.x.coordinates.max = emax;
  obj->band_data.x.coordinates.values.free_array(update);
  obj->in_data.x.coordinates.values.free_array(data);
  return true;
}

DLV::drawable_obj *DLV::edited_obj::replace_edit(class toolkit_obj &id)
{
  delete draw;
  draw = drawable_obj::create_edit(id);
  return draw;
}

void DLV::orthoslice::reload_data(drawable_obj *draw)
{
  toolkit_obj t(obj->out_fld.obj_id());
  drawable_obj *out = replace_edit(t);
  set_drawable(out);
  OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::slice::reload_data(drawable_obj *draw)
{
  toolkit_obj t(obj->out_fld.obj_id());
  drawable_obj *out = replace_edit(t);
  set_drawable(out);
  OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::clamp::reload_data(drawable_obj *draw)
{
  // Todo
  OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::crop::reload_data(drawable_obj *draw)
{
  // Todo
  OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::cut::reload_data(drawable_obj *draw)
{
  // Todo
  OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::downsize::reload_data(drawable_obj *draw)
{
  // Todo
  OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::extension::reload_data(drawable_obj *draw)
{
  // Todo
  OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::data_math::reload_data(drawable_obj *draw)
{
  // Todo
  //OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::volume_render::reload_data(drawable_obj *draw)
{
  // Todo
  //OMset_obj_val(obj->in_field.obj_id(), draw->get_id().get_id(), 0);
}

void DLV::phonon_spectrum::reload_data(drawable_obj *draw)
{
  toolkit_obj t(obj->field.obj_id());
  drawable_obj *out = replace_edit(t);
  set_drawable(out);
}

void DLV::phonon_traj::reload_data(drawable_obj *draw)
{
}

void DLV::dos_shift::reload_data(drawable_obj *draw)
{
  // Todo
}

void DLV::band_shift::reload_data(drawable_obj *draw)
{
  // Todo
}

void DLV::bdos_shift::reload_data(drawable_obj *draw)
{
  // Todo
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::orthoslice *t,
				    const unsigned int file_version)
    {
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    DLV::orthoslice *t,
				    const unsigned int file_version)
    {
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::orthoslice(DLV::find_parent(p, sp), sp);
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::slice *t,
				    const unsigned int file_version)
    {
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::slice *t,
				    const unsigned int file_version)
    {
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::slice(DLV::find_parent(p, sp), sp);
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::clamp *t,
				    const unsigned int file_version)
    {
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::clamp *t,
				    const unsigned int file_version)
    {
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::clamp(DLV::find_parent(p, sp), sp);
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::crop *t,
				    const unsigned int file_version)
    {
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::crop *t,
				    const unsigned int file_version)
    {
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::crop(DLV::find_parent(p, sp), sp);
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::cut *t,
				    const unsigned int file_version)
    {
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::cut *t,
				    const unsigned int file_version)
    {
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::cut(DLV::find_parent(p, sp), sp);
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::downsize *t,
				    const unsigned int file_version)
    {
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::downsize *t,
				    const unsigned int file_version)
    {
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::downsize(DLV::find_parent(p, sp), sp);
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::extension *t,
				    const unsigned int file_version)
    {
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::extension *t,
				    const unsigned int file_version)
    {
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::extension(DLV::find_parent(p, sp), 0, 0, 0, sp);
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::data_math *t,
				    const unsigned int file_version)
    {
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::data_math *t,
				    const unsigned int file_version)
    {
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::data_math(DLV::find_parent(p, sp), sp);
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::volume_render *t,
				    const unsigned int file_version)
    {
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::volume_render *t,
				    const unsigned int file_version)
    {
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::volume_render(DLV::find_parent(p, sp), sp);
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::phonon_spectrum *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    DLV::phonon_spectrum *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::phonon_spectrum(DLV::find_parent(p, false), i);
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar,
				    const DLV::phonon_traj *t,
				    const unsigned int file_version)
    {
      int i = t->get_index();
      ar << i;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    DLV::phonon_traj *t,
				    const unsigned int file_version)
    {
      int i;
      ar >> i;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::phonon_traj(DLV::find_parent(p, false), i);
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::dos_shift *t,
				    const unsigned int file_version)
    {
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::dos_shift *t,
				    const unsigned int file_version)
    {
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::dos_shift(DLV::find_parent(p, sp), sp);
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::band_shift *t,
				    const unsigned int file_version)
    {
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::band_shift *t,
				    const unsigned int file_version)
    {
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::band_shift(DLV::find_parent(p, sp), sp);
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::bdos_shift *t,
				    const unsigned int file_version)
    {
      bool sp = t->is_k_space();
      ar << sp;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::bdos_shift *t,
				    const unsigned int file_version)
    {
      bool sp;
      ar >> sp;
      DLV::render_parent *p = DLV::render_parent::get_serialize_obj();
      ::new(t)DLV::bdos_shift(DLV::find_parent(p, sp), sp);
    }

  }
}

template <class Archive>
void DLV::orthoslice::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  int i = (int)obj->OrthoSliceParam.plane;
  ar & i;
  i = (int)obj->OrthoSliceParam.axis;
  ar & i;
}

template <class Archive>
void DLV::orthoslice::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  int i;
  ar & i;
  obj->OrthoSliceParam.plane = i;
  ar & i;
  obj->OrthoSliceParam.axis = i;
}

template <class Archive>
void DLV::slice::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  int i = (int)obj->SliceParam.plane;
  ar & i;
  float f = (float)obj->SliceParam.dist;
  ar & f;
}

template <class Archive>
void DLV::slice::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  int i;
  ar & i;
  obj->SliceParam.plane = i;
  float f;
  ar & f;
  obj->SliceParam.dist = f;
}

template <class Archive>
void DLV::clamp::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  int i = (int)obj->ClampParam.vector;
  ar & i;
  i = (int)obj->ClampParam.component;
  ar & i;
  i = (int)obj->ClampParam.above;
  ar & i;
  i = (int)obj->ClampParam.below;
  ar & i;
  i = (int)obj->ClampParam.reset_minmax;
  ar & i;
}

template <class Archive>
void DLV::clamp::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  int i;
  ar & i;
  obj->ClampParam.vector = i;
  ar & i;
  obj->ClampParam.component = i;
  ar & i;
  obj->ClampParam.above = i;
  ar & i;
  obj->ClampParam.below = i;
  ar & i;
  obj->ClampParam.reset_minmax = i;
}

template <class Archive>
void DLV::crop::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  // Todo?
}

template <class Archive>
void DLV::crop::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  // Todo?
}

template <class Archive>
void DLV::cut::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  int i = (int)obj->CutParam.above;
  ar & i;
  float f = (float)obj->CutParam.dist;
  ar & f;
}

template <class Archive>
void DLV::cut::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  int i;
  ar & i;
  obj->CutParam.above = i;
  float f;
  ar & f;
  obj->CutParam.dist = f;
}

template <class Archive>
void DLV::downsize::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  float f = (float)obj->DownsizeParam.factor0;
  ar & f;
  f = (float)obj->DownsizeParam.factor1;
  ar & f;
  f = (float)obj->DownsizeParam.factor2;
  ar & f;
}

template <class Archive>
void DLV::downsize::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  float f;
  ar & f;
  obj->DownsizeParam.factor0 = f;
  ar & f;
  obj->DownsizeParam.factor1 = f;
  ar & f;
  obj->DownsizeParam.factor2 = f;
}

template <class Archive>
void DLV::extension::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  int i = (int)obj->params.a;
  ar & i;
  i = (int)obj->params.b;
  ar & i;
  i = (int)obj->params.c;
  ar & i;
}

template <class Archive>
void DLV::extension::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  int i;
  ar & i;
  obj->params.a = i;
  ar & i;
  obj->params.b = i;
  ar & i;
  obj->params.c = i;
}

template <class Archive>
void DLV::data_math::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  string c = (char *)obj->params.operation;
  ar & c;
  int i = (int)obj->params.selection2;
  ar & i;
  i = (int)obj->params.selection3;
  ar & i;
  i = (int)obj->params.selection4;
  ar & i;
  i = (int)obj->params.cmp1;
  ar & i;
  i = (int)obj->params.cmp2;
  ar & i;
  i = (int)obj->params.cmp3;
  ar & i;
  i = (int)obj->params.cmp4;
  ar & i;
  i = (int)obj->params.trust_user;
  ar & i;
}

template <class Archive>
void DLV::data_math::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  string c;
  ar & c;
  obj->params.operation = c.c_str();
  int i;
  ar & i;
  obj->params.selection2 = i;
  ar & i;
  obj->params.selection3 = i;
  ar & i;
  obj->params.selection4 = i;
  ar & i;
  obj->params.cmp1 = i;
  ar & i;
  obj->params.cmp2 = i;
  ar & i;
  obj->params.cmp3 = i;
  ar & i;
  obj->params.cmp4 = i;
  ar & i;
  obj->params.trust_user = i;
}

template <class Archive>
void DLV::volume_render::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  // Todo
  //ar & (int)data->IsoParam.iso_component;
}

template <class Archive>
void DLV::volume_render::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  //ar & data->IsoParam.iso_component;
}

template <class Archive>
void DLV::phonon_spectrum::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  int i = (int)obj->SpectrumData.index;
  ar & i;
  i = (int)obj->SpectrumData.ir_or_raman;
  ar & i;
  float f = (float)obj->SpectrumData.emin;
  ar & f;
  f = (float)obj->SpectrumData.emax;
  ar & f;
  i = (int)obj->SpectrumData.npoints;
  ar & i;
  f = (float)obj->SpectrumData.broaden;
  ar & f;
  f = (float)obj->SpectrumData.harmonic_scale;
  ar & f;
}

template <class Archive>
void DLV::phonon_spectrum::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  int i;
  ar & i;
  obj->SpectrumData.index = i;
  ar & i;
  obj->SpectrumData.ir_or_raman = i;
  float f;
  ar & f;
  obj->SpectrumData.emin = f;
  ar & f;
  obj->SpectrumData.emax = f;
  ar & i;
  obj->SpectrumData.npoints = i;
  ar & f;
  obj->SpectrumData.broaden = f;
  ar & f;
  obj->SpectrumData.harmonic_scale = f;
  toolkit_obj t(obj->field.obj_id());
  drawable_obj *out = drawable_obj::create_edit(t);
  set_drawable(out);
}

template <class Archive>
void DLV::phonon_traj::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  int i = (int)obj->PhononData.index;
  ar & i;
  i = (int)obj->PhononData.nframes;
  ar & i;
  float f = (float)obj->PhononData.magnitude;
  ar & f;
  i = (int)obj->PhononData.use_temp;
  ar & i;
  f = (int)obj->PhononData.temperature;
  ar & f;
}

template <class Archive>
void DLV::phonon_traj::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  int i;
  ar & i;
  obj->PhononData.index = i;
  ar & i;
  obj->PhononData.nframes = i;
  float f;
  ar & f;
  obj->PhononData.magnitude = f;
  ar & i;
  obj->PhononData.use_temp = i;
  ar & f;
  obj->PhononData.temperature = f;
}

template <class Archive>
void DLV::dos_shift::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  int i = (int)obj->params.use_property;
  ar & i;
  i = (int)obj->params.object;
  ar & i;
  float f = (float)obj->params.shift;
  ar & f;
}

template <class Archive>
void DLV::dos_shift::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  int i;
  ar & i;
  obj->params.use_property = i;
  ar & i;
  obj->params.object = i;
  float f;
  ar & f;
  obj->params.shift = f;
}

template <class Archive>
void DLV::band_shift::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  int i = (int)obj->params.use_property;
  ar & i;
  i = (int)obj->params.object;
  ar & i;
  float f = (float)obj->params.shift;
  ar & f;
}

template <class Archive>
void DLV::band_shift::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  int i;
  ar & i;
  obj->params.use_property = i;
  ar & i;
  obj->params.object = i;
  float f;
  ar & f;
  obj->params.shift = f;
}

template <class Archive>
void DLV::bdos_shift::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  int i = (int)obj->params.use_property;
  ar & i;
  i = (int)obj->params.object;
  ar & i;
  float f = (float)obj->params.shift;
  ar & f;
}

template <class Archive>
void DLV::bdos_shift::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edited_obj>(*this);
  int i;
  ar & i;
  obj->params.use_property = i;
  ar & i;
  obj->params.object = i;
  float f;
  ar & f;
  obj->params.shift = f;
}

BOOST_CLASS_EXPORT_GUID(DLV::orthoslice, "DLV::orthoslice")
BOOST_CLASS_EXPORT_GUID(DLV::slice, "DLV::slice")
BOOST_CLASS_EXPORT_GUID(DLV::clamp, "DLV::clamp")
BOOST_CLASS_EXPORT_GUID(DLV::crop, "DLV::crop")
BOOST_CLASS_EXPORT_GUID(DLV::cut, "DLV::cut")
BOOST_CLASS_EXPORT_GUID(DLV::downsize, "DLV::downsize")
BOOST_CLASS_EXPORT_GUID(DLV::extension, "DLV::extension")
BOOST_CLASS_EXPORT_GUID(DLV::data_math, "DLV::data_math")
BOOST_CLASS_EXPORT_GUID(DLV::volume_render, "DLV::volume_render")
BOOST_CLASS_EXPORT_GUID(DLV::phonon_spectrum, "DLV::phonon_spectrum")
BOOST_CLASS_EXPORT_GUID(DLV::phonon_traj, "DLV::phonon_traj")
BOOST_CLASS_EXPORT_GUID(DLV::dos_shift, "DLV::dos_shift")
BOOST_CLASS_EXPORT_GUID(DLV::band_shift, "DLV::band_shift")
BOOST_CLASS_EXPORT_GUID(DLV::bdos_shift, "DLV::bdos_shift")

#endif // DLV_USES_SERIALIZE
