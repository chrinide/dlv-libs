
#ifndef DLV_DRAWABLE_PRIVATE
#define DLV_DRAWABLE_PRIVATE

// forward declare
class CCP3_Renderers_Plots_multi_grid_data;
class CCP3_Renderers_Plots_dos_data;
class CCP3_Renderers_Plots_band_data;
class CCP3_Renderers_Points_atom_bond_text_data;
class CCP3_Renderers_Points_text_data;
class CCP3_Renderers_Points_real_data;
class CCP3_Renderers_Points_file_view_data;
class CCP3_Renderers_Points_buffer_view_data;
class CCP3_Renderers_Points_label_field;
class CCP3_Renderers_Volumes_box_data;
class CCP3_Renderers_Volumes_sphere_data;
class CCP3_Renderers_Volumes_wulff_plot_data;
class CCP3_Renderers_Plots_leed_data;
class CCP3_Renderers_Planes_plane_data;
class CCP3_Renderers_Lines_line_data;
class CCP3_Renderers_Points_point_data;
class CCP3_Renderers_Points_vector_field;

namespace DLV {

  class text_obj : public drawable_obj {
  public:
    text_obj(const OMobj_id id);
    ~text_obj();

    toolkit_obj get_id() const;
    bool set_value(const string value, char message[], const int mlen);
    void update_text_value(const string value);

  private:
    CCP3_Renderers_Points_text_data *data;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<drawable_obj>(*this);
      // Todo
    }
#endif // DLV_USES_SERIALIZE
  };

  class real_obj : public drawable_obj {
  public:
    real_obj(const OMobj_id id);
    ~real_obj();

    toolkit_obj get_id() const;
    bool set_value(const double value, char message[], const int mlen);

  private:
    CCP3_Renderers_Points_real_data *data;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class text_view_obj : public drawable_obj {
  public:
    text_view_obj(const OMobj_id id);
    ~text_view_obj();

    toolkit_obj get_id() const;
    bool set_name(const string name, char message[], const int mlen);
    void update_text_value(const string value);

  private:
    CCP3_Renderers_Points_file_view_data *data;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<drawable_obj>(*this);
      // Todo
    }
#endif // DLV_USES_SERIALIZE
  };

  class buffer_view_obj : public drawable_obj {
  public:
    buffer_view_obj(const OMobj_id id);
    ~buffer_view_obj();

    toolkit_obj get_id() const;
    bool set_name(const string name, char message[], const int mlen);

    void expand_text_list(const int n);
    void add_text_line(const char line[], const int index, const bool append);

  private:
    CCP3_Renderers_Points_buffer_view_data *data;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<drawable_obj>(*this);
      // Todo
    }
#endif // DLV_USES_SERIALIZE
  };

  class atoms_bonds_text_obj : public drawable_obj {
  public:
    atoms_bonds_text_obj(const OMobj_id id, const string formula);
    ~atoms_bonds_text_obj();

    toolkit_obj get_id() const;

    void update_atoms_and_bonds(const int natoms, const double x,
				const double y, const double z,
				const double length, const double x2,
				const double y2, const double z2,
				const double angle, const char sym1[],
				const char sym2[], const char sym3[],
				const float r1, const float r2,
				const float r3, const char group[]);
    void update_formula(const string f);

  private:
    CCP3_Renderers_Points_atom_bond_text_data *data;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class atom_text_obj : public drawable_obj {
  public:
    atom_text_obj(const OMobj_id id);
    ~atom_text_obj();

    toolkit_obj get_id() const;
    bool field(float coords[][3], int **array, const string labels[],
	       const int ndata, const int natoms, char message[],
	       const int mlen);
    bool field(float coords[][3], float **array, const string labels[],
	       const int ndata, const int natoms, char message[],
	       const int mlen);
    void update_selected_atom(const int index);
    void update_text_field(float coords[][3], int **array,
			   const string labels[], const int ndata,
			   const int natoms, char message[], const int mlen);
    void update_text_field(float coords[][3], float **array,
			   const string labels[], const int ndata,
			   const int natoms, char message[], const int mlen);

  private:
    CCP3_Renderers_Points_label_field *data;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class atom_vector_obj : public drawable_obj {
  public:
    atom_vector_obj(const OMobj_id id);
    ~atom_vector_obj();

    toolkit_obj get_id() const;
    bool field(float coords[][3], float array[][3], const int natoms,
	       const int cmpts, char message[], const int mlen);
    void update_vector_field(float coords[][3], float array[][3],
			     const int natoms, const int cmpt,
			     char message[], const int mlen);

  private:
    //int kp;
    //int fp;
    CCP3_Renderers_Points_vector_field *data;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class struct_obj : public drawable_obj {
  public:
    struct_obj(const OMobj_id id, const bool sp);
    ~struct_obj();

    void reload_data(const render_parent *parent, const float origin[3],
		     const float astep[3], const float bstep[3],
		     float *data[], const int nx, const int ny,
		     const string labels[], const int ndata,
		     const bool use_ignore, const float ignore,
		     char message[], const int mlen);
    void reload_data(const render_parent *parent, const float origin[3],
		     const float astep[3], const float bstep[3],
		     const float cstep[3], float *data[], const int nx,
		     const int ny, const int nz, const int dims[],
		     const string labels[], const int ndata,
		     const bool kspace, const bool use_ignore,
		     const float ignore, char message[], const int mlen);
    void reload_wavefn(const render_parent *parent, const float origin[3],
		       const float astep[3], const float bstep[3],
		       const float cstep[3], float *data[], float *phases[],
		       const int nx, const int ny, const int nz,
		       const string labels[], const int ndata,
		       const bool use_ignore, const float ignore,
		       char message[], const int mlen);
    void reload_bloch(const render_parent *parent, const float origin[3],
		      const float astep[3], const float bstep[3],
		      const float cstep[3], float *data[], float *phases[],
		      const int nx, const int ny, const int nz,
		      const string labels[], const int ndata, const int knum[],
		      const int kden[], const bool use_ignore,
		      const float ignore, char message[], const int mlen);

    bool volume(const float origin[3], const float astep[3],
		const float bstep[3], const float cstep[3],
		float *datavals[], const int nx, const int ny,
		const int nz, const int vdims[],
		const string labels[], const int ndata,
		const bool use_ignore, const float ignore,
		char message[], const int mlen);
    bool volume(const float origin[3], const float astep[3],
		const float bstep[3], const float cstep[3], float *mag[],
		float *phase[], const int nx, const int ny, const int nz,
		const string labels[], const int ndata, const bool use_ignore,
		const float ignore, char message[], const int mlen);
    bool volume(const float origin[3], const float astep[3],
		const float bstep[3], const float cstep[3], float *mag[],
		float *phase[], const int nx, const int ny, const int nz,
		const string labels[], const int ndata, const int knum[],
		const int kden[], const bool use_ignore, const float ignore,
		char message[], const int mlen);
    bool slice(const float origin[3], const float astep[3],
	       const float bstep[3], float *datavals[],
	       const int nx, const int ny,
	       const string labels[], const int ndata,
	       const bool use_ignore, const float ignore,
	       char message[], const int mlen);
    drawable_obj *duplicate(const class toolkit_obj &id) const;

    toolkit_obj get_id() const;

  private:
    FLD_Field_Struct *data;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class rect_obj : public drawable_obj {
  public:
    rect_obj(const OMobj_id id, const bool sp);
    ~rect_obj();

    void reload_data(const render_parent *parent, const float origin[3],
		     const float astep[3], const float bstep[3],
		     float *data[], const int nx, const int ny,
		     const string labels[], const int ndata,
		     const bool use_ignore, const float ignore,
		     char message[], const int mlen);
    void reload_data(const render_parent *parent, const float origin[3],
		     const float astep[3], const float bstep[3],
		     const float cstep[3], float *data[], const int nx,
		     const int ny, const int nz, const int dims[],
		     const string labels[], const int ndata,
		     const bool kspace, const bool use_ignore,
		     const float ignore, char message[], const int mlen);
    void reload_wavefn(const render_parent *parent, const float origin[3],
		       const float astep[3], const float bstep[3],
		       const float cstep[3], float *data[], float *phases[],
		       const int nx, const int ny, const int nz,
		       const string labels[], const int ndata,
		       const bool use_ignore, const float ignore,
		       char message[], const int mlen);
    void reload_bloch(const render_parent *parent, const float origin[3],
		      const float astep[3], const float bstep[3],
		      const float cstep[3], float *data[], float *phases[],
		      const int nx, const int ny, const int nz,
		      const string labels[], const int ndata, const int knum[],
		      const int kden[], const bool use_ignore,
		      const float ignore, char message[], const int mlen);

    bool volume(const float origin[3], const float astep[3],
		const float bstep[3], const float cstep[3],
		float *datavals[], const int nx, const int ny,
		const int nz, const int vdims[],
		const string labels[], const int ndata,
		const bool use_ignore, const float ignore,
		char message[], const int mlen);
    bool volume(const float origin[3], const float astep[3],
		const float bstep[3], const float cstep[3], float *mag[],
		float *phase[], const int nx, const int ny, const int nz,
		const string labels[], const int ndata, const bool use_ignore,
		const float ignore, char message[], const int mlen);
    bool volume(const float origin[3], const float astep[3],
		const float bstep[3], const float cstep[3], float *mag[],
		float *phase[], const int nx, const int ny, const int nz,
		const string labels[], const int ndata, const int knum[],
		const int kden[], const bool use_ignore, const float ignore,
		char message[], const int mlen);
    bool slice(const float origin[3], const float astep[3],
	       const float bstep[3], float *datavals[],
	       const int nx, const int ny,
	       const string labels[], const int ndata,
	       const bool use_ignore, const float ignore,
	       char message[], const int mlen);
    drawable_obj *duplicate(const class toolkit_obj &id) const;

    toolkit_obj get_id() const;

  private:
    FLD_Field_Rect *data;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class rect_dos : public drawable_obj {
  public:
    rect_dos(const OMobj_id id);
    ~rect_dos();

    void reload_data(const render_parent *parent, float grid[],
		     const int npoints, float *data[],
		     const string labels[], const int ngraphs,
		     const bool spin, const float xpoints[],
		     const string xlabels[], const int nx,
		     const float yrange[], char message[],
		     const int mlen);
    bool plot(float grid[], const int npoints, float *plots[],
	      const string labels[], const int ngraphs, const bool spin,
	      const float xpoints[], const string xlabels[], const int nx,
	      const float yrange[], char message[], const int mlen);
    
    toolkit_obj get_id() const;

  protected:
    rect_dos(bool const sp, CCP3_Renderers_Plots_dos_data *d);
    CCP3_Renderers_Plots_dos_data *get_data();

  private:
    CCP3_Renderers_Plots_dos_data *data;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<drawable_obj>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

  class rect_multi : public rect_dos {
  public:
    rect_multi(const OMobj_id id);

    void reload_data(const render_parent *parent, float *multi_grids[],
		     const int ngrids, int *grid_sizes,
		     const int npoints, float *data[],
		     int select_grid[],
		     const string labels[], const int ngraphs,
		     const bool spin, const float xpoints[],
		     const string xlabels[], const int nx,
		     const float yrange[], char message[],
		     const int mlen);
    bool plot(float *multi_grids[], const int ngrids, int *select_grid,
	      int *grid_sizes,
	      const int npoints, float *plots[],
	      const string labels[], const int ngraphs, const bool spin,
	      const float xpoints[], const string xlabels[], const int nx,
	      const float yrange[], char message[], const int mlen);

  private:

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<rect_dos>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };


  class rect_band : public drawable_obj {
  public:
    rect_band(const OMobj_id id);
    ~rect_band();

    void reload_data(const render_parent *parent, float grid[],
		     const int npoints, float *data[],
		     const string labels[], const int ngraphs,
		     const bool spin, const float xpoints[],
		     const string xlabels[], const int nx,
		     const float ypoints[], const string ylabels[],
		     const int ny, const float xrange[], const float yrange[],
		     char message[], const int mlen);
    void reload_data(const render_parent *parent, float grid[],
		     const int npoints, float *data[], const string labels[],
		     const int ngraphs, const bool spin, const float xpoints[],
		     const string xlabels[], const int nx,
		     const float ypoints[], const string ylabels[],
		     const int ny, const float xrange[], const float yrange[],
		     float dgrid[], const int dnpoints, float *ddata[],
		     const int dngraphs, char message[], const int mlen);
    bool plot(float grid[], const int npoints, float *plots[],
	      const string labels[], const int ngraphs, const bool spin,
	      const float xpoints[], const string xlabels[], const int nx,
	      const float ypoints[], const string ylabels[], const int ny,
	      const float xrange[], const float yrange[],
	      char message[], const int mlen);
    bool plot(float grid[], const int npoints, float *plots[],
	      const string labels[], const int ngraphs, const bool spin,
	      const float xpoints[], const string xlabels[], const int nx,
	      const float ypoints[], const string ylabels[], const int ny,
	      const float xrange[], const float yrange[], float dgrid[],
	      const int dnpoints, float *ddata[], const int dngraphs,
	      char message[], const int mlen);

    toolkit_obj get_id() const;

  private:
    CCP3_Renderers_Plots_band_data *data;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<drawable_obj>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

  class drawable_edit : public drawable_obj {
  public:
    drawable_edit(const OMobj_id id);

    toolkit_obj get_id() const;

  private:
    OMobj_id obj_id;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<drawable_obj>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

  class box_obj : public drawable_obj {
  public:
    box_obj(const OMobj_id id, const bool sp);
    ~box_obj();

    toolkit_obj get_id() const;

    void update_box(const float o[3], const float a[3], const float b[3],
		    const float c[3]);

  private:
    CCP3_Renderers_Volumes_box_data *data;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<drawable_obj>(*this);
      // Todo
    }
#endif // DLV_USES_SERIALIZE
  };

  class sphere_obj : public drawable_obj {
  public:
    sphere_obj(const OMobj_id id, const bool sp);
    ~sphere_obj();

    toolkit_obj get_id() const;

    void update_sphere(const float o[3], const float radius);

  private:
    CCP3_Renderers_Volumes_sphere_data *data;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<drawable_obj>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

  class plane_obj : public drawable_obj {
  public:
    plane_obj(const OMobj_id id, const bool sp);
    ~plane_obj();

    toolkit_obj get_id() const;

    void update_plane(const float o[3], const float a[3], const float b[3]);

  private:
    CCP3_Renderers_Planes_plane_data *data;

    static void generate_transform_matrix(const float o[3], const float a[3],
					  const float x[3], float r[3][3]);

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<drawable_obj>(*this);
      // Todo
    }
#endif // DLV_USES_SERIALIZE
  };

  class line_obj : public drawable_obj {
  public:
    line_obj(const OMobj_id id, const bool sp);
    ~line_obj();

    toolkit_obj get_id() const;

    void update_line(const float a[3], const float b[3]);

  private:
    CCP3_Renderers_Lines_line_data *data;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<drawable_obj>(*this);
      // Todo
    }
#endif // DLV_USES_SERIALIZE
  };

  class point_obj : public drawable_obj {
  public:
    point_obj(const OMobj_id id, const bool sp);
    ~point_obj();

    toolkit_obj get_id() const;

    void update_point(const float point[3]);

  private:
    CCP3_Renderers_Points_point_data *data;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<drawable_obj>(*this);
      // Todo
    }
#endif // DLV_USES_SERIALIZE
  };

  class wulff_obj : public drawable_obj {
  public:
    wulff_obj(const OMobj_id id);
    ~wulff_obj();

    toolkit_obj get_id() const;

    bool wulff(float (*vertices)[3], const int nverts, float (*colours)[3],
	       float (*normals)[3], long connects[], const long nconnects,
	       float (*pos)[3], string labels[], int nodes[], const int nnodes,
	       float (*lverts)[3], int nlverts, long lines[],
	       const long nlines, char message[], const int mlen);

  private:
    CCP3_Renderers_Volumes_wulff_plot_data *data;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<drawable_obj>(*this);
      // Todo
    }
#endif // DLV_USES_SERIALIZE
  };

  class leed_obj : public drawable_obj {
  public:
    leed_obj(const OMobj_id id);
    ~leed_obj();

    toolkit_obj get_id() const;

    void update_leed(float (*points)[2], const int npoints,
		     float (*domains)[4], const int ndomains, int *colours);

  private:
    CCP3_Renderers_Plots_leed_data *data;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<drawable_obj>(*this);
      // Todo
    }
#endif // DLV_USES_SERIALIZE
  };

}

inline CCP3_Renderers_Plots_dos_data *DLV::rect_dos::get_data()
{
  return data;
}


#endif // DLV_DRAWABLE_PRIVATE


