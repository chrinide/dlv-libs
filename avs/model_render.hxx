
#ifndef DLV_MODEL_RENDERER
#define DLV_MODEL_RENDERER

// Forward declarations
class CCP3_Renderers_Model_Model_R;
class CCP3_Renderers_Model_Model_K;
class CCP3_Renderers_Model_Outline;
class CCP3_Renderers_Model_Shell;

namespace DLV {

  class model_display_obj {
  public:
    virtual ~model_display_obj();
  };

  class model_display_r : public model_display_obj {
  public:
    model_display_r(class render_parent *p);
    ~model_display_r();
    class toolkit_obj get_ui_obj() const;

    void deactivate_atom_flags();
    void activate_atom_flags(const string label);
    void toggle_atom_flags(const string label);

    void copy_settings(const model_display_r *parent);
    void get_lattice_data(bool &draw, bool &labels) const;
    void set_lattice(float ipoints[][3], float fpoints[][3], const int n);
    void set_lattice_labels(float points[][3], const char *labels[],
			    const int n);
    void detach_lattice();
    void detach_lattice_labels();
    void get_atom_data(bool &draw, float &scale, int &select,
		       bool &use_flags, int &flag_type) const;
    int get_atom_group_index() const;
    int get_selection_symmetry() const;
    void set_selection_symmetry(const int s);
    void draw_opaque_atoms(float coords[][3], float colours[][3],
			   float radii[], const int natoms, const int nframes);
    void draw_transparent_atoms(float coords[][3], float colours[][3],
				float radii[], const int natoms,
				const int nframes);
    void draw_edit_atoms(float coords[][3], float colours[][3],
			 float radii[], const int natoms, const int nframes);
    void detach_opaque_atoms();
    void detach_transparent_atoms();
    void detach_edit_atoms();
    void detach_all_atoms();
    void get_bond_data(bool &draw, bool &polyhedra, float &overlap,
		       bool &tubes, float &radius, int &subdiv) const;
    void draw_opaque_bonds(float coords[][3], float colours[][3],
			   const long nbonds, long connects[], const long nc,
			   const int nframes, const bool tubes,
			   const int frame);
    void draw_transparent_bonds(float coords[][3], float colours[][3],
				const long nbonds, long connects[],
				const long nc, const int nframes,
				const bool tubes, const int frame);
    void draw_edit_bonds(float coords[][3], float colours[][3],
			 const long nbonds, long connects[], const long nc,
			 const int nframes, const bool tubes, const int frame);
    void detach_opaque_bonds();
    void detach_transparent_bonds();
    void detach_edit_bonds();
    void detach_all_bonds();
    void draw_polyhedra(float coords[][3], float colours[][3],
			const long nverts, long connects[], const long nc,
			const int nframes, const int frame);
    void detach_polyhedra();
    void attach_editor(const bool v, const toolkit_obj &t, float matrix[4][4],
		       float translate[3], float centre[3], char message[],
		       const int mlen) const;

  private:
    class CCP3_Renderers_Model_Model_R *obj;
    bool lattice_attached;
    bool lattice_labels;
    bool opaque_atoms;
    bool transparent_atoms;
    bool edit_atoms;
    bool opaque_line_bonds;
    bool transparent_line_bonds;
    bool edit_line_bonds;
    bool opaque_tube_bonds;
    bool transparent_tube_bonds;
    bool edit_tube_bonds;
    bool polyhedra;

    void detach_atoms();
    void detach_line_bonds();
    void detach_tube_bonds();

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class model_display_k : public model_display_obj {
  public:
    model_display_k(class render_parent *p);
    ~model_display_k();
    class toolkit_obj get_ui_obj() const;

    void get_lattice_data(bool &draw, bool &labels) const;
    void set_lattice(float vertices[][3], const int nv, long connects[],
		     const long nc);
    void detach_lattice();

  private:
    class CCP3_Renderers_Model_Model_K *obj;
    bool lattice_attached;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class model_outline_r : public model_display_obj {
  public:
    model_outline_r(class render_parent *p);
    ~model_outline_r();
    class toolkit_obj get_ui_obj() const;

    void set_data(float vertices[][3], const int nvertices, int planes[],
		  const int nplanes, float iradius[], float oradius[],
		  float dmin[], float dmax[], int orientation[],
		  const int ncylinders, float radii[], const int nspheres);

  private:
    class CCP3_Renderers_Model_Outline *obj;
    bool data_set;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class model_shell_r : public model_display_obj {
  public:
    model_shell_r(class render_parent *p);
    ~model_shell_r();
    class toolkit_obj get_ui_obj() const;

    void draw(const float colour[3], const int nshells,
	      const float radii[], const float shell_colours[][3]);

  private:
    class CCP3_Renderers_Model_Shell *obj;
    bool is_drawn;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::model_display_r *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::model_display_r(DLV::render_parent::get_serialize_obj());
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::model_display_k *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::model_display_k(DLV::render_parent::get_serialize_obj());
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::model_outline_r *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::model_outline_r(DLV::render_parent::get_serialize_obj());
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::model_shell_r *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::model_shell_r(DLV::render_parent::get_serialize_obj());
    }

  }
}

#endif // DLV_USES_SERIALIZE

#endif // DLV_MODEL_RENDERER
