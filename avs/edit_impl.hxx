
#ifndef DLV_EDIT_PRIVATE
#define DLV_EDIT_PRIVATE

namespace DLV {

  enum edit_object_types {
    EDIT_orthoslice = 0, EDIT_extend = 1, EDIT_cut = 2, EDIT_slice = 3,
    EDIT_crop = 4, EDIT_clamp = 5, EDIT_downsize = 6, EDIT_math = 7,
    EDIT_vol = 8, EDIT_eigenvec = 9, EDIT_intens = 10, EDIT_plot = 11
  };

  class orthoslice : public edited_obj {
  public:
    orthoslice(const OMobj_id id, const bool sp);
    ~orthoslice();

    void attach_data(const drawable_obj *draw);
    void attach_params() const;
    void reload_data(drawable_obj *draw);

  protected:
    int get_type() const;
    
  private:
    CCP3_Renderers_Edits_orthoslice *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class slice : public edited_obj {
  public:
    slice(const OMobj_id id, const bool sp);
    ~slice();

    void attach_data(const drawable_obj *draw);
    void attach_params() const;
    void reload_data(drawable_obj *draw);
    bool update_slice(const class drawable_obj *draw, char message[],
		      const int mlen);

  protected:
    int get_type() const;
    
  private:
    CCP3_Renderers_Edits_slice *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class clamp : public edited_obj {
  public:
    clamp(const OMobj_id id, const bool sp);
    ~clamp();

    void attach_data(const drawable_obj *draw, const int component);
    void attach_params() const;
    void reload_data(drawable_obj *draw);

  protected:
    int get_type() const;
    
  private:
    CCP3_Renderers_Edits_clamp *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class crop : public edited_obj {
  public:
    crop(const OMobj_id id, const bool sp);
    ~crop();

    void attach_data(const drawable_obj *draw);
    void attach_params() const;
    void reload_data(drawable_obj *draw);

  protected:
    int get_type() const;
    
  private:
    CCP3_Renderers_Edits_crop *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class cut : public edited_obj {
  public:
    cut(const OMobj_id id, const bool sp);
    ~cut();

    void attach_data(const drawable_obj *draw);
    void attach_params() const;
    void reload_data(drawable_obj *draw);
    bool update_cut(const class drawable_obj *draw, char message[],
		    const int mlen);

  protected:
    int get_type() const;
    
  private:
    CCP3_Renderers_Edits_cut *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class downsize : public edited_obj {
  public:
    downsize(const OMobj_id id, const bool sp);
    ~downsize();

    void attach_data(const drawable_obj *draw);
    void attach_params() const;
    void reload_data(drawable_obj *draw);

  protected:
    int get_type() const;
    
  private:
    CCP3_Renderers_Edits_downsize *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class extension : public edited_obj {
  public:
    extension(const OMobj_id id, const int x, const int y,
	      const int z, const bool sp);
    ~extension();

    void attach_data(const drawable_obj *draw);
    void attach_params() const;
    void reload_data(drawable_obj *draw);

    bool update_extension(const int na, const int nb, const int nc,
			  const float o[3], const float a[3],
			  const float b[3], const float c[3],
			  const int onx, const int ony, const int onz,
			  float *origdata[], const int vdims[],
			  const int ndata, char message[], const int mlen);

  protected:
    int get_type() const;
    
  private:
    CCP3_Renderers_Edits_extend_obj *obj;
    class drawable_obj *field;
    int incx;
    int incy;
    int incz;
    int nx;
    int ny;
    int nz;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class data_math : public edited_obj {
  public:
    data_math(const OMobj_id id, const bool sp);
    ~data_math();

    void attach_data(const drawable_obj *draw, const int component);
    void attach_params() const;
    void reload_data(drawable_obj *draw);

    bool update_math(const class drawable_obj *draw, const int cmpt,
		     const int index, char message[], const int mlen);

  protected:
    int get_type() const;
    
  private:
    CCP3_Renderers_Edits_data_math *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class volume_render : public edited_obj {
  public:
    volume_render(const OMobj_id id, const bool sp);
    ~volume_render();

    void attach_data(const drawable_obj *draw);
    void attach_params() const;
    void reload_data(drawable_obj *draw);

    bool update_volr(const float o[3], const float a[3],
		     const float b[3], const float c[3],
		     const int nx, const int ny, const int nz,
		     float *origdata[], const int vdims[],
		     const int ndata, char message[], const int mlen);

  protected:
    int get_type() const;
    
  private:
    CCP3_Renderers_Edits_volr_obj *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class phonon_spectrum : public edited_obj {
  public:
    phonon_spectrum(const OMobj_id id, const int i);
    ~phonon_spectrum();

    void attach_data();
    void attach_params() const;
    void reload_data(drawable_obj *draw);

    bool update_spectrum(float grid[], float data[], const int n,
			 const string name, char message[], const int mlen);

    int get_index() const;

  protected:
    int get_type() const;
    
  private:
    CCP3_Renderers_Edits_spectrum_data *obj;
    int index;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class phonon_traj : public edited_obj {
  public:
    phonon_traj(const OMobj_id id, const int i);
    ~phonon_traj();

    void attach_data();
    void attach_params() const;
    void reload_data(drawable_obj *draw);

    int get_index() const;

  protected:
    int get_type() const;
    
  private:
    CCP3_Renderers_Edits_phonon_trajectory *obj;
    int index;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class dos_shift : public edited_obj {
  public:
    dos_shift(const OMobj_id id, const bool sp);
    ~dos_shift();

    void attach_data(const drawable_obj *draw);
    void attach_params() const;
    void reload_data(drawable_obj *draw);
    bool update_shift(const float shift, char message[], const int mlen);

  protected:
    int get_type() const;
    
  private:
    CCP3_Renderers_Edits_edit_dos *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class band_shift : public edited_obj {
  public:
    band_shift(const OMobj_id id, const bool sp);
    ~band_shift();

    void attach_data(const drawable_obj *draw);
    void attach_params() const;
    void reload_data(drawable_obj *draw);
    bool update_shift(const float shift, char message[], const int mlen);

  protected:
    int get_type() const;
    
  private:
    CCP3_Renderers_Edits_edit_bands *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  // Todo - do we need this? just use bands?
  class bdos_shift : public edited_obj {
  public:
    bdos_shift(const OMobj_id id, const bool sp);
    ~bdos_shift();

    void attach_data(const drawable_obj *draw);
    void attach_params() const;
    void reload_data(drawable_obj *draw);
    bool update_shift(const float shift, char message[], const int mlen);

  protected:
    int get_type() const;
    
  private:
    CCP3_Renderers_Edits_edit_bands *obj;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

}

inline DLV::orthoslice::orthoslice(OMobj_id id, const bool sp)
  : edited_obj(sp), obj(new CCP3_Renderers_Edits_orthoslice(id))
{
}

inline DLV::slice::slice(OMobj_id id, const bool sp)
  : edited_obj(sp), obj(new CCP3_Renderers_Edits_slice(id))
{
}

inline DLV::clamp::clamp(OMobj_id id, const bool sp)
  : edited_obj(sp), obj(new CCP3_Renderers_Edits_clamp(id))
{
}

inline DLV::crop::crop(OMobj_id id, const bool sp)
  : edited_obj(sp), obj(new CCP3_Renderers_Edits_crop(id))
{
}

inline DLV::cut::cut(OMobj_id id, const bool sp)
  : edited_obj(sp), obj(new CCP3_Renderers_Edits_cut(id))
{
}

inline DLV::downsize::downsize(OMobj_id id, const bool sp)
  : edited_obj(sp), obj(new CCP3_Renderers_Edits_downsize(id))
{
}

inline DLV::extension::extension(OMobj_id id, const int x, const int y,
				 const int z, const bool sp)
  : edited_obj(sp), obj(new CCP3_Renderers_Edits_extend_obj(id)),
    field(0), incx(0), incy(0), incz(0), nx(x), ny(y), nz(z)
{
}

inline DLV::data_math::data_math(OMobj_id id, const bool sp)
  : edited_obj(sp), obj(new CCP3_Renderers_Edits_data_math(id))
{
}

inline DLV::volume_render::volume_render(OMobj_id id, const bool sp)
  : edited_obj(sp), obj(new CCP3_Renderers_Edits_volr_obj(id))
{
}

inline DLV::phonon_spectrum::phonon_spectrum(OMobj_id id, const int i)
  : edited_obj(false), obj(new CCP3_Renderers_Edits_spectrum_data(id)),
    index(i)
{
}

inline int DLV::phonon_spectrum::get_index() const
{
  return index;
}

inline DLV::phonon_traj::phonon_traj(OMobj_id id, const int i)
  : edited_obj(false), obj(new CCP3_Renderers_Edits_phonon_trajectory(id)),
    index(i)
{
}

inline int DLV::phonon_traj::get_index() const
{
  return index;
}

inline DLV::dos_shift::dos_shift(OMobj_id id, const bool sp)
  : edited_obj(sp), obj(new CCP3_Renderers_Edits_edit_dos(id))
{
}

inline DLV::band_shift::band_shift(OMobj_id id, const bool sp)
  : edited_obj(sp), obj(new CCP3_Renderers_Edits_edit_bands(id))
{
}

inline DLV::bdos_shift::bdos_shift(OMobj_id id, const bool sp)
  : edited_obj(sp), obj(new CCP3_Renderers_Edits_edit_bands(id))
{
}

#endif // DLV_EDIT_PRIVATE
