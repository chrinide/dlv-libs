
#include <avs/omx.hxx>
#include "../dlv/types.hxx"
#include "../graphics/calculations.hxx"

DLV::string DLV::get_code_path(const char code[])
{
  char stuff[1024];
  strcpy(stuff, "DLV.calculations.");
  strcat(stuff, code);
  strcat(stuff, ".exec_location");
  OMobj_id obj = OMfind_str_subobj(OMinst_obj, stuff, OM_OBJ_RW);
  DLV::string path;
  if (!OMis_null_obj(obj)) {
    stuff[0] = '\0';
    char *str = stuff;
    OMget_str_val(obj, &str, 1024);
    path = stuff;
  }
  return path;
}

void DLV::show_job_panel()
{
  static OMobj_id panel = OMnull_obj;
  if (OMis_null_obj(panel))
    panel = OMfind_str_subobj(OMinst_obj,
			      "DLV.app_window.JobUI.menu_display.panel_open",
			      OM_OBJ_RW);
  OMset_int_val(panel, 1);
}

void DLV::clear_job_list()
{
  OMobj_id obj = OMfind_str_subobj(OMinst_obj, "DLV.project.job_list",
				   OM_OBJ_RW);
  OMset_array_size(obj, 0);
}

void DLV::add_to_job_list(const string name, const string state,
			  const int index)
{
  OMobj_id obj = OMfind_str_subobj(OMinst_obj, "DLV.project.job_list",
				   OM_OBJ_RW);
  xp_long n = 0;
  OMget_array_size(obj, &n);
  xp_long i = n + 1;
  OMset_array_size(obj, i);
  OMobj_id arr_id = OMnull_obj;
  OMget_array_val(obj, n, &arr_id, OM_OBJ_RW);
  OMobj_id sub_obj = OMfind_str_subobj(arr_id, "label", OM_OBJ_RW);
  OMset_str_val(sub_obj, name.c_str());
  sub_obj = OMfind_str_subobj(arr_id, "status", OM_OBJ_RW);
  OMset_str_val(sub_obj, state.c_str());
  sub_obj = OMfind_str_subobj(arr_id, "index", OM_OBJ_RW);
  OMset_int_val(sub_obj, index);
}

void DLV::update_job_status(const string state, const int index)
{
  OMobj_id obj = OMfind_str_subobj(OMinst_obj, "DLV.project.job_list",
				   OM_OBJ_RW);
  xp_long n = 0;
  OMget_array_size(obj, &n);
  OMobj_id arr_id = OMnull_obj;
  OMobj_id sub_obj = OMnull_obj;
  for (xp_long i = 0; i < n; i++) {
    OMget_array_val(obj, i, &arr_id, OM_OBJ_RW);
    sub_obj = OMfind_str_subobj(arr_id, "index", OM_OBJ_RW);
    int obj_index = -1;
    OMget_int_val(sub_obj, &obj_index);
    if (index == obj_index) {
      sub_obj = OMfind_str_subobj(arr_id, "status", OM_OBJ_RW);
      OMset_str_val(sub_obj, state.c_str());
      break;
    }
  }
}

void DLV::remove_from_job_list(const int index)
{
  OMobj_id obj = OMfind_str_subobj(OMinst_obj, "DLV.project.job_list",
				   OM_OBJ_RW);
  xp_long n = 0;
  OMget_array_size(obj, &n);
  if (n == 1)
    OMset_array_size(obj, 0);
  else {
    int pos;
    OMobj_id arr_id1 = OMnull_obj;
    OMobj_id sub_obj1 = OMnull_obj;
    int value;
    // Find index in the array
    for (pos = 0; pos < n; pos++) {
      OMget_array_val(obj, pos, &arr_id1, OM_OBJ_RW);
      sub_obj1 = OMfind_str_subobj(arr_id1, "index", OM_OBJ_RW);
      OMget_int_val(sub_obj1, &value);
      if (value == index)
	break;
    }
    // shift everything back one
    OMobj_id arr_id2 = OMnull_obj;
    OMobj_id sub_obj2 = OMnull_obj;
    for (int j = pos + 1; j < n; j++) {
      OMget_array_val(obj, j, &arr_id2, OM_OBJ_RW);
      sub_obj2 = OMfind_str_subobj(arr_id2, "label", OM_OBJ_RW);
      char label[256];
      char *buff = label;
      OMget_str_val(sub_obj2, &buff, 256);
      sub_obj1 = OMfind_str_subobj(arr_id1, "label", OM_OBJ_RW);
      OMset_str_val(sub_obj1, label);
      sub_obj2 = OMfind_str_subobj(arr_id2, "status", OM_OBJ_RW);
      OMget_str_val(sub_obj2, &buff, 256);
      sub_obj1 = OMfind_str_subobj(arr_id1, "status", OM_OBJ_RW);
      OMset_str_val(sub_obj1, label);
      sub_obj2 = OMfind_str_subobj(arr_id2, "index", OM_OBJ_RW);
      OMget_int_val(sub_obj2, &value);
      sub_obj1 = OMfind_str_subobj(arr_id1, "index", OM_OBJ_RW);
      OMset_int_val(sub_obj1, value);
      arr_id1 = arr_id2;
    }
    OMset_array_size(obj, n - 1);
  }
}
