
#ifndef CRYSTAL_EXPRESS
#define CRYSTAL_EXPRESS

namespace CRYSTAL {

  void trigger_dos_atom_selections();
  void trigger_bdos_atom_selections();

}

#endif // CRYSTAL_EXPRESS
