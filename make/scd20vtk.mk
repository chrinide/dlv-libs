
CXX = g++
PIC = -fPIC
CXXFLAGS = -g -std=c++11 -pedantic -Wall -Wno-long-long -pthread -DENABLE_DLV_JOB_THREADS -DDLV_USES_SERIALIZE -DENABLE_DLV_GRAPHICS -DDLV_USES_VTK_GRAPHICS
# -DENABLE_DLV_GRAPHICS
# -DDLV_USES_AVS_GRAPHICS -DXP_WIDE_API
# -DDLV_USES_SERIALIZE
# -DENABLE_ROD
# release code
#CXXFLAGS = -g -O2 -ansi -pedantic -Wall -Wno-long-long -pthread -DENABLE_DLV_JOB_THREADS -DENABLE_DLV_GRAPHICS -DDLV_USES_AVS_GRAPHICS -DXP_WIDE_API -DDLV_USES_SERIALIZE -DDLV_RELEASE -DBOOST_DISABLE_ASSERTS
CXXTEMPLATES = $(CXXFLAGS)
SHARED = -shared
LD = $(CXX)
LDFLAGS = $(CXXFLAGS) $(SHARED)
SO = so

TOOLDIR = vtk
TOOLSRC = $(SRCDIR)/$(TOOLDIR)
TOOLLIB = $(LIBDIR)/libDLVgraphics_$(TOOLDIR).$(SO)

BOOSTDIR =
BOOSTLIB =

PYHDR = /usr/include/python3.10

CCTBXDIR = /home/bgs/projects/cctbx/include
CCTBXLIB = /home/bgs/projects/cctbx/libcctbx.a
IOTBXLIB = /home/bgs/projects/cctbx/libiotbx.a

RODDIR = ../../rod/src
RODLIB = ../../rod/librod.a

# Only for DLV_DL
#BABELDIR = ../../babel/include/openbabel-2.0
#BABELLIB = /home/bgs/express/3.0/babel/lib/libopenbabel.a
#else
BABELDIR =
BABELLIB =

RENDERDIR = /home/bgs/scratch/vtk/install/include/vtk-9.2
RENDERLIB = -L/home/bgs/scratch/vtk/install/lib64 -lvtkRenderingContextOpenGL2-9.2 -lvtkRenderingLOD-9.2 -lvtkRenderingOpenGL2-9.2 -lvtkRenderingCore-9.2 -lvtkFiltersSources-9.2 -lvtkFiltersCore-9.2 -lvtkCommonColor-9.2 -lvtkCommonExecutionModel-9.2 -lvtkCommonDataModel-9.2 -lvtkCommonCore-9.2 -lvtksys-9.2

GUIDIR =
GUILIB = 

INCDIRS = -I$(CCTBXDIR) -I$(RODDIR) -I$(RODDIR)/include -I$(RENDERDIR)

GRAPHICSLIBS = $(RENDERLIB)
BASETYPELIBS = -lDLVgraphics_$(TOOLDIR) -lboost_serialization -lboost_filesystem -lboost_thread -lboost_chrono
WULFFLIBS =
SYMMETRYLIBS = $(CCTBXLIB)
MODELLIBS = $(IOTBXLIB) -lboost_serialization
DATALIBS = -lboost_serialization
DATAMODELLIBS = -lboost_serialization
