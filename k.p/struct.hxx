
#ifndef K_P_LOAD_STRUCTURE
#define K_P_LOAD_STRUCTURE

namespace K_P {

  class load_structure : public DLV::load_model_op, public structure_file {
  public:
    static operation *create(const char name[], const char filename[],
			     char message[], const int_g mlen);

  protected:
    load_structure(DLV::model *m, const char file[]);

    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<DLV::load_model_op>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

}

inline K_P::load_structure::load_structure(DLV::model *m, const char file[])
  : load_model_op(m, file)
{
}

#endif // K_P_LOAD_STRUCTURE
