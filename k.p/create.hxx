
#ifndef K_P_CREATE_STRUCTURE
#define K_P_CREATE_STRUCTURE

namespace K_P {

  class create_structure : public DLV::create_model_op {
  public:
    static operation *create(int shape,float base, float height, float c,
			     bool wl, int n, float WLthickness, bool lat, int dim, int LatType, float a1, float a2, float a3, int dir, char message[], const int_g mlen);

  protected:
    create_structure(DLV::model *m,int shape,  float base, float height, float c, bool wl,int n, float WLthickness);
    static void read(DLV::model *structure, int shape, float base, float height, float c,
			    bool wl, int n, float WLthickness, char message[], const int_g mlen);

    DLV::string get_name() const;

  private:
    int s;
    float b, h, c1;
    bool wettinglayer;
    int nsides;
    float wlthickness;
    bool lat;
    int dim;
    int LatType;
    float a1;
    float a2;
    float a3;
    int dir;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<DLV::create_model_op>(*this);
      ar & s;
      ar & b;
      ar & h;
      ar & c1;
      ar & wettinglayer;
      ar & nsides;
      ar & wlthickness;
      ar & lat;
      ar & dim;
      ar & LatType;
      ar & a1;
      ar & a2;
      ar & a3;
      ar & dir;
    }
#endif // DLV_USES_SERIALIZE
  };

}

inline K_P::create_structure::create_structure(DLV::model *m, int shape,
					       float base, float height,
					       float c, bool wl, int n, float WLthickness)
  : create_model_op(m),s(shape), b(base), h(height), c1(c), wettinglayer(wl),
    nsides(n), wlthickness(WLthickness) 
{
}

#endif // K_P_CREATE_STRUCTURE
