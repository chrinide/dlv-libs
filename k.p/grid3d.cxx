
#include <cstdio>
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/constants.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_vol.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_admin.hxx"
#include "calcs.hxx"
#include "grid3d.hxx"

DLV::operation *K_P::load_3d_data::create(const char filename[],int_g N_x, int_g N_y, int_g N_z, real_g L_x, real_g L_y, real_g L_z, char message[], const int_g mlen)
{
  load_3d_data *op = new load_3d_data(filename,N_x, N_y, N_z, L_x, L_y, L_z);
  op->attach_pending();
  DLV::data_object *data = op->read_data(op, filename, N_x, N_y, N_z, L_x, L_y, L_z, message, mlen);
  if (data == 0) {
    op->cancel_pending();
    delete op;
    op = 0;
  } else {
    op->accept_pending();
    //op->attach();
    op->attach_data(data);
  }
  return op;
}

DLV::string K_P::load_3d_data::get_name() const
{
  return ("Load k.p 3D data - " + get_filename());
}

bool K_P::load_3d_data::reload_data(DLV::data_object *data,
				    char message[], const int_g mlen)
{
  strncpy(message, "Reload k.p grid3d not implemented", mlen);
  return false;
}

DLV::volume_data *K_P::grid3D::read_data(DLV::operation *op,
					 const char filename[],int_g N_x, int_g N_y, int_g N_z, real_g L_x, real_g L_y, real_g L_z,
					 char message[], const int_g mlen)
{
  DLV::volume_data *data = 0;
  if (DLV::check_filename(filename, message, mlen)) {
     std::FILE *input = fopen(filename, "r");
    if (input != 0) {
      char label[128];
      int_g nx = N_x;
      int_g ny = N_y;
      int_g nz = N_z;
      real_g origin[3];
      real_g astep[3];
      real_g bstep[3];
      real_g cstep[3];

      if (get_grid(input,data, N_x, N_y, N_z, L_x, L_y, L_z, nx, ny, nz, origin, astep, bstep, cstep,
		   label, message, mlen)) {
	if (op->get_model()->get_number_of_periodic_dims() == 0){
	data = new DLV::real_space_volume("k.p", filename, op, nx, ny, nz,
							     origin, astep, bstep, cstep);
	} else {
	  data = new DLV::rspace_periodic_volume("k.p", filename, op, nx, ny, nz,
							     origin, astep, bstep, cstep);
	} 
	if (!get_data(input, data,N_x, N_y, N_z, L_x, L_y, L_z, nx, ny, nz, label, message, mlen)) {
	  delete data;
	  data = 0;
	}
      }
      fclose(input);
    } else
      strncpy(message, "Failed to open file", mlen);
  }
  return data;
}

bool K_P::grid3D::get_grid(std::FILE *input, DLV::volume_data *data,
			    const int_g N_x, const int_g N_y, const int_g N_z,const
                            real_g L_x, const real_g L_y, const real_g L_z, int_g &nx, int_g &ny,                       
			   int_g &nz, real_g origin[3], real_g astep[3],
			   real_g bstep[3], real_g cstep[3], char label[],
			   char message[], const int_g mlen)
{
  //char text[128];
  //char *buff = text;
  //size_t s = 128;
  if (nx < 1 or ny < 1 or nz < 1) {
    strncpy(message,
	    "Empty grid - is this really a k.p volume file?", mlen);
    return false;
  } else
    return read_grid(input, data, N_x, N_y, N_z, L_x, L_y, L_z, origin, astep, bstep, cstep, message, mlen);
}

bool K_P::grid3D::read_grid(std::FILE *input, DLV::volume_data *data,
			    const int_g N_x, const int_g N_y, const int_g N_z,const
                            real_g L_x, const real_g L_y, const real_g L_z,  real_g origin[3],
			    real_g astep[3], real_g bstep[3], real_g cstep[3],
			    char message[], const int_g mlen)
{
      origin[0] = -(L_x/2);
      origin[1] = -(L_y/2);
      origin[2] = -(L_z/2);
      
      astep[0] = L_x/(N_x - 1);
      astep[1] = 0;
      astep[2] = 0;

      bstep[0] = 0;
      bstep[1] = L_y/(N_y - 1);
      bstep[2] = 0;

      cstep[0] = 0;
      cstep[1] = 0;
      cstep[2] = L_z/(N_z - 1);
  //char buff[128];
  // Get end of line char after grid!
  //input.fgets(buff, 128);
  return true;
}




bool K_P::grid3D::get_data(std::FILE *input, DLV::volume_data *data,const int_g N_x, const int_g N_y, const int_g N_z,const
			   real_g L_x, const real_g L_y, const real_g L_z,
			   const int_g nx, const int_g ny, const int_g nz,
			   const char label[], char message[],
			   const int_g mlen)
{
    int_l n = (int_l)N_x * (int_l)N_y * (int_l)N_z;
  real_g *array = new real_g[n];
  if (array == 0) {
    strncpy(message, "Unable to allocate memory for data", mlen);
    return false;
  } else {
    int i = 0;
    real_g dummyx, dummyy, dummyz;
    for (int_l ipx = 0; ipx < N_x; ipx++) {
      for (int_l ipy = 0; ipy < N_y; ipy++) {
	for (int_l ipz = 0; ipz < N_z; ipz++) {
	  fscanf(input, "%f %f %f %f", &dummyx, &dummyy, &dummyz, &array[(ipz*N_y*N_x)+(ipy*N_x)+ipx]);
	  i++;
	}
      }
    }
      data->add_data(array, 1, "Wavefunction", false);
    return true;
  }
}


