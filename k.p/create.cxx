
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_model.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "calcs.hxx"
#include "create.hxx"
#include "../dlv/constants.hxx"

DLV::operation *K_P::create_structure::create(int shape, float base, float height, float c, bool wl, int n, float WLthickness, bool lat, int dim, int LatType, float a1,float a2, float a3, int dir,
					    char message[], const int_g mlen)
{
  if (!lat) {
  
  dim = 0;

 } else {

  dim += 1; 

 }

  DLV::string name = "test";
  DLV::model *structure = DLV::model::create_outline(name, dim, a1, a2, a3, dir, LatType);
  read(structure, shape, base, height, c, wl, n, WLthickness, message, mlen);
  create_structure *op = 0;
  if (structure != 0) {
    op = new create_structure(structure, shape, base, height, c, wl, n, WLthickness);
    attach_base(op);
  }
  return op;
}

DLV::string K_P::create_structure::get_name() const
{
  return (get_model_name() + " - Create k.p structure");
}

void K_P::create_structure::read(DLV::model *structure, int shape, float base, float height,float c, bool wl, int n, float WLthickness,
					char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  if (structure == 0) {
    strncpy(message, "Create model failed", mlen - 1);
    ok = DLV_ERROR;
  } else {

    // creates wetting layer  
    if (wl){
         float l;
	
	 l = 2 * base;

      const DLV::coord_type wlsq_cord0[4][3] = {
	{-1,-1, 0},
	{1, -1, 0},
	{1, 1, 0},
	{-1, 1, 0}
      };

      DLV::coord_type wlsq_cord[4][3];

      for (int n=0; n<4; n++){
        
	float x = 0, y= 0, z = 0;
        
	x = wlsq_cord0[n][0];
	y = wlsq_cord0[n][1];
	z = wlsq_cord0[n][2];
        
	x *= l;
	y *= l;
	z *= l;
        
	wlsq_cord[n][0] = x;
	wlsq_cord[n][1] = y;
	wlsq_cord[n][2] = z;
        
      }

      if (!structure->add_polygon(wlsq_cord, 4, message, mlen)) {
	ok = DLV_ERROR;
      } 

      DLV::coord_type wlsq_cord1[4][3];

      for (int n=0; n<4; n++){
        
	float x = 0, y= 0, z = 0;
        
	x = wlsq_cord0[n][0];
	y = wlsq_cord0[n][1];
	z = wlsq_cord0[n][2];
        
	x *= l;
	y *= l;
	z = - WLthickness;
        
	wlsq_cord1[n][0] = x;
	wlsq_cord1[n][1] = y;
	wlsq_cord1[n][2] = z;
        
      }

      if (!structure->add_polygon(wlsq_cord1, 4, message, mlen)) {
	ok = DLV_ERROR;
      } 

      for (int i = 0; i<=3; i++){
    
	if (i < 3){

	  DLV::coord_type wlside_cord[4][3] = {
	    { wlsq_cord[i][0], wlsq_cord[i][1], wlsq_cord[i][2]},
	    { wlsq_cord[i+1][0], wlsq_cord[i+1][1], wlsq_cord[i+1][2]},
	    { wlsq_cord1[i+1][0], wlsq_cord1[i+1][1], wlsq_cord1[i+1][2]},
	    { wlsq_cord1[i][0], wlsq_cord1[i][1], wlsq_cord1[i][2]}
	  };
	
	  if (!structure->add_polygon(wlside_cord, 4, message, mlen)) {
	    ok = DLV_ERROR;
	  }
	}

	else if (i == 3){
	  DLV::coord_type wlside_cord[4][3] = {
	    { wlsq_cord[i][0], wlsq_cord[i][1], wlsq_cord[i][2]},
	    { wlsq_cord[0][0], wlsq_cord[0][1], wlsq_cord[0][2]},
	    { wlsq_cord1[0][0], wlsq_cord1[0][1], wlsq_cord1[0][2]},
	    { wlsq_cord1[i][0], wlsq_cord1[i][1], wlsq_cord1[i][2]}
	  };

	  if (!structure->add_polygon(wlside_cord, 4, message, mlen)) {
	    ok = DLV_ERROR;
	  }
	}
      }
    
    } //end wl if 
    

    if (shape == 0){

      // create coordinates    
      // ** Square Base **
    
      //Vertex values of square to be multiplied by 1/2 base
    
      const DLV::coord_type bsquare_cord0[4][3] = {
	{-1,-1, 0},
	{1, -1, 0},
	{1, 1, 0},
	{-1, 1, 0}
      };
    
      //Array to store generated coordinates
    
      DLV::coord_type bsquare_cord[4][3];
      float b0 = 0.5 * base;
    
      //Loop generates values for x, y, z and stores in array
    
      for (int n=0; n<4; n++){
        
	float x = 0, y= 0, z = 0;
        
	x = bsquare_cord0[n][0];
	y = bsquare_cord0[n][1];
	z = bsquare_cord0[n][2];
        
	x *= b0;
	y *= b0;
	z *= b0;
        
	bsquare_cord[n][0] = x;
	bsquare_cord[n][1] = y;
	bsquare_cord[n][2] = z;
        
      }
    
      if (!structure->add_polygon(bsquare_cord, 4, message, mlen)) {
	ok = DLV_ERROR;
      }

      // **  Triangle Sides **
    
      DLV::coord_type tri_side1[3][3] = {
	{bsquare_cord[0][0], bsquare_cord[0][1], bsquare_cord[0][2]},
	{bsquare_cord[1][0],bsquare_cord[1][1], bsquare_cord[1][2]},
	{0, 0, 1}
      };
    
      tri_side1[2][2] = height;
    
      if (!structure->add_polygon(tri_side1, 3, message, mlen)) {
	ok = DLV_ERROR;
      }
    
      DLV::coord_type tri_side2[3][3] = {
	{bsquare_cord[1][0], bsquare_cord[1][1], bsquare_cord[1][2]},
	{bsquare_cord[2][0],bsquare_cord[2][1], bsquare_cord[2][2]},
	{0, 0, 1}
      };
    
      tri_side2[2][2] = height;

      if (!structure->add_polygon(tri_side2, 3, message, mlen)) {
	ok = DLV_ERROR;
      }
    
    
      DLV::coord_type tri_side3[3][3] = {
	{bsquare_cord[2][0], bsquare_cord[2][1], bsquare_cord[2][2]},
	{bsquare_cord[3][0],bsquare_cord[3][1], bsquare_cord[3][2]},
	{0, 0, 1}
      };
    
      tri_side3[2][2] = height;

      if (!structure->add_polygon(tri_side3, 3, message, mlen)) {
	ok = DLV_ERROR;
      }
 
    
      DLV::coord_type tri_side4[3][3] = {
	{bsquare_cord[3][0], bsquare_cord[3][1], bsquare_cord[3][2]},
	{bsquare_cord[0][0],bsquare_cord[0][1], bsquare_cord[0][2]},
	{0, 0, 1}
      };
    
      tri_side4[2][2] = height;
    
      if (!structure->add_polygon(tri_side4, 3, message, mlen)) {
	ok = DLV_ERROR;
      }
    
    }
    else if(shape == 1){
      /* 
      ** N-gon **
     
      ** Regular only **
     
      */
      
      const float r = base / 2;
      const float r1 = c / 2;
      float theta = 0;
      

      DLV::coord_type (*bgon_cord)[3] = new DLV::coord_type[n][3];
      DLV::coord_type (*tgon_cord)[3] = new DLV::coord_type[n][3];

      for (int i = 0; i<n; i++){

	float x = 0, y= 0, z = 0;

	if ( n == 4){
	  theta = (2 * DLV::pi * i / n) + (DLV::pi/4);
	}
	else {
	   theta = 2 * DLV::pi * i / n;
	}

	float deg = theta * 180 / DLV::pi;

	if ( deg == 90 || deg == 270 ) {

	  x = 0;
	  y = r * sin(theta);
	  z = 0;

	}

	else if (deg == 180){

	  x = r * cos(theta);
	  y = 0;
	  z = 0;
	
	}

	else {
	  
	  x = r * cos(theta);
	  y = r * sin(theta);
	  z = 0;

	}

	bgon_cord[i][0] = x;
	bgon_cord[i][1] = y;
	bgon_cord[i][2] = z;

      }

       if (!structure->add_polygon(bgon_cord, n, message, mlen)) {
	    ok = DLV_ERROR;
	  }
      
      for (int i = 0; i<n; i++){

	float x = 0, y= 0, z = 0;
	
	if ( n == 4){
	  theta = (2 * DLV::pi * i / n) + (DLV::pi/4);
	}
	else {
	   theta = 2 * DLV::pi * i / n;
	}

	float deg = theta * 180 / DLV::pi;

	if ( deg == 90 || deg == 270 ) {

	  x = 0;
	  y = r1 * sin(theta);
	  z = height;

	}

	else if (deg == 180){

	  x = r1 * cos(theta);
	  y = 0;
	  z = height;
	
	}

	else {
	  
	  x = r1 * cos(theta);
	  y = r1 * sin(theta);
	  z = height;

	}

	tgon_cord[i][0] = x;
	tgon_cord[i][1] = y;
	tgon_cord[i][2] = z;

      }

       if (!structure->add_polygon(tgon_cord, n, message, mlen)) {
	    ok = DLV_ERROR;
	  }

      for (int i=n-1; i>=0; i--){
    
	if (i == n-1){

	  DLV::coord_type nside_cord[4][3] = {
	    { bgon_cord[i][0], bgon_cord[i][1], bgon_cord[i][2]},
	    { bgon_cord[0][0], bgon_cord[0][1], bgon_cord[0][2]},
	    { tgon_cord[0][0], tgon_cord[0][1], tgon_cord[0][2]},
	    { tgon_cord[i][0], tgon_cord[i][1], tgon_cord[i][2]}
	  };

	  if (!structure->add_polygon(nside_cord, 4, message, mlen)) {
	    ok = DLV_ERROR;
	  }
	}
	else{
	
	  DLV::coord_type nside_cord[4][3] = {
	    { bgon_cord[i][0], bgon_cord[i][1], bgon_cord[i][2]},
	    { bgon_cord[i+1][0], bgon_cord[i+1][1], bgon_cord[i+1][2]},
	    { tgon_cord[i+1][0], tgon_cord[i+1][1], tgon_cord[i+1][2]},
	    { tgon_cord[i][0], tgon_cord[i][1], tgon_cord[i][2]}
	  };
	
	  if (!structure->add_polygon(nside_cord, 4, message, mlen)) {
	    ok = DLV_ERROR;
	  }
	}
      }

       delete []tgon_cord;
       delete []bgon_cord;

    }

    else if (shape == 2){

      structure->add_cylinder(0, base, 0, height, 2);

    }

     else if (shape == 3){

       structure->add_cylinder(c, base, 0, height, 2);

    }

     else if (shape == 4){

       structure->add_cylinder(0, base, 0, height, 2);

       float height1 = height * 10;
       float height2 = height * (-10);

       structure->add_cylinder(0, base, height, height1, 2);

       structure->add_cylinder(0, base, 0, height2, 2);
     }

    else if (shape == 5){

       structure->add_sphere(base);
    }

     else if (shape == 6){

       structure->add_sphere(base);
        structure->add_sphere(height);
    }

       else if (shape == 7){

       structure->add_sphere(base);
        structure->add_sphere(height);
	 structure->add_sphere(c);
    }

    if (ok != DLV_OK) {
      delete structure;
      structure = 0;
    } else
      structure->complete();
  }
  if (ok != DLV_OK)
    message[mlen - 1] = '\0';
  //return structure;
}
