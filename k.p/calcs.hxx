
#ifndef K_P_CALCS
#define K_P_CALCS

namespace K_P {

  using DLV::int_g;
  using DLV::int_l;
  using DLV::real_g;

  class structure_file {
  protected:
    static DLV::model *read(const DLV::string name, const char filename[],
			    char message[], const int_g mlen);
  };

}

#endif // K_P_CALCS
