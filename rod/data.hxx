
#ifndef ROD_DATA
#define ROD_DATA

namespace ROD {

  class data {
  private:
    static real_g *get_coeff(char el[3], real_g coeff[9],
			    char message[], const int_g mlen);
  public:
    static int_g initialise_model(char message[], const int_g mlen);
    static int_g set_missing_rod_ids();
    static int_g read_data_file(const char filename[],  bool *use_scale2,
			      char message[], const int_g mlen);
    static int_g read_par_file(const char filename[],
			       char message[], const int_g mlen);
    static int_g show_atom_labels(const int_g ltype, char message[],
				  const int_g mlen);
    static int_g update_rod_props(const int_g new_dw1, const bool use_dw1,
				  int_g &ndwtot, const int_g new_dw2,
				  const bool use_dw2, int_g &ndwtot2,
				  const int_g new_occ, const bool use_occ,
				  int_g &nocctot, const bool all,
				  char message[], const int_g mlen);
  };
}
#endif // ROD_DATA

