
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_model.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_plots.hxx"
#include "../dlv/data_atoms.hxx"
#include "calcs.hxx"
#include "struct.hxx"

DLV::operation *ROD::load_structure::create(const char name[],
					    const char filename[],
					    const char filename2[],
					    const int_g file_type,
					    const bool set_bonds,
					    char message[], const int_g mlen)
{
  // make name from both filenames when file_type = 2???
  DLV::string model_name;
  if(file_type == 2 || file_type == 4) //0=bulk, 1=surf, 2=bulk+surf, 3=fit, 4=bulk+fit
    model_name = name_from_file(name, filename, filename2);
  else if (file_type == 0)
    model_name = name_from_file(name, filename);
  else
    model_name = name_from_file(name, filename2);
  DLV::model *structure = read(model_name, filename, filename2, file_type,
			       message, mlen);
  load_structure *op = 0;
  if (structure != 0) {
    op = new load_structure(structure, filename);
    attach_base(op);
    if (set_bonds)
      op->set_bond_all();
  }
  return op;
}

DLV::string ROD::load_structure::get_name() const
{
  return (get_model_name() + " - Load rod structure file");
}

#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, ROD::load_structure *t,
				    const unsigned int file_version)
    {
      ::new(t)ROD::load_structure(0, "recover");
    }

  }
}

template <class Archive>
void ROD::load_structure::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::load_atom_model_op>(*this);
}

BOOST_CLASS_EXPORT_GUID(ROD::load_structure, "ROD::load_structure")

#  ifdef DLV_EXPLICIT_TEMPLATES

template class boost::serialization::singleton<boost::archive::detail::oserializer<boost::archive::text_oarchive, ROD::load_structure> >;
template class boost::serialization::singleton<boost::archive::detail::iserializer<boost::archive::text_iarchive, ROD::load_structure> >;
template class boost::serialization::singleton<boost::archive::detail::extra_detail::guid_initializer<ROD::load_structure> >;
template class boost::serialization::singleton<boost::serialization::extended_type_info_typeid<ROD::load_structure> >;
template class boost::serialization::singleton<boost::archive::detail::pointer_oserializer<boost::archive::text_oarchive, ROD::load_structure> >;
template class boost::serialization::singleton<boost::archive::detail::pointer_iserializer<boost::archive::text_iarchive, ROD::load_structure> >;

template class boost::serialization::singleton<boost::serialization::void_cast_detail::void_caster_primitive<ROD::load_structure, DLV::load_atom_model_op> >;

template class boost::serialization::void_cast_detail::void_caster_primitive<ROD::load_structure, DLV::load_atom_model_op>;

template class boost::serialization::detail::singleton_wrapper<boost::archive::detail::oserializer<boost::archive::text_oarchive, ROD::load_structure> >;
template class boost::serialization::detail::singleton_wrapper<boost::archive::detail::iserializer<boost::archive::text_iarchive, ROD::load_structure> >;
template class boost::serialization::detail::singleton_wrapper<boost::archive::detail::extra_detail::guid_initializer<ROD::load_structure> >;
template class boost::serialization::detail::singleton_wrapper<boost::serialization::extended_type_info_typeid<ROD::load_structure> >;
template class boost::serialization::detail::singleton_wrapper<boost::archive::detail::pointer_oserializer<boost::archive::text_oarchive, ROD::load_structure> >;
template class boost::serialization::detail::singleton_wrapper<boost::archive::detail::pointer_iserializer<boost::archive::text_iarchive, ROD::load_structure> >;
template class boost::serialization::detail::singleton_wrapper<boost::serialization::void_cast_detail::void_caster_primitive<ROD::load_structure, DLV::load_atom_model_op> >;

template class boost::archive::detail::pointer_oserializer<boost::archive::text_oarchive, ROD::load_structure>;
template class boost::archive::detail::pointer_iserializer<boost::archive::text_iarchive, ROD::load_structure>;

template class boost::archive::detail::oserializer<boost::archive::text_oarchive, ROD::load_structure>;
template class boost::archive::detail::iserializer<boost::archive::text_iarchive, ROD::load_structure>;

template void boost::archive::detail::instantiate_ptr_serialization<ROD::load_structure>(ROD::load_structure*, int, boost::archive::detail::adl_tag);
template ROD::load_structure const* boost::serialization::smart_cast<ROD::load_structure const*, DLV::load_atom_model_op const*>(DLV::load_atom_model_op const*);
template DLV::load_atom_model_op const* boost::serialization::smart_cast<DLV::load_atom_model_op const*, ROD::load_structure const*>(ROD::load_structure const*);

template class boost::serialization::extended_type_info_typeid<ROD::load_structure>;

template ROD::load_structure* boost::serialization::factory<ROD::load_structure, 0>(std::va_list);
template ROD::load_structure* boost::serialization::factory<ROD::load_structure, 1>(std::va_list);
template ROD::load_structure* boost::serialization::factory<ROD::load_structure, 2>(std::va_list);
template ROD::load_structure* boost::serialization::factory<ROD::load_structure, 3>(std::va_list);
template ROD::load_structure* boost::serialization::factory<ROD::load_structure, 4>(std::va_list);

template void ROD::load_structure::serialize<boost::archive::text_oarchive>(boost::archive::text_oarchive&, unsigned int);
template void ROD::load_structure::serialize<boost::archive::text_iarchive>(boost::archive::text_iarchive&, unsigned int);

template boost::serialization::detail::base_cast<DLV::load_atom_model_op, ROD::load_structure>::type& boost::serialization::base_object<DLV::load_atom_model_op, ROD::load_structure>(ROD::load_structure&);

#  endif // DLV_EXPLICT_TEMPLATES

#endif // DLV_USES_SERIALIZE
