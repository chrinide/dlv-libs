
#ifndef ROD_PLOT
#define ROD_PLOT

namespace ROD {

  class rod_plot : public ROD::rod_calc{
  public:
    rod_plot(DLV::model *m, const real_g att, const real_g sc,
	     const real_g sc2, const real_g be, const real_g sfr,
	     real_g dst[], real_g d1[], real_g d2[], real_g occ[],
	     const int_g ndistt, const int_g ndwt,
	     const int_g ndwt2, const int_g nocct,
	     const bool fit_d, const bool exp_d,
	     const real_g hr, const real_g kr,
	     const real_g lst, const real_g le, const int_g n,
	     const bool pl_bulk, const bool pl_surf,
	     const bool pl_both, const bool pl_exp,
	     const bool n_view);
    int_g initialise_data(char message[], const int_g mlen);
    int_g plot_rod(char message[], const int_g mlen);
    class DLV::rod1d_plot *read_rod_data(DLV::operation *op,
					 char message[],
					 const int_g mlen);
    //ideally these should be protected
    real_g get_hrod();
    real_g get_krod();
    real_g get_lstart();
    real_g get_lend();
    int_g get_nl();
    bool get_plot_bulk();
    bool get_plot_surf();
    bool get_plot_both();
    bool get_plot_exp();
    bool get_new_view();
    static DLV::operation *get_parent();
    static void save_parent(DLV::operation *op);
  protected:
    DLV::string get_name() const;
  private:
    real_g hrod;
    real_g krod;
    real_g lstart;
    real_g lend;
    int_g nl;
    bool plot_bulk;
    bool plot_surf;
    bool plot_both;
    bool plot_exp;
    bool new_view;
    static DLV::operation *plot1D_parent;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class rod_ffac : public ROD::rod_calc{
  public:
    rod_ffac(DLV::model *m, const real_g att, const real_g sc,
	     const real_g sc2, const real_g be, const real_g sfr,
	     real_g dst[], real_g d1[], real_g d2[], real_g occ[],
	     const int_g ndistt, const int_g ndwt,
	     const int_g ndwt2, const int_g nocct,
	     const bool fit_d, const bool exp_d,
	     const int_g sel_fs, real_g h_st,
	     real_g k_st, const real_g h_e,
	     const real_g k_e, const real_g h_stp,
	     const real_g k_stp,
	     const real_g l, const real_g mxq,
	     const real_g scale_factor,
	     const bool pl_calc, const bool pl_exp,
	     const bool n_view);
    int_g initialise_data(char message[], const int_g mlen);
    int_g plot_ffactors(char message[], const int_g mlen);
    class DLV::rod2d_plot *read_ffactors_data(DLV::operation *op,
					      const real_g radius,
					      char message[], const int_g mlen);
    int_g get_select_fs();
    real_g get_h_start();
    real_g get_k_start();
    real_g get_h_end();
    real_g get_k_end();
    real_g get_h_step();
    real_g get_k_step();
    real_g get_l_value();
    real_g get_maxq();
    real_g get_scale_factor();
    bool get_plot_calc();
    bool get_plot_exp();
    bool get_new_view();
    static DLV::operation *get_parent();
    static void save_parent(DLV::operation *op);
  protected:
    DLV::string get_name() const;
  private:
    int_g select_fs;
    real_g h_start;
    real_g k_start;
    real_g h_end;
    real_g k_end;
    real_g h_step;
    real_g k_step;
    real_g l_value;
    real_g maxq;
    real_g scale_factor;
    bool plot_calc;
    bool plot_exp;
    bool new_view;
    static DLV::operation *plot2D_parent;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

inline ROD::real_g ROD::rod_plot::get_hrod()
{
  return hrod;
}
inline ROD::real_g ROD::rod_plot::get_krod()
{
  return krod;
}
inline ROD::real_g ROD::rod_plot::get_lstart()
{
  return lstart;
}
inline ROD::real_g ROD::rod_plot::get_lend()
{
  return lend;
}
inline ROD::int_g ROD::rod_plot::get_nl()
{
  return nl;
}
inline bool ROD::rod_plot::get_plot_bulk()
{
  return plot_bulk;
}
inline bool ROD::rod_plot::get_plot_surf()
{
  return plot_surf;
}
inline bool ROD::rod_plot::get_plot_both()
{
  return plot_both;
}
inline bool ROD::rod_plot::get_plot_exp()
{
  return plot_exp;
}
inline bool ROD::rod_plot::get_new_view()
{
  return new_view;
}
inline ROD::int_g ROD::rod_ffac::get_select_fs()
{
  return select_fs;
}
inline ROD::real_g ROD::rod_ffac::get_h_start()
{
  return h_start;
}
inline ROD::real_g ROD::rod_ffac::get_k_start()
{
  return k_start;
}
inline ROD::real_g ROD::rod_ffac::get_h_end()
{
  return h_end;
}
inline ROD::real_g ROD::rod_ffac::get_k_end()
{
  return k_end;
}
inline ROD::real_g ROD::rod_ffac::get_h_step()
{
  return h_step;
}
inline ROD::real_g ROD::rod_ffac::get_k_step()
{
  return k_step;
}
inline ROD::real_g ROD::rod_ffac::get_l_value()
{
  return l_value;
}
inline ROD::real_g ROD::rod_ffac::get_maxq()
{
  return maxq;
}
inline ROD::real_g ROD::rod_ffac::get_scale_factor()
{
  return scale_factor;
}
inline bool ROD::rod_ffac::get_plot_calc()
{
  return plot_calc;
}
inline bool ROD::rod_ffac::get_plot_exp()
{
  return plot_exp;
}
inline bool ROD::rod_ffac::get_new_view()
{
  return new_view;
}
#endif // ROD_PLOT
