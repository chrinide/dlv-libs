
#ifndef ROD_LOAD_STRUCTURE
#define ROD_LOAD_STRUCTURE

namespace ROD {

  class load_structure : public DLV::load_atom_model_op,
			 public structure_file {
  public:
    static operation *create(const char name[], const char filename[],
			     const char filename2[],
			     const int_g file_type, const bool set_bonds,
			     char message[], const int_g mlen);
    load_structure(DLV::model *m, const char file[]);
  protected:
    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

inline ROD::load_structure::load_structure(DLV::model *m, const char file[])
  : load_atom_model_op(m, file)
{
}

#endif // ROD_LOAD_STRUCTURE
