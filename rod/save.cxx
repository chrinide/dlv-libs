#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/op_admin.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "save.hxx"
#ifdef ENABLE_ROD
#include "rod/rod.h"

DLVreturn_type ROD::save_structure::buffer(char buffer[][256],
					   const int_g file_type,
					   int_g &nlines,
					   char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  DLV::string yesno;
  int_g ntot = init_fitpar();
  switch (file_type) {
  case 0:
    strcpy (buffer[0], "bulk unit cell");
    sprintf(buffer[1],"%10.5f %10.5f %10.5f %10.3f %10.3f %10.3f",
	    DLAT[0], DLAT[1], DLAT[2],
	    DLAT[3]*RAD, DLAT[4]*RAD, DLAT[5]*RAD);
    for(int_g i = 0; i<NBULK; i++)
      sprintf(buffer[i+2],"%s %10.5f %10.5f %10.5f", ELEMENT[TB[i]],
	      XB[i], YB[i], ZB[i]);
    nlines = NBULK + 2;
    break;
  case 1:
    strcpy (buffer[0], "surface cell");
    sprintf(buffer[1],"%10.5f %10.5f %10.5f %10.3f %10.3f %10.3f",
	    DLAT[0], DLAT[1], DLAT[2],
	    DLAT[3]*RAD, DLAT[4]*RAD, DLAT[5]*RAD);
    for(int_g i = 0; i<NSURF; i++)
      sprintf(buffer[i+2],"%s %10.5f %10.5f %10.5f", ELEMENT[TS[i]],
	      XS[i], YS[i], ZS[i]);
    nlines = NSURF + 2;
    break;
  case 2:
    strcpy (buffer[0], "fit model");
    sprintf(buffer[1],"%10.5f %10.5f %10.5f %10.3f %10.3f %10.3f",
	    DLAT[0], DLAT[1], DLAT[2],
	    DLAT[3]*RAD, DLAT[4]*RAD, DLAT[5]*RAD);
    for(int_g i = 0; i<NSURF; i++)
      sprintf(buffer[i+2],"%s %10.5f %9.4f %2i %9.4f %2i %10.5f %9.4f %2i %9.4f %2i %10.5f %2i %2i %2i %2i",
	      ELEMENT[TS[i]],
	      XS[i], XCONST[i], NXDIS[i], X2CONST[i], NX2DIS[i],
	      YS[i], YCONST[i], NYDIS[i], Y2CONST[i], NY2DIS[i],
	      ZS[i], NZDIS[i], NDWS[i], NDWS2[i], NOCCUP[i]);
    nlines = NSURF + 2;
    break;
  case 3:
    strcpy (buffer[0], "!best-fit parameter values");
    strcpy (buffer[1], "!Goto set parameter menu");
    strcpy (buffer[2], "set par");
    char labels[MAXFIT][20];
    strcpy (labels[0], "scale      ");
    strcpy (labels[1], "scale2     ");
    strcpy (labels[2], "beta       ");
    strcpy (labels[3], "surffrac   ");
    for(int_g i = 0; i< NDISTOT; i++)
      sprintf(labels[4+i], "displace  %1d", i+1);
    for(int_g i = 0; i< NDWTOT; i++)
      sprintf(labels[4+NDISTOT+i], "b1         %1d", i+1);
    for(int_g i = 0; i< NDWTOT2; i++)
      sprintf(labels[4+NDISTOT+NDWTOT+i], "b2         %1d", i+1);
    for(int_g i = 0; i< NOCCTOT; i++)
      sprintf(labels[4+NDISTOT+NDWTOT+NDWTOT2+i], "occupancy %1d", i+1);
    for(int_g i = 0; i< ntot; i++){
      yesno = FIXPAR[i]==0 ? "NO " : "YES";
      sprintf(buffer[i+3],"%14s %9.4f %9.4f %9.4f %s",
	      labels[i], FITPAR[i], FITMIN[i], FITMAX[i], yesno.c_str());
    }
    strcpy (buffer[3+ntot], "return return");
    nlines = 4+ntot;
    break;
  default:
    ok = DLV_ERROR;
    strncpy(message, "unknown file type", mlen);
  }
  return ok;
}


DLVreturn_type ROD::save_structure::create(const char filename[],
					   const int_g file_type,
					   char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  save_structure *op = new save_structure(filename);
  if (op == 0) {
    strncpy(message, "Failed to allocate CRYSTAL::save operation", mlen);
    ok = DLV_ERROR;
  }
  else {
    message[0] = '\0';
    switch (file_type) {
    case 0:
      ok = op->write_bul(filename, message, mlen);
      break;
          case 1:
      ok = op->write_sur(filename, message, mlen);
      break;
    case 2:
      ok = op->write_fit(filename, message, mlen);
      break;
    case 3:
      ok = op->write_par(filename, message, mlen);
      break;
    default:
      ok = DLV_ERROR;
      strncpy(message, "unknown file type", mlen);
    }
  }
  if (ok == DLV_ERROR) {
    delete op;
    op = 0;
  } else
    op->attach();
  return ok;
}

DLV::string ROD::save_structure::get_name() const
{
  return ("Save ROD structure - " + get_filename());
}

DLVreturn_type ROD::save_structure::write_bul(const char filename[],
					      char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  std::ofstream output;
  char str[256];
  if (DLV::open_file_write(output, filename, message, mlen)) {
    output << "bulk unit cell\n";
    sprintf(str,"%10.5f %10.5f %10.5f %10.3f %10.3f %10.3f \n",
	    DLAT[0], DLAT[1], DLAT[2],
	    DLAT[3]*RAD, DLAT[4]*RAD, DLAT[5]*RAD);
    output << str;
    for(int_g i = 0; i<NBULK; i++){
      sprintf(str,"%s %10.5f %10.5f %10.5f\n", ELEMENT[TB[i]],
	      XB[i], YB[i], ZB[i]);
      output << str;
    }
    output.close();
  }
  return ok;
}

DLVreturn_type ROD::save_structure::write_sur(const char filename[],
					      char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  std::ofstream output;
  char str[256];
  if (DLV::open_file_write(output, filename, message, mlen)) {
    output << "surface cell\n";
    sprintf(str,"%10.5f %10.5f %10.5f %10.3f %10.3f %10.3f \n",
	    DLAT[0], DLAT[1], DLAT[2],
	    DLAT[3]*RAD, DLAT[4]*RAD, DLAT[5]*RAD);
    output << str;
    for(int_g i = 0; i<NSURF; i++){
      sprintf(str,"%s %10.5f %10.5f %10.5f\n", ELEMENT[TB[i]],
	      XS[i], YS[i], ZS[i]);
      output << str;
    }
    output.close();
  }
  return ok;
}

DLVreturn_type ROD::save_structure::write_fit(const char filename[],
					      char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  std::ofstream output;
  char str[256];
  if (DLV::open_file_write(output, filename, message, mlen)) {
    output << "fit model\n";
    sprintf(str,"%10.5f %10.5f %10.5f %10.3f %10.3f %10.3f \n",
	    DLAT[0], DLAT[1], DLAT[2],
	    DLAT[3]*RAD, DLAT[4]*RAD, DLAT[5]*RAD);
    output << str;
    for(int_g i = 0; i<NSURF; i++){
      sprintf(str,"%s %10.5f %9.4f %2i %9.4f %2i %10.5f %9.4f %2i %9.4f %2i %10.5f %2i %2i %2i %2i\n",
	      ELEMENT[TB[i]],
	      XS[i], XCONST[i], NXDIS[i], X2CONST[i], NX2DIS[i],
	      YS[i], YCONST[i], NYDIS[i], Y2CONST[i], NY2DIS[i],
	      ZS[i], NZDIS[i], NDWS[i], NDWS2[i], NOCCUP[i]);
      output << str;
    }
    output.close();
  }
  return ok;
}

DLVreturn_type ROD::save_structure::write_par(const char filename[],
					      char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  std::ofstream output;
  int_g ntot = init_fitpar();
  char str[256];
  if (DLV::open_file_write(output, filename, message, mlen)) {
    output << "!best-fit parameter values\n";
    output << "!Goto set parameter menu\n";
    output << "set par\n";
    DLV::string yesno;
    char labels[MAXFIT][20];
    strcpy (labels[0], "scale      ");
    strcpy (labels[1], "scale2     ");
    strcpy (labels[2], "beta       ");
    strcpy (labels[3], "surffrac   ");
    for(int_g i = 0; i< NDISTOT; i++)
      sprintf(labels[4+i], "displace  %1d", i+1);
    for(int_g i = 0; i< NDWTOT; i++)
      sprintf(labels[4+NDISTOT+i], "b1        %1d", i+1);
    for(int_g i = 0; i< NDWTOT2; i++)
      sprintf(labels[4+NDISTOT+NDWTOT+i], "b2         %1d", i+1);
    for(int_g i = 0; i< NOCCTOT; i++)
      sprintf(labels[4+NDISTOT+NDWTOT+NDWTOT2+i], "occupancy %1d", i+1);
    for(int_g i = 0; i< ntot; i++){
      yesno = FIXPAR[i]==0 ? "NO " : "YES";
      sprintf(str,"%14s %9.4f %9.4f %9.4f %s \n",
	      labels[i], FITPAR[i], FITMIN[i], FITMAX[i], yesno.c_str());
    output << str;
    }
    output << "return return\n";
    output.close();
  }
  return ok;
}

#else // ENABLE_ROD
DLVreturn_type ROD::save_structure::create(const char filename[],
					   const int_g file_type,
					   char message[], const int_g mlen)
{
  strncpy(message, "Compiled without ROD", mlen - 1);
  return DLV_ERROR;
}

DLVreturn_type ROD::save_structure::buffer(char buffer[][256],
					   const int_g file_type,
					   int_g &nlines,
					   char message[], const int_g mlen)
{
  strncpy(message, "Compiled without ROD", mlen - 1);
  return DLV_ERROR;
}


DLV::string ROD::save_structure::get_name() const
{
  return ("Save ROD structure - " + get_filename());
}

DLVreturn_type ROD::save_structure::write_bul(const char filename[],
					      char message[], const int_g mlen)
{
  strncpy(message, "Compiled without ROD", mlen - 1);
  return DLV_ERROR;
}
#endif // ENABLE_ROD


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, ROD::save_structure *t,
				    const unsigned int file_version)
    {
      ::new(t)ROD::save_structure("recover");
    }
  }
}

template <class Archive>
void ROD::save_structure::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::save_model_op>(*this);
}

BOOST_CLASS_EXPORT_GUID(ROD::save_structure, "ROD::save_structure")

#  ifdef DLV_EXPLICIT_TEMPLATES

template class boost::serialization::singleton<boost::archive::detail::oserializer<boost::archive::text_oarchive, ROD::save_structure> >;
template class boost::serialization::singleton<boost::archive::detail::iserializer<boost::archive::text_iarchive, ROD::save_structure> >;
template class boost::serialization::singleton<boost::archive::detail::extra_detail::guid_initializer<ROD::save_structure> >;
template class boost::serialization::singleton<boost::serialization::extended_type_info_typeid<ROD::save_structure> >;
template class boost::serialization::singleton<boost::archive::detail::pointer_oserializer<boost::archive::text_oarchive, ROD::save_structure> >;
template class boost::serialization::singleton<boost::archive::detail::pointer_iserializer<boost::archive::text_iarchive, ROD::save_structure> >;

template class boost::serialization::singleton<boost::serialization::void_cast_detail::void_caster_primitive<ROD::save_structure, DLV::save_model_op> >;

template class boost::serialization::void_cast_detail::void_caster_primitive<ROD::save_structure, DLV::save_model_op>;

template class boost::serialization::detail::singleton_wrapper<boost::archive::detail::oserializer<boost::archive::text_oarchive, ROD::save_structure> >;
template class boost::serialization::detail::singleton_wrapper<boost::archive::detail::iserializer<boost::archive::text_iarchive, ROD::save_structure> >;
template class boost::serialization::detail::singleton_wrapper<boost::archive::detail::extra_detail::guid_initializer<ROD::save_structure> >;
template class boost::serialization::detail::singleton_wrapper<boost::serialization::extended_type_info_typeid<ROD::save_structure> >;
template class boost::serialization::detail::singleton_wrapper<boost::archive::detail::pointer_oserializer<boost::archive::text_oarchive, ROD::save_structure> >;
template class boost::serialization::detail::singleton_wrapper<boost::archive::detail::pointer_iserializer<boost::archive::text_iarchive, ROD::save_structure> >;
template class boost::serialization::detail::singleton_wrapper<boost::serialization::void_cast_detail::void_caster_primitive<ROD::save_structure, DLV::save_model_op> >;

template class boost::archive::detail::pointer_oserializer<boost::archive::text_oarchive, ROD::save_structure>;
template class boost::archive::detail::pointer_iserializer<boost::archive::text_iarchive, ROD::save_structure>;

template class boost::archive::detail::oserializer<boost::archive::text_oarchive, ROD::save_structure>;
template class boost::archive::detail::iserializer<boost::archive::text_iarchive, ROD::save_structure>;

template void boost::archive::detail::instantiate_ptr_serialization<ROD::save_structure>(ROD::save_structure*, int, boost::archive::detail::adl_tag);
template ROD::save_structure const* boost::serialization::smart_cast<ROD::save_structure const*, DLV::save_model_op const*>(DLV::save_model_op const*);
template DLV::save_model_op const* boost::serialization::smart_cast<DLV::save_model_op const*, ROD::save_structure const*>(ROD::save_structure const*);

template class boost::serialization::extended_type_info_typeid<ROD::save_structure>;

template ROD::save_structure* boost::serialization::factory<ROD::save_structure, 0>(std::va_list);
template ROD::save_structure* boost::serialization::factory<ROD::save_structure, 1>(std::va_list);
template ROD::save_structure* boost::serialization::factory<ROD::save_structure, 2>(std::va_list);
template ROD::save_structure* boost::serialization::factory<ROD::save_structure, 3>(std::va_list);
template ROD::save_structure* boost::serialization::factory<ROD::save_structure, 4>(std::va_list);

template void ROD::save_structure::serialize<boost::archive::text_oarchive>(boost::archive::text_oarchive&, unsigned int);
template void ROD::save_structure::serialize<boost::archive::text_iarchive>(boost::archive::text_iarchive&, unsigned int);

template boost::serialization::detail::base_cast<DLV::save_model_op, ROD::save_structure>::type& boost::serialization::base_object<DLV::save_model_op, ROD::save_structure>(ROD::save_structure&);

#  endif // DLV_EXPLICT_TEMPLATES

#endif // DLV_USES_SERIALIZE
