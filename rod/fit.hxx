
#ifndef ROD_FIT
#define ROD_FIT

namespace ROD {

  class rod_fit : public ROD::rod_calc{
  public:
    rod_fit(DLV::model *m, const real_g att, const real_g sc, const real_g sc2,
	    const real_g be, const real_g sfr,
	    const bool fit_d, const bool exp_d,
	    const real_g sc_min, const real_g sc_max,  const bool sc_fit,
	    const real_g sc2_min, const real_g sc2_max,const bool sc2_fit,
	    const real_g be_min, const real_g be_max, const bool be_fit,
	    const real_g sfr_min, const real_g sfr_max, const bool sfr_fit,
	    real_g dis[], const real_g dis_min[], const real_g dis_max[],
	    const int_g dis_fit[], const int_g nditot,
	    real_g d1[], const real_g d1_min[], const real_g d1_max[],
	    const int_g d1_fit[], const int_g ndtot,
	    real_g d2[], const real_g d2_min[], const real_g d2_max[],
	    const int_g d2_fit[], const int_g ndtot2,
	    real_g occ[], const real_g occ_min[], const real_g occ_max[],
	    const int_g occ_fit[], const int_g notot,
	    real_g *chi, real_g *nor, real_g *qual);
    int_g initialise_data(char message[], const int_g mlen);
    DLV::operation *run_fit(const int_g fit_method, real_g fit_results[][2],
			    char message[], const int_g mlen,
			    const bool attach_op = true);
  protected:
     DLV::string get_name() const;

  private:
    real_g scale_min, scale_max;
    bool scale_fit;
    real_g scale2_min, scale2_max;
    bool scale2_fit;
    real_g beta_min, beta_max;
    bool beta_fit;
    real_g sfrac_min, sfrac_max;
    bool sfrac_fit;
    real_g *dist_min, *dist_max;
    int_g *dist_fit;
    real_g *dw1_min, *dw1_max;
    int_g *dw1_fit;
    real_g *dw2_min, *dw2_max;
    int_g *dw2_fit;
    real_g *occup_min, *occup_max;
    int_g *occup_fit;
    real_g *chisqr, *norm, *quality;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };
}
#endif // ROD_PLOT


