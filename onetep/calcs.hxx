
#ifndef ONETEP_CALCS
#define ONETEP_CALCS

namespace ONETEP {

  using DLV::int_g;
  using DLV::int_l;
  using DLV::real_g;
  using DLV::real_l;

  // Interface to ONETEP specific stuff
  class cell_file {
  protected:
    static DLV::model *read(const DLV::string name, const char filename[],
			    char message[], const int_g mlen);
    void write(std::ofstream &output, const DLV::model *const structure,
	       const int_g *species, char message[], const int_g mlen);

  private:
    static real_l get_units(const char buff[], char message[],
			    const int_g mlen);
  };

}

#endif // ONETEP_CALCS
