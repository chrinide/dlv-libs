
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/constants.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/op_admin.hxx"
#include "../dlv/op_model.hxx"
//#include "../dlv/calculation.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "../dlv/data_objs.hxx"
#include "../dlv/data_vol.hxx"
//#include "../dlv/job_setup.hxx"
#include "../dlv/op_data.hxx"
#include "calcs.hxx"
#include "grid3d.hxx"

DLV::operation *ONETEP::load_cube_file::create(const char filename[],
					       const bool load_model,
					       const bool set_bonds,
					       char message[], const int_g mlen)
{
  if (load_model)
    return load_cube_model::create(filename, set_bonds, message, mlen);
  else
    return load_cube_data::create(filename, message, mlen);
}

DLV::operation *ONETEP::load_cube_model::create(const char filename[],
						const bool set_bonds,
						char message[],
						const int_g mlen)
{
  DLV::string model_name = name_from_file("", filename);
  DLV::model *structure = DLV::model::create_atoms(model_name, 3);
  if (structure == 0)
    strncpy(message, "Create model failed", mlen - 1);
  else {
    if (read_model(structure, filename, message, mlen)) {
      load_cube_model *op = new load_cube_model(structure, filename);
      DLV::data_object *data = op->read_data(op, 0, filename,
					     filename, "", message, mlen);
      if (data == 0) {
	delete op;
	op = 0;
      } else {
	attach_base(op);
	op->attach_data(data);
	if (set_bonds)
	  op->set_bond_all();
      }
      return op;
    } else
      delete structure;
  }
  return 0;
}

DLV::operation *ONETEP::load_cube_data::create(const char filename[],
					       char message[], const int_g mlen)
{
  load_cube_data *op = new load_cube_data(filename);
  DLV::data_object *data = op->read_data(op, 0, filename,
					 filename, "", message, mlen);
  if (data == 0) {
    delete op;
    op = 0;
  } else {
    op->attach();
    op->attach_data(data);
  }
  return op;
}

DLV::string ONETEP::load_cube_data::get_name() const
{
  return ("Load ONETEP cube data - " + get_filename());
}

DLV::string ONETEP::load_cube_model::get_name() const
{
  return ("Load ONETEP cube model - " + get_filename());
}

DLV::string ONETEP::load_cube_file::get_label(std::ifstream &input,
					      const DLV::string tag,
					      const DLV::string id)
{
  char line[128];
  input.getline(line, 128);
  input.getline(line, 128);
  // process label
  DLV::string label = line;
  DLV::string::size_type p = label.find("for:");
  if (p == DLV::string::npos)
    p = label.find("\"");
  label = label.substr(0, p - 1);
  if (tag.length() > 0) {
    label += tag;
    label += ", ";
    label += id;
  }
  return label;
}

DLV::volume_data *
ONETEP::load_cube_file::create_data(const DLV::string label, DLV::operation *op,
				    const int_g nx, const int_g ny,
				    const int_g nz, const real_g origin[3],
				    const real_g astep[3],
				    const real_g bstep[3],
				    const real_g cstep[3])
{
  return new DLV::rspace_periodic_volume("ONETEP", label, op, nx, ny, nz,
					 origin, astep, bstep, cstep);
}

bool ONETEP::load_cube_model::reload_data(DLV::data_object *data,
					  char message[], const int_g mlen)
{
  DLV::volume_data *v = dynamic_cast<DLV::volume_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload cube data", mlen);
    return false;
  } else
    return read_data(this, v, get_filename().c_str(), "", "", message, mlen);
}

bool ONETEP::load_cube_data::reload_data(DLV::data_object *data,
					 char message[], const int_g mlen)
{
  DLV::volume_data *v = dynamic_cast<DLV::volume_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload cube data", mlen);
    return false;
  } else
    return read_data(this, v, get_filename().c_str(), "", "", message, mlen);
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, ONETEP::load_cube_data *t,
				    const unsigned int file_version)
    {
      ::new(t)ONETEP::load_cube_data("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, ONETEP::load_cube_model *t,
				    const unsigned int file_version)
    {
      ::new(t)ONETEP::load_cube_model(0, "recover");
    }

  }
}

template <class Archive>
void ONETEP::load_cube_model::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::load_atom_model_op>(*this);
}

template <class Archive>
void ONETEP::load_cube_data::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<DLV::load_data_op>(*this);
}

//BOOST_CLASS_EXPORT_IMPLEMENT(ONETEP::load_cube_file)
BOOST_CLASS_EXPORT_IMPLEMENT(ONETEP::load_cube_model)
BOOST_CLASS_EXPORT_IMPLEMENT(ONETEP::load_cube_data)

//DLV_SUPPRESS_TEMPLATES(DLV::cube_file)
DLV_SUPPRESS_TEMPLATES(DLV::load_atom_model_op)
DLV_SUPPRESS_TEMPLATES(DLV::load_data_op)

//DLV_NORMAL_EXPLICIT(ONETEP::load_cube_file)
DLV_NORMAL_EXPLICIT(ONETEP::load_cube_model)
DLV_NORMAL_EXPLICIT(ONETEP::load_cube_data)

#endif // DLV_USES_SERIALIZE
