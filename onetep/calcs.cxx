
#include "../dlv/types.hxx"
#include "../dlv/constants.hxx"
#include "../dlv/boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "../dlv/utils.hxx"
#include "../dlv/operation.hxx"
#include "../dlv/calculation.hxx"
#include "../dlv/atom_model.hxx"
#include "../dlv/model.hxx"
#include "calcs.hxx"

ONETEP::real_l ONETEP::cell_file::get_units(const char buff[], char message[],
					    const int_g mlen)
{
  if (strcmp(buff, "ang") == 0 || strcmp(buff, "ANG") == 0)
    return 1.0;
  else if (strcmp(buff, "bohr") == 0 || strcmp(buff, "BOHR") == 0)
    return DLV::bohr_to_angstrom;
  else {
    strncpy(message, "Units not recognised", mlen);
    return 0.0;
  }
}

DLV::model *ONETEP::cell_file::read(const DLV::string name,
				    const char filename[], char message[],
				    const int_g mlen)
{
  DLV::model *structure = 0;
  DLVreturn_type ok = DLV_OK;
  if (DLV::check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (DLV::open_file_read(input, filename, message, mlen)) {
      structure = DLV::model::create_atoms(name, 3);
      if (structure == 0) {
	strncpy(message, "Create model failed", mlen - 1);
	ok = DLV_ERROR;
      } else {
	int_g natoms = 0;
	char line[256], text[256];
	bool read_data = false;
	real_l scale = DLV::bohr_to_angstrom;
	input.getline(line, 256);
	while (!input.eof()) {
	  if (strncmp(line, "%block", 6) == 0 or
	      strncmp(line, "%BLOCK", 6) == 0) {
	    scale = DLV::bohr_to_angstrom;
	    sscanf(&line[6], "%s", text);
	    if (strcmp(text, "LATTICE_CART") == 0 or
		strcmp(text, "lattice_cart") == 0) {
	      read_data = true;
	      input.getline(line, 256);
	      sscanf(line, "%s", text);
	      if (isalpha(text[0])) {
		scale = get_units(text, message, mlen);
		if (scale == 0.0) {
		  ok = DLV_ERROR;
		  break;
		}
		input.getline(line, 256);
	      }
	      real_l a[3];
	      if (sscanf(line, "%lf %lf %lf", &a[0], &a[1], &a[2]) != 3) {
		strncpy(message, "Error reading a lattice vector", mlen - 1);
		ok = DLV_ERROR;
	      } else {
		a[0] *= scale;
		a[1] *= scale;
		a[2] *= scale;
		input.getline(line, 256);
		real_l b[3];
		if (sscanf(line, "%lf %lf %lf", &b[0], &b[1], &b[2]) != 3) {
		  strncpy(message, "Error reading b lattice vector", mlen - 1);
		  ok = DLV_ERROR;
		} else {
		  b[0] *= scale;
		  b[1] *= scale;
		  b[2] *= scale;
		  input.getline(line, 256);
		  real_l c[3];
		  if (sscanf(line, "%lf %lf %lf", &c[0], &c[1], &c[2]) != 3) {
		    strncpy(message, "Error reading c lattice vector",
			    mlen - 1);
		    ok = DLV_ERROR;
		  } else {
		    c[0] *= scale;
		    c[1] *= scale;
		    c[2] *= scale;
		    if (!structure->set_primitive_lattice(a, b, c)) {
		      strncpy(message, "Error setting lattice vectors",
			      mlen - 1);
		      ok = DLV_ERROR;
		    }
		  }
		}
	      }
	      // skip %endblock
	      input.getline(line, 256);
	    } else if (strcmp(text, "POSITIONS_ABS") == 0 or
		       strcmp(text, "positions_abs") == 0) {
	      scale = DLV::bohr_to_angstrom;
	      read_data = true;
	      input.getline(line, 256);
	      sscanf(line, "%s", text);
	      if (isalpha(text[0])) {
		if (DLV::atom_type::check_atomic_symbol(text) == 0) {
		  scale = get_units(text, message, mlen);
		  if (scale == 0.0) {
		    ok = DLV_ERROR;
		    break;
		  }
		  input.getline(line, 256);
		}
	      }
	      while (strncmp(line, "%endblock", 9) != 0 and
		     strncmp(line, "%ENDBLOCK", 9) != 0) {
		if (strlen(line) > 0) {
		  DLV::coord_type coords[3];
		  if (sscanf(line, "%s %lf %lf %lf", text, &coords[0],
			     &coords[1], &coords[2]) != 4) {
		    strncpy(message, "Error reading atoms", mlen - 1);
		    ok = DLV_ERROR;
		    break;
		  } else {
		    coords[0] *= scale;
		    coords[1] *= scale;
		    coords[2] *= scale;
		    DLV::atom my_atom;
		    if (isdigit(text[0])) {
		      int_g id = atoi(text);
		      my_atom = structure->add_cartesian_atom(id, coords);
		    } else
		      my_atom = structure->add_cartesian_atom(text, coords);
		    if (my_atom == 0) {
		      strncpy(message, "Error adding atom to model", mlen - 1);
		      ok = DLV_ERROR;
		      break;
		    } else
		      natoms++;
		  }
		}
		input.getline(line, 256);
	      }
	    } else {
	      // Something else, just skip it.
	      do {
		input.getline(line, 256);
	      } while (strncmp(line, "%endblock", 9) != 0 and
		       strncmp(line, "%ENDBLOCK", 9) != 0);
	    }
	  }
	  input.getline(line, 256);
	}
	if (natoms == 0) {
	  strncpy(message, "No atoms found in file", 256);
	  ok = DLV_ERROR;
	}
	if (ok != DLV_OK or (!read_data)) {
	  delete structure;
	  structure = 0;
	} else
	  structure->complete();
      }
      if (ok != DLV_OK)
	message[mlen - 1] = '\0';
      input.close();
    }
  }
  return structure;
}

void ONETEP::cell_file::write(std::ofstream &output,
			      const DLV::model *const structure,
			      const int_g *species, char message[],
			      const int_g mlen)
{
  DLV::coord_type a[3];
  DLV::coord_type b[3];
  DLV::coord_type c[3];
  structure->get_primitive_lattice(a, b, c);
  output << "%BLOCK LATTICE_CART" << '\n';
  output.width(18);
  output.precision(8);
  output << a[0] / DLV::bohr_to_angstrom;
  output.precision(8);
  output.width(18);
  output << a[1] / DLV::bohr_to_angstrom;
  output.precision(8);
  output.width(18);
  output << a[2] / DLV::bohr_to_angstrom;
  output << '\n';
  output.precision(8);
  output.width(18);
  output << b[0] / DLV::bohr_to_angstrom;
  output.precision(8);
  output.width(18);
  output << b[1] / DLV::bohr_to_angstrom;
  output.precision(8);
  output.width(18);
  output << b[2] / DLV::bohr_to_angstrom;
  output << '\n';
  output.precision(8);
  output.width(18);
  output << c[0] / DLV::bohr_to_angstrom;
  output.precision(8);
  output.width(18);
  output << c[1] / DLV::bohr_to_angstrom;
  output.precision(8);
  output.width(18);
  output << c[2] / DLV::bohr_to_angstrom;
  output << '\n';
  output << "%ENDBLOCK LATTICE_CART" << '\n';
  int_g natoms = structure->get_number_of_primitive_atoms();
  int_g *atom_types = new_local_array1(int_g, natoms);
  DLV::coord_type (*coords)[3] = new_local_array2(DLV::coord_type,natoms,3);
  structure->get_primitive_atom_types(atom_types, natoms);
  structure->get_primitive_atom_cart_coords(coords, natoms);
  int_g *asym_indices = 0;
  if (species != 0) {
    asym_indices = new_local_array1(int_g, natoms);
    structure->get_primitive_asym_indices(asym_indices, natoms);
  }
  output << "%BLOCK POSITIONS_ABS" << '\n';
  for (int_g i = 0; i < natoms; i++) {
    output << DLV::atom_type::get_atomic_symbol(atom_types[i]);
    if (species != 0)
      output << species[asym_indices[i]];
    output << ' ';
    output.precision(8);
    output.width(18);
    output << coords[i][0] / DLV::bohr_to_angstrom;
    output.precision(8);
    output.width(18);
    output << coords[i][1] / DLV::bohr_to_angstrom;
    output.precision(8);
    output.width(18);
    output << coords[i][2] / DLV::bohr_to_angstrom;
    output << '\n';
  }
  output << "%ENDBLOCK POSITIONS_ABS" << '\n';
  if (asym_indices != 0)
    delete_local_array(asym_indices);
  delete_local_array(coords);
  delete_local_array(atom_types);
}
