
#ifndef ONETEP_DENSITY_OF_STATES
#define ONETEP_DENSITY_OF_STATES

namespace ONETEP {

  class dos_file {
  protected:
    class DLV::electron_dos *read_data(DLV::operation *op,
				       DLV::electron_dos *plot,
				       const char filename[],
				       const DLV::string id,
				       const int_g nproj, const bool next,
				       char message[], const int_g mlen);
  };

  class load_dos_data : public DLV::load_data_op, public dos_file {
  public:
    static DLV::operation *create(const char filename[],
				  char message[], const int_g mlen);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

    load_dos_data(const char file[]);

  protected:
    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(ONETEP::load_dos_data)
#endif // DLV_USES_SERIALIZE

inline ONETEP::load_dos_data::load_dos_data(const char file[])
  : load_data_op(file)
{
}

#endif // ONETEP_DENSITY_OF_STATES
