
#ifndef ONETEP_CUBE_DATA
#define ONETEP_CUBE_DATA

namespace ONETEP {

  class load_cube_file : public DLV::cube_file {
  public:
    static DLV::operation *create(const char filename[], const bool load_model,
				  const bool set_bonds, char message[],
				  const int_g mlen);

  protected:
    DLV::string get_label(std::ifstream &input, const DLV::string tag,
			  const DLV::string id);
    DLV::volume_data *create_data(const DLV::string label, DLV::operation *op,
				  const int_g nx, const int_g ny,
				  const int_g nz, const real_g origin[3],
				  const real_g astep[3], const real_g bstep[3],
				  const real_g cstep[3]);
    DLV::model *create_model(const DLV::string name);

  };

  class load_cube_model : public DLV::load_atom_model_op,
			  public load_cube_file {
  public:
    static DLV::operation *create(const char filename[], const bool set_bonds,
				  char message[], const int_g mlen);
    // public for serialization
    load_cube_model(DLV::model *m, const char file[]);
    
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_cube_data : public DLV::load_data_op, public load_cube_file {
  public:
    static DLV::operation *create(const char filename[],
				  char message[], const int_g mlen);
    // public for serialization
    load_cube_data(const char file[]);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(ONETEP::load_cube_file)
BOOST_CLASS_EXPORT_KEY(ONETEP::load_cube_model)
BOOST_CLASS_EXPORT_KEY(ONETEP::load_cube_data)
#endif // DLV_USES_SERIALIZE

inline ONETEP::load_cube_model::load_cube_model(DLV::model *m,
						const char file[])
  : load_atom_model_op(m, file)
{
}

inline ONETEP::load_cube_data::load_cube_data(const char file[])
  : load_data_op(file)
{
}

#endif // ONETEP_CUBE_DATA
