
#ifndef ONETEP_SCF_CALCS
#define ONETEP_SCF_CALCS

namespace ONETEP {

  class Tolerances {
  public:
    Tolerances();
    Tolerances(const real_g c, const real_g g, const real_g k);

    real_g energy_cutoff;
    real_g kernel_cutoff;
    real_g fine_grid;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class Hamiltonian {
  public:
    Hamiltonian();
    Hamiltonian(const bool u, const int f, const int_g d, const bool s,
	       const int_g sp);

    bool unrestricted;
    enum dft_func_type { dft_capz, dft_vwn, dft_pw91, dft_pbe, dft_revpbe,
			 dft_rpbe, dft_blyp, dft_xlyp } functional;
    int_g dispersion;
    bool set_spin;
    int_g spin;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class Properties {
  public:
    Properties();
    Properties(const int_g homo, const int_g lumo, const int_g eigs,
	       const bool dos, const real_g smear, const bool dp,
	       const bool pop, const real_g rad);

    int_g num_homo_plots;
    int_g num_lumo_plots;
    int_g num_eigenvalues;
    bool dos_calc;
    real_g dos_smearing;
    bool calc_density;
    bool mulliken_calc;
    real_g mulliken_radius;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class Optimise {
  public:
    Optimise();
    Optimise(const int_g m, const int_g iter, const real_g energy,
	     const real_g forces, const real_g disp);

    int_g method;
    int_g max_iterations;
    real_g energy_tol;
    real_g force_tol;
    real_g displacement_tol;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class scf_base : public DLV::batch_calc {
  public:
    static operation *create(const int_g t,
			     char message[], const int_g mlen);
    static bool add_pseudo(const int_g ngwfs, const real_g radius,
			   const char filename[], const bool set_default,
			   bool &complete, char message[],
			   const int_g mlen);
    static bool calculate(const Tolerances &tol, const Hamiltonian &h,
			  const Properties &props, const Optimise &opt,
			  const DLV::job_setup_data &job,
			  const bool extern_job, const char extern_dir[],
			  char message[], const int_g mlen);

  protected:
    scf_base();
    virtual DLV::string get_base_name() const = 0;
    virtual void set_params(const Tolerances &tol, const Hamiltonian &h,
			    const Properties &p, const Optimise &opt) = 0;
    virtual bool find_restart(char message[], const int_g mlen) = 0;
    virtual bool create_files(const bool is_parallel, const bool is_local,
			      char message[], const int_g mlen) = 0;
    bool common_files(const bool is_local, const char tag[]);
    virtual bool add_restart(const bool is_local) = 0;
    void add_properties(const Properties &props, const bool is_local,
			const char tag[], const bool spin, const int_g n);
    virtual bool add_output(const char tag[], const bool is_local,
			    char message[], const int_g mlen) = 0;
    virtual bool add_pseudo_pot(const int_g nfns, const real_g radius,
				const char filename[], const bool set_default,
				int_g * &indices, int_g &value,
				char message[], const int_g mlen) = 0;
    virtual bool check_pseudos() const = 0;
    virtual bool write(const DLV::string filename,
		       const DLV::model *const structure,
		       char message[], const int_g mlen) = 0;

    DLV::string get_path() const;
    bool no_errors(char message[], const int_g len);

    DLV::string get_serial_executable() const;
    DLV::string get_parallel_executable() const;

  private:
    static DLV::string serial_binary;
    static DLV::string parallel_binary;
    bool show_basis;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_SERIALIZATION_ASSUME_ABSTRACT(ONETEP::scf_base)
BOOST_CLASS_EXPORT_KEY(ONETEP::scf_base)
#endif // DLV_USES_SERIALIZE

inline ONETEP::scf_base::scf_base() : show_basis(false)
{
}

inline ONETEP::Tolerances::Tolerances()
  : energy_cutoff(0.0), kernel_cutoff(0.0), fine_grid(0.0)
{
}

inline ONETEP::Tolerances::Tolerances(const real_g c, const real_g g,
				      const real_g k)
  : energy_cutoff(c), kernel_cutoff(k), fine_grid(g)
{
}

inline ONETEP::Hamiltonian::Hamiltonian()
  : unrestricted(false), functional(dft_capz), dispersion(0),
    set_spin(false), spin(0)
{
}

inline ONETEP::Properties::Properties()
  : num_homo_plots(0), num_lumo_plots(0), num_eigenvalues(0),
    dos_calc(false), dos_smearing(0.0), calc_density(false),
    mulliken_calc(false), mulliken_radius(0.0)
{
}

inline ONETEP::Properties::Properties(const int_g homo, const int_g lumo,
				      const int_g eigs, const bool dos,
				      const real_g smear, const bool dp,
				      const bool pop, const real_g rad)
  : num_homo_plots(homo), num_lumo_plots(lumo), num_eigenvalues(eigs),
    dos_calc(dos), dos_smearing(smear), calc_density(dp),
    mulliken_calc(pop), mulliken_radius(rad)
{
}

inline ONETEP::Optimise::Optimise()
  : method(0), max_iterations(10), energy_tol(1e-6), force_tol(0.02),
    displacement_tol(1e-3)
{
}

inline ONETEP::Optimise::Optimise(const int_g m, const int_g iter,
				  const real_g energy, const real_g forces,
				  const real_g disp)
  : method(m), max_iterations(iter), energy_tol(energy), force_tol(forces),
    displacement_tol(disp)
{
}

#endif // ONETEP_SCF_CALCS
