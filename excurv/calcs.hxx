
#ifndef EXCURV_CALCS
#define EXCURV_CALCS

#include <list>

namespace EXCURV {

  using DLV::int_g;
  using DLV::nat_g;
  using DLV::real_g;

  class shell_data {
  public:
    shell_data();
    shell_data(const int_g type, const real_g n, const bool ref_n,
	       const real_g r, const bool ref_r, const real_g dw,
	       const bool ref_dw, const real_g th, const real_g ph);

    int_g atom_type;
    real_g number_of_atoms;
    bool refine_number;
    real_g radius;
    bool refine_radius;
    real_g debye_waller;
    bool refine_dw;
    real_g theta;
    real_g phi;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class refine_data {
  public:
    refine_data();
    refine_data(const int_g ns, const int_g step, const int_g iter,
		const real_g dw, const real_g ef, const bool cdw,
		const bool ref_user, const bool all_num, const bool all_r,
		const bool all_dw, const bool efermi,
		const bool correlate, const bool ms, const int_g atmax,
		const real_g plmax, const real_g plmin, const real_g minang,
		const real_g minmag, const int_g dlmax, const int_g tlmax,
		const int_g numax,const int_g omin, const int_g omax,
		const real_g output, const int_g theor, const char sym[]);

    void add_shell(const int_g type, const real_g n, const bool ref_n,
		   const real_g r, const bool ref_r, const real_g dw,
		   const bool ref_dw, const real_g theta, const real_g phi);

    int_g number_of_shells;
    int_g step_size;
    int_g number_of_iterations;
    real_g central_dw;
    real_g fermi_energy;
    bool refine_cdw;
    bool user_defined_ref;
    bool refine_all_atom_num;
    bool refine_all_radii;
    bool refine_all_dw;
    bool refine_efermi;
    bool correlations;
    bool multiple_scat;
    int_g max_atom_path;
    real_g max_path_length;
    real_g real_g_plmin;
    real_g real_g_minang;
    real_g real_g_minmag;
    int_g int_dlmax;
    int_g int_tlmax;
    int_g int_numax;
    int_g int_omin;
    int_g int_omax;
    real_g real_g_output;
    int_g theory;
    DLV::string symmetry;
    std::list<shell_data> shells;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class calculation : public DLV::socket_calc {
  public:
    DLVreturn_type open(char message[], const int_g mlen);
    static DLVreturn_type close();

    bool process_socket_data(char data[], const bool suppress);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);
    static class DLV::text_buffer *get_viewer();

  protected:
    calculation();
    calculation(DLV::model *m);
    bool create_files(char message[], const int_g mlen);
    void recover();
    static bool is_connected();

  private:
    static DLV::string binary_name;
    static DLV::string def_name;
    static DLV::string executable;
    static DLV::string def_file;
    static calculation *base_calc;
    static class DLV::text_buffer *viewer;

    static DLV::string get_path();

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class read_experimental_data : public calculation {
  public:
    static DLV::operation *create(const char filename[], const int_g atom,
				  const char edge[], const int_g freq,
				  const int_g xcol, const int_g ycol,
				  const int_g kweight, bool &started,
				  const DLV::job_setup_data &j,
				  char message[], const int_g mlen);
    bool process_socket_data(char data[], const bool suppress);

    // public for serialization
    read_experimental_data(const char filename[], const int_g atom,
			   const char edge[], const int_g freq,
			   const int_g xcol, const int_g ycol,
			   const int_g kweight);

  protected:
    DLV::string get_name() const;

  private:
    DLV::string datafile;
    int_g atom_type;
    DLV::string xray_edge;
    int_g frequency;
    int_g x_index;
    int_g y_index;
    int_g k_weighting;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class potential_and_phase : public calculation {
  public:
    static DLV::operation *create(const int_g central_atom,
				  const int_g central_neighb,
				  const int_g natoms, const int_g atoms[],
				  const int_g neighbours[],
				  const bool constant_V0,
				  const int_g calc_method,
				  char message[], const int_g mlen);
    bool process_socket_data(char data[], const bool suppress);

    // public for serialization
    potential_and_phase();

  protected:
    DLV::string get_name() const;

  private:
    int_g centre_atom;
    int_g centre_neighbour;
    int_g number_shell_atoms;
    int_g *shell_atoms;
    int_g *shell_neighbours;
    bool fix_v0;
    int_g method;

    potential_and_phase(const int_g central_atom, const int_g central_neighb,
			const int_g natoms, const int_g atoms[],
			const int_g neighbours[], const bool constant_V0,
			const int_g calc_method);

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class plot : public calculation {
  public:
    static DLV::operation *create(const int_g x_value,  const int_g y[],
				  const int_g ylen, char message[],
				  const int_g mlen);
    bool will_process_socket() const;
    bool process_socket_data(char data[], const bool suppress);

    // public for serialization
    plot(const int_g x_value);

  protected:
    DLV::string get_name() const;
    void send(const int_g x_value,  const int_g y[], const int_g ylen,
	      const char filename[], const bool print,
	      char message[], const int_g mlen);

  private:
    int_g value;
    int_g columns;
    int_g nlines;
    int_g clines;
    int_g npoints;
    int_g point;
    DLV::string title;
    DLV::string xaxis;
    DLV::string yaxis;
    real_g *x;
    real_g *y[16];
    DLV::string labels[16];
    bool got_labels[16];
    bool got_x;
    bool got_y;
    bool got_title;
    bool got_lines;
    bool complete;

    bool create_line(char *line, const DLV::string buff, int_g &bufptr,
		     bool &cont);

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class print : public plot {
  public:
    static DLV::operation *create(const int_g x_value, const int_g y[],
				  const int_g ylen, const char filename[],
				  char message[], const int_g mlen);
    bool will_process_socket() const;

    // public for serialization
    print(const int_g x_value, const char filename[]);

  protected:
    DLV::string get_name() const;

  private:
    DLV::string outfile;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class refinement_data : public calculation {
  public:
    bool process_socket_data(char data[], const bool suppress);

  protected:
    refinement_data(const refine_data &rdata, const bool update_shells);
    DLVreturn_type update();

  private:
    refine_data data;
    bool shells;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class update_refine_params : public refinement_data {
  public:
    static DLV::operation *create(const refine_data &data,
				  const bool update_shells,
				  char message[], const int_g mlen);

    // public for serialization
    update_refine_params(const refine_data &rdata, const bool update_shells);

  protected:
    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class resume_refinement : public calculation {
  public:
    static DLV::operation *create(const refine_data &data,
				  char message[], const int_g mlen);

    // public for serialization
    resume_refinement(const int_g step, const int_g iter, const bool correlate);

  protected:
    DLV::string get_name() const;

  private:
    int_g step_size;
    int_g niterations;
    bool correlations;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class refine_structure : public refinement_data {
  public:
    static DLV::operation *create(const refine_data &data,
				  char message[], const int_g mlen);

    // public for serialization
    refine_structure(const refine_data &rdata);

  protected:
    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class list_parameters : public calculation {
  public:
    static DLV::operation *create(const int_g nshells,
				  char message[], const int_g mlen);
    bool process_socket_data(char data[], const bool suppress);

    // public for serialization
    list_parameters(const int_g nshells);

  protected:
    DLV::string get_name() const;

  private:
    int_g number_of_shells;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class set_kweight : public calculation {
  public:
    static DLV::operation *create(const int_g k,
				  char message[], const int_g mlen);

    // public for serialization
    set_kweight(const int_g value);

  protected:
    DLV::string get_name() const;

  private:
    int_g k;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class set_emin : public calculation {
  public:
    static DLV::operation *create(const real_g value,
				  char message[], const int_g mlen);

    // public for serialization
    set_emin(const real_g value);

  protected:
    DLV::string get_name() const;

  private:
    real_g energy;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class set_emax : public calculation {
  public:
    static DLV::operation *create(const real_g value,
				  char message[], const int_g mlen);

    // public for serialization
    set_emax(const real_g value);

  protected:
    DLV::string get_name() const;

  private:
    real_g energy;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class set_kmin : public calculation {
  public:
    static DLV::operation *create(const real_g value,
				  char message[], const int_g mlen);

    // public for serialization
    set_kmin(const real_g value);

  protected:
    DLV::string get_name() const;

  private:
    real_g k;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class set_kmax : public calculation {
  public:
    static DLV::operation *create(const real_g value,
				  char message[], const int_g mlen);

    // public for serialization
    set_kmax(const real_g value);

  protected:
    DLV::string get_name() const;

  private:
    real_g k;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class multiple_scattering : public calculation {
  public:
    static DLV::operation *create(const int_g ms,
				  char message[], const int_g mlen);

    //public for serialization
    multiple_scattering(const bool ms);

  protected:
    DLV::string get_name() const;

  private:
    bool multiple_scat;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class save_data : public calculation {
  public:
    static DLV::operation *create(const char name[],
				  char message[], const int_g mlen);

    // public for serialization
    save_data(const char name[]);

  protected:
    DLV::string get_name() const;

  private:
    DLV::string filename;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class model : public DLV::create_model_op {
  public:
    static DLV::operation *create(const int_g atomic_number,
				  char message[], const int_g mlen);
    static DLV::operation *create(const int_g atomic_number,
				  const int_g types[], const real_g numbers[],
				  const real_g radii[], const int_g nshells,
				  char message[], const int_g mlen);

    //public for serialization
    model(DLV::model *m);

  protected:
    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class calc_geom : public calculation {
  public:
    bool is_geometry() const;
    bool is_geom_edit() const;

  protected:
    calc_geom(DLV::model *m);
    void inherit_model();
    void inherit_data();
    void map_data();

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class convert_model : public calc_geom {
  public:
    static DLV::operation *create(const int_g atomic_number,
				  const int_g types[], const real_g numbers[],
				  const real_g radii[], const int_g nshells,
				  char message[], const int_g mlen);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

    // public for serialization
    convert_model(DLV::model *m);

  protected:
    DLV::string get_name() const;
    void add_standard_data_objects();

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class update_model : public calc_geom {
  public:
    static DLV::operation *update(const int_g types[], const real_g numbers[],
				  const real_g radii[], const int_g nshells,
				  bool &changed, char message[],
				  const int_g mlen);
    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

    // public for serialization
    update_model(DLV::model *m);

  protected:
    DLV::string get_name() const;

  private:
    void add_standard_data_objects();

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(EXCURV::calculation)
BOOST_CLASS_EXPORT_KEY(EXCURV::read_experimental_data)
BOOST_CLASS_EXPORT_KEY(EXCURV::potential_and_phase)
BOOST_CLASS_EXPORT_KEY(EXCURV::plot)
BOOST_CLASS_EXPORT_KEY(EXCURV::print)
BOOST_SERIALIZATION_ASSUME_ABSTRACT(EXCURV::refinement_data)
BOOST_CLASS_EXPORT_KEY(EXCURV::refinement_data)
BOOST_CLASS_EXPORT_KEY(EXCURV::update_refine_params)
BOOST_CLASS_EXPORT_KEY(EXCURV::resume_refinement)
BOOST_CLASS_EXPORT_KEY(EXCURV::refine_structure)
BOOST_CLASS_EXPORT_KEY(EXCURV::list_parameters)
BOOST_CLASS_EXPORT_KEY(EXCURV::set_kweight)
BOOST_CLASS_EXPORT_KEY(EXCURV::set_emin)
BOOST_CLASS_EXPORT_KEY(EXCURV::set_emax)
BOOST_CLASS_EXPORT_KEY(EXCURV::set_kmin)
BOOST_CLASS_EXPORT_KEY(EXCURV::set_kmax)
BOOST_CLASS_EXPORT_KEY(EXCURV::multiple_scattering)
BOOST_CLASS_EXPORT_KEY(EXCURV::save_data)
BOOST_CLASS_EXPORT_KEY(EXCURV::model)
BOOST_CLASS_EXPORT_KEY(EXCURV::calc_geom)
BOOST_CLASS_EXPORT_KEY(EXCURV::convert_model)
BOOST_CLASS_EXPORT_KEY(EXCURV::update_model)
#endif // DLV_USES_SERIALIZE

inline EXCURV::shell_data::shell_data()
{
}

inline EXCURV::shell_data::shell_data(const int_g type, const real_g n,
				      const bool ref_n, const real_g r,
				      const bool ref_r, const real_g dw,
				      const bool ref_dw, const real_g th,
				      const real_g ph)
  : atom_type(type), number_of_atoms(n), refine_number(ref_n), radius(r),
    refine_radius(ref_r), debye_waller(dw), refine_dw(ref_dw), theta(th),
    phi(ph)
{
}

inline EXCURV::refine_data::refine_data()
{
}

inline EXCURV::refine_data::refine_data(const int_g ns, const int_g step,
					const int_g iter, const real_g dw,
					const real_g ef, const bool cdw,
					const bool ref_user,
					const bool all_num, const bool all_r,
					const bool all_dw, const bool efermi,
					const bool correlate, const bool ms,
					const int_g atmax, const real_g plmax,
					const real_g plmin, const real_g minang,
					const real_g minmag, const int_g dlmax,
					const int_g tlmax, const int_g numax,
					const int_g omin, const int_g omax,
					const real_g output, const int_g theor,
					const char sym[])
  : number_of_shells(ns), step_size(step), number_of_iterations(iter),
    central_dw(dw), fermi_energy(ef), refine_cdw(cdw),
    user_defined_ref(ref_user), refine_all_atom_num(all_num),
    refine_all_radii(all_r), refine_all_dw(all_dw), refine_efermi(efermi),
    correlations(correlate), multiple_scat(ms), max_atom_path(atmax),
    max_path_length(plmax), real_g_plmin(plmin),real_g_minang(minang),
    real_g_minmag(minmag), int_dlmax(dlmax), int_tlmax(tlmax),
    int_numax(numax), int_omin(omin), int_omax(omax), real_g_output(output),
    theory(theor), symmetry(sym)
{
}

inline void EXCURV::refine_data::add_shell(const int_g type,
					   const real_g n, const bool ref_n,
					   const real_g r, const bool ref_r,
					   const real_g dw, const bool ref_dw,
					   const real_g theta, const real_g phi)
{
  shells.push_back(shell_data(type, n, ref_n, r, ref_r, dw, ref_dw,
			      theta, phi));
}

inline bool EXCURV::calculation::is_connected()
{
  return (base_calc != 0);
}

inline EXCURV::calculation::calculation()
  : DLV::socket_calc()
{
}

inline EXCURV::calculation::calculation(DLV::model *m)
  : DLV::socket_calc(m)
{
}

inline
EXCURV::read_experimental_data::read_experimental_data(const char filename[],
						       const int_g atom,
						       const char edge[],
						       const int_g freq,
						       const int_g xcol,
						       const int_g ycol,
						       const int_g kweight)
  : datafile(filename), atom_type(atom), xray_edge(edge),
    frequency(freq), x_index(xcol), y_index(ycol), k_weighting(kweight)
{
}

inline EXCURV::potential_and_phase::potential_and_phase()
  : centre_atom(0), centre_neighbour(0),
    number_shell_atoms(0), shell_atoms(0), shell_neighbours(0),
    fix_v0(false), method(0)
{
}

inline
EXCURV::potential_and_phase::potential_and_phase(const int_g central_atom,
						 const int_g central_neighb,
						 const int_g natoms,
						 const int_g atoms[],
						 const int_g neighbours[],
						 const bool constant_V0,
						 const int_g calc_method)
  : centre_atom(central_atom), centre_neighbour(central_neighb),
    number_shell_atoms(natoms), shell_atoms(0), shell_neighbours(0),
    fix_v0(constant_V0), method(calc_method)
{
  shell_atoms = new int_g[natoms];
  shell_neighbours = new int_g[natoms];
  for (int_g i = 0; i < natoms; i++) {
    shell_atoms[i] = atoms[i];
    shell_neighbours[i] = neighbours[i];
  }
}

inline EXCURV::plot::plot(const int_g x_value)
  : value(x_value), nlines(0), clines(0), npoints(0), point(0),
    x(0), got_x(false), got_y(false), got_title(false), got_lines(false),
    complete(false)
{
  for (int_g i = 0; i < 16; i++)
    got_labels[i] = false;
}

inline EXCURV::print::print(const int_g x_value, const char filename[])
  : plot(x_value), outfile(filename)
{
}

inline EXCURV::refinement_data::refinement_data(const refine_data &rdata,
						const bool update_shells)
  : data(rdata), shells(update_shells)
{
}

inline
EXCURV::update_refine_params::update_refine_params(const refine_data &rdata,
						   const bool update_shells)
  : refinement_data(rdata, update_shells)
{
}

inline EXCURV::resume_refinement::resume_refinement(const int_g step,
						    const int_g iter,
						    const bool correlate)
  : step_size(step), niterations(iter), correlations(correlate)
{
}

inline EXCURV::refine_structure::refine_structure(const refine_data &rdata)
  : refinement_data(rdata, false)
{
}

inline EXCURV::list_parameters::list_parameters(const int_g nshells)
  : number_of_shells(nshells)
{
}

inline EXCURV::set_kweight::set_kweight(const int_g value)
  : k(value)
{
}

inline EXCURV::set_emin::set_emin(const real_g value)
  : energy(value)
{
}

inline EXCURV::set_emax::set_emax(const real_g value)
  : energy(value)
{
}

inline EXCURV::set_kmin::set_kmin(const real_g value)
  : k(value)
{
}

inline EXCURV::set_kmax::set_kmax(const real_g value)
  : k(value)
{
}

inline EXCURV::multiple_scattering::multiple_scattering(const bool ms)
  : multiple_scat(ms)
{
}

inline EXCURV::save_data::save_data(const char name[])
  : filename(name)
{
}

inline EXCURV::model::model(DLV::model *m)
  : create_model_op(m)
{
}

inline EXCURV::calc_geom::calc_geom(DLV::model *m)
  : calculation(m)
{
}

inline EXCURV::convert_model::convert_model(DLV::model *m)
  : calc_geom(m)
{
}

inline EXCURV::update_model::update_model(DLV::model *m)
  : calc_geom(m)
{
}

#endif // EXCURV_CALCS
