
#ifndef CHEMSHELL_SAVE_STRUCTURE
#define CHEMSHELL_SAVE_STRUCTURE

namespace CHEMSHELL {

  class save_structure : public DLV::save_model_op, public pun_file {
  public:
    static operation *create(const char filename[], const char group[],
			     char message[], const int_g mlen);

    // for serialization
    save_structure(const char file[]);

  protected:

    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(CHEMSHELL::save_structure)
#endif // DLV_USES_SERIALIZE

inline CHEMSHELL::save_structure::save_structure(const char file[])
  : save_model_op(file)
{
}

#endif // CHEMSHELL_SAVE_STRUCTURE
