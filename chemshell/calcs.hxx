
#ifndef CHEMSHELL_CALCS
#define CHEMSHELL_CALCS

namespace CHEMSHELL {

  using DLV::int_g;
  using DLV::real_g;
  using DLV::real_l;

  // Interface to CHEMSHELL specific stuff
  class pun_file {
  protected:
    static DLV::model *read(const DLV::string name, const char filename[],
			    char message[], const int_g mlen);
    void write(const char filename[], const char group[],
	       const DLV::model *const structure,
	       char message[], const int_g mlen);
  };

}

#endif // CHEMSHELL_CALCS
