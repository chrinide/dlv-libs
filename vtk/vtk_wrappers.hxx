
#ifndef CCP3_VTK_WRAPPERS
#define CCP3_VTK_WRAPPERS

// forward declare
class DLVview;
class vtkActor;

namespace CCP3 {

  void attach_display_structure(render_atoms *data, const bool kspace = false);
  void attach_to_view(DLVview *view, vtkRenderer *renderer, const char name[]);

}

#endif // CCP3_VTK_WRAPPERS
