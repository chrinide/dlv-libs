
#ifdef ENABLE_DLV_GRAPHICS
#  if defined(DLV_USES_AVS_GRAPHICS)
#    error "This graphics interface should not be built when using the AVS/Express environment"
#  elif !defined(DLV_USES_VTK_GRAPHICS)
#    error "DLV_USES_VTK_GRAPHICS should also be defined when building this graphics interface"
#  endif // Check for AVS/VTK
#else
#  error "ENABLE DLV_GRAPHICS and DLV_USES_VTK_GRAPHICS should be defined when building this graphics interface"
#endif // GRAPHICS check

#include <cmath>
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#include "../graphics/toolkit_obj.hxx"
#include "../graphics/render_base.hxx"

#include <vtkGlyph3DMapper.h>
#include <vtkTrivialProducer.h>
#include <vtkPointData.h>
#include <vtkLODActor.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkSphereSource.h>
#include <vtkNamedColors.h>
#include <vtkFloatArray.h>
#include <vtkUnsignedCharArray.h>
#include <vtkDataObject.h>
#include <vtkRenderer.h>

#include "vtk_wrappers.hxx"
#include "render_base.hxx"

#ifdef WIN32
#  define snprintf _snprintf
#endif // WIN32

vtkSphereSource *CCP3::render_atoms::sphere = nullptr;

template <> DLV::toolkit_obj *DLV::render_parent::parent = nullptr;
template <> DLV::render_parent *DLV::render_parent::serialize_obj = nullptr;

CCP3::render_base::render_base() : rview(nullptr), kview(nullptr), kspace(nullptr)
{
  background[0] = 1.0;
  background[1] = 1.0;
  background[2] = 1.0;
  // Need to work out what to do for kspace - bool arg to choose which to create?
  vtkNew<vtkNamedColors> colors;
  rspace = vtkRenderer::New();
  rspace->SetBackground(colors->GetColor3d("White").GetData());
}
    
template <class scene_t, class parent_t>
DLV::render_parent_templ<scene_t, parent_t>::render_parent_templ()
{
}

template <class scene_t, class parent_t>
DLV::render_parent_templ<scene_t, parent_t>::~render_parent_templ()
{
}

template <class scene_t, class parent_t, class atoms_t>
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::~render_atoms_templ()
{
  delete obj;
}

template <class scene_t, class parent_t, class outline_t>
DLV::render_outline_templ<scene_t, parent_t, outline_t>::~render_outline_templ()
{
  delete obj;
}

template <class scene_t, class parent_t, class shells_t>
DLV::render_shells_templ<scene_t, parent_t, shells_t>::~render_shells_templ()
{
  delete obj;
}

template <class scene_t, class parent_t, class atoms_t>
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::render_atoms_templ(const char name[])
  : obj(new atoms_t)
{
}

template <class scene_t, class parent_t, class outline_t>
DLV::render_outline_templ<scene_t, parent_t, outline_t>::render_outline_templ(const char name[])
  : obj(new outline_t)
{
}

template <class scene_t, class parent_t, class shells_t>
DLV::render_shells_templ<scene_t, parent_t, shells_t>::render_shells_templ(const char name[])
  : obj(new shells_t)
{
}

template <class scene_t, class parent_t>
const char *DLV::render_parent_templ<scene_t, parent_t>::get_name() const
{
  return "";
}

template <class scene_t, class parent_t>
DLV::toolkit_obj *DLV::render_parent_templ<scene_t, parent_t>::get_parent() const
{
  return parent;
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::get_background(float c[3]) const
{
  c[0] = get_object()->background[0];
  c[1] = get_object()->background[1];
  c[2] = get_object()->background[2];
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::get_inverted_background(float c[3]) const
{
  c[0] = get_object()->background[0];
  c[1] = get_object()->background[1];
  c[2] = get_object()->background[2];
  const float tol = 0.02;
  if (std::abs(c[0] - c[1]) < tol and std::abs(c[0] - c[2]) < tol) {
    // probably a grey
    if (c[0] > 0.5 or c[1] > 0.5 or c[2] > 0.5) {
      // light background so use black
      c[0] = 0.0;
      c[1] = 0.0;
      c[2] = 0.0;
    } else {
      c[0] = 1.0;
      c[1] = 1.0;
      c[2] = 1.0;
    }
  } else {
    //coloured background so invert
    c[0] = 1.0 - c[0];
    c[1] = 1.0 - c[1];
    c[2] = 1.0 - c[2];
  }
}

template <class scene_t, class parent_t, class atoms_t>
atoms_t *DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::get_atom_obj() const
{
  return obj;
}

template <class scene_t, class parent_t, class atoms_t>
parent_t *DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::get_object() const
{
  return obj;
}

template <class scene_t, class parent_t, class outline_t>
parent_t *DLV::render_outline_templ<scene_t, parent_t, outline_t>::get_object() const
{
  return obj;
}

template <class scene_t, class parent_t, class shells_t>
parent_t *DLV::render_shells_templ<scene_t, parent_t, shells_t>::get_object() const
{
  return obj;
}

template <class scene_t, class parent_t> DLVreturn_type
DLV::render_parent_templ<scene_t, parent_t>::attach_k3D(scene_t *view)
{
  // BUG
  return DLV_ERROR;
}

template <class scene_t, class parent_t, class atoms_t> DLVreturn_type
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::attach_r3D(scene_t *view)
{
  obj->set_r_view(view);
  CCP3::attach_to_view(view, obj->get_r_render(), "model name");
  return DLV_OK;
}

template <class scene_t, class parent_t, class atoms_t> DLVreturn_type
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::attach_k3D(scene_t *view)
{
  obj->set_k_view(view);
  return DLV_OK;
}

template <class scene_t, class parent_t> DLVreturn_type
DLV::render_parent_templ<scene_t, parent_t>::detach_k3D(scene_t *view)
{
  // BUG
  return DLV_ERROR;
}

template <class scene_t, class parent_t, class atoms_t> DLVreturn_type
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::detach_r3D(scene_t *view)
{
  obj->set_r_view(nullptr);
  std::cerr << "detach renderer\n";
  return DLV_OK;
}

template <class scene_t, class parent_t, class atoms_t> DLVreturn_type
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::detach_k3D(scene_t *view)
{
  obj->set_k_view(nullptr);
  return DLV_OK;
}

// Todo - some of this is common to render_atoms
template <class scene_t, class parent_t, class outline_t> DLVreturn_type
DLV::render_outline_templ<scene_t, parent_t, outline_t>::attach_r3D(scene_t *view)
{
  obj->set_r_view(view);
  return DLV_OK;
}

template <class scene_t, class parent_t, class outline_t> DLVreturn_type
DLV::render_outline_templ<scene_t, parent_t, outline_t>::detach_r3D(scene_t *view)
{
  obj->set_r_view(nullptr);
  return DLV_OK;
}

template <class scene_t, class parent_t, class shells_t> DLVreturn_type
DLV::render_shells_templ<scene_t, parent_t, shells_t>::attach_r3D(scene_t *view)
{
  obj->set_r_view(view);
  return DLV_OK;
}

template <class scene_t, class parent_t, class shells_t> DLVreturn_type
DLV::render_shells_templ<scene_t, parent_t, shells_t>::detach_r3D(scene_t *view)
{
  obj->set_r_view(nullptr);
  return DLV_OK;
}

template <class scene_t, class parent_t, class atoms_t> DLV::toolkit_obj
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::get_rspace() const
{
  return toolkit_obj(obj);
}

template <class scene_t, class parent_t, class atoms_t> DLV::toolkit_obj
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::get_kspace() const
{
  return toolkit_obj(obj);
}

template <class scene_t, class parent_t, class outline_t> DLV::toolkit_obj
DLV::render_outline_templ<scene_t, parent_t, outline_t>::get_rspace() const
{
  return toolkit_obj();
}

template <class scene_t, class parent_t, class outline_t> DLV::toolkit_obj
DLV::render_outline_templ<scene_t, parent_t, outline_t>::get_kspace() const
{
  return toolkit_obj();
}

template <class scene_t, class parent_t, class shells_t> DLV::toolkit_obj
DLV::render_shells_templ<scene_t, parent_t, shells_t>::get_rspace() const
{
  return toolkit_obj();
}

template <class scene_t, class parent_t, class shells_t> DLV::toolkit_obj
DLV::render_shells_templ<scene_t, parent_t, shells_t>::get_kspace() const
{
  return toolkit_obj();
}

template <class scene_t, class parent_t, class atoms_t> DLV::toolkit_obj
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::get_redit() const
{
  return toolkit_obj();
}

template <class scene_t, class parent_t, class atoms_t> DLV::toolkit_obj
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::get_kedit() const
{
  return toolkit_obj();
}

template <class scene_t, class parent_t, class outline_t> DLV::toolkit_obj
DLV::render_outline_templ<scene_t, parent_t, outline_t>::get_redit() const
{
  return toolkit_obj();
}

template <class scene_t, class parent_t, class outline_t> DLV::toolkit_obj
DLV::render_outline_templ<scene_t, parent_t, outline_t>::get_kedit() const
{
  return toolkit_obj();
}

template <class scene_t, class parent_t, class shells_t> DLV::toolkit_obj
DLV::render_shells_templ<scene_t, parent_t, shells_t>::get_redit() const
{
  return toolkit_obj();
}

template <class scene_t, class parent_t, class shells_t> DLV::toolkit_obj
DLV::render_shells_templ<scene_t, parent_t, shells_t>::get_kedit() const
{
  return toolkit_obj();
}

template <class scene_t, class parent_t> DLVreturn_type
DLV::render_parent_templ<scene_t, parent_t>::attach_kui(toolkit_obj &k_ui) const
{
  // BUG
  return DLV_ERROR;
}

template <class scene_t, class parent_t, class atoms_t> DLVreturn_type
DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::attach_rui(toolkit_obj &r_ui) const
{
  CCP3::attach_display_structure(obj, false);
  return DLV_OK;
}

template <class scene_t, class parent_t, class atoms_t>
DLVreturn_type DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::attach_kui(toolkit_obj &k_ui) const
{
  CCP3::attach_display_structure(obj, true);
  return DLV_OK;
}

template <class scene_t, class parent_t, class outline_t>
DLVreturn_type DLV::render_outline_templ<scene_t, parent_t, outline_t>::attach_rui(toolkit_obj &r_ui) const
{
  return DLV_OK;
}

template <class scene_t, class parent_t, class shells_t>
DLVreturn_type DLV::render_shells_templ<scene_t, parent_t, shells_t>::attach_rui(toolkit_obj &r_ui) const
{
  return DLV_OK;
}

template <class scene_t, class parent_t, class atoms_t>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::copy_settings(render_parent_templ<scene_t, parent_t> *p, const bool copy_cell,
				      const bool copy_wavefn)
{
}

template <class scene_t, class parent_t, class outline_t>
void DLV::render_outline_templ<scene_t, parent_t, outline_t>::copy_settings(render_parent_templ<scene_t, parent_t> *p, const bool copy_cell,
					const bool)
{
}

template <class scene_t, class parent_t, class shells_t>
void DLV::render_shells_templ<scene_t, parent_t, shells_t>::copy_settings(render_parent_templ<scene_t, parent_t> *p, const bool, const bool)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_data_size(const int n)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::select_data_object(const int n)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_edit_size(const int n)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_data_label(const string label, const int n)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_edit_label(const string label, const int n)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_sub_data_size(const int s, const int n)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_sub_edit_size(const int s, const int n)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_sub_data_label(const string label,
					    const int s, const int n)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_sub_edit_label(const string label,
					    const int s, const int n)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_display_type(const int v, const int n)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_edit_type(const int v, const int n)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_sub_data_vector(const bool v,
					     const int s, const int n)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_sub_edit_vector(const bool v,
					     const int s, const int n)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::add_display_list(const string name,
					  const int id, const int index) const
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::reset_display_list(const int object) const
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::reset_display_list(const int first,
					    const int last) const
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::empty_display_list() const
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::show_data_panel()
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::stop_animation()
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::show_animate_panel()
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::add_edit_list(const string name,
				       const int id, const int index) const
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::reset_edit_list(const int object) const
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::empty_edit_list() const
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_atom_group_size(const int)
{
  // a bug
}

template <class scene_t, class parent_t, class atoms_t>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::set_atom_group_size(const int size)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_atom_group_name(const string, const int)
{
  // a bug
}

template <class scene_t, class parent_t, class atoms_t>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::set_atom_group_name(const string name,
					       const int index)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::get_cell_data(int &na, int &nb, int &nc,
								bool &centre_cell, bool &conv_cell,
								bool &edges, double &tol) const
{
  // a bug
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_transforms(const int n,
					const float transforms[][3])
{
  // a bug
}

template <class scene_t, class parent_t, class atoms_t>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::get_cell_data(int &na, int &nb, int &nc,
									bool &centre_cell,
									bool &conv_cell,
									bool &edges,
									double &tol) const
{
  na = obj->common.na;
  nb = obj->common.nb;
  nc = obj->common.nc;
  centre_cell = obj->common.centred;
  conv_cell = obj->common.cell_type == CCP3::render_atoms::common_t::is_conventional_cell;
  edges = !obj->common.is_asymmetric_cell;
  tol = obj->common.cell_tolerance;
}

template <class scene_t, class parent_t, class atoms_t>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::set_transforms(const int n,
				       const float transforms[][3])
{
}

template <class scene_t, class parent_t, class outline_t>
void DLV::render_outline_templ<scene_t, parent_t, outline_t>::get_cell_data(int &na, int &nb, int &nc,
				      bool &centre_cell,
				      int &cell_type, double &tol) const
{
}

template <class scene_t, class parent_t, class outline_t>
void DLV::render_outline_templ<scene_t, parent_t, outline_t>::set_transforms(const int n,
				       const float transforms[][3])
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_CRYSTAL_wavefn(const int valence_min,
					    const int valence_max,
					    const int nbands,
					    const int lattice,
					    const int centre, const bool spin,
					    const int kpoints[][3],
					    const int nkpoints, const int sa,
					    const int sb, const int sc)
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_CRYSTAL_scf()
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_CRYSTAL_tddft()
{
}

template <class scene_t, class parent_t>
void DLV::render_parent_templ<scene_t, parent_t>::set_ONETEP_scf()
{
}

template <class scene_t, class parent_t, class atoms_t>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::set_CRYSTAL_wavefn(const int valence_min,
					   const int valence_max,
					   const int nbands,
					   const int lattice,
					   const int centre, const bool spin,
					   const int kpoints[][3],
					   const int nkpoints, const int sa,
					   const int sb, const int sc)
{
}

template <class scene_t, class parent_t, class atoms_t>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::set_CRYSTAL_scf()
{
}

template <class scene_t, class parent_t, class atoms_t>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::set_CRYSTAL_tddft()
{
}

template <class scene_t, class parent_t, class atoms_t>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::set_ONETEP_scf()
{
}

template class DLV::render_parent_templ<DLVview, CCP3::render_base>;
template class DLV::render_atoms_templ<DLVview, CCP3::render_base, CCP3::render_atoms>;
template class DLV::render_outline_templ<DLVview, CCP3::render_base, CCP3::render_outline>;
template class DLV::render_shells_templ<DLVview, CCP3::render_base, CCP3::render_shells>;

#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::render_atoms *t,
				    const unsigned int file_version)
    {
      DLV::string name = t->get_name();
      ar << name;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::render_atoms *t,
				    const unsigned int file_version)
    {
      DLV::string name;
      ar >> name;
      ::new(t)DLV::render_atoms(name.c_str());
      t->set_serialize_obj();
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::render_outline *t,
				    const unsigned int file_version)
    {
      DLV::string name = t->get_name();
      ar << name;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::render_outline *t,
				    const unsigned int file_version)
    {
      DLV::string name;
      ar >> name;
      ::new(t)DLV::render_outline(name.c_str());
      t->set_serialize_obj();
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::render_shells *t,
				    const unsigned int file_version)
    {
      DLV::string name = t->get_name();
      ar << name;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::render_shells *t,
				    const unsigned int file_version)
    {
      DLV::string name;
      ar >> name;
      ::new(t)DLV::render_shells(name.c_str());
      t->set_serialize_obj();
    }

  }
}

template <class Archive>
void CCP3::render_base::serialize(Archive &ar, const unsigned int version)
{
}

template <class Archive>
void CCP3::render_atoms::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<render_base>(*this);
  ar & common.na;
  ar & common.nb;
  ar & common.nc;
  ar & common.centred;
  ar & common.cell_type;
  ar & common.cell_tolerance;
  ar & lattice.draw_lattice;
  ar & lattice.label_lattice;
  ar & lines.line_width;
  ar & lines.smooth_lines;
  ar & atoms.draw_atoms;
  ar & atoms.atom_scale_percent;
  ar & atoms.selections_type;
  ar & atoms.label_selections;
  ar & atoms.what_to_select;
  ar & atoms.opacity_percent;
  ar & atoms.atom_subdivisions;
  ar & atoms.property_method; // Todo
  ar & bond_data.draw_bonds;
  ar & bond_data.draw_polyhedra;
  ar & bond_data.bond_overlap;
  ar & bond_data.bond_draw_type;
  ar & bond_data.bond_subdivisions;
  ar & bond_data.bond_radius;
  ar & bond_data.outline_polyhedra;
  ar & symmetry_selector; // Todo
  ar & background[0];
  ar & background[1];
  ar & background[2];
}

template <class Archive>
void CCP3::render_outline::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<render_base>(*this);
}

template <class Archive>
void CCP3::render_shells::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<render_base>(*this);
}

template <class scene_t, class parent_t> template <class Archive>
void DLV::render_parent_templ<scene_t, parent_t>::serialize(Archive &ar, const unsigned int version)
{
}

template <class scene_t, class parent_t, class atoms_t> template <class Archive>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<render_parent>(*this);
}

template <class scene_t, class parent_t, class atoms_t> template <class Archive>
void DLV::render_atoms_templ<scene_t, parent_t, atoms_t>::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<render_parent>(*this);
}

template <class scene_t, class parent_t, class outline_t> template <class Archive>
void DLV::render_outline_templ<scene_t, parent_t, outline_t>::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<render_parent>(*this);
  // Todo
}

template <class scene_t, class parent_t, class outline_t> template <class Archive>
void DLV::render_outline_templ<scene_t, parent_t, outline_t>::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<render_parent>(*this);
  // Todo - obj needs to exist!
}

template <class scene_t, class parent_t, class shells_t> template <class Archive>
void DLV::render_shells_templ<scene_t, parent_t, shells_t>::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<render_parent>(*this);
  // Todo
}

template <class scene_t, class parent_t, class shells_t> template <class Archive>
void DLV::render_shells_templ<scene_t, parent_t, shells_t>::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<render_parent>(*this);
  // Todo - obj needs to exist!
}

BOOST_CLASS_EXPORT_IMPLEMENT(CCP3::render_base)
BOOST_CLASS_EXPORT_IMPLEMENT(CCP3::render_atoms)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::render_parent)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::render_atoms)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::render_outline)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::render_shells)

#endif // DLV_USES_SERIALIZE
