
#include "../dlv/types.hxx"
#include "../dlv/boost_lib.hxx"
#include "../graphics/toolkit_obj.hxx"
#include "../graphics/render_base.hxx"
#include "../graphics/drawable.hxx"
#include "../graphics/edit_objs.hxx"

DLV::edited_obj::~edited_obj()
{
}

void DLV::edited_obj::update_edit_list(const render_parent *parent,
				       const string name, const int index)
{
}

DLV::edited_obj *
DLV::edited_obj::create_orthoslice(const render_parent *parent,
				   const drawable_obj *draw, const bool kspace,
				   char message[], const int mlen)
{
  return nullptr;
}

DLV::edited_obj *
DLV::edited_obj::create_slice(const render_parent *parent,
			      const drawable_obj *draw, const bool kspace,
			      char message[], const int mlen)
{
  return nullptr;
}

DLV::edited_obj *
DLV::edited_obj::create_clamp(const render_parent *parent,
			      const drawable_obj *draw,
			      const int component, const bool kspace,
			      char message[], const int mlen)
{
  return nullptr;
}

DLV::edited_obj *
DLV::edited_obj::create_crop(const render_parent *parent,
			     const drawable_obj *draw, const bool kspace,
			     char message[], const int mlen)
{
  return nullptr;
}

DLV::edited_obj *
DLV::edited_obj::create_cut(const render_parent *parent,
			    const drawable_obj *draw, const bool kspace,
			    char message[], const int mlen)
{
  return nullptr;
}

DLV::edited_obj *
DLV::edited_obj::create_downsize(const render_parent *parent,
				 const drawable_obj *draw, const bool kspace,
				 char message[], const int mlen)
{
  return nullptr;
}

DLV::edited_obj *
DLV::edited_obj::create_extension(const render_parent *parent,
				  const drawable_obj *draw, const bool kspace,
				  const int x, const int y, const int z,
				  char message[], const int mlen)
{
  return nullptr;
}

DLV::edited_obj *
DLV::edited_obj::create_math(const render_parent *parent,
			     const drawable_obj *draw, const int component,
			     const bool kspace, char message[], const int mlen)
{
  return nullptr;
}

DLV::edited_obj *
DLV::edited_obj::create_vol_render(const render_parent *parent,
				   const drawable_obj *draw, const bool kspace,
				   char message[], const int mlen)
{
  return nullptr;
}

DLV::edited_obj *
DLV::edited_obj::create_spectrum(const render_parent *parent, const int index,
				 char message[], const int mlen)
{
  return nullptr;
}

DLV::edited_obj *
DLV::edited_obj::create_phonon_traj(const render_parent *parent,
				    const int index, char message[],
				    const int mlen)
{
  return nullptr;
}

DLV::edited_obj *
DLV::edited_obj::create_dos_shift(const render_parent *parent,
				  const drawable_obj *draw,
				  const bool kspace, char message[],
				  const int mlen)
{
  return nullptr;
}

DLV::edited_obj *
DLV::edited_obj::create_band_shift(const render_parent *parent,
				   const drawable_obj *draw,
				   const bool kspace, char message[],
				   const int mlen)
{
  return nullptr;
}

DLV::edited_obj *
DLV::edited_obj::create_bdos_shift(const render_parent *parent,
				   const drawable_obj *draw,
				   const bool kspace, char message[],
				   const int mlen)
{
  return nullptr;
}

bool DLV::edited_obj::update_spectrum(float grid[], float data[], const int n,
				 const string name, char message[],
				 const int mlen)
{
  return false;
}

bool DLV::edited_obj::update_slice(const class drawable_obj *draw,
				   char message[], const int mlen)
{
  return false;
}

bool DLV::edited_obj::update_cut(const class drawable_obj *draw, char message[],
				 const int mlen)
{
  return false;
}

bool DLV::edited_obj::update_extension(const int na, const int nb, const int nc,
				       const float o[3], const float a[3],
				       const float b[3], const float c[3],
				       const int onx, const int ony,
				       const int onz, float *origdata[],
				       const int vdims[], const int ndata,
				       char message[], const int mlen)
{
  return false;
}

bool DLV::edited_obj::update_math(const class drawable_obj *draw,
				  const int cmpt, const int index,
				  char message[], const int mlen)
{
  return false;
}

bool DLV::edited_obj::update_volr(const float o[3], const float a[3],
				  const float b[3], const float c[3],
				  const int nx, const int ny,
				  const int nz, float *origdata[],
				  const int vdims[], const int ndata,
				  char message[], const int mlen)
{
  return false;
}

bool DLV::edited_obj::update_shift(const float shift, char message[],
				   const int mlen)
{
  return false;
}

class DLV::drawable_obj *DLV::edited_obj::replace_edit(class toolkit_obj &id)
{
  return nullptr;
}

#ifdef DLV_USES_SERIALIZE

template <class Archive>
void DLV::edited_obj::serialize(Archive &ar, const unsigned int version)
{
  ar & draw;
  ar & reciprocal_space;
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::edited_obj)

#endif // DLV_USES_SERIALIZE
