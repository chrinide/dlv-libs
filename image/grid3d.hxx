
#ifndef IMAGE_VOLUME_DATA
#define IMAGE_VOLUME_DATA

namespace IMAGE {

  // interface class for 3D grids
  class grid3D {
  protected:
    DLV::volume_data *read_data(DLV::operation *op, DLV::model *m,
				const char filename[], char message[],
				const int_g mlen);
    bool get_grid(std::FILE *input, int_g &nx, int_g &ny, int_g &nz,
		  real_g origin[3], real_g astep[3], real_g bstep[3],
		  real_g cstep[3], char label[], char message[],
		  const int_g mlen);
    bool get_data(std::FILE *input, DLV::volume_data *data,
		  const int_g nx, const int_g ny, const int_g nz,
		  const char label[], char message[], const int_g mlen);
    bool read_grid(std::FILE *input, real_g origin[3], real_g astep[3],
		   real_g bstep[3], real_g cstep[3], char message[],
		   const int_g mlen);
    bool read_data(std::FILE *input, DLV::volume_data *data,
		   const int_g nx, const int_g ny, const int_g nz,
		   const int_g vec, const char label[],
		   char message[], const int_g mlen);
  };

  class load_3d_data : public DLV::load_model_op, public grid3D {
  public:
    static DLV::operation *create(const char filename[],
				  char message[], const int_g mlen);

    bool reload_data(DLV::data_object *data, char message[], const int_g mlen);

  protected:
    load_3d_data(DLV::model *m, const char file[]);

    DLV::string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version)
    {
      ar & boost::serialization::base_object<DLV::load_data_op>(*this);
    }
#endif // DLV_USES_SERIALIZE
  };

}

inline IMAGE::load_3d_data::load_3d_data(DLV::model *m, const char file[])
  : load_model_op(m, file)
{
}

#endif // IMAGE_VOLUME_DATA
