/**********************************************************************
* This software was developed at the National Institute of Standards  *
* and Technology by employees of the Federal Government in the course *
* of their official duties.  Pursuant to title 17 Section 105 of the  *
* United States Code this software is not subject to copyright        *
* protection and is in the public domain.                             *
**********************************************************************/ 
#include <math.h>
#include "vertex.h"
#include <stdio.h>

using namespace Wulff;

vector::vector()
{
  X = Y = Z = 0;
}

vector::vector(double x, double y, double z)
{
  X = x; Y = y; Z = z;
}

double vector::mag(void)
{
  return sqrt(X*X+Y*Y+Z*Z);
}

double vector::mag2(void)
{
  return X*X+Y*Y+Z*Z;
}

void vector::normalize(void)
{
  double imag = 1./sqrt(X*X+Y*Y+Z*Z);
  X *= imag;
  Y *= imag;
  Z *= imag;
}



static int current_vertex = 0;
vertex::vertex()
{
  which = current_vertex++;
  X = Y = Z = 0;
  allocated_planes = num_planes = 0;
  plane = NULL;
}

vertex::vertex(vertex &copy)
{
  X = copy.X; Y = copy.Y; Z = copy.Z;
  num_planes = copy.num_planes;
  if(plane) delete [] plane;
  plane = new int[num_planes];
  allocated_planes = num_planes;
  for(int i = 0; i<num_planes; i++)
    plane[i] = copy.plane[i];
}

void vertex::add_plane(int num)
{
  int i;
  i = which;
  for(i = 0; i<num_planes && plane[i] < num; i++);
  if(i<num_planes && plane[i] == num)
    return; // Got that one.
/*  if(which == 0)
    fprintf(stderr,"%d adding %d\n",which,num);*/

  if(num_planes == allocated_planes)
  {
    allocated_planes += 5;
    int *temp_planes = new int[allocated_planes];
    int j;
    for(j = 0; j<i; j++)
      temp_planes[j] = plane[j];
    temp_planes[i] = num;
    for(j = i; j<num_planes; j++)
      temp_planes[j+1] = plane[j];
    if(plane)
      delete [] plane;
    plane = temp_planes;
  }
  else
  {
    int j;
    for(j = num_planes; j>i; j--)
      plane[j] = plane[j-1];
    plane[j] = num;
  }
  num_planes++;
}

void vertex::remove_plane(int num)
{
  // BGS, i scoping was incorrect for ISO C++
  int i;
  for(i = 0; i<num_planes && plane[i] < num; i++);
  if(plane[i] == num)
  {
    num_planes--;
    for(int j = i; j<num_planes; j++)
      plane[j] = plane[j+1];
  }
}
