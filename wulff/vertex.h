/**********************************************************************
* This software was developed at the National Institute of Standards  *
* and Technology by employees of the Federal Government in the course *
* of their official duties.  Pursuant to title 17 Section 105 of the  *
* United States Code this software is not subject to copyright        *
* protection and is in the public domain.                             *
**********************************************************************/ 

namespace Wulff {

class vector
{
    public:
  double X,Y,Z;
  int color;
  vector();
  vector(double x, double y, double z);

  double mag();
  double mag2();
  void normalize();

  vector operator *=(double r)
  {
    X *= r; Y *= r; Z *= r;
    return *this;
  }

  vector operator /=(double r)
  {
    X /= r; Y /= r; Z /= r;
    return *this;
  }

  vector operator +=(vector v)
  {
    X += v.X;
    Y += v.Y;
    Z += v.Z;
    return *this;
  }

  vector operator -=(vector v)
  {
    X -= v.X;
    Y -= v.Y;
    Z -= v.Z;
    return *this;
  }

  inline double operator *(vector v) const            // Dot product
  {
    return X*v.X + Y*v.Y + Z*v.Z;
  }

  vector operator %(vector v) const            // Cross product
  {
    return vector(Y*v.Z-Z*v.Y,Z*v.X-X*v.Z,X*v.Y-Y*v.X);
  }

  vector operator +(vector v) const            // Vector addition
  {
    return vector(X+v.X,Y+v.Y,Z+v.Z);
  }

  vector operator -(vector v) const            // Vector subtraction
  {
    return vector(X-v.X,Y-v.Y,Z-v.Z);
  }

  vector operator+() const                     // Unary + (nothing)
  {
    return *this;
  }

  vector operator-() const                     // Unary - (negation)
  {
    return vector(-X,-Y,-Z);
  }
};

class vertex:public vector
{
    private:
  int allocated_planes;
  int which;

    public:
  int *plane;
  int num_planes;

  vertex();
  vertex(double x, double y, double z):vector(x,y,z)
  {
    allocated_planes = num_planes = 0;
  }
  vertex(vertex &copy);

  ~vertex()
  {
    if(plane)
      delete [] plane;
  }
    
  void add_plane(int);
  void remove_plane(int);
};

extern void OOGL_output_shape(vertex *v, int nv, vector *planes,
			      int ** &face_vertices, int * &num_face_vertices,
			      int &num_faces);

}
