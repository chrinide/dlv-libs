/**********************************************************************
* This software was developed at the National Institute of Standards  *
* and Technology by employees of the Federal Government in the course *
* of their official duties.  Pursuant to title 17 Section 105 of the  *
* United States Code this software is not subject to copyright        *
* protection and is in the public domain.                             *
**********************************************************************/ 
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "vertex.h"

namespace Wulff {
/* This file is a hack to use earlier code */

typedef vertex *p2vertex;

/* Produces the proper list of vertices and the planes they are attached to */
extern void make_shape(vector *, double *, int, p2vertex &, int &);

/* Writes OOGL code for geomview */
extern void OOGL_output_shape(vertex *v, int nv, vector *planes,
			      int ** &face_vertices, int * &num_face_vertices,
			      int &num_faces, int nnormals);

static void set_clipper(vector *, double *, int);

extern double clipper_energy;
extern double clipper_skew;
extern int clipper_color;
extern int clipper_number;

}

using namespace Wulff;

void output_OOGL(const int nnormals, const double normals[][3],
		 const double surface_energy[], const int colours[],
		 double (* &verts)[3], int &nverts, int ** &face_vertices,
		 int * &nface_vertices, int &num_faces)
{
  int nn = nnormals;
  vector *normal = new vector[nn+clipper_number];
  double *gamma = new double[nn+clipper_number];

  int i = 0;
  for(i = 0; i < nnormals; i++) {
    normal[i].X = normals[i][0];
    normal[i].Y = normals[i][1];
    normal[i].Z = normals[i][2];
    normal[i].color = colours[i];
    gamma[i] = surface_energy[i];
  }

  // includes the clipper normals
  set_clipper(normal,gamma,nn);

  vertex *the_vertices = NULL;
  int num_vertices = 0;
  make_shape(normal, gamma, nn+clipper_number, the_vertices, num_vertices);
  if(num_vertices) {
    OOGL_output_shape(the_vertices, num_vertices, normal, face_vertices,
		      nface_vertices, num_faces, nnormals);
    // BGS, copy vertices back to AVS
    verts = new double[num_vertices][3];
    for (i = 0; i < num_vertices; i++) {
      verts[i][0] = the_vertices[i].X;
      verts[i][1] = the_vertices[i].Y;
      verts[i][2] = the_vertices[i].Z;
    }
    nverts = num_vertices;
  }
  delete [] gamma;
  delete [] normal;
  if(the_vertices != NULL)
    delete [] the_vertices;
}

vector *clipper = NULL;
void Wulff::set_clipper(vector *n, double *gamma, int nn)
{
  static int last_nn = 0;
  static int last_cn = 0;
  static double last_cp = 0;

  // (re)construct the clipper
  if(nn != last_nn || last_cn != clipper_number ||
     clipper_skew != last_cp || clipper == NULL)
  {
    last_nn = nn;
    last_cn = clipper_number;
    last_cp = clipper_skew;

    if(clipper != NULL)
      delete [] clipper;
    clipper = new vector[clipper_number];
    // First eight bounding normals we put in an octahedron by default
    // BGS - i scoping incorrect for ISO C++
    int i;
    for(i = 0; i<8; i++)
    {
      clipper[i].X = i&1 ? 1 : -1;
      clipper[i].Y = i&2 ? 1 : -1;
      clipper[i].Z = i&4 ? 1 : -1;
    }
    
    // Randomly scatter the remainder.
    for(; i<clipper_number; i++)
    {
      clipper[i].X   = rand();
      clipper[i].Y = rand();
      clipper[i].Z = rand();
      clipper[i].normalize();
      if(i&1)
	clipper[i].X *= -1;
      if(i&2)
	clipper[i].Y *= -1;
      if(i&4)
	clipper[i].Z *= -1;
    }

    // Then we skew all the points around 
    vector *vel = new vector[clipper_number];
    for(int loops = 0; loops < 5; loops++)
    {
      for(i = 0; i<clipper_number; i++)
      {
	vel[i] = vector(0,0,0);
	if(clipper_skew)
	{
	  // Existing normals skew the 'smooth' ones towards corners.
	  for(int j = 0; j<nn; j++)
	  {
	    vector v = clipper[i] - n[j];
	    double scale = v.mag2();
	    if(scale)
	      v /= scale;
	    vel[i] += v;
	  }
	  vel[i] *= clipper_skew;
	}

	// Then we even things out a bit here
	for(int j = 0; j<i; j++)
	{
	  vector v = clipper[i] - clipper[j];
	  v /= v.mag2();
	  vel[i] += v;
	  vel[j] -= v;
	}
      }
      for(i = 0; i<clipper_number; i++)
      {
	clipper[i] += vel[i];
	clipper[i].normalize();
      }
    }
  }
  // Move pointers up
  n += nn;
  gamma += nn;
  // Load in the clipper values.
  for(int i = 0; i<clipper_number; i++)
  {
    n[i] = clipper[i];
    n[i].color = clipper_color;
    gamma[i] = clipper_energy;
  }
}
