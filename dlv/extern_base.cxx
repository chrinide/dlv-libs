
#include <list>
#include <map>
#include <iostream>
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "utils.hxx"
#include "math_fns.hxx"
#include "model.hxx"
#include "operation.hxx"
#include "data_objs.hxx"
#include "project.hxx"
#include "atom_prefs.hxx"
#include "extern_base.hxx"

bool DLV::initialise(const char prefs_file[])
{
  atomic_data = new DLV::atomic_prefs;
  if (prefs_file != 0) {
    if (strlen(prefs_file) > 0) {
      char message[256];
      if (atomic_data->read(prefs_file, message, 256) != DLV_OK) {
	std::cout << message << "\n";
	return false;
      }
    }
  }
  return true;
}

bool DLV::init_project(const char prefs_file[])
{
  char message[256];
  DLVreturn_type ok = project::initialise(prefs_file, "", "Unnamed",
					  message, 256);
  if (ok != DLV_OK)
    std::cout << message << std::endl;
  return (ok != DLV_ERROR);
}
