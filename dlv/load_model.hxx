
#ifndef DLV_LOAD_MODEL
#define DLV_LOAD_MODEL

namespace DLV {

  class load_xyz_file : public load_atom_model_op {
  public:
    static operation *create(const char name[], const char filename[],
			     const bool bonds, char message[],
			     const int_g mlen);

    // public for serialization
    load_xyz_file(model *m, const char file[]);

  protected:
    string get_name() const;
    static model *read(std::ifstream &input, string def_name,
		       char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_xtl_file : public load_atom_model_op {
  public:
    static operation *create(const char name[], const char filename[],
			     const bool bonds, char message[],
			     const int_g mlen);

    // public for serialization
    load_xtl_file(model *m, const char file[]);

  protected:
    string get_name() const;
    static model *read(std::ifstream &input, string def_name,
		       char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_cssr_file : public load_atom_model_op {
  public:
    static operation *create(const char name[], const char filename[],
			     const bool bonds, char message[],
			     const int_g mlen);

    // public for serialization
    load_cssr_file(model *m, const char file[]);

  protected:
    string get_name() const;
    static model *read(std::ifstream &input, string def_name,
		       char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class load_shelx_file : public load_atom_model_op {
  public:
    static operation *create(const char name[], const char filename[],
			     const bool bonds, char message[],
			     const int_g mlen);

    // public for serialization
    load_shelx_file(model *m, const char file[]);

  protected:
    string get_name() const;
    static model *read(std::ifstream &input, string def_name,
		       char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE

BOOST_CLASS_EXPORT_KEY(DLV::load_xyz_file)
BOOST_CLASS_EXPORT_KEY(DLV::load_xtl_file)
BOOST_CLASS_EXPORT_KEY(DLV::load_cssr_file)
BOOST_CLASS_EXPORT_KEY(DLV::load_shelx_file)

#endif // DLV_USES_SERIALIZE

inline DLV::load_xyz_file::load_xyz_file(model *m, const char file[])
  : load_atom_model_op(m, file)
{
}

inline DLV::load_xtl_file::load_xtl_file(model *m, const char file[])
  : load_atom_model_op(m, file)
{
}

inline DLV::load_cssr_file::load_cssr_file(model *m, const char file[])
  : load_atom_model_op(m, file)
{
}

inline DLV::load_shelx_file::load_shelx_file(model *m, const char file[])
  : load_atom_model_op(m, file)
{
}

#endif // DLV_LOAD_MODEL
