
#ifndef DLV_JOBS
#define DLV_JOBS

#ifdef WIN32
  #define LOCAL_EXEC_SUFFIX ".exe"
#else
  #define LOCAL_EXEC_SUFFIX ""
#endif // WIN32

#include <map>

namespace DLV {

  enum job_status_type {
    run_and_completed, run_and_failed, run_and_recovered,
    still_running, running_and_updated, not_started, recovered_with_errors,
    in_queue, being_submitted, suspended, initialising_job, nonDLV_job
  };

  class job {
  public:
    static job *create(const string local_path, const string serial_bin,
		       const string parallel_bin, const string args,
		       const class job_setup_data &job_data,
		       const bool use_stdin, const int_g input,
		       file_obj *output, file_obj *error, file_obj *jerror,
		       std::map<int, file_obj *> &inp,
		       std::map<int, file_obj *> &out,
		       const bool allow_parallel_job, const bool external_job,
		       const string external_dir);
    static job *connect(const string binary, file_obj *jerror,
			std::map<int, file_obj *> &inp,
			std::map<int, file_obj *> &out,
			const string dir, class text_buffer *data,
			char message[], const int_g mlen);

    virtual ~job();

    bool execute(const bool foreground, char message[], const int_g mlen);
    job_status_type get_current_status() const;
    static string get_status_info(job_status_type s);
    virtual job_status_type get_status() = 0;
    virtual void reset_user_status();
    virtual bool is_external_job() const;
    virtual bool recover_files(bool &err_file, bool &log_file,
			       char message[], const int_g mlen) = 0;
    virtual bool tidy_files(char message[], const int_g mlen) = 0;
    virtual bool kill(char message[], const int_g mlen) = 0;

    void set_failure();

    virtual bool is_connected() const;
    virtual DLVreturn_type write_connection(const char command[]);
    virtual void read_connection(class operation *op, const bool complete);
    virtual void close_connection();

#ifdef ENABLE_DLV_JOB_THREADS
    static boost::mutex::scoped_lock *acquire_status_lock();
    static bool get_global_status();
    static void clear_global_status();
    void clear_status();
    bool status_has_changed() const;
    void change_status();
    void watch();
    virtual void monitor_job();
#endif // ENABLE_DLV_JOB_THREADS

  protected:
    job(const string binary, const string args, const bool input,
	const int_g in_idx, file_obj *output, file_obj *serror,
	file_obj *jerror, std::map<int, file_obj *> &inp,
	std::map<int, file_obj *> &out,
	const job_status_type js = initialising_job);

    static string make_scratch_name(const string dir,
				    const bool exclude = false);
    string get_job_dir() const;
    file_obj *get_stdin() const;
    file_obj *get_output() const;
    file_obj *get_error() const;
    file_obj *get_joberr() const;
    std::map<int, file_obj *>::const_iterator inp_begin();
    std::map<int, file_obj *>::const_iterator inp_end();
    std::map<int, file_obj *>::const_iterator out_begin();
    std::map<int, file_obj *>::const_iterator out_end();
    string get_executable() const;
    string get_cmdline_args() const;
    bool need_stdin() const;

    virtual bool copy_files(char message[], const int_g mlen) = 0;
    virtual bool run_binary(const bool foreground, char message[],
			    const int_g mlen) = 0;
    void set_status(const job_status_type s);

    void start_monitoring();
    void interrupt_monitoring();
    void stop_monitoring();

  private:
    string executable;
    string cmdline_args;
    string scratch_dir;
    job_status_type status;
    //class file_obj *stdinput;
    class file_obj *stdoutput;
    class file_obj *stderror;
    class file_obj *joberror;
    bool use_stdin;
    int_g sinput;
    std::map<int, class file_obj *> &inp_files;
    std::map<int, class file_obj *> &out_files;
#ifdef ENABLE_DLV_JOB_THREADS
    // monitor stuff
    static bool global_update;
    static boost::mutex mutex;
    boost::thread *thread;
    bool status_update;
#endif // ENABLE_DLV_JOB_THREADS

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_SERIALIZATION_ASSUME_ABSTRACT(DLV::job)
#endif // DLV_USES_SERIALIZE

inline DLV::string DLV::job::get_job_dir() const
{
  return scratch_dir;
}

inline class DLV::file_obj *DLV::job::get_stdin() const
{
  return inp_files.find(sinput)->second;
}

inline class DLV::file_obj *DLV::job::get_output() const
{
  return stdoutput;
}

inline class DLV::file_obj *DLV::job::get_error() const
{
  return stderror;
}

inline class DLV::file_obj *DLV::job::get_joberr() const
{
  return joberror;
}

inline
std::map<int, class DLV::file_obj *>::const_iterator DLV::job::inp_begin()
{
  return inp_files.begin();
}

inline std::map<int, class DLV::file_obj *>::const_iterator DLV::job::inp_end()
{
  return inp_files.end();
}

inline
std::map<int, class DLV::file_obj *>::const_iterator DLV::job::out_begin()
{
  return out_files.begin();
}

inline std::map<int, class DLV::file_obj *>::const_iterator DLV::job::out_end()
{
  return out_files.end();
}

inline bool DLV::job::need_stdin() const
{
  return use_stdin;
}

inline void DLV::job::set_status(const job_status_type s)
{
  status = s;
}

inline DLV::job_status_type DLV::job::get_current_status() const
{
  return status;
}

inline DLV::string DLV::job::get_executable() const
{
  return executable;
}

inline DLV::string DLV::job::get_cmdline_args() const
{
  return cmdline_args;
}

inline void DLV::job::set_failure()
{
  set_status(recovered_with_errors);
}

#ifdef ENABLE_DLV_JOB_THREADS

inline bool DLV::job::get_global_status()
{
  return global_update;
}

inline void DLV::job::clear_global_status()
{
  global_update = false;
}

inline void DLV::job::clear_status()
{
  status_update = false;
}

inline bool DLV::job::status_has_changed() const
{
  return status_update;
}

inline void DLV::job::change_status()
{
  status_update = true;
  global_update = true;
}

#endif // ENABLE_DLV_JOB_THREADS

#endif // DLV_JOBS
