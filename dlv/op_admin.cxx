
#include <map>
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS // for data_objs
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "utils.hxx"
//#include "atom_model.hxx"
#include "model.hxx"
#include "operation.hxx"
#include "op_admin.hxx"

DLV::operation *DLV::subproject::create(const char label[], const char dir[],
					char message[], const int_g mlen)
{
  subproject *op = 0;
  char dir_name[256];
  if (strlen(dir) == 0) {
    strncpy(dir_name, label, 64);
    dir_name[63] = '\0';
  } else {
    strncpy(dir_name, dir, 256);
    dir_name[255] = '\0';
  }
  if (file_exists(dir_name))
    strncpy(message, "directory already exists", mlen);
  else if (make_directory(dir_name)) {
    op = new subproject(label, dir);
    op->attach();
  } else
    strncpy(message, "Failed to create sub-project directory", mlen);
  return op;
}

DLV::string DLV::subproject::get_name() const
{
  return (name + " (Subproject)");
}

bool DLV::subproject::is_subproject() const
{
  return true;
}

bool DLV::subproject::reload_data(class data_object *, char [], const int_g)
{
  // Null call, Nothing to reload
  return true;
}

void DLV::subproject::add_standard_data_objects()
{
  // Do nothing
}

void DLV::subproject::add_path(string &dir) const
{
  string name = directory;
  name += DIR_SEP_CHR;
  name += dir;
  dir = name;
}

bool DLV::load_data_op::is_data_load() const
{
  return true;
}

bool DLV::save_file_op::reload_data(class data_object *, char [], const int_g)
{
  // Null call, Nothing to reload
  return true;
}

void DLV::empty_operation::add_standard_data_objects()
{
  // Do nothing
}

void DLV::load_data_op::add_standard_data_objects()
{
  // Do nothing
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::subproject *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::subproject("subproject", "");
    }

    /*
    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::save_model_op *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::save_model_op("save");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::load_data_op *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::load_data_op("load");
    }
    */

  }
}

template <class Archive>
void DLV::subproject::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<operation>(*this);
  ar & name;
  ar & directory;
}

template <class Archive>
void DLV::save_file_op::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<empty_operation>(*this);
  ar & filename;
}

template <class Archive>
void DLV::save_input_op::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<save_file_op>(*this);
}

template <class Archive>
void DLV::save_model_op::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<save_file_op>(*this);
}

template <class Archive>
void DLV::load_data_op::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<load_op>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::subproject)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::empty_operation)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::save_file_op)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::save_input_op)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::save_model_op)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::load_data_op)

DLV_SUPPRESS_TEMPLATES(DLV::operation)

DLV_NORMAL_EXPLICIT(DLV::subproject)
DLV_NORMAL_EXPLICIT(DLV::empty_operation)
DLV_NORMAL_EXPLICIT(DLV::save_file_op)
DLV_NORMAL_EXPLICIT(DLV::save_input_op)
DLV_NORMAL_EXPLICIT(DLV::save_model_op)
DLV_EXPORT_EXPLICIT(DLV::save_model_op)
DLV_NORMAL_EXPLICIT(DLV::load_data_op)
DLV_EXPORT_EXPLICIT(DLV::load_data_op)

#endif // DLV_USES_SERIALIZE
