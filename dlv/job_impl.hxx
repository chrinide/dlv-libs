
#ifndef DLV_LOCAL_JOBS
#define DLV_LOCAL_JOBS

#include <queue>

namespace DLV {

  class job_watcher {
  public:
    job_watcher(job *j);

    void operator() ();

  private:
    job *parent;
  };

  class local_job : public job {
  public:
    local_job(const string binary, const string args, const bool input,
	      const int_g in_idx, file_obj *output, file_obj *serror,
	      file_obj *jerror, std::map<int, file_obj *> &inp,
	      std::map<int, file_obj *> &out,
	      const job_status_type js = initialising_job);

    job_status_type get_status();
    bool recover_files(bool &err_file, bool &log_file,
		       char message[], const int_g mlen);
    bool tidy_files(char message[], const int_g mlen);
    bool kill(char message[], const int_g mlen);

#ifdef ENABLE_DLV_JOB_THREADS
    void monitor_job();
#endif // ENABLE_DLV_JOB_THREADS

  protected:
    bool copy_files(char message[], const int_g mlen);
    bool run_binary(const bool foreground, char message[], const int_g mlen);

#ifdef WIN32
    void set_process_id(const DWORD proc);
#else
    void set_process_id(const int_g id);
#endif // WIN32

  private:
#ifdef WIN32
    DWORD process_id;
#else
    pid_t pid;
#endif // WIN32
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  // Todo - is the inheritence here the right way round?
  class user_job : public local_job {
  public:
    user_job(const string binary, const string args, const bool input,
	     const int_g in_idx, file_obj *output, file_obj *serror,
	     file_obj *jerror, std::map<int, file_obj *> &inp,
	     std::map<int, file_obj *> &out);

    job_status_type get_status();
    void reset_user_status();
    bool is_external_job() const;
    bool recover_files(bool &err_file, bool &log_file,
		       char message[], const int_g mlen);
    bool kill(char message[], const int_g mlen);

#ifdef ENABLE_DLV_JOB_THREADS
    void monitor_job();
#endif // ENABLE_DLV_JOB_THREADS

  protected:
    bool run_binary(const bool foreground, char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class socket_job : public local_job {
  public:
    socket_job(const string binary, file_obj *jerror,
	       std::map<int, file_obj *> &inp,
	       std::map<int, file_obj *> &out, class text_buffer *data);

    job_status_type get_status();
    bool recover_files(bool &err_file, bool &log_file,
		       char message[], const int_g mlen);
    bool tidy_files(char message[], const int_g mlen);
    bool kill(char message[], const int_g mlen);

    bool is_connected() const;
    DLVreturn_type write_connection(const char command[]);
    void read_connection(class operation *op, const bool complete);
    void close_connection();

#ifdef ENABLE_DLV_JOB_THREADS
    void monitor_job();
#endif // ENABLE_DLV_JOB_THREADS

  protected:
    //bool copy_files(char message[], const int_g mlen);
    bool run_binary(const bool foreground, char message[], const int_g mlen);

#ifndef WIN32
    static void read_socket(char *job);
#endif // WIN32

  private:
    class text_buffer *buffer;
    std::queue<class operation *> ops;
    string op_data;
#ifndef WIN32
    int socket1, socket2;
#else
    HANDLE RdPipe, WrPipe;
#endif // WIN32
    bool in_dlv;

    int_g read_data(char *buff, const int_g length) const;
    DLVreturn_type read_socket_data(char * &dbuff, const bool check) const;
#ifdef WIN32
    void read_socket(const bool check);
#else
    void read_socket();
#endif // WIN32

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE

BOOST_CLASS_EXPORT_KEY(DLV::local_job)
BOOST_CLASS_EXPORT_KEY(DLV::user_job)
BOOST_CLASS_EXPORT_KEY(DLV::socket_job)

#endif // DLV_USES_SERIALIZE

inline DLV::local_job::local_job(const string binary, const string args,
				 const bool input, const int_g in_idx,
				 file_obj *output, file_obj *serror,
				 file_obj *jerror,
				 std::map<int, file_obj *> &inp,
				 std::map<int, file_obj *> &out,
				 const job_status_type js)
  : job(binary, args, input, in_idx, output, serror, jerror, inp, out, js)
{
}

inline DLV::user_job::user_job(const string binary, const string args,
			       const bool input, const int_g in_idx,
			       file_obj *output, file_obj *serror,
			       file_obj *jerror,
			       std::map<int, file_obj *> &inp,
			       std::map<int, file_obj *> &out)
  : local_job(binary, args, input, in_idx, output, serror, jerror, inp, out,
	      nonDLV_job)
{
}

inline DLV::socket_job::socket_job(const string binary, file_obj *jerror,
				   std::map<int, file_obj *> &inp,
				   std::map<int, file_obj *> &out,
				   class text_buffer *data)
  : local_job(binary, "", false, -1, 0, jerror, 0, inp, out), buffer(data),
#ifndef WIN32
    socket1(0), socket2(0),
#endif //WIN32
    in_dlv(false)
{
}

#ifdef WIN32

inline void DLV::local_job::set_process_id(const DWORD proc)
{
  process_id = proc;
}

#else

inline void DLV::local_job::set_process_id(const int_g id)
{
  pid = id;
}

#endif // WIN32

#endif // DLV_LOCAL_JOBS
