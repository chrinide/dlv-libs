
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
//#  include "../graphics/toolkit_obj.hxx"
#  include "../graphics/render_base.hxx"
//#  include "../graphics/calculations.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "operation.hxx"
#include "op_model.hxx"
#include "op_atoms.hxx"
#include "extern_data_model.hxx"

//creates a model that requires a data_obj
DLVreturn_type DLV::wulff_crystal(const char name[], const real_l r,
				  const int selection, char message[],
				  const int_g mlen)
{
  return wulff::create(name, r, selection, message, mlen);
}

DLVreturn_type DLV::wulff_update(const real_l r, char message[],
				 const int_g mlen)
{
  return operation::wulff_update(r, message, mlen);
}

#ifdef ENABLE_DLV_GRAPHICS

// can't guarantee that data isn't atom based
DLV::operation *DLV::edit_current_data(const int_g object,
				       const int_g component,
				       const int_g method, char message[],
				       const int_g mlen)
{
  return operation::edit_current_data(object, component, method, message, mlen);
}

DLVreturn_type DLV::select_current_edit(const int_g object, char message[],
					const int_g mlen)
{
  return operation::select_current_edit(object, message, mlen);
}

DLVreturn_type DLV::update_current_spectrum(const int_g object,
					    const int_g type, const real_g emin,
					    const real_g emax, const int_g np,
					    const real_g w,
					    const real_g harmonics,
					    char message[], const int_g mlen)
{
  return operation::update_current_spectrum(object, type, emin, emax, np, w,
					    harmonics, message, mlen);
}

// definitely an atom based data object
DLVreturn_type DLV::update_current_phonon(const int_g object,
					  const int_g nframes,
					  const real_g scale,
					  const bool use_temp,
					  const real_g temperature,
					  char message[], const int_g mlen)
{
  return operation::update_current_phonon(object, nframes, scale, use_temp,
					  temperature, message, mlen);
}

#endif // ENABLE_DLV_GRAPHICS
