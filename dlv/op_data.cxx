
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "utils.hxx"
#include "constants.hxx"
#include "operation.hxx"
#include "op_admin.hxx"
#include "op_model.hxx"
#include "atom_model.hxx"
#include "model.hxx"
#include "data_objs.hxx"
#include "data_simple.hxx"
#include "data_surf.hxx"
#include "data_vol.hxx"
#include "data_atoms.hxx"
#include "op_data.hxx"
#include "math_fns.hxx"
#include "extern_model.hxx"

#ifdef USE_ESCDF
//extern "C" {

//#include "escdf.h"
#include "escdf_error.h"
#include "escdf_system.h"

//}
#endif // ESCDF

// Wulff code call
extern void output_OOGL(const int nnormals, const double normals[][3],
			const double surface_energy[], const int colours[],
			double (* &verts)[3], int &nverts,
			int ** &face_vertices, int * &nface_vertices,
			int &num_faces);

DLV::operation *DLV::load_cube_file::create(const char name[],
					    const char filename[],
					    const bool load_model,
					    const bool set_bonds,
					    char message[], const int_g mlen)
{
  if (load_model)
    return load_cube_model::create(name, filename, set_bonds, message, mlen);
  else
    return load_cube_data::create(filename, message, mlen);
}

DLV::operation *DLV::load_cube_model::create(const char name[],
					     const char filename[],
					     const bool set_bonds,
					     char message[],
					     const int_g mlen)
{
  string model_name = name_from_file(name, filename);
  model *structure = create_atoms(model_name, 0);
  if (structure == 0)
    strncpy(message, "Create model failed", mlen - 1);
  else {
    if (read_model(structure, filename, message, mlen)) {
      load_cube_model *op = new load_cube_model(structure, filename);
      data_object *data = op->read_data(op, 0, filename,
					filename, "", message, mlen);
      if (data == 0) {
	delete op;
	op = 0;
      } else {
	attach_base(op);
	op->attach_data(data);
	if (set_bonds)
	  op->set_bond_all();
      }
      return op;
    } else
      delete structure;
  }
  return 0;
}

DLV::operation *DLV::load_cube_data::create(const char filename[],
					    char message[], const int_g mlen)
{
  load_cube_data *op = new load_cube_data(filename);
  data_object *data = op->read_data(op, 0, filename,
				    filename, "", message, mlen);
  if (data == 0) {
    delete op;
    op = 0;
  } else {
    op->attach();
    op->attach_data(data);
  }
  return op;
}

DLV::cube_file::~cube_file()
{
}

DLV::string DLV::load_cube_data::get_name() const
{
  return ("Load Gaussian cube data - " + get_filename());
}

DLV::string DLV::load_cube_model::get_name() const
{
  return ("Load Gaussian cube model - " + get_filename());
}

DLV::string DLV::load_cube_file::get_label(std::ifstream &input,
					   const string tag, const string id)
{
  char line[128];
  input.getline(line, 128);
  string label = line;
  if (tag.length() > 0) {
    label += tag;
    label += ", ";
    label += id;
  }
  input.getline(line, 128);
  return label;
}

DLV::volume_data *
DLV::load_cube_file::create_data(const string label, operation *op,
				 const int_g nx, const int_g ny, const int_g nz,
				 const real_g origin[3], const real_g astep[3],
				 const real_g bstep[3], const real_g cstep[3])
{
  return new real_space_volume("DLV", label, op, nx, ny, nz,
			       origin, astep, bstep, cstep);
}
		      
// Todo - load various calc data sets into same grid?
DLV::volume_data *DLV::cube_file::read_data(operation *op,
					    volume_data *v,
					    const char filename[],
					    const string id,
					    const string tag,
					    char message[], const int_g mlen)
{
  volume_data *data = 0;
  if (check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (open_file_read(input, filename, message, mlen)) {
      char line[128];
      int_g natoms;
      int_g nx = 0;
      int_g ny = 0;
      int_g nz = 0;
      real_g origin[3];
      real_g astep[3];
      real_g bstep[3];
      real_g cstep[3];
      // read the header
      string label = get_label(input, tag, id);
      // read the grid
      input >> natoms;
      input >> origin[0];
      origin[0] *= bohr_to_angstrom;
      input >> origin[1];
      origin[1] *= bohr_to_angstrom;
      input >> origin[2];
      origin[2] *= bohr_to_angstrom;
      input >> nx;
      input >> astep[0];
      astep[0] *= bohr_to_angstrom;
      input >> astep[1];
      astep[1] *= bohr_to_angstrom;
      input >> astep[2];
      astep[2] *= bohr_to_angstrom;
      input >> ny;
      input >> bstep[0];
      bstep[0] *= bohr_to_angstrom;
      input >> bstep[1];
      bstep[1] *= bohr_to_angstrom;
      input >> bstep[2];
      bstep[2] *= bohr_to_angstrom;
      input >> nz;
      input >> cstep[0];
      cstep[0] *= bohr_to_angstrom;
      input >> cstep[1];
      cstep[1] *= bohr_to_angstrom;
      input >> cstep[2];
      cstep[2] *= bohr_to_angstrom;
      // get end of line
      input.getline(line, 128);
      // skip atom lines
      for (int i = 0; i < natoms; i++)
	input.getline(line, 128);
      // now read the data
      int_l n = (int_l)nx * (int_l)ny * (int_l)nz;
      real_g *array = new real_g[n];
      if (array == 0) {
	strncpy(message, "Unable to allocate memory for data", mlen);
	input.close();
	return 0;
      } else {
	for (int_g i = 0; i < nx; i++) {
	  for (int_g j = 0; j < ny; j++) {
	    for (int_g k = 0; k < nz; k++) {
	      input >> array[i + j * nx + k * nx * ny];
	    }
	  }
	}
      }
      if (v == 0)
	data = create_data(label, op, nx, ny, nz,
			   origin, astep, bstep, cstep);
      else
	data = v;
      data->add_data(array, 1, label.c_str(), false);
      input.close();
    }
  }
  return data;
}

bool DLV::cube_file::read_model(model *structure, const char filename[],
				char message[], const int_g mlen)
{
  bool status = false;
  if (check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (open_file_read(input, filename, message, mlen)) {
      char line[128];
      int_g natoms;
      int_g nx = 0;
      int_g ny = 0;
      int_g nz = 0;
      coord_type origin[3];
      coord_type astep[3];
      coord_type bstep[3];
      coord_type cstep[3];
      // read the header
      input.getline(line, 128);
      input.getline(line, 128);
      //string label = get_label(input, "", "");
      // read the grid
      input >> natoms;
      input >> origin[0];
      origin[0] *= bohr_to_angstrom;
      input >> origin[1];
      origin[1] *= bohr_to_angstrom;
      input >> origin[2];
      origin[2] *= bohr_to_angstrom;
      input >> nx;
      input >> astep[0];
      astep[0] *= bohr_to_angstrom * (nx - 1);
      input >> astep[1];
      astep[1] *= bohr_to_angstrom * (nx - 1);
      input >> astep[2];
      astep[2] *= bohr_to_angstrom * (nx - 1);
      input >> ny;
      input >> bstep[0];
      bstep[0] *= bohr_to_angstrom * (ny - 1);
      input >> bstep[1];
      bstep[1] *= bohr_to_angstrom * (ny - 1);
      input >> bstep[2];
      bstep[2] *= bohr_to_angstrom * (ny - 1);
      input >> nz;
      input >> cstep[0];
      cstep[0] *= bohr_to_angstrom * (nz - 1);
      input >> cstep[1];
      cstep[1] *= bohr_to_angstrom * (nz - 1);
      input >> cstep[2];
      cstep[2] *= bohr_to_angstrom * (nz - 1);
      // get end of line
      input.getline(line, 128);
      DLVreturn_type ok = DLV_OK;
      // Todo - problem if origin is not 0,0,0
      if (structure->get_number_of_periodic_dims() == 3) {
	if (!structure->set_primitive_lattice(astep, bstep, cstep)) {
	  strncpy(message, "Error setting lattice vectors", mlen - 1);
	  ok = DLV_ERROR;
	}
      }
      if (ok == DLV_OK) {
	// read atom lines
	for (int_g i = 0; i < natoms; i++) {
	  coord_type coords[3];
	  coord_type dummy;
	  input.getline(line, 256);
	  int_g atn;
	  if (sscanf(line, "%d %lf %lf %lf %lf", &atn, &dummy, &coords[0],
		     &coords[1], &coords[2]) != 5) {
	    strncpy(message, "Error reading atoms", mlen - 1);
	    ok = DLV_ERROR;
	    break;
	  } else {
	    coords[0] *= bohr_to_angstrom;
	    coords[1] *= bohr_to_angstrom;
	    coords[2] *= bohr_to_angstrom;
	    int_g index;
	    if (!structure->add_cartesian_atom(index, atn, coords)) {
	      strncpy(message, "Error adding atom to model", mlen);
	      ok = DLV_ERROR;
	      break;
	    }
	  }
	}
      }
      if (ok == DLV_OK) {
	structure->complete();
	status = true;
      }
      input.close();
    }
  }
  return status;
}

bool DLV::load_cube_model::reload_data(data_object *data,
				       char message[], const int_g mlen)
{
  volume_data *v = dynamic_cast<volume_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload cube data", mlen);
    return false;
  } else
    return read_data(this, v, get_filename().c_str(), "", "", message, mlen);
}

bool DLV::load_cube_data::reload_data(data_object *data,
				      char message[], const int_g mlen)
{
  volume_data *v = dynamic_cast<volume_data *>(data);
  if (v == 0) {
    strncpy(message, "Incorrect data object to reload cube data", mlen);
    return false;
  } else
    return read_data(this, v, get_filename().c_str(), "", "", message, mlen);
}

DLV::operation *DLV::load_xsf_data::create(const char filename[],
					    char message[], const int_g mlen)
{
  load_xsf_data *op = new load_xsf_data(filename);
  real_data *fermi = 0;
  bool ok = op->read_data(op, get_current_model(), op->md,
			  op->v1, op->v2, op->s1, fermi, false, filename,
			  filename, message, mlen);
  if (ok) {
    op->attach();
    if (op->md != 0)
      op->attach_data(op->md);
    if (op->v1 != 0)
      op->attach_data(op->v1);
    if (op->v2 != 0)
      op->attach_data(op->v2);
    if (op->s1 != 0)
      op->attach_data(op->s1);
    if (fermi != 0)
      op->attach_data(fermi);
  } else {
    delete op;
    op = 0;
  }
  return op;
}

DLV::operation *DLV::load_xsf_model::create(const char name[],
					    const char filename[],
					    const bool set_bonds,
					    bool &new_model, char message[],
					    const int_g mlen)
{
  new_model = true;
  string model_name = name_from_file(name, filename);
  model *structure = read_model(model_name, filename, message, mlen);
  if (structure == 0) {
    if (get_current() == 0)
      strncpy(message, "No model in XSF file", mlen - 1);
    else {
      new_model = false;
      return load_xsf_data::create(filename, message, mlen);
    }
  } else {
    load_xsf_model *op = new load_xsf_model(structure, filename);
    real_data *fermi = 0;
    bool ok = op->read_data(op, structure, op->md, op->v1, op->v2, op->s1,
			    fermi, false, filename, filename,
			    message, mlen);
    if (ok) {
      attach_base(op);
      if (op->md != 0)
	op->attach_data(op->md);
      if (op->v1 != 0)
	op->attach_data(op->v1);
      if (op->v2 != 0)
	op->attach_data(op->v2);
      if (op->s1 != 0)
	op->attach_data(op->s1);
      if (fermi != 0)
	op->attach_data(fermi);
      if (set_bonds)
	op->set_bond_all();
      return op;
    } else {
      delete op;
      op = 0;
    }
  }
  return 0;
}

DLV::string DLV::load_xsf_data::get_name() const
{
  return ("Load Gaussian xsf data - " + get_filename());
}

DLV::string DLV::load_xsf_model::get_name() const
{
  return ("Load XCrySDen xsf model - " + get_filename());
}

void DLV::xsf_file::shift_origin(const int nx, const int ny, const int nz,
				 const real_g origin[3],const real_g astep[3],
				 const real_g bstep[3],const real_g cstep[3],
				 int &mx, int &my, int &mz, real_g o[3])
{
  // Todo - don't assume 0,0,0 for origin?
  // if even number of points then reduce by one and map to just inside +-1/2
  // could map to just outside but test against BZ would then delete it anyway
  mx = 2 * nx - 1;
  my = 2 * ny - 1;
  mz = 2 * nz - 1;
  o[0] = - (astep[0] * real_g(nx - 1) + bstep[0] * real_g(ny - 1)
	    + cstep[0] * real_g(nz - 1));
  o[1] = - (astep[1] * real_g(nx - 1) + bstep[1] * real_g(ny - 1)
		 + cstep[1] * real_g(nz - 1));
  o[2] = - (astep[2] * real_g(nx - 1) + bstep[2] * real_g(ny - 1)
		 + cstep[2] * real_g(nz - 1));
}

DLV::volume_data *DLV::xsf_file::create_bz(const char name[], const string tag,
					   operation *op, const int nx,
					   const int ny, const int nz,
					   const real_g origin[3],
					   const real_g astep[3],
					   const real_g bstep[3],
					   const real_g cstep[3])
{
  // shift origin to -1/2 -1/2 -1/2
  real_g o[3];
  int mx;
  int my;
  int mz;
  shift_origin(nx, ny, nz, origin, astep, bstep, cstep, mx, my, mz, o);
  return new k_space_volume("XSF", tag, op, mx, my, mz, o, astep, bstep, cstep);
}

void DLV::xsf_file::map_bz(real_g * &array, const int nx, const int ny,
			   const int nz, const real_g origin[3],
			   const real_g astep[3], const real_g bstep[3],
			   const real_g cstep[3], const model *structure)
{
  // same mapping as above.
  real_g o[3];
  int mx;
  int my;
  int mz;
  shift_origin(nx, ny, nz, origin, astep, bstep, cstep, mx, my, mz, o);
  int hx = nx - 1;
  int hy = ny - 1;
  int hz = nz - 1;
  // now translate upper parts into other half of gamma point centred 2x2x2.
  // row major for avs [mz][my][mx]
  real_g *data = new real_g[mx * my * mz];
  for (int i = 0; i < nx; i++) {
    for (int j = 0; j < ny; j++) {
      for (int k = 0; k < nz; k++) {
	// data[hz + k][hy + j][hx + i] = array[k][j][i];
	data[(hz + k) * mx * my + (hy + j) * mx + hx + i] =
	  array[k * nx * ny + j * nx + i];
      }
    }
  }
  for (int i = 0; i < hx; i++) {
    for (int j = 0; j < ny; j++) {
      for (int k = 0; k < nz; k++) {
	data[(hz + k) * mx * my + (hy + j) * mx + i] =
	  array[k * nx * ny + j * nx + i];
      }
    }
  }
  for (int i = 0; i < nx; i++) {
    for (int j = 0; j < hy; j++) {
      for (int k = 0; k < nz; k++) {
	data[(hz + k) * mx * my + j * mx + hx + i] =
	  array[k * nx * ny + j * nx + i];
      }
    }
  }
  for (int i = 0; i < nx; i++) {
    for (int j = 0; j < ny; j++) {
      for (int k = 0; k < hz; k++) {
	data[k * mx * my + (hy + j) * mx + hx + i] =
	  array[k * nx * ny + j * nx + i];
      }
    }
  }
  for (int i = 0; i < hx; i++) {
    for (int j = 0; j < hy; j++) {
      for (int k = 0; k < nz; k++) {
	data[(hz + k) * mx * my + j * mx + i] =
	  array[k * nx * ny + j * nx + i];
      }
    }
  }
  for (int i = 0; i < hx; i++) {
    for (int j = 0; j < ny; j++) {
      for (int k = 0; k < hz; k++) {
	data[k * mx * my + (hy + j) * mx + i] =
	  array[k * nx * ny + j * nx + i];
      }
    }
  }
  for (int i = 0; i < nx; i++) {
    for (int j = 0; j < hy; j++) {
      for (int k = 0; k < hz; k++) {
	data[k * mx * my + j * mx + hx + i] =
	  array[k * nx * ny + j * nx + i];
      }
    }
  }
  for (int i = 0; i < hx; i++) {
    for (int j = 0; j < hy; j++) {
      for (int k = 0; k < hz; k++) {
	data[k * mx * my + j * mx + i] =
	  array[k * nx * ny + j * nx + i];
      }
    }
  }
  delete [] array;
  array = data;
  // Todo - this is a copy of generate_k_space_vertices, bad idea
#ifdef ENABLE_DLV_GRAPHICS
  const real_g hide_data = 10000.0 * Hartree_to_eV;
  // Brillouin zone drawing, seems ok for tetragonal, tig/hex, and cubic
  // Untested for Triclinic/Monoclinic.
  // Orthorhombic prim and base seem ok, but not sure about body or face
  // as there are so many complicated shapes involved.
  static real_g hkl[][3] = {
    {  1.0,  0.0,  0.0 },
    { -1.0,  0.0,  0.0 },
    {  0.0,  1.0,  0.0 },
    {  0.0, -1.0,  0.0 },
    {  0.0,  0.0,  1.0 },
    {  0.0,  0.0, -1.0 },
    {  1.0,  1.0,  1.0 },
    { -1.0, -1.0, -1.0 },
    { -1.0,  1.0,  1.0 },
    {  1.0, -1.0,  1.0 },
    {  1.0,  1.0, -1.0 },
    {  1.0, -1.0, -1.0 },
    { -1.0,  1.0, -1.0 },
    { -1.0, -1.0,  1.0 },
    {  1.0,  1.0,  0.0 },
    { -1.0, -1.0,  0.0 },
    { -1.0,  1.0,  0.0 },
    {  1.0, -1.0,  0.0 },
    {  1.0,  0.0,  1.0 },
    { -1.0,  0.0,  1.0 },
    {  1.0,  0.0, -1.0 },
    { -1.0,  0.0, -1.0 },
    {  0.0,  1.0,  1.0 },
    {  0.0, -1.0,  1.0 },
    {  0.0,  1.0, -1.0 },
    {  0.0, -1.0, -1.0 }
  };
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  structure->get_reciprocal_lattice(a, b, c);
  int_g ntemp = 26;
  real_l temp_norms[26][3];
  real_l temp_e[26];
  int_g colours[26];
  for (int_g i = 0; i < ntemp; i++)
    colours[i] = 1;
  real_l min_e = 10000.0;
  for (int_g i = 0; i < ntemp; i++) {
    real_l sum = 0.0;
    for (int_g j = 0; j < 3; j++) {
      temp_norms[i][j] = (a[j] * hkl[i][0]
			  + b[j] * hkl[i][1]
			  + c[j] * hkl[i][2]);
      sum += temp_norms[i][j] * temp_norms[i][j];
    }
    sum = sqrt(sum);
    temp_e[i] = 0.5 * sum;
    if (temp_e[i] < min_e)
      min_e = temp_e[i];
    for (int_g j = 0; j < 3; j++)
      temp_norms[i][j] /= sum;
  }
  // added Sep 07 BGS, orthorhombic 10, 8, 6 seems to suggest we must
  // always include (001) vector set
  int_g nnormals = 0;
  real_l normals[26][3];
  real_l energies[26];
  for (int_g i = 0; i < 6; i++) {
    normals[nnormals][0] = temp_norms[i][0];
    normals[nnormals][1] = temp_norms[i][1];
    normals[nnormals][2] = temp_norms[i][2];
    energies[nnormals] = temp_e[i];
    nnormals++;
  }
  // Sort normals based on length
  const real_l tol = 0.01;
  real_l min2 = 10000.0;
  // Find shortest lattice vectors
  for (int_g i = 0; i < ntemp; i++) {
    if (abs(temp_e[i] - min_e) < tol) {
      if (i >= 6) {
	normals[nnormals][0] = temp_norms[i][0];
	normals[nnormals][1] = temp_norms[i][1];
	normals[nnormals][2] = temp_norms[i][2];
	energies[nnormals] = temp_e[i];
	nnormals++;
      }
    } else if (temp_e[i] < min2)
      min2 = temp_e[i];
  }
  real_l min3 = 10000.0;
  // Include 2nd shortest lengths
  for (int_g i = 0; i < ntemp; i++) {
    if (abs(temp_e[i] - min2) < tol) {
      if (i >= 6) {
	normals[nnormals][0] = temp_norms[i][0];
	normals[nnormals][1] = temp_norms[i][1];
	normals[nnormals][2] = temp_norms[i][2];
	energies[nnormals] = temp_e[i];
	nnormals++;
      }
    } else if ((temp_e[i] < min3) && (temp_e[i] > min2))
      min3 = temp_e[i];
  }
  // fcc, bcc, sc, hex + P trig, R trig a > c seem ok with
  // first 2 lengths. Try 3rd shortest for R trig c > a.
  // Doesn't seem to cause problems! but still not sure whether
  // sqrt(2) c > a or < for Trig R cross over where we expect.
  for (int_g i = 0; i < ntemp; i++) {
    if (abs(temp_e[i] - min3) < tol) {
      if (i >= 6) {
	normals[nnormals][0] = temp_norms[i][0];
	normals[nnormals][1] = temp_norms[i][1];
	normals[nnormals][2] = temp_norms[i][2];
	energies[nnormals] = temp_e[i];
	nnormals++;
      }
    }
  }
  // Call wulffman code to generate object
  int_g nverts = 0, nfaces = 0;
  int_g **face_vertices, *nface_vertices = 0;
  real_l (*verts)[3];
  output_OOGL(nnormals, normals, energies, colours, verts, nverts,
	      face_vertices, nface_vertices, nfaces);
  // now prune points based on whether they are inside or outside planes
  // based on build_wulff_shape for mapping atoms into wulff crystallite
  if (nverts > 0) {
    // for each plane define A,B,C as the first 3 points, then B-A and C-A are
    // vectors in the plane and (B-A)x(C-A) is normal. The origin is inside the
    // plane so if the distance of the origin d (A-O).n is +ve change sign
    // of normal. Then any atom position whose distance from the plane is -ve
    // will be inside the plane (same side as origin)
    // plane has formula (r-a).n = 0
    int nplanes = 0;
    for (int i = 0; i < nfaces; i++)
      if (nface_vertices[i] > 2)
	nplanes++;
    real_g (*points)[3] = new_local_array2(real_g, nplanes, 3);
    int idx = 0;
    for (int i = 0; i < nfaces; i++) {
      if (nface_vertices[i] > 2) {
	points[idx][0] = verts[face_vertices[i][0]][0];
	points[idx][1] = verts[face_vertices[i][0]][1];
	points[idx][2] = verts[face_vertices[i][0]][2];
	idx++;
      }
    }
    real_g (*normals)[3] = new_local_array2(real_g, nplanes, 3);
    idx = 0;
    for (int i = 0; i < nfaces; i++) {
      if (nface_vertices[i] > 2) {
	real_g ba[3];
	ba[0] = verts[face_vertices[i][1]][0] - verts[face_vertices[i][0]][0];
	ba[1] = verts[face_vertices[i][1]][1] - verts[face_vertices[i][0]][1];
	ba[2] = verts[face_vertices[i][1]][2] - verts[face_vertices[i][0]][2];
	real_g ca[3];
	ca[0] = verts[face_vertices[i][2]][0] - verts[face_vertices[i][0]][0];
	ca[1] = verts[face_vertices[i][2]][1] - verts[face_vertices[i][0]][1];
	ca[2] = verts[face_vertices[i][2]][2] - verts[face_vertices[i][0]][2];
	normals[idx][0] = ba[1] * ca[2] - ba[2] * ca[1];
	normals[idx][1] = ba[2] * ca[0] - ba[0] * ca[2];
	normals[idx][2] = ba[0] * ca[1] - ba[1] * ca[0];
	real_g len = sqrt(normals[idx][0] * normals[idx][0]
			  + normals[idx][1] * normals[idx][1]
			  + normals[idx][2] * normals[idx][2]);
	// normalize
	normals[idx][0] /= len;
	normals[idx][1] /= len;
	normals[idx][2] /= len;
	// find distance of origin
	real_g dot = points[idx][0] * normals[idx][0]
	  + points[idx][1] * normals[idx][1] + points[idx][2] * normals[idx][2];
	// should be safe since wulff planes shouldn't go through origin
	if (dot > 0.0) {
	  normals[idx][0] = - normals[idx][0];
	  normals[idx][1] = - normals[idx][1];
	  normals[idx][2] = - normals[idx][2];
	}
	idx++;
      }
    }
    for (int i = 0; i < mx; i++) {
      coord_type xa[3];
      xa[0] = o[0] + astep[0] * coord_type(i);
      xa[1] = o[1] + astep[1] * coord_type(i);
      xa[2] = o[2] + astep[2] * coord_type(i);
      for (int j = 0; j < my; j++) {
	coord_type xb[3];
	xb[0] = xa[0] + bstep[0] * coord_type(j);
	xb[1] = xa[1] + bstep[1] * coord_type(j);
	xb[2] = xa[2] + bstep[2] * coord_type(j);
	for (int k = 0; k < mz; k++) {
	  coord_type xc[3];
	  xc[0] = xb[0] + cstep[0] * coord_type(k);
	  xc[1] = xb[1] + cstep[1] * coord_type(k);
	  xc[2] = xb[2] + cstep[2] * coord_type(k);
	  bool inside = true;
	  // find distance of atom from plane
	  for (int l = 0; l < nplanes; l++) {
	    real_g dot = (points[l][0] - xc[0]) * normals[l][0]
	      + (points[l][1] - xc[1]) * normals[l][1]
	      + (points[l][2] - xc[2]) * normals[l][2];
	    if (dot > 0.0) {
	      inside = false;
	      break;
	    }
	  }
	  if (!inside)
	    data[k * my * mz + j * mx + i] = hide_data;
	}
      }
    }
    delete_local_array(normals);
    delete_local_array(points);
    for (int_g i = 0; i < nfaces; i++)
      if (face_vertices[i] != 0)
	delete [] face_vertices[i];
    delete [] nface_vertices;
  }
#endif // ENABLE_DLV_GRAPHICS
}

bool DLV::xsf_file::read_data(operation *op, model *m, md_trajectory * &md,
			      volume_data * &v1, volume_data * &v2,
			      surface_data * &s1, real_data * &fermi,
			      const bool reload, const char filename[],
			      const string id, char message[], const int_g mlen)
{
  // Todo - forces?
  int vindex = 0;
  int sindex = 0;
  bool ok = true;
  if (check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (open_file_read(input, filename, message, mlen)) {
      char line[128];
      bool finished = false;
      bool in_data = false;
      bool in_fermi = false;
      bool at_start = false;
      int grid_dim = 0;
      int steps = 1;
      string label;
      volume_data *v3 = 0;
      volume_data *b3 = 0;
      surface_data *s2 = 0;
      bool periodic = m->get_number_of_periodic_dims() > 0;
      int natoms = m->get_number_of_asym_atoms(); // no sym asym == prim
      const int_g size = 5000;
      real_g (*p)[size][3] = 0;
      do {
	input.getline(line, 128);
	if (strlen(line) == 0)
	  continue;
	char text[128];
	sscanf(line, "%s", text);
	if (text[0] == '#')
	  ; // a comment
	else if (in_data) {
	  if (strncmp(text, "END_BLOCK_DATAGRID_", 19) == 0) {
	    in_data = false;
	    s2 = 0;
	    v3 = 0;
	  } else if (strncmp(text, "BEGIN_DATAGRID_", 15) == 0) {
	    // Todo - check dim of datagrid?
	    string tag = &text[18];
	    int nx;
	    int ny;
	    int nz;
	    input >> nx;
	    input >> ny;
	    if (grid_dim > 2)
	      input >> nz;
	    else
	      nz = 1;
	    real_g origin[3];
	    real_g astep[3];
	    real_g bstep[3];
	    real_g cstep[3];
	    input >> origin[0];
	    input >> origin[1];
	    input >> origin[2];
	    input >> astep[0];
	    input >> astep[1];
	    input >> astep[2];
	    astep[0] /= (nx - 1);
	    astep[1] /= (nx - 1);
	    astep[2] /= (nx - 1);
	    input >> bstep[0];
	    input >> bstep[1];
	    input >> bstep[2];
	    bstep[0] /= (ny - 1);
	    bstep[1] /= (ny - 1);
	    bstep[2] /= (ny - 1);
	    if (grid_dim > 2) {
	      input >> cstep[0];
	      input >> cstep[1];
	      input >> cstep[2];
	      cstep[0] /= (nz - 1);
	      cstep[1] /= (nz - 1);
	      cstep[2] /= (nz - 1);
	    }
	    if (grid_dim == 2 and s2 == 0) {
	      if (sindex == 1) {
		strncpy(message, "Too many surfaces - truncating", mlen);
		finished = true;
	      } else if (reload) {
		s2 = s1;
		s1->set_loaded();
	      } else {
		if (periodic)
		  s2 = new DLV::rspace_periodic_surface("XSF", label, op, nx,
							ny, origin, astep,
							bstep);
		else
		  s2 = new DLV::real_space_surface("XSF", label, op, nx, ny,
						   origin, astep, bstep);
		s1 = s2;
	      }
	      sindex++;
	    } else if (grid_dim == 3 and v3 == 0) {
	      if (vindex == 2) {
		strncpy(message, "Too many volumes - truncating", mlen);
		finished = true;
	      } else if (reload) {
		if (vindex == 0) {
		  v3 = v1;
		  v1->set_loaded();
		} else if (vindex == 1) {
		  v3 = v2;
		  v2->set_loaded();
		}
	      } else {
		if (periodic)
		  v3 = new DLV::rspace_periodic_volume("XSF", label, op, nx,
						       ny, nz, origin, astep,
						       bstep, cstep);
		else
		  v3 = new DLV::real_space_volume("XSF", label, op, nx, ny, nz,
						  origin, astep, bstep, cstep);
		if (vindex == 0)
		  v1 = v3;
		else if (vindex == 1)
		  v2 = v3;
	      }
	      vindex++;
	    }
	    if (!finished) {
	      int_l n = (int_l)nx * (int_l)ny * (int_l)nz;
	      real_g *array = new real_g[n];
	      if (array == 0) {
		strncpy(message, "Unable to allocate memory for data", mlen);
		finished = true;
	      } else {
		for (int_l i = 0; i < n; i++)
		  input >> array[i];
	      }
	      input.getline(line, 128);
	      input.getline(line, 128);
	      sscanf(line, "%s", text);
	      if (strncmp(text, "END_DATAGRID", 12) != 0) {
		strncpy(message, "Missing end to datagrid", mlen);
		finished = true;
		delete [] array;
	      } else {
		if (grid_dim == 2)
		  s2->add_data(array, tag.c_str(), false);
		else
		  v3->add_data(array, 1, tag.c_str(), false);
	      }
	    }
	  } else if (at_start) {
	    at_start = false;
	    label = text;
	  }
	} else if (strcmp(text, "BEGIN_BLOCK_DATAGRID_2D") == 0) {
	  in_data = true;
	  at_start = true;
	  grid_dim = 2;
	  s2 = 0;
	} else if (strcmp(text, "BEGIN_BLOCK_DATAGRID_3D") == 0) {
	  in_data = true;
	  at_start = true;
	  grid_dim = 3;
	  v3 = 0;
	} else if (in_fermi) {
	  if (strncmp(text, "END_BLOCK_BANDGRID_", 19) == 0)
	    in_fermi = false;
	  else if (strncmp(text, "BEGIN_BANDGRID_", 15) == 0) {
	    string tag = &text[18];
	    int nbands;
	    input >> nbands;
	    int nx;
	    int ny;
	    int nz;
	    input >> nx;
	    input >> ny;
	    input >> nz;
	    real_g origin[3];
	    real_g astep[3];
	    real_g bstep[3];
	    real_g cstep[3];
	    input >> origin[0];
	    input >> origin[1];
	    input >> origin[2];
	    input >> astep[0];
	    input >> astep[1];
	    input >> astep[2];
	    astep[0] /= (nx - 1);
	    astep[1] /= (nx - 1);
	    astep[2] /= (nx - 1);
	    input >> bstep[0];
	    input >> bstep[1];
	    input >> bstep[2];
	    bstep[0] /= (ny - 1);
	    bstep[1] /= (ny - 1);
	    bstep[2] /= (ny - 1);
	    input >> cstep[0];
	    input >> cstep[1];
	    input >> cstep[2];
	    cstep[0] /= (nz - 1);
	    cstep[1] /= (nz - 1);
	    cstep[2] /= (nz - 1);
	    if (b3 == 0) {
	      // shouldn't be possible
	      if (vindex == 2) {
		strncpy(message, "Too many volumes - truncating", mlen);
		finished = true;
	      } else if (reload) {
		if (vindex == 0) {
		  b3 = v1;
		  v1->set_loaded();
		} else if (vindex == 1) {
		  b3 = v2;
		  v2->set_loaded();
		}
	      } else {
		b3 = create_bz("XSF", tag, op, nx, ny, nz,
			       origin, astep, bstep, cstep);
		if (vindex == 0)
		  v1 = b3;
		else if (vindex == 1)
		  v2 = b3;
	      }
	      vindex++;
	    }
	    if (!finished) {
	      input.getline(line, 128);
	      int_l n = (int_l)nx * (int_l)ny * (int_l)nz;
	      for (int nb = 0; (nb < nbands and !finished); nb++) {
		input.getline(line, 128);
		int band_id;
		sscanf(line, "%s %d", text, &band_id); 
		real_g *array = new real_g[n];
		if (array == 0) {
		  strncpy(message, "Unable to allocate memory for data", mlen);
		  finished = true;
		} else {
		  for (int_g i = 0; i < nx; i++) {
		    for (int_g j = 0; j < ny; j++) {
		      for (int_g k = 0; k < nz; k++) {
			input >> array[i + j * nx + k * nx * ny];
		      }
		    }
		  }
		  input.getline(line, 128);
		  snprintf(text, 64, "band %d", band_id);
		  map_bz(array, nx, ny, nz, origin, astep, bstep, cstep, m);
		  b3->add_data(array, 1, text, false);
		}
	      }
	      input.getline(line, 128);
	      sscanf(line, "%s", text);
	      if (strncmp(text, "END_BANDGRID", 12) != 0) {
		strncpy(message, "Missing end to datagrid", mlen);
		finished = true;
		ok = false;
	      }
	    } else if (at_start) {
	      at_start = false;
	      label = text;
	    }
	  }
	} else if (strcmp(text, "BEGIN_BLOCK_BANDGRID_3D") == 0) {
	  in_fermi = true;
	  at_start = true;
	  b3 = 0;
	} else if (strcmp(text, "BEGIN_INFO") == 0) {
	  do {
	    input.getline(line, 128);
	    sscanf(line, "%s", text);
	    if (strcmp(text, "Fermi") == 0) {
	      if (!reload) {
		float fermi_e;
		char buff[64];
		// Fermi Energy: ev
		sscanf(line, "%s %s %f", text, buff, &fermi_e);
		fermi = new real_data("XSF", "bandgrid",
				      "Fermi Energy (eV)", fermi_e);
	      }
	    }
	  } while (strcmp(text, "END_INFO") != 0);
	} else if (strcmp(text, "ANIMSTEPS") == 0) {
	  sscanf(line, "%s %d", text, &steps);
	  p = new real_g[natoms][size][3];
	} else if (strcmp(text, "PRIMCOORD") == 0) {
	  int index = 1;
	  sscanf(line, "%s %d", text, &index);
	  input.getline(line, 128);
	  sscanf(line, "%d", &natoms);
	  for (int_g i = 0; i < natoms; i++) {
	    coord_type coords[3];
	    input.getline(line, 256);
	    char atn[8];
	    if (sscanf(line, "%s %lf %lf %lf", atn, &coords[0],
		       &coords[1], &coords[2]) != 4) {
	      strncpy(message, "Error reading atoms", mlen - 1);
	      finished = true;
	      break;
	    } else if (p != 0) {
	      p[i][index - 1][0] = coords[0];
	      p[i][index - 1][1] = coords[1];
	      p[i][index - 1][2] = coords[2];
	    }
	  }
	} else if (strcmp(text, "ATOMS") == 0) {
	  int index = 1;
	  sscanf(line, "%s %d", text, &index);
	  for (int_g i = 0; i < natoms; i++) {
	    coord_type coords[3];
	    input.getline(line, 256);
	    char atn[8];
	    if (sscanf(line, "%s %lf %lf %lf", atn, &coords[0],
		       &coords[1], &coords[2]) != 4) {
	      strncpy(message, "Error reading atoms", mlen - 1);
	      finished = true;
	      break;
	    } if (p != 0) {
	      p[i][index - 1][0] = coords[0];
	      p[i][index - 1][1] = coords[1];
	      p[i][index - 1][2] = coords[2];
	    }
	  }
	} // else skip keyword.
      } while (!finished and !input.eof());
      input.close();
      if (p != 0) {
	DLV::coord_type (*coords)[3] = new_local_array2(DLV::coord_type,
							natoms, 3);
	for (int_g ip = 0; ip < natoms; ip++)
	  for (int_g kp = 0; kp < 3; kp++)
	    coords[ip][kp] = p[ip][0][kp];
	if (md == 0)
	  md = new DLV::md_trajectory("XSF", "animation", op, steps, 0.0);
	else
	  md->set_loaded();
	md->set_data(&(p[0][0]), coords, natoms, size);
	delete_local_array(coords);
	delete [] p;
      }
    }
  }
  return ok;
}

DLV::model *DLV::xsf_file::read_model(const string name, const char filename[],
				      char message[], const int_g mlen)
{
  model *structure = 0;
  if (check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (open_file_read(input, filename, message, mlen)) {
      DLVreturn_type ok = DLV_OK;
      char line[128];
      bool finished = false;
      int dim = -1;
      int natoms = 0;
      coord_type aprim[3];
      coord_type bprim[3];
      coord_type cprim[3];
      bool has_prim = false;
      coord_type aconv[3];
      coord_type bconv[3];
      coord_type cconv[3];
      bool has_conv = false;
      do {
	input.getline(line, 128);
	if (strlen(line) == 0)
	  continue;
	char text[128];
	sscanf(line, "%s", text);
	if (text[0] == '#')
	  ; // a comment
	else if (strcmp(text, "ATOMS") == 0) {
	  if (structure == 0 or dim == 0) {
	    if (structure == 0)
	      structure = create_atoms(name, 0);
	    int index = 1;
	    bool use_index = sscanf(line, "%s %d", text, &index) == 2;
	    do {
	      coord_type coords[3];
	      input.getline(line, 256);
	      char atsym[8];
	      if (sscanf(line, "%s %lf %lf %lf", atsym, &coords[0],
			 &coords[1], &coords[2]) != 4) {
		finished = true;
		break;
	      } else if (!use_index or (use_index and index == 1)) {
		bool fail = false;
		int_g index;
		if (isdigit(atsym[0])) {
		  fail = !structure->add_cartesian_atom(index, atoi(atsym),
							coords);
		} else {
		  fail = !structure->add_cartesian_atom(index, atsym, coords);
		}
		if (fail) {
		  strncpy(message, "Error adding atom to model", mlen);
		  ok = DLV_ERROR;
		  finished = true;
		  break;
		}
	      }
	    } while (!finished);
	    // we finish since once we've read the atoms the molecule ends.
	  } else {
	    strncpy(message, "Multiple structures in file", mlen);
	    ok = DLV_ERROR;
	    finished = true;
	  }
	} else if (strcmp(text, "MOLECULE") == 0) {
	  dim = 0;
	  if (structure == 0)
	    structure = create_atoms(name, 0);
	  else {
	    strncpy(message, "Multiple structures in file", mlen);
	    ok = DLV_ERROR;
	    finished = true;
	  }
	} else if (strcmp(text, "POLYMER") == 0) {
	  dim = 1;
	  if (structure == 0)
	    structure = create_atoms(name, 1);
	  else {
	    strncpy(message, "Multiple structures in file", mlen);
	    ok = DLV_ERROR;
	    finished = true;
	  }
	} else if (strcmp(text, "SLAB") == 0) {
	  dim = 2;
	  if (structure == 0)
	    structure = create_atoms(name, 2);
	  else {
	    strncpy(message, "Multiple structures in file", mlen);
	    ok = DLV_ERROR;
	    finished = true;
	  }
	} else if (strcmp(text, "CRYSTAL") == 0) {
	  dim = 3;
	  if (structure == 0)
	    structure = create_atoms(name, 3);
	  else {
	    strncpy(message, "Multiple structures in file", mlen);
	    ok = DLV_ERROR;
	    finished = true;
	  }
	} else if (strcmp(text, "PRIMVEC") == 0) {
	  int index = 1;
	  bool use_index = sscanf(line, "%s %d", text, &index) == 2;
	  input >> aprim[0];
	  input >> aprim[1];
	  input >> aprim[2];
	  input >> bprim[0];
	  input >> bprim[1];
	  input >> bprim[2];
	  input >> cprim[0];
	  input >> cprim[1];
	  input >> cprim[2];
	  input.getline(line, 128);
	  if (!use_index or (use_index and index == 1)) {
	    if (dim > 0)
	      structure->set_primitive_lattice(aprim, bprim, cprim);
	    has_prim = true;
	  } else
	    strncpy(message, "Animated cells not currently supported", mlen);
	} else if (strcmp(text, "CONVVEC") == 0) {
	  int index = 1;
	  bool use_index = sscanf(line, "%s %d", text, &index) == 2;
	  input >> aconv[0];
	  input >> aconv[1];
	  input >> aconv[2];
	  input >> bconv[0];
	  input >> bconv[1];
	  input >> bconv[2];
	  input >> cconv[0];
	  input >> cconv[1];
	  input >> cconv[2];
	  input.getline(line, 128);
	  if (!use_index or (use_index and index == 1)) {
	    has_conv = true;
	  } else
	    strncpy(message, "Animated cells not currently supported", mlen);
	} else if (strcmp(text, "PRIMCOORD") == 0) {
	  int index = 1;
	  bool use_index = sscanf(line, "%s %d", text, &index) == 2;
	  input.getline(line, 128);
	  sscanf(line, "%d", &natoms);
	  for (int_g i = 0; i < natoms; i++) {
	    coord_type coords[3];
	    input.getline(line, 256);
	    char atsym[8];
	    if (sscanf(line, "%s %lf %lf %lf", atsym, &coords[0],
		       &coords[1], &coords[2]) != 4) {
	      strncpy(message, "Error reading atoms", mlen - 1);
	      ok = DLV_ERROR;
	      finished = true;
	      break;
	    } else if (!use_index or (use_index and index == 1)) {
	      bool fail = false;
	      int_g index;
	      if (isdigit(atsym[0])) {
		fail = !structure->add_cartesian_atom(index, atoi(atsym),
						      coords);
	      } else {
		fail = !structure->add_cartesian_atom(index, atsym, coords);
	      }
	      if (fail) {
		strncpy(message, "Error adding atom to model", mlen);
		ok = DLV_ERROR;
		finished = true;
		break;
	      }
	    }
	  }
	} else if (strcmp(text, "CONVCOORD") == 0) {
	  int index = 1;
	  bool use_index = sscanf(line, "%s %d", text, &index) == 2;
	  int n = 0;
	  input.getline(line, 128);
	  sscanf(line, "%d", &n);
	  for (int_g i = 0; i < n; i++) {
	    input.getline(line, 256);
	    // only use if we haven't seen any primitive coords
	    if (natoms == 0) {
	      coord_type coords[3];
	      char atsym[8];
	      if (sscanf(line, "%s %lf %lf %lf", atsym, &coords[0],
			 &coords[1], &coords[2]) != 4) {
		strncpy(message, "Error reading atoms", mlen - 1);
		ok = DLV_ERROR;
		finished = true;
		break;
	      } else if (!use_index or (use_index and index == 1)) {
		bool fail = false;
		int_g index;
		if (isdigit(atsym[0])) {
		  fail = !structure->add_cartesian_atom(index, atoi(atsym),
							coords);
		} else {
		  fail = !structure->add_cartesian_atom(index, atsym, coords);
		}
		if (fail) {
		  strncpy(message, "Error adding atom to model", mlen);
		  ok = DLV_ERROR;
		  finished = true;
		  break;
		}
	      }
	    }
	  }
	} else if (strncmp(text, "BEGIN", 5) == 0)
	  finished = true;
      } while (!finished and !input.eof());
      input.close();
      if (dim > 0) {
	if (has_conv and !has_prim)
	  structure->set_primitive_lattice(aconv, bconv, cconv);
	else if (has_conv and has_prim) {
	  if (dim > 1) {
	    // CV = T PV => T = CV PV-1
	    real_l conv[3][3];
	    conv[0][0] = aconv[0];
	    conv[0][1] = aconv[1];
	    conv[0][2] = aconv[2];
	    conv[1][0] = bconv[0];
	    conv[1][1] = bconv[1];
	    conv[1][2] = bconv[2];
	    conv[2][0] = cconv[0];
	    conv[2][1] = cconv[1];
	    conv[2][2] = cconv[2];
	    real_l prim[3][3];
	    prim[0][0] = aprim[0];
	    prim[0][1] = aprim[1];
	    prim[0][2] = aprim[2];
	    prim[1][0] = bprim[0];
	    prim[1][1] = bprim[1];
	    prim[1][2] = bprim[2];
	    prim[2][0] = cprim[0];
	    prim[2][1] = cprim[1];
	    prim[2][2] = cprim[2];
	    real_l inv[3][3];
	    matrix_invert(prim, inv, dim);
	    real_l t[3][3];
	    mat_nn_mult(conv, inv, t, dim);
	    // identify transformations
	    const real_l tol = 0.001;
	    if (dim == 2) {
	      if (abs(t[0][0] - 1.0) < tol and abs(t[0][1] + 1.0) < tol and
		  abs(t[1][0] - 1.0) < tol and abs(t[1][1] - 1.0) < tol)
		structure->set_lattice_and_centre(0, 1);
	    } else {
	      int centre = 0;
	      int lattice = 0;
	      if (abs(t[0][0] - 1.0) < tol and abs(t[0][1]) < tol and
		  abs(t[0][2]) < tol and abs(t[1][0]) < tol and
		  abs(t[1][1] - 1.0) < tol and abs(t[1][2] + 1.0) < tol and
		  abs(t[2][0]) < tol and abs(t[2][1] - 1.0) < tol and
		  abs(t[2][2] - 1.0) < tol)
		centre = 1; // a_face
	      else if (abs(t[0][0] - 1.0) < tol and abs(t[0][1]) < tol and
		       abs(t[0][2] - 1.0) < tol and abs(t[1][0]) < tol and
		       abs(t[1][1] - 1.0) < tol and abs(t[1][2]) < tol and
		       abs(t[2][0] + 1.0) < tol and abs(t[2][1]) < tol and
		       abs(t[2][2] - 1.0) < tol)
		centre = 2; // b_face
	      else if (abs(t[0][0] - 1.0) < tol and abs(t[0][1] + 1.0) < tol and
		       abs(t[0][2]) < tol and abs(t[1][0] - 1.0) < tol and
		       abs(t[1][1] - 1.0) < tol and abs(t[1][2]) < tol and
		       abs(t[2][0]) < tol and abs(t[2][1]) < tol and
		       abs(t[2][2] - 1.0) < tol)
		centre = 3; // c_face
	      else if (abs(t[0][0] + 1.0) < tol and abs(t[0][1] - 1.0) < tol and
		       abs(t[0][2] - 1.0) < tol and abs(t[1][0] - 1.0) < tol and
		       abs(t[1][1] + 1.0) < tol and abs(t[1][2] - 1.0) < tol and
		       abs(t[2][0] - 1.0) < tol and abs(t[2][1] - 1.0) < tol and
		       abs(t[2][2] + 1.0) < tol)
		centre = 4; // face
	      else if (abs(t[0][0]) < tol and abs(t[0][1] - 1.0) < tol and
		       abs(t[0][2] - 1.0) < tol and abs(t[1][0] - 1.0) < tol and
		       abs(t[1][1]) < tol and abs(t[1][2] - 1.0) < tol and
		       abs(t[2][0] - 1.0) < tol and abs(t[2][1] - 1.0) < tol and
		       abs(t[2][2]) < tol)
		centre = 5; // body
	      else if (abs(t[0][0] - 1.0) < tol and abs(t[0][1] + 1.0) < tol and
		       abs(t[0][2]) < tol and abs(t[1][0]) < tol and
		       abs(t[1][1] - 1.0) < tol and abs(t[1][2] + 1.0) < tol and
		       abs(t[2][0] - 1.0) < tol and abs(t[2][1] - 1.0) < tol and
		       abs(t[2][2] - 1.0) < tol) {
		centre = 6; // rhomb
		lattice = 4;
	      }
	      structure->set_lattice_and_centre(lattice, centre);
	    }
	  }
	}
      }
      if (ok == DLV_OK and structure != 0)
	structure->complete();
    }
  }
  return structure;
}

bool DLV::load_xsf_model::reload_data(data_object *data,
				       char message[], const int_g mlen)
{
  //volume_data *v = dynamic_cast<volume_data *>(data);
  //if (v == 0) {
  //  strncpy(message, "Incorrect data object to reload xsf data", mlen);
  //  return false;
  //} else
  real_data *fermi = 0;
  return read_data(this, get_model(), md, v1, v2, s1, fermi, true,
		   get_filename().c_str(), "", message, mlen);
}

bool DLV::load_xsf_data::reload_data(data_object *data,
				      char message[], const int_g mlen)
{
  //volume_data *v = dynamic_cast<volume_data *>(data);
  //if (v == 0) {
  //  strncpy(message, "Incorrect data object to reload xsf data", mlen);
  //  return false;
  //} else
  real_data *fermi = 0;
  return read_data(this, get_model(), md, v1, v2, s1, fermi, true,
		   get_filename().c_str(), "", message, mlen);
}

#ifdef USE_ESCDF
DLV::operation *DLV::load_escdf_file::create(const char name[],
					     const char filename[],
					     const bool load_model,
					     const bool set_bonds,
					     char message[], const int_g mlen)
{
  //if (load_model)
    return load_escdf_model::create(name, filename, set_bonds, message, mlen);
    //else
    //return load_escdf_data::create(filename, message, mlen);
}

DLV::operation *DLV::load_escdf_model::create(const char name[],
					      const char filename[],
					      const bool set_bonds,
					      char message[],
					      const int_g mlen)
{
  string model_name = name_from_file(name, filename);
  model *structure = read_model(model_name, filename, message, mlen);
  if (structure != 0) {
    load_escdf_model *op = new load_escdf_model(structure, filename);
    /*
      data_object *data = op->read_data(op, 0, filename,
      filename, "", message, mlen);
    */
    /*
      if (data == 0) {
      delete op;
      op = 0;
      } else {
    */
    attach_base(op);
    //op->attach_data(data);
    if (set_bonds)
      op->set_bond_all();
    //}
    return op;
  }
  return 0;
}

DLV::escdf_file::~escdf_file()
{
}

DLV::string DLV::load_escdf_model::get_name() const
{
  return ("Load ESCDF model - " + get_filename());
}

DLV::string DLV::load_escdf_file::get_label(std::ifstream &input,
					    const string tag, const string id)
{
  char line[128];
  input.getline(line, 128);
  string label = line;
  if (tag.length() > 0) {
    label += tag;
    label += ", ";
    label += id;
  }
  input.getline(line, 128);
  return label;
}

DLV::model *DLV::escdf_file::read_model(const string name,
					const char filename[], char message[],
					const int_g mlen)
{
  model *structure = 0;
  if (check_filename(filename, message, mlen)) {
    escdf_handle_t *input = escdf_open(filename, 0);
    if (escdf_error_get_last(0) == ESCDF_SUCCESS) {
      //escdf_system_t *geom = escdf_system_open(input, 0); // _R
      escdf_system_t *geom = escdf_system_open(input, "Silicon"); // _A
      if (escdf_error_get_last(0) == ESCDF_SUCCESS) {
	if (escdf_system_read_metadata(geom) != ESCDF_SUCCESS) {
	  strncpy(message, "Unable to read geometry metadata", mlen - 1);
	} else {
	  // Should we get number_of_physical_dimensions - mandatory 3 so why?
	  bool ok = true;
	  unsigned int ndims = 3;
	  //if (!escdf_system_is_set_number_of_physical_dimensions(geom))
	  //strncpy(message, "Assuming number of physical dimensions == 3",
	  //  mlen - 1);
	  if (escdf_system_get_number_of_physical_dimensions(geom, &ndims) !=
	      ESCDF_SUCCESS)
	    strncpy(message, "Couldn't get number of dimensions", mlen - 1);
	  if (ndims != 3) {
	    strncpy(message, "Physical dimensions != 3 unsupported", mlen - 1);
	    ok = false;
	  }
	  if (ok) {
	    ok = true;
	    int dims[3];
	    if (escdf_system_get_dimension_types(geom,
						 dims) == ESCDF_SUCCESS) {
	      //fprintf(stderr, "Read dims\n");
	      int periodic = 0;
	      int semi_infinite = 0;
	      for (int i = 0; i < 3; i++) {
		if (dims[i] == 1)
		  periodic++;
		else if (dims[i] == 2)
		  semi_infinite++;
	      }
	      if (semi_infinite > 0) {
		strncpy(message, "Semi-infinite systems unsupported", mlen - 1);
		ok = false;
	      } else {
		// Is there an issue with which dims are periodic???
		//fprintf(stderr, "Get system name for model name...\n");
		string model_name = name;
		structure = create_atoms(model_name, periodic);
		if (structure == 0) {
		  strncpy(message, "Create model failed", mlen - 1);
		  ok = false;
		} else {
		  double lattice[3][3];
		  escdf_errno_t err = escdf_system_get_lattice_vectors(geom,
								       (double *)lattice);
		  if (err != ESCDF_SUCCESS) {
		    strncpy(message, "Error getting lattice vectors", mlen - 1);
		    ok = false;
		  }
		  if (ok) {
		    DLV::coord_type va[3];
		    va[0] = lattice[0][0];
		    va[0] = lattice[0][1];
		    va[0] = lattice[0][2];
		    DLV::coord_type vb[3];
		    vb[0] = lattice[1][0];
		    vb[0] = lattice[1][1];
		    vb[0] = lattice[1][2];
		    DLV::coord_type vc[3];
		    vc[0] = lattice[2][0];
		    vc[0] = lattice[2][1];
		    vc[0] = lattice[2][2];
		    if (!structure->set_primitive_lattice(va, vb, vc)) {
		      strncpy(message, "Problem setting lattice", mlen - 1);
		      ok = false;
		    }
		    // Todo - symmetry?
		    unsigned int nspecies = 0;
		    if (ok) {
		      err = escdf_system_get_number_of_species(geom, &nspecies);
		      if (err != ESCDF_SUCCESS) {
			strncpy(message, "Failed to get number of species",
				mlen - 1);
			ok = false;
		      }
		    }
		    unsigned int nsites = 0;
		    if (ok) {
		      err = escdf_system_get_number_of_sites(geom, &nsites);
		      if (err != ESCDF_SUCCESS) {
			strncpy(message, "Failed to get number of sites",
				mlen - 1);
			ok = false;
		      }
		    }
		    if (ok) {
		      // Todo
		    }
		  }
		}
	      }
	    } else {
	      strncpy(message, "Couldn't get ESCDF dims", mlen - 1);
	      ok = false;
	    }
	    if (ok) {
	      /*fprintf(stderr, "Geometry ok - %p\n", structure)*/;
	      structure->complete();
	    } else if (structure != 0) {
	      fprintf(stderr, "Delete\n");
	      delete structure;
	      structure = 0;
	    }
	  }
	}
	escdf_system_close(geom);
      } else
	strncpy(message, "Failure to find ESCDF geometry", mlen - 1);
      escdf_close(input);
      escdf_error_free();
    } else
      strncpy(message, "Error opening ESCDF file", mlen - 1);
  }
  return structure;
}
#endif // ESCDF

#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::load_cube_data *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::load_cube_data("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::load_cube_model *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::load_cube_model(0, "recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::load_xsf_model *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::load_xsf_model(0, "recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::load_xsf_data *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::load_xsf_data("recover");
    }

#  ifdef USE_ESCDF
    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::load_escdf_model *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::load_escdf_model(0, "recover");
    }
#  endif // ESCDF

  }
}

template <class Archive>
void DLV::load_cube_model::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<load_atom_model_op>(*this);
}

template <class Archive>
void DLV::load_cube_data::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<load_data_op>(*this);
}

template <class Archive>
void DLV::load_xsf_model::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<load_atom_model_op>(*this);
  ar & md;
  ar & v1;
  ar & v2;
  ar & s1;
}

template <class Archive>
void DLV::load_xsf_data::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<load_data_op>(*this);
  ar & md;
  ar & v1;
  ar & v2;
  ar & s1;
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::load_cube_model)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::load_cube_data)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::load_xsf_model)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::load_xsf_data)

DLV_SUPPRESS_TEMPLATES(DLV::load_atom_model_op)
DLV_SUPPRESS_TEMPLATES(DLV::load_data_op)
DLV_SUPPRESS_TEMPLATES(DLV::surface_data)
DLV_SUPPRESS_TEMPLATES(DLV::volume_data)
DLV_SUPPRESS_TEMPLATES(DLV::md_trajectory)

DLV_NORMAL_EXPLICIT(DLV::load_cube_model)
DLV_NORMAL_EXPLICIT(DLV::load_cube_data)
DLV_NORMAL_EXPLICIT(DLV::load_xsf_model)
DLV_NORMAL_EXPLICIT(DLV::load_xsf_data)

#  ifdef USE_ESCDF

template <class Archive>
void DLV::load_escdf_model::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<load_atom_model_op>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::load_escdf_model)

DLV_NORMAL_EXPLICIT(DLV::load_escdf_model)

#  endif // ESCDF

#endif // DLV_USES_SERIALIZE
