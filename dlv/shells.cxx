
#include <list>
#include <map>
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/toolkit_obj.hxx"
#  include "../graphics/render_base.hxx"
#  include "../graphics/model_render.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "utils.hxx"
#include "model.hxx"
//#include "symmetry.hxx"
#include "atom_prefs.hxx"
#include "atom_model.hxx"
#include "shells.hxx"

DLV::shell_model::~shell_model()
{
#ifdef ENABLE_DLV_GRAPHICS
  delete render_object;
#endif // ENABLE_DLV_GRAPHICS
}

DLV::model *DLV::shell_model::duplicate_model(const string label) const
{
  shell_model *m = new shell_model(label);
  m->central_atom = central_atom;
  m->shells = shells;
  return m;
}

bool DLV::shell_model::is_shell() const
{
  return true;
}

// Dummy functions required by virtual in model (Todo - better way?)
// An excurv calculation with symmetry is a cluster not a shell model.
bool DLV::shell_model::set_group(const int_g)
{
  return false;
}

bool DLV::shell_model::set_group(const char [], const int_g,
				 const int_g, const bool)
{
  return false;
}

bool DLV::shell_model::set_cartesian_sym_ops(const real_l [][3][3],
					     const real_l [][3], const int_g)
{
  return false;
}

bool DLV::shell_model::set_fractional_sym_ops(const real_l [][3][3],
					      const real_l [][3],
					      const int_g)
{
  return false;
}

bool DLV::shell_model::set_primitive_lattice(const coord_type [3],
					     const coord_type [3],
					     const coord_type [3])
{
  return false;
}

bool DLV::shell_model::set_lattice_parameters(const coord_type,
					      const coord_type,
					      const coord_type,
					      const coord_type,
					      const coord_type,
					      const coord_type)
{
  return false;
}

bool DLV::shell_model::add_cartesian_atom(int_g &index,
					  const int_g atomic_number,
					  const coord_type coords[3])
{
  if (atomic_number > 0 and atomic_number <= max_atomic_number) {
    central_atom = atom(new atom_type());
    central_atom->set_atomic_number(atomic_number);
    central_atom->set_cartesian_coords(coords);
    index = 0;
    return true;
  } else {
    return false;
  }
}

bool DLV::shell_model::add_fractional_atom(int_g &, const int_g,
					   const coord_type [3],
					   const bool, const bool)
{
  return false;
}

bool DLV::shell_model::add_cartesian_atom(int_g &, const char [],
					  const coord_type [3])
{
  return false;
}

bool DLV::shell_model::add_fractional_atom(int_g &, const char [],
					   const coord_type [3],
					   const bool, const bool)
{
  return false;
}

void DLV::shell_model::add_shells(const int_g types[], const real_g numbers[],
				  const real_g radii[], const int_g nshells)
{
  for (int_g i = 0; i < nshells; i++) {
    shell_data d;
    d.atomic_number = types[i];
    d.number_of_atoms = numbers[i];
    d.radius = radii[i];
    shells.push_back(d);
  }
}

void DLV::shell_model::update_shells(const int_g types[],
				     const real_g numbers[],
				     const real_g radii[], const int_g nshells)
{
  int_g n = (int)shells.size();
  std::list<shell_data>::iterator ptr = shells.begin();
  for (int_g i = 0; i < nshells; i++) {
    if (i < n) {
      ptr->atomic_number = types[i];
      ptr->number_of_atoms = numbers[i];
      ptr->radius = radii[i];
    } else {
      shell_data d;
      d.atomic_number = types[i];
      d.number_of_atoms = numbers[i];
      d.radius = radii[i];
      shells.push_back(d);
    }
  }
  if (n > nshells) {
    for (int_g i = nshells; i < n; i++)
      shells.pop_back();
  }
}

DLV::int_g DLV::shell_model::get_number_of_periodic_dims() const
{
  return 0;
}

DLV::int_g DLV::shell_model::get_lattice_type() const
{
  return 0;
}

DLV::int_g DLV::shell_model::get_lattice_centring() const
{
  return 0;
}

// Kind of the dimensionality - for the User Interface menus.
DLV::int_g DLV::shell_model::get_model_type() const
{
  return 7;
}

void DLV::shell_model::set_crystal03_lattice_type(const int_g, const int_g)
{
  // Todo - a bug
}

void DLV::shell_model::get_primitive_lattice(coord_type a[3], coord_type b[3],
					     coord_type c[3]) const
{
  // Todo - BUG
  a[0] = 0.0;
  a[1] = 0.0;
  a[2] = 0.0;
  b[0] = 0.0;
  b[1] = 0.0;
  b[2] = 0.0;
  c[0] = 0.0;
  c[1] = 0.0;
  c[2] = 0.0;
}

void DLV::shell_model::get_conventional_lattice(coord_type a[3],
						coord_type b[3],
						coord_type c[3]) const
{
  get_primitive_lattice(a, b, c);
}

void DLV::shell_model::get_reciprocal_lattice(coord_type a[3],
					      coord_type b[3],
					      coord_type c[3]) const
{
  get_primitive_lattice(a, b, c);
}

DLV::int_g DLV::shell_model::get_hermann_mauguin_group(string &name) const
{
  name = "P1";
  return 1;
}

void DLV::shell_model::get_point_group(string &base, int_g &n,
				       string &tail) const
{
  base = "c";
  n = 1;
  tail = "";
}

DLV::int_g DLV::shell_model::get_number_of_sym_ops() const
{
  return 1;
}

void DLV::shell_model::get_cart_rotation_operators(real_l r[][3][3],
						   const int_g) const
{
  for (int_g i = 0; i < 3; i++)
    for (int_g j = 0; j < 3; j++)
      r[0][i][j] = 0.0;
  r[0][0][0] = 1.0;
  r[0][1][1] = 1.0;
  r[0][2][2] = 1.0;
}

void DLV::shell_model::get_frac_rotation_operators(real_l r[][3][3],
						   const int_g) const
{
  for (int_g i = 0; i < 3; i++)
    for (int_g j = 0; j < 3; j++)
      r[0][i][j] = 0.0;
  r[0][0][0] = 1.0;
  r[0][1][1] = 1.0;
  r[0][2][2] = 1.0;
}

void DLV::shell_model::get_cart_translation_operators(real_l t[][3],
						      const int_g) const
{
  for (int_g i = 0; i < 3; i++)
    t[0][i] = 0.0;
}

void DLV::shell_model::get_frac_translation_operators(real_l t[][3],
						      const int_g) const
{
  for (int_g i = 0; i < 3; i++)
    t[0][i] = 0.0;
}

DLV::int_g DLV::shell_model::get_number_of_asym_atoms() const
{
  return 0;
}

DLV::int_g DLV::shell_model::get_number_of_primitive_atoms() const
{
  return 0;
}

void DLV::shell_model::get_asym_atom_types(int_g atom_types[],
					   const int_g n) const
{
  // Todo - get central atom?
  if (n > 0)
    atom_types[0] = 0;
}

void DLV::shell_model::get_primitive_atom_types(int_g atom_types[],
						const int_g n) const
{
  if (n > 0)
    atom_types[0] = 0;
}

void DLV::shell_model::get_asym_atom_cart_coords(coord_type coords[][3],
						 const int_g n) const
{
  if (n > 0) {
    coords[0][0] = 0.0;
    coords[0][1] = 0.0;
    coords[0][2] = 0.0;
  }
}

void DLV::shell_model::get_asym_atom_frac_coords(coord_type coords[][3],
						 const int_g n) const
{
  if (n > 0) {
    coords[0][0] = 0.0;
    coords[0][1] = 0.0;
    coords[0][2] = 0.0;
  }
}

void DLV::shell_model::get_primitive_atom_cart_coords(coord_type coords[][3],
						      const int_g n) const
{
  if (n > 0) {
    coords[0][0] = 0.0;
    coords[0][1] = 0.0;
    coords[0][2] = 0.0;
  }
}

void DLV::shell_model::get_primitive_atom_frac_coords(coord_type coords[][3],
						      const int_g n) const
{
  if (n > 0) {
    coords[0][0] = 0.0;
    coords[0][1] = 0.0;
    coords[0][2] = 0.0;
  }
}

void DLV::shell_model::generate_transforms(const int_g, const int_g,
					   const int_g, const int_g,
					   const int_g, const int_g,
					   const bool, const bool,
					   int_g &ntransforms,
					   real_g (* &transforms)[3]) const
{
  ntransforms = 1;
  transforms = new real_g[1][3];
  transforms[0][0] = 0.0;
  transforms[0][1] = 0.0;
  transforms[0][2] = 0.0;
}

void DLV::shell_model::complete(const bool)
{
  // Todo - not sure what this does for a shell model
}

#ifdef ENABLE_DLV_GRAPHICS

DLV::render_parent *DLV::shell_model::create_render_parent(render_parent *p,
							   const bool,
							   const bool)
{
  return new render_shells(get_model_name().c_str());
}

DLVreturn_type DLV::shell_model::render_r(render_parent *p, const model *m,
					  char message[], const int_g mlen)
{
  if (render_object == 0)
    render_object = new model_shell_r(p);
  return update_shells(p, message, mlen);
}

DLVreturn_type DLV::shell_model::update_shells(render_parent *p,
					       char message[], const int_g mlen)
{
  colour_data c;
  central_atom->get_colour(c);
  real_g colour[3];
  colour[0] = c.red;
  colour[1] = c.green;
  colour[2] = c.blue;
  int_g size = (int)shells.size();
  real_g *radii = 0;
  real_g (*cdata)[3] = 0;
  if (size > 0) {
    radii = new_local_array1(real_g, size);
    cdata = new_local_array2(real_g, size, 3);
    std::list<shell_data>::const_iterator p;
    int_g i = 0;
    for (p = shells.begin(); p != shells.end(); ++p ) {
      radii[i] = p->radius;
      atomic_data->find_colour(c, p->atomic_number);
      cdata[i][0] = c.red;
      cdata[i][1] = c.green;
      cdata[i][2] = c.blue;
    }
  }
  render_object->draw(colour, size, radii, cdata);
  if (size > 0) {
    delete_local_array(cdata);
    delete_local_array(radii);
  }
  return DLV_OK;
}

DLVreturn_type DLV::shell_model::render_k(render_parent *p,
					  char message[], const int_g mlen)
{
  strncpy(message, "BUG - Shell model k space rendering", mlen);
  return DLV_ERROR;
}

DLV::toolkit_obj DLV::shell_model::get_r_ui_obj() const
{
  return toolkit_obj();
}

DLVreturn_type DLV::shell_model::update_cell(render_parent *p,
					     char message[], const int_g mlen)
{
  strncpy(message, "BUG: Shell model cell update called", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::shell_model::update_lattice(render_parent *p,
						const bool kspace,
						char message[],
						const int_g mlen)
{
  strncpy(message, "BUG: Shell model lattice update called", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::shell_model::update_atoms(const render_parent *p,
					      char message[], const int_g mlen)
{
  strncpy(message, "BUG: Shell model atom update called", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::shell_model::update_bonds(const render_parent *p,
					      char message[],
					      const int_g mlen) const
{
  strncpy(message, "BUG: Shell model bond update called", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::shell_model::add_bond(render_parent *p,
					  char message[], const int_g mlen)
{
  strncpy(message, "BUG: Shell model bond update called", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::shell_model::del_bond(render_parent *p,
					  char message[], const int_g mlen)
{
  strncpy(message, "BUG: Shell model bond update called", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::shell_model::add_bond_type(render_parent *p,
					       char message[], const int_g mlen)
{
  strncpy(message, "BUG: Shell model bond update called", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::shell_model::del_bond_type(render_parent *p,
					       char message[], const int_g mlen)
{
  strncpy(message, "BUG: Shell model bond update called", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::shell_model::add_bond_all(render_parent *p,
					      char message[], const int_g mlen)
{
  strncpy(message, "BUG: Shell model bond update called", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::shell_model::del_bond_all(render_parent *p,
					      char message[], const int_g mlen)
{
  strncpy(message, "BUG: Shell model bond update called", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::shell_model::select_atom(render_parent *p,
					     const real_g coords[3],
					     const int_g status,
					     int_g &nselects,
					     char message[], const int_g mlen)
{
  nselects = 0;
  strncpy(message, "BUG: Shell model select atom called", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::shell_model::deselect_all_atoms(render_parent *p,
						    char message[],
						    const int_g mlen)
{
  strncpy(message, "BUG: Shell model deselect atom called", mlen);
  return DLV_ERROR;
}

#endif // ENABLE_DLV_GRAPHICS


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::shell_model *t,
				    const unsigned int file_version)
    {
      DLV::string n = t->get_model_name();
      ar << n;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::shell_model *t,
				    const unsigned int file_version)
    {
      DLV::string n;
      ar >> n;
      ::new(t)DLV::shell_model(n);
    }

  }
}

template <class Archive>
void DLV::shell_data::serialize(Archive &ar, const unsigned int version)
{
  ar & atomic_number;
  ar & number_of_atoms;
  ar & radius;
}

template <class Archive>
void DLV::shell_model::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<model>(*this);
  ar & central_atom;
  ar & shells;
#ifdef ENABLE_DLV_GRAPHICS
  ar & render_object;
#endif // ENABLE_DLV_GRAPHICS
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::shell_model)

DLV_SUPPRESS_TEMPLATES(DLV::model)
#ifdef ENABLE_DLV_GRAPHICS
DLV_SUPPRESS_TEMPLATES(DLV::model_shell_r)
#endif // ENABLE_DLV_GRAPHICS

DLV_NORMAL_EXPLICIT(DLV::shell_model)

#endif // DLV_USES_SERIALIZE
