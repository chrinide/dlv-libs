
#ifndef DLV_OPERATION
#define DLV_OPERATION

#include <list>
#include <map>

#ifdef DLV_USES_SERIALIZE
  #include <boost/serialization/list.hpp>
  #include <boost/serialization/map.hpp>
#endif // DLV_USES_SERIALIZE

// inputs, operations and outputs as nodes in project 'graph'
namespace DLV {

  // MSVC requires forward declaration.
  class data_object;
  class model;
  class job;
  class file_obj;

  typedef std::map<int, class operation *> op_list;
  typedef bool (operation::* op_test)() const;

  extern "C" void check_job_list(char *);

  class operation {
  public:
    virtual ~operation();

    static void check_job_list();

    // Todo - protect?
    static void set_pending(DLV::operation *op); //clb to protect
    static void attach_base(operation *op);
    static void clear_all();
    operation *find_parent(const string datatype, const string program);
    //clb to protect?
    static operation *find_parent_geometry(real_l r[3][3], bool &ok);

    static int_g max_items();
    static int_g list_all(string *names, int_g *ids, const int_g n);
    static int_g list_op(op_test test, string *names, int_g *ids, const int_g n,
			 const int_g selection);
    static int_g list_op1_or_op2(op_test test1, op_test test2, string *names,
				 int_g *ids, const int_g n,
				 const int_g selection);
    static int_g list_op1_and_op2(op_test test1, op_test test2, string *names,
				  int_g *ids, const int_g n,
				  const int_g selection);
    static operation *select(const int_g n);
    static int_g list_models(string * &names, const int model_type);
    static int_g list_neb_models(string * &names, char message[], 
			       const int_g len);
    static int_g get_neb_first_model();
    static void remove(const int_g n, const bool all_refs = false);
    static void load(std::ifstream &input);
    static void save(std::ofstream &output);

    virtual bool is_all() const;
    virtual bool is_subproject() const;
    virtual bool is_geometry() const;
    virtual bool is_geom_edit() const;
    virtual bool is_geom_load() const;
    virtual bool is_data_load() const;
    virtual bool is_calc() const;
    virtual bool open_new_view() const;

    virtual bool will_process_socket() const;
    virtual bool process_socket_data(char data[], const bool suppress);
    virtual bool reload_data(data_object *data,
			     char message[], const int_g mlen) = 0;

    void set_current(); 
    static operation *get_current();
    static operation *get_editing();
    static operation *get_pending(); //clb   
    int_g get_model_type() const;
    string get_model_name() const;
    void get_model_name(char name[], const int_g n) const;
    operation *get_parent() const;

    static model *get_current_model();
    static void get_current_lattice(int_g &c, int_g &l);
    model *get_model() const;
    operation *get_neb_op(int_g nmodel);
    virtual job *get_job() const;

    static void generate_transforms(const int_g na, const int_g nb,
				    const int_g nc, const int_g sa,
				    const int_g sb, const int_g sc,
				    int_g &n, real_g (* &transforms)[3]);

    static DLVreturn_type create_add_atom(const int_g atn,
					  const bool set_charge,
					  const int_g charge,
					  const bool use_rad,
					  const real_g radius, const real_l x,
					  const real_l y, const real_l z,
					  const bool rhomb, char message[],
					  const int_g mlen);
    static DLVreturn_type create_model(const char name[], const int_g dims,
				       const int_g lattice, const int_g centre,
				       const char group[], const int_g setting,
				       const int_g nsettings, const real_l a,
				       const real_l b, const real_l c,
				       const real_l alpha, const real_l beta,
				       const real_l gamma, char message[],
				       const int_g mlen);
    static DLVreturn_type accept_pending(const bool is_edit = false,
					 const bool no_parent = false,
					 const bool set_cont = false);
    static DLVreturn_type accept_pending_no_attach(const bool set_cont = false);
    static void detach_pending();
    static DLVreturn_type delete_pending();
    static DLVreturn_type cancel_pending();
    static bool last_model();

    static DLVreturn_type update_supercell(const int_g matrix[3][3],
					   const bool conv_cell,
					   char message[], const int_g mlen);
    static DLVreturn_type update_edit_lattice(const real_l values[6],
					      char message[], const int_g mlen);
    static DLVreturn_type update_origin(const real_l x, const real_l y,
					const real_l z, const bool frac,
					char message[], const int_g mlen);
    static DLVreturn_type update_origin_symm(real_l &x, real_l &y,
					     real_l &z, const bool frac,
					     char message[], const int_g mlen);
    static DLVreturn_type update_vacuum(const real_l c, const real_l origin,
					char message[], const int_g mlen);
    static DLVreturn_type update_cluster(const int_g n,
					 char message[], const int_g mlen);
    static DLVreturn_type update_cluster(const int_g n, const real_l x,
					 const real_l y, const real_l z,
					 char message[], const int_g mlen);
    static DLVreturn_type update_cluster(const real_l r,
					 char message[], const int_g mlen);
    static DLVreturn_type update_slab(const int_g h, const int_g k,
				      const int_g l, const bool conventional,
				      const real_g tol, int_g &nlayers,
				      string * &labels, int_g &n,
				      char message[], const int_g mlen);
    static DLVreturn_type update_slab(const real_g tol, int_g &nlayers,
				      string * &labels, int_g &n,
				      char message[], const int_g mlen);
    static DLVreturn_type update_slab_term(const int_g term, char message[],
					   const int_g mlen);
    static DLVreturn_type update_slab(const int_g nlayers, char message[],
				      const int_g mlen);
    static DLVreturn_type update_surface(const int_g h, const int_g k,
					 const int_g l, const bool conventional,
					 const real_g tol, string * &labels,
					 int_g &n, char message[],
					 const int_g mlen);
    static DLVreturn_type update_surface(const real_g tol, string * &labels,
					 int_g &n, char message[],
					 const int_g mlen);
    static DLVreturn_type update_surface(const int_g term, char message[],
					 const int_g mlen);
    static DLVreturn_type update_salvage(const real_g tol, char message[],
					 const int_g mlen);
    static DLVreturn_type update_salvage(const int_g nlayers, char message[],
					 const int_g mlen);
    static DLVreturn_type wulff_update(const real_l r, char message[],
				       const int_g mlen);
    static DLVreturn_type multilayer_update(const real_l r, char message[],
					    const int_g mlen);
    static DLVreturn_type geometry_update(char message[], const int_g mlen);
    virtual DLVreturn_type update_supercell(const model *parent,
					    const int_g m[3][3],
					    const bool conv,
					    char message[], const int_g mlen);
    virtual DLVreturn_type update_lattice(const model *parent,
					  const real_l values[6],
					  char message[], const int_g mlen);
    virtual DLVreturn_type update_origin(const model *parent,
					 const real_l x, const real_l y,
					 const real_l z, const bool frac,
					 char message[], const int_g mlen);
    virtual DLVreturn_type update_origin_symm(const model *parent,
					      real_l &x, real_l &y,
					      real_l &z, const bool frac,
					      char message[], const int_g mlen);
    virtual DLVreturn_type update_vacuum(const model *parent,
					 const real_l c, const real_l origin,
					 char message[], const int_g mlen);
    virtual DLVreturn_type update_cluster(model *parent, const int_g n,
					  char message[], const int_g mlen);
    virtual DLVreturn_type update_cluster(model *parent, const int_g n,
					  const real_l x, const real_l y,
					  const real_l z, char message[],
					  const int_g mlen);
    virtual DLVreturn_type update_cluster(model *parent, const real_l r,
					  char message[], const int_g mlen);
    virtual DLVreturn_type update_slab(model *parent, const int_g h,
				       const int_g k, const int_g l,
				       const bool conventional,
				       const real_g tol, int_g &nlayers,
				       string * &labels, int_g &n, 
				       char message[], const int_g mlen);
    virtual DLVreturn_type update_slab(model *parent, const real_g tol,
				       int_g &nlayers, string * &labels,
				       int_g &n, char message[],
				       const int_g mlen);
    virtual DLVreturn_type update_slab_term(model *parent, const int_g term,
					    char message[], const int_g mlen);
    virtual DLVreturn_type update_slab(model *parent, const int_g nlayers,
				       char message[], const int_g mlen);
    virtual DLVreturn_type update_surface(model *parent, const int_g h,
					  const int_g k, const int_g l,
					  const bool conventional,
					  const real_g tol, string * &labels,
					  int_g &n, char message[],
					  const int_g mlen);
    virtual DLVreturn_type update_surface(model *parent, const real_g tol,
					  string * &labels, int_g &n,
					  char message[], const int_g mlen);
    virtual DLVreturn_type update_surface(model *parent, const int_g term,
					  char message[], const int_g mlen);
    virtual DLVreturn_type update_salvage(model *parent, const real_g tol,
					  char message[], const int_g mlen);
    virtual DLVreturn_type update_salvage(model *parent, const int_g nlayers,
					  char message[], const int_g mlen);
    virtual DLVreturn_type wulff_update(model *parent, const real_l r,
					char message[], const int_g mlen);
    virtual DLVreturn_type multilayer_update(model *parent, const real_l r,
					     char message[], const int_g mlen);
    virtual DLVreturn_type geometry_update(model *parent,
					   char message[], const int_g mlen);

    static operation *edit_current_data(const int_g object,
					const int_g component,
					const int_g method, char message[],
					const int_g mlen);
    static DLVreturn_type select_current_edit(const int_g object,
					      char message[], const int_g mlen);

    // Todo - protect these?
    const data_object *find_data(const string datatype) const;
    data_object *find_data(const string datatype,
			   const string program) const;
    shared_ptr<data_object> find_shared_data(const string datatype,
					     const string program) const;
    void attach_data(data_object *data);
    void attach_data(shared_ptr<data_object> data);
    void detach_data(shared_ptr<data_object> data);
    static data_object *find_wulff(const int_g selection);
    static DLVreturn_type recover_current_job(const int_g index,
					      const bool use_list,
					      operation * &op, char message[],
					      const int_g mlen);
    static DLVreturn_type delete_current_job(const int_g index, char message[],
					     const int_g mlen);

    static data_object *find_plane(const int_g selection);
    static data_object *find_3D_region(const int_g selection);

#ifdef ENABLE_DLV_GRAPHICS
    void render_data(data_object *data);
    void transfer_and_render_data(DLV::operation *op, data_object *data); 
    render_parent *get_display_obj();
    static bool continue_has_selections();
    static DLVreturn_type create_atom_group(const char label[],
					    const char group[], const bool all,
					    const char remainder[],
					    char message[], const int_g mlen);
    static DLVreturn_type update_cluster_region(const real_g radii[],
						const int_g nradii,
						const char label[],
						char message[],
						const int_g mlen);
    static DLVreturn_type accept_cluster_region(char message[],
						const int_g mlen);
    static DLVreturn_type cancel_cluster_region(const char label[],
						char message[],
						const int_g mlen);
    DLVreturn_type render_r(char message[], const int_g mlen);
    DLVreturn_type render_k(char message[], const int_g mlen);
#  if defined(DLV_USES_AVS_GRAPHICS) || defined(DLV_USES_VTK_GRAPHICS)
    DLVreturn_type detach_from_rviewer(class VIEWER3D *view);
    DLVreturn_type detach_from_kviewer(class VIEWER3D *view);
    DLVreturn_type attach_to_rviewer(class VIEWER3D *view);
    DLVreturn_type attach_to_kviewer(class VIEWER3D *view);
#  endif //GRAPHICS
    void detach_from_kviewer();
    void attach_to_kviewer();
    class toolkit_obj get_k3D_obj() const;
    DLVreturn_type attach_rui();
    DLVreturn_type attach_kui();
    void attach_updatable_data();

    DLVreturn_type update_cell(char message[], const int_g mlen);
    DLVreturn_type update_lattice(const bool kspace, char message[],
				  const int_g mlen);
    DLVreturn_type update_atoms(char message[], const int_g mlen);
    DLVreturn_type update_bonds(char message[], const int_g mlen);
    static DLVreturn_type add_current_bond(char message[], const int_g mlen);
    static DLVreturn_type del_current_bond(char message[], const int_g mlen);
    static DLVreturn_type add_current_bond_type(char message[],
						const int_g mlen);
    static DLVreturn_type del_current_bond_type(char message[],
						const int_g mlen);
    static void set_current_bond_all();
    static DLVreturn_type add_current_bond_all(char message[],
					       const int_g mlen);
    static DLVreturn_type del_current_bond_all(char message[],
					       const int_g mlen);
    static DLVreturn_type display_current_data(const int_g object,
					       const int_g component,
					       const int_g method, bool &kspace,
					       char message[],
					       const int_g mlen);
    static DLVreturn_type delete_current_data(const int_g object,
					      bool &kspace,
					      char message[],
					      const int_g mlen);
    static DLVreturn_type select_current_display(const int_g object,
						 char message[],
						 const int_g mlen);
    static DLVreturn_type delete_current_display(const int_g object,
						 char message[],
						 const int_g mlen);
    static DLVreturn_type select_current_atom(const real_g coords[3],
					      const int_g status,
					      int_g &nselects,
					      char message[], const int_g mlen);
    static DLVreturn_type deselect_all_atoms(char message[], const int_g mlen);
    static DLVreturn_type update_current_outline(char message[],
						 const int_g mlen);
    static bool select_current_job(const int_g index, char status[],
				   bool &list_files, bool &recover, bool &log,
				   bool &run, char message[], const int_g mlen);
    static DLVreturn_type kill_current_job(const int_g index, char message[],
					   const int_g mlen);

    static DLVreturn_type update_deleted_atoms(char message[],
					       const int_g mlen);
    static DLVreturn_type set_edit_transform(const bool v, real_l &x,
					     real_l &y, real_l &z,
					     char message[], const int_g mlen);
    static DLVreturn_type update_inserted_atom(const int_g atn, const bool frac,
					       const real_l x, const real_l y,
					       const real_l z, char message[],
					       const int_g mlen);
    static DLVreturn_type switch_atom_coords(const bool frac,
					     const bool displace, real_l &x,
					     real_l &y, real_l &z,
					     char message[], const int_g mlen);
    static DLVreturn_type update_from_selections(real_l &x, real_l &y,
						 real_l &z, char message[],
						 const int_g mlen);
    static DLVreturn_type update_inserted_model(const bool frac,
						const real_l x, const real_l y,
						const real_l z, char message[],
						const int_g mlen);
    static DLVreturn_type position_from_selections(real_l &x, real_l &y,
						   real_l &z, char message[],
						   const int_g mlen);
    static DLVreturn_type update_move_atoms(const bool frac,
					    const bool displace,
					    const real_l x, const real_l y,
					    const real_l z, char message[],
					    const int_g mlen);
    static DLVreturn_type update_atom(const int_g atn, const bool all,
				      char message[], const int_g mlen);
    static DLVreturn_type update_props(const int_g charge, real_g &radius,
				       const real_g red, const real_g green,
				       const real_g blue, const int_g spin,
				       const bool use_charge,
				       const bool use_radius,
				       const bool use_colour,
				       const bool use_spin, const bool all,
				       char message[], const int_g mlen);
    virtual DLVreturn_type delete_atoms(model *parent, char message[],
					const int_g mlen);
    virtual DLVreturn_type transform_editor(model *parent, const bool v,
					    real_l &x, real_l &y, real_l &z,
					    char message[], const int_g mlen);
    virtual DLVreturn_type insert_atom(const model *parent, const int_g atn,
				       const real_l x, const real_l y,
				       const real_l z, const bool frac,
				       char message[], const int_g mlen);
    virtual DLVreturn_type switch_coords(const bool frac, const bool displace,
					 real_l &x, real_l &y, real_l &z,
					 char message[], const int_g mlen);
    virtual DLVreturn_type use_selections(model *parent, real_l &x,
					  real_l &y, real_l &z, char message[],
					  const int_g mlen);
    virtual DLVreturn_type insert_model(const model *parent,
					const real_l x, const real_l y,
					const real_l z, const bool frac,
					char message[], const int_g mlen);
    virtual DLVreturn_type centre_from_selects(const model *parent, real_l &x,
					       real_l &y, real_l &z,
					       char message[],
					       const int_g mlen);
    virtual DLVreturn_type move_atoms(model *parent, const real_l x,
				      const real_l y, const real_l z,
				      const bool frac, const bool displace,
				      char message[], const int_g mlen);
    virtual DLVreturn_type update_atom(model *parent, const int_g atn,
				       const bool all, char message[],
				       const int_g mlen);
    virtual DLVreturn_type update_props(model *parent, const int_g charge,
					real_g &radius, const real_g red,
					const real_g green, const real_g blue,
					const int_g spin, const bool use_charge,
					const bool use_radius,
					const bool use_colour,
					const bool use_spin, const bool all,
					char message[], const int_g mlen);
    virtual DLVreturn_type update_rod_props(model *parent, const int_g dw1,
					    const bool use_dw1, 
					    const int_g dw2, const bool use_dw2,
					    const int_g occ, const bool use_occ,
					    const bool all,
					    char message[], const int_g mlen);
    static DLVreturn_type update_origin_atoms(real_l &x, real_l &y,
					      real_l &z, const bool frac,
					      char message[], const int_g mlen);
    virtual DLVreturn_type update_origin_atoms(const model *parent,
					       real_l &x, real_l &y,
					       real_l &z, const bool frac,
					       char message[],
					       const int_g mlen);

    static DLVreturn_type update_3D_region(const int_g method, const int_g h,
					   const int_g k, const int_g l,
					   const real_g x, const real_g y,
					   const real_g z, const int_g object,
					   const bool conv, const bool frac,
					   const bool p_obj, char message[],
					   const int_g mlen);
    static DLVreturn_type update_sphere(const int_g method, const real_g radius,
					char message[], const int_g mlen);
    static DLVreturn_type update_plane(const int_g method, const int_g h,
				       const int_g k, const int_g l,
				       const real_g x, const real_g y,
				       const real_g z, const int_g obj,
				       const bool conv, const bool frac,
				       const bool parent, char message[],
				       const int_g mlen);
    static DLVreturn_type update_line(const int_g method, const int_g h,
				      const int_g k, const int_g l,
				      const real_g x, const real_g y,
				      const real_g z, const int_g obj,
				      const bool conv, const bool frac,
				      const bool parent, char message[],
				      const int_g mlen);
    static DLVreturn_type update_point(const int_g method, const real_g x,
				       const real_g y, const real_g z,
				       const bool conv, char message[],
				       const int_g mlen);
    static DLVreturn_type update_wulff(const int_g h, const int_g k,
				       const int_g l, const real_g e,
				       const real_g r, const real_g g,
				       const real_g b, const bool conv,
				       char message[], const int_g mlen);
    static DLVreturn_type update_wulff(const char filename[], char message[],
				       const int_g mlen);
    static DLVreturn_type update_leed(const bool domains, char message[],
				      const int_g mlen);
    static DLVreturn_type set_data_transform(const bool v, char message[],
					     const int_g mlen);
    static DLVreturn_type update_current_spectrum(const int_g object,
						  const int_g type,
						  const real_g emin,
						  const real_g emax,
						  const int_g np,
						  const real_g w,
						  const real_g harmonics,
						  char message[],
						  const int_g mlen);
    static DLVreturn_type update_current_phonon(const int_g object,
						const int_g nframes,
						const real_g scale,
						const bool use_temp,
						const real_g temperature,
						char message[],
						const int_g mlen);
    static DLVreturn_type hide_current_render(const int_g object,
					      const int_g visible, bool &kspace,
					      char message[],
					      const int_g mlen);
    static DLVreturn_type update_current_streamlines(const int_g object,
						     const int_g plane,
						     char message[],
						     const int_g mlen);
    static DLVreturn_type update_current_slice(const int_g object,
					       const int_g plane,
					       char message[],
					       const int_g mlen);
    static DLVreturn_type update_current_cut(const int_g object,
					     const int_g plane,
					     char message[], const int_g mlen);
    static DLVreturn_type update_current_extension(const int_g object,
						   const int_g na,
						   const int_g nb,
						   const int_g nc,
						   char message[],
						   const int_g mlen);

    static DLVreturn_type list_current_data_math(const int_g object,
						 const bool all,
						 string * &names, int_g &n,
						 char message[],
						 const int_g mlen);
    static DLVreturn_type update_current_data_math(const int_g object,
						   const int_g selection,
						   const bool all,
						   const int_g index,
						   char message[],
						   const int_g mlen);
    static DLVreturn_type list_current_reals(string * &names, int_g &n,
					     char message[], const int_g mlen);
    static DLVreturn_type update_current_shift(const int_g object,
					       const real_g shift,
					       const int_g selection,
					       const bool use_select,
					       char message[],
					       const int_g mlen);
    static int_g list_lines(string * &labels, char message[], const int_g mlen);
    static int_g list_planes(string * &labels, char message[],
			     const int_g mlen);
    static int_g list_3D_regions(string * &labels, char message[],
			       const int_g mlen);
    static int_g list_wulff(string * &labels, char message[], const int_g mlen);
    static data_object *find_line(const int_g selection);
  #ifdef DLV_USES_VTK_GRAPHICS
    // This may not be necessary for vTk, but it will be for some non-AVS stuff
    static DLVreturn_type update_current_spheres(char message[],
						 const int_g mlen);
    static DLVreturn_type update_current_lines(char message[],
					       const int_g mlen);
  #endif // DLV_USES_VTK_GRAPHICS
    
#endif // ENABLE_DLV_GRAPHICS

  protected:
    operation();
    operation(model *m);

    void check_job();

    static operation *get_continue();
    virtual string get_name() const = 0;
    virtual bool job_failed() const;

    static operation *find_current_surface_parent(int_g supercell[2][2]);
    //operation *find_surface_parent(int_g supercell[2][2]) const;
    virtual bool copy_cell_replicate() const;
    virtual bool is_supercell() const;
    virtual bool is_property_change() const;
    virtual void get_supercell(int_g supercell[2][2]) const;
    virtual bool get_lattice_rotation(real_l r[3][3]) const;

    virtual void inherit_model();
    virtual void inherit_data();
    void inherit_non_std_data();
    virtual void map_data();
    void attach();
    void attach_no_inherit_model();
    void attach_no_current();
    void attach_base_no_current();
    static void attach_tree(operation *op);
    void attach_pending(const bool data = false);
    void attach_pending_copy_model(const string tag);
    void replace_model(model *m);
    static const operation *select_model(const int_g n, const int_g model_type,
					 char message[], const int_g mlen);
    void attach_model(model *m);
    std::map<string, shared_ptr<class job> > *get_parent_conn();
    void set_parent_conn(std::map< string,
			 shared_ptr<class job> > *conn);

    static void set_editing(DLV::operation *op);
    void support_create();

    static operation *find_pending_parent(const string datatype,
					  const string program);
    virtual void add_path(string &dir) const;
    string get_sub_project_dir() const;

    virtual DLVreturn_type add_atom(const int_g atn, const bool set_charge,
				    const int_g charge, const bool use_rad,
				    const real_g radius, const real_l x,
				    const real_l y, const real_l z,
				    const bool rhomb, char message[],
				    const int_g mlen);
    virtual void add_standard_data_objects() = 0;
    void set_bond_all();

    virtual int_g get_job_number() const;
    bool find_job(job * &cur_job, operation * &op, const int_g index);
    static job *find_job(const int_g index, operation * &op);
    virtual DLVreturn_type recover_job(char message[], const int_g mlen);
    virtual DLVreturn_type remove_job(char message[], const int_g mlen);
    virtual DLVreturn_type kill_job(char message[], const int_g mlen);

#ifdef ENABLE_DLV_GRAPHICS
    bool set_atom_index_and_flags(int_g indices[], const int_g value,
				  const bool set_def, char message[],
       				  const int_g mlen);
    void set_selected_atom_flags(const bool done);
    DLVreturn_type update_atom_selection_info(char message[], const int_g mlen);
    virtual void atom_selections_changed();
    DLVreturn_type update_cell_info(char message[], const int_g mlen);
    DLVreturn_type deselect_atoms(char message[], const int_g mlen);

    void deactivate_atom_flags();
    void toggle_atom_flags(const string label);
#endif // ENABLE_DLV_GRAPHICS

  private:
    static operation *current;
    static operation *pending;
    static operation *editing;
    static operation *continue_edit;
    static bool is_pending;
    static bool data_pending;
    static int_g counter;
    static int_g size;
    //static op_list top;
    static std::map<int, operation *> top;
    string date_stamp;
    op_list children;
    operation *parent;
#ifdef ENABLE_DLV_GRAPHICS
    // Needed here for reverse destruction order?
    shared_ptr<render_parent> display_parent;
#endif // ENABLE_DLV_GRAPHICS
    shared_ptr<class model> structure;
    std::list< shared_ptr<class data_object> > inherited_data;
    std::list< shared_ptr<class data_object> > mappable_data;
    std::list< shared_ptr<class data_object> > calculated_data;
    bool rspace_view;
    bool kspace_view;
    int_g display_index;
    int_g edit_index;
    // Todo - just use one in the map?
    int_g index;
    bool reload;
    bool dont_update; // flag to squash updates from UI attach

    int_g count_display_objects() const;
    data_object *find_edit(const int_g object);
    data_object *find_display(const int_g object);

#ifdef ENABLE_DLV_GRAPHICS
    void list_displayable_data(const bool set_data = false);
    void list_editable_data();
    void list_edited_data();
    void list_displayed_data();
    operation *edit_data(const int_g object, const int_g component,
			 const int_g method, char message[], const int_g mlen);
    DLVreturn_type select_edited(const int_g object, char message[],
				 const int_g mlen);
    DLVreturn_type display_data(const int_g object, const int_g component,
				const int_g method, bool &kspace,
				char message[], const int_g mlen);
    operation *find_calc_data(shared_ptr<data_object> p, const bool erase);
    void delete_child_data(shared_ptr<data_object> p);
    DLVreturn_type delete_data(const int_g object, bool &kspace,
			       char message[], const int_g mlen);
    DLVreturn_type select_display(const int_g object, char message[],
				  const int_g mlen);
    DLVreturn_type delete_display(const int_g object, char message[],
				  const int_g mlen);
    DLVreturn_type add_bond(char message[], const int_g mlen);
    DLVreturn_type del_bond(char message[], const int_g mlen);
    DLVreturn_type add_bond_type(char message[], const int_g mlen);
    DLVreturn_type del_bond_type(char message[], const int_g mlen);
    DLVreturn_type add_bond_all(char message[], const int_g mlen);
    DLVreturn_type del_bond_all(char message[], const int_g mlen);
    DLVreturn_type select_atom(const real_g coords[3], const int_g status,
			       int_g &nselects, char message[],
			       const int_g mlen);
    DLVreturn_type update_outline(char message[], const int_g mlen);
    DLVreturn_type update_spectrum(const int_g object, const int_g type,
				   const real_g emin, const real_g emax,
				   const int_g np, const real_g w,
				   const real_g harmonics, char message[],
				   const int_g mlen);
    DLVreturn_type update_phonon(const int_g object, const int_g nframes,
				 const real_g scale, const bool use_temp,
				 const real_g temperature, char message[],
				 const int_g mlen);
    DLVreturn_type hide_render(const int_g object, const int_g visible,
			       bool &kspace, char message[], const int_g mlen);
    DLVreturn_type update_streamlines(const int_g object, const int_g plane,
				      char message[], const int_g mlen);
    DLVreturn_type update_slice(const int_g object, const int_g plane,
				char message[], const int_g mlen);
    DLVreturn_type update_cut(const int_g object, const int_g plane,
			      char message[], const int_g mlen);
    DLVreturn_type update_extension(const int_g object, const int_g na,
				    const int_g nb, const int_g nc,
				    char message[], const int_g mlen);
    DLVreturn_type list_data_math(const int_g object, const bool all,
				  string * &names, int_g &n, char message[],
				  const int_g mlen);
    DLVreturn_type update_data_math(const int_g object, const int_g selection,
				    const bool all, const int_g index,
				    char message[], const int_g mlen);
    DLVreturn_type list_reals(string * &names, int_g &n, char message[],
			      const int_g mlen);
    DLVreturn_type update_shift(const int_g object, const real_g shift,
				const int_g selection, const bool use_select,
				char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

    void list_tree_all(string *names, int_g *ids, const int_g n,
		       int_g &count, const string indent);
    void list_tree_op(op_test test, string *names, int_g *ids, const int_g n,
		      const int_g selection, int_g &count, const string indent);
    void list_tree_op1_or_op2(op_test test1, op_test test2, string *names,
			      int_g *ids, const int_g n, const int_g selection,
			      int_g &count, const string indent);
    void list_tree_op1_and_op2(op_test test1, op_test test2, string *names,
			       int_g *ids, const int_g n, const int_g selection,
			       int_g &count, const string indent);
    operation *search_tree(const int_g n) const;
    operation *find_bottom_child();
    void list_tree_model(string *names, const int model_type, int_g &count);
    void list_tree_neb(string *names, int_g &count, int_g limit=9999);
    void list_tree_neb_first_model(int_g &count, bool &success);
    void list_tree_neb_op(int_g &count, int_g limit, DLV::operation* &nebop);
    int_g compare_neb_structures(int_g limit);
    const operation *search_model(const int_g n, const int_g model_type,
				  int_g &count) const;
    void remove_data(shared_ptr<class data_object> data);
    bool remove_tree(const int_g n, const bool all_refs);

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class load_op : public operation {
  protected:
    load_op(const char file[]);
    load_op(model *m, const char file[]);

    string get_filename() const;

  private:
    file_obj *file;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class edit_data_op : public operation {
  public:
    edit_data_op();

    bool reload_data(data_object *data, char message[], const int_g mlen);

  protected:
    string get_name() const;
    void add_standard_data_objects();

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
  BOOST_SERIALIZATION_ASSUME_ABSTRACT(DLV::operation)
  BOOST_CLASS_EXPORT_KEY(DLV::operation)
  BOOST_CLASS_EXPORT_KEY(DLV::load_op)
  BOOST_CLASS_EXPORT_KEY(DLV::edit_data_op)
#endif // DLV_USES_SERIALIZE

inline void DLV::operation::set_current()
{
  current = this;
}

inline void DLV::operation::set_editing(DLV::operation *op)
{
  editing = op;
}

inline void DLV::operation::set_pending(DLV::operation *op)
{
  pending = op;
}

inline void DLV::operation::support_create()
{
  is_pending = true;
  pending = current;
  editing = this;
  current = this;
}

inline DLV::operation *DLV::operation::get_current()
{
  return current;
}

inline DLV::operation *DLV::operation::get_continue()
{
  operation *op = current;
  if (continue_edit) {
    op = continue_edit;
    continue_edit = 0;
  }
  return op;
}

inline
DLV::operation *DLV::operation::find_pending_parent(const string datatype,
						    const string program)
{
  return pending->find_parent(datatype, program);
}

inline DLV::operation *DLV::operation::get_editing()
{
  return editing;
}

inline DLV::operation *DLV::operation::get_pending()
{
  return pending;
}

inline class DLV::model *DLV::operation::get_model() const
{
  return &(*structure);
}

inline class DLV::model *DLV::operation::get_current_model()
{
  return &(*(current->structure));
}

inline DLV::int_g DLV::operation::max_items()
{
  return counter;
}

#ifdef ENABLE_DLV_GRAPHICS

inline void DLV::operation::detach_from_kviewer()
{
  kspace_view = false;
}

inline void DLV::operation::attach_to_kviewer()
{
  kspace_view = true;
}

#endif // ENABLE_DLV_GRAPHICS

#endif // DLV_OPERATION
