
#ifndef DLV_FILE_IMPLEMENTATIONS
#define DLV_FILE_IMPLEMENTATIONS

namespace DLV {

  // i.e. file://name
  class local_file : public file_obj {
  public:
    local_file(const string dir, const string name, const string run_name,
	       const bool link);

  private:
    bool loaded;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class remote_file : public file_obj {
  public:
    remote_file(const string dir, const string name, const string run_name,
		const bool link);

    void set_run_directory(const string dir, const string host);
    string get_filename() const;
    string get_runname() const;

  private:
    string hostname;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  // Todo - what types do we need here
  /*class http_file : public file {
    };*/

}

#ifdef DLV_USES_SERIALIZE

BOOST_CLASS_EXPORT_KEY(DLV::local_file)
BOOST_CLASS_EXPORT_KEY(DLV::remote_file)

#endif // DLV_USES_SERIALIZE

inline DLV::local_file::local_file(const string dir, const string name,
				   const string run_name, const bool link)
  : file_obj(dir, name, run_name, link), loaded(true)
{
}

inline DLV::remote_file::remote_file(const string dir, const string name,
				     const string run_name, const bool link)
  : file_obj(dir, name, run_name, link)
{
}

#endif // DLV_FILE_IMPLEMENTATIONS
