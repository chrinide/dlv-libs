
#ifndef DLV_SAVE_MODEL
#define DLV_SAVE_MODEL

namespace DLV {

  class save_xyz_file : public save_model_op {
  public:
    static operation *create(const char filename[],
			     char message[], const int_g mlen);

    // public for serialization
    save_xyz_file(const char file[]);

  protected:
    DLV::string get_name() const;
    DLVreturn_type write(const char filename[],
			 const DLV::model *const structure,
			 char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class save_cif_file : public save_model_op {
  public:
    static operation *create(const char filename[],
			     char message[], const int_g mlen);

    // public for serialization
    save_cif_file(const char file[]);

  protected:
    DLV::string get_name() const;
    DLVreturn_type write(const char filename[],
			 const DLV::model *const structure,
			 char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

#  ifdef USE_ESCDF
  class save_escdf_file : public save_model_op {
  public:
    static operation *create(const char filename[],
			     char message[], const int_g mlen);

    // public for serialization
    save_escdf_file(const char file[]);

  protected:
    DLV::string get_name() const;
    DLVreturn_type write(const char filename[],
			 const DLV::model *const structure,
			 char message[], const int_g mlen);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };
#  endif // ESCDF

}

#ifdef DLV_USES_SERIALIZE

BOOST_CLASS_EXPORT_KEY(DLV::save_xyz_file)
BOOST_CLASS_EXPORT_KEY(DLV::save_cif_file)
#  ifdef USE_ESCDF
BOOST_CLASS_EXPORT_KEY(DLV::save_escdf_file)
#  endif // ESCDF

#endif // DLV_USES_SERIALIZE

inline DLV::save_xyz_file::save_xyz_file(const char file[])
  : save_model_op(file)
{
}

inline DLV::save_cif_file::save_cif_file(const char file[])
  : save_model_op(file)
{
}

#  ifdef USE_ESCDF
inline DLV::save_escdf_file::save_escdf_file(const char file[])
  : save_model_op(file)
{
}
#  endif // ESCDF

#endif // DLV_SAVE_MODEL
