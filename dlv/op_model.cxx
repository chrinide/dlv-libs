
#include <list>
#include <map>
#include <cctype>
#include <sys/types.h>
#ifndef WIN32
#  include <sys/socket.h>
#  include <netinet/ip.h>
#endif // WIN32
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS // for data_objs
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "constants.hxx"
#include "utils.hxx"
#include "atom_model.hxx"
#include "model.hxx"
#include "operation.hxx"
#include "op_model.hxx"
#include "data_objs.hxx"
#include "symmetry.hxx"
#include "settings.hxx"
#include "scitbx/array_family/shared.h"
#include "iotbx/pdb/input.h"
#include "iotbx/pdb/hierarchy.h"
#include "extern_model.hxx"

// This should be a static part of  load_pdb but that would create a
// dependency in the header file I'd rather not have. Todo?
namespace {

  iotbx::pdb::input *pdb_file_data = 0;
  iotbx::pdb::hierarchy::root pdb_hierarchy;

}

#ifdef ENABLE_DLV_GRAPHICS

DLV::string DLV::cds_cif_socket::data;

#endif // ENABLE_DLV_GRAPHICS

void DLV::change_geometry::inherit_model()
{
  // Null op, we have a new structure
}

void DLV::change_geometry::inherit_data()
{
  // Todo - I think this is possibly a null op, but see map_data
}

void DLV::edit_geometry::map_data()
{
  // Load or create are disconnected from other and can't map data,
  // but a structural edit possibly can. Tricky - Todo
  // This may need to be implemented separately for each edit op.
}

bool DLV::edit_geometry::reload_data(class data_object *, char [], const int_g)
{
  // Do nothing?
  return true;
}

void DLV::edit_geometry::add_standard_data_objects()
{
  data_object *data = new atom_and_bond_data();
  if (data != 0) {
    attach_data(data);
#ifdef ENABLE_DLV_GRAPHICS
    render_data(data);
#endif // ENABLE_DLV_GRAPHICS
  }
  if (get_model()->get_number_of_periodic_dims() > 1) {
    data = new lattice_direction_data();
    if (data != 0)
      attach_data(data);
  }
}

bool DLV::change_geometry::is_geometry() const
{
  return true;
}

bool DLV::edit_geometry::is_geom_edit() const
{
  return true;
}

bool DLV::load_model_op::is_geometry() const
{
  return true;
}

bool DLV::load_model_op::is_geom_load() const
{
  return true;
}

bool DLV::load_model_op::reload_data(class data_object *, char [], const int_g)
{
  // Null call, Nothing to reload
  return true;
}

void DLV::load_model_op::inherit_model()
{
  // Null op, we have a new structure
}

void DLV::load_model_op::inherit_data()
{
  // Todo - I think this is possibly a null op, but see map_data
}

void DLV::load_model_op::add_standard_data_objects()
{
  // Do nothing
}

void DLV::load_atom_model_op::add_standard_data_objects()
{
  data_object *data = new atom_and_bond_data();
  if (data != 0)
    attach_data(data);
  if (get_model()->get_number_of_periodic_dims() > 1) {
    data = new lattice_direction_data();
    if (data != 0)
      attach_data(data);
  }
}

DLV::string DLV::load_model_op::name_from_file(const char name[],
					       const char filename[])
{
  string model_name;
  // Todo - better numbering
  if (name == 0)
    model_name = "Model";
  else if (name[0] == '\0')
    model_name = "Model";
  else
    model_name = name;
  model_name += " (";
  model_name += base_file_name(filename);
  model_name += ")";
  return model_name;
}

DLV::string DLV::load_model_op::name_from_file(const char name[],
					       const char filename[], 
					       const char filename2[])
{
  string model_name;
  // Todo - better numbering
  if (name == 0)
    model_name = "Model";
  else if (name[0] == '\0')
    model_name = "Model";
  else
    model_name = name;
  model_name += " (";
  model_name += base_file_name(filename);
  model_name += " + ";
  model_name += base_file_name(filename2);
  model_name += ")";
  return model_name;
}

DLV::string DLV::create_model_op::get_name() const
{
  return (get_model_name() + " - New Model");
}

bool DLV::create_model_op::reload_data(class data_object *, char [],
				       const int_g)
{
  // Null call, Nothing to reload
  return true;
}

void DLV::create_model_op::add_standard_data_objects()
{
  // Do nothing
}

void DLV::create_atom_model_op::add_standard_data_objects()
{
  data_object *data = new atom_and_bond_data();
  if (data != 0) {
    attach_data(data);
#ifdef ENABLE_DLV_GRAPHICS
    render_data(data);
#endif // ENABLE_DLV_GRAPHICS
  }
  if (get_model()->get_number_of_periodic_dims() > 1) {
    data = new lattice_direction_data();
    if (data != 0)
      attach_data(data);
  }
}

// Don't really beint_l here, although they only work for create_model_op.
DLVreturn_type
DLV::operation::create_model(const char name[], const int_g dims,
			     const int_g lattice, const int_g centre,
			     const char group[], const int_g setting,
			     const int_g nsettings, const real_l a,
			     const real_l b, const real_l c,
			     const real_l alpha, const real_l beta,
			     const real_l gamma, char message[],
			     const int_g mlen)
{
  string label;
  if (name == 0 or strlen(name) == 0) {
    char buff[24];
    snprintf(buff, 24, "Model%1d", counter + 1);
    label = buff;
  } else
    label = name;
  model *m = create_atoms(label, dims);
  if (m == 0) {
    strncpy(message, "Failed to create model", mlen);
    return DLV_ERROR;
  } else {
    bool ok = true;
    if (dims == 0)
      ok = m->set_point_group(group);
    else {
      bool hex = false;
      if (dims == 3)
	hex = (lattice == 4 and centre == 0);
      else if (dims == 2)
	hex = (lattice == 2);
      ok = m->set_group(group, setting, nsettings, hex);
    }
    if (!ok) {
      strncpy(message, "Failed to set group", mlen);
      return DLV_ERROR;
    } else {
      if (dims > 0)
	ok = m->set_lattice_parameters(a, b, c, alpha, beta, gamma);
      if (!ok) {
	strncpy(message, "Failed to set lattice", mlen);
	return DLV_ERROR;
      } else {
	m->complete();
	operation *op = new create_atom_model_op(m);
	if (op == 0) {
	  delete m;
	  strncpy(message, "Failed to create model", mlen);
	  return DLV_ERROR;
	} else {
	  is_pending = true;
	  pending = current;
	  editing = op;
	  current = op;
	}
      }
    }
  }
  return DLV_OK;
}

DLVreturn_type
DLV::operation::create_add_atom(const int_g atn, const bool set_charge,
				const int_g charge, const bool use_rad,
				const real_g radius, const real_l x,
				const real_l y, const real_l z,
				const bool rhomb, char message[],
				const int_g mlen)
{
  if (!is_pending) {
    strncpy(message, "BUG: not in geometry operation", mlen);
    return DLV_ERROR;
  } else
    return editing->add_atom(atn, set_charge, charge, use_rad, radius, x, y, z,
			     rhomb, message, mlen);
}

DLVreturn_type
DLV::create_atom_model_op::add_atom(const int_g atn, const bool set_charge,
				    const int_g charge, const bool use_rad,
				    const real_g radius, const real_l x,
				    const real_l y, const real_l z,
				    const bool rhomb, char [], const int_g)
{
  coord_type c[3];
  c[0] = x;
  c[1] = y;
  c[2] = z;
  model *m = get_model();
  int_g index;
  if (m->add_fractional_atom(index, atn, c, rhomb)) {
    if (set_charge)
      m->set_atom_charge(index, charge);
    if (use_rad)
      m->set_atom_radius(index, (real_g)radius);
    m->complete(true);
    return DLV_OK;
  } else
    return DLV_ERROR;
}

DLV::string DLV::load_cif_file::get_name() const
{
  return (get_model_name() + " - Load CIF");
}

DLV::operation *DLV::load_cif_file::create(const char name[],
					   const char filename[],
					   const bool bonds,
					   char message[], const int_g mlen)
{
  message[0] = '\0';
  load_cif_file *op = 0;
  DLVreturn_type ok = DLV_OK;
  if (check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (open_file_read(input, filename, message, mlen)) {
      bool has_bonds = false;
      model *structure = read(input, name_from_file(name, filename),
			      has_bonds, false, message, mlen);
      if (structure == 0) {
	strncpy(message, "Create model failed", mlen - 1);
	ok = DLV_ERROR;
      } else {
	if (ok == DLV_OK) {
	  structure->complete();
	  op = new load_cif_file(structure, filename);
	  attach_base(op);
	  if (bonds and !has_bonds)
	    op->set_bond_all();
	  //Todo? if (strlen(message) > 0)
	  //  ok = DLV_WARNING;
	} else
	  delete structure;
      }
      if (ok != DLV_OK)
	message[mlen - 1] = '\0';
      input.close();
    }
  }
  return op;
}

DLV::model *DLV::load_cif_file::read(std::istream &input, string def_name,
				     bool &has_bonds, const bool cds,
				     char message[], const int_g mlen)
{
  has_bonds = false;
  char buff[256];
  bool in_data = false;
  bool has_lattice = false;
  double a = 1.0;
  double b = 1.0;
  double c = 1.0;
  double alpha = 90.0;
  double beta = 90.0;
  double gamma = 90.0;
  string space_group;
  int_g spgp_no = 1;
  string name = def_name;
  bool in_loop = false;
  int_g nloop_items = 0;
  int_g ntemp_items = 0;
  bool temp_use[16];
  string temp_labels[16];
  bool loop_use[16];
  string loop_labels[16];
  bool loop_data = false;
  bool loop_ok = false;
  int_g max_items = 512;
  //string data_items[max_items][16];
  int_g column = 0;
  int_g nlines = 0;
  char (*symbols)[3] = new char[max_items][3];
  string *sites = new string[max_items];
  double (*coords)[3] = new double[max_items][3];
  bool cartesian = false;
  bool rhomb = false;
  bool has_atom_symbol = false;
  bool has_atom_info = false;
  string operators[192];
  int_g nops = 0;
  bool symm_ok = false;
  int_g symm_col = -1;
  crystal_class cell_type = triclinic;
  lattice_centering lattice_type = primitive;
  bool has_setting = false;
  bool pair_ok = false;
  int_g bond1 = -1;
  int_g bond2 = -1;
  const int_g max_pairs = 8 * max_items;
  int_g npairs = 0;
  int_g (*pairs)[2] = new int_g[max_pairs][2];
  bool end_struct = false;
  while (!input.eof()) {
    input.getline(buff, 256);
    // skip blank lines
    if (strlen(buff) == 0 or buff[0] == '\r')
      continue;
    else {
      int_g i = 0;
      while (buff[i] != '\0') {
	if (!isspace(buff[i])) // != ' ' and buff[i] != '\r')
	  break;
	i++;
      }
      if (i == (int)strlen(buff))
	continue;
    }
    if (end_struct) {
      strncpy(message, "Skipping structures after first one", mlen);
      break;
    }
    char text[128];
    sscanf(buff, "%s", text);
    if (text[0] == '#') {
      if (strncmp(text, "#END", 4) == 0 or strncmp(text, "#End", 4) == 0 or
	  strncmp(text, "#end", 4) == 0)
	end_struct = true;
      continue;
    }
    if (in_data) {
      if (text[0] == '_') {
	if (in_loop) {
	  if (loop_data) {
	    // Todo - check that column == 0?
	    loop_data = false;
	    in_loop = false;
	  } else if (strncmp(text, "_atom", 5) == 0) {
	    // Only use the loop with the atom coordinates
	    // loop_ok = true;
	    // Todo - occupancy? _atom_site_occupancy
	    // Todo - oxidation information? may be in separate loop
	    // _atom_type_oxidation_number, _atom_type_symbol
	    if (strcmp(text, "_atom_site_fract_x") == 0) {
	      temp_use[ntemp_items] = true;
	      temp_labels[ntemp_items] = text;
	      loop_ok = true;
	      ntemp_items++;
	    } else if (strcmp(text, "_atom_site_fract_y") == 0) {
	      temp_use[ntemp_items] = true;
	      temp_labels[ntemp_items] = text;
	      loop_ok = true;
	      ntemp_items++;
	    } else if (strcmp(text, "_atom_site_fract_z") == 0) {
	      temp_use[ntemp_items] = true;
	      temp_labels[ntemp_items] = text;
	      loop_ok = true;
	      ntemp_items++;
	    } else if (strcmp(text, "_atom_site_Cartn_x") == 0) {
	      temp_use[ntemp_items] = true;
	      temp_labels[ntemp_items] = text;
	      loop_ok = true;
	      cartesian = true;
	      ntemp_items++;
	    } else if (strcmp(text, "_atom_site_Cartn_y") == 0) {
	      temp_use[ntemp_items] = true;
	      temp_labels[ntemp_items] = text;
	      loop_ok = true;
	      cartesian = true;
	      ntemp_items++;
	    } else if (strcmp(text, "_atom_site_Cartn_z") == 0) {
	      temp_use[ntemp_items] = true;
	      temp_labels[ntemp_items] = text;
	      loop_ok = true;
	      cartesian = true;
	      ntemp_items++;
	    } else if (strcmp(text, "_atom_site_type_symbol") == 0) {
	      temp_use[ntemp_items] = true;
	      temp_labels[ntemp_items] = text;
	      has_atom_symbol = true;
	      has_atom_info = true;
	      ntemp_items++;
	    } else if (strcmp(text, "_atom_site_label") == 0) {
	      temp_use[ntemp_items] = true;
	      temp_labels[ntemp_items] = text;
	      has_atom_info = true;
	      ntemp_items++;
	    } else {
	      temp_use[ntemp_items] = false;
	      ntemp_items++;
	    }
	  } else if (strncmp(text, "_symm", 5) == 0) {
	    if (strcmp(text, "_symmetry_equiv_pos_as_xyz") == 0) {
	      symm_col = ntemp_items;
	      symm_ok = true;
	      ntemp_items++;
	    } else
	      ntemp_items++;
	  } else if (strcmp(text, "_space_group_symop_operation_xyz") == 0) {
	    symm_col = ntemp_items;
	    symm_ok = true;
	    ntemp_items++;
	  } else if (strncmp(text, "_geom_bond", 10) == 0) {
	    if (strcmp(text, "_geom_bond_atom_site_label_1") == 0) {
	      bond1 = ntemp_items;
	      pair_ok = true;
	      ntemp_items++;
	    } else if (strcmp(text, "_geom_bond_atom_site_label_2") == 0) {
	      bond2 = ntemp_items;
	      pair_ok = true;
	      ntemp_items++;
	    } else
	      ntemp_items++;
	  } else {
	    temp_use[ntemp_items] = false;
	    ntemp_items++;
	    loop_ok = false;
	    symm_ok = false;
	    pair_ok = false;
	  }
	}
	if (!in_loop) {
	  if (strcmp(text, "_pd_phase_name") == 0) {
	    char fname[64];
	    sscanf(buff, "%s %s", text, fname);
	    name = fname;
	  } else if (strcmp(text, "_cell_length_a") == 0) {
	    sscanf(buff, "%s %lf", text, &a);
	    has_lattice = true;
	  } else if (strcmp(text, "_cell_length_b") == 0) {
	    sscanf(buff, "%s %lf", text, &b);
	    has_lattice = true;
	  } else if (strcmp(text, "_cell_length_c") == 0) {
	    sscanf(buff, "%s %lf", text, &c);
	    has_lattice = true;
	  } else if (strcmp(text, "_cell_angle_alpha") == 0) {
	    sscanf(buff, "%s %lf", text, &alpha);
	    has_lattice = true;
	  } else if (strcmp(text, "_cell_angle_beta") == 0) {
	    sscanf(buff, "%s %lf", text, &beta);
	    has_lattice = true;
	  } else if (strcmp(text, "_cell_angle_gamma") == 0) {
	    sscanf(buff, "%s %lf", text, &gamma);
	    has_lattice = true;
	  } else if (strcmp(text, "_symmetry_cell_setting") == 0 or
		     strcmp(text, "_space_group_crystal_system") == 0) {
	    has_setting = true;
	    char fname[64];
	    sscanf(buff, "%s %s", text, fname);
	    if (strcmp(fname, "triclinic") == 0)
	      cell_type = triclinic;
	    else if (strcmp(fname, "monoclinic") == 0)
	      cell_type = monoclinic;
	    else if (strcmp(fname, "orthorhombic") == 0)
	      cell_type = orthorhombic;
	    else if (strcmp(fname, "tetragonal") == 0)
	      cell_type = tetragonal;
	    else if (strcmp(fname, "trigonal") == 0)
	      cell_type = trigonal;
	    else if (strcmp(fname, "hexagonal") == 0)
	      cell_type = hexagonal;
	    else if (strcmp(fname, "cubic") == 0)
	      cell_type = cubic;
	    else if (strcmp(fname, "rhombohedral") == 0) {
	      rhomb = true;
	      cell_type = trigonal;
	    }
	  } else if (strcmp(text, "_symmetry_space_group_name_H-M") == 0) {
	    int_g i = 30;
	    while (buff[i] != '\'' and buff[i] != '\0' and buff[i] == ' ')
	      i++;
	    if (buff[i] != '\0') {
	      if (buff[i] == '\'') {
		i++;
		if (buff[i] != '?') {
		  while (buff[i] != '\'') {
		    space_group += buff[i];
		    i++;
		  }
		}
	      } else { // No quotes
		if (buff[i] != '?') {
		  while (buff[i] != '\0') {
		    space_group += buff[i];
		    i++;
		  }
		}
	      }
	    }
	    if (space_group.length() == 1 and space_group[0] == ' ')
	      space_group = "";
	  } else if (strcmp(text, "_symmetry_Int_Tables_number") == 0 or
		     strcmp(text, "_symmetry_Int_tables_number") == 0 or
		     strcmp(text, "_symmetry_space_group_number") == 0)
	    sscanf(buff, "%s %d", text, &spgp_no);
	  //else - skip
	}
      } else if (strcmp(text, "loop_") == 0) {
	in_loop = true;
	loop_ok = false;
	symm_ok = false;
	pair_ok = false;
	loop_data = false;
	ntemp_items = 0;
	// Todo check column == 0?
      } else if (in_loop and !loop_data) {
	loop_data = true;
	if (loop_ok)  {
	  column = 0;
	  nlines = 0;
	  nloop_items = ntemp_items;
	  for (int_g i = 0; i < nloop_items; i++) {
	    loop_use[i] = temp_use[i];
	    loop_labels[i] = temp_labels[i];
	  }
	} else if (symm_ok) {
	  column = 0;
	  nops = 0;
	} else if (pair_ok) {
	  column = 0;
	  npairs = 0;
	}
      }
      if (in_loop and loop_data) {
	if (symm_ok) {
	  if (nops < 192) { // prefer 48 but cubic conv can be messy
	    const char delim[] = " \t\n\r";
	    char *data = strtok(buff, delim);
	    while (data != 0) {
	      if (column == symm_col) {
		if (data[0] == '\'') {
		  int_g i = 1;
		  bool end_string = false;
		  do {
		    for (; (data[i] != '\0' and data[i] != '\''); i++) {
		      if (data[i] != ' ')
			operators[nops] += data[i];
		    }
		    if (data[i] == '\'')
		      end_string = true;
		    else
		      data = strtok(0, delim);
		    i = 0;
		  } while (!end_string);
		} else
		  operators[nops] = data;
		nops++;
	      }
	      column++;
	      if (column == ntemp_items)
		column = 0;
	      data = strtok(0, delim);
	    }
	  } else
	    strncpy(message, "Unexpected number of operators", mlen);
	} else if (loop_ok) {
	  if (nlines == max_items) {
	    char (*new_symbols)[3] = new char[2 * max_items][3];
	    string *new_sites = new string[2 * max_items];
	    double (*new_coords)[3] = new double[2 * max_items][3];
	    for (int j = 0; j < max_items; j++) {
	      strncpy(new_symbols[j], symbols[j], 3);
	      new_sites[j] = sites[j];
	      new_coords[j][0] = coords[j][0];
	      new_coords[j][1] = coords[j][1];
	      new_coords[j][2] = coords[j][2];
	    }
	    delete [] coords;
	    delete [] sites;
	    delete [] symbols;
	    symbols = new_symbols;
	    sites = new_sites;
	    coords = new_coords;
	    max_items *= 2;
	  }
	  /*if (nlines < max_items)*/ {
	    const char delim[] = " \t\n\r";
	    char *data = strtok(buff, delim);
	    while (data != 0) {
	      if (loop_use[column]) {
		if (loop_labels[column] == "_atom_site_fract_x") {
		  sscanf(data, "%lf", &coords[nlines][0]);
		} else if (loop_labels[column] == "_atom_site_fract_y") {
		  sscanf(data, "%lf", &coords[nlines][1]);
		} else if (loop_labels[column] == "_atom_site_fract_z") {
		  sscanf(data, "%lf", &coords[nlines][2]);
		} else if (loop_labels[column] == "_atom_site_Cartn_x") {
		  sscanf(data, "%lf", &coords[nlines][0]);
		} else if (loop_labels[column] == "_atom_site_Cartn_y") {
		  sscanf(data, "%lf", &coords[nlines][1]);
		} else if (loop_labels[column] == "_atom_site_Cartn_z") {
		  sscanf(data, "%lf", &coords[nlines][2]);
		} else if (loop_labels[column] == "_atom_site_type_symbol") {
		  symbols[nlines][0] = std::toupper(data[0]);
		  if (std::isalpha(data[1])) {
		    symbols[nlines][1] = std::tolower(data[1]);
		    symbols[nlines][2] = '\0';
		  } else
		    symbols[nlines][1] = '\0';
		} else if (loop_labels[column] == "_atom_site_label") {
		  sites[nlines] = data;
		  if (!has_atom_symbol) {
		    symbols[nlines][0] = std::toupper(data[0]);
		    if (std::isalpha(data[1])) {
		      symbols[nlines][1] = std::tolower(data[1]);
		      symbols[nlines][2] = '\0';
		    } else
		      symbols[nlines][1] = '\0';
		  }
		}
		//data_items[nlines][column] = data;
	      }
	      column++;
	      if (column == nloop_items) {
		column = 0;
		nlines++;
	      }
	      data = strtok(0, delim);
	    }
	  }
	} else if (pair_ok) {
	  pairs[npairs][0] = -1;
	  pairs[npairs][1] = -1;
	  if (npairs < max_pairs) {
	    const char delim[] = " \t\n\r";
	    char *data = strtok(buff, delim);
	    while (data != 0) {
	      if (column == bond1 or column == bond2) {
		char site_label[64];
		sscanf(data, "%s", site_label);
		string site_name = site_label;
		// Todo - issue if sites not defined
		if (nlines > 0) {
		  int_g pos;
		  for (pos = 0; pos < nlines; pos++)
		    if (site_name == sites[pos])
		      break;
		  if (column == bond1)
		    pairs[npairs][0] = pos;
		  else if (column == bond2)
		    pairs[npairs][1] = pos;
		}
	      }
	      column++;
	      if (column == ntemp_items) {
		column = 0;
		// Todo > -1 also?
		if (pairs[npairs][0] < nlines and pairs[npairs][1] < nlines)
		  npairs++;
	      }
	      data = strtok(0, delim);
	    }
	  } else
	    strncpy(message, "Too many pairs - truncating", mlen);
	}
      }
    } else if (strncmp(buff, "data_", 5) == 0) {
      if (cds) {
	name = &buff[5];
	name += " (CDS)";
      }
      in_data = true;
    }
    // else skip stuff
  }
  model *structure = 0;
  bool prim = false;
  const double tol = 0.001;
  if (has_lattice) {
    structure = create_atoms(name, 3);
    // use _symmetry_cell_setting and hex info?
    bool spg_done = false;
    if (space_group.length() > 0) {
      if (rhomb and gamma > (120.0 - 0.001) and gamma < (120.0 + 0.001))
	rhomb = false;
      if (space_group[0] == 'R') {
	// reset values for internal routines 
	if (space_group[space_group.length() - 1] == 'R') {
	  space_group[space_group.length() - 1] = ' ';
	  rhomb = true;
	} else if (space_group[space_group.length() - 1] == 'H')
	  space_group[space_group.length() - 1] = ' ';
	if (!rhomb)
	  alpha = 90.0;
      }
      if (space_group[space_group.length() - 1] == 'Z')
	space_group[space_group.length() - 1] = ' ';	
      // unless we have setting info then force this to 1st one
      if (space_group[1] != ' ')
	expand_symbol(space_group);
      int_g nsettings = 1;
      if (spgp_no > 1)
	nsettings = get_number_of_origins(spgp_no);
      else
	nsettings = get_number_of_origins(space_group);
      if (nsettings == 1 or nops == 0) {
	// If there is only 1 setting or we have no ops then try the symbol
	spg_done = true;
	try {
	  structure->set_group(space_group.c_str(), 1, nsettings, rhomb);
	}
	catch (...) {
	  spg_done = false;
	  strncpy(message, "Failed to interpret spacegroup symbol", mlen);
	}
	if (spg_done and cell_type == triclinic) {
	  cell_type = (crystal_class)structure->get_lattice_type();
	  lattice_type = (lattice_centering)structure->get_lattice_centring();
	}
      }
    } else if (!has_setting and nops > 1) {
      // try to guess lattice from parameters, since we have symmetry
      if (alpha < 90.0 + tol and alpha > 90.0 - tol and
	  beta < 90.0 + tol and beta > 90.0 - tol) {
	if (gamma < 120.0 + tol and gamma > 120.0 - tol) {
	  cell_type = trigonal;
	  rhomb = false;
	} else if (gamma < 90.0 + tol and gamma > 90.0 - tol) {
	  if (b < a + tol and b > a - tol) {
	    if (c < a + tol and c > a - tol)
	      cell_type = cubic;
	    else
	      cell_type = tetragonal;
	  } else
	    cell_type = orthorhombic;
	}
      } else if (beta < alpha + tol and beta > alpha - tol and
		 gamma < alpha + tol and gamma > alpha - tol) {
	if (alpha < 60.0 + tol and alpha > 60.0 - tol)
	  cell_type = cubic;
	else {
	  cell_type = trigonal;
	  rhomb = true;
	} 
      }
      // Todo - stuff to recognise bcc etc?
    }
    // Todo - am I missing lattice types?
    switch (cell_type) {
    case orthorhombic:
      if (alpha > 90.0 + tol or alpha < 90.0 - tol or beta > 90.0 + tol or
	  beta < 90.0 - tol or gamma > 90.0 + tol or gamma < 90.0 - tol) {
	if (alpha < 90.0 - tol and beta < 90.0 - tol and gamma < 90.0 - tol) {
	  lattice_type = face_centred;
	  double ap = a;
	  double bp = b;
	  double cp = c;
	  a = std::sqrt(4.0 * bp * cp * std::cos(to_radians(alpha)));
	  b = std::sqrt(4.0 * ap * cp * std::cos(to_radians(beta)));
	  c = std::sqrt(4.0 * ap * bp * std::cos(to_radians(gamma)));
	  alpha = 90.0;
	  beta = 90.0;
	  gamma = 90.0;
	  prim = true;
	} else if ((a - tol < b) and (a + tol > b) and
		   (a - tol < c) and (a + tol > c)) {
	  lattice_type = body_centred;
	  double pambmc = 4.0 * a * a * std::cos(to_radians(alpha));
	  double mapbmc = 4.0 * a * a * std::cos(to_radians(beta));
	  double mambpc = 4.0 * a * a * std::cos(to_radians(gamma));
	  a = std::sqrt((mapbmc + mambpc) / -2.0);
	  b = std::sqrt((pambmc + mapbmc) / -2.0);
	  c = std::sqrt((pambmc + mambpc) / -2.0);
	  alpha = 90.0;
	  beta = 90.0;
	  gamma = 90.0;
	  prim = true;
	} else if ((b - tol < c) and (b + tol > c) and (beta > 90.0 - tol) and
		   (beta < 90.0 + tol) and (gamma > 90.0 - tol) and
		   (gamma < 90.0 + tol)) {
	  lattice_type = a_face;
	  double calpha = std::cos(to_radians(alpha));
	  b = std::sqrt(2.0 * c * c * (1.0 - calpha));
	  c = std::sqrt(2.0 * c * c * (1.0 + calpha));
	  alpha = 90.0;
	  beta = 90.0;
	  gamma = 90.0;
	  prim = true;
	} else if ((a - tol < c) and (a + tol > c) and (alpha > 90.0 - tol) and
		   (alpha < 90.0 + tol) and (gamma > 90.0 - tol) and
		   (gamma < 90.0 + tol)) {
	  lattice_type = b_face;
	  double cbeta = std::cos(to_radians(beta));
	  a = std::sqrt(2.0 * c * c * (1.0 + cbeta));
	  c = std::sqrt(2.0 * c * c * (1.0 - cbeta));
	  alpha = 90.0;
	  beta = 90.0;
	  gamma = 90.0;
	  prim = true;
	} else if ((a - tol < b) and (a + tol > b) and (alpha > 90.0 - tol) and
		   (alpha < 90.0 + tol) and (beta > 90.0 - tol) and
		   (beta < 90.0 + tol)) {
	  lattice_type = c_face;
	  double cgamma = std::cos(to_radians(gamma));
	  a = std::sqrt(2.0 * b * b * (1.0 - cgamma));
	  b = std::sqrt(2.0 * b * b * (1.0 + cgamma));
	  alpha = 90.0;
	  beta = 90.0;
	  gamma = 90.0;
	  prim = true;
	}
      }
      break;
    case tetragonal:
      if ((a - tol < b) and (a + tol > b) and (a - tol < c) and (a + tol > c)) {
	if ((alpha - tol < beta) and (alpha + tol > beta)) {
	  lattice_type = body_centred;
	  c = std::sqrt(-4.0 * a * a * std::cos(to_radians(alpha)));
	  a = std::sqrt((4.0 * a * a - c * c) / 2.0);
	  b = a;
	  alpha = 90.0;
	  beta = 90.0;
	  gamma = 90.0;
	  prim = true;
	}
      }
      break;
    case trigonal:
      if (rhomb)
	lattice_type = rhombohedral;
      break;
    case cubic:
      if ((a - tol < b) and (a + tol > b) and (a - tol < c) and (a + tol > c)) {
	if ((alpha - tol < beta) and (alpha + tol > beta) and
	    (alpha - tol < gamma) and (alpha + tol > gamma)) {
	  double const theta = 90.0 + std::asin(1.0 / 3.0);
	  if (alpha < 60.0 + tol and alpha > 60.0 - tol) {
	    lattice_type = face_centred;
	    a *= std::sqrt(2.0);
	    b = a;
	    c = a;
	    alpha = 90.0;
	    beta = 90.0;
	    gamma = 90.0;
	    prim = true;
	  } else if (alpha < theta + tol and alpha > theta - tol) {
	    lattice_type = body_centred;
	    a *= (2.0 / std::sqrt(3.0));
	    b = a;
	    c = a;
	    alpha = 90.0;
	    beta = 90.0;
	    gamma = 90.0;
	    prim = true;
	  }
	}
      }
      break;
    default:
      break;
    }
    // must set centre first in case prim is true for setting operators
    structure->set_lattice_and_centre((int)cell_type, (int)lattice_type);
    if (!spg_done) {
      if (nops > 0) {
	int_g gp = spgp_no;
	if (gp <= 1) {
	  if (cell_type == hexagonal)
	    gp = 168;
	  else if (cell_type == trigonal)
	    gp = 143;
	}
	if ((gp > 142 and gp < 168) and !rhomb) {
	  // a == b != c is hex, could use gamma = 120?
	  if (b == c)
	    rhomb = true;
	}
	if (rhomb and (alpha - tol < gamma) and (alpha + tol > gamma))
	  prim = true;
	structure->set_operators(operators, nops, gp, rhomb, prim);
      } else
	structure->set_group(spgp_no);
    }
    structure->set_lattice_parameters(a, b, c, alpha, beta, gamma);
    // Force lattice setup for fractional atom coordinates
    structure->complete();
  } else
    structure = create_atoms(name, 0);
  if (!has_atom_info)
    strncpy(message, "No atom symbol data found", mlen);
  for (int_g i = 0; i < nlines; i++) {
    //printf("%f %f %f\n", coords[i][0], coords[i][1], coords[i][2]);
    coord_type cx[3];
    cx[0] = coords[i][0];
    cx[1] = coords[i][1];
    cx[2] = coords[i][2];
    int_g index;
    if (cartesian)
      structure->add_cartesian_atom(index, symbols[i], cx);
    else
      structure->add_fractional_atom(index, symbols[i], cx, rhomb, prim);
  }
  if (npairs > 0) {
    has_bonds = true;
    for (int_g i = 0; i < npairs; i++) {
      if (pairs[i][0] > -1 and pairs[i][0] < nlines and
	  pairs[i][1] > -1 and pairs[i][1] < nlines) {
	structure->add_bond(pairs[i][0], pairs[i][1]);
      }
    }
    bool *bonds_used = new bool[max_pairs];
    for (int_g i = 0; i < npairs; i++)
      bonds_used[i] = false;
    bool *bonded = new bool[max_items];
    int_g nmolecules = 0;
    for (int_g i = 0; i < npairs; i++) {
      if (!bonds_used[i]) {
	for (int_g j = 0; j < nlines; j++)
	  bonded[j] = false;
	bonds_used[i] = true;
	bonded[pairs[i][0]] = true;
	bonded[pairs[i][1]] = true;
	int_g nbonds = 1;
	bool done = false;
	while (!done) {
	  int_g k;
	  for (k = i + 1; k < npairs; k++) {
	    if (!bonds_used[k]) {
	      if (bonded[pairs[k][0]] or bonded[pairs[k][1]])
		break;
	    }
	  }
	  if (k == npairs)
	    done = true;
	  else {
	    bonds_used[k] = true;
	    bonded[pairs[k][0]] = true;
	    bonded[pairs[k][1]] = true;
	    nbonds++;
	  }
	}
	if (nbonds > 0) {
	  char atgp_label[64];
	  nmolecules++;
	  snprintf(atgp_label, 64, "molecule%1d", nmolecules);
	  structure->group_atoms("CIF Molecules", atgp_label, bonded);
	}
      }
    }
    //fprintf(stderr, "Molecules found = %d\n", nmolecules);
    delete [] bonded;
    delete [] bonds_used;
  }
  delete [] pairs;
  delete [] coords;
  delete [] sites;
  delete [] symbols;
  return structure;
}

void DLV::load_cif_file::expand_symbol(string &symbol)
{
  string temp = symbol;
  symbol = "";
  int_g l = temp.length();
  //int j = 0;
  for (int_g i = 0; i < l; i++) {
    if (temp[i] != ' ') {
      symbol += temp[i];
      if (isalpha(temp[i])) {
	symbol += ' ';
	//j += 2;
      } else if (isdigit(temp[i])) {
	if (temp[i + 1] == '/' or isdigit(temp[i + 1])) {
	  /*j++*/;
	} else {
	  symbol += ' ';
	  //j += 2;
	}
      } // else if (temp[i] != ' ')
      //j++;
    }
  }
}

#ifdef ENABLE_DLV_GRAPHICS

#  ifdef WIN32

SOCKET DLV::cds_cif_socket::socket_fd = 0;

DLV::operation *DLV::cds_cif_socket::create(const bool bonds,
					    char message[], const int_g len)
{
  cds_cif_socket *op = 0;
  DLVreturn_type ok = DLV_OK;
  std::istringstream input(data);
  message[0] = '\0';
  bool has_bonds = false;
  model *structure = load_cif_file::read(input, "CDS", has_bonds,
					 true, message, len);
  if (structure == 0) {
    strncpy(message, "Create model failed", len - 1);
    ok = DLV_ERROR;
  } else {
    if (ok == DLV_OK) {
      structure->complete();
      op = new cds_cif_socket(structure);
      attach_base(op);
      if (bonds and !has_bonds)
	op->set_bond_all();
    } else
      delete structure;
  }
  if (ok != DLV_OK)
    message[len - 1] = '\0';
  //input.close();
  data = "";
  return op;
}

SOCKET DLV::cds_cif_socket::start_listening()
{
  if (socket_fd != 0)
    return 0;
  if ((socket_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    return 0;
  else {
    //fprintf(stderr, "Binding\n");
    sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons((short)socket_number);
    addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    //addr.sin_addr.s_addr = INADDR_ANY;
    if (bind(socket_fd, (sockaddr *) &addr,
	     sizeof(sockaddr_in)) == -1) {
      closesocket(socket_fd);
      socket_fd = 0;
      fprintf(stderr, "%d - %s\n", errno, strerror(errno));
      return 0;
    } else
      listen(socket_fd, 1);
  }
  return socket_fd;
}

void DLV::cds_cif_socket::stop_listening()
{
  //if (current_fd != 0) {
  //  DLV::detach_event(current_fd, &(read), 0);
  //  close(current_fd);
  //  current_fd = 0;
  //}
  closesocket(socket_fd);
  socket_fd = 0;
}

bool DLV::cds_cif_socket::read(char message[], const int_g len)
{
  //fprintf(stderr, "Accept\n");
  sockaddr addr;
  int_g s = sizeof(addr);
  SOCKET current_fd = accept(socket_fd, &addr, &s);
  int_g sz;
  static char buff[4097];
  bool first = true;
  bool its_a_cif = false;
  while ((sz = ::recv(current_fd, buff, 4096, 0)) != 0) {
    //fprintf(stderr, "loop - %d\n", sz);
    if (sz == SOCKET_ERROR) {
      int_g err = WSAGetLastError();
      if (err == WSAEWOULDBLOCK)
	continue;
      // this shouldn't really occur.
      snprintf(message, len, "Socket error %d", err);
      break;
    } else {
      if (first) {
	if (buff[0] == '#') {
	  //fprintf(stderr, "check true\n");
	  its_a_cif = true;
	}
	first = false;
      }
      buff[sz] = '\0';
      // Todo - is this safe on Windows?
      // Convert Java tabs to line-feed for reader
      for (int_g i = 0; i < sz; i++)
	if (buff[i] == '\t')
	  buff[i] = '\n';
      data += buff;
      //fprintf(stderr, "Data\n%s\n", buff);
    }
  }
  closesocket(current_fd);
  current_fd = -1;
  if (sz != -1) {
    return true;
  } else
    return false;
}

#else

DLV::int_g DLV::cds_cif_socket::socket_fd = 0;

DLV::operation *DLV::cds_cif_socket::create(const bool bonds,
					    char message[], const int_g len)
{
  cds_cif_socket *op = 0;
  DLVreturn_type ok = DLV_OK;
  std::istringstream input(data);
  message[0] = '\0';
  bool has_bonds = false;
  model *structure = load_cif_file::read(input, "CDS", has_bonds,
					 true, message, len);
  if (structure == 0) {
    strncpy(message, "Create model failed", len - 1);
    ok = DLV_ERROR;
  } else {
    if (ok == DLV_OK) {
      structure->complete();
      op = new cds_cif_socket(structure);
      attach_base(op);
      if (bonds and !has_bonds)
	op->set_bond_all();
    } else
      delete structure;
  }
  if (ok != DLV_OK)
    message[len - 1] = '\0';
  //input.close();
  data = "";
  return op;
}

DLV::int_g DLV::cds_cif_socket::start_listening()
{
  if (socket_fd != 0)
    return 0;
  if ((socket_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    return 0;
  else {
    //fprintf(stderr, "Binding\n");
    sockaddr_in addr;
#ifdef __APPLE__
    memset(&addr, 0, sizeof(sockaddr_in));
#endif // __APPLE__
    addr.sin_family = AF_INET;
    addr.sin_port = htons((short)socket_number);
    addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    //addr.sin_addr.s_addr = INADDR_ANY;
    if (bind(socket_fd, (sockaddr *) &addr, sizeof(sockaddr_in)) == -1) {
      close(socket_fd);
      socket_fd = 0;
      fprintf(stderr, "%d - %s\n", errno, strerror(errno));
      return 0;
    } else
      listen(socket_fd, 1);
  }
  return socket_fd;
}

void DLV::cds_cif_socket::stop_listening()
{
  //if (current_fd != 0) {
  //  DLV::detach_event(current_fd, &(read), 0);
  //  close(current_fd);
  //  current_fd = 0;
  //}
  close(socket_fd);
  socket_fd = 0;
}

bool DLV::cds_cif_socket::read(char message[], const int_g len)
{
  /*
  if (current_fd != 0) {
    static char buff[4097];
    int_g s = ::read(current_fd, buff, 4096);
    if (s == -1) {
      // this shouldn't really occur.
      if (errno == EAGAIN)
	return;
    } else if (s == 0) { // or some other condition
      (void) create();
      data = "";
      DLV::detach_event(current_fd, &(read), 0);
      close(current_fd);
      current_fd = 0;
      fprintf(stderr, "EOF\n");
    } else {
      buff[4096] = '\0';
      data += buff;
      fprintf(stderr, "Data\n%s\n", buff);
    }
  } else {
    fprintf(stderr, "Accept\n");
    sockaddr addr;
    socklen_t s = sizeof(sockaddr);
    current_fd = accept(socket_fd, &addr, &s);
    DLV::attach_event(current_fd, &(read), 0);
  }
  */
  //fprintf(stderr, "Accept\n");
  sockaddr addr;
  socklen_t s = sizeof(sockaddr);
  int_g current_fd = accept(socket_fd, &addr, &s);
  int_g sz;
  static char buff[4097];
  bool first = true;
  //bool its_a_cif = false;
  while ((sz = ::read(current_fd, buff, 4096)) != 0) {
    //fprintf(stderr, "loop - %d\n", sz);
    if (sz == -1) {
      // this shouldn't really occur.
      if (errno != EAGAIN) {
	strncpy(message, strerror(errno), len);
	break;
      }
    } else {
      if (first) {
	if (buff[0] == '#') {
	  //fprintf(stderr, "check true\n");
	  /*its_a_cif = true*/;
	}
	first = false;
      }
      buff[sz] = '\0';
      // Todo - is this safe on Windows?
      // Convert Java tabs to line-feed for reader
      for (int_g i = 0; i < sz; i++)
	if (buff[i] == '\t')
	  buff[i] = '\n';
      data += buff;
      //fprintf(stderr, "Data\n%s\n", buff);
    }
  }
  close(current_fd);
  current_fd = -1;
  if (sz != -1) {
    /*operation *op = 0;
    if (its_a_cif) {
      //fprintf(stderr, "Read stuff\n");
      op = create(bonds, message, len);
    }
    data = "";
    //fprintf(stderr, "EOF, %p\n", op);
    return op;
    */
    return true;
  } else
    return false;
}

#  endif // WIN32

DLV::string DLV::cds_cif_socket::get_name() const
{
  return (get_model_name() + " - from CDS");
}

#endif // ENABLE_DLV_GRAPHICS

DLV::string DLV::load_pdb_file::get_name() const
{
  return (get_model_name() + " - Load PDB");
}

DLV::operation *DLV::load_pdb_file::create(const char name[],
					   const char filename[],
					   const int_g model_index,
					   const int_g conformer,
					   const int_g nconformers,
					   const bool bonds, char message[],
					   const int_g mlen)
{
  // Todo - do some of these structures still use bohr? (properties?)
  load_pdb_file *op = 0;
  DLVreturn_type ok = DLV_OK;
  if (pdb_file_data == 0) {
    strncpy(message, "PDB file object not initialised", mlen);
    return op;
  }
  model *structure = read(name_from_file(name, filename), model_index,
			  conformer, nconformers, message, mlen);
  if (structure == 0) {
    strncpy(message, "Create model failed", mlen - 1);
    ok = DLV_ERROR;
  } else {
    if (ok == DLV_OK) {
      structure->complete();
      op = new load_pdb_file(structure, filename);
      attach_base(op);
      if (bonds)
	op->set_bond_all();
    } else
      delete structure;
  }
  if (ok != DLV_OK)
    message[mlen - 1] = '\0';
  return op;
}

DLV::model *DLV::load_pdb_file::read(const string def_name,
				     const int_g model_index,
				     const int_g conformer,
				     const int_g nconformers,
				     char message[], const int_g mlen)
{
  model *m = 0;
  int_g count = 0;
  std::vector<iotbx::pdb::hierarchy::model>::iterator mptr;
  for (mptr = pdb_hierarchy.models_begin();
       mptr != pdb_hierarchy.models().end(); ++mptr) {
    if (count == model_index)
      break;
    count++;
  }
  if (mptr == pdb_hierarchy.models().end())
    strncpy(message, "Failed to find model in PDB file", mlen);
  else {
    scitbx::af::shared<std::string> cryst =
      pdb_file_data->crystallographic_section();
    int_g spgr = 1;
    float cell[6];
    char group_name[16];
    bool no_group_name = true;
    int_g i = 0;
    if (!cryst.empty()) {
      i = sscanf(cryst.front().c_str(), "CRYST1%9f%9f%9f%7f%7f%7f %10c %d",
		 &cell[0], &cell[1], &cell[2], &cell[3], &cell[4], &cell[5],
		 group_name, &spgr);
      group_name[10] = '\0';
      group_name[11] = '\0';
      group_name[12] = '\0';
      group_name[13] = '\0';
      group_name[14] = '\0';
      group_name[15] = '\0';
    }
    int_g dimension = 3;
    if (i < 6)
      dimension = 0;
    if (i < 7)
      spgr = 1;
    else
      no_group_name = false;
    //cryst = pdb_file_data->title_section();
    //fprintf(stderr, "%f %f %f %f %f %f %s %d\n", cell[0], cell[1], cell[2],
    //      cell[3], cell[4], cell[5], group_name, spgr);
    m = create_atoms(def_name, dimension);
    if (dimension > 0) {
      coord_type cl[6];
      cl[0] = real_l(cell[0]);
      cl[1] = real_l(cell[1]);
      cl[2] = real_l(cell[2]);
      cl[3] = real_l(cell[3]);
      cl[4] = real_l(cell[4]);
      cl[5] = real_l(cell[5]);
      m->set_lattice_parameters(cl[0], cl[1], cl[2], cl[3], cl[4], cl[5]);
    }
    if (no_group_name)
      m->set_group(spgr);
    else {
      for (i = 15; i > 1; i--) {
	if (isalnum(group_name[i]))
	  break;
	group_name[i] = '\0';
      }
      m->set_group(group_name);
    }
    m->add_atom_group("Chains");
    m->add_atom_group("Residues");
    m->add_atom_group("Alpha Carbons");
    // atoms
    std::vector<iotbx::pdb::hierarchy::chain>::iterator cptr;
    for (cptr = mptr->chains_begin();
	 cptr != mptr->chains().end(); ++cptr) {
      string chainid;
      if (cptr->data->id[0] == ' ') {
	if (isalnum(cptr->data->id[1]))
	  chainid = cptr->data->id[1];
	else
	  chainid = "none";
      } else {
	chainid = cptr->data->id[0];
	chainid += cptr->data->id[1];
      }
      std::vector<iotbx::pdb::hierarchy::residue_group>::iterator rptr;
      int_g n = nconformers;
      int_g conf = conformer;
      for (rptr = cptr->residue_groups_begin();
	   rptr != cptr->residue_groups().end(); ++rptr) {
	string resid = rptr->resid();
	std::vector<iotbx::pdb::hierarchy::atom_group>::iterator agptr;
	for (agptr = rptr->atom_groups_begin();
	     agptr != rptr->atom_groups().end(); ++agptr) {
	  //fprintf(stderr, "confid '%s'\n", agptr->confid().c_str());
	  int_g cid = conf % n;
	  // cid = 0 is 'A', 'A' + 1 = 'B' (cid 1)
	  if ((agptr->confid())[0] == ' ' or
	      (agptr->confid())[0] == ('A' + cid)) {
	    string resname = resid;
	    //resname += ":";
	    resname += agptr->confid().substr(1, 3);
	    std::vector<iotbx::pdb::hierarchy::atom>::iterator aptr;
	    for (aptr = agptr->atoms_begin();
		 aptr != agptr->atoms().end(); ++aptr) {
	      // Todo - occupancy? name?
	      coord_type coords[3];
	      coords[0] = aptr->data->xyz[0];
	      coords[1] = aptr->data->xyz[1];
	      coords[2] = aptr->data->xyz[2];
	      char symbol[3];
	      symbol[0] = aptr->data->element.elems[0];
	      bool set_charge = false;
	      bool carbon = false;
	      if (symbol[0] == ' ') {
		if (aptr->data->element.elems[1] == ' ') {
		  // Try and guess symbol from name
		  symbol[0] = aptr->data->name.elems[1];
		  strncpy(message, "Missing element field in PDB, "
			  "guessing atom types from name field", mlen);
		} else
		  symbol[0] = aptr->data->element.elems[1];
		symbol[1] = '\0';
		set_charge = (symbol[0] == 'H' or symbol[0] == 'C' or
			      symbol[0] == 'N' or symbol[0] == 'O' or
			      symbol[0] == 'S');
		carbon = (symbol[0] == 'C');
	      } else {
		symbol[1] = tolower(aptr->data->element.elems[1]);
		symbol[2] = '\0';
	      }
	      int_g index;
	      if (m->add_cartesian_atom(index, symbol, coords)) {
		// Todo - use charge from file?
		if (set_charge)
		  m->set_atom_charge(index, 0);
		if (carbon and aptr->data->name == " CA ")
		  m->set_atoms_group(index, "Alpha Carbons", "true");
		else
		  m->set_atoms_group(index, "Alpha Carbons", "false");
		m->set_atoms_group(index, "Chains", chainid);
		m->set_atoms_group(index, "Residues", resname);
	      }
	    }
	  }
	}
	if (n > 1) {
	  if (rptr->have_conformers()) {
	    int_g j = rptr->conformers().size();
	    conf /= j;
	    n /= j;
	    if (n == 0) // Shouldn't happen?
	      n = 1;
	  }
	}
      }
    }
  }
  return m;
}

DLV::int_g DLV::load_pdb_file::count_models(const char filename[],
					    char message[],
					    const int_g mlen)
{
  message[0] = '\0';
  if (pdb_file_data != 0) {
    delete pdb_file_data;
    pdb_file_data = 0;
  }
  int_g count = 0;
  if (check_filename(filename, message, mlen)) {
    pdb_file_data = new iotbx::pdb::input(filename);
    pdb_hierarchy = pdb_file_data->construct_hierarchy();
    scitbx::af::shared<std::string> names = pdb_file_data->model_ids();
    count = (int)names.size();
  }
  if (count == 0)
    strncpy(message, "No models found in PDB file", mlen);
  return count;
}

DLV::int_g DLV::load_pdb_file::count_conformers(const char filename[],
					 const int_g model, char message[],
					 const int_g mlen)
{
  message[0] = '\0';
  if (pdb_file_data == 0) {
    strncpy(message, "PDB file object not initialised", mlen);
    return 0;
  }
  int_g count = 0;
  std::vector<iotbx::pdb::hierarchy::model>::iterator ptr;
  for (ptr = pdb_hierarchy.models_begin();
       ptr != pdb_hierarchy.models().end(); ++ptr) {
    if (count == model)
      break;
    count++;
  }
  if (ptr == pdb_hierarchy.models().end())
    strncpy(message, "Failed to find model in PDB file", mlen);
  else {
    count = 1;
    std::vector<iotbx::pdb::hierarchy::chain>::iterator cptr;
    for (cptr = ptr->chains_begin(); cptr != ptr->chains().end(); ++cptr) {
      std::vector<iotbx::pdb::hierarchy::residue_group>::iterator rptr;
      for (rptr = cptr->residue_groups_begin();
	   rptr != cptr->residue_groups().end(); ++rptr) {
	if (rptr->have_conformers())
	  count *= rptr->conformers().size();
	if (count > 32768)
	  break;
      }
      if (count > 32768)
	break;
    }
    //if (count > 32768) {
      //strncpy(message, "Restricting conformers to 32768 most likely",
      //      mlen);
      //count = 32768;
    //}
    if (count == 0)
      strncpy(message, "No conformers found for PDB model", mlen);
  }
  return count;
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    /*
    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::load_atom_model_op *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::load_atom_model_op("recover");
    }
    */

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::create_model_op *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::create_model_op(0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::create_atom_model_op *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::create_atom_model_op(0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::load_cif_file *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::load_cif_file(0, "recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::load_pdb_file *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::load_pdb_file(0, "recover");
    }

  }
}

template <class Archive>
void DLV::change_geometry::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<operation>(*this);
}

template <class Archive>
void DLV::edit_geometry::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<change_geometry>(*this);
}

template <class Archive>
void DLV::load_model_op::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<load_op>(*this);
}

template <class Archive>
void DLV::load_atom_model_op::serialize(Archive &ar,
					const unsigned int version)
{
  ar & boost::serialization::base_object<load_model_op>(*this);
}

template <class Archive>
void DLV::load_cif_file::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<load_atom_model_op>(*this);
}

#ifdef ENABLE_DLV_GRAPHICS

template <class Archive>
void DLV::cds_cif_socket::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<load_cif_file>(*this);
}

#endif // ENABLE_DLV_GRAPHICS

template <class Archive>
void DLV::load_pdb_file::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<load_atom_model_op>(*this);
}

template <class Archive>
void DLV::create_model_op::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<change_geometry>(*this);
}

template <class Archive>
void DLV::create_atom_model_op::serialize(Archive &ar,
					  const unsigned int version)
{
  ar & boost::serialization::base_object<create_model_op>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::change_geometry)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::edit_geometry)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::load_model_op)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::load_atom_model_op)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::load_cif_file)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::load_pdb_file)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::create_model_op)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::create_atom_model_op)

DLV_SUPPRESS_TEMPLATES(DLV::operation)

DLV_NORMAL_EXPLICIT(DLV::change_geometry)
DLV_NORMAL_EXPLICIT(DLV::edit_geometry)
DLV_EXPORT_EXPLICIT(DLV::edit_geometry)
DLV_NORMAL_EXPLICIT(DLV::load_model_op)
DLV_NORMAL_EXPLICIT(DLV::load_atom_model_op)
DLV_NORMAL_EXPLICIT(DLV::load_cif_file)
DLV_NORMAL_EXPLICIT(DLV::load_pdb_file)
DLV_NORMAL_EXPLICIT(DLV::create_model_op)
DLV_NORMAL_EXPLICIT(DLV::create_atom_model_op)
DLV_EXPORT_EXPLICIT(DLV::load_atom_model_op)

#endif // DLV_USES_SERIALIZE
