
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "utils.hxx"
#include "model.hxx"
#include "operation.hxx"
#include "data_objs.hxx"
#include "data_simple.hxx"
#include "data_plots.hxx"
#include "data_surf.hxx"
#include "data_vol.hxx"
#include "op_objs.hxx"

DLV::operation *DLV::op_volume::create(const char name[], const bool kspace,
				       char message[], const int_g mlen)
{
  op_volume *op = nullptr;
  data_object *data = nullptr;
  if (kspace)
    data = new k_space_box(name);
  else
    data = new real_space_box(name);
  if (data == nullptr)
    strncpy(message, "Unable to allocate 3D object", 256);
  else {
    op = new op_volume(name);
    if (op == nullptr)
      strncpy(message, "Unable to allocate box", 256);
    else {
      op->attach_pending(true);
      op->attach_data(data);
#ifdef ENABLE_DLV_GRAPHICS
      op->render_data(data);
#endif // ENABLE_DLV_GRAPHICS
    }
  }
  return op;
}

DLV::operation *DLV::op_sphere::create(const char name[], const bool kspace,
				       char message[], const int_g mlen)
{
  op_sphere *op = nullptr;
  data_object *data = nullptr;
  if (kspace)
    data = nullptr; //new k_space_box(name);
  else
    data = new real_space_sphere(name);
  if (data == nullptr)
    strncpy(message, "Unable to allocate sphere", 256);
  else {
    op = new op_sphere(name);
    if (op == nullptr)
      strncpy(message, "Unable to allocate sphere", 256);
    else {
      op->attach_pending(true);
      op->attach_data(data);
#ifdef ENABLE_DLV_GRAPHICS
      op->render_data(data);
#endif // ENABLE_DLV_GRAPHICS
    }
  }
  return op;
}

DLV::operation *DLV::op_plane::create(const char name[], const bool kspace,
				      char message[], const int_g mlen)
{
  op_plane *op = nullptr;
  data_object *data = nullptr;
  if (kspace)
    data = new k_space_plane(name);
  else
    data = new real_space_plane(name);
  if (data == nullptr)
    strncpy(message, "Unable to allocate plane object", 256);
  else {
    op = new op_plane(name);
    if (op == nullptr)
      strncpy(message, "Unable to allocate plane", 256);
    else {
      op->attach_pending(true);
      op->attach_data(data);
#ifdef ENABLE_DLV_GRAPHICS
      op->render_data(data);
#endif // ENABLE_DLV_GRAPHICS
    }
  }
  return op;
}

DLV::operation *DLV::op_line::create(const char name[], const bool kspace,
				     char message[], const int_g mlen)
{
  op_line *op = nullptr;
  data_object *data = nullptr;
  if (kspace)
    data = new k_space_line(name);
  else
    data = new real_space_line(name);
  if (data == nullptr)
    strncpy(message, "Unable to allocate line object", 256);
  else {
    op = new op_line(name);
    if (op == nullptr)
      strncpy(message, "Unable to allocate line", 256);
    else {
      op->attach_pending(true);
      op->attach_data(data);
#ifdef ENABLE_DLV_GRAPHICS
      op->render_data(data);
#endif // ENABLE_DLV_GRAPHICS
    }
  }
  return op;
}

void DLV::op_line::create_path(const real_g points[][3], const string labels[],
			       const int_g n, const bool kspace, char message[],
			       const int_g mlen)
{
  bool attached = false;
  for (int_g i = 0; i < n; i++) {
    if (labels[i].length() > 0) {
      if (!attached) {
	attach_pending(true);
	attached = true;
      }
      int_g j;
      for (j = 0; j < i; j++)
	if (labels[j] == labels[i])
	  break;
      if (j == i) {
	DLV::point *p = 0;
	if (kspace)
	  p = new DLV::k_space_point(labels[i].c_str());
	else
	  p = new DLV::real_space_point(labels[i].c_str());
	attach_data(p);
#ifdef ENABLE_DLV_GRAPHICS
	render_data(p);
	p->update0D(0, points[i][0], points[i][1], points[i][2], true,
		    get_model(), message, mlen);
#endif // ENABLE_DLV_GRAPHICS
      }
    }
    if (i > 0) {
      if (!attached) {
	attach_pending(true);
	attached = true;
      }
      DLV::string name = labels[i - 1];
      name += " to ";
      name += labels[i];
      DLV::line *l = 0;
      if (kspace)
	l = new DLV::k_space_line(name.c_str());
      else
	l = new DLV::real_space_line(name.c_str());
      attach_data(l);
#ifdef ENABLE_DLV_GRAPHICS
      render_data(l);
      l->update1D(6, 0, 0, 0, points[i - 1][0], points[i - 1][1],
		  points[i - 1][2], 0, true, true, false,
		  get_model(), message, mlen);
      l->update1D(5, 0, 0, 0, points[i][0], points[i][1],
		  points[i][2], 0, true, true, false,
		  get_model(), message, mlen);
#endif // ENABLE_DLV_GRAPHICS
    }
  }
  accept_pending();
}

DLV::operation *DLV::op_point::create(const char name[], const bool kspace,
				      char message[], const int_g mlen)
{
  op_point *op = nullptr;
  data_object *data = nullptr;
  if (kspace)
    data = new k_space_point(name);
  else
    data = new real_space_point(name);
  if (data == nullptr)
    strncpy(message, "Unable to allocate point object", 256);
  else {
    op = new op_point(name);
    if (op == nullptr)
      strncpy(message, "Unable to allocate point", 256);
    else {
      op->attach_pending(true);
      op->attach_data(data);
#ifdef ENABLE_DLV_GRAPHICS
      op->render_data(data);
#endif // ENABLE_DLV_GRAPHICS
    }
  }
  return op;
}

DLV::string DLV::op_volume::get_name() const
{
  string label = "Create 3D region - ";
  label += name;
  return label;
}

void DLV::op_volume::add_standard_data_objects()
{
  // Do nothing
}

bool DLV::op_volume::reload_data(class data_object *data, char message[],
				 const int_g mlen)
{
  // Do nothing
  return true;
}

DLV::string DLV::op_sphere::get_name() const
{
  string label = "Create sphere - ";
  label += name;
  return label;
}

void DLV::op_sphere::add_standard_data_objects()
{
  // Do nothing
}

bool DLV::op_sphere::reload_data(class data_object *data, char message[],
				 const int_g mlen)
{
  // Do nothing
  return true;
}

DLV::string DLV::op_plane::get_name() const
{
  string label = "Create plane - ";
  label += name;
  return label;
}

void DLV::op_plane::add_standard_data_objects()
{
  // Do nothing
}

bool DLV::op_plane::reload_data(class data_object *data, char message[],
				const int_g mlen)
{
  // Do nothing
  return true;
}

DLV::string DLV::op_line::get_name() const
{
  string label = "Create line - ";
  label += name;
  return label;
}

void DLV::op_line::add_standard_data_objects()
{
  // Do nothing
}

bool DLV::op_line::reload_data(class data_object *data, char message[],
			       const int_g mlen)
{
  // Do nothing
  return true;
}

DLV::string DLV::op_point::get_name() const
{
  string label = "Create point - ";
  label += name;
  return label;
}

void DLV::op_point::add_standard_data_objects()
{
  // Do nothing
}

bool DLV::op_point::reload_data(class data_object *data, char message[],
				const int_g mlen)
{
  // Do nothing
  return true;
}

DLV::operation *DLV::op_wulff::create(const char name[],
				      char message[], const int_g mlen)
{
  op_wulff *op = nullptr;
  data_object *data = nullptr;
  data = new wulff_plot(name);
  if (data == nullptr)
    strncpy(message, "Unable to allocate wulff object", 256);
  else {
    op = new op_wulff(name);
    if (op == nullptr)
      strncpy(message, "Unable to allocate wulff", 256);
    else {
      op->attach_pending(true);
      op->attach_data(data);
    }
  }
  return op;
}

DLV::string DLV::op_wulff::get_name() const
{
  string label = "Create Wulff Plot - ";
  label += name;
  return label;
}

void DLV::op_wulff::add_standard_data_objects()
{
  // Do nothing
}

bool DLV::op_wulff::reload_data(class data_object *data, char message[],
				const int_g mlen)
{
  // Do nothing
  return true;
}

DLV::operation *DLV::op_leed::create(char message[], const int_g mlen)
{
  op_leed *op = nullptr;
  model *base = nullptr;
  int_g supercell[2][2] = { {1, 0}, {0, 1} };
  model *m = get_current_model();
  if (m->get_model_type() == 4) {
    if (m->has_salvage()) {
      operation *parent = find_current_surface_parent(supercell);
      if (parent != nullptr)
	base = parent->get_model();
    }
  }
  data_object *data = new leed_pattern(base, supercell);
  if (data == nullptr)
    strncpy(message, "Unable to allocate LEED object", 256);
  else {
    op = new op_leed();
    if (op == nullptr)
      strncpy(message, "Unable to allocate LEED", 256);
    else {
      op->attach_pending(true);
      op->attach_data(data);
#ifdef ENABLE_DLV_GRAPHICS
      op->render_data(data);
#endif // ENABLE_DLV_GRAPHICS
    }
  }
  return op;
}

DLV::string DLV::op_leed::get_name() const
{
  string label = "Create LEED pattern";
  return label;
}

void DLV::op_leed::add_standard_data_objects()
{
  // Do nothing
}

bool DLV::op_leed::reload_data(class data_object *data, char message[],
			       const int_g mlen)
{
  // Do nothing
  return true;
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::op_volume *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::op_volume("create");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::op_sphere *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::op_sphere("create");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::op_plane *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::op_plane("create");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::op_line *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::op_line("create");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::op_point *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::op_point("create");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::op_wulff *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::op_wulff("create");
    }

  }
}

template <class Archive>
void DLV::op_volume::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<operation>(*this);
  ar & name;
}

template <class Archive>
void DLV::op_sphere::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<operation>(*this);
  ar & name;
}

template <class Archive>
void DLV::op_plane::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<operation>(*this);
  ar & name;
}

template <class Archive>
void DLV::op_line::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<operation>(*this);
  ar & name;
}

template <class Archive>
void DLV::op_point::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<operation>(*this);
  ar & name;
}

template <class Archive>
void DLV::op_wulff::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<operation>(*this);
  ar &name;
}

template <class Archive>
void DLV::op_leed::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<operation>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::op_volume)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::op_sphere)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::op_plane)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::op_line)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::op_point)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::op_wulff)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::op_leed)

DLV_SUPPRESS_TEMPLATES(DLV::operation)

DLV_NORMAL_EXPLICIT(DLV::op_volume)
DLV_NORMAL_EXPLICIT(DLV::op_sphere)
DLV_NORMAL_EXPLICIT(DLV::op_plane)
DLV_NORMAL_EXPLICIT(DLV::op_line)
DLV_NORMAL_EXPLICIT(DLV::op_point)
DLV_NORMAL_EXPLICIT(DLV::op_wulff)
DLV_NORMAL_EXPLICIT(DLV::op_leed)

#endif // DLV_USES_SERIALIZE
