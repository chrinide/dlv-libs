
#ifndef DLV_CONSTANTS
#define DLV_CONSTANTS

#ifndef WIN32
  #include <cmath>
#endif // WIN32

namespace DLV {

#ifdef WIN32
  const double pi = 3.1415926535897932385E0;
#else
  const double pi = M_PI;
#endif // WIN32

  // 2010 values from http://physics.nist.gov/cuu/Constants/index.html
  const real_l bohr_to_angstrom = 0.52917721092;
  const real_l Hartree_to_eV = 27.21138505;
  // NIST converter m^-1 to K in energy equivalents
  const real_l inv_cm_to_K = 1.4387770;

  template <class real> real to_radians(const real x);
  template <class real> real to_degrees(const real x);

}

template <class real>
inline real DLV::to_radians(const real x)
{
  return (x * pi / 180.0);
}

template <class real>
inline real DLV::to_degrees(const real x)
{
  return (x * 180.0 / pi);
}

#endif // DLV_CONSTANTS
