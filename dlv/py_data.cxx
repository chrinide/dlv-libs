
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "operation.hxx"
#include "extern_data.hxx"
#include "boost/python.hpp"

#define DLV_MODULE_NAME data

BOOST_PYTHON_MODULE(DLV_MODULE_NAME)
{
  using namespace boost::python;
  /*
  def("create_volume", DLV::create_volume);
  def("create_plane", DLV::create_plane);
  def("create_line", DLV::create_line);
  def("create_sphere", DLV::create_sphere);
  def("create_point", DLV::create_point);
  def("create_wulff", DLV::create_wulff);
  def("create_leed", DLV::create_leed);
  */

#ifdef ENABLE_DLV_GRAPHICS

  /*
  def("update_current_slice", DLV::update_current_slice);
  */
  
#endif // ENABLE_DLV_GRAPHICS
}
