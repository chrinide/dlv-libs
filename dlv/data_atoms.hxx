
#ifndef DLV_ATOM_BASED_DATA
#define DLV_ATOM_BASED_DATA

#include <vector>

namespace DLV {

  class atom_based_data : public reloadable_data {
  public:
    bool is_displayable() const;
    bool is_editable() const;
    string get_edit_label() const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

  protected:
    atom_based_data(const string code, const string src, operation *p,
		    const bool show = true);

#ifdef ENABLE_DLV_GRAPHICS
    void reset_data_sets();
#endif // ENABLE_DLV_GRAPHICS

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class atom_scalars : public atom_based_data {
  public:
    atom_scalars(const string code, const string src,
		 operation *p, const string id,
		 const bool show = true);
    ~atom_scalars();

    string get_obj_label() const;
    int_g get_number_data_sets() const;
    string get_data_label() const;
    string get_edit_label() const;
    string get_sub_data_label(const int_g n) const;
    void set_grid(real_g grid[][3], const int_g n, const bool copy_data = true);
    void set_grid(const atom_scalars *copy);
    void reset_data_sets();
    void unload_data();

#ifdef ENABLE_DLV_GRAPHICS
    bool update_atom_selection_info(const model *m, char message[],
				    const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  protected:
    void add_data(const string label);

    int_g get_ndata_sets() const;
    int_g get_data_size() const;
    int_g get_data_length() const;
    real_g (*get_grid())[3];
    const real_g (*get_grid() const)[3];
    string *get_labels() const;

#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render_data(const class model *structure,
			       char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  private:
    string name;
    int_g natoms;
    int_g n_data_sets;
    real_g (*coords)[3];
    string *labels;
    int_g size;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class atom_integers : public atom_scalars {
  public:
    atom_integers(const string code, const string src,
		  operation *p, const string id,
		  const bool show = true);
    ~atom_integers();

    void add_data(int_g values[], const string label,
		  const bool copy_data = false);
    bool swop_two_data_items(const int_g item1, const int_g item2, 
			     const int_g dataset, const class model *structure);
#ifdef ENABLE_DLV_GRAPHICS
    void reset_data_sets();
    void set_3D_display(const class model *structure);
    void unset_3D_display(const class model *structure);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
    bool update_cell_info(model *m, char message[], const int_g mlen);
    bool map_atom_selections(const model *m, int_g &n, int_g * &labels,
			     int_g * &atom_types, atom * &parents,
			     const bool all_atoms) const;
    bool map_selected_atoms(const model *m, int_g &n, int_g * &labels,
			    int_g (* &shifts)[3]) const;
    bool hide_render(const int_g object, const int_g visible,
		     const render_parent *parent, class model *structure,
		     char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  private:
    int_g **data;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class atom_reals : public atom_scalars {
  public:
    atom_reals(const string code, const string src,
	       operation *p, const string id);
    ~atom_reals();

    void add_data(real_g values[], const string label,
		  const bool copy_data = false);

#ifdef ENABLE_DLV_GRAPHICS
    void reset_data_sets();
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
    bool update_cell_info(model *m, char message[], const int_g mlen);
    bool hide_render(const int_g object, const int_g visible,
		     const render_parent *parent, class model *structure,
		     char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  private:
    real_g **data;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class phonon_frequency {
  public:
    phonon_frequency();
    ~phonon_frequency();

    int_g natoms;
    real_g (*mags)[3];
    real_g (*phases)[3];
    real_g frequency;
    real_g ir_intensity;
    real_g raman_intensity;
    string symbol;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class phonon_kpoint {
  public:
    phonon_kpoint();
    ~phonon_kpoint();

    int_g nfrequencies;
    real_g ka;
    real_g kb;
    real_g kc;
    phonon_frequency *phonons;
    bool has_phases;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class phonon_vectors : public atom_based_data {
  public:
    phonon_vectors(const string code, const string src, operation *p,
		   const int_g nf, const int_g nk);
    ~phonon_vectors();

    // Todo - put grid into base atom_based_data?
    void set_grid(real_g grid[][3], const int_g n, const bool copy_data = true);
    bool add_data(const int_g kp, const int_g fp, const real_g ka,
		  const real_g kb, const real_g kc, const real_g freq,
		  real_g mags[][3], const int_g length,
		  const bool copy_data = true);
    bool add_data(const int_g kp, const int_g fp, const real_g ka,
		  const real_g kb, const real_g kc, const real_g freq,
		  const string sym, real_g mags[][3], const int_g length,
		  const bool copy_data = true);
    bool add_data(const int_g kp, const int_g fp, const real_g ka,
		  const real_g kb, const real_g kc, const real_g freq,
		  real_g mags[][3], real_g phases[][3], const int_g length,
		  const bool copy_data = true);
    bool add_data(const int_g kp, const int_g fp, const real_g ka,
		  const real_g kb, const real_g kc, const real_g freq,
		  const string sym, real_g mags[][3], real_g phases[][3],
		  const int_g length, const bool copy_data = true);

    void unload_data();

    bool is_editable() const;
    string get_data_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_edit_type() const;
    string get_edit_label() const;

#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
    bool update_cell_info(model *m, char message[], const int_g mlen);
    data_object *edit(const render_parent *parent,
		      class model *structure, const int_g component,
		      const int_g method, const int_g index, char message[],
		      const int_g mlen);
    bool test_gamma_point(const int_g kp) const;
    void reset_data_sets();
    bool hide_render(const int_g object, const int_g visible,
		     const render_parent *parent, class model *structure,
		     char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

    bool generate_spectrum(real_g grid[], real_g data[], const int_g type,
			   const real_g emin, const real_g emax, const int_g np,
			   const real_g w, const real_g harmonic,
			   char message[], const int_g mlen);
    bool generate_frames(std::vector<real_g (*)[3]> &map_data,
			 const class model *structure, const int_g component,
			 const int_g nframes, const real_g scale,
			 const bool use_temp, const real_g temp,
			 char message[], const int_g mlen);
    void get_kpoint_cell(int_g kc[3], int_g kpoint) const;

  protected:
#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render_data(const class model *structure,
			       char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

    void set_intensities(const int_g kp, const int_g fp, const real_g intensity,
			 const int_g ir, const int_g raman);

  private:
    int_g nfrequencies;
    int_g n_k_points;
    int_g natoms;
    real_g (*coords)[3];
    phonon_kpoint *vectors;

    void widen_lines(const int_g kp, real_g data[], const int_g type,
		     const real_g min_energy, const real_g max_energy,
		     const real_g energy_step, const real_g scale,
		     const int_g npoints);
    void convolute(real_g data[], real_g temp[], const real_g sigma,
		   const int_g npoints);

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class vectors_and_intensity : public phonon_vectors {
  public:
    vectors_and_intensity(const string code, const string src, operation *p,
			  const int_g nf, const int_g nk);

    bool add_data(const int_g kp, const int_g fp, const real_g ka,
		  const real_g kb, const real_g kc, const real_g freq,
		  const real_g intensity, const int_g ir, const int_g raman,
		  const string sym, real_g mags[][3], const int_g length,
		  const bool copy_data = true);

    int_g get_edit_type() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class md_trajectory : public atom_based_data {
  public:
    md_trajectory(const string code, const string src, operation *p,
		  const int_g n, const real_g t);
    ~md_trajectory();

    void set_data(real_g (*c)[3], const coord_type coords[][3],
		  const int_g n, const int_g s);

    void unload_data();

    string get_data_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;

#ifdef ENABLE_DLV_GRAPHICS
    int_g get_nframes() const;
    const std::vector<real_g (*)[3]> *get_trajectory() const;
    bool is_gamma_point() const;
    void update_trajectory(class model *structure);

    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
    bool hide_render(const int_g object, const int_g visible,
		     const render_parent *parent, class model *structure,
		     char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  protected:
#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render_data(const class model *structure,
			       char message[], const int_g mlen);

    void reset_data_sets();
#endif // ENABLE_DLV_GRAPHICS

  private:
    int_g nsteps;
    real_g time_step;
    int_g natoms;
    real_g (*atom_coords)[3];
    std::vector<real_g (*)[3]> trajectory;
    std::vector<real_g (*)[3]> vectors;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

#ifdef ENABLE_DLV_GRAPHICS
  class edit_atom_object : public edit_object {
  protected:
    edit_atom_object(data_object *d, edited_obj *e, const int_g i,
		     const string s);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class edit_atom_spectrum : public edit_atom_object {
  public:
    static edit_atom_spectrum *create(const render_parent *parent,
				      data_object *data, const int_g component,
				      const string name, const int_g index,
				      char message[], const int_g mlen);

    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

    bool hide_render(const int_g object, const int_g visible,
		     const render_parent *parent, class model *structure,
		     char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
    DLVreturn_type update_spectrum(const int_g type, const real_g emin,
				   const real_g emax, const int_g np,
				   const real_g w, const real_g harmonic,
				   char message[], const int_g mlen);

    // Only public for serialization
    edit_atom_spectrum(data_object *d, edited_obj *e, const int_g i,
		       const string s);

  protected:
    void set_values(const int_g type, const real_g emin, const real_g emax,
		    const int_g np, const real_g w, const real_g harmonic);

  private:
    int_g spectra_type;
    real_g min_energy;
    real_g max_energy;
    int_g npoints;
    real_g width;
    real_g harmonic_scale;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class edit_phonon_trajectory : public edit_atom_object {
  public:
    static edit_phonon_trajectory *create(const render_parent *parent,
					  data_object *data,
					  class model *structure,
					  const int_g component,
					  const string name, const int_g index,
					  const int_g nfreq, char message[],
					  const int_g mlen);
    ~edit_phonon_trajectory();

    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

    int_g get_nframes() const;
    const std::vector<real_g (*)[3]> *get_trajectory() const;
    bool is_gamma_point() const;
    void get_kpoint_cell(int_g kc[3]) const;
    void update_trajectory(class model *structure);

    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
    DLVreturn_type update_phonon(const render_parent *parent,
				 class model *structure,
				 const int_g nframes, const real_g scale,
				 const bool use_temp, const real_g temp,
				 char message[], const int_g mlen);
    bool hide_render(const int_g object, const int_g visible,
		     const render_parent *parent, class model *structure,
		     char message[], const int_g mlen);

    // Only public for serialization
    edit_phonon_trajectory(data_object *d, edited_obj *e, const int_g i,
			   const string s, const int_g c,
			   const int_g k, const int_g f, class model *st);

  private:
    int_g component;
    int_g kpoint;
    int_g fpoint;
    int_g number_of_frames;
    real_g magnitude;
    bool use_temperature;
    real_g temperature;
    class model *geom;
    std::vector<real_g (*)[3]> vectors;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };
#endif // ENABLE_DLV_GRAPHICS

}

#ifdef DLV_USES_SERIALIZE
BOOST_SERIALIZATION_ASSUME_ABSTRACT(DLV::atom_based_data)
BOOST_SERIALIZATION_ASSUME_ABSTRACT(DLV::atom_scalars)

BOOST_CLASS_EXPORT_KEY(DLV::atom_integers)
BOOST_CLASS_EXPORT_KEY(DLV::atom_reals)
BOOST_CLASS_EXPORT_KEY(DLV::phonon_vectors)
BOOST_CLASS_EXPORT_KEY(DLV::vectors_and_intensity)
BOOST_CLASS_EXPORT_KEY(DLV::md_trajectory)

#  ifdef ENABLE_DLV_GRAPHICS
BOOST_SERIALIZATION_ASSUME_ABSTRACT(DLV::edit_atom_object)
BOOST_CLASS_EXPORT_KEY(DLV::edit_atom_spectrum)
BOOST_CLASS_EXPORT_KEY(DLV::edit_phonon_trajectory)
#  endif // ENABLE_DLV_GRAPHICS
#endif // DLV_USES_SERIALIZE

inline DLV::atom_based_data::atom_based_data(const string code,
					     const string src, operation *p,
					     const bool show)
  : reloadable_data(code, src, p, show)
{
}

inline DLV::atom_scalars::atom_scalars(const string code, const string src,
				       operation *p, const string id,
				       const bool show)
  : atom_based_data(code, src, p, show), name(id), natoms(0), n_data_sets(0),
    coords(0), labels(0), size(0)
{
}

inline DLV::int_g DLV::atom_scalars::get_ndata_sets() const
{
  return n_data_sets;
}

inline DLV::int_g DLV::atom_scalars::get_data_size() const
{
  return size;
}

inline DLV::int_g DLV::atom_scalars::get_data_length() const
{
  return natoms;
}

inline DLV::real_g (*DLV::atom_scalars::get_grid())[3]
{
  return coords;
}

inline const DLV::real_g (*DLV::atom_scalars::get_grid() const)[3]
{
  return coords;
}

inline DLV::string *DLV::atom_scalars::get_labels() const
{
  return labels;
}

inline DLV::atom_integers::atom_integers(const string code, const string src,
					 operation *p, const string id,
					 const bool show)
  : atom_scalars(code, src, p, id, show), data(0)
{
}

inline DLV::atom_reals::atom_reals(const string code, const string src,
				   operation *p, const string id)
  : atom_scalars(code, src, p, id), data(0)
{
}

inline DLV::phonon_frequency::phonon_frequency()
  : mags(0), phases(0), frequency(0.0), ir_intensity(0.0),
    raman_intensity(0.0)
{
}

inline DLV::phonon_frequency::~phonon_frequency()
{
  delete [] mags;
  delete [] phases;
}

inline DLV::phonon_kpoint::phonon_kpoint()
  : ka(0.0), kb(0.0), kc(0.0), phonons(0), has_phases(false)
{
}

inline DLV::phonon_kpoint::~phonon_kpoint()
{
  delete [] phonons;
}
inline DLV::phonon_vectors::phonon_vectors(const string code, const string src,
					   operation *p, const int_g nf,
					   const int_g nk)
  : atom_based_data(code, src, p), nfrequencies(nf), n_k_points(nk),
    natoms(0), coords(0), vectors(new phonon_kpoint[nk])
{
}

inline DLV::vectors_and_intensity::vectors_and_intensity(const string code,
							 const string src,
							 operation *p,
							 const int_g nf,
							 const int_g nk)
  : phonon_vectors(code, src, p, nf, nk)
{
}

inline DLV::md_trajectory::md_trajectory(const string code, const string src,
					 operation *p, const int_g n,
					 const real_g t)
  : atom_based_data(code, src, p), nsteps(n), time_step(t), natoms(0),
    trajectory(n)
{
}

#  ifdef ENABLE_DLV_GRAPHICS

inline DLV::edit_atom_object::edit_atom_object(data_object *d, edited_obj *e,
					       const int_g i, const string s)
  : edit_object(d, e, i, s)
{
}

inline DLV::edit_atom_spectrum::edit_atom_spectrum(data_object *d,
						   edited_obj *e, const int_g i,
						   const string s)
  : edit_atom_object(d, e, i, s)
{
}

inline
void DLV::edit_atom_spectrum::set_values(const int_g type, const real_g emin,
					 const real_g emax, const int_g np,
					 const real_g w, const real_g harmonic)
{
  spectra_type = type;
  min_energy = emin;
  max_energy = emax;
  npoints = np;
  width = w;
  harmonic_scale = harmonic;
}

inline DLV::edit_phonon_trajectory::edit_phonon_trajectory(data_object *d,
							   edited_obj *e,
							   const int_g i,
							   const string s,
							   const int_g c,
							   const int_g k,
							   const int_g f,
							   model *st)
  : edit_atom_object(d, e, i, s), component(c), kpoint(k), fpoint(f),
    number_of_frames(0), magnitude(0.0), use_temperature(false),
    temperature(0.0), geom(st)
{
}

#  endif // ENABLE_DLV_GRAPHICS

#endif // DLV_ATOM_BASED_DATA
