
#ifndef DLV_DATA_EXPORTS
#define DLV_DATA_EXPORTS

namespace DLV {

  bool create_volume(const char name[], const bool kspace);
  bool create_sphere(const char name[], const bool kspace);
  bool create_plane(const char name[], const bool kspace);
  bool create_line(const char name[], const bool kspace);
  bool create_point(const char name[], const bool kspace);
  bool create_wulff(const char name[]);
  bool create_leed();

#ifdef ENABLE_DLV_GRAPHICS

  DLVreturn_type update_current_slice(const int_g object, const int_g plane,
				      char message[], const int_g mlen);
  DLVreturn_type update_current_cut(const int_g object, const int_g plane,
				    char message[], const int_g mlen);
  DLVreturn_type update_current_extension(const int_g object, const int_g na,
					  const int_g nb, const int_g nc,
					  char message[], const int_g mlen);

#endif // ENABLE_DLV_GRAPHICS

}

#endif // DLV_DATA_EXPORTS
