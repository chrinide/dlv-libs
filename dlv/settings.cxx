
#include "types.hxx"
#include "settings.hxx"

namespace {

  struct group_setting_data {
    DLV::int_g index;
    DLV::int_g norigins;
    DLV::string label;
  };

  // Todo - is there a way to use cctbx to get this info?

  group_setting_data gp_settings[] = {
    {
       1,
      //setting= "0",
       1,
      "P 1",
      //hall = " P 1\0"
    },
    {
       2,
      //setting= "0",
       1,
      "P -1",
      //hall = "-P 1\0"
    },
    {
       3,
      //setting = "qf::b",
       1,
      "P 1 2 1",
      //hall = " P 2y\0"
    },
    {
       3,
      //setting = "qf::c",
       1,
      "P 1 1 2",
      //hall = " P 2\0"
    },
    {
       3,
      //setting = "qf::a",
       1,
      "P 2 1 1",
      //hall = " P 2x\0"
    },
    {
       4,
      //setting = "qf::b",
       1,
      "P 1 21 1",
      //hall = " P 2yb\0"
    },
    {
       4,
      //setting = "qf::c",
       1,
      "P 1 1 21",
      //hall = " P 2c\0"
    },
    {
       4,
      //setting = "qf::a",
       1,
      "P 21 1 1",
      //hall = " P 2xa\0"
    },
    {
       6,
      //setting = "qf::b",
       1,
      "P 1 m 1",
      //hall = " P -2y\0"
    },
    {
       6,
      //setting = "qf::c",
       1,
      "P 1 1 m",
      //hall = " P -2\0"
    },
    {
       6,
      //setting = "qf::a",
       1,
      "P m 1 1",
      //hall = " P -2x\0"
    },
    {
       7,
      //setting = "qf::b1",
       1,
      "P 1 c 1",
      //hall = " P -2yc\0"
    },
    {
       7,
      //setting = "qf::b2",
       1,
      "P 1 n 1",
      //hall = " P -2yac\0"
    },
    {
       7,
      //setting = "qf::b3",
       1,
      "P 1 a 1",
      //hall = " P -2ya\0"
    },
    {
       7,
      //setting = "qf::c1",
       1,
      "P 1 1 a",
      //hall = " P -2a\0"
    },
    {
       7,
      //setting = "qf::c2",
       1,
      "P 1 1 n",
      //hall = " P -2ab\0"
    },
    {
       7,
      //setting = "qf::c3",
       1,
      "P 1 1 b",
      //hall = " P -2b\0"
    },
    {
       7,
      //setting = "qf::a1",
       1,
      "P b 1 1",
      //hall = " P -2xb\0"
    },
    {
       7,
      //setting = "qf::a2",
       1,
      "P n 1 1",
      //hall = " P -2xbc\0"
    },
    {
       7,
      //setting = "qf::a3",
       1,
      "P c 1 1",
      //hall = " P -2xc\0"
    },
    {
      10,
      //setting = "qf::b",
       1,
      "P 1 2/m 1",
      //hall = "-P 2y\0"
    },
    {
      10,
      //setting = "qf::c",
       1,
      "P 1 1 2/m",
      //hall = "-P 2\0"
    },
    {
      10,
      //setting = "qf::a",
       1,
      "P 2/m 1 1",
      //hall = "-P 2x\0"
    },
    {
      11,
      //setting = "qf::b",
       1,
      "P 1 21/m 1",
      //hall = "-P 2yb\0"
    },
    {
      11,
      //setting = "qf::c",
       1,
      "P 1 1 21/m",
      //hall = "-P 2c\0"
    },
    {
      11,
      //setting = "qf::a",
       1,
      "P 21/m 1 1",
      //hall = "-P 2xa\0"
    },
    {
      13,
      //setting = "qf::b1",
       1,
      "P 1 2/c 1",
      //hall = "-P 2yc\0"
    },
    {
      13,
      //setting = "qf::b2",
       1,
      "P 1 2/n 1",
      //hall = "-P 2yac\0"
    },
    {
      13,
      //setting = "qf::b3",
       1,
      "P 1 2/a 1",
      //hall = "-P 2ya\0"
    },
    {
      13,
      //setting = "qf::c1",
       1,
      "P 1 1 2/a",
      //hall = "-P 2a\0"
    },
    {
      13,
      //setting = "qf::c2",
       1,
      "P 1 1 2/n",
      //hall = "-P 2ab\0"
    },
    {
      13,
      //setting = "qf::c3",
       1,
      "P 1 1 2/b",
      //hall = "-P 2b\0"
    },
    {
      13,
      //setting = "qf::a1",
       1,
      "P 2/b 1 1",
      //hall = "-P 2xb\0"
    },
    {
      13,
      //setting = "qf::a2",
       1,
      "P 2/n 1 1",
      //hall = "-P 2xbc\0"
    },
    {
      13,
      //setting = "qf::a3",
       1,
      "P 2/c 1 1",
      //hall = "-P 2xc\0"
    },
    {
      14,
      //setting = "qf::b1",
       1,
      "P 1 21/c 1",
      //hall = "-P 2ybc\0"
    },
    {
      14,
      //setting = "qf::b2",
       1,
      "P 1 21/n 1",
      //hall = "-P 2yn\0"
    },
    {
      14,
      //setting = "qf::b3",
       1,
      "P 1 21/a 1",
      //hall = "-P 2yab\0"
    },
    {
      14,
      //setting = "qf::c1",
       1,
      "P 1 1 21/a",
      //hall = "-P 2ac\0"
    },
    {
      14,
      //setting = "qf::c2",
       1,
      "P 1 1 21/n",
      //hall = "-P 2n\0"
    },
    {
      14,
      //setting = "qf::c3",
       1,
      "P 1 1 21/b",
      //hall = "-P 2bc\0"
    },
    {
      14,
      //setting = "qf::a1",
       1,
      "P 21/b 1 1",
      //hall = "-P 2xab\0"
    },
    {
      14,
      //setting = "qf::a2",
       1,
      "P 21/n 1 1",
      //hall = "-P 2xn\0"
    },
    {
      14,
      //setting = "qf::a3",
       1,
      "P 21/c 1 1",
      //hall = "-P 2xac\0"
    },
    {
       5,
      //setting = "qf::b2",
       1,
      "A 1 2 1",
      //hall = " A 2y\0"
    },
    {
       5,
      //setting = "qf::c1",
       1,
      "A 1 1 2",
      //hall = " A 2\0"
    },
    {
       8,
      //setting = "qf::b2",
       1,
      "A 1 m 1",
      //hall = " A -2y\0"
    },
    {
       8,
      //setting = "qf::c1",
       1,
      "A 1 1 m",
      //hall = " A -2\0"
    },
    {
       9,
      //setting = "qf::b2",
       1,
      "A 1 n 1",
      //hall = " A -2yab\0"
    },
    {
       9,
      //setting = "qf::mb1",
       1,
      "A 1 a 1",
      //hall = " A -2ya\0"
    },
    {
       9,
      //setting = "qf::c1",
       1,
      "A 1 1 a",
      //hall = " A -2a\0"
    },
    {
       9,
      //setting = "qf::mc2",
       1,
      "A 1 1 n",
      //hall = " A -2ab\0"
    },
    {
      12,
      //setting = "qf::b2",
       1,
      "A 1 2/m 1",
      //hall = "-A 2y\0"
    },
    {
      12,
      //setting = "qf::c1",
       1,
      "A 1 1 2/m",
      //hall = "-A 2\0"
    },
    {
      15,
      //setting = "qf::b2",
       1,
      "A 1 2/n 1",
      //hall = "-A 2yab\0"
    },
    {
      15,
      //setting = "qf::mb1",
       1,
      "A 1 2/a 1",
      //hall = "-A 2ya\0"
    },
    {
      15,
      //setting = "qf::c1",
       1,
      "A 1 1 2/a",
      //hall = "-A 2a\0"
    },
    {
      15,
      //setting = "qf::mc2",
       1,
      "A 1 1 2/n",
      //hall = "-A 2ab\0"
    },
    {
       5,
      //setting = "qf::c2",
       1,
      "B 1 1 2",
      //hall = " B 2\0"
    },
    {
       5,
      //setting = "qf::a1",
       1,
      "B 2 1 1",
      //hall = " B 2x\0"
    },
    {
       8,
      //setting = "qf::c2",
       1,
      "B 1 1 m",
      //hall = " B -2\0"
    },
    {
       8,
      //setting = "qf::a1",
       1,
      "B m 1 1",
      //hall = " B -2x\0"
    },
    {
       9,
      //setting = "qf::c2",
       1,
      "B 1 1 n",
      //hall = " B -2ab\0"
    },
    {
       9,
      //setting = "qf::mc1",
       1,
      "B 1 1 b",
      //hall = " B -2b\0"
    },
    {
       9,
      //setting = "qf::a1",
       1,
      "B b 1 1",
      //hall = " B -2xb\0"
    },
    {
       9,
      //setting = "qf::ma2",
       1,
      "B n 1 1",
      //hall = " B -2xab\0"
    },
    {
      12,
      //setting = "qf::c2",
       1,
      "B 1 1 2/m",
      //hall = "-B 2\0"
    },
    {
      12,
      //setting = "qf::a1",
       1,
      "B 2/m 1 1",
      //hall = "-B 2x\0"
    },
    {
      15,
      //setting = "qf::c2",
       1,
      "B 1 1 2/n",
      //hall = "-B 2ab\0"
    },
    {
      15,
      //setting = "qf::mc1",
       1,
      "B 1 1 2/b",
      //hall = "-B 2b\0"
    },
    {
      15,
      //setting = "qf::a1",
       1,
      "B 2/b 1 1",
      //hall = "-B 2xb\0"
    },
    {
      15,
      //setting = "qf::ma2",
       1,
      "B 2/n 1 1",
      //hall = "-B 2xab\0"
    },
    {
       5,
      //setting = "qf::b1",
       1,
      "C 1 2 1",
      //hall = " C 2y\0"
    },
    {
       5,
      //setting = "qf::a2",
       1,
      "C 2 1 1",
      //hall = " C 2x\0"
    },
    {
       8,
      //setting = "qf::b1",
       1,
      "C 1 m 1",
      //hall = " C -2y\0"
    },
    {
       8,
      //setting = "qf::a2",
       1,
      "C m 1 1",
      //hall = " C -2x\0"
    },
    {
       9,
      //setting = "qf::b1",
       1,
      "C 1 c 1",
      //hall = " C -2yc\0"
    },
    {
       9,
      //setting = "qf::mb2",
       1,
      "C 1 n 1",
      //hall = " C -2yac\0"
    },
    {
       9,
      //setting = "qf::a2",
       1,
      "C n 1 1",
      //hall = " C -2xac\0"
    },
    {
       9,
      //setting = "qf::ma1",
       1,
      "C c 1 1",
      //hall = " C -2xc\0"
    },
    {
      12,
      //setting = "qf::b1",
       1,
      "C 1 2/m 1",
      //hall = "-C 2y\0"
    },
    {
      12,
      //setting = "qf::a2",
       1,
      "C 2/m 1 1",
      //hall = "-C 2x\0"
    },
    {
      15,
      //setting = "qf::b1",
       1,
      "C 1 2/c 1",
      //hall = "-C 2yc\0"
    },
    {
      15,
      //setting = "qf::mb2",
       1,
      "C 1 2/n 1",
      //hall = "-C 2yac\0"
    },
    {
      15,
      //setting = "qf::a2",
       1,
      "C 2/n 1 1",
      //hall = "-C 2xac\0"
    },
    {
      15,
      //setting = "qf::ma1",
       1,
      "C 2/c 1 1",
      //hall = "-C 2xc\0"
    },
    {
       5,
      //setting = "qf::b3",
       1,
      "I 1 2 1",
      //hall = " I 2y\0"
    },
    {
       5,
      //setting = "qf::c3",
       1,
      "I 1 1 2",
      //hall = " I 2\0"
    },
    {
       5,
      //setting = "qf::a3",
       1,
      "I 2 1 1",
      //hall = " I 2x\0"
    },
    {
       8,
      //setting = "qf::b3",
       1,
      "I 1 m 1",
      //hall = " I -2y\0"
    },
    {
       8,
      //setting = "qf::c3",
       1,
      "I 1 1 m",
      //hall = " I -2\0"
    },
    {
       8,
      //setting = "qf::a3",
       1,
      "I m 1 1",
      //hall = " I -2x\0"
    },
    {
       9,
      //setting = "qf::b3",
       1,
      "I 1 a 1",
      //hall = " I -2ya\0"
    },
    {
       9,
      //setting = "qf::mb3",
       1,
      "I 1 c 1",
      //hall = " I -2yc\0"
    },
    {
       9,
      //setting = "qf::c3",
       1,
      "I 1 1 b",
      //hall = " I -2b\0"
    },
    {
       9,
      //setting = "qf::mc3",
       1,
      "I 1 1 a",
      //hall = " I -2a\0"
    },
    {
       9,
      //setting = "qf::a3",
       1,
      "I c 1 1",
      //hall = " I -2xc\0"
    },
    {
       9,
      //setting = "qf::ma3",
       1,
      "I b 1 1",
      //hall = " I -2xb\0"
    },
    {
      12,
      //setting = "qf::b3",
       1,
      "I 1 2/m 1",
      //hall = "-I 2y\0"
    },
    {
      12,
      //setting = "qf::c3",
       1,
      "I 1 1 2/m",
      //hall = "-I 2\0"
    },
    {
      12,
      //setting = "qf::a3",
       1,
      "I 2/m 1 1",
      //hall = "-I 2x\0"
    },
    {
      15,
      //setting = "qf::b3",
       1,
      "I 1 2/a 1",
      //hall = "-I 2ya\0"
    },
    {
      15,
      //setting = "qf::mb3",
       1,
      "I 1 2/c 1",
      //hall = "-I 2yc\0"
    },
    {
      15,
      //setting = "qf::c3",
       1,
      "I 1 1 2/b",
      //hall = "-I 2b\0"
    },
    {
      15,
      //setting = "qf::mc3",
       1,
      "I 1 1 2/a",
      //hall = "-I 2a\0"
    },
    {
      15,
      //setting = "qf::a3",
       1,
      "I 2/c 1 1",
      //hall = "-I 2xc\0"
    },
    {
      15,
      //setting = "qf::ma3",
       1,
      "I 2/b 1 1",
      //hall = "-I 2xb\0"
    },
    {
      16,
      //setting= "0",
       1,
      "P 2 2 2",
      //hall = " P 2 2\0"
    },
    {
      17,
      //setting= "0",
       1,
      "P 2 2 21",
      //hall = " P 2c 2\0"
    },
    {
      17,
      //setting = "qf::cab",
       1,
      "P 21 2 2",
      //hall = " P 2a 2a\0"
    },
    {
      17,
      //setting = "qf::bca",
       1,
      "P 2 21 2",
      //hall = " P 2 2b\0"
    },
    {
      18,
      //setting= "0",
       1,
      "P 21 21 2",
      //hall = " P 2 2ab\0"
    },
    {
      18,
      //setting = "qf::cab",
       1,
      "P 2 21 21",
      //hall = " P 2bc 2\0"
    },
    {
      18,
      //setting = "qf::bca",
       1,
      "P 21 2 21",
      //hall = " P 2ac 2ac\0"
    },
    {
      19,
      //setting= "0",
       1,
      "P 21 21 21",
      //hall = " P 2ac 2ab\0"
    },
    {
      25,
      //setting= "0",
       1,
      "P m m 2",
      //hall = " P 2 -2\0"
    },
    {
      25,
      //setting = "qf::cab",
       1,
      "P 2 m m",
      //hall = " P -2 2\0"
    },
    {
      25,
      //setting = "qf::bca",
       1,
      "P m 2 m",
      //hall = " P -2 -2\0"
    },
    {
      26,
      //setting= "0",
       1,
      "P m c 21",
      //hall = " P 2c -2\0"
    },
    {
      26,
      //setting = "qf::bamc",
       1,
      "P c m 21",
      //hall = " P 2c -2c\0"
    },
    {
      26,
      //setting = "qf::cab",
       1,
      "P 21 m a",
      //hall = " P -2a 2a\0"
    },
    {
      26,
      //setting = "qf::mcba",
       1,
      "P 21 a m",
      //hall = " P -2 2a\0"
    },
    {
      26,
      //setting = "qf::bca",
       1,
      "P b 21 m",
      //hall = " P -2 -2b\0"
    },
    {
      26,
      //setting = "qf::amcb",
       1,
      "P m 21 b",
      //hall = " P -2b -2\0"
    },
    {
      27,
      //setting= "0",
       1,
      "P c c 2",
      //hall = " P 2 -2c\0"
    },
    {
      27,
      //setting = "qf::cab",
       1,
      "P 2 a a",
      //hall = " P -2a 2\0"
    },
    {
      27,
      //setting = "qf::bca",
       1,
      "P b 2 b",
      //hall = " P -2b -2b\0"
    },
    {
      28,
      //setting= "0",
       1,
      "P m a 2",
      //hall = " P 2 -2a\0"
    },
    {
      28,
      //setting = "qf::bamc",
       1,
      "P b m 2",
      //hall = " P 2 -2b\0"
    },
    {
      28,
      //setting = "qf::cab",
       1,
      "P 2 m b",
      //hall = " P -2b 2\0"
    },
    {
      28,
      //setting = "qf::mcba",
       1,
      "P 2 c m",
      //hall = " P -2c 2\0"
    },
    {
      28,
      //setting = "qf::bca",
       1,
      "P c 2 m",
      //hall = " P -2c -2c\0"
    },
    {
      28,
      //setting = "qf::amcb",
       1,
      "P m 2 a",
      //hall = " P -2a -2a\0"
    },
    {
      29,
      //setting= "0",
       1,
      "P c a 21",
      //hall = " P 2c -2ac\0"
    },
    {
      29,
      //setting = "qf::bamc",
       1,
      "P b c 21",
      //hall = " P 2c -2b\0"
    },
    {
      29,
      //setting = "qf::cab",
       1,
      "P 21 a b",
      //hall = " P -2b 2a\0"
    },
    {
      29,
      //setting = "qf::mcba",
       1,
      "P 21 c a",
      //hall = " P -2ac 2a\0"
    },
    {
      29,
      //setting = "qf::bca",
       1,
      "P c 21 b",
      //hall = " P -2bc -2c\0"
    },
    {
      29,
      //setting = "qf::amcb",
       1,
      "P b 21 a",
      //hall = " P -2a -2ab\0"
    },
    {
      30,
      //setting= "0",
       1,
      "P n c 2",
      //hall = " P 2 -2bc\0"
    },
    {
      30,
      //setting = "qf::bamc",
       1,
      "P c n 2",
      //hall = " P 2 -2ac\0"
    },
    {
      30,
      //setting = "qf::cab",
       1,
      "P 2 n a",
      //hall = " P -2ac 2\0"
    },
    {
      30,
      //setting = "qf::mcba",
       1,
      "P 2 a n",
      //hall = " P -2ab 2\0"
    },
    {
      30,
      //setting = "qf::bca",
       1,
      "P b 2 n",
      //hall = " P -2ab -2ab\0"
    },
    {
      30,
      //setting = "qf::amcb",
       1,
      "P n 2 b",
      //hall = " P -2bc -2bc\0"
    },
    {
      31,
      //setting= "0",
       1,
      "P m n 21",
      //hall = " P 2ac -2\0"
    },
    {
      31,
      //setting = "qf::bamc",
       1,
      "P n m 21",
      //hall = " P 2bc -2bc\0"
    },
    {
      31,
      //setting = "qf::cab",
       1,
      "P 21 m n",
      //hall = " P -2ab 2ab\0"
    },
    {
      31,
      //setting = "qf::mcba",
       1,
      "P 21 n m",
      //hall = " P -2 2ac\0"
    },
    {
      31,
      //setting = "qf::bca",
       1,
      "P n 21 m",
      //hall = " P -2 -2bc\0"
    },
    {
      31,
      //setting = "qf::amcb",
       1,
      "P m 21 n",
      //hall = " P -2ab -2\0"
    },
    {
      32,
      //setting= "0",
       1,
      "P b a 2",
      //hall = " P 2 -2ab\0"
    },
    {
      32,
      //setting = "qf::cab",
       1,
      "P 2 c b",
      //hall = " P -2bc 2\0"
    },
    {
      32,
      //setting = "qf::bca",
       1,
      "P c 2 a",
      //hall = " P -2ac -2ac\0"
    },
    {
      33,
      //setting= "0",
       1,
      "P n a 21",
      //hall = " P 2c -2n\0"
    },
    {
      33,
      //setting = "qf::bamc",
       1,
      "P b n 21",
      //hall = " P 2c -2ab\0"
    },
    {
      33,
      //setting = "qf::cab",
       1,
      "P 21 n b",
      //hall = " P -2bc 2a\0"
    },
    {
      33,
      //setting = "qf::mcba",
       1,
      "P 21 c n",
      //hall = " P -2n 2a\0"
    },
    {
      33,
      //setting = "qf::bca",
       1,
      "P c 21 n",
      //hall = " P -2n -2ac\0"
    },
    {
      33,
      //setting = "qf::amcb",
       1,
      "P n 21 a",
      //hall = " P -2ac -2n\0"
    },
    {
      34,
      //setting= "0",
       1,
      "P n n 2",
      //hall = " P 2 -2n\0"
    },
    {
      34,
      //setting = "qf::cab",
       1,
      "P 2 n n",
      //hall = " P -2n 2\0"
    },
    {
      34,
      //setting = "qf::bca",
       1,
      "P n 2 n",
      //hall = " P -2n -2n\0"
    },
    {
      47,
      //setting= "0",
       1,
      "P m m m",
      //hall = "-P 2 2\0"
    },
    {
      48,
      //setting = "0:2",
       2,
      "P n n n",
      //hall = " P 2 2 -1n\0-P 2ab 2bc\0"
    },
    {
      49,
      //setting= "0",
       1,
      "P c c m",
      //hall = "-P 2 2c\0"
    },
    {
      49,
      //setting = "qf::cab",
       1,
      "P m a a",
      //hall = "-P 2a 2\0"
    },
    {
      49,
      //setting = "qf::bca",
       1,
      "P b m b",
      //hall = "-P 2b 2b\0"
    },
    {
      50,
      //setting= "0",
       1,
      "P b a n",
      //hall = " P 2 2 -1ab\0-P 2ab 2b\0"
    },
    {
      50,
      //setting = "qf::cab",
       1,
      "P n c b",
      //hall = " P 2 2 -1bc\0-P 2b 2bc\0"
    },
    {
      50,
      //setting = "qf::bca",
       1,
      "P c n a",
      //hall = " P 2 2 -1ac\0-P 2a 2c\0"
    },
    {
      51,
      //setting= "0",
       1,
      "P m m a",
      //hall = "-P 2a 2a\0"
    },
    {
      51,
      //setting = "qf::bamc",
       1,
      "P m m b",
      //hall = "-P 2b 2\0"
    },
    {
      51,
      //setting = "qf::cab",
       1,
      "P b m m",
      //hall = "-P 2 2b\0"
    },
    {
      51,
      //setting = "qf::mcba",
       1,
      "P c m m",
      //hall = "-P 2c 2c\0"
    },
    {
      51,
      //setting = "qf::bca",
       1,
      "P m c m",
      //hall = "-P 2c 2\0"
    },
    {
      51,
      //setting = "qf::amcb",
       1,
      "P m a m",
      //hall = "-P 2 2a\0"
    },
    {
      52,
      //setting= "0",
       1,
      "P n n a",
      //hall = "-P 2a 2bc\0"
    },
    {
      52,
      //setting = "qf::bamc",
       1,
      "P n n b",
      //hall = "-P 2b 2n\0"
    },
    {
      52,
      //setting = "qf::cab",
       1,
      "P b n n",
      //hall = "-P 2n 2b\0"
    },
    {
      52,
      //setting = "qf::mcba",
       1,
      "P c n n",
      //hall = "-P 2ab 2c\0"
    },
    {
      52,
      //setting = "qf::bca",
       1,
      "P n c n",
      //hall = "-P 2ab 2n\0"
    },
    {
      52,
      //setting = "qf::amcb",
       1,
      "P n a n",
      //hall = "-P 2n 2bc\0"
    },
    {
      53,
      //setting= "0",
       1,
      "P m n a",
      //hall = "-P 2ac 2\0"
    },
    {
      53,
      //setting = "qf::bamc",
       1,
      "P n m b",
      //hall = "-P 2bc 2bc\0"
    },
    {
      53,
      //setting = "qf::cab",
       1,
      "P b m n",
      //hall = "-P 2ab 2ab\0"
    },
    {
      53,
      //setting = "qf::mcba",
       1,
      "P c n m",
      //hall = "-P 2 2ac\0"
    },
    {
      53,
      //setting = "qf::bca",
       1,
      "P n c m",
      //hall = "-P 2 2bc\0"
    },
    {
      53,
      //setting = "qf::amcb",
       1,
      "P m a n",
      //hall = "-P 2ab 2\0"
    },
    {
      54,
      //setting= "0",
       1,
      "P c c a",
      //hall = "-P 2a 2ac\0"
    },
    {
      54,
      //setting = "qf::bamc",
       1,
      "P c c b",
      //hall = "-P 2b 2c\0"
    },
    {
      54,
      //setting = "qf::cab",
       1,
      "P b a a",
      //hall = "-P 2a 2b\0"
    },
    {
      54,
      //setting = "qf::mcba",
       1,
      "P c a a",
      //hall = "-P 2ac 2c\0"
    },
    {
      54,
      //setting = "qf::bca",
       1,
      "P b c b",
      //hall = "-P 2bc 2b\0"
    },
    {
      54,
      //setting = "qf::amcb",
       1,
      "P b a b",
      //hall = "-P 2b 2ab\0"
    },
    {
      55,
      //setting= "0",
       1,
      "P b a m",
      //hall = "-P 2 2ab\0"
    },
    {
      55,
      //setting = "qf::cab",
       1,
      "P m c b",
      //hall = "-P 2bc 2\0"
    },
    {
      55,
      //setting = "qf::bca",
       1,
      "P c m a",
      //hall = "-P 2ac 2ac\0"
    },
    {
      56,
      //setting= "0",
       1,
      "P c c n",
      //hall = "-P 2ab 2ac\0"
    },
    {
      56,
      //setting = "qf::cab",
       1,
      "P n a a",
      //hall = "-P 2ac 2bc\0"
    },
    {
      56,
      //setting = "qf::bca",
       1,
      "P b n b",
      //hall = "-P 2bc 2ab\0"
    },
    {
      57,
      //setting= "0",
       1,
      "P b c m",
      //hall = "-P 2c 2b\0"
    },
    {
      57,
      //setting = "qf::bamc",
       1,
      "P c a m",
      //hall = "-P 2c 2ac\0"
    },
    {
      57,
      //setting = "qf::cab",
       1,
      "P m c a",
      //hall = "-P 2ac 2a\0"
    },
    {
      57,
      //setting = "qf::mcba",
       1,
      "P m a b",
      //hall = "-P 2b 2a\0"
    },
    {
      57,
      //setting = "qf::bca",
       1,
      "P b m a",
      //hall = "-P 2a 2ab\0"
    },
    {
      57,
      //setting = "qf::amcb",
       1,
      "P c m b",
      //hall = "-P 2bc 2c\0"
    },
    {
      58,
      //setting= "0",
       1,
      "P n n m",
      //hall = "-P 2 2n\0"
    },
    {
      58,
      //setting = "qf::cab",
       1,
      "P m n n",
      //hall = "-P 2n 2\0"
    },
    {
      58,
      //setting = "qf::bca",
       1,
      "P n m n",
      //hall = "-P 2n 2n\0"
    },
    {
      59,
      //setting= "0",
       1,
      "P m m n",
      //hall = " P 2 2ab -1ab\0-P 2ab 2a\0"
    },
    {
      59,
      //setting = "qf::cab",
       1,
      "P n m m",
      //hall = " P 2bc 2 -1bc\0-P 2c 2bc\0"
    },
    {
      59,
      //setting = "qf::bca",
       1,
      "P m n m",
      //hall = " P 2ac 2ac -1ac\0-P 2c 2a\0"
    },
    {
      60,
      //setting= "0",
       1,
      "P b c n",
      //hall = "-P 2n 2ab\0"
    },
    {
      60,
      //setting = "qf::bamc",
       1,
      "P c a n",
      //hall = "-P 2n 2c\0"
    },
    {
      60,
      //setting = "qf::cab",
       1,
      "P n c a",
      //hall = "-P 2a 2n\0"
    },
    {
      60,
      //setting = "qf::mcba",
       1,
      "P n a b",
      //hall = "-P 2bc 2n\0"
    },
    {
      60,
      //setting = "qf::bca",
       1,
      "P b n a",
      //hall = "-P 2ac 2b\0"
    },
    {
      60,
      //setting = "qf::amcb",
       1,
      "P c n b",
      //hall = "-P 2b 2ac\0"
    },
    {
      61,
      //setting= "0",
       1,
      "P b c a",
      //hall = "-P 2ac 2ab\0"
    },
    {
      61,
      //setting = "qf::bamc",
       1,
      "P c a b",
      //hall = "-P 2bc 2ac\0"
    },
    {
      62,
      //setting= "0",
       1,
      "P n m a",
      //hall = "-P 2ac 2n\0"
    },
    {
      62,
      //setting = "qf::bamc",
       1,
      "P m n b",
      //hall = "-P 2bc 2a\0"
    },
    {
      62,
      //setting = "qf::cab",
       1,
      "P b n m",
      //hall = "-P 2c 2ab\0"
    },
    {
      62,
      //setting = "qf::mcba",
       1,
      "P c m n",
      //hall = "-P 2n 2ac\0"
    },
    {
      62,
      //setting = "qf::bca",
       1,
      "P m c n",
      //hall = "-P 2n 2a\0"
    },
    {
      62,
      //setting = "qf::amcb",
       1,
      "P n a m",
      //hall = "-P 2c 2n\0"
    },
    {
      20,
      //setting = "qf::cab",
       1,
      "A 21 2 2",
      //hall = " A 2a 2a\0"
    },
    {
      21,
      //setting = "qf::cab",
       1,
      "A 2 2 2",
      //hall = " A 2 2\0"
    },
    {
      35,
      //setting = "qf::cab",
       1,
      "A 2 m m",
      //hall = " A -2 2\0"
    },
    {
      36,
      //setting = "qf::cab",
       1,
      "A 21 m a",
      //hall = " A -2a 2a\0"
    },
    {
      36,
      //setting = "qf::mcba",
       1,
      "A 21 a m",
      //hall = " A -2 2a\0"
    },
    {
      37,
      //setting = "qf::cab",
       1,
      "A 2 a a",
      //hall = " A -2a 2\0"
    },
    {
      38,
      //setting= "0",
       1,
      "A m m 2",
      //hall = " A 2 -2\0"
    },
    {
      38,
      //setting = "qf::amcb",
       1,
      "A m 2 m",
      //hall = " A -2 -2\0"
    },
    {
      39,
      //setting= "0",
       1,
      "A b m 2",
      //hall = " A 2 -2b\0"
    },
    {
      39,
      //setting = "qf::amcb",
       1,
      "A c 2 m",
      //hall = " A -2b -2b\0"
    },
    {
      40,
      //setting= "0",
       1,
      "A m a 2",
      //hall = " A 2 -2a\0"
    },
    {
      40,
      //setting = "qf::amcb",
       1,
      "A m 2 a",
      //hall = " A -2a -2a\0"
    },
    {
      41,
      //setting= "0",
       1,
      "A b a 2",
      //hall = " A 2 -2ab\0"
    },
    {
      41,
      //setting = "qf::amcb",
       1,
      "A c 2 a",
      //hall = " A -2ab -2ab\0"
    },
    {
      63,
      //setting = "qf::cab",
       1,
      "A m m a",
      //hall = "-A 2a 2a\0"
    },
    {
      63,
      //setting = "qf::mcba",
       1,
      "A m a m",
      //hall = "-A 2 2a\0"
    },
    {
      64,
      //setting = "qf::cab",
       1,
      "A b m a",
      //hall = "-A 2ab 2ab\0"
    },
    {
      64,
      //setting = "qf::mcba",
       1,
      "A c a m",
      //hall = "-A 2 2ab\0"
    },
    {
      65,
      //setting = "qf::cab",
       1,
      "A m m m",
      //hall = "-A 2 2\0"
    },
    {
      66,
      //setting = "qf::cab",
       1,
      "A m a a",
      //hall = "-A 2a 2\0"
    },
    {
      67,
      //setting = "qf::cab",
       1,
      "A b m m",
      //hall = "-A 2b 2b\0"
    },
    {
      67,
      //setting = "qf::mcba",
       1,
      "A c m m",
      //hall = "-A 2 2b\0"
    },
    {
      68,
      //setting = "qf::cab",
       1,
      "A b a a",
      //hall = " A 2 2 -1ab\0-A 2a 2b\0"
    },
    {
      68,
      //setting = "qf::mcba",
       1,
      "A c a a",
      //hall = " A 2 2 -1ab\0-A 2ab 2b\0"
    },
    {
      20,
      //setting = "qf::bca",
       1,
      "B 2 21 2",
      //hall = " B 2 2b\0"
    },
    {
      21,
      //setting = "qf::bca",
       1,
      "B 2 2 2",
      //hall = " B 2 2\0"
    },
    {
      35,
      //setting = "qf::bca",
       1,
      "B m 2 m",
      //hall = " B -2 -2\0"
    },
    {
      36,
      //setting = "qf::bca",
       1,
      "B b 21 m",
      //hall = " B -2 -2b\0"
    },
    {
      36,
      //setting = "qf::amcb",
       1,
      "B m 21 b",
      //hall = " B -2b -2\0"
    },
    {
      37,
      //setting = "qf::bca",
       1,
      "B b 2 b",
      //hall = " B -2b -2b\0"
    },
    {
      38,
      //setting = "qf::bamc",
       1,
      "B m m 2",
      //hall = " B 2 -2\0"
    },
    {
      38,
      //setting = "qf::cab",
       1,
      "B 2 m m",
      //hall = " B -2 2\0"
    },
    {
      39,
      //setting = "qf::bamc",
       1,
      "B m a 2",
      //hall = " B 2 -2a\0"
    },
    {
      39,
      //setting = "qf::cab",
       1,
      "B 2 c m",
      //hall = " B -2a 2\0"
    },
    {
      40,
      //setting = "qf::bamc",
       1,
      "B b m 2",
      //hall = " B 2 -2b\0"
    },
    {
      40,
      //setting = "qf::cab",
       1,
      "B 2 m b",
      //hall = " B -2b 2\0"
    },
    {
      41,
      //setting = "qf::bamc",
       1,
      "B b a 2",
      //hall = " B 2 -2ab\0"
    },
    {
      41,
      //setting = "qf::cab",
       1,
      "B 2 c b",
      //hall = " B -2ab 2\0"
    },
    {
      63,
      //setting = "qf::bca",
       1,
      "B b m m",
      //hall = "-B 2 2b\0"
    },
    {
      63,
      //setting = "qf::amcb",
       1,
      "B m m b",
      //hall = "-B 2b 2\0"
    },
    {
      64,
      //setting = "qf::bca",
       1,
      "B b c m",
      //hall = "-B 2 2ab\0"
    },
    {
      64,
      //setting = "qf::amcb",
       1,
      "B m a b",
      //hall = "-B 2ab 2\0"
    },
    {
      65,
      //setting = "qf::bca",
       1,
      "B m m m",
      //hall = "-B 2 2\0"
    },
    {
      66,
      //setting = "qf::bca",
       1,
      "B b m b",
      //hall = "-B 2b 2b\0"
    },
    {
      67,
      //setting = "qf::bca",
       1,
      "B m c m",
      //hall = "-B 2 2a\0"
    },
    {
      67,
      //setting = "qf::amcb",
       1,
      "B m a m",
      //hall = "-B 2a 2\0"
    },
    {
      68,
      //setting = "qf::bca",
       1,
      "B b c b",
      //hall = " B 2 2 -1ab\0-B 2ab 2b\0"
    },
    {
      68,
      //setting = "qf::amcb",
       1,
      "B b a b",
      //hall = " B 2 2 -1ab\0-B 2b 2ab\0"
    },
    {
      20,
      //setting= "0",
       1,
      "C 2 2 21",
      //hall = " C 2c 2\0"
    },
    {
      21,
      //setting= "0",
       1,
      "C 2 2 2",
      //hall = " C 2 2\0"
    },
    {
      35,
      //setting= "0",
       1,
      "C m m 2",
      //hall = " C 2 -2\0"
    },
    {
      36,
      //setting= "0",
       1,
      "C m c 21",
      //hall = " C 2c -2\0"
    },
    {
      36,
      //setting = "qf::bamc",
       1,
      "C c m 21",
      //hall = " C 2c -2c\0"
    },
    {
      37,
      //setting= "0",
       1,
      "C c c 2",
      //hall = " C 2 -2c\0"
    },
    {
      38,
      //setting = "qf::mcba",
       1,
      "C 2 m m",
      //hall = " C -2 2\0"
    },
    {
      38,
      //setting = "qf::bca",
       1,
      "C m 2 m",
      //hall = " C -2 -2\0"
    },
    {
      39,
      //setting = "qf::mcba",
       1,
      "C 2 m b",
      //hall = " C -2a 2\0"
    },
    {
      39,
      //setting = "qf::bca",
       1,
      "C m 2 a",
      //hall = " C -2a -2a\0"
    },
    {
      40,
      //setting = "qf::mcba",
       1,
      "C 2 c m",
      //hall = " C -2c 2\0"
    },
    {
      40,
      //setting = "qf::bca",
       1,
      "C c 2 m",
      //hall = " C -2c -2c\0"
    },
    {
      41,
      //setting = "qf::mcba",
       1,
      "C 2 c b",
      //hall = " C -2ac 2\0"
    },
    {
      41,
      //setting = "qf::bca",
       1,
      "C c 2 a",
      //hall = " C -2ac -2ac\0"
    },
    {
      63,
      //setting= "0",
       1,
      "C m c m",
      //hall = "-C 2c 2\0"
    },
    {
      63,
      //setting = "qf::bamc",
       1,
      "C c m m",
      //hall = "-C 2c 2c\0"
    },
    {
      64,
      //setting= "0",
       1,
      "C m c a",
      //hall = "-C 2ac 2\0"
    },
    {
      64,
      //setting = "qf::bamc",
       1,
      "C c m b",
      //hall = "-C 2ac 2ac\0"
    },
    {
      65,
      //setting= "0",
       1,
      "C m m m",
      //hall = "-C 2 2\0"
    },
    {
      66,
      //setting= "0",
       1,
      "C c c m",
      //hall = "-C 2 2c\0"
    },
    {
      67,
      //setting= "0",
       1,
      "C m m a",
      //hall = "-C 2a 2\0"
    },
    {
      67,
      //setting = "qf::bamc",
       1,
      "C m m b",
      //hall = "-C 2a 2a\0"
    },
    {
      68,
      //setting= "0",
       1,
      "C c c a",
      //hall = " C 2 2 -1ac\0-C 2a 2ac\0"
    },
    {
      68,
      //setting = "qf::bamc",
       1,
      "C c c b",
      //hall = " C 2 2 -1ac\0-C 2a 2c\0"
    },
    {
      22,
      //setting= "0",
       1,
      "F 2 2 2",
      //hall = " F 2 2\0"
    },
    {
      42,
      //setting= "0",
       1,
      "F m m 2",
      //hall = " F 2 -2\0"
    },
    {
      42,
      //setting = "qf::cab",
       1,
      "F 2 m m",
      //hall = " F -2 2\0"
    },
    {
      42,
      //setting = "qf::bca",
       1,
      "F m 2 m",
      //hall = " F -2 -2\0"
    },
    {
      43,
      //setting= "0",
       1,
      "F d d 2",
      //hall = " F 2 -2d\0"
    },
    {
      43,
      //setting = "qf::cab",
       1,
      "F 2 d d",
      //hall = " F -2d 2\0"
    },
    {
      43,
      //setting = "qf::bca",
       1,
      "F d 2 d",
      //hall = " F -2d -2d\0"
    },
    {
      69,
      //setting= "0",
       1,
      "F m m m",
      //hall = "-F 2 2\0"
    },
    {
      70,
      //setting = "0:2",
       2,
      "F d d d",
      //hall = " F 2 2 -1d\0-F 2uv 2vw\0"
    },
    {
      23,
      //setting= "0",
       1,
      "I 2 2 2",
      //hall = " I 2 2\0"
    },
    {
      24,
      //setting= "0",
       1,
      "I 21 21 21",
      //hall = " I 2b 2c\0"
    },
    {
      44,
      //setting= "0",
       1,
      "I m m 2",
      //hall = " I 2 -2\0"
    },
    {
      44,
      //setting = "qf::cab",
       1,
      "I 2 m m",
      //hall = " I -2 2\0"
    },
    {
      44,
      //setting = "qf::bca",
       1,
      "I m 2 m",
      //hall = " I -2 -2\0"
    },
    {
      45,
      //setting= "0",
       1,
      "I b a 2",
      //hall = " I 2 -2c\0"
    },
    {
      45,
      //setting = "qf::cab",
       1,
      "I 2 c b",
      //hall = " I -2a 2\0"
    },
    {
      45,
      //setting = "qf::bca",
       1,
      "I c 2 a",
      //hall = " I -2b -2b\0"
    },
    {
      46,
      //setting= "0",
       1,
      "I m a 2",
      //hall = " I 2 -2a\0"
    },
    {
      46,
      //setting = "qf::bamc",
       1,
      "I b m 2",
      //hall = " I 2 -2b\0"
    },
    {
      46,
      //setting = "qf::cab",
       1,
      "I 2 m b",
      //hall = " I -2b 2\0"
    },
    {
      46,
      //setting = "qf::mcba",
       1,
      "I 2 c m",
      //hall = " I -2c 2\0"
    },
    {
      46,
      //setting = "qf::bca",
       1,
      "I c 2 m",
      //hall = " I -2c -2c\0"
    },
    {
      46,
      //setting = "qf::amcb",
       1,
      "I m 2 a",
      //hall = " I -2a -2a\0"
    },
    {
      71,
      //setting= "0",
       1,
      "I m m m",
      //hall = "-I 2 2\0"
    },
    {
      72,
      //setting= "0",
       1,
      "I b a m",
      //hall = "-I 2 2c\0"
    },
    {
      72,
      //setting = "qf::cab",
       1,
      "I m c b",
      //hall = "-I 2a 2\0"
    },
    {
      72,
      //setting = "qf::bca",
       1,
      "I c m a",
      //hall = "-I 2b 2b\0"
    },
    {
      73,
      //setting= "0",
       1,
      "I b c a",
      //hall = "-I 2b 2c\0"
    },
    {
      73,
      //setting = "qf::bamc",
       1,
      "I c a b",
      //hall = "-I 2a 2b\0"
    },
    {
      74,
      //setting= "0",
       1,
      "I m m a",
      //hall = "-I 2b 2\0"
    },
    {
      74,
      //setting = "qf::bamc",
       1,
      "I m m b",
      //hall = "-I 2a 2a\0"
    },
    {
      74,
      //setting = "qf::cab",
       1,
      "I b m m",
      //hall = "-I 2c 2c\0"
    },
    {
      74,
      //setting = "qf::mcba",
       1,
      "I c m m",
      //hall = "-I 2 2b\0"
    },
    {
      74,
      //setting = "qf::bca",
       1,
      "I m c m",
      //hall = "-I 2 2a\0"
    },
    {
      74,
      //setting = "qf::amcb",
       1,
      "I m a m",
      //hall = "-I 2c 2\0"
    },
    {
      75,
      //setting= "0",
       1,
      "P 4",
      //hall = " P 4\0"
    },
    {
      76,
      //setting= "0",
       1,
      "P 41",
      //hall = " P 4w\0"
    },
    {
      77,
      //setting= "0",
       1,
      "P 42",
      //hall = " P 4c\0"
    },
    {
      78,
      //setting= "0",
       1,
      "P 43",
      //hall = " P 4cw\0"
    },
    {
      81,
      //setting= "0",
       1,
      "P -4",
      //hall = " P -4\0"
    },
    {
      83,
      //setting= "0",
       1,
      "P 4/m",
      //hall = "-P 4\0"
    },
    {
      84,
      //setting= "0",
       1,
      "P 42/m",
      //hall = "-P 4c\0"
    },
    {
      85,
      //setting = "0:2",
       2,
      "P 4/n",
      //hall = " P 4ab -1ab\0-P 4a\0"
    },
    {
      86,
      //setting = "0:2",
       2,
      "P 42/n",
      //hall = " P 4n -1n\0-P 4bc\0"
    },
    {
      89,
      //setting= "0",
       1,
      "P 4 2 2",
      //hall = " P 4 2\0"
    },
    {
      90,
      //setting= "0",
       1,
      "P 4 21 2",
      //hall = " P 4ab 2ab\0"
    },
    {
      91,
      //setting= "0",
       1,
      "P 41 2 2",
      //hall = " P 4w 2c\0"
    },
    {
      92,
      //setting= "0",
       1,
      "P 41 21 2",
      //hall = " P 4abw 2nw\0"
    },
    {
      93,
      //setting= "0",
       1,
      "P 42 2 2",
      //hall = " P 4c 2\0"
    },
    {
      94,
      //setting= "0",
       1,
      "P 42 21 2",
      //hall = " P 4n 2n\0"
    },
    {
      95,
      //setting= "0",
       1,
      "P 43 2 2",
      //hall = " P 4cw 2c\0"
    },
    {
      96,
      //setting= "0",
       1,
      "P 43 21 2",
      //hall = " P 4nw 2abw\0"
    },
    {
      99,
      //setting= "0",
       1,
      "P 4 m m",
      //hall = " P 4 -2\0"
    },
    {
      100,
      //setting= "0",
       1,
      "P 4 b m",
      //hall = " P 4 -2ab\0"
    },
    {
      101,
      //setting= "0",
       1,
      "P 42 c m",
      //hall = " P 4c -2c\0"
    },
    {
      102,
      //setting= "0",
       1,
      "P 42 n m",
      //hall = " P 4n -2n\0"
    },
    {
      103,
      //setting= "0",
       1,
      "P 4 c c",
      //hall = " P 4 -2c\0"
    },
    {
      104,
      //setting= "0",
       1,
      "P 4 n c",
      //hall = " P 4 -2n\0"
    },
    {
      105,
      //setting= "0",
       1,
      "P 42 m c",
      //hall = " P 4c -2\0"
    },
    {
      106,
      //setting= "0",
       1,
      "P 42 b c",
      //hall = " P 4c -2ab\0"
    },
    {
      111,
      //setting= "0",
       1,
      "P -4 2 m",
      //hall = " P -4 2\0"
    },
    {
      112,
      //setting= "0",
       1,
      "P -4 2 c",
      //hall = " P -4 2c\0"
    },
    {
      113,
      //setting= "0",
       1,
      "P -4 21 m",
      //hall = " P -4 2ab\0"
    },
    {
      114,
      //setting= "0",
       1,
      "P -4 21 c",
      //hall = " P -4 2n\0"
    },
    {
      115,
      //setting= "0",
       1,
      "P -4 m 2",
      //hall = " P -4 -2\0"
    },
    {
      116,
      //setting= "0",
       1,
      "P -4 c 2",
      //hall = " P -4 -2c\0"
    },
    {
      117,
      //setting= "0",
       1,
      "P -4 b 2",
      //hall = " P -4 -2ab\0"
    },
    {
      118,
      //setting= "0",
       1,
      "P -4 n 2",
      //hall = " P -4 -2n\0"
    },
    {
      123,
      //setting= "0",
       1,
      "P 4/m m m",
      //hall = "-P 4 2\0"
    },
    {
      124,
      //setting= "0",
       1,
      "P 4/m c c",
      //hall = "-P 4 2c\0"
    },
    {
      125,
      //setting = "0:2",
       2,
      "P 4/n b m",
      //hall = " P 4 2 -1ab\0-P 4a 2b\0"
    },
    {
      126,
      //setting = "0:2",
       2,
      "P 4/n n c",
      //hall = " P 4 2 -1n\0-P 4a 2bc\0"
    },
    {
      127,
      //setting= "0",
       1,
      "P 4/m b m",
      //hall = "-P 4 2ab\0"
    },
    {
      128,
      //setting= "0",
       1,
      "P 4/m n c",
      //hall = "-P 4 2n\0"
    },
    {
      129,
      //setting = "0:2",
       2,
      "P 4/n m m",
      //hall = " P 4ab 2ab -1ab\0-P 4a 2a\0"
    },
    {
      130,
      //setting = "0:2",
       2,
      "P 4/n c c",
      //hall = " P 4ab 2n -1ab\0-P 4a 2ac\0"
    },
    {
      131,
      //setting= "0",
       1,
      "P 42/m m c",
      //hall = "-P 4c 2\0"
    },
    {
      132,
      //setting= "0",
       1,
      "P 42/m c m",
      //hall = "-P 4c 2c\0"
    },
    {
      133,
      //setting = "0:2",
       2,
      "P 42/n b c",
      //hall = " P 4n 2c -1n\0-P 4ac 2b\0"
    },
    {
      134,
      //setting = "0:2",
       2,
      "P 42/n n m",
      //hall = " P 4n 2 -1n\0-P 4ac 2bc\0"
    },
    {
      135,
      //setting= "0",
       1,
      "P 42/m b c",
      //hall = "-P 4c 2ab\0"
    },
    {
      136,
      //setting= "0",
       1,
      "P 42/m n m",
      //hall = "-P 4n 2n\0"
    },
    {
      137,
      ////setting = "0:2",
       2,
      "P 42/n m c",
      //hall = " P 4n 2n -1n\0-P 4ac 2a\0"
    },
    {
      138,
      ////setting = "0:2",
       2,
      "P 42/n c m",
      //hall = " P 4n 2ab -1n\0-P 4ac 2ac\0"
    },
    {
      79,
      //setting= "0",
       1,
      "I 4",
      //hall = " I 4\0"
    },
    {
      80,
      //setting= "0",
       1,
      "I 41",
      //hall = " I 4bw\0"
    },
    {
      82,
      //setting= "0",
       1,
      "I -4",
      //hall = " I -4\0"
    },
    {
      87,
      //setting= "0",
       1,
      "I 4/m",
      //hall = "-I 4\0"
    },
    {
      88,
      //setting = "0:2",
       2,
      "I 41/a",
      //hall = " I 4bw -1bw\0-I 4ad\0"
    },
    {
      97,
      //setting= "0",
       1,
      "I 4 2 2",
      //hall = " I 4 2\0"
    },
    {
      98,
      //setting= "0",
       1,
      "I 41 2 2",
      //hall = " I 4bw 2bw\0"
    },
    {
      107,
      //setting= "0",
       1,
      "I 4 m m",
      //hall = " I 4 -2\0"
    },
    {
      108,
      //setting= "0",
       1,
      "I 4 c m",
      //hall = " I 4 -2c\0"
    },
    {
      109,
      //setting= "0",
       1,
      "I 41 m d",
      //hall = " I 4bw -2\0"
    },
    {
      110,
      //setting= "0",
       1,
      "I 41 c d",
      //hall = " I 4bw -2c\0"
    },
    {
      119,
      //setting= "0",
       1,
      "I -4 m 2",
      //hall = " I -4 -2\0"
    },
    {
      120,
      //setting= "0",
       1,
      "I -4 c 2",
      //hall = " I -4 -2c\0"
    },
    {
      121,
      //setting= "0",
       1,
      "I -4 2 m",
      //hall = " I -4 2\0"
    },
    {
      122,
      //setting= "0",
       1,
      "I -4 2 d",
      //hall = " I -4 2bw\0"
    },
    {
      139,
      //setting= "0",
       1,
      "I 4/m m m",
      //hall = "-I 4 2\0"
    },
    {
      140,
      //setting= "0",
       1,
      "I 4/m c m",
      //hall = "-I 4 2c\0"
    },
    {
      141,
      //setting = "0:2",
       2,
      "I 41/a m d",
      //hall = " I 4bw 2bw -1bw\0-I 4bd 2\0"
    },
    {
      142,
      //setting = "0:2",
       2,
      "I 41/a c d",
      //hall = " I 4bw 2aw -1bw\0-I 4bd 2c\0"
    },
    {
      143,
      //setting= "0",
       1,
      "P 3",
      //hall = " P 3\0"
    },
    {
      144,
      //setting= "0",
       1,
      "P 31",
      //hall = " P 31\0"
    },
    {
      145,
      //setting= "0",
       1,
      "P 32",
      //hall = " P 32\0"
    },
    {
      146,
      //setting= "0",
       1,
      "R 3",
      //hall = " R 3\0 P 3*\0"
    },
    {
      147,
      //setting= "0",
       1,
      "P -3",
      //hall = "-P 3\0"
    },
    {
      148,
      //setting= "0",
       1,
      "R -3",
      //hall = "-R 3\0-P 3*\0"
    },
    {
      149,
      //setting= "0",
       1,
      "P 3 1 2",
      //hall = " P 3 2\0"
    },
    {
      150,
      //setting= "0",
       1,
      "P 3 2 1",
      //hall = " P 3 2\"\0"
    },
    {
      151,
      //setting= "0",
       1,
      "P 31 1 2",
      //hall = " P 31 2 (0 0 4)\0"
    },
    {
      152,
      //setting= "0",
       1,
      "P 31 2 1",
      //hall = " P 31 2\"\0"
    },
    {
      153,
      //setting= "0",
       1,
      "P 32 1 2",
      //hall = " P 32 2 (0 0 2)\0"
    },
    {
      154,
      //setting= "0",
       1,
      "P 32 2 1",
      //hall = " P 32 2\"\0"
    },
    {
      155,
      //setting= "0",
       1,
      "R 3 2",
      //hall = " R 3 2\"\0 P 3* 2\0"
    },
    {
      156,
      //setting= "0",
       1,
      "P 3 m 1",
      //hall = " P 3 -2\"\0"
    },
    {
      157,
      //setting= "0",
       1,
      "P 3 1 m",
      //hall = " P 3 -2\0"
    },
    {
      158,
      //setting= "0",
       1,
      "P 3 c 1",
      //hall = " P 3 -2\"c\0"
    },
    {
      159,
      //setting= "0",
       1,
      "P 3 1 c",
      //hall = " P 3 -2c\0"
    },
    {
      160,
      //setting= "0",
       1,
      "R 3 m",
      //hall = " R 3 -2\"\0 P 3* -2\0"
    },
    {
      161,
      //setting= "0",
       1,
      "R 3 c",
      //hall = " R 3 -2\"c\0 P 3* -2n\0"
    },
    {
      162,
      //setting= "0",
       1,
      "P -3 1 m",
      //hall = "-P 3 2\0"
    },
    {
      163,
      //setting= "0",
       1,
      "P -3 1 c",
      //hall = "-P 3 2c\0"
    },
    {
      164,
      //setting= "0",
       1,
      "P -3 m 1",
      //hall = "-P 3 2\"\0"
    },
    {
      165,
      //setting= "0",
       1,
      "P -3 c 1",
      //hall = "-P 3 2\"c\0"
    },
    {
      166,
      //setting= "0",
       1,
      "R -3 m",
      //hall = "-R 3 2\"\0-P 3* 2\0"
    },
    {
      167,
      //setting= "0",
       1,
      "R -3 c",
      //hall = "-R 3 2\"c\0-P 3* 2n\0"
    },
    {
      146,
      //setting= "0",
       1,
      "R 3",
      //hall = " R 3\0 P 3*\0"
    },
    {
      148,
      //setting= "0",
       1,
      "R -3",
      //hall = "-R 3\0-P 3*\0"
    },
    {
      155,
      //setting= "0",
       1,
      "R 3 2",
      //hall = " R 3 2\"\0 P 3* 2\0"
    },
    {
      160,
      //setting= "0",
       1,
      "R 3 m",
      //hall = " R 3 -2\"\0 P 3* -2\0"
    },
    {
      161,
      //setting= "0",
       1,
      "R 3 c",
      //hall = " R 3 -2\"c\0 P 3* -2n\0"
    },
    {
      166,
      //setting= "0",
       1,
      "R -3 m",
      //hall = "-R 3 2\"\0-P 3* 2\0"
    },
    {
      167,
      //setting= "0",
       1,
      "R -3 c",
      //hall = "-R 3 2\"c\0-P 3* 2n\0"
    },
    {
      168,
      //setting= "0",
       1,
      "P 6",
      //hall = " P 6\0"
    },
    {
      169,
      //setting= "0",
       1,
      "P 61",
      //hall = " P 61\0"
    },
    {
      170,
      //setting= "0",
       1,
      "P 65",
      //hall = " P 65\0"
    },
    {
      171,
      //setting= "0",
       1,
      "P 62",
      //hall = " P 62\0"
    },
    {
      172,
      //setting= "0",
       1,
      "P 64",
      //hall = " P 64\0"
    },
    {
      173,
      //setting= "0",
       1,
      "P 63",
      //hall = " P 6c\0"
    },
    {
      174,
      //setting= "0",
       1,
      "P -6",
      //hall = " P -6\0"
    },
    {
      175,
      //setting= "0",
       1,
      "P 6/m",
      //hall = "-P 6\0"
    },
    {
      176,
      //setting= "0",
       1,
      "P 63/m",
      //hall = "-P 6c\0"
    },
    {
      177,
      //setting= "0",
       1,
      "P 6 2 2",
      //hall = " P 6 2\0"
    },
    {
      178,
      //setting= "0",
       1,
      "P 61 2 2",
      //hall = " P 61 2 (0 0 5)\0"
    },
    {
      179,
      //setting= "0",
       1,
      "P 65 2 2",
      //hall = " P 65 2 (0 0 1)\0"
    },
    {
      180,
      //setting= "0",
       1,
      "P 62 2 2",
      //hall = " P 62 2 (0 0 4)\0"
    },
    {
      181,
      //setting= "0",
       1,
      "P 64 2 2",
      //hall = " P 64 2 (0 0 2)\0"
    },
    {
      182,
      //setting= "0",
       1,
      "P 63 2 2",
      //hall = " P 6c 2c\0"
    },
    {
      183,
      //setting= "0",
       1,
      "P 6 m m",
      //hall = " P 6 -2\0"
    },
    {
      184,
      //setting= "0",
       1,
      "P 6 c c",
      //hall = " P 6 -2c\0"
    },
    {
      185,
      //setting= "0",
       1,
      "P 63 c m",
      //hall = " P 6c -2\0"
    },
    {
      186,
      //setting= "0",
       1,
      "P 63 m c",
      //hall = " P 6c -2c\0"
    },
    {
      187,
      //setting= "0",
       1,
      "P -6 m 2",
      //hall = " P -6 2\0"
    },
    {
      188,
      //setting= "0",
       1,
      "P -6 c 2",
      //hall = " P -6c 2\0"
    },
    {
      189,
      //setting= "0",
       1,
      "P -6 2 m",
      //hall = " P -6 -2\0"
    },
    {
      190,
      //setting= "0",
       1,
      "P -6 2 c",
      //hall = " P -6c -2c\0"
    },
    {
      191,
      //setting= "0",
       1,
      "P 6/m m m",
      //hall = "-P 6 2\0"
    },
    {
      192,
      //setting= "0",
       1,
      "P 6/m c c",
      //hall = "-P 6 2c\0"
    },
    {
      193,
      //setting= "0",
       1,
      "P 63/m c m",
      //hall = "-P 6c 2\0"
    },
    {
      194,
      //setting= "0",
       1,
      "P 63/m m c",
      //hall = "-P 6c 2c\0"
    },
    {
      195,
      //setting= "0",
       1,
      "P 2 3",
      //hall = " P 2 2 3\0"
    },
    {
      198,
      //setting= "0",
       1,
      "P 21 3",
      //hall = " P 2ac 2ab 3\0"
    },
    {
      200,
      //setting= "0",
       1,
      "P m -3",
      //hall = "-P 2 2 3\0"
    },
    {
      201,
      //setting = "0:2",
       2,
      "P n -3",
      //hall = " P 2 2 3 -1n\0-P 2ab 2bc 3\0"
    },
    {
      205,
      //setting= "0",
       1,
      "P a -3",
      //hall = "-P 2ac 2ab 3\0"
    },
    {
      207,
      //setting= "0",
       1,
      "P 4 3 2",
      //hall = " P 4 2 3\0"
    },
    {
      208,
      //setting= "0",
       1,
      "P 42 3 2",
      //hall = " P 4n 2 3\0"
    },
    {
      212,
      //setting= "0",
       1,
      "P 43 3 2",
      //hall = " P 4acd 2ab 3\0"
    },
    {
      213,
      //setting= "0",
       1,
      "P 41 3 2",
      //hall = " P 4bd 2ab 3\0"
    },
    {
      215,
      //setting= "0",
       1,
      "P -4 3 m",
      //hall = " P -4 2 3\0"
    },
    {
      218,
      //setting= "0",
       1,
      "P -4 3 n",
      //hall = " P -4n 2 3\0"
    },
    {
      221,
      //setting= "0",
       1,
      "P m -3 m",
      //hall = "-P 4 2 3\0"
    },
    {
      222,
      //setting = "0:2",
       2,
      "P n -3 n",
      //hall = " P 4 2 3 -1n\0-P 4a 2bc 3\0"
    },
    {
      223,
      //setting= "0",
       1,
      "P m -3 n",
      //hall = "-P 4n 2 3\0"
    },
    {
      224,
      //setting = "0:2",
       2,
      "P n -3 m",
      //hall = " P 4n 2 3 -1n\0-P 4bc 2bc 3\0"
    },
    {
      196,
      //setting= "0",
       1,
      "F 2 3",
      //hall = " F 2 2 3\0"
    },
    {
      202,
      //setting= "0",
       1,
      "F m -3",
      //hall = "-F 2 2 3\0"
    },
    {
      203,
      //setting = "0:2",
       2,
      "F d -3",
      //hall = " F 2 2 3 -1d\0-F 2uv 2vw 3\0"
    },
    {
      209,
      //setting= "0",
       1,
      "F 4 3 2",
      //hall = " F 4 2 3\0"
    },
    {
      210,
      //setting= "0",
       1,
      "F 41 3 2",
      //hall = " F 4d 2 3\0"
    },
    {
      216,
      //setting= "0",
       1,
      "F -4 3 m",
      //hall = " F -4 2 3\0"
    },
    {
      219,
      //setting= "0",
       1,
      "F -4 3 c",
      //hall = " F -4a 2 3\0"
    },
    {
      225,
      //setting= "0",
       1,
      "F m -3 m",
      //hall = "-F 4 2 3\0"
    },
    {
      226,
      //setting= "0",
       1,
      "F m -3 c",
      //hall = "-F 4a 2 3\0"
    },
    {
      227,
      //setting = "0:2",
       2,
      "F d -3 m",
      //hall = " F 4d 2 3 -1d\0-F 4vw 2vw 3\0"
    },
    {
      228,
      //setting = "0:2",
       2,
      "F d -3 c",
      //hall = " F 4d 2 3 -1ad\0-F 4ud 2vw 3\0"
    },
    {
      197,
      //setting= "0",
       1,
      "I 2 3",
      //hall = " I 2 2 3\0"
    },
    {
      199,
      //setting= "0",
       1,
      "I 21 3",
      //hall = " I 2b 2c 3\0"
    },
    {
      204,
      //setting= "0",
       1,
      "I m -3",
      //hall = "-I 2 2 3\0"
    },
    {
      206,
      //setting= "0",
       1,
      "I a -3",
      //hall = "-I 2b 2c 3\0"
    },
    {
      211,
      //setting= "0",
       1,
      "I 4 3 2",
      //hall = " I 4 2 3\0"
    },
    {
      214,
      //setting= "0",
       1,
      "I 41 3 2",
      //hall = " I 4bd 2c 3\0"
    },
    {
      217,
      //setting= "0",
       1,
      "I -4 3 m",
      //hall = " I -4 2 3\0"
    },
    {
      220,
      //setting= "0",
       1,
      "I -4 3 d",
      //hall = " I -4bd 2c 3\0"
    },
    {
      229,
      //setting= "0",
       1,
      "I m -3 m",
      //hall = "-I 4 2 3\0"
    },
    {
      230,
      //setting= "0",
       1,
      "I a -3 d",
      //hall = "-I 4bd 2c 3\0"
    },
    {  //terminate
      0,
       0,
      "",
    }
  };

}

DLV::int_g DLV::get_number_of_origins(const int_g spgp)
{
  for (int_g i = 0; gp_settings[i].index != 0; i++) {
    if (gp_settings[i].index == spgp)
      return gp_settings[i].norigins;
  }
  return 0;
}

DLV::int_g DLV::get_number_of_origins(const string spgp)
{
  for (int_g i = 0; gp_settings[i].index != 0; i++) {
    if (gp_settings[i].label == spgp)
      return gp_settings[i].norigins;
  }
  return 0;
}
