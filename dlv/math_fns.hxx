
#ifndef DLV_MATH_FUNCTIONS
#define DLV_MATH_FUNCTIONS

#include <cmath>

namespace DLV {

  extern real_l truncate(const real_l x);
  extern real_l nint(const real_l x);
  extern real_l round(const real_l x);

  extern void matrix_transpose(const real_l matrix[3][3],
			       real_l transpose[3][3], const int_g dim);
  extern void matrix_invert(const real_l matrix[3][3], real_l inverse[3][3],
			    const int_g dim);
  extern real_l determinant(const real_l matrix[3][3]);
  extern void mat_mult(const real_l a[3][3], const real_l b[3][3],
		       real_l c[3][3]);
  extern void mat_nn_mult(const real_l a[3][3], const real_l b[3][3],
			  real_l c[3][3], const int_g dim);
  extern void mat_nt_mult(const real_l a[3][3], const real_l b[3][3],
			  real_l c[3][3], const int_g dim);
  extern void mat_tn_mult(const real_l a[3][3], const real_l b[3][3],
			  real_l c[3][3], const int_g dim);
  extern void mat_nt_mult_sum(const real_l a[3][3], const real_l b[3][3],
			      real_l c[3][3], const int_g dim);
  extern void mat_tn_mult_sum(const real_l a[3][3], const real_l b[3][3],
			      real_l c[3][3], const int_g dim);

  extern bool linear_solve(real_l a[3][3], real_l b[3], const int_g n);
  extern void euclid(const int_g a, int_g &x, const int_g b,
		     int_g &y, const int_g gcd);

}

inline DLV::real_l DLV::truncate(const real_l x)
{
  const real_l tol = 1e-6;
  return(floor(x + tol));
}

inline DLV::real_l DLV::nint(const real_l x)
{
  // 0 if -0.5 < x <= 0.5
  //const real_l tol = 1e-6;
  if (x < real_l(0.0))
    return (ceil(x - 0.5));
  else
    return(floor(x + 0.4999999999));
}

inline DLV::real_l DLV::round(const real_l x)
{
  if (x < real_l(0.0))
    return ceil(x - 0.5);
  else
    return floor(x + 0.5);
}

inline void DLV::matrix_transpose(const real_l matrix[3][3],
				  real_l transpose[3][3], const int_g dim)
{
  for (int_g i = 0; i < dim; i++)
    for (int_g j = 0; j < dim; j++)
      transpose[i][j] = matrix[j][i];
}

inline void DLV::mat_mult(const real_l a[3][3], const real_l b[3][3],
			  real_l c[3][3])
{
  for (int_g i = 0; i < 3; i++)
    for (int_g j = 0; j < 3; j++)
      c[i][j] = a[i][0] * b[0][j] + a[i][1] * b[1][j] + a[i][2] * b[2][j];
}

inline void DLV::mat_nn_mult(const real_l a[3][3], const real_l b[3][3],
			     real_l c[3][3], const int_g dim)
{
  for (int_g i = 0; i < dim; i++) {
    for (int_g j = 0; j < dim; j++) {
      c[i][j] = 0.0;
      for (int_g k = 0; k < dim; k++)
	c[i][j] += a[i][k] * b[k][j];
    }
  }
}

// Todo - shouldn't the k loop also be limited to dim?
inline void DLV::mat_nt_mult(const real_l a[3][3], const real_l b[3][3],
			     real_l c[3][3], const int_g dim)
{
  for (int_g i = 0; i < dim; i++)
    for (int_g j = 0; j < dim; j++)
      c[i][j] = a[i][0] * b[j][0] + a[i][1] * b[j][1] + a[i][2] * b[j][2];
}

inline void DLV::mat_tn_mult(const real_l a[3][3], const real_l b[3][3],
			     real_l c[3][3], const int_g dim)
{
  for (int_g i = 0; i < dim; i++)
    for (int_g j = 0; j < dim; j++)
      c[i][j] = a[0][i] * b[0][j] + a[1][i] * b[1][j] + a[2][i] * b[2][j];
}

inline void DLV::mat_nt_mult_sum(const real_l a[3][3], const real_l b[3][3],
				 real_l c[3][3], const int_g dim)
{
  for (int_g i = 0; i < dim; i++)
    for (int_g j = 0; j < dim; j++)
      c[i][j] += a[i][0] * b[j][0] + a[i][1] * b[j][1] + a[i][2] * b[j][2];
}

inline void DLV::mat_tn_mult_sum(const real_l a[3][3], const real_l b[3][3],
				 real_l c[3][3], const int_g dim)
{
  for (int_g i = 0; i < dim; i++)
    for (int_g j = 0; j < dim; j++)
      c[i][j] += a[0][i] * b[0][j] + a[1][i] * b[1][j] + a[2][i] * b[2][j];
}

#endif // DLV_MATH_FUNCTIONS
