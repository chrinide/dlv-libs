
#include <cmath>
#include <map>
#include <cstdio>
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/toolkit_obj.hxx"
#  include "../graphics/render_base.hxx"
#  include "../graphics/display_objs.hxx"
#  include "../graphics/drawable.hxx"
#  include "../graphics/edit_objs.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "model.hxx"
#include "data_objs.hxx"
#include "operation.hxx" // find_plane
#include "data_simple.hxx"
#include "data_surf.hxx"
// for update in plane
#include "utils.hxx"

DLV::surface_data::~surface_data()
{
  for (int_g i = 0; i < n_data_sets; i++)
    delete [] data[i];
  delete [] data;
  delete [] labels;
}

bool DLV::plane::is_plane() const
{
  return true;
}

DLV::string DLV::plane::get_name() const
{
  return name;
}

bool DLV::plane::is_displayable() const
{
  return true;
}

bool DLV::plane::is_editable() const
{
  return false;
}

bool DLV::plane::is_edited() const
{
  return false;
}

DLV::string DLV::plane::get_data_label() const
{
  return (name + " - plane");
}
 
DLV::string DLV::plane::get_edit_label() const
{
  return "";
}

DLV::int_g DLV::plane::get_number_data_sets() const
{
  return 1;
}

DLV::string DLV::plane::get_sub_data_label(const int_g) const
{
  return "";
}

DLV::int_g DLV::plane::get_display_type() const
{
  return display_plane;
}

DLV::int_g DLV::plane::get_edit_type() const
{
  return no_edit;
}

bool DLV::plane::get_sub_is_vector(const int_g) const
{
  return false;
}

bool DLV::k_space_plane::is_kspace() const
{
  return true;
}

void DLV::plane::get_vertices(real_g o[3], real_g a[3], real_g b[3]) const
{
  o[0] = origin[0];
  o[1] = origin[1];
  o[2] = origin[2];
  a[0] = pointa[0];
  a[1] = pointa[1];
  a[2] = pointa[2];
  b[0] = pointb[0];
  b[1] = pointb[1];
  b[2] = pointb[2];
}

void DLV::plane::unload_data()
{
  // Do nothing
}

DLV::surface_data::surface_data(const char code[], const string src,
				operation *p, const int_g na,
				const int_g nb, const real_g o[3],
				const real_g as[3], const real_g bs[3])
  : reloadable_data(code, src, p), nx(na), ny(nb), n_data_sets(0), labels(0),
    data(0), size(0)
{
  origin[0] = o[0];
  origin[1] = o[1];
  origin[2] = o[2];
  a_step[0] = as[0];
  a_step[1] = as[1];
  a_step[2] = as[2];
  b_step[0] = bs[0];
  b_step[1] = bs[1];
  b_step[2] = bs[2];
}

void DLV::surface_data::add_data(real_g *array, const char label[],
				 const bool copy_data)
{
  const int_g block_size = 16;
  if (n_data_sets == size) {
    real_g **new_data = new real_g*[size + block_size];
    string *new_str = new string[size + block_size];
    for (int_g i = 0; i < size; i++) {
      new_data[i] = data[i];
      new_str[i] = labels[i];
    }
    if (data != 0)
      delete [] data;
    data = new_data;
    if (labels != 0)
      delete [] labels;
    labels = new_str;
    size += block_size;
  }
  labels[n_data_sets] = label;
  if (copy_data) {
    data[n_data_sets] = new real_g[nx * ny];
    for (int_g i = 0; i < nx * ny; i++)
      data[n_data_sets][i] = array[i];
  } else
    data[n_data_sets] = array;
  n_data_sets++;
}

void DLV::surface_data::unload_data()
{
  fprintf(stderr, "surface data unload not implemented - Todo\n");
}

bool DLV::surface_data::is_displayable() const
{
  return true;
}

bool DLV::surface_data::is_editable() const
{
  return true;
}

DLV::string DLV::surface_data::get_data_label() const
{
  string name = "2D" + get_tags();
  return name;
}

DLV::string DLV::surface_data::get_edit_label() const
{
  string name = "2D" + get_tags();
  return name;
}

DLV::int_g DLV::surface_data::get_number_data_sets() const
{
  return n_data_sets;
}

DLV::string DLV::surface_data::get_sub_data_label(const int_g n) const
{
  return labels[n];
}

DLV::int_g DLV::surface_data::get_display_type() const
{
  return (int)display_2D;
}

DLV::int_g DLV::surface_data::get_edit_type() const
{
  return (int)edit_2D;
}

bool DLV::surface_data::get_sub_is_vector(const int_g n) const
{
  // Todo ?
  return false;
}

bool DLV::surface_data::is_kspace() const
{
  return false;
}

bool DLV::surface_data::is_periodic() const
{
  return false;
}

bool DLV::rspace_periodic_surface::is_periodic() const
{
  return true;
}

#ifdef ENABLE_DLV_GRAPHICS

DLVreturn_type DLV::plane::render(const render_parent *parent,
				  class model *structure,
				  char message[], const int_g mlen)
{
  //return render(parent, structure, 0, 0, 0, message, mlen);
  return DLV_OK;
}

bool DLV::plane::hide_render(const int_g object, const int_g visible,
			     const render_parent *parent,
			     class model *structure,
			     char message[], const int_g mlen)
{
  // This routine is now badly named
  if (visible == 1) {
    message[0] = '\0';
    drawable_obj *obj = get_drawable();
    obj->update_plane(origin, pointa, pointb);
    reattach_objects(parent);
  }
  return true;
}

DLVreturn_type DLV::plane::render(const render_parent *parent,
				  class model *structure,
				  const int_g component, const int_g method,
				  const int_g index, char message[],
				  const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    obj = DLV::drawable_obj::create_plane_data(parent, is_kspace(),
					       message, mlen);
    if (obj == 0)
      return DLV_ERROR;
    else
      set_drawable(obj);
  }
  if (count_display_objects() == 0) {
    display_obj *ptr = 0;
    ptr = display_obj::create_plane_obj(parent, obj, name, index, is_kspace(),
					message, mlen);
    if (ptr == 0) {
      if (strlen(message) == 0)
	strncpy(message, "Unable to allocate display object", mlen);
      return DLV_ERROR;
    } else {
      add_display_obj(ptr);
      ptr->update_display_list(parent, name);
      ptr->attach_params();
    }
  }
  return DLV_OK;
}

DLVreturn_type DLV::plane::use_view_editor(const render_parent *parent,
					   const bool v, char message[],
					   const int_g mlen)
{
  display_obj *ptr = get_display_obj();
  if (ptr == 0) {
    strncpy(message, "plane is not displayed", mlen);
    return DLV_ERROR;
  } else {
    //bool ok = true;
    real_g matrix[4][4];
    real_g translate[3];
    real_g centre[3];
    if (is_kspace())
      (void) ptr->attach_editor(v, parent->get_kedit(), matrix, translate,
				centre, message, mlen);
    else
      (void) ptr->attach_editor(v, parent->get_redit(), matrix, translate,
				centre, message, mlen);
    if (!v) {
      ptr->transform_point(origin, matrix, translate, centre);
      ptr->transform_point(pointa, matrix, translate, centre);
      ptr->transform_point(pointb, matrix, translate, centre);
      drawable_obj *obj = get_drawable();
      if (obj == 0) {
	strncpy(message, "BUG: missing drawable for plane", mlen);
	return DLV_ERROR;
      } else
	obj->update_plane(origin, pointa, pointb);
    }
    return DLV_OK;
  }
}

DLVreturn_type DLV::plane::update2D(const int_g method, const int_g h,
				    const int_g k, const int_g l,
				    const real_g x, const real_g y,
				    const real_g z, const int_g obj,
				    const bool conv, const bool frac,
				    const bool parent, model *const m,
				    char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  drawable_obj *dobj = get_drawable();
  if (dobj == 0) {
    strncpy(message, "BUG: missing drawable for plane", mlen);
    ok = DLV_ERROR;
  } else {
    if (method < 4) {
      coord_type atom1[3];
      coord_type atom2[3];
      if (!m->get_selected_positions(atom1, atom2)) {
	strncpy(message, "At least two atoms must be selected", mlen);
	return DLV_WARNING;
      }
      if (method == 1) {
	pointa[0] = origin[0] + (real_g)(atom1[0] - atom2[0]);
	pointa[1] = origin[1] + (real_g)(atom1[1] - atom2[1]);
	pointa[2] = origin[2] + (real_g)(atom1[2] - atom2[2]);
      } else if (method == 3) {
	pointb[0] = origin[0] + (real_g)(atom1[0] - atom2[0]);
	pointb[1] = origin[1] + (real_g)(atom1[1] - atom2[1]);
	pointb[2] = origin[2] + (real_g)(atom1[2] - atom2[2]);
      } else {
	real_g offset[3];
	for (int_g i = 0; i < 3; i++)
	  offset[i] = (real_g)atom2[i] - origin[i];
	origin[0] = (real_g)atom2[0];
	origin[1] = (real_g)atom2[1];
	origin[2] = (real_g)atom2[2];
	if (method == 0) {
	  pointa[0] = (real_g)atom1[0];
	  pointa[1] = (real_g)atom1[1];
	  pointa[2] = (real_g)atom1[2];
	  pointb[0] += offset[0];
	  pointb[1] += offset[1];
	  pointb[2] += offset[2];
	} else {
	  pointb[0] = (real_g)atom1[0];
	  pointb[1] = (real_g)atom1[1];
	  pointb[2] = (real_g)atom1[2];
	  pointa[0] += offset[0];
	  pointa[1] += offset[1];
	  pointa[2] += offset[2];
	}
      }
    } else if (method == 4) {
      // average atom positions and set origin
      coord_type position[3];
      m->get_average_selection_position(position);
      real_g offset[3];
      for (int_g i = 0; i < 3; i++)
	offset[i] = (real_g)position[i] - origin[i];
      origin[0] = (real_g)position[0];
      origin[1] = (real_g)position[1];
      origin[2] = (real_g)position[2];
      pointa[0] += offset[0];
      pointa[1] += offset[1];
      pointa[2] += offset[2];
      pointb[0] += offset[0];
      pointb[1] += offset[1];
      pointb[2] += offset[2];
    } else if (method == 5) {
      // average atom positions and set origin so this is the centre
      coord_type position[3];
      m->get_average_selection_position(position);
      real_g average[3];
      // Need average of 4 corners
      for (int_g i = 0; i < 3; i++)
	average[i] = (real_g)((pointa[i] + pointb[i]) / 2.0);
      real_g offset[3];
      for (int_g i = 0; i < 3; i++)
	offset[i] = (real_g)position[i] - average[i];
      origin[0] += offset[0];
      origin[1] += offset[1];
      origin[2] += offset[2];
      pointa[0] += offset[0];
      pointa[1] += offset[1];
      pointa[2] += offset[2];
      pointb[0] += offset[0];
      pointb[1] += offset[1];
      pointb[2] += offset[2];
    } else if (method == 6) {
      // Use miller indices
      if (h == 0 and k == 0 and l == 0) {
	strncpy(message, "At least one miller index must be non-zero", mlen);
	return DLV_WARNING;
      }
      coord_type a[3];
      coord_type b[3];
      coord_type c[3];
      //int_g dim;
      bool has_rotation = true;
      if (parent) {
	if (is_kspace()) {
	  strncpy(message, "K space lattice mapping not implemented", mlen);
	  ok = DLV_WARNING;
	} else {
	  real_l r[3][3];
	  operation *op = operation::find_parent_geometry(r, has_rotation);
	  if (op == 0) {
	    strncpy(message, "Model doesn't have a parent lattice", mlen);
	    ok = DLV_WARNING;
	  } else if (!has_rotation) {
	    strncpy(message,
		    "Lattice mapping not implemented for geometry edit", mlen);
	    ok = DLV_WARNING;
	  } else {
	    coord_type aa[3];
	    coord_type bb[3];
	    coord_type cc[3];
	    if (is_kspace())
	      op->get_model()->get_reciprocal_lattice(aa, bb, cc);
	    else if (conv)
	      op->get_model()->get_conventional_lattice(aa, bb, cc);
	    else
	      op->get_model()->get_primitive_lattice(aa, bb, cc);
	    //dim = op->get_model()->get_number_of_periodic_dims();
	    for (int i = 0; i < 3; i++) {
	      a[i] = r[i][0] * aa[0] + r[i][1] * aa[1] + r[i][2] * aa[2];
	      b[i] = r[i][0] * bb[0] + r[i][1] * bb[1] + r[i][2] * bb[2];
	      c[i] = r[i][0] * cc[0] + r[i][1] * cc[1] + r[i][2] * cc[2];
	    }
	  }
	}
      } else {
	if (is_kspace())
	  m->get_reciprocal_lattice(a, b, c);
	else if (conv)
	  m->get_conventional_lattice(a, b, c);
	else
	  m->get_primitive_lattice(a, b, c);
	//dim = m->get_number_of_periodic_dims();
      }
      real_g t;
      if (h == 0 and k == 0) {
	t = 1.0f / (real_g) l;
	for (int_g i = 0; i < 3; i++) {
	  origin[i] = t * (real_g)c[i];
	  pointa[i] = origin[i] + (real_g)a[i];
	  pointb[i] = origin[i] + (real_g)b[i];
	}
      } else if (h == 0 and l == 0) {
	t = 1.0f / (real_g) k;
	for (int_g i = 0; i < 3; i++) {
	  origin[i] = t * (real_g)b[i];
	  pointa[i] = origin[i] + (real_g)a[i];
	  pointb[i] = origin[i] + (real_g)c[i];
	}
      } else if (k == 0 and l == 0) {
	t = 1.0f / (real_g) h;
	for (int_g i = 0; i < 3; i++) {
	  origin[i] = t * (real_g)a[i];
	  pointa[i] = origin[i] + (real_g)b[i];
	  pointb[i] = origin[i] + (real_g)c[i];
	}
      } else if (h == 0) {
	t = 1.0f / (real_g) k;
	for (int_g i = 0; i < 3; i++) {
	  origin[i] = t * (real_g)b[i];
	  pointa[i] = origin[i] + (real_g)a[i];
	}
	t = 1.0f / (real_g) l;
	for (int_g i = 0; i < 3; i++)
	  pointb[i] = t * (real_g)c[i];
      } else if (k == 0) {
	t = 1.0f / (real_g) h;
	for (int_g i = 0; i < 3; i++) {
	  origin[i] = t * (real_g)a[i];
	  pointa[i] = origin[i] + (real_g)b[i];
	}
	t = 1.0f / (real_g) l;
	for (int_g i = 0; i < 3; i++)
	  pointb[i] = t * (real_g)c[i];
      } else if (l == 0) {
	t = 1.0f / (real_g) h;
	for (int_g i = 0; i < 3; i++) {
	  origin[i] = t * (real_g)a[i];
	  pointa[i] = origin[i] + (real_g)c[i];
	}
	t = 1.0f / (real_g) k;
	for (int_g i = 0; i < 3; i++)
	  pointb[i] = t * (real_g)b[i];
      } else {
	t = 1.0f / (real_g) h;
	for (int_g i = 0; i < 3; i++)
	  origin[i] = t * (real_g)a[i];
	t = 1.0f / (real_g) k;
	for (int_g i = 0; i < 3; i++)
	  pointa[i] = t * (real_g)b[i];
	t = 1.0f / (real_g) l;
	for (int_g i = 0; i < 3; i++)
	  pointb[i] = t * (real_g)c[i];
      }
    } else if (method == 7 or method == 9) {
      if (method == 9) {
	// set OABC to lattice
	origin[0] = 0.0;
	origin[1] = 0.0;
	origin[2] = 0.0;
      }
      coord_type va[3];
      coord_type vb[3];
      coord_type vc[3];
      //int_g dim;
      bool has_rotation = true;
      if (parent) {
	if (is_kspace()) {
	  strncpy(message, "K space lattice mapping not implemented", mlen);
	  ok = DLV_WARNING;
	} else {
	  real_l r[3][3];
	  operation *op = operation::find_parent_geometry(r, has_rotation);
	  if (op == 0) {
	    strncpy(message, "Model doesn't have a parent lattice", mlen);
	    ok = DLV_WARNING;
	  } else if (!has_rotation) {
	    strncpy(message,
		    "Lattice mapping not implemented for geometry edit", mlen);
	    ok = DLV_WARNING;
	  } else {
	    coord_type aa[3];
	    coord_type bb[3];
	    coord_type cc[3];
	    if (is_kspace())
	      op->get_model()->get_reciprocal_lattice(aa, bb, cc);
	    else if (conv)
	      op->get_model()->get_conventional_lattice(aa, bb, cc);
	    else
	      op->get_model()->get_primitive_lattice(aa, bb, cc);
	    //dim = op->get_model()->get_number_of_periodic_dims();
	    for (int i = 0; i < 3; i++) {
	      va[i] = r[i][0] * aa[0] + r[i][1] * aa[1] + r[i][2] * aa[2];
	      vb[i] = r[i][0] * bb[0] + r[i][1] * bb[1] + r[i][2] * bb[2];
	      vc[i] = r[i][0] * cc[0] + r[i][1] * cc[1] + r[i][2] * cc[2];
	    }
	  }
	}
      } else {
	if (is_kspace())
	  m->get_reciprocal_lattice(va, vb, vc);
	else if (conv)
	  m->get_conventional_lattice(va, vb, vc);
	else
	  m->get_primitive_lattice(va, vb, vc);
	//dim = m->get_number_of_periodic_dims();
      }
      // align ABC aint_l lattice vectors
      switch (m->get_number_of_periodic_dims()) {
      case 0:
	if (method == 9) {
	  va[0] = 1.0;
	  va[1] = 0.0;
	  va[2] = 0.0;
	} else {
	  va[0] = pointa[0] - origin[0];
	  va[1] = pointa[1] - origin[1];
	  va[2] = pointa[2] - origin[2];
	}
      case 1:
	if (method == 9) {
	  vb[0] = 0.0;
	  vb[1] = 1.0;
	  vb[2] = 0.0;
	} else {
	  vb[0] = pointb[0] - origin[0];
	  vb[1] = pointb[1] - origin[1];
	  vb[2] = pointb[2] - origin[2];
	}
      default:
	break;
      }
      pointa[0] = origin[0] + (real_g)va[0];
      pointa[1] = origin[1] + (real_g)va[1];
      pointa[2] = origin[2] + (real_g)va[2];
      pointb[0] = origin[0] + (real_g)vb[0];
      pointb[1] = origin[1] + (real_g)vb[1];
      pointb[2] = origin[2] + (real_g)vb[2];
    } else if (method == 8 or method == 10) {
      if (method == 10) {
	// set OABC to XYZ
	origin[0] = 0.0;
	origin[1] = 0.0;
	origin[2] = 0.0;
      }
      // align ABC to XYZ
      pointa[0] = origin[0] + 1.0f;
      pointa[1] = origin[1];
      pointa[2] = origin[2];
      pointb[0] = origin[0];
      pointb[1] = origin[1] + 1.0f;
      pointb[2] = origin[2];
    } else if (method == 11) {
      // set positions from all 3 atoms
      coord_type atom1[3];
      coord_type atom2[3];
      coord_type atom3[3];
      if (!m->get_selected_positions(atom1, atom2, atom3)) {
	strncpy(message, "At least three atoms must be selected", mlen);
	return DLV_WARNING;
      }
      origin[0] = (real_g)atom2[0];
      origin[1] = (real_g)atom2[1];
      origin[2] = (real_g)atom2[2];
      pointa[0] = (real_g)atom3[0];
      pointa[1] = (real_g)atom3[1];
      pointa[2] = (real_g)atom3[2];
      pointb[0] = (real_g)atom1[0];
      pointb[1] = (real_g)atom1[1];
      pointb[2] = (real_g)atom1[2];
    } else if (method < 16) {
      coord_type position[3];
      position[0] = x;
      position[1] = y;
      position[2] = z;
      if (frac) {
	coord_type a[3];
	coord_type b[3];
	coord_type c[3];
	int_g dim;
	bool has_rotation = true;
	if (parent) {
	  if (is_kspace()) {
	    strncpy(message, "K space lattice mapping not implemented", mlen);
	    ok = DLV_WARNING;
	  } else {
	    real_l r[3][3];
	    operation *op = operation::find_parent_geometry(r, has_rotation);
	    if (op == 0) {
	      strncpy(message, "Model doesn't have a parent lattice", mlen);
	      ok = DLV_WARNING;
	    } else if (!has_rotation) {
	      strncpy(message,
		      "Lattice mapping not implemented for geometry edit",
		      mlen);
	      ok = DLV_WARNING;
	    } else {
	      coord_type aa[3];
	      coord_type bb[3];
	      coord_type cc[3];
	      if (is_kspace())
		op->get_model()->get_reciprocal_lattice(aa, bb, cc);
	      else if (conv)
		op->get_model()->get_conventional_lattice(aa, bb, cc);
	      else
		op->get_model()->get_primitive_lattice(aa, bb, cc);
	      dim = op->get_model()->get_number_of_periodic_dims();
	      for (int i = 0; i < 3; i++) {
		a[i] = r[i][0] * aa[0] + r[i][1] * aa[1] + r[i][2] * aa[2];
		b[i] = r[i][0] * bb[0] + r[i][1] * bb[1] + r[i][2] * bb[2];
		c[i] = r[i][0] * cc[0] + r[i][1] * cc[1] + r[i][2] * cc[2];
	      }
	    }
	  }
	} else {
	  dim = m->get_number_of_periodic_dims();
	  if (is_kspace())
	    m->get_reciprocal_lattice(a, b, c);
	  else if (conv)
	    m->get_conventional_lattice(a, b, c);
	  else
	    m->get_primitive_lattice(a, b, c);
	}
	if (ok == DLV_OK) {
	  // loop to dim should mean that non-periodic ones shouldn't change
	  // from original value set.
	  for (int_g i = 0; i < dim; i++)
	    position[i] = a[i] * x;
	  if (dim > 1) {
	    for (int_g i = 0; i < dim; i++)
	      position[i] += b[i] * y;
	    if (dim > 2) {
	      for (int_g i = 0; i < dim; i++)
		position[i] += c[i] * z;
	    }
	  }
	}
      }
      if (method == 12) {
	// set origin
	real_g offset[3];
	for (int_g i = 0; i < 3; i++)
	  offset[i] = (real_g)position[i] - origin[i];
	origin[0] = (real_g)position[0];
	origin[1] = (real_g)position[1];
	origin[2] = (real_g)position[2];
	pointa[0] += offset[0];
	pointa[1] += offset[1];
	pointa[2] += offset[2];
	pointb[0] += offset[0];
	pointb[1] += offset[1];
	pointb[2] += offset[2];
      } else if (method == 13) {
	pointa[0] = position[0];
	pointa[1] = position[1];
	pointa[2] = position[2];
      } else if (method == 14) {
	pointb[0] = position[0];
	pointb[1] = position[1];
	pointb[2] = position[2];
      } else if (method == 15) {
	// set origin so this is the centre
	real_g average[3];
	// Need average of 4 corners
	for (int_g i = 0; i < 3; i++)
	  average[i] = (real_g)((pointa[i] + pointb[i]) / 2.0);
	real_g offset[3];
	for (int_g i = 0; i < 3; i++)
	  offset[i] = (real_g)position[i] - average[i];
	origin[0] += offset[0];
	origin[1] += offset[1];
	origin[2] += offset[2];
	pointa[0] += offset[0];
	pointa[1] += offset[1];
	pointa[2] += offset[2];
	pointb[0] += offset[0];
	pointb[1] += offset[1];
	pointb[2] += offset[2];
      }
    } else { // 16 - inherit
      data_object *l = operation::find_plane(obj);
      if (l == 0) {
	strncpy(message, "Failed to find selected plane object", mlen);
	return DLV_WARNING;
      } else {
	plane *ldata = dynamic_cast<plane *>(l);
	if (ldata == 0) {
	  strncpy(message, "Selected object isn't plane", mlen);
	  return DLV_WARNING;
	} else {
	  origin[0] = ldata->origin[0];
	  origin[1] = ldata->origin[1];
	  origin[2] = ldata->origin[2];
	  pointa[0] = ldata->pointa[0];
	  pointa[1] = ldata->pointa[1];
	  pointa[2] = ldata->pointa[2];
	  pointb[0] = ldata->pointb[0];
	  pointb[1] = ldata->pointb[1];
	  pointb[2] = ldata->pointb[2];
	}
      }
    }
    dobj->update_plane(origin, pointa, pointb);
  }
  return ok;
}

void DLV::surface_data::reset_data_sets()
{
  n_data_sets = 0;
}

DLV::drawable_obj *
DLV::surface_data::create_drawable(const render_parent *parent,
				   char message[], const int_g mlen)
{
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    obj = DLV::drawable_obj::create_real_slice(parent, origin, a_step, b_step,
					       data, nx, ny, labels,
					       n_data_sets, false, 0.0,
					       message, mlen);
    if (obj != 0)
      set_drawable(obj);
  }
  return obj;
}

DLVreturn_type DLV::real_space_surface::render_data(const model *structure,
						    char message[],
						    const int_g mlen)
{
  // Todo
  return DLV_OK;
}

DLVreturn_type
DLV::rspace_periodic_surface::render_data(const model *structure,
					  char message[], const int_g mlen)
{
  // Todo
  return DLV_OK;
}

bool DLV::surface_data::hide_render(const int_g object, const int_g visible,
				    const render_parent *parent,
				    class model *structure,
				    char message[], const int_g mlen)
{
  // This routine is now badly named
  if (visible == 1) {
    message[0] = '\0';
    if (!reload_data(this, message, mlen)) {
      drawable_obj *obj = get_drawable();
      obj->reload_data(parent, origin, a_step, b_step, data, nx, ny, labels,
		       n_data_sets, false, 0.0, message, mlen);
      reattach_objects(parent);
      return false;
    }
  }
  return true;
}

DLVreturn_type DLV::surface_data::render(const render_parent *parent,
					 class model *structure,
					 const int_g component,
					 const int_g method, const int_g index,
					 char message[], const int_g mlen)
{
  message[0] = '\0';
  bool data_ok = reload_data(this, message, mlen);
  if (strlen(message) > 0)
    return DLV_ERROR;
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    obj = DLV::drawable_obj::create_real_slice(parent, origin, a_step, b_step,
					       data, nx, ny, labels,
					       n_data_sets, false, 0.0,
					       message, mlen);
    if (obj == 0)
      return DLV_ERROR;
    else
      set_drawable(obj);
  } else if (!data_ok) {
    obj->reload_data(parent, origin, a_step, b_step, data, nx, ny, labels,
		     n_data_sets, false, 0.0, message, mlen);
  }
  // Todo - what about vectors?
  display_obj *ptr = 0;
  string name = get_data_label();
  name += " ";
  name += labels[component];
  char buff[32];
  snprintf(buff, 32, " %1d", count_display_objects());
  name += buff;
  // Can't use AG contours for unstructured slice.
  int_g m = method;
  if (get_display_type() == display_2Dunstr)
    m++;
  switch (method) {
  case 0:
    ptr = display_obj::create_ag_contour(parent, obj, is_periodic(),
					 is_kspace(), component, name, index,
					 message, mlen);
    name += " (contours)";
    break;
  case 1:
    ptr = display_obj::create_contour(parent, obj, is_periodic(),
				      is_kspace(), component, name, index,
				      message, mlen);
    name += " (avs contours)";
    break;
  case 4:
    ptr = display_obj::create_pn_contour(parent, obj, is_periodic(),
					 is_kspace(), component, name, index,
					 message, mlen);
    name += " (pn contours)";
    break;
  case 3:
    ptr = display_obj::create_solid_ct(parent, obj, is_periodic(),
				       is_kspace(), component, name, index,
				       message, mlen);
    name += " (solid contours)";
    break;
  case 2:
    ptr = display_obj::create_shaded_ct(parent, obj, is_periodic(),
					is_kspace(), component, name, index,
					message, mlen);
    name += " (shaded contours)";
    break;
  case 5:
    ptr = display_obj::create_surface_plot(parent, obj, is_periodic(),
					   is_kspace(), component, name, index,
					   message, mlen);
    name += " (surface plot)";
    break;
  default:
    strncpy(message, "BUG: Unrecognised slice display method", mlen);
    break;
  }
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

DLV::data_object *DLV::surface_data::edit(const render_parent *parent,
					  class model *structure,
					  const int_g component,
					  const int_g method, const int_g index,
					  char message[], const int_g mlen)
{
  message[0] = '\0';
  edit2D_object *ptr = 0;
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    obj = DLV::drawable_obj::create_real_slice(parent, origin, a_step, b_step,
					       data, nx, ny, labels,
					       n_data_sets, false, 0.0,
					       message, mlen);
    if (obj == 0)
      return ptr;
    else
      set_drawable(obj);
  }
  // Now setup the edited object, the drawable is the output obj
  string name = "Orthoslice of ";
  string basename = get_data_label();
  basename += " ";
  basename += labels[component];
  // vectors don't use extend
  int_g m = method;
  //if (get_sub_is_vector(component) and method > 0)
  //  m++;
  switch (m) {
  case 0:
    name = "Orthoslice of ";
    name += basename;
    ptr = edit2D_ortho::create(parent, this, obj, is_kspace(), name,
			       index, message, mlen);
    break;
    /*
  case 1:
    name = "Extension of ";
    name += basename;
    ptr = edit2D_extend::create(parent, this, obj, component, is_kspace(),
				name, index, message, mlen);
    break;
    */
  case 1:
    name = "Slice of ";
    name += basename;
    ptr = edit2D_slice::create(parent, this, obj, component, is_kspace(), name,
			       index, message, mlen);
    break;
  case 2:
    name = "Clamp of ";
    name += basename;
    ptr = edit2D_clamp::create(parent, this, obj, component, is_kspace(), name,
			       index, message, mlen);
    break;
  case 3:
    name = "Downsize of ";
    name += basename;
    ptr = edit2D_downsize::create(parent, this, obj, component, is_kspace(),
				  name, index, message, mlen);
    break;
  case 4:
    name = "Crop of ";
    name += basename;
    ptr = edit2D_crop::create(parent, this, obj, component, is_kspace(), name,
			      index, message, mlen);
    break;
  case 5:
    name = "Cut of ";
    name += basename;
    ptr = edit2D_cut::create(parent, this, obj, component, is_kspace(), name,
			     index, message, mlen);
    break;
  case 6:
    name = "Math of ";
    name += basename;
    ptr = edit2D_math::create(parent, this, obj, component, is_kspace(), name,
			      index, message, mlen);
    break;
  default:
    strncpy(message, "BUG: Unrecognised edit data method", mlen);
    break;
  }
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate data edit object", mlen);
  }
  return ptr;
}

DLV::edit2D_ortho *DLV::edit2D_ortho::create(const render_parent *parent,
					     data_object *data,
					     const drawable_obj *obj,
					     const bool kspace,
					     const string name,
					     const int_g index, char message[],
					     const int_g mlen)
{
  edited_obj *ptr = edited_obj::create_orthoslice(parent, obj, kspace,
						  message, mlen);
  if (ptr == 0)
    return 0;
  else {
    edit2D_ortho *edit = new edit2D_ortho(data, ptr, index, name);
    if (edit == 0)
      strncpy(message, "Failed to allocate new data object", mlen);
    else {
      edit->set_drawable(ptr->get_drawable());
      data->add_edit(ptr);
      // Do these need to happen after the new op is added?
      ptr->update_edit_list(parent, name, index);
      ptr->attach_params();
    }
    return edit;
  }
}

bool DLV::edit2D_ortho::is_displayable() const
{
  return true;
}

bool DLV::edit2D_ortho::is_editable() const
{
  return false;
}

DLV::string DLV::edit2D_ortho::get_data_label() const
{
  string name = "1D orthoslice of " + get_base_data()->get_data_label();
  return name;
}

DLV::string DLV::edit2D_ortho::get_edit_label() const
{
  return get_data_label();
}

DLV::int_g DLV::edit2D_ortho::get_number_data_sets() const
{
  return get_base_data()->get_number_data_sets();
}

DLV::string DLV::edit2D_ortho::get_sub_data_label(const int_g n) const
{
  return get_base_data()->get_sub_data_label(n);
}

DLV::int_g DLV::edit2D_ortho::get_display_type() const
{
  return (int)display_plot;
}

DLV::int_g DLV::edit2D_ortho::get_edit_type() const
{
  return no_edit;
}

bool DLV::edit2D_ortho::get_sub_is_vector(const int_g n) const
{
  // Todo - no 1D vectors => false?
  return get_base_data()->get_sub_is_vector(n);
}

DLV::edit2D_slice *DLV::edit2D_slice::create(const render_parent *parent,
					     data_object *data,
					     const drawable_obj *obj,
					     const int_g component,
					     const bool kspace,
					     const string name,
					     const int_g index,
					     char message[], const int_g mlen)
{
  edited_obj *ptr = edited_obj::create_slice(parent, obj, kspace,
					     message, mlen);
  if (ptr == 0)
    return 0;
  else {
    edit2D_slice *edit = new edit2D_slice(data, ptr, index, name);
    if (edit == 0)
      strncpy(message, "Failed to allocate new data object", mlen);
    else {
      edit->set_drawable(ptr->get_drawable());
      data->add_edit(ptr);
      // Do these need to happen after the new op is added?
      ptr->update_edit_list(parent, name, index);
      ptr->attach_params();
    }
    return edit;
  }
}

bool DLV::edit2D_slice::is_displayable() const
{
  return true;
}

bool DLV::edit2D_slice::is_editable() const
{
  return false;
}

DLV::string DLV::edit2D_slice::get_data_label() const
{
  string name = "1D slice of " + get_base_data()->get_data_label();
  return name;
}

DLV::string DLV::edit2D_slice::get_edit_label() const
{
  return get_data_label();
}

DLV::int_g DLV::edit2D_slice::get_number_data_sets() const
{
  return get_base_data()->get_number_data_sets();
}

DLV::string DLV::edit2D_slice::get_sub_data_label(const int_g n) const
{
  return get_base_data()->get_sub_data_label(n);
}

DLV::int_g DLV::edit2D_slice::get_display_type() const
{
  return (int)display_plot;
}

DLV::int_g DLV::edit2D_slice::get_edit_type() const
{
  // Todo - no 1D vectors => false?
  return no_edit;
}

bool DLV::edit2D_slice::get_sub_is_vector(const int_g n) const
{
  return get_base_data()->get_sub_is_vector(n);
}

DLV::edit2D_cut *DLV::edit2D_cut::create(const render_parent *parent,
					 data_object *data,
					 const drawable_obj *obj,
					 const int_g component,
					 const bool kspace,
					 const string name, const int_g index,
					 char message[], const int_g mlen)
{
  edited_obj *ptr = edited_obj::create_cut(parent, obj, kspace,
					   message, mlen);
  if (ptr == 0)
    return 0;
  else {
    edit2D_cut *edit = new edit2D_cut(data, ptr, index, name);
    if (edit == 0)
      strncpy(message, "Failed to allocate new data object", mlen);
    else {
      edit->set_drawable(ptr->get_drawable());
      data->add_edit(ptr);
      // Do these need to happen after the new op is added?
      ptr->update_edit_list(parent, name, index);
      ptr->attach_params();
    }
    return edit;
  }
}

bool DLV::edit2D_cut::is_displayable() const
{
  return true;
}

bool DLV::edit2D_cut::is_editable() const
{
  return false;
}

DLV::string DLV::edit2D_cut::get_data_label() const
{
  string name = "2D cut of " + get_base_data()->get_data_label();
  return name;
}

DLV::string DLV::edit2D_cut::get_edit_label() const
{
  return get_data_label();
}

DLV::int_g DLV::edit2D_cut::get_number_data_sets() const
{
  return get_base_data()->get_number_data_sets();
}

DLV::string DLV::edit2D_cut::get_sub_data_label(const int_g n) const
{
  return get_base_data()->get_sub_data_label(n);
}

DLV::int_g DLV::edit2D_cut::get_display_type() const
{
  return (int)display_2D;
}

DLV::int_g DLV::edit2D_cut::get_edit_type() const
{
  return edit_2D;
}

bool DLV::edit2D_cut::get_sub_is_vector(const int_g n) const
{
  return get_base_data()->get_sub_is_vector(n);
}

DLV::edit2D_clamp *DLV::edit2D_clamp::create(const render_parent *parent,
					     data_object *data,
					     const drawable_obj *obj,
					     const int_g component,
					     const bool kspace,
					     const string name,
					     const int_g index,
					     char message[], const int_g mlen)
{
  edited_obj *ptr = edited_obj::create_clamp(parent, obj, component, kspace,
					     message, mlen);
  if (ptr == 0)
    return 0;
  else {
    edit2D_clamp *edit = new edit2D_clamp(data, ptr, index, name, component);
    if (edit == 0)
      strncpy(message, "Failed to allocate new data object", mlen);
    else {
      edit->set_drawable(ptr->get_drawable());
      data->add_edit(ptr);
      // Do these need to happen after the new op is added?
      ptr->update_edit_list(parent, name, index);
      ptr->attach_params();
    }
    return edit;
  }
}

bool DLV::edit2D_clamp::is_displayable() const
{
  return true;
}

bool DLV::edit2D_clamp::is_editable() const
{
  return false;
}

DLV::string DLV::edit2D_clamp::get_data_label() const
{
  string name = "2D clamp of ";
  name += get_base_data()->get_sub_data_label(component);
  name += " ";
  name += get_base_data()->get_data_label();
  return name;
}

DLV::string DLV::edit2D_clamp::get_edit_label() const
{
  return get_data_label();
}

DLV::int_g DLV::edit2D_clamp::get_number_data_sets() const
{
  return 1;
}

DLV::string DLV::edit2D_clamp::get_sub_data_label(const int_g n) const
{
  // Todo - if n > 0?
  return get_base_data()->get_sub_data_label(component);
}

DLV::int_g DLV::edit2D_clamp::get_display_type() const
{
  return (int)display_2D;
}

DLV::int_g DLV::edit2D_clamp::get_edit_type() const
{
  return edit_2D;
}

bool DLV::edit2D_clamp::get_sub_is_vector(const int_g n) const
{
  return get_base_data()->get_sub_is_vector(component);
}

DLV::edit2D_crop *DLV::edit2D_crop::create(const render_parent *parent,
					   data_object *data,
					   const drawable_obj *obj,
					   const int_g component,
					   const bool kspace,
					   const string name, const int_g index,
					   char message[], const int_g mlen)
{
  edited_obj *ptr = edited_obj::create_crop(parent, obj, kspace,
					    message, mlen);
  if (ptr == 0)
    return 0;
  else {
    edit2D_crop *edit = new edit2D_crop(data, ptr, index, name);
    if (edit == 0)
      strncpy(message, "Failed to allocate new data object", mlen);
    else {
      edit->set_drawable(ptr->get_drawable());
      data->add_edit(ptr);
      // Do these need to happen after the new op is added?
      ptr->update_edit_list(parent, name, index);
      ptr->attach_params();
    }
    return edit;
  }
}

bool DLV::edit2D_crop::is_displayable() const
{
  return true;
}

bool DLV::edit2D_crop::is_editable() const
{
  return false;
}

DLV::string DLV::edit2D_crop::get_data_label() const
{
  string name = "2D crop of " + get_base_data()->get_data_label();
  return name;
}

DLV::string DLV::edit2D_crop::get_edit_label() const
{
  return get_data_label();
}

DLV::int_g DLV::edit2D_crop::get_number_data_sets() const
{
  return get_base_data()->get_number_data_sets();
}

DLV::string DLV::edit2D_crop::get_sub_data_label(const int_g n) const
{
  return get_base_data()->get_sub_data_label(n);
}

DLV::int_g DLV::edit2D_crop::get_display_type() const
{
  return (int)display_2D;
}

DLV::int_g DLV::edit2D_crop::get_edit_type() const
{
  return edit_2D;
}

bool DLV::edit2D_crop::get_sub_is_vector(const int_g n) const
{
  return get_base_data()->get_sub_is_vector(n);
}

DLV::edit2D_extend *
DLV::edit2D_extend::create(const render_parent *parent,
			   data_object *data, const drawable_obj *obj,
			   const int_g component, const bool kspace,
			   const string name, const int_g index,
			   char message[], const int_g mlen)
{
  edited_obj *ptr = edited_obj::create_extension(parent, obj, kspace,
						 0, 0, 0, message, mlen);
  if (ptr == 0)
    return 0;
  else {
    edit2D_extend *edit = new edit2D_extend(data, ptr, index, name);
    if (edit == 0)
      strncpy(message, "Failed to allocate new data object", mlen);
    else {
      edit->set_drawable(ptr->get_drawable());
      data->add_edit(ptr);
      // Do these need to happen after the new op is added?
      ptr->update_edit_list(parent, name, index);
      ptr->attach_params();
    }
    return edit;
  }
}

bool DLV::edit2D_extend::is_displayable() const
{
  return true;
}

bool DLV::edit2D_extend::is_editable() const
{
  return false;
}

DLV::string DLV::edit2D_extend::get_data_label() const
{
  string name = "2D extension of " + get_base_data()->get_data_label();
  return name;
}

DLV::string DLV::edit2D_extend::get_edit_label() const
{
  return get_data_label();
}

DLV::int_g DLV::edit2D_extend::get_number_data_sets() const
{
  return get_base_data()->get_number_data_sets();
}

DLV::string DLV::edit2D_extend::get_sub_data_label(const int_g n) const
{
  return get_base_data()->get_sub_data_label(n);
}

DLV::int_g DLV::edit2D_extend::get_display_type() const
{
  return (int)display_2D;
}

DLV::int_g DLV::edit2D_extend::get_edit_type() const
{
  return edit_2D;
}

bool DLV::edit2D_extend::get_sub_is_vector(const int_g n) const
{
  return get_base_data()->get_sub_is_vector(n);
}

DLVreturn_type
DLV::edit2D_extend::update_extension(const render_parent *parent,
				     const int_g na, const int_g nb,
				     const int_g, char message[],
				     const int_g mlen)
{
  // Todo
  return DLV_ERROR;
}

DLV::edit2D_downsize *
DLV::edit2D_downsize::create(const render_parent *parent,
			     data_object *data, const drawable_obj *obj,
			     const int_g component, const bool kspace,
			     const string name, const int_g index,
			     char message[], const int_g mlen)
{
  edited_obj *ptr = edited_obj::create_downsize(parent, obj, kspace,
						message, mlen);
  if (ptr == 0)
    return 0;
  else {
    edit2D_downsize *edit = new edit2D_downsize(data, ptr, index, name);
    if (edit == 0)
      strncpy(message, "Failed to allocate new data object", mlen);
    else {
      edit->set_drawable(ptr->get_drawable());
      data->add_edit(ptr);
      // Do these need to happen after the new op is added?
      ptr->update_edit_list(parent, name, index);
      ptr->attach_params();
    }
    return edit;
  }
}

bool DLV::edit2D_downsize::is_displayable() const
{
  return true;
}

bool DLV::edit2D_downsize::is_editable() const
{
  return false;
}

DLV::string DLV::edit2D_downsize::get_data_label() const
{
  string name = "2D downsize of " + get_base_data()->get_data_label();
  return name;
}

DLV::string DLV::edit2D_downsize::get_edit_label() const
{
  return get_data_label();
}

DLV::int_g DLV::edit2D_downsize::get_number_data_sets() const
{
  return get_base_data()->get_number_data_sets();
}

DLV::string DLV::edit2D_downsize::get_sub_data_label(const int_g n) const
{
  return get_base_data()->get_sub_data_label(n);
}

DLV::int_g DLV::edit2D_downsize::get_display_type() const
{
  return (int)display_2D;
}

DLV::int_g DLV::edit2D_downsize::get_edit_type() const
{
  return edit_2D;
}

bool DLV::edit2D_downsize::get_sub_is_vector(const int_g n) const
{
  return get_base_data()->get_sub_is_vector(n);
}

DLV::edit2D_math *DLV::edit2D_math::create(const render_parent *parent,
					   data_object *data,
					   const drawable_obj *obj,
					   const int_g component,
					   const bool kspace,
					   const string name, const int_g index,
					   char message[], const int_g mlen)
{
  edited_obj *ptr = edited_obj::create_math(parent, obj, component, kspace,
					    message, mlen);
  if (ptr == 0)
    return 0;
  else {
    edit2D_math *edit = new edit2D_math(data, component, ptr, index, name);
    if (edit == 0)
      strncpy(message, "Failed to allocate new data object", mlen);
    else {
      edit->set_drawable(ptr->get_drawable());
      data->add_edit(ptr);
      // Do these need to happen after the new op is added?
      ptr->update_edit_list(parent, name, index);
      ptr->attach_params();
    }
    return edit;
  }
}

bool DLV::edit2D_math::is_displayable() const
{
  return true;
}

bool DLV::edit2D_math::is_editable() const
{
  return false;
}

DLV::string DLV::edit2D_math::get_data_label() const
{
  string name = "2D math of " + get_base_data()->get_data_label();
  return name;
}

DLV::string DLV::edit2D_math::get_edit_label() const
{
  return get_data_label();
}

DLV::int_g DLV::edit2D_math::get_number_data_sets() const
{
  return 1;
}

DLV::string DLV::edit2D_math::get_sub_data_label(const int_g n) const
{
  return get_base_data()->get_sub_data_label(obj_component);
}

DLV::int_g DLV::edit2D_math::get_display_type() const
{
  return (int)display_2D;
}

DLV::int_g DLV::edit2D_math::get_edit_type() const
{
  return edit_2D;
}

bool DLV::edit2D_math::get_sub_is_vector(const int_g n) const
{
  return get_base_data()->get_sub_is_vector(obj_component);
}

DLVreturn_type DLV::edit2D_ortho::render(const render_parent *parent,
					 class model *structure,
					 const int_g component,
					 const int_g method, const int_g index,
					 char message[], const int_g mlen)
{
  strncpy(message, "2D->1D orthoslice rendering not implemented - Todo", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::edit2D_slice::render(const render_parent *parent,
					 class model *structure,
					 const int_g component,
					 const int_g method, const int_g index,
					 char message[], const int_g mlen)
{
  strncpy(message, "2D->1D slice rendering not implemented - Todo", mlen);
  return DLV_ERROR;
}

// Lots of apparent copying from surface_data here - Todo
DLVreturn_type DLV::edit2D_clamp::render(const render_parent *parent,
					 class model *structure,
					 const int_g component,
					 const int_g method, const int_g index,
					 char message[], const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    strncpy(message, "Missing object for surface", mlen);
    return DLV_ERROR;
  }
  // Todo - what about vectors?
  display_obj *ptr = 0;
  string name = get_data_label();
  name += " ";
  name += get_sub_data_label(component);
  char buff[32];
  snprintf(buff, 32, " - %1d", count_display_objects());
  name += buff;
  // Can't use AG contours for unstructured slice.
  int_g m = method;
  if (get_display_type() == display_2Dunstr)
    m++;
  switch (method) {
  case 0:
    ptr = display_obj::create_ag_contour(parent, obj, is_periodic(),
					 is_kspace(), component, name, index,
					 message, mlen);
    name += " (contours)";
    break;
  case 1:
    ptr = display_obj::create_contour(parent, obj, is_periodic(),
				      is_kspace(), component, name, index,
				      message, mlen);
    name += " (avs contours)";
    break;
  case 4:
    ptr = display_obj::create_pn_contour(parent, obj, is_periodic(),
					 is_kspace(), component, name, index,
					 message, mlen);
    name += " (pn contours)";
    break;
  case 3:
    ptr = display_obj::create_solid_ct(parent, obj, is_periodic(),
				       is_kspace(), component, name, index,
				       message, mlen);
    name += " (solid contours)";
    break;
  case 2:
    ptr = display_obj::create_shaded_ct(parent, obj, is_periodic(),
					is_kspace(), component, name, index,
					message, mlen);
    name += " (shaded contours)";
    break;
  case 5:
    ptr = display_obj::create_surface_plot(parent, obj, is_periodic(),
					   is_kspace(), component, name, index,
					   message, mlen);
    name += " (surface plot)";
    break;
  default:
    strncpy(message, "BUG: Unrecognised surface display method", mlen);
    break;
  }
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

DLVreturn_type DLV::edit2D_cut::render(const render_parent *parent,
				       class model *structure,
				       const int_g component,
				       const int_g method, const int_g index,
				       char message[], const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    strncpy(message, "Missing object for surface", mlen);
    return DLV_ERROR;
  }
  // Todo - what about vectors?
  display_obj *ptr = 0;
  string name = get_data_label();
  name += " ";
  name += get_sub_data_label(component);
  char buff[32];
  snprintf(buff, 32, " - %1d", count_display_objects());
  name += buff;
  // Can't use AG contours for unstructured slice.
  int_g m = method;
  if (get_display_type() == display_2Dunstr)
    m++;
  switch (method) {
  case 0:
    ptr = display_obj::create_ag_contour(parent, obj, is_periodic(),
					 is_kspace(), component, name, index,
					 message, mlen);
    name += " (contours)";
    break;
  case 1:
    ptr = display_obj::create_contour(parent, obj, is_periodic(),
				      is_kspace(), component, name, index,
				      message, mlen);
    name += " (avs contours)";
    break;
  case 4:
    ptr = display_obj::create_pn_contour(parent, obj, is_periodic(),
					 is_kspace(), component, name, index,
					 message, mlen);
    name += " (pn contours)";
    break;
  case 3:
    ptr = display_obj::create_solid_ct(parent, obj, is_periodic(),
				       is_kspace(), component, name, index,
				       message, mlen);
    name += " (solid contours)";
    break;
  case 2:
    ptr = display_obj::create_shaded_ct(parent, obj, is_periodic(),
					is_kspace(), component, name, index,
					message, mlen);
    name += " (shaded contours)";
    break;
  case 5:
    ptr = display_obj::create_surface_plot(parent, obj, is_periodic(),
					   is_kspace(), component, name, index,
					   message, mlen);
    name += " (surface plot)";
    break;
  default:
    strncpy(message, "BUG: Unrecognised surface display method", mlen);
    break;
  }
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

DLVreturn_type DLV::edit2D_crop::render(const render_parent *parent,
					class model *structure,
					const int_g component,
					const int_g method, const int_g index,
					char message[], const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    strncpy(message, "Missing object for surface", mlen);
    return DLV_ERROR;
  }
  // Todo - what about vectors?
  display_obj *ptr = 0;
  string name = get_data_label();
  name += " ";
  name += get_sub_data_label(component);
  char buff[32];
  snprintf(buff, 32, " - %1d", count_display_objects());
  name += buff;
  // Can't use AG contours for unstructured slice.
  int_g m = method;
  if (get_display_type() == display_2Dunstr)
    m++;
  switch (method) {
  case 0:
    ptr = display_obj::create_ag_contour(parent, obj, is_periodic(),
					 is_kspace(), component, name, index,
					 message, mlen);
    name += " (contours)";
    break;
  case 1:
    ptr = display_obj::create_contour(parent, obj, is_periodic(),
				      is_kspace(), component, name, index,
				      message, mlen);
    name += " (avs contours)";
    break;
  case 4:
    ptr = display_obj::create_pn_contour(parent, obj, is_periodic(),
					 is_kspace(), component, name, index,
					 message, mlen);
    name += " (pn contours)";
    break;
  case 3:
    ptr = display_obj::create_solid_ct(parent, obj, is_periodic(),
				       is_kspace(), component, name, index,
				       message, mlen);
    name += " (solid contours)";
    break;
  case 2:
    ptr = display_obj::create_shaded_ct(parent, obj, is_periodic(),
					is_kspace(), component, name, index,
					message, mlen);
    name += " (shaded contours)";
    break;
  case 5:
    ptr = display_obj::create_surface_plot(parent, obj, is_periodic(),
					   is_kspace(), component, name, index,
					   message, mlen);
    name += " (surface plot)";
    break;
  default:
    strncpy(message, "BUG: Unrecognised surface display method", mlen);
    break;
  }
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

DLVreturn_type DLV::edit2D_downsize::render(const render_parent *parent,
					    class model *structure,
					    const int_g component,
					    const int_g method,
					    const int_g index,
					    char message[], const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    strncpy(message, "Missing object for surface", mlen);
    return DLV_ERROR;
  }
  // Todo - what about vectors?
  display_obj *ptr = 0;
  string name = get_data_label();
  name += " ";
  name += get_sub_data_label(component);
  char buff[32];
  snprintf(buff, 32, " - %1d", count_display_objects());
  name += buff;
  // Can't use AG contours for unstructured slice.
  int_g m = method;
  if (get_display_type() == display_2Dunstr)
    m++;
  switch (method) {
  case 0:
    ptr = display_obj::create_ag_contour(parent, obj, is_periodic(),
					 is_kspace(), component, name, index,
					 message, mlen);
    name += " (contours)";
    break;
  case 1:
    ptr = display_obj::create_contour(parent, obj, is_periodic(),
				      is_kspace(), component, name, index,
				      message, mlen);
    name += " (avs contours)";
    break;
  case 4:
    ptr = display_obj::create_pn_contour(parent, obj, is_periodic(),
					 is_kspace(), component, name, index,
					 message, mlen);
    name += " (pn contours)";
    break;
  case 3:
    ptr = display_obj::create_solid_ct(parent, obj, is_periodic(),
				       is_kspace(), component, name, index,
				       message, mlen);
    name += " (solid contours)";
    break;
  case 2:
    ptr = display_obj::create_shaded_ct(parent, obj, is_periodic(),
					is_kspace(), component, name, index,
					message, mlen);
    name += " (shaded contours)";
    break;
  case 5:
    ptr = display_obj::create_surface_plot(parent, obj, is_periodic(),
					   is_kspace(), component, name, index,
					   message, mlen);
    name += " (surface plot)";
    break;
  default:
    strncpy(message, "BUG: Unrecognised surface display method", mlen);
    break;
  }
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

DLVreturn_type DLV::edit2D_extend::render(const render_parent *parent,
					  class model *structure,
					  const int_g component,
					  const int_g method, const int_g index,
					  char message[], const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    strncpy(message, "Missing object for surface", mlen);
    return DLV_ERROR;
  }
  // Todo - what about vectors?
  display_obj *ptr = 0;
  string name = get_data_label();
  name += " ";
  name += get_sub_data_label(component);
  char buff[32];
  snprintf(buff, 32, " - %1d", count_display_objects());
  name += buff;
  // Can't use AG contours for unstructured slice.
  int_g m = method;
  if (get_display_type() == display_2Dunstr)
    m++;
  switch (method) {
  case 0:
    ptr = display_obj::create_ag_contour(parent, obj, is_periodic(),
					 is_kspace(), component, name, index,
					 message, mlen);
    name += " (contours)";
    break;
  case 1:
    ptr = display_obj::create_contour(parent, obj, is_periodic(),
				      is_kspace(), component, name, index,
				      message, mlen);
    name += " (avs contours)";
    break;
  case 4:
    ptr = display_obj::create_pn_contour(parent, obj, is_periodic(),
					 is_kspace(), component, name, index,
					 message, mlen);
    name += " (pn contours)";
    break;
  case 3:
    ptr = display_obj::create_solid_ct(parent, obj, is_periodic(),
				       is_kspace(), component, name, index,
				       message, mlen);
    name += " (solid contours)";
    break;
  case 2:
    ptr = display_obj::create_shaded_ct(parent, obj, is_periodic(),
					is_kspace(), component, name, index,
					message, mlen);
    name += " (shaded contours)";
    break;
  case 5:
    ptr = display_obj::create_surface_plot(parent, obj, is_periodic(),
					   is_kspace(), component, name, index,
					   message, mlen);
    name += " (surface plot)";
    break;
  default:
    strncpy(message, "BUG: Unrecognised surface display method", mlen);
    break;
  }
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

DLVreturn_type DLV::edit2D_math::render(const render_parent *parent,
					class model *structure,
					const int_g component,
					const int_g method, const int_g index,
					char message[], const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    strncpy(message, "Missing object for surface", mlen);
    return DLV_ERROR;
  }
  // Todo - what about vectors?
  display_obj *ptr = 0;
  string name = get_data_label();
  name += " ";
  name += get_sub_data_label(component);
  char buff[32];
  snprintf(buff, 32, " - %1d", count_display_objects());
  name += buff;
  // Can't use AG contours for unstructured slice.
  int_g m = method;
  if (get_display_type() == display_2Dunstr)
    m++;
  switch (method) {
  case 0:
    ptr = display_obj::create_ag_contour(parent, obj, is_periodic(),
					 is_kspace(), component, name, index,
					 message, mlen);
    name += " (contours)";
    break;
  case 1:
    ptr = display_obj::create_contour(parent, obj, is_periodic(),
				      is_kspace(), component, name, index,
				      message, mlen);
    name += " (avs contours)";
    break;
  case 4:
    ptr = display_obj::create_pn_contour(parent, obj, is_periodic(),
					 is_kspace(), component, name, index,
					 message, mlen);
    name += " (pn contours)";
    break;
  case 3:
    ptr = display_obj::create_solid_ct(parent, obj, is_periodic(),
				       is_kspace(), component, name, index,
				       message, mlen);
    name += " (solid contours)";
    break;
  case 2:
    ptr = display_obj::create_shaded_ct(parent, obj, is_periodic(),
					is_kspace(), component, name, index,
					message, mlen);
    name += " (shaded contours)";
    break;
  case 5:
    ptr = display_obj::create_surface_plot(parent, obj, is_periodic(),
					   is_kspace(), component, name, index,
					   message, mlen);
    name += " (surface plot)";
    break;
  default:
    strncpy(message, "BUG: Unrecognised surface display method", mlen);
    break;
  }
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

void DLV::edit2D_math::data_math_parent(const data_object * &obj,
					int_g &component) const
{
  obj = get_base_data();
  component = obj_component;
}

void DLV::surface_data::match_data_math(const data_object *obj,
					const int_g selection, const bool all,
					const int_g cmpt, int_g &nmatch,
					int_g &n) const
{
  const surface_data *data = dynamic_cast<const surface_data *>(obj);
  if (data != 0) {
    if (is_periodic() == data->is_periodic() and
	is_kspace() == data->is_kspace()) {
      if (nx == data->nx and ny == data->ny) {
	// Todo - worry about vectors?
	if (all) {
	  nmatch = selection - n;
	  n += n_data_sets;
	} else {
	  real_g o1[3];
	  real_g o2[3];
	  for (int_g i = 0; i < 3; i++)
	    o1[i] = origin[i];
	  for (int_g i = 0; i < 3; i++)
	    o2[i] = data->origin[i];
	  const real_g tol = 0.0001;
	  bool ok = true;
	  for (int_g i = 0; i < 3; i++)
	    ok = ok and (std::abs(o1[i] - o2[i]) < tol);
	  if (ok) {
	    for (int_g i = 0; i < 3; i++)
	      o1[i] = a_step[i];
	    for (int_g i = 0; i < 3; i++)
	      o2[i] = data->a_step[i];
	    bool ok = true;
	    for (int_g i = 0; i < 3; i++)
	      ok = ok and (std::abs(o1[i] - o2[i]) < tol);
	    if (ok) {
	      for (int_g i = 0; i < 3; i++)
		o1[i] = b_step[i];
	      for (int_g i = 0; i < 3; i++)
		o2[i] = data->b_step[i];
	      bool ok = true;
	      for (int_g i = 0; i < 3; i++)
		ok = ok and (std::abs(o1[i] - o2[i]) < tol);
	      if (ok) {
		for (int_g i = 0; i < n_data_sets; i++) {
		  if (labels[i] == data->labels[cmpt]) {
		    if (selection == n)
		      nmatch = i;
		    n++;
		  }
		}
	      }
	    }
	  }
	}
      }
    }
  }
}

// Todo - most of this is a copy of match
void DLV::surface_data::list_data_math(const data_object *obj, const bool all,
				       const int_g cmpt, string *names,
				       int_g &n) const
{
  const surface_data *data = dynamic_cast<const surface_data *>(obj);
  if (data != 0) {
    if (is_periodic() == data->is_periodic() and
	is_kspace() == data->is_kspace()) {
      if (nx == data->nx and ny == data->ny) {
	// Todo - worry about vectors?
	string base = get_data_label() + " - ";
	if (all) {
	  for (int_g i = 0; i < n_data_sets; i++) {
	    names[n] = base + get_sub_data_label(i);
	    n++;
	  }
	} else {
	  real_g o1[3];
	  real_g o2[3];
	  for (int_g i = 0; i < 3; i++)
	    o1[i] = origin[i];
	  for (int_g i = 0; i < 3; i++)
	    o2[i] = data->origin[i];
	  const real_g tol = 0.0001;
	  bool ok = true;
	  for (int_g i = 0; i < 3; i++)
	    ok = ok and (std::abs(o1[i] - o2[i]) < tol);
	  if (ok) {
	    for (int_g i = 0; i < 3; i++)
	      o1[i] = a_step[i];
	    for (int_g i = 0; i < 3; i++)
	      o2[i] = data->a_step[i];
	    bool ok = true;
	    for (int_g i = 0; i < 3; i++)
	      ok = ok and (std::abs(o1[i] - o2[i]) < tol);
	    if (ok) {
	      for (int_g i = 0; i < 3; i++)
		o1[i] = b_step[i];
	      for (int_g i = 0; i < 3; i++)
		o2[i] = data->b_step[i];
	      bool ok = true;
	      for (int_g i = 0; i < 3; i++)
		ok = ok and (std::abs(o1[i] - o2[i]) < tol);
	      if (ok) {
		for (int_g i = 0; i < n_data_sets; i++) {
		  if (labels[i] == data->labels[cmpt]) {
		    names[n] = base + get_sub_data_label(i);
		    n++;
		  }
		}
	      }
	    }
	  }
	}
      }
    }
  }
}

DLVreturn_type DLV::edit2D_math::update_data_math(const render_parent *parent,
						  data_object *obj,
						  const int_g cmpt,
						  const int_g index,
						  char message[],
						  const int_g mlen)
{
  switch (index) {
  case 0:
    obj1 = obj;
    component1 = cmpt;
    break;
  case 1:
    obj2 = obj;
    component2 = cmpt;
    break;
  case 2:
    obj3 = obj;
    component3 = cmpt;
    break;
  default:
    strncpy(message, "BUG: invalid data math field index", mlen);
    return DLV_ERROR;
  }
  message[0] = '\0';
  edited_obj *myedit = get_edit_obj();
  if (myedit == 0) {
    strncpy(message, "Missing object for data math", mlen);
    return DLV_ERROR;
  }
  drawable_obj *p = obj->create_drawable(parent, message, mlen);
  if (p == 0) {
    strncpy(message, "Missing field for data math", mlen);
    return DLV_ERROR;
  }
  if (myedit->update_math(p, cmpt, index, message, mlen))
    return DLV_OK;
  else
    return DLV_ERROR;
}

#endif // ENABLE_DLV_GRAPHICS


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

#  ifndef ENABLE_DLV_GRAPHICS
    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::surface_data *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::surface_data("recover", "", nullptr, 0, 0, nullptr, nullptr,
				nullptr);
    }
#  endif // ENABLE_DLV_GRAPHICS

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::real_space_plane *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::real_space_plane("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::k_space_plane *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::k_space_plane("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    DLV::real_space_surface *t,
				    const unsigned int file_version)
    {
      float a[3];
      float b[3];
      float c[3];
      ::new(t)DLV::real_space_surface("recover", "temp",
				      0, 0, 0, a, b, c);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    DLV::rspace_periodic_surface *t,
				    const unsigned int file_version)
    {
      float a[3];
      float b[3];
      float c[3];
      ::new(t)DLV::rspace_periodic_surface("recover", "temp",
					   0, 0, 0, a, b, c);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::plane *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::plane("recover");
    }
  }
}

template <class Archive>
void DLV::plane::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<data_object>(*this);
  ar & name;
  ar & origin;
  ar & pointa;
  ar & pointb;
}

template <class Archive>
void DLV::real_space_plane::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<plane>(*this);
}

template <class Archive>
void DLV::k_space_plane::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<plane>(*this);
}

template <class Archive>
void DLV::surface_data::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<reloadable_data>(*this);
  ar & nx;
  ar & ny;
  ar & n_data_sets;
  for (int_g i = 0; i < n_data_sets; i++)
    ar & labels[i];
  for (int_g i = 0; i < 3; i++) {
    ar & origin[i];
    ar & a_step[i];
    ar & b_step[i];
  }
}

template <class Archive>
void DLV::surface_data::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<reloadable_data>(*this);
  ar & nx;
  ar & ny;
  ar & n_data_sets;
  labels = new string[n_data_sets];
  data = new real_g *[n_data_sets];
  size = n_data_sets;
  for (int_g i = 0; i < n_data_sets; i++)
    ar & labels[i];
  for (int_g i = 0; i < 3; i++) {
    ar & origin[i];
    ar & a_step[i];
    ar & b_step[i];
  }
}

template <class Archive>
void DLV::real_space_surface::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<surface_data>(*this);
}

template <class Archive>
void DLV::rspace_periodic_surface::serialize(Archive &ar,
					     const unsigned int version)
{
  ar & boost::serialization::base_object<real_space_surface>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::plane)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::real_space_plane)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::k_space_plane)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::surface_data)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::real_space_surface)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::rspace_periodic_surface)

DLV_SUPPRESS_TEMPLATES(DLV::data_object)
DLV_SUPPRESS_TEMPLATES(DLV::reloadable_data)

DLV_NORMAL_EXPLICIT(DLV::plane)
DLV_NORMAL_EXPLICIT(DLV::real_space_plane)
DLV_NORMAL_EXPLICIT(DLV::k_space_plane)
DLV_SPLIT_EXPLICIT(DLV::surface_data)
DLV_NORMAL_EXPLICIT(DLV::real_space_surface)
DLV_NORMAL_EXPLICIT(DLV::rspace_periodic_surface)

#  ifdef ENABLE_DLV_GRAPHICS

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::edit2D_ortho *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::edit2D_ortho(0, 0, 0, "");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::edit2D_slice *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::edit2D_slice(0, 0, 0, "");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::edit2D_cut *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::edit2D_cut(0, 0, 0, "");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::edit2D_crop *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::edit2D_crop(0, 0, 0, "");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::edit2D_clamp *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::edit2D_clamp(0, 0, 0, "", 0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::edit2D_extend *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::edit2D_extend(0, 0, 0, "");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::edit2D_downsize *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::edit2D_downsize(0, 0, 0, "");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::edit2D_math *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::edit2D_math(0, 0, 0, 0, "");
    }
  }
}

template <class Archive>
void DLV::edit2D_object::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit_object>(*this);
}

template <class Archive>
void DLV::edit2D_ortho::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit2D_object>(*this);
}

template <class Archive>
void DLV::edit2D_slice::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit2D_object>(*this);
}

template <class Archive>
void DLV::edit2D_cut::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit2D_object>(*this);
}

template <class Archive>
void DLV::edit2D_clamp::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit2D_object>(*this);
  ar & component;
}

template <class Archive>
void DLV::edit2D_crop::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit2D_object>(*this);
}

template <class Archive>
void DLV::edit2D_extend::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit2D_object>(*this);
}

template <class Archive>
void DLV::edit2D_downsize::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit2D_object>(*this);
}

template <class Archive>
void DLV::edit2D_math::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit2D_object>(*this);
  ar & obj_component;
  ar & obj1;
  ar & obj2;
  ar & obj3;
  ar & component1;
  ar & component2;
  ar & component3;
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::edit2D_ortho)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::edit2D_slice)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::edit2D_cut)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::edit2D_clamp)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::edit2D_crop)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::edit2D_extend)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::edit2D_downsize)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::edit2D_math)

DLV_SUPPRESS_TEMPLATES(DLV::edit_object)

DLV_NORMAL_EXPLICIT(DLV::edit2D_ortho)
DLV_NORMAL_EXPLICIT(DLV::edit2D_slice)
DLV_NORMAL_EXPLICIT(DLV::edit2D_cut)
DLV_NORMAL_EXPLICIT(DLV::edit2D_clamp)
DLV_NORMAL_EXPLICIT(DLV::edit2D_crop)
DLV_NORMAL_EXPLICIT(DLV::edit2D_extend)
DLV_NORMAL_EXPLICIT(DLV::edit2D_downsize)
DLV_NORMAL_EXPLICIT(DLV::edit2D_math)

#  endif // ENABLE_DLV_GRAPHICS

#endif // DLV_USES_SERIALIZE
