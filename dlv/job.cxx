
#ifdef WIN32
#  include <windows.h>
#  include <direct.h>
#  include <time.h>
#else
#  include <unistd.h>
#  include <sys/stat.h>
#  include <sys/types.h>
#  include <sys/wait.h>
#  include <fcntl.h>
#  include <errno.h>
#  include <sys/socket.h>
#  ifdef __DARWIN_UNIX03
#    include <signal.h>
#  endif // DARWIN
#endif // WIN32

#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  ifndef WIN32
#    include "../graphics/events.hxx"
#  endif // WIN32
#  include "../graphics/render_base.hxx" // for data_objs
#endif // ENABLE_DLV_GRAPHICS
#include "utils.hxx"
#include "file.hxx"
#include "job_setup.hxx"
#include "job.hxx"
#include "job_impl.hxx"
#include "data_objs.hxx"
#include "operation.hxx"

// Todo - make more use of boost::filesystem for portable copy_file etc

#ifdef ENABLE_DLV_JOB_THREADS

boost::mutex DLV::job::mutex;
bool DLV::job::global_update = false;

#endif // ENABLE_DLV_JOB_THREADS

DLV::job::~job()
{
  // Todo?
}

DLV::job *DLV::job::create(const string local_path, const string serial_bin,
			   const string parallel_bin, const string args,
			   const job_setup_data &job_data,
			   const bool use_stdin, const int_g input,
			   file_obj *output, file_obj *error, file_obj *jerror,
			   std::map<int, file_obj *> &inp,
			   std::map<int, file_obj *> &out,
			   const bool allow_parallel_job,
			   const bool external_job,
			   const string external_dir)
{
  job *new_job = 0;
  if (allow_parallel_job and job_data.is_parallel()) {
    string exec = local_path;
    if (local_path.length() > 0)
      if (local_path[local_path.length() - 1] != REMOTE_DIR_CHAR)
	exec += REMOTE_DIR_CHAR;
    exec += parallel_bin;
  } else {
    if (external_job or job_data.is_local()) {
      string exec = local_path;
      if (local_path.length() > 0)
	if (local_path[local_path.length() - 1] != DIR_SEP_CHR)
	  exec += DIR_SEP_CHR;
      exec += serial_bin;
      exec += LOCAL_EXEC_SUFFIX;
      if (external_job)
	new_job = new user_job(exec, args, use_stdin, input, output, error,
			       jerror, inp, out);
      else
	new_job = new local_job(exec, args, use_stdin, input, output, error,
				jerror, inp, out);
    } else
      ;
  }
  if (new_job != 0) {
    string dir;
    if (external_job) {
      dir = external_dir;
      if (external_dir[external_dir.length() - 1] != DIR_SEP_CHR)
	dir += DIR_SEP_CHR;
    } else
      dir = make_scratch_name(job_data.get_scratch_dir());
    new_job->scratch_dir = dir;
    //input->set_run_directory(dir, job_data.get_host());
    output->set_run_directory(dir, job_data.get_host());
    error->set_run_directory(dir, job_data.get_host());
    if (jerror != 0)
      jerror->set_run_directory(dir, job_data.get_host());
    std::map<int, file_obj *>::const_iterator x;
    for (x = inp.begin(); x != inp.end(); ++x )
      x->second->set_run_directory(dir, job_data.get_host());
    for (x = out.begin(); x != out.end(); ++x )
      x->second->set_run_directory(dir, job_data.get_host());
    // Todo - anything else?
  }
  return new_job;
}

DLV::job *DLV::job::connect(const string binary, file_obj *jerror,
			    std::map<int, file_obj *> &inp,
			    std::map<int, file_obj *> &out,
			    const string dir, class text_buffer *data,
			    char message[], const int_g mlen)
{
  job *new_job = 0;
  string exec = binary;
  new_job = new socket_job(binary, jerror, inp, out, data);
  if (new_job != 0) {
    new_job->scratch_dir = dir;
    // we run in the job dir
    std::map<int, file_obj *>::const_iterator x;
    for (x = inp.begin(); x != inp.end(); ++x )
      x->second->set_run_directory(dir, "localhost");
  }
  return new_job;
}

DLV::job::job(const string binary, const string args, const bool input,
	      const int_g in_idx, file_obj *output, file_obj *serror,
	      file_obj *jerror, std::map<int, file_obj *> &inp,
	      std::map<int, file_obj *> &out, const job_status_type js)
  : executable(binary), cmdline_args(args), status(js),
    stdoutput(output), stderror(serror), joberror(jerror), use_stdin(input),
    sinput(in_idx), inp_files(inp), out_files(out)
#ifdef ENABLE_DLV_JOB_THREADS
  , thread(0), status_update(false)
#endif // ENABLE_DLV_JOB_THREADS
{
}

bool DLV::job::execute(const bool foreground, char message[], const int_g mlen)
{
  if (copy_files(message, mlen))
    return run_binary(foreground, message, mlen);
  return false;
}

bool DLV::user_job::run_binary(const bool, char message[], const int_g mlen)
{
  //set_status(nonDLV_job);
  string name = get_job_dir();
  name += "README";
  std::ofstream readme;
  if (open_file_write(readme, name.c_str(), message, mlen)) {
    readme << get_executable();
    readme << " < " << get_stdin()->get_runname();
    readme << " > " << get_output()->get_runname() << std::endl;
    readme << "DLV expects to read the following files from this directory\n";
    readme << get_output()->get_runname() << std::endl;
    if (get_joberr() != 0) {
      readme << get_joberr()->get_runname();
      readme << " (if it exists)" << std::endl;
    }
    std::map<int, file_obj *>::const_iterator x;
    for (x = out_begin(); x != out_end(); ++x )
      readme << x->second->get_runname() << std::endl;
    readme.close();
  }
  return true;
}

DLV::job_status_type DLV::user_job::get_status()
{
  return get_current_status();
}

void DLV::job::reset_user_status()
{
  // dummy
}

void DLV::user_job::reset_user_status()
{
  set_status(run_and_completed);
}

bool DLV::user_job::kill(char message[], const int_g mlen)
{
  // Do nothing
  return true;
}

bool DLV::local_job::tidy_files(char message[], const int_g mlen)
{
  stop_monitoring();
  if (is_external_job()) {
    // probably failed because the directory existed, don't delete it!
    if (get_status() == not_started)
      return true;
  }
  if (get_job_dir().length() > 0) {
    try {
      boost::filesystem::path dir(get_job_dir());
      (void) boost::filesystem::remove_all(dir);
      //strncpy(message,
      //	"Unable to delete files from scratch directory", mlen);
    }
    catch (...) {
      strncpy(message,
	      "Unable to delete files from scratch directory (boost)",
	      mlen);
    }
  }
  return true;
}

bool DLV::job::is_external_job() const
{
  return false;
}

bool DLV::user_job::is_external_job() const
{
  return true;
}

#ifdef WIN32

DLV::string DLV::job::make_scratch_name(const string dir, const bool exclude)
{
  time_t stamp;
  (void) time(&stamp);
  char timestr[128];
  strftime(timestr, 128, "%Y%m%d%H%M%S", localtime(&stamp));
  string name = dir + "dlv-";
  name += timestr;
  if (!exclude)
    name += DIR_SEP_CHR;
  return name;
}

bool DLV::local_job::copy_files(char message[], const int_g mlen)
{
  if (!use_directory(get_job_dir(), true, message, mlen)) {
    set_status(DLV::not_started);
    return false;
  }
  std::map<int, file_obj *>::const_iterator x;
  for (x = inp_begin(); x != inp_end(); ++x ) {
    if (CopyFileA((LPCSTR)x->second->get_filename().c_str(),
		 (LPCSTR)x->second->get_runname().c_str(), FALSE) == 0) {
      strncpy(message, "Unable to copy input files to scratch directory",
	      mlen);
      set_status(DLV::not_started);
      return false;
    }
  }
  return true;
}

bool DLV::local_job::recover_files(bool &err_file, bool &log_file,
				   char message[], const int_g mlen)
{
  err_file = false;
  log_file = true;
  job_status_type job_status = get_current_status();
  bool ok = true;
  if (job_status == run_and_completed or job_status == run_and_failed) {
    if (CopyFileA((LPCSTR)get_error()->get_runname().c_str(),
		  (LPCSTR)get_error()->get_filename().c_str(), FALSE) == 0) {
      ok = false;
      strncpy(message, "Unable to copy sys error file from scratch directory",
	      mlen);
      //err_file = false;
    }
    if (get_joberr() != 0) {
      if (file_exists(get_joberr()->get_runname().c_str())) {
	if (CopyFileA((LPCSTR)get_joberr()->get_runname().c_str(),
		      (LPCSTR)get_joberr()->get_filename().c_str(),
		      FALSE) == 0) {
	  ok = false;
	  strncpy(message,
		  "Unable to copy job error file from scratch directory",
		  mlen);
	} else
	  err_file = true;
      }
    }
    if (CopyFileA((LPCSTR)get_output()->get_runname().c_str(),
		  (LPCSTR)get_output()->get_filename().c_str(), FALSE) == 0) {
      ok = false;
      strncpy(message, "Unable to copy log file from scratch directory",
	      mlen);
      log_file = false;
    }
    std::map<int, file_obj *>::const_iterator x;
    for (x = out_begin(); x != out_end(); ++x ) {
      if (CopyFileA((LPCSTR)x->second->get_runname().c_str(),
		   (LPCSTR)x->second->get_filename().c_str(), FALSE) == 0) {
	ok = false;
	strncpy(message, "Unable to copy output files from scratch directory",
		mlen);
      }
    }
    // Tidy the files up even if copying had problems.
    if (ok)
      ok = tidy_files(message, mlen);
    else {
      char info[256];
      (void) tidy_files(info, 256);
    }
    if (ok)
      set_status(run_and_recovered);
    else
      set_status(recovered_with_errors);
    // Todo log_recovered = true;
  }
  return ok;
}

bool DLV::user_job::recover_files(bool &err_file, bool &log_file,
				  char message[], const int_g mlen)
{
  err_file = false;
  log_file = true;
  job_status_type job_status = get_current_status();
  bool ok = true;
  if (job_status == run_and_completed or job_status == run_and_failed) {
    if (get_joberr() != 0) {
      if (file_exists(get_joberr()->get_runname().c_str())) {
	if (CopyFileA((LPCSTR)get_joberr()->get_runname().c_str(),
		      (LPCSTR)get_joberr()->get_filename().c_str(),
		      FALSE) == 0) {
	  ok = false;
	  strncpy(message,
		  "Unable to copy job error file from scratch directory",
		  mlen);
	} else
	  err_file = true;
      }
    }
    if (CopyFileA((LPCSTR)get_output()->get_runname().c_str(),
		  (LPCSTR)get_output()->get_filename().c_str(), FALSE) == 0) {
      ok = false;
      strncpy(message, "Unable to copy log file from scratch directory",
	      mlen);
      log_file = false;
    }
    std::map<int, file_obj *>::const_iterator x;
    for (x = out_begin(); x != out_end(); ++x ) {
      if (CopyFileA((LPCSTR)x->second->get_runname().c_str(),
		   (LPCSTR)x->second->get_filename().c_str(), FALSE) == 0) {
	ok = false;
	strncpy(message, "Unable to copy output files from scratch directory",
		mlen);
      }
    }
    // Tidy the files up even if copying had problems.
    if (ok)
      ok = tidy_files(message, mlen);
    else {
      char info[256];
      (void) tidy_files(info, 256);
    }
    if (ok)
      set_status(run_and_recovered);
    else
      set_status(recovered_with_errors);
    // Todo log_recovered = true;
  }
  return ok;
}

bool DLV::local_job::run_binary(const bool foreground,
				char message[], const int_g mlen)
{
  if (!file_exists(get_executable().c_str())) {
    strncpy(message, "Can't find executable", mlen);
    return false;
  }
  bool ok = true;
  string dir = get_job_dir();
  char exe[1024];
  std::strcpy(exe, get_executable().c_str());
  char buff[4096];
  std::strcpy(buff, exe);
  strcat(buff, " ");
  std::strcat(buff, get_cmdline_args().c_str());
  STARTUPINFOA si;
  PROCESS_INFORMATION pi;
  memset(&si, 0, sizeof(si));
  si.cb = sizeof(si);
  si.lpReserved = NULL;
  si.lpDesktop = NULL;
  si.lpTitle = NULL;
  si.dwFlags = STARTF_USESHOWWINDOW | STARTF_USESTDHANDLES;
  si.wShowWindow = SW_HIDE;
  // From MSDN example
  SECURITY_ATTRIBUTES sa;
  memset(&sa, 0, sizeof(sa));
  sa.nLength = sizeof(sa);
  sa.bInheritHandle = TRUE;
  sa.lpSecurityDescriptor = NULL;
  HANDLE inpfd = 0;
  if (need_stdin()) {
    inpfd = CreateFileA((LPCSTR)get_stdin()->get_runname().c_str(),
			GENERIC_READ, 0, &sa, OPEN_EXISTING, 0, NULL);
    if (inpfd == INVALID_HANDLE_VALUE) {
      strncpy(message, "Open of input file failed", mlen);
      return false;
    }
  }
  si.hStdInput = inpfd;
  HANDLE outfd = CreateFileA((LPCSTR)get_output()->get_runname().c_str(),
			     GENERIC_WRITE, 0, &sa, CREATE_ALWAYS, 0, NULL);
  if (outfd == INVALID_HANDLE_VALUE) {
    strncpy(message, "Open of output file failed", mlen);
    return false;
  }
  si.hStdOutput = outfd;
  HANDLE errfd = CreateFileA((LPCSTR)get_error()->get_runname().c_str(),
			     GENERIC_WRITE, 0, &sa, CREATE_ALWAYS, 0, NULL);
  if (errfd == INVALID_HANDLE_VALUE) {
    strncpy(message, "Open of error file failed", mlen);
    return false;
  }
  si.hStdError = errfd;
  int_g flags = CREATE_DEFAULT_ERROR_MODE;
  BOOL process = CreateProcessA((LPCSTR)exe, buff, NULL, NULL, TRUE,
				flags, NULL, (LPCSTR)dir.c_str(), &si, &pi);
  CloseHandle(errfd);
  CloseHandle(outfd);
  if (need_stdin())
    CloseHandle(inpfd);
  if (process == 0) {
    // If we get here exec failed, may need better error info
    strncpy(message, "Unable to run local command", mlen);
    set_status(run_and_failed);
    return false;
  } else {
    if (foreground) {
      DWORD jstat = 0;
      WaitForSingleObject(pi.hProcess, INFINITE);
      GetExitCodeProcess(pi.hProcess, &jstat);
      if (jstat != 0) {
	ok = false;
	set_status(run_and_failed);
      } else
	set_status(run_and_completed);
    } else {
      //set_status(still_running);
      set_process_id(pi.dwProcessId);
      start_monitoring();
    }
    CloseHandle(pi.hThread);
    CloseHandle(pi.hProcess);
  }
  return ok;
}

DLV::job_status_type DLV::local_job::get_status()
{
  job_status_type job_status = get_current_status();
  if (job_status == initialising_job or job_status == still_running or
      job_status == running_and_updated) {
    if (process_id == 0)
      job_status = not_started;
    //else if (pid == -1) {
    //job_status = run_and_completed;
    else {
      HANDLE phandle = OpenProcess(PROCESS_QUERY_INFORMATION,
				   FALSE, process_id);
      if (phandle == 0)
	job_status = run_and_completed;
      else {
	DWORD status;
	if (GetExitCodeProcess(phandle, &status)) {
	  if (status != STILL_ACTIVE) {
	    job_status = run_and_completed;
	    //DLVset_normal_cursor();
	  } else {
	    job_status = still_running;
	    //update_recoverable_files();
	  }
	}
	CloseHandle(phandle);
        //errno = 0;
      }
    }
    set_status(job_status);
  }
  return job_status;
}

bool DLV::local_job::kill(char message[], const int_g mlen)
{
  HANDLE phandle = OpenProcess(PROCESS_TERMINATE, FALSE, process_id);
  if (phandle == 0)
    return tidy_files(message, mlen);
  else {
    bool ok = true;
    if (TerminateProcess(phandle, 0) == 0) {
      strncpy(message, "Failure killing job", mlen);
      ok = false;
    }
    CloseHandle(phandle);
    if (!ok)
      return ok;
  }
  // Todo - we don't know how to kill thread but it should stop normally once
  // we've killed the process
  interrupt_monitoring();
  stop_monitoring();
  process_id = 0;
  return tidy_files(message, mlen);
}

bool DLV::socket_job::kill(char message[], const int_g mlen)
{
  CloseHandle(WrPipe);
  CloseHandle(RdPipe);
  WrPipe = 0;
  return local_job::kill(message, mlen);
}

bool DLV::socket_job::run_binary(const bool, char message[], const int_g mlen)
{
  if (!file_exists(get_executable().c_str())) {
    strncpy(message, "Can't find executable", mlen);
    return false;
  }
  bool ok = true;
  string dir = get_job_dir();
  char exe[1024];
  std::strcpy(exe, get_executable().c_str());
  char buff[4096];
  std::strcpy(buff, exe);
  strcat(buff, " ");
  std::strcat(buff, get_cmdline_args().c_str());
  STARTUPINFOA si;
  PROCESS_INFORMATION pi;
  memset(&si, 0, sizeof(si));
  si.cb = sizeof(si);
  si.lpReserved = NULL;
  si.lpDesktop = NULL;
  si.lpTitle = NULL;
  si.dwFlags = STARTF_USESHOWWINDOW | STARTF_USESTDHANDLES;
  si.wShowWindow = SW_HIDE;
  // From MSDN example
  SECURITY_ATTRIBUTES sa;
  memset(&sa, 0, sizeof(sa));
  sa.nLength = sizeof(sa);
  sa.bInheritHandle = TRUE;
  sa.lpSecurityDescriptor = NULL;
  // stdout
  const int_g suggested_pipe_size = 65536;
  HANDLE out_rd, out_wr;
  if (!CreatePipe(&out_rd, &out_wr, &sa, suggested_pipe_size)) {
    strncpy(message, "Output Pipe create failed", mlen);
    return false;
  }
  si.hStdOutput = out_wr;
  int_g status;
  status = DuplicateHandle(GetCurrentProcess(), out_rd,
			   GetCurrentProcess(), &RdPipe, 0, FALSE,
			   DUPLICATE_SAME_ACCESS);
  if (!status) {
    strncpy(message, "Output Pipe dup failed", mlen);
    return false;
  }
  CloseHandle(out_rd);
  // stdin
  HANDLE in_rd, in_wr;
  if (!CreatePipe(&in_rd, &in_wr, &sa, suggested_pipe_size)) {
    strncpy(message, "Input Pipe create failed", mlen);
    return false;
  }
  si.hStdInput = in_rd;
  status = DuplicateHandle(GetCurrentProcess(), in_wr,
			   GetCurrentProcess(), &WrPipe, 0, FALSE,
			   DUPLICATE_SAME_ACCESS);
  if (!status) {
    strncpy(message, "Input Pipe dup failed", mlen);
    return false;
  }
  CloseHandle(in_wr);
  // stderr - fortran progs seem to require it and AVS -novcp doesn't use it
  //strcat(errname, "dlv_errors");
  HANDLE err_wr = CreateFileA((LPCSTR)get_error()->get_filename().c_str(),
			      GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, 0, NULL);
  if (err_wr == INVALID_HANDLE_VALUE) {
    strncpy(message, "Error file create failed", mlen);
    return false;
  }
  si.hStdError = err_wr;
  int_g flags = CREATE_DEFAULT_ERROR_MODE;
  BOOL process = CreateProcessA((LPCSTR)exe, buff, NULL, NULL, TRUE,
			       flags, NULL, (LPCSTR)dir.c_str(), &si, &pi);
  CloseHandle(err_wr);
  if (process == 0) {
    // If we get here exec failed, may need better error info
    strncpy(message, "Unable to run local command", mlen);
    set_status(run_and_failed);
    return false;
  } else {
    //set_status(still_running);
    set_process_id(pi.dwProcessId);
    CloseHandle(pi.hThread);
    CloseHandle(pi.hProcess);
    //start_monitoring();
  }
  return ok;
}

DLV::int_g DLV::socket_job::read_data(char *buff, const int_g length) const
{
  // Allow process to release this timeslice
  Sleep(0);
  DWORD size = 0;
  DWORD nbytes = length;
  int_g res = ReadFile(RdPipe, buff, nbytes, &size, NULL);
  if (res == 0)
    return -1;
  else
    return size;
}

void DLV::socket_job::read_socket(const bool check)
{
  char *data = 0;
  bool processed = false;
  if (read_socket_data(data, check) == DLV_OK) {
    if (ops.size() > 0 and !in_dlv) {
      operation *op = ops.front();
      if (op->will_process_socket()) {
	processed = true;
	if (op->process_socket_data(data, false))
	  ops.pop();
	if (strlen(data) == 0)
	  return;
      }
    }
    int_g len = strlen(data);
    int_g nlines = 0;
    int_g offset = 5;
    // Save context so that split datas across in_dlv work.
    bool dlv_check = in_dlv;
    int_g i;
    for (i = 0; i < len; i++) {
      if (data[i] == '\n') {
        if (strncmp(&data[i - offset], "#DLV", 4) == 0 or
            strncmp(&data[i - offset], "#dlv", 4) == 0)
          dlv_check = !dlv_check;
        else if (!dlv_check)
          nlines++;
      }
    }
    bool no_eol = false;
    if (data[i - 1] != '\n' and !dlv_check) {
      nlines++;
      no_eol = true;
    }
    char mystring[1024];
    if (nlines > 0)
      buffer->expand_lines(nlines);
    int_g j = 0;
    for (i = 0; i < len; i++) {
      if (data[i] == '\0')
        break;
      else if (data[i] == '\n') {
        if (strncmp(&data[i - offset], "#DLV", 4) == 0 or
            strncmp(&data[i - offset], "#dlv", 4) == 0) {
	  if (in_dlv) {
	    if (ops.size() > 0) {
	      operation *op = ops.front();
	      char *mybuff = new_local_array1(char, op_data.length() + 1);
	      strcpy(mybuff, op_data.c_str());
	      mybuff[op_data.length()] = '\0';
	      if (op->process_socket_data(mybuff, true))
		ops.pop();
	      else {
		do {
		  ops.pop();
		  if (ops.size() == 0 or strlen(mybuff) == 0)
		    break;
		  op = ops.front();
		} while (!op->process_socket_data(mybuff, true));
	      }
	      delete_local_array(mybuff);
	    }
	    //fprintf(stderr, "Process - %s\n", op_data.c_str());
	    op_data = "";
	  }
          in_dlv = !in_dlv;
          //fprintf(stderr, "#DLV\n");
	  j = 0;
        } else {
          if (in_dlv) {
	    //if (op_data.length() != 0) {
              mystring[j] = '\n';
              mystring[j + 1] = '\0';
              op_data += mystring;
              //fprintf(stderr, "%s", mystring);
	    //} else fprintf(stderr, "Discard %s\n", mystring);
          } else {
            mystring[j] = '\0';
            // Strip off some blanks.
            for (j = j - 1; j > 2; j--) {
              if (mystring[j] != ' ')
                break;
              else
                mystring[j] = '\0';
            }
            buffer->add_line(mystring, false);
	    //fprintf(stderr, "Update line - %s\n", mystring);
          }
          j = 0;
        }
      } else if (data[i] != 13) {
        mystring[j] = data[i];
        j++;
      }
    }
    mystring[j] = '\0';
    if (strlen(mystring) > 0 and !in_dlv) {
      buffer->add_line(mystring, no_eol);
      j = 0;
    }
    if (!processed and ops.size() > 0) {
      operation *op = ops.front();
      if (op->process_socket_data("", false))
	ops.pop();
    }
    delete data;
  }
}

bool DLV::socket_job::is_connected() const
{
  return (WrPipe != 0);
}

DLVreturn_type DLV::socket_job::write_connection(const char command[])
{
  DWORD l = strlen(command);
  DWORD len = 0;
  int_g res = WriteFile(WrPipe, command, l, &len, NULL);
  if (res == 0 or len != l) {
    //DLVerror("DLVjob::write_command", "Failed to write all data");
    return DLV_ERROR;
  } else
    return DLV_OK;
}

void DLV::socket_job::close_connection()
{
  CloseHandle(WrPipe);
  CloseHandle(RdPipe);
  WrPipe = 0;
  char message[256];
  (void) tidy_files(message, 256);
}

DLVreturn_type DLV::socket_job::read_socket_data(char * &dbuff,
						 const bool check) const
{
  const int_g increment = 262144;
  char *buff = new char[increment];
  int_g bptr = 0;
  int_g blen = increment;
  bool complete = false;
  while (!complete) {
    int_g length = blen - bptr;
    int_g nbytes = read_data(&buff[bptr], length);
    if (nbytes == length) {
      char *new_buff = new char[blen + increment];
      //fprintf(stderr, "Got %d bytes, increment buffer\n", nbytes);
      memcpy(new_buff, buff, blen);
      delete [] buff;
      buff = new_buff;
      blen += increment;
    } else if (nbytes == -1) {
      DWORD err = GetLastError();
      //char buffx[256];
      //FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, 0, GetLastError(),
      //	      0, buffx, 256, 0);
      //DLVerror("DLVjob::read_data", "Read error - %s", buffx);
      dbuff = 0;
      delete [] buff;
      return DLV_ERROR;
    }
    // excurv terminates command with '======='
    bptr += nbytes;
    buff[bptr] = '\0';
    if (check) {
      //fprintf(stderr, "Checking - %d\n%s\n", bptr, buff);
      complete = (buff[bptr - 10] == '=');
    } else
      complete = true;
  }
  dbuff = buff;
  //fprintf(stderr, "Read bytes - %d\n%s\n", bptr, buff);
  return DLV_OK;
}

void DLV::socket_job::read_connection(class operation *op, const bool complete)
{
  if (op != 0)
    ops.push(op);
  read_socket(complete);
}

#else

DLV::string DLV::job::make_scratch_name(const string dir, const bool exclude)
{
  std::time_t stamp;
  (void) std::time(&stamp);
  char timestr[128];
  std::strftime(timestr, 128, "%Y%m%d%H%M%S", std::localtime(&stamp));
  string name = dir + "dlv-";
  name += timestr;
  if (!exclude)
    name += DIR_SEP_CHR;
  return name;
}

bool DLV::local_job::copy_files(char message[], const int_g mlen)
{
  if (!use_directory(get_job_dir(), true, message, mlen)) {
    set_status(DLV::not_started);
    return false;
  }
  /*if (get_input()->use_fast_copy())
    snprintf(buff, 1024, "ln -s %s %s", get_input()->get_filename().c_str(),
	     get_input()->get_runname().c_str());
  else
    snprintf(buff, 1024, "cp %s %s", get_input()->get_filename().c_str(),
	     get_input()->get_runname().c_str());
  if (DLV::system(buff) != 0) {
    strncpy(message, "Unable to copy command file to scratch directory",
	    mlen);
    set_status(DLV::not_started);
    return false;
    }*/
  std::map<int, file_obj *>::const_iterator x;
  for (x = inp_begin(); x != inp_end(); ++x ) {
    try {
      if (x->second->use_fast_copy())
	symlink(x->second->get_filename().c_str(),
		x->second->get_runname().c_str());
      else
	copy_file(x->second->get_filename().c_str(),
		  x->second->get_runname().c_str());
    }
    catch (boost::filesystem::filesystem_error &) {
      strncpy(message, "Unable to copy input files to scratch directory",
	      mlen);
      set_status(DLV::not_started);
      return false;
    }
  }
  return true;
}

// Todo - OS independent boost::filesystem copy/rename?
bool DLV::local_job::recover_files(bool &err_file, bool &log_file,
				   char message[], const int_g mlen)
{
  err_file = false;
  log_file = true;
  job_status_type job_status = get_current_status();
  bool ok = true;
  if (job_status == run_and_completed or job_status == run_and_failed) {
    bool problem = false;
    if (get_error()->use_fast_copy()) {
      if (rename(get_error()->get_runname().c_str(),
		 get_error()->get_filename().c_str()) != 0) {
	problem = true;
      }
    } else {
      try {
	copy_file(get_error()->get_runname().c_str(),
		  get_error()->get_filename().c_str());
      }
      catch (boost::filesystem::filesystem_error &) {
	problem = true;
      }
    }
    if (problem) {
      ok = false;
      strncpy(message,
	      "Unable to copy sys error file from scratch directory", mlen);
      //err_file = false;
    }
    if (get_joberr() != 0) {
      if (file_exists(get_joberr()->get_runname().c_str())) {
	problem = false;
	if (get_joberr()->use_fast_copy()) {
	  if (rename(get_joberr()->get_runname().c_str(),
		     get_joberr()->get_filename().c_str()) == 0)
	    err_file = true;
	  else
	    problem = true;
	} else {
	  err_file = true;
	  try {
	    copy_file(get_joberr()->get_runname().c_str(),
		      get_joberr()->get_filename().c_str());
	  }
	  catch (boost::filesystem::filesystem_error &) {
	    problem = true;
	    err_file = false;
	  }
	}
	if (problem) {
	  ok = false;
	  strncpy(message,
		  "Unable to copy job error file from scratch directory",
		  mlen);
	}
      } //else {
      //fprintf(stderr, "No file - %s\n", get_joberr()->get_runname().c_str());
      //}
    }
    problem = false;
    if (get_output()->use_fast_copy()) {
      if (rename(get_output()->get_runname().c_str(),
		 get_output()->get_filename().c_str()) != 0)
	problem = true;
    } else {
      try {
	copy_file(get_output()->get_runname().c_str(),
		  get_output()->get_filename().c_str());
      }
      catch (boost::filesystem::filesystem_error &) {
	problem = true;
      }
    }
    if (problem) {
      ok = false;
      strncpy(message, "Unable to copy log file from scratch directory",
	      mlen);
      log_file = false;
    }
    std::map<int, file_obj *>::const_iterator x;
    for (x = out_begin(); x != out_end(); ++x ) {
      problem = false;
      if (x->second->use_fast_copy()) {
	if (rename(x->second->get_runname().c_str(),
		   x->second->get_filename().c_str()) != 0)
	  problem = true;
      } else {
	try {
	  copy_file(x->second->get_runname().c_str(),
		    x->second->get_filename().c_str());
	}
	catch (boost::filesystem::filesystem_error &) {
	  problem = true;
	}
      }
      if (problem) {
	ok = false;
	strncpy(message, "Unable to copy output files from scratch directory",
		mlen);
      }
    }
    // Tidy the files up even if copying had problems.
    if (ok)
      ok = tidy_files(message, mlen);
    else {
      char info[256];
      (void) tidy_files(info, 256);
    }
    if (ok)
      set_status(run_and_recovered);
    else
      set_status(recovered_with_errors);
    // Todo log_recovered = true;
  }
  return ok;
}

// Todo - use rename as in local_job?
bool DLV::user_job::recover_files(bool &err_file, bool &log_file,
				  char message[], const int_g mlen)
{
  err_file = false;
  log_file = true;
  job_status_type job_status = get_current_status();
  bool ok = true;
  if (job_status == run_and_completed or job_status == run_and_failed) {
    bool problem = false;
    if (get_joberr() != 0) {
      if (file_exists(get_joberr()->get_runname().c_str())) {
	problem = false;
	if (get_joberr()->use_fast_copy()) {
	  if (rename(get_joberr()->get_runname().c_str(),
		     get_joberr()->get_filename().c_str()) == 0)
	    err_file = true;
	  else
	    problem = true;
	} else {
	  err_file = true;
	  try {
	    copy_file(get_joberr()->get_runname().c_str(),
		      get_joberr()->get_filename().c_str());
	  }
	  catch (boost::filesystem::filesystem_error &) {
	    problem = true;
	    err_file = false;
	  }
	}
	if (problem) {
	  ok = false;
	  strncpy(message,
		  "Unable to copy job error file from scratch directory",
		  mlen);
	}
      }
    }
    problem = false;
    if (get_output()->use_fast_copy()) {
      if (rename(get_output()->get_runname().c_str(),
		 get_output()->get_filename().c_str()) != 0)
	problem = true;
    } else {
      try {
	copy_file(get_output()->get_runname().c_str(),
		  get_output()->get_filename().c_str());
      }
      catch (boost::filesystem::filesystem_error &) {
	problem = true;
      }
    }
    if (problem) {
      ok = false;
      strncpy(message, "Unable to copy log file from scratch directory",
	      mlen);
      log_file = false;
    }
    std::map<int, file_obj *>::const_iterator x;
    for (x = out_begin(); x != out_end(); ++x ) {
      problem = false;
      if (x->second->use_fast_copy()) {
	if (rename(x->second->get_runname().c_str(),
		   x->second->get_filename().c_str()) != 0)
	  problem = true;
      } else {
	try {
	  copy_file(x->second->get_runname().c_str(),
		    x->second->get_filename().c_str());
	}
	catch (boost::filesystem::filesystem_error &) {
	  problem = true;
	}
      }
      if (problem) {
	ok = false;
	strncpy(message, "Unable to copy output files from scratch directory",
		mlen);
      }
    }
    // Tidy the files up even if copying had problems.
    if (ok)
      ok = tidy_files(message, mlen);
    else {
      char info[256];
      (void) tidy_files(info, 256);
    }
    if (ok)
      set_status(run_and_recovered);
    else
      set_status(recovered_with_errors);
    // Todo log_recovered = true;
  }
  return ok;
}

/*
bool DLV::local_job::tidy_files(char message[], const int_g mlen)
{
  char buff[1024];
  snprintf(buff, 1024, "rm -rf %s", get_job_dir().c_str());
  if (DLV::system(buff) != 0)
    strncpy(message, "Unable to delete files from scratch directory", mlen);
  return true;
}
*/

bool DLV::local_job::run_binary(const bool foreground,
				char message[], const int_g mlen)
{
  if (!file_exists(get_executable().c_str())) {
    strncpy(message, "Can't find executable", mlen);
    return false;
  }
  bool ok = true;
  pid_t process = fork();
  if (process == 0) {
    string dir = get_job_dir();
    char buff[4096];
    // Todo - is 32 enough?
    char *args[32];
    args[0] = &buff[0];
    std::strcpy(buff, get_executable().c_str());
    int_g p = strlen(args[0]) + 1;
    // split into space separated parts
    char argbuff[4096];
    std::strcpy(argbuff, get_cmdline_args().c_str());
    int_g i = 1;
    if (std::strlen(argbuff) > 0) {
      char delim[] = " ";
      char *token = std::strtok(argbuff, delim);
      while (token != 0) {
	args[i] = &buff[p];
	std::strcpy(args[i], token);
	p += strlen(args[i]) + 1;
	i++;
	token = std::strtok(0, delim);
      }
    }
    args[i] = 0;
    int_g inpfd = -1;
    if (need_stdin()) {
      inpfd = open(get_stdin()->get_runname().c_str(), O_RDONLY);
      if (inpfd == -1) {
	fprintf(stderr, "Open of input file failed - %s\n",
		std::strerror(errno));
	exit(1);
      }
    }
    int_g outfd = open(get_output()->get_runname().c_str(),
		     O_WRONLY | O_CREAT, 0664);
    if (outfd == -1) {
      fprintf(stderr, "Open of output file failed - %s %s\n",
	      std::strerror(errno), get_output()->get_runname().c_str());
      exit(1);
    }
    int_g errfd = open(get_error()->get_runname().c_str(),
		     O_WRONLY | O_CREAT, 0664);
    if (errfd == -1) {
      fprintf(stderr, "Open of error file failed - %s\n",
	      std::strerror(errno));
      exit(1);
    }
    // change dir after file open so we don't need base names
    if (!change_directory(dir.c_str())) {
      fprintf(stderr, "Failed to change to scratch directory - %s\n",
	      std::strerror(errno));
      exit(1);
    }
    close(2);
    close(1);
    if (need_stdin()) {
      close(0);
      if (dup2(inpfd, 0) != 0) {
	fprintf(stderr, "Failed to attach stdin to file - %s\n",
		std::strerror(errno));
	exit(1);
      }
    }
    if (dup2(outfd, 1) != 1 or dup2(errfd, 2) != 2) {
      // will cerr work here?
      fprintf(stderr, "Failed to attach stdout/err to files - %s\n",
	      std::strerror(errno));
      exit(1);
    }
    execvp(buff, args);
    // If we get here exec failed, may need better error info
    fprintf(stderr, "Exec of %s failed - %s\n", buff, std::strerror(errno));
    exit(1);
  } else {
    if (process == -1) {
      ok = false;
      strncpy(message, "Unable to run local command", mlen);
      set_status(run_and_failed);
    } else {
      if (foreground) {
	int_g jstat;
        waitpid(process, &jstat, 0);
        if (!(WIFEXITED(jstat))) {
          ok = false;
          set_status(run_and_failed);
	} else
	  set_status(run_and_completed);
      } else {
	//set_status(still_running);
	pid = process;
	start_monitoring();
      }
    }
  }
  return ok;
}

DLV::job_status_type DLV::local_job::get_status()
{
  job_status_type job_status = get_current_status();
  if (job_status == initialising_job or job_status == still_running or
      job_status == running_and_updated) {
    if (pid == 0)
      job_status = not_started;
    else if (pid == -1) {
      job_status = run_and_completed;
      //DLVset_normal_cursor();
    } else {
      if (::kill(pid, 0) == -1) {
        if (errno == ESRCH) {
          pid = -1;
          job_status = run_and_completed;
          //DLVset_normal_cursor();
        }
        errno = 0;
      } else {
        job_status = still_running;
        //update_recoverable_files();
      }
    }
    set_status(job_status);
  }
  return job_status;
}

bool DLV::local_job::kill(char message[], const int_g mlen)
{
  if (pid == -1)
    return tidy_files(message, mlen);
#ifdef __sgi
  // Todo - I don't understand why sending any signal (not just SIGINT) to
  // the child process seems to result in a SIGINT to DLV on the SGI.
  // This is not the way to solve the problem though.
  static bool signal_replaced = false;
  if (!signal_replaced) {
    void (*sig)(int) = sigset(SIGINT, SIG_IGN);
    signal_replaced = true;
  }
#endif // sgi
  // Bourne shell trap is set intercept SIGINT
  int_g stat = ::kill(pid, SIGINT);
  if (stat != -1) {
    // Wait a bit and then see if its still running
    ::sleep(1);
    if (::kill(pid, 0) != -1) {
      //fprintf(stderr, "Kill signal required\n");
      // Last try - use SIGKILL
      stat = ::kill(pid, SIGKILL);
    }
  }
  if (stat == -1)
    stat = errno;
  else
    stat = ESRCH;
  errno = 0;
  if (stat != ESRCH) {
    strncpy(message, "Failure killing job", mlen);
    return false;
  }
  // Todo - we don't know how to kill thread but it should stop normally once
  // we've killed the process
  interrupt_monitoring();
  stop_monitoring();
  pid = -1;
  return tidy_files(message, mlen);
}

bool DLV::socket_job::kill(char message[], const int_g mlen)
{
#ifdef ENABLE_DLV_GRAPHICS
  detach_event(socket1, &(read_socket), this);
#endif // ENABLE_DLV_GRAPHICS
  close(socket2);
  close(socket1);
  return local_job::kill(message, mlen);
}

/*
bool DLV::socket_job::copy_files(char message[], const int_g mlen)
{
  char buff[1024];
  // Todo - will have problems if we have to write an input file
  std::map<int, file_obj *>::const_iterator x;
  for (x = inp_begin(); x != inp_end(); ++x ) {
    if (x->second->use_fast_copy())
      snprintf(buff, 1024, "ln -s %s %s", x->second->get_filename().c_str(),
	       x->second->get_runname().c_str());
    else
      snprintf(buff, 1024, "cp %s %s", x->second->get_filename().c_str(),
	       x->second->get_runname().c_str());
    if (DLV::system(buff) != 0) {
      strncpy(message, "Unable to copy input files to scratch directory",
	      mlen);
      set_status(DLV::not_started);
      return false;
    }
  }
  return true;
}
*/

bool DLV::socket_job::run_binary(const bool, char message[], const int_g mlen)
{
  if (!file_exists(get_executable().c_str())) {
    strncpy(message, "Can't find executable", mlen);
    return false;
  }
  // Create socket comms pair, maybe later use one for stderr?
  int sv[2];
  if (socketpair(AF_UNIX, SOCK_STREAM, 0, sv) != 0) {
    strncpy(message, "Unable to create socket", mlen);
    return false;
  }
  bool ok = true;
  socket1 = sv[0];
  socket2 = sv[1];
  pid_t process = fork();
  if (process == 0) {
    string dir = get_job_dir();
    char buff[4096];
    // Todo - is 32 enough?
    char *args[32];
    args[0] = &buff[0];
    std::strcpy(buff, get_executable().c_str());
    int_g p = strlen(args[0]) + 1;
    // split into space separated parts
    char argbuff[4096];
    std::strcpy(argbuff, get_cmdline_args().c_str());
    int_g i = 1;
    if (std::strlen(argbuff) > 0) {
      char delim[] = " ";
      char *token = std::strtok(argbuff, delim);
      while (token != 0) {
	args[i] = &buff[p];
	std::strcpy(args[i], token);
	p += strlen(args[i]) + 1;
	i++;
      }
    }
    args[i] = 0;
    close(2);
    close(1);
    close(0);
    if (dup(socket2) != 0 || dup(socket2) != 1 || dup(socket2) != 2) {
      std::cerr << "Failed to attach stdin/stdout to socket\n";
      exit(1);
    }
    // change dir after file open so we don't need base names
    if (!change_directory(dir.c_str())) {
      fprintf(stderr, "Failed to change to scratch directory - %s\n",
	      std::strerror(errno));
      exit(1);
    }
    execvp(buff, args);
    // If we get here exec failed, may need better error info
    fprintf(stderr, "Exec of %s failed - %s\n", buff, std::strerror(errno));
    exit(1);
  } else {
    if (process == -1) {
      ok = false;
      strncpy(message, "Unable to run local command", mlen);
      set_status(run_and_failed);
    } else {
#ifdef ENABLE_DLV_GRAPHICS
      // Set AVS/Express callback for socket comms
      attach_event(socket1, &(read_socket), this);
#endif // ENABLE_DLV_GRAPHICS
      set_status(still_running);
      set_process_id(process);
    }
  }
  return ok;
}

DLV::int_g DLV::socket_job::read_data(char *buff, const int_g length) const
{
  return read(socket1, buff, length);
}

void DLV::socket_job::read_socket()
{
  char *data = 0;
  bool processed = false;
  if (read_socket_data(data, false) == DLV_OK) {
    if (ops.size() > 0 and !in_dlv) {
      operation *op = ops.front();
      if (op->will_process_socket()) {
	processed = true;
	if (op->process_socket_data(data, false))
	  ops.pop();
	if (strlen(data) == 0)
	  return;
      }
    }
    int_g len = strlen(data);
    int_g nlines = 0;
    int_g offset = 4;
    // Save context so that split datas across in_dlv work.
    bool dlv_check = in_dlv;
    int_g i;
    for (i = 0; i < len; i++) {
      if (data[i] == '\n') {
        if (strncmp(&data[i - offset], "#DLV", 4) == 0 or
            strncmp(&data[i - offset], "#dlv", 4) == 0)
          dlv_check = !dlv_check;
        else if (!dlv_check)
          nlines++;
      }
    }
    bool no_eol = false;
    if (data[i - 1] != '\n' and !dlv_check) {
      nlines++;
      no_eol = true;
    }
    char mystring[1024];
#ifdef ENABLE_DLV_GRAPHICS
    if (nlines > 0)
      buffer->expand_lines(nlines);
#endif // ENABLE_DLV_GRAPHICS
    int_g j = 0;
    for (i = 0; i < len; i++) {
      if (data[i] == '\0')
        break;
      else if (data[i] == '\n') {
        if (strncmp(&data[i - offset], "#DLV", 4) == 0 or
            strncmp(&data[i - offset], "#dlv", 4) == 0) {
	  if (in_dlv) {
	    if (ops.size() > 0) {
	      operation *op = ops.front();
	      char *mybuff = new_local_array1(char, op_data.length() + 1);
	      strcpy(mybuff, op_data.c_str());
	      mybuff[op_data.length()] = '\0';
	      if (op->process_socket_data(mybuff, true))
		ops.pop();
	      else {
		do {
		  ops.pop();
		  if (ops.size() == 0 or strlen(mybuff) == 0)
		    break;
		  op = ops.front();
		} while (!op->process_socket_data(mybuff, true));
	      }
	      delete_local_array(mybuff);
	    }
	    //fprintf(stderr, "Process - %s\n", op_data.c_str());
	    op_data = "";
	  }
          in_dlv = !in_dlv;
          //fprintf(stderr, "#DLV\n");
	  j = 0;
        } else {
          if (in_dlv) {
	    //if (op_data.length() != 0) {
              mystring[j] = '\n';
              mystring[j + 1] = '\0';
              op_data += mystring;
              //fprintf(stderr, "%s", mystring);
	    //} else fprintf(stderr, "Discard %s\n", mystring);
          } else {
            mystring[j] = '\0';
            // Strip off some blanks.
            for (j = j - 1; j > 2; j--) {
              if (mystring[j] != ' ')
                break;
              else
                mystring[j] = '\0';
            }
#ifdef ENABLE_DLV_GRAPHICS
            buffer->add_line(mystring, false);
#endif // ENABLE_DLV_GRAPHICS
	    //fprintf(stderr, "Update line - %s\n", mystring);
          }
          j = 0;
        }
      } else if (data[i] != 13) {
        mystring[j] = data[i];
        j++;
      }
    }
    mystring[j] = '\0';
    if (strlen(mystring) > 0 and !in_dlv) {
#ifdef ENABLE_DLV_GRAPHICS
      buffer->add_line(mystring, no_eol);
#endif // ENABLE_DLV_GRAPHICS
      j = 0;
    }
    if (!processed and ops.size() > 0) {
      operation *op = ops.front();
      char mylocaldata[8];
      mylocaldata[0] = '\0';
      if (op->process_socket_data(mylocaldata, false))
	ops.pop();
    }
    delete data;
  }
}

bool DLV::socket_job::is_connected() const
{
  return (socket1 != 0);
}

DLVreturn_type DLV::socket_job::write_connection(const char command[])
{
  int_g l = strlen(command);
  int_g res = write(socket1, command, l);
  //fprintf(stderr, "comm - %s\n", command);
  if (res != l) {
    //DLVerror("DLVjob::write_command", "Failed to write all data");
    return DLV_ERROR;
  } else
    return DLV_OK;
}

void DLV::socket_job::close_connection()
{
#ifdef ENABLE_DLV_GRAPHICS
  detach_event(socket1, &(read_socket), this);
#endif // ENABLE_DLV_GRAPHICS
  //Todoif (status() == still_running)
  //  return local_job::kill();
  //else {
  close(socket2);
  close(socket1);
  char message[256];
  (void) tidy_files(message, 256);
  //}
}

DLVreturn_type DLV::socket_job::read_socket_data(char * &dbuff,
						 const bool) const
{
  const int_g increment = 262144;
  char *buff = new char[increment];
  int_g length = increment;
  int_g nbytes = 0;
  int_g bptr = 0;
  int_g blen = 0;
  while ((nbytes = read_data(&buff[bptr], length)) == length) {
    char *new_buff = new char[blen + increment];
    //fprintf(stderr, "Got %d bytes, increment buffer\n", nbytes);
    memcpy(new_buff, buff, blen);
    delete [] buff;
    buff = new_buff;
    bptr += length;
    blen += increment;
    length = increment;
  }
  //fprintf(stderr, "Got %d bytes\n", nbytes);
  if (nbytes == -1) {
    dbuff = 0;
    //DLVerror("DLVjob::read_data", "Read error");
    return DLV_ERROR;
  } else {
    bptr += nbytes;
    buff[bptr] = '\0';
    dbuff = buff;
  }
  //fprintf(stderr, "Read bytes - %d\n%s\n", bptr, buff);
  return DLV_OK;
}

void DLV::socket_job::read_connection(class operation *op, const bool)
{
  if (op != 0)
    ops.push(op);
}

void DLV::socket_job::read_socket(char *job)
{
  socket_job *j = reinterpret_cast<socket_job *>(job);
  //fprintf(stderr, "Socket woke up\n");
  j->read_socket();
}

#endif // WIN32

DLV::job_status_type DLV::socket_job::get_status()
{
  return still_running;
}

bool DLV::socket_job::recover_files(bool &err_file, bool &log_file,
				    char message[], const int_g mlen)
{
  return true;
}

bool DLV::socket_job::tidy_files(char message[], const int_g mlen)
{
  return true;
}

bool DLV::job::is_connected() const
{
  // Todo
  return false;
}

DLVreturn_type DLV::job::write_connection(const char command[])
{
  return DLV_ERROR;
}

void DLV::job::read_connection(class operation *, const bool)
{
  // error
}

void DLV::job::close_connection()
{
  // dummy
}

#ifdef ENABLE_DLV_JOB_THREADS

void DLV::job::monitor_job()
{
}

void DLV::local_job::monitor_job()
{
  start_monitoring();
}

void DLV::user_job::monitor_job()
{
  //start_monitoring();
}

void DLV::socket_job::monitor_job()
{
  start_monitoring();
}

DLV::job_watcher::job_watcher(job *j) : parent(j)
{
}

void DLV::job_watcher::operator() ()
{
  parent->watch();
}

boost::mutex::scoped_lock *DLV::job::acquire_status_lock()
{
  return new boost::mutex::scoped_lock(mutex);
}

// Had to do it this way since *this doesn't work with abstract virtuals
void DLV::job::start_monitoring()
{
  thread = new boost::thread(job_watcher(this));
}

void DLV::job::interrupt_monitoring()
{
  if (thread != 0)
    thread->interrupt();
}

void DLV::job::stop_monitoring()
{
  // I've set the stop to be after its killed, so join may work and I may
  // not need to kill thread - Todo?
  if (thread != 0) {
    thread->join();
    delete thread;
    thread = 0;
  }
}

void DLV::job::watch()
{
  //const int_g time_step = 5;
  job_status_type state = get_current_status();
  boost::chrono::duration<long> timer = boost::chrono::seconds(5);
  while (state == initialising_job or state == still_running or
	 state == running_and_updated) {
    try {
      boost::mutex::scoped_lock *lock = DLV::job::acquire_status_lock();
      job_status_type new_state = get_status();
      if (new_state != state)
	change_status();
      delete lock;
      state = new_state;
      boost::this_thread::sleep_for(timer);
    }
    catch (boost::thread_interrupted) {
      return;
    }
  }
  //stop_monitoring();
}

#else

void DLV::job::start_monitoring()
{
}

void DLV::job::interrupt_monitoring()
{
}

void DLV::job::stop_monitoring()
{
}

#endif // ENABLE_DLV_JOB_THREADS

DLV::string DLV::job::get_status_info(const job_status_type s)
{
  switch (s) {
  case run_and_completed:
    return "completed";
    break;
  case run_and_failed:
    return "failed";
    break;
  case run_and_recovered:
    return "recovered";
    break;
  case still_running:
    return "still running";
    break;
  case running_and_updated:
    return "still running - files have been updated";
    break;
  case not_started:
    return "didn't start";
    break;
  case recovered_with_errors:
    return "recovered with errors";
    break;
  case in_queue:
    return "queued";
    break;
  case being_submitted:
    return "Globus is submitting job";
    break;
  case suspended:
    return "suspended";
  case initialising_job:
    return "starting";
    break;
  case nonDLV_job:
    return "external job";
    break;
  default:
    return "status unknown";
    break;
  }
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::local_job *t,
				    const unsigned int file_version)
    {
      // Todo - this will leak (small amounts) of memory
      std::map<int, DLV::file_obj *> *imap = new std::map<int, DLV::file_obj *>;
      std::map<int, DLV::file_obj *> *omap = new std::map<int, DLV::file_obj *>;
      ::new(t)DLV::local_job("recover", "", false, 0, 0, 0,
			     0, *imap, *omap);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::user_job *t,
				    const unsigned int file_version)
    {
      // Todo - this will leak (small amounts) of memory
      std::map<int, DLV::file_obj *> *imap = new std::map<int, DLV::file_obj *>;
      std::map<int, DLV::file_obj *> *omap = new std::map<int, DLV::file_obj *>;
      ::new(t)DLV::user_job("recover", "", false, 0, 0, 0,
			    0, *imap, *omap);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::socket_job *t,
				    const unsigned int file_version)
    {
      std::map<int, DLV::file_obj *> *imap = new std::map<int, DLV::file_obj *>;
      std::map<int, DLV::file_obj *> *omap = new std::map<int, DLV::file_obj *>;
      ::new(t)DLV::socket_job("recover", 0, *imap, *omap, 0);
    }

  }
}

template <class Archive>
void DLV::job::serialize(Archive &ar, const unsigned int version)
{
  ar & executable;
  ar & cmdline_args;
  ar & scratch_dir;
  ar & status;
  ar & stdoutput;
  ar & stderror;
  ar & joberror;
  ar & use_stdin;
  ar & sinput;
  ar & inp_files;
  ar & out_files;
}

template <class Archive>
void DLV::local_job::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<job>(*this);
#  ifdef WIN32
  ar & process_id;
#  else
  ar & pid;
#  endif // WIN32
}

template <class Archive>
void DLV::user_job::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<local_job>(*this);
}

template <class Archive>
void DLV::socket_job::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<local_job>(*this);
  // Don't save anything else since its closed.
}

//BOOST_CLASS_EXPORT_GUID(DLV::job, "DLV::job")
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::local_job)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::user_job)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::socket_job)

DLV_SUPPRESS_TEMPLATES(DLV::file_obj)

DLV_NORMAL_EXPLICIT(DLV::local_job)
DLV_NORMAL_EXPLICIT(DLV::user_job)
DLV_NORMAL_EXPLICIT(DLV::socket_job)

#endif // DLV_USES_SERIALIZE
