
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/calculations.hxx"
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "utils.hxx"
#include "operation.hxx"
#include "calculation.hxx"
#include "file.hxx"
#include "job.hxx"
#include "job_setup.hxx"
#include "project.hxx"

bool DLV::calculate_data::is_calc() const
{
  return true;
}

bool DLV::calculate_data::job_failed() const
{
  return job_had_errors;
}

void DLV::batch_calc::set_failure()
{
  set_job_failed();
}

bool DLV::batch_calc::execute(const string local_path,
			      const string serial_exec,
			      const string parallel_exec,
			      const string args,
			      const class job_setup_data &job_data,
			      const bool allow_parallel_job,
			      const bool external_job, const string extern_dir,
			      char message[], const int_g mlen,
			      const bool foreground)
{
  bool allow_parallel = allow_parallel_job and !foreground;
  job *new_job = job::create(local_path, serial_exec, parallel_exec, args,
			     job_data, uses_stdin, stdin_index, stdoutput,
			     stderror, get_error_file(), get_input_files(),
			     get_output_files(), allow_parallel,
			     external_job, extern_dir);
  if (new_job != 0) {
    add_job(new_job, job_data.get_host(), external_job);
    if (new_job->execute(foreground, message, mlen))
      return (recover_job(message, mlen) == DLV_OK);
  }
  return false;
}

void DLV::batch_calc::add_job(job *j, const string hostname,
			      const bool external_job)
{
  calc = j;
  job_index = project::get_job_number();
  project::inc_job_number();
  // Add stuff to project job list
  string name = get_name();
  if (!external_job) {
    name += " on host ";
    name += hostname;
  }
#ifdef ENABLE_DLV_GRAPHICS
  char info[32];
  snprintf(info, 32, " (%1d)", job_index);
  name += info;
  add_to_job_list(name, job::get_status_info(j->get_current_status()),
		  job_index);
  show_job_panel();
#endif // ENABLE_DLV_GRAPHICS
}

DLVreturn_type DLV::batch_calc::recover_job(char message[], const int_g mlen)
{
  message[0] = '\0';
  if (calc->get_current_status() == run_and_completed) {
    bool err_file = true;
    bool log_file = true;
    message[0] = '\0';
    bool ok = calc->recover_files(err_file, log_file, message, mlen);
#ifdef ENABLE_DLV_GRAPHICS
    if (ok)
      update_job_status(job::get_status_info(calc->get_current_status()),
			get_job_number());
#endif // ENABLE_DLV_GRAPHICS
    ok = ok and recover(ok, log_file, message, mlen);
    if (err_file or (get_error_file() == 0 and stderror != 0)) {
      if (no_errors(message, mlen) and ok)
	return DLV_OK;
      else {
	fixup_job();
	calc->set_failure();
      }
    } else {
      if (ok)
	return DLV_OK;
      else
	calc->set_failure();
    }
    set_failure();
  }
  return DLV_ERROR;
}

void DLV::batch_calc::fixup_job()
{
  // Dummy
}

DLVreturn_type DLV::batch_calc::remove_job(char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  // Todo - clean files?
  if (!calc->tidy_files(message, mlen))
    ok = DLV_ERROR;
#ifdef ENABLE_DLV_GRAPHICS
  remove_from_job_list(job_index);
#endif // ENABLE_DLV_GRAPHICS
  delete calc;
  calc = 0;
  return ok;
}

DLVreturn_type DLV::batch_calc::kill_job(char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  if (!calc->kill(message, mlen))
    ok = DLV_ERROR;
#ifdef ENABLE_DLV_GRAPHICS
  remove_from_job_list(job_index);
#endif // ENABLE_DLV_GRAPHICS
  delete calc;
  calc = 0;
  return ok;
}

DLV::int_g DLV::batch_calc::get_job_number() const
{
  return job_index;
}

DLV::string DLV::batch_calc::get_job_id() const
{
  char buff[8];
  snprintf(buff, 8, "job%04d", job_index);
  return buff;
}

DLV::job *DLV::batch_calc::get_job() const
{
  return calc;
}

DLV::job *DLV::socket_calc::get_job() const
{
  return connection;
}

bool DLV::socket_calc::inherit_connections()
{
  operation *op = get_parent();
  connection = op->get_job();
  return (connection != 0);
}

void DLV::calculate_data::add_standard_data_objects()
{
  // Do nothing
}

DLV::string DLV::calculate_data::make_dir_name() const
{
  // subdir from project dir (if any)
  string dir = get_sub_project_dir();
  string name;
  if (dir.length() > 0)
    name = dir + DIR_SEP_CHR;
  int_g num = project::get_job_number();
  char label[16];
  snprintf(label, 16, "%08d", num);
  name += label;
  name += '-';
  name += get_user_id();
  name += DIR_SEP_CHR;
  return name;
}

DLV::string DLV::calculate_data::make_name(const string tag,
					   const string suffix) const
{
  string name;
  string model_name = get_model_name();
  if (model_name.length() > 0) {
    // make name safe
    for (nat_g i = 0; i < model_name.length(); i++) {
      if (model_name[i] == ' ')
	name += '_';
      else if (isalnum(model_name[i]))
	name += model_name[i];
      else if (model_name[i] == '(')
	break;
    }
    if (name[name.length() - 1] != '_')
      name += '_';
  }
  name += tag;
  name += '.';
  name += suffix;
  return name;
}

bool DLV::calculate_data::make_directory(char message[], const int_g mlen) const
{
  string name = make_dir_name();
  bool ok = DLV::make_directory(name.c_str(), false);
  if (!ok)
    strncpy(message, "Failed to make job directory", mlen);
  return ok;
}

void DLV::calculate_data::attach_input(const int_g index, file_obj *file)
{
  inp_files[index] = file;
}

void DLV::batch_calc::add_command_file(const int_g index, const string tag,
				       const string suffix,
				       const bool is_local, const bool link)
{
  string dir = make_dir_name();
  string local_name = make_name(tag, suffix);
  file_obj *file = file_obj::create(dir, local_name, is_local, link);
  attach_input(index, file);
  uses_stdin = true;
  stdin_index = index;
}

void DLV::calculate_data::add_data_file(const int_g index,
					const string filename,
					const bool is_local, const bool link)
{
  string dir = get_directory_path(filename);
  if (dir.length() > 0)
    dir += DIR_SEP_CHR;
  string base = get_file_name(filename.c_str());
  file_obj *file = file_obj::create(dir, base, base, is_local, link);
  inp_files[index] = file;
}

void DLV::calculate_data::add_data_file(const int_g index,
					const string filename,
					const string name, const bool is_local,
					const bool link)
{
  string dir = get_directory_path(filename);
  if (dir.length() > 0)
    dir += DIR_SEP_CHR;
  string base = get_file_name(filename.c_str());
  file_obj *file = file_obj::create(dir, base, name, is_local, link);
  inp_files[index] = file;
}

void DLV::calculate_data::add_input_file(const int_g index, const string tag,
					 const string suffix,
					 const bool is_local, const bool link)
{
  // Todo - be smarter about index and conflicts
  string dir = make_dir_name();
  string local_name = make_name(tag, suffix);
  file_obj *file = file_obj::create(dir, local_name, local_name,
				    is_local, link);
  inp_files[index] = file;
}

void DLV::calculate_data::add_input_file(const int_g index, const string tag,
					 const string suffix,
					 const string name,
					 const bool is_local, const bool link)
{
  string dir = make_dir_name();
  string local_name = make_name(tag, suffix);
  file_obj *file = file_obj::create(dir, local_name, name, is_local, link);
  inp_files[index] = file;
}

void DLV::batch_calc::add_log_file(const string tag, const string suffix,
				   const bool is_local, const bool link)
{
  string dir = make_dir_name();
  string local_name = make_name(tag, suffix);
  stdoutput = file_obj::create(dir, local_name, is_local, link);
}

void DLV::batch_calc::add_log_file(const string tag, const string suffix,
				   const string name, const bool is_local,
				   const bool link)
{
  string dir = make_dir_name();
  string local_name = make_name(tag, suffix);
  stdoutput = file_obj::create(dir, local_name, name, is_local, link);
}

void DLV::batch_calc::add_sys_error_file(const string tag, const string suffix,
					 const bool is_local, const bool link)
{
  string dir = make_dir_name();
  string local_name = make_name(tag, suffix);
  stderror = file_obj::create(dir, local_name, is_local, link);
}

void DLV::calculate_data::add_job_error_file(const string tag,
					     const string suffix,
					     const string name,
					     const bool is_local,
					     const bool link)
{
  string dir = make_dir_name();
  string local_name = make_name(tag, suffix);
  errors = file_obj::create(dir, local_name, name, is_local, link);
}

void DLV::calculate_data::add_output_file(const int_g index, const string tag,
					  const string suffix,
					  const bool is_local, const bool link)
{
  string dir = make_dir_name();
  string local_name = make_name(tag, suffix);
  file_obj *file = file_obj::create(dir, local_name, local_name,
				    is_local, link);
  out_files[index] = file;
}

void DLV::calculate_data::add_output_file(const int_g index, const string tag,
					  const string suffix,
					  const string name,
					  const bool is_local, const bool link)
{
  string dir = make_dir_name();
  string local_name = make_name(tag, suffix);
  file_obj *file = file_obj::create(dir, local_name, name, is_local, link);
  out_files[index] = file;
}

DLV::string DLV::calculate_data::get_infile_name(const int_g index) const
{
  return inp_files.find(index)->second->get_filename();
}

DLV::string DLV::calculate_data::get_outfile_name(const int_g index) const
{
  return out_files.find(index)->second->get_filename();
}

DLV::string DLV::batch_calc::get_sys_error_file() const
{
  return stderror->get_filename();
}

DLV::string DLV::calculate_data::get_job_error_file() const
{
  return errors->get_filename();
}

DLV::string DLV::batch_calc::get_log_filename() const
{
  return stdoutput->get_filename();
}

bool DLV::socket_calc::execute(const string binary, class text_buffer *data,
			       char message[], const int_g mlen)
{
  string dir = make_dir_name();
  project::inc_job_number();
  DLV::job *job = job::connect(binary, get_error_file(), get_input_files(),
			       get_output_files(), dir, data, message, mlen);
  if (job != 0) {
    if (job->execute(false, message, mlen)) {
      add_connection(job);
      return true;
    }
  }
  return false;
}

DLVreturn_type DLV::socket_calc::write_connection(const char command[])
{
  return connection->write_connection(command);
}

void DLV::socket_calc::read_connection(operation *op, const bool complete)
{
  connection->read_connection(op, complete);
}

void DLV::socket_calc::close_connection()
{
  connection->close_connection();
}

//Instantiate these here to try and avoid them being part of libcrystal etc
#ifdef DLV_USES_SERIALIZE

template <class Archive>
void DLV::calculate_data::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<operation>(*this);
  ar & errors;
  ar & inp_files;
  ar & out_files;
}

template <class Archive>
void DLV::batch_calc::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<calculate_data>(*this);
  ar & stdoutput;
  ar & stderror;
  ar & uses_stdin;
  ar & stdin_index;
  ar & calc;
  ar & job_index;
#ifdef ENABLE_DLV_JOB_THREADS
  if (calc != 0) {
    // Todo - in_queue?
    if (calc->get_status() == still_running)
      calc->monitor_job();
    // a lot of this is like add_job
    string name = get_name();
    if (!calc->is_external_job())
      name += " on host localhost";
    char info[32];
    snprintf(info, 32, " (%1d)", job_index);
    name += info;
#ifdef ENABLE_DLV_GRAPHICS
    add_to_job_list(name, job::get_status_info(calc->get_current_status()),
		    job_index);
    show_job_panel();
#endif // ENABLE_DLV_GRAPHICS
  }
#endif // ENABLE_DLV_JOB_THREADS
}

template <class Archive>
void DLV::batch_calc::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<calculate_data>(*this);
  ar & stdoutput;
  ar & stderror;
  ar & uses_stdin;
  ar & stdin_index;
  ar & calc;
  ar & job_index;
}

template <class Archive>
void DLV::socket_calc::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<calculate_data>(*this);
  // Skip connections I think.
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::calculate_data)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::batch_calc)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::socket_calc)

DLV_SUPPRESS_TEMPLATES(DLV::file_obj)
DLV_SUPPRESS_TEMPLATES(DLV::operation)

DLV_NORMAL_EXPLICIT(DLV::calculate_data)
DLV_SPLIT_EXPLICIT(DLV::batch_calc)
DLV_EXPORT_EXPLICIT(DLV::batch_calc)
DLV_EXPORT_EXPLICIT(DLV::socket_calc)

#endif // DLV_USES_SERIALIZE
