
#ifndef DLV_VOLUME_DATA
#define DLV_VOLUME_DATA

namespace DLV {

  class box : public data_object {
  public:
    void get_points(real_g o[3], real_g a[3], real_g b[3], real_g c[3]) const;

    // public for serialization
    box(const char label[]);

  protected:
    bool is_displayable() const;
    bool is_editable() const;
    bool is_edited() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;
    string get_name() const;

    bool is_box() const;

#ifdef ENABLE_DLV_GRAPHICS
    bool hide_render(const int_g object, const int_g visible,
		     const render_parent *parent, class model *structure,
		     char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
    DLVreturn_type use_view_editor(const render_parent *parent,
				   const bool v, char message[],
				   const int_g mlen);
    DLVreturn_type update3D(const int_g method, const int_g h, const int_g k,
			    const int_g l, const real_g x, const real_g y,
			    const real_g z, const int_g object,
			    const bool conv, const bool frac,
			    const bool p_obj, class model *const m,
			    char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

    void unload_data();

  private:
    string name;
    real_g origin[3];
    real_g pointa[3];
    real_g pointb[3];
    real_g pointc[3];

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class real_space_box : public box {
  public:
    real_space_box(const char label[]);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class k_space_box : public box {
  public:
    k_space_box(const char label[]);
    bool is_kspace() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class sphere : public data_object {
  public:
    // public for serialization
    sphere(const char label[]);

  protected:
    bool is_displayable() const;
    bool is_editable() const;
    bool is_edited() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;
    string get_name() const;

    //bool is_box() const;

#ifdef ENABLE_DLV_GRAPHICS
    bool hide_render(const int_g object, const int_g visible,
		     const render_parent *parent, class model *structure,
		     char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
    DLVreturn_type update3D(const int_g method, const real_g r,
			    class model *const m, char message[],
			    const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

    void unload_data();

  private:
    string name;
    real_g origin[3];
    real_g radius;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class real_space_sphere : public sphere {
  public:
    real_space_sphere(const char label[]);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class wulff_plot : public data_object {
  public:
    wulff_plot(const char label[]);
    bool is_wulff() const;
    const std::list<wulff_data> &get_wulff_data() const;

  protected:
    bool is_displayable() const;
    bool is_editable() const;
    bool is_edited() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;
    string get_name() const;

#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
    DLVreturn_type update_wulff(const int_g h, const int_g k, const int_g l,
				const real_g e, const real_g r, const real_g g,
				const real_g b,const bool conv, model *const m,
				char message[], const int_g mlen);
    DLVreturn_type update_wulff(const char filename[], model *const m,
				char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

    void unload_data();

  private:
    string name;
    std::list<wulff_data> planes;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class volume_data : public reloadable_data {
  public:
    virtual volume_data *create(const char code[], const string src,
				operation *op) const = 0;
    ~volume_data();
    void add_data(real_g *array, const int_g vec, const char label[],
		  const bool copy_data = true);
    virtual void add_data(real_g *r, real_g *theta, const char label[],
			  const bool copy_data = true);
    virtual void set_k_point(const int_g k1, const int_g k2, const int_g k3,
			     const int_g s1, const int_g s2, const int_g s3);

    void unload_data();

    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

    int_g get_ndata_sets() const;
    int_g get_data_size() const;
    int_l get_data_length() const;

    bool is_periodic() const;

#ifdef ENABLE_DLV_GRAPHICS
    class drawable_obj *create_drawable(const render_parent *parent,
					char message[], const int_g mlen);
    bool hide_render(const int_g object, const int_g visible,
		     const render_parent *parent, class model *structure,
		     char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
    data_object *edit(const render_parent *parent,
		      class model *structure, const int_g component,
		      const int_g method, const int_g index, char message[],
		      const int_g mlen);
    void match_data_math(const data_object *obj, const int_g selection,
			 const bool all, const int_g cmpt, int_g &nmatch,
			 int_g &n) const;
    void list_data_math(const data_object *obj, const bool all,
			const int_g cmpt, string *names, int_g &n) const;
#endif // ENABLE_DLV_GRAPHICS

    // Need access for extension object
    void get_origin(real_g o[]) const;
    void get_astep(real_g o[]) const;
    void get_bstep(real_g o[]) const;
    void get_cstep(real_g o[]) const;
    int_g get_nx() const;
    int_g get_ny() const;
    int_g get_nz() const;
    real_g **get_data() const;
    int_g *get_vector_data() const;

  protected:
    volume_data(const char code[], const string src, operation *p,
		const int_g na, const int_g nb, const int_g nc,
		const real_g o[3], const real_g as[3], const real_g bs[3],
		const real_g cs[3]);

    string *get_labels() const;
#ifdef ENABLE_DLV_GRAPHICS
    void reset_data_sets();
#endif // ENABLE_DLV_GRAPHICS

  private:
    int_g nx;
    int_g ny;
    int_g nz;
    real_g origin[3];
    real_g a_step[3];
    real_g b_step[3];
    real_g c_step[3];
    int_g n_data_sets;
    string *labels;
    real_g **data;
    int_g *vector_dim;
    int_g size;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  // Todo volume_data, real_space; volume_data, real_space_periodic
  // inheritence with interface classes (also used for slices)
  // or is the connection rspace -> rspace_periodic important?
  // OR vol, rspace  with (vol, rspace), periodic
  // depends on if we need to ask space or periodicity or if local
  // render method deals with this directly.
  class real_space_volume : public volume_data {
  public:
    real_space_volume(const char code[], const string src, operation *p,
		      const int_g na, const int_g nb, const int_g nc,
		      const real_g o[3], const real_g as[3], const real_g bs[3],
		      const real_g cs[3]);

    volume_data *create(const char code[], const string src,
			operation *op) const;

  protected:
#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render_data(const class model *structure,
			       char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

    bool is_kspace() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class rspace_periodic_volume : public real_space_volume {
  public:
    rspace_periodic_volume(const char code[], const string src, operation *p,
			   const int_g na, const int_g nb, const int_g nc,
			   const real_g o[3], const real_g as[3],
			   const real_g bs[3], const real_g cs[3]);

    volume_data *create(const char code[], const string src,
			operation *op) const;

  protected:
#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render_data(const class model *structure,
			       char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

    bool is_periodic() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class k_space_volume : public volume_data {
  public:
    k_space_volume(const char code[], const string src, operation *p,
		   const int_g na, const int_g nb, const int_g nc,
		   const real_g o[3], const real_g as[3], const real_g bs[3],
		   const real_g cs[3]);

    volume_data *create(const char code[], const string src,
			operation *op) const;
    string get_data_label() const;
    string get_edit_label() const;

  protected:
#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type render_data(const class model *structure,
			       char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

    bool is_kspace() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class rspace_wavefunction : public real_space_volume {
  public:
    rspace_wavefunction(const char code[], const string src, operation *p,
			const int_g na, const int_g nb, const int_g nc,
			const real_g o[3], const real_g as[3],
			const real_g bs[3], const real_g cs[3]);
    ~rspace_wavefunction();

    volume_data *create(const char code[], const string src,
			operation *op) const;

    void add_data(real_g *r, real_g *theta, const char label[],
		  const bool copy_data = true);

  protected:
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_edit_type() const;

    real_g **get_phase() const;

#ifdef ENABLE_DLV_GRAPHICS
    bool hide_render(const int_g object, const int_g visible,
		     const render_parent *parent, class model *structure,
		     char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  private:
    real_g **phases;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  class bloch_wavefunction : public rspace_wavefunction {
  public:
    bloch_wavefunction(const char code[], const string src, operation *p,
		       const int_g na, const int_g nb, const int_g nc,
		       const real_g o[3], const real_g as[3],
		       const real_g bs[3], const real_g cs[3]);

    volume_data *create(const char code[], const string src,
			operation *op) const;

    void set_k_point(const int_g k1, const int_g k2, const int_g k3,
		     const int_g s1, const int_g s2, const int_g s3);

  protected:
    string get_data_label() const;
    string get_edit_label() const;

    bool is_periodic() const;

#ifdef ENABLE_DLV_GRAPHICS
    bool hide_render(const int_g object, const int_g visible,
		     const render_parent *parent, class model *structure,
		     char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  private:
    int_g k_num[3];
    int_g k_den[3];

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

#ifdef ENABLE_DLV_GRAPHICS
  class edit3D_object : public edit_object {
  protected:
    edit3D_object(data_object *d, edited_obj *e, const int_g i, const string s);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class edit3D_ortho : public edit3D_object {
  public:
    static edit3D_ortho *create(const render_parent *parent,
				data_object *data, const drawable_obj *obj,
				const bool kspace, const string name,
				const int_g index, char message[],
				const int_g mlen);

    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

    bool hide_render(const int_g object, const int_g visible,
		     const render_parent *parent,
		     class model *structure, char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);

    // Public for serialization
    edit3D_ortho(data_object *d, edited_obj *e, const int_g i, const string s);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class edit3D_slice : public edit3D_object {
  public:
    static edit3D_slice *create(const render_parent *parent,
				data_object *data,
				const drawable_obj *obj, const int_g component,
				const bool kspace, const string name,
				const int_g index, char message[],
				const int_g mlen);


    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

    bool hide_render(const int_g object, const int_g visible,
		     const render_parent *parent,
		     class model *structure, char message[], const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
    DLVreturn_type update_slice(const render_parent *parent,
				class model *structure, data_object *plane,
				char message[], const int_g mlen);

    edit3D_slice(data_object *d, edited_obj *e, const int_g i, const string s);

  private:
    data_object *twod;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE

  };

  class edit3D_cut : public edit3D_object {
  public:
    static edit3D_cut *create(const render_parent *parent,
			      data_object *data,
			      const drawable_obj *obj, const int_g component,
			      const bool kspace, const string name,
			      const int_g index, char message[],
			      const int_g mlen);


    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
    DLVreturn_type update_cut(const render_parent *parent,
			      class model *structure, data_object *plane,
			      char message[], const int_g mlen);

    edit3D_cut(data_object *d, edited_obj *e, const int_g i, const string s);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class edit3D_clamp : public edit3D_object {
  public:
    static edit3D_clamp *create(const render_parent *parent,
				data_object *data,
				const drawable_obj *obj, const int_g component,
				const bool kspace, const string name,
				const int_g index, char message[],
				const int_g mlen);


    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

    data_object *edit(const render_parent *parent,
		      class model *structure, const int_g component,
		      const int_g method, const int_g index, char message[],
		      const int_g mlen);
    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);

    edit3D_clamp(data_object *d, edited_obj *e, const int_g i,
		 const string s, const int_g c);

  private:
    int_g component;
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class edit3D_crop : public edit3D_object {
  public:
    static edit3D_crop *create(const render_parent *parent,
			       data_object *data,
			       const drawable_obj *obj, const int_g component,
			       const bool kspace, const string name,
			       const int_g index, char message[],
			       const int_g mlen);


    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);

    edit3D_crop(data_object *d, edited_obj *e, const int_g i, const string s);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class edit3D_extend : public edit3D_object {
  public:
    static edit3D_extend *create(const render_parent *parent,
				 data_object *data,
				 const drawable_obj *obj, const int_g component,
				 const bool kspace, const string name,
				 const int_g index, const coord_type a[3],
				 const coord_type b[3], const coord_type c[3],
				 const int_g dim, char message[],
				 const int_g mlen);


    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
    DLVreturn_type update_extension(const render_parent *parent,
				    const int_g na, const int_g nb,
				    const int_g nc, char message[],
				    const int_g mlen);

    edit3D_extend(data_object *d, edited_obj *e, const int_g i, const string s);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class edit3D_downsize : public edit3D_object {
  public:
    static edit3D_downsize *create(const render_parent *parent,
				   data_object *data,
				   const drawable_obj *obj,
				   const int_g component, const bool kspace,
				   const string name, const int_g index,
				   char message[], const int_g mlen);


    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);

    edit3D_downsize(data_object *d, edited_obj *e, const int_g i,
		    const string s);

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class edit3D_math : public edit3D_object {
  public:
    static edit3D_math *create(const render_parent *parent,
			       data_object *data,
			       const drawable_obj *obj, const int_g component,
			       const bool kspace, const string name,
			       const int_g index, char message[],
			       const int_g mlen);


    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);
    void data_math_parent(const data_object * &obj, int_g &component) const;
    DLVreturn_type update_data_math(const render_parent *parent,
				    data_object *obj, const int_g cmpt,
				    const int_g index, char message[],
				    const int_g mlen);

    edit3D_math(data_object *d, const int_g component,
		edited_obj *e, const int_g i, const string s);

  private:
    int_g obj_component;
    data_object *obj1;
    data_object *obj2;
    data_object *obj3;
    int_g component1;
    int_g component2;
    int_g component3;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class edit3D_vol_render : public edit3D_object {
  public:
    static edit3D_vol_render *create(const render_parent *parent,
				     data_object *data,
				     const drawable_obj *obj,
				     const int_g component,
				     const bool kspace, const string name,
				     const int_g index, char message[],
				     const int_g mlen);


    bool is_displayable() const;
    bool is_editable() const;
    string get_data_label() const;
    string get_edit_label() const;
    int_g get_number_data_sets() const;
    string get_sub_data_label(const int_g n) const;
    int_g get_display_type() const;
    int_g get_edit_type() const;
    bool get_sub_is_vector(const int_g n) const;

    DLVreturn_type render(const render_parent *parent,
			  class model *structure,
			  const int_g component, const int_g method,
			  const int_g index, char message[], const int_g mlen);

    edit3D_vol_render(data_object *d, edited_obj *e,
		      const int_g i, const string s);

  protected:
    void update();

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };
#endif // ENABLE_DLV_GRAPHICS

}

#ifdef DLV_USES_SERIALIZE
BOOST_SERIALIZATION_ASSUME_ABSTRACT(DLV::volume_data)

BOOST_CLASS_EXPORT_KEY(DLV::box)
BOOST_CLASS_EXPORT_KEY(DLV::real_space_box)
BOOST_CLASS_EXPORT_KEY(DLV::k_space_box)
//BOOST_CLASS_EXPORT_KEY(DLV::sphere)
BOOST_CLASS_EXPORT_KEY(DLV::real_space_sphere)
BOOST_CLASS_EXPORT_KEY(DLV::wulff_plot)
BOOST_CLASS_EXPORT_KEY(DLV::rspace_periodic_volume)
BOOST_CLASS_EXPORT_KEY(DLV::k_space_volume)
BOOST_CLASS_EXPORT_KEY(DLV::rspace_wavefunction)
BOOST_CLASS_EXPORT_KEY(DLV::bloch_wavefunction)

#  ifdef ENABLE_DLV_GRAPHICS
BOOST_SERIALIZATION_ASSUME_ABSTRACT(DLV::edit3D_object)
//BOOST_CLASS_EXPORT_KEY(DLV::edit3D_object)
BOOST_CLASS_EXPORT_KEY(DLV::edit3D_ortho)
BOOST_CLASS_EXPORT_KEY(DLV::edit3D_slice)
BOOST_CLASS_EXPORT_KEY(DLV::edit3D_cut)
BOOST_CLASS_EXPORT_KEY(DLV::edit3D_clamp)
BOOST_CLASS_EXPORT_KEY(DLV::edit3D_crop)
BOOST_CLASS_EXPORT_KEY(DLV::edit3D_extend)
BOOST_CLASS_EXPORT_KEY(DLV::edit3D_downsize)
BOOST_CLASS_EXPORT_KEY(DLV::edit3D_math)
BOOST_CLASS_EXPORT_KEY(DLV::edit3D_vol_render)
#  endif // ENABLE_DLV_GRAPHICS

#endif // DLV_USES_SERIALIZE

inline DLV::box::box(const char label[])
  : data_object("DLV", "user"), name(label)
{
  origin[0] = 0.0;
  origin[1] = 0.0;
  origin[2] = 0.0;
  pointa[0] = 1.0;
  pointa[1] = 0.0;
  pointa[2] = 0.0;
  pointb[0] = 0.0;
  pointb[1] = 1.0;
  pointb[2] = 0.0;
  pointc[0] = 0.0;
  pointc[1] = 0.0;
  pointc[2] = 1.0;
}

inline DLV::real_space_box::real_space_box(const char label[]) : box(label)
{
}

inline DLV::k_space_box::k_space_box(const char label[]) : box(label)
{
}

inline DLV::sphere::sphere(const char label[])
  : data_object("DLV", "user"), name(label)
{
  origin[0] = 0.0;
  origin[1] = 0.0;
  origin[2] = 0.0;
  radius = 1.0;
}

inline DLV::real_space_sphere::real_space_sphere(const char label[])
  : sphere(label)
{
}

inline DLV::wulff_plot::wulff_plot(const char label[])
  : data_object("DLV", "user"), name(label)
{
}

inline const std::list<DLV::wulff_data> &DLV::wulff_plot::get_wulff_data() const
{
  return planes;
}

inline
DLV::real_space_volume::real_space_volume(const char code[], const string src,
					  operation *p, const int_g na,
					  const int_g nb, const int_g nc,
					  const real_g o[3], const real_g as[3],
					  const real_g bs[3],
					  const real_g cs[3])
  : volume_data(code, src, p, na, nb, nc, o, as, bs, cs)
{
}

inline
DLV::rspace_periodic_volume::rspace_periodic_volume(const char code[],
						    const string src,
						    operation *p,
						    const int_g na,
						    const int_g nb,
						    const int_g nc,
						    const real_g o[3],
						    const real_g as[3],
						    const real_g bs[3],
						    const real_g cs[3])
  : real_space_volume(code, src, p, na, nb, nc, o, as, bs, cs)
{
}

inline
DLV::k_space_volume::k_space_volume(const char code[], const string src,
				    operation *p, const int_g na,
				    const int_g nb, const int_g nc,
				    const real_g o[3], const real_g as[3],
				    const real_g bs[3], const real_g cs[3])
  : volume_data(code, src, p, na, nb, nc, o, as, bs, cs)
{
}

inline
DLV::rspace_wavefunction::rspace_wavefunction(const char code[],
					      const string src, operation *p,
					      const int_g na, const int_g nb,
					      const int_g nc, const real_g o[3],
					      const real_g as[3],
					      const real_g bs[3],
					      const real_g cs[3])
  : real_space_volume(code, src, p, na, nb, nc, o, as, bs, cs), phases(0)
{
}

inline
DLV::bloch_wavefunction::bloch_wavefunction(const char code[],
					    const string src, operation *p,
					    const int_g na, const int_g nb,
					    const int_g nc, const real_g o[3],
					    const real_g as[3],
					    const real_g bs[3],
					    const real_g cs[3])
  : rspace_wavefunction(code, src, p, na, nb, nc, o, as, bs, cs)
{
}

inline DLV::int_g DLV::volume_data::get_ndata_sets() const
{
  return n_data_sets;
}

inline DLV::int_g DLV::volume_data::get_data_size() const
{
  return size;
}

inline DLV::int_l DLV::volume_data::get_data_length() const
{
  return (nx * ny * nz);
}

inline void DLV::volume_data::get_origin(real_g o[]) const
{
  o[0] = origin[0];
  o[1] = origin[1];
  o[2] = origin[2];
}

inline void DLV::volume_data::get_astep(real_g o[]) const
{
  o[0] = a_step[0];
  o[1] = a_step[1];
  o[2] = a_step[2];
}

inline void DLV::volume_data::get_bstep(real_g o[]) const
{
  o[0] = b_step[0];
  o[1] = b_step[1];
  o[2] = b_step[2];
}

inline void DLV::volume_data::get_cstep(real_g o[]) const
{
  o[0] = c_step[0];
  o[1] = c_step[1];
  o[2] = c_step[2];
}

inline DLV::int_g DLV::volume_data::get_nx() const
{
  return nx;
}

inline DLV::int_g DLV::volume_data::get_ny() const
{
  return ny;
}

inline DLV::int_g DLV::volume_data::get_nz() const
{
  return nz;
}

inline DLV::real_g **DLV::volume_data::get_data() const
{
  return data;
}

inline DLV::int_g *DLV::volume_data::get_vector_data() const
{
  return vector_dim;
}

inline DLV::string *DLV::volume_data::get_labels() const
{
  return labels;
}

inline DLV::real_g **DLV::rspace_wavefunction::get_phase() const
{
  return phases;
}

#ifdef ENABLE_DLV_GRAPHICS

inline DLV::edit3D_object::edit3D_object(data_object *d, edited_obj *e,
					 const int_g i, const string s)
  : edit_object(d, e, i, s)
{
}

inline DLV::edit3D_ortho::edit3D_ortho(data_object *d, edited_obj *e,
				       const int_g i, const string s)
  : edit3D_object(d, e, i, s)
{
}

inline DLV::edit3D_slice::edit3D_slice(data_object *d, edited_obj *e,
				       const int_g i, const string s)
  : edit3D_object(d, e, i, s), twod(0)
{
}

inline DLV::edit3D_cut::edit3D_cut(data_object *d, edited_obj *e,
				   const int_g i, const string s)
  : edit3D_object(d, e, i, s)
{
}

inline DLV::edit3D_clamp::edit3D_clamp(data_object *d, edited_obj *e,
				       const int_g i, const string s,
				       const int_g c)
  : edit3D_object(d, e, i, s), component(c)
{
}

inline DLV::edit3D_crop::edit3D_crop(data_object *d, edited_obj *e,
				     const int_g i, const string s)
  : edit3D_object(d, e, i, s)
{
}

inline DLV::edit3D_extend::edit3D_extend(data_object *d, edited_obj *e,
					 const int_g i, const string s)
  : edit3D_object(d, e, i, s)
{
}

inline DLV::edit3D_downsize::edit3D_downsize(data_object *d, edited_obj *e,
					     const int_g i, const string s)
  : edit3D_object(d, e, i, s)
{
}

inline DLV::edit3D_math::edit3D_math(data_object *d, const int_g component,
				     edited_obj *e, const int_g i,
				     const string s)
  : edit3D_object(d, e, i, s), obj_component(component), obj1(0), obj2(0),
    obj3(0), component1(0), component2(0), component3(0)
{
}

inline DLV::edit3D_vol_render::edit3D_vol_render(data_object *d, edited_obj *e,
						 const int_g i, const string s)
  : edit3D_object(d, e, i, s)
{
}

#endif // ENABLE_DLV_GRAPHICS

#endif // DLV_VOLUME_DATA
