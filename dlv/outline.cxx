
#include <list>
#include <map>
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/toolkit_obj.hxx"
#  include "../graphics/render_base.hxx"
#  include "../graphics/model_render.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "utils.hxx"
//#include "symmetry.hxx"
//#include "atom_model.hxx"
#include "model.hxx"
#include "constants.hxx"
#include "outline.hxx"

DLV::outline_model::~outline_model()
{
#ifdef ENABLE_DLV_GRAPHICS
  delete render_object;
#endif // ENABLE_DLV_GRAPHICS
}

DLV::outline_oneD::outline_oneD(const string model_name, float a1, int dir)
  : outline_model(model_name)
{
  vector[0] = 0;
  vector[1] = 0;
  vector[2] = 0;
  if (dir == 0)
    vector[0] = a1;
  else if (dir == 1) 
    vector[1] = a1;
  else if (dir == 2)
    vector[2] = a1;
}

DLV::outline_twoD::outline_twoD(const string model_name, float a1,
				float a2, int dir, int Lat_type)
  : outline_model(model_name)
{
  coord_type a = a1;
  coord_type b = a2;
  coord_type gamma;
  //  get_parameters(a, b, gamma); // slab
  switch (Lat_type) {
  case 0:
    gamma = 90.0;
    break;
  case 1:
    b = a;
    gamma = 90.0;
    break;
  case 2:
    b = a;
    gamma = 120.0;
    break;
  default:
    break;
  }
  // store all parameter values.
  // set_parameters(a, b, gamma); // slab
  if (dir == 2) {
    vector[0][0] = a;
    vector[0][1] = 0.0;
    vector[0][2] = 0.0;
    vector[1][0] = b * cos(to_radians(gamma));
    vector[1][1] = b * sin(to_radians(gamma));
    vector[1][2] = 0.0;
  } else if (dir == 1) {
    vector[0][0] = 0.0;
    vector[0][1] = 0.0;
    vector[0][2] = a;
    vector[1][0] = b * sin(to_radians(gamma));
    vector[1][1] = 0.0;
    vector[1][2] = b * cos(to_radians(gamma));
  } else if (dir == 0) {
    vector[0][0] = 0.0;
    vector[0][1] = a;
    vector[0][2] = 0.0;
    vector[1][0] = b * cos(to_radians(gamma));
    vector[1][1] = 0.0;
    vector[1][2] = b * sin(to_radians(gamma));
  }
}

DLV::outline_threeD::outline_threeD(const string model_name, float a1,
				    float a2, float a3, int dir, int Lat_type)
  : outline_model(model_name)
{
  //const real_l tol = 0.01;
  coord_type a = a1;
  coord_type b = a2;
  coord_type c = a3;
  //coord_type alpha;
  //coord_type beta;
  //coord_type gamma;
  // This may be incomplete, Barry.
  // get_parameters(a, b, c, alpha, beta, gamma); //crystal
  switch (Lat_type) {
  case 0:
    //alpha = 90.0;
    //beta = 90.0;
    //gamma = 90.0;
    break;
  case 1:
    b = a;
    //alpha = 90.0;
    //beta = 90.0;
    //gamma = 90.0;
    break;
  case 2:
    b = a;
    //alpha = 90.0;
    //beta = 90.0;
    //gamma = 120.0;
    break;
  case 3:
    b = a;
    c = a;
    //alpha = 90.0;
    //beta = 90.0;
    //gamma = 90.0;
    break;
  default:
    break;
  }
  {
    real_l scale[3];
    scale[0] = a;
    scale[1] = b;
    scale[2] = c;
    // Create unit conventional cell
    real_l ma[3][3] = {{ 1.0, 0.0, 0.0 }, { 0.0, 1.0, 0.0 }, { 0.0, 0.0, 1.0 }};
    if (Lat_type == 2) {
      ma[0][1] = -0.5;
      ma[0][0] = std::sqrt(0.75);
    }
    // scale to actual conventional cell
    for (int_g i = 0; i < 3; i++) {
      real_l p = scale[i];
      for (int_g j = 0; j < 3; j++)
	vector[i][j] = ma[i][j] * p;
    }
  }
}

void DLV::outline_model::duplicate(outline_model *m) const
{
  m->polyhedra = polyhedra;
  m->cylinders = cylinders;
}

DLV::model *DLV::outline_molecule::duplicate_model(const string label) const
{
  outline_molecule *m = new outline_molecule(label);
  duplicate(m);
  return m;
}

DLV::model *DLV::outline_oneD::duplicate_model(const string label) const
{
  outline_oneD *m = new outline_oneD(label, vector);
  duplicate(m);
  return m;
}

DLV::model *DLV::outline_twoD::duplicate_model(const string label) const
{
  outline_twoD *m = new outline_twoD(label, vector);
  duplicate(m);
  return m;
}

DLV::model *DLV::outline_threeD::duplicate_model(const string label) const
{
  outline_threeD *m = new outline_threeD(label, vector);
  duplicate(m);
  return m;
  }

void DLV::outline_model::add_polyhedra()
{
  int_g index = int_g(polyhedra.size());
  std::list<std::list<plane_vertex> > data;
  polyhedra[index + 1] = data;
}

bool DLV::outline_model::add_polygon(coord_type c[][3], const int_g n,
				     char message[], const int_g mlen)
{
  if (polyhedra.size() == 0) {
    std::list<std::list<plane_vertex> > data;
    polyhedra[1] = data;
  }
  // find the object
  int_g index = int_g(polyhedra.size());
  std::map<int, std::list<std::list<plane_vertex> > >::iterator p
    = polyhedra.find(index);
  std::list<plane_vertex> plane;
  for (int_g i = 0; i < n; i++)
    plane.push_back(c[i]);
  p->second.push_back(plane);
  return true;
}

void DLV::outline_model::add_cylinder(const coord_type r1, const coord_type r2,
				      const coord_type zmin,
				      const coord_type zmax, const int_g o)
{
  int_g index = int_g(cylinders.size());
  cylinder_data d;
  d.inner_radius = r1;
  d.outer_radius = r2;
  d.min_p = zmin;
  d.max_p = zmax;
  d.orientation = o;
  cylinders[index + 1] = d;
}

void DLV::outline_model::add_sphere(const coord_type radius)
{
  int_g index = int_g(spheres.size());
  sphere_data d;
  d.radius = radius;
  spheres[index + 1] = d;
}

void DLV::outline_model::complete(const bool)
{
  // Don't think we need to do anything.
}

// Dummy functions required by virtual in model (Todo - better way?)
bool DLV::outline_model::set_group(const int_g)
{
  return false;
}

bool DLV::outline_model::set_group(const char[], const int_g,
				   const int_g, const bool)
{
  return false;
}

bool DLV::outline_model::set_cartesian_sym_ops(const real_l [][3][3],
					       const real_l [][3],
					       const int_g)
{
  return false;
}

bool DLV::outline_model::set_fractional_sym_ops(const real_l [][3][3],
						const real_l [][3],
						const int_g)
{
  return false;
}

bool DLV::outline_model::set_primitive_lattice(const coord_type [3],
					       const coord_type [3],
					       const coord_type [3])
{
  return false;
}

bool DLV::outline_model::set_lattice_parameters(const coord_type,
						const coord_type,
						const coord_type,
						const coord_type,
						const coord_type,
						const coord_type)
{
  return false;
}

bool DLV::outline_model::add_cartesian_atom(int_g &, const int_g,
					    const coord_type [3])
{
  return false;
}

bool DLV::outline_model::add_fractional_atom(int_g &, const int_g,
					     const coord_type [3],
					     const bool, const bool)
{
  return false;
}

bool DLV::outline_model::add_cartesian_atom(int_g &, const char [],
					    const coord_type [3])
{
  return false;
}

bool DLV::outline_model::add_fractional_atom(int_g &, const char [],
					     const coord_type [3],
					     const bool, const bool)
{
  return false;
}

DLV::int_g DLV::outline_molecule::get_number_of_periodic_dims() const
{
  return 0;
}

DLV::int_g DLV::outline_oneD::get_number_of_periodic_dims() const
{
  return 1;
}

DLV::int_g DLV::outline_twoD::get_number_of_periodic_dims() const
{
  return 2;
}

DLV::int_g DLV::outline_threeD::get_number_of_periodic_dims() const
{
  return 3;
}


DLV::int_g DLV::outline_model::get_lattice_type() const
{
  return 0;
}

DLV::int_g DLV::outline_model::get_lattice_centring() const
{
  return 0;
}

// Kind of the dimensionality - for the User Interface menus.
DLV::int_g DLV::outline_molecule::get_model_type() const
{
  return 8;
}

DLV::int_g DLV::outline_oneD::get_model_type() const
{
  return 9;
}

DLV::int_g DLV::outline_twoD::get_model_type() const
{
  return 10;
  }

DLV::int_g DLV::outline_threeD::get_model_type() const
{
  return 11;
  }

void DLV::outline_model::set_crystal03_lattice_type(const int_g, const int_g)
{
  // Todo - a bug
}

void DLV::outline_molecule::get_primitive_lattice(coord_type a[3],
						  coord_type b[3],
						  coord_type c[3]) const
{
  a[0] = 0.0;
  a[1] = 0.0;
  a[2] = 0.0;
  b[0] = 0.0;
  b[1] = 0.0;
  b[2] = 0.0;
  c[0] = 0.0;
  c[1] = 0.0;
  c[2] = 0.0;
}


void DLV::outline_oneD::get_primitive_lattice(coord_type a[3],
						  coord_type b[3],
						  coord_type c[3]) const
{
  a[0] = vector[0];
  a[1] = vector[1];
  a[2] = vector[2];
  b[0] = 0.0;
  b[1] = 0.0;
  b[2] = 0.0;
  c[0] = 0.0;
  c[1] = 0.0;
  c[2] = 0.0;
}

void DLV::outline_twoD::get_primitive_lattice(coord_type a[3],
						  coord_type b[3],
						  coord_type c[3]) const
{
  a[0] = vector[0][0];
  a[1] = vector[0][1];
  a[2] = vector[0][2];
  b[0] = vector[1][0];
  b[1] = vector[1][1];
  b[2] = vector[1][2];
  c[0] = 0.0;
  c[1] = 0.0;
  c[2] = 0.0;
}

void DLV::outline_threeD::get_primitive_lattice(coord_type a[3],
						  coord_type b[3],
						  coord_type c[3]) const
{
  a[0] = vector[0][0];
  a[1] = vector[0][1];
  a[2] = vector[0][2];
  b[0] = vector[1][0];
  b[1] = vector[1][1];
  b[2] = vector[1][2];
  c[0] = vector[2][0];
  c[1] = vector[2][1];
  c[2] = vector[2][2];
}




void DLV::outline_model::get_conventional_lattice(coord_type a[3],
						     coord_type b[3],
						     coord_type c[3]) const
{
  get_primitive_lattice(a, b, c);
}

void DLV::outline_model::get_reciprocal_lattice(coord_type a[3],
						coord_type b[3],
						coord_type c[3]) const
{
  get_primitive_lattice(a, b, c);
}

DLV::int_g DLV::outline_model::get_hermann_mauguin_group(string &name) const
{
  name = "P1";
  return 1;
}

void DLV::outline_model::get_point_group(string &base, int_g &n,
					 string &tail) const
{
  base = "c";
  n = 1;
  tail = "";
}

DLV::int_g DLV::outline_model::get_number_of_sym_ops() const
{
  return 1;
}

void DLV::outline_model::get_cart_rotation_operators(real_l r[][3][3],
						     const int_g) const
{
  for (int_g i = 0; i < 3; i++)
    for (int_g j = 0; j < 3; j++)
      r[0][i][j] = 0.0;
  r[0][0][0] = 1.0;
  r[0][1][1] = 1.0;
  r[0][2][2] = 1.0;
}

void DLV::outline_model::get_frac_rotation_operators(real_l r[][3][3],
						     const int_g) const
{
  for (int_g i = 0; i < 3; i++)
    for (int_g j = 0; j < 3; j++)
      r[0][i][j] = 0.0;
  r[0][0][0] = 1.0;
  r[0][1][1] = 1.0;
  r[0][2][2] = 1.0;
}

void DLV::outline_model::get_cart_translation_operators(real_l t[][3],
							const int_g) const
{
  for (int_g i = 0; i < 3; i++)
    t[0][i] = 0.0;
}

void DLV::outline_model::get_frac_translation_operators(real_l t[][3],
							const int_g) const
{
  for (int_g i = 0; i < 3; i++)
    t[0][i] = 0.0;
}

DLV::int_g DLV::outline_model::get_number_of_asym_atoms() const
{
  return 0;
}

DLV::int_g DLV::outline_model::get_number_of_primitive_atoms() const
{
  return 0;
}

void DLV::outline_model::get_asym_atom_types(int_g atom_types[],
					     const int_g n) const
{
  if (n > 0)
    atom_types[0] = 0;
}

void DLV::outline_model::get_primitive_atom_types(int_g atom_types[],
						  const int_g n) const
{
  if (n > 0)
    atom_types[0] = 0;
}

void DLV::outline_model::get_asym_atom_cart_coords(coord_type coords[][3],
						   const int_g n) const
{
  if (n > 0) {
    coords[0][0] = 0.0;
    coords[0][1] = 0.0;
    coords[0][2] = 0.0;
  }
}

void DLV::outline_model::get_asym_atom_frac_coords(coord_type coords[][3],
						   const int_g n) const
{
  if (n > 0) {
    coords[0][0] = 0.0;
    coords[0][1] = 0.0;
    coords[0][2] = 0.0;
  }
}

void DLV::outline_model::get_primitive_atom_cart_coords(coord_type coords[][3],
							const int_g n) const
{
  if (n > 0) {
    coords[0][0] = 0.0;
    coords[0][1] = 0.0;
    coords[0][2] = 0.0;
  }
}

void DLV::outline_model::get_primitive_atom_frac_coords(coord_type coords[][3],
							const int_g n) const
{
  if (n > 0) {
    coords[0][0] = 0.0;
    coords[0][1] = 0.0;
    coords[0][2] = 0.0;
  }
}

void DLV::outline_molecule::generate_transforms(const int_g, const int_g,
						const int_g, const int_g,
						const int_g, const int_g,
						const bool, const bool,
						int_g &ntransforms,
						real_g (* &transforms)[3]) const
{
  ntransforms = 1;
  transforms = new real_g[1][3];
  transforms[0][0] = 0.0;
  transforms[0][1] = 0.0;
  transforms[0][2] = 0.0;
}

void DLV::outline_oneD::generate_transforms(const int_g na, const int_g nb,
						const int_g nc, const int_g sa,
						const int_g sb, const int_g sc,
						const bool centre_cell, const bool conventional,
						int_g &ntransforms,
						real_g (* &transforms)[3]) const
{
  ntransforms = na;
  transforms = new real_g[na][3];
  int_g min_a = 0;
  int_g max_a = na;
    max_a = na / 2;
    min_a = - max_a;
    if (max_a * 2 != na)
      max_a++;
  for (int_g i = min_a; i < max_a; i++) {
    transforms[i - min_a][0] = (real_g)(i * sa) * vector[0];
    transforms[i - min_a][1] = (real_g)(i * sa) * vector[1];
    transforms[i - min_a][2] = (real_g)(i * sa) * vector[2];
  }
}
// copy from slab
void DLV::outline_twoD::generate_transforms(const int_g na, const int_g nb,
				    const int_g, const int_g sa, const int_g sb,
				    const int_g, const bool centre_cell,
				    const bool conventional, int_g &ntransforms,
				    real_g (* &transforms)[3]) const
{
  int_g size = na * nb;
  ntransforms = size;
  transforms = new real_g[size][3];
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  int_g min_a = 0;
  int_g max_a = na;
  int_g min_b = 0;
  int_g max_b = nb;
    max_a = na / 2;
    min_a = - max_a;
    if (max_a * 2 != na)
      max_a++;
    max_b = nb / 2;
    min_b = - max_b;
    if (max_b * 2 != nb)
      max_b++;
  int_g k = 0;
  for (int_g i = min_a; i < max_a; i++) {
    real_g x[3];
    x[0] = (real_g)(i * sa) * (real_g)a[0];
    x[1] = (real_g)(i * sa) * (real_g)a[1];
    x[2] = (real_g)(i * sa) * (real_g)a[2];
    for (int_g j = min_b; j < max_b; j++) {
      transforms[k][0] = x[0] + (real_g)(j * sb) * (real_g)b[0];
      transforms[k][1] = x[1] + (real_g)(j * sb) * (real_g)b[1];
      transforms[k][2] = x[2] + (real_g)(j * sb) * (real_g)b[2];
      k++;
    }
  }
}


void DLV::outline_threeD::generate_transforms(const int_g na, const int_g nb,
				       const int_g nc, const int_g sa,
				       const int_g sb, const int_g sc,
				       const bool centre_cell,
				       const bool conventional,
				       int_g &ntransforms,
				       real_g (* &transforms)[3]) const
{
  int_g size = na * nb * nc;
  ntransforms = size;
  transforms = new real_g[size][3];
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  int_g count = 0;
  int_g min_a = 0;
  int_g max_a = na;
  int_g min_b = 0;
  int_g max_b = nb;
  int_g min_c = 0;
  int_g max_c = nc;
    max_a = na / 2;
    min_a = - max_a;
    if (max_a * 2 != na)
      max_a++;
    max_b = nb / 2;
    min_b = - max_b;
    if (max_b * 2 != nb)
      max_b++;
    max_c = nc / 2;
    min_c = - max_c;
    if (max_c * 2 != nc)
      max_c++;
  for (int_g i = min_a; i < max_a; i++) {
    real_g x[3];
    x[0] = (real_g)(i * sa) * (real_g)a[0];
    x[1] = (real_g)(i * sa) * (real_g)a[1];
    x[2] = (real_g)(i * sa) * (real_g)a[2];
    for (int_g j = min_b; j < max_b; j++) {
      real_g y[3];
      y[0] = x[0] + (real_g)(j * sb) * (real_g)b[0];
      y[1] = x[1] + (real_g)(j * sb) * (real_g)b[1];
      y[2] = x[2] + (real_g)(j * sb) * (real_g)b[2];
      for (int_g k = min_c; k < max_c; k++) {
	transforms[count][0] = y[0] + (real_g)(k * sc) * (real_g)c[0];
	transforms[count][1] = y[1] + (real_g)(k * sc) * (real_g)c[1];
	transforms[count][2] = y[2] + (real_g)(k * sc) * (real_g)c[2];
	count++;
      }
    }
  }
}
 

#ifdef ENABLE_DLV_GRAPHICS

DLV::render_parent *DLV::outline_model::create_render_parent(render_parent *p,
							     const bool,
							     const bool)
{
  return new render_outline(get_model_name().c_str());
}

DLVreturn_type DLV::outline_model::render_r(render_parent *p,
					    const model *m,
					    char message[], const int_g mlen)
{
  if (render_object == 0)
    render_object = new model_outline_r(p);
  return update_outline(p, message, mlen);
}

DLVreturn_type DLV::outline_model::render_k(render_parent *p,
					    char message[], const int_g mlen)
{
  strncpy(message, "BUG: Outline model k space rendering", mlen);
  return DLV_ERROR;
}

DLV::toolkit_obj DLV::outline_model::get_r_ui_obj() const
{
  if (render_object == 0)
    return toolkit_obj();
  else
    return render_object->get_ui_obj();
}


DLVreturn_type DLV::outline_model::update_cell(render_parent *p,
					       char message[], const int_g mlen)
{
   int_g na;
  int_g nb;
  int_g nc;
  bool centre_cell;
  bool conventional;
  bool edges;
  real_l tol;
  p->get_cell_data(na, nb, nc, centre_cell, conventional, edges, tol);
  DLVreturn_type ok = update_lattice(p, false, message, mlen);
  int_g ntransforms = 1;
  real_g (*transforms)[3];
  generate_transforms(na, nb, nc, 1, 1, 1, centre_cell, conventional,
		      ntransforms, transforms);
  p->set_transforms(ntransforms, transforms);
  delete [] transforms;
  return ok;
}

DLVreturn_type DLV::outline_molecule::update_lattice(render_parent *p,
						  const bool kspace,
						  char message[],
						  const int_g mlen)
{
  //strncpy(message, "BUG: Outline model lattice update called", mlen);
  return DLV_OK;
}

DLVreturn_type DLV::outline_oneD::update_lattice(render_parent *p,
						  const bool kspace,
						  char message[],
						  const int_g mlen)
{
  //strncpy(message, "BUG: Outline model lattice update called", mlen);
  return DLV_OK;
}

DLVreturn_type DLV::outline_twoD::update_lattice(render_parent *p,
						  const bool kspace,
						  char message[],
						  const int_g mlen)
{
  //strncpy(message, "BUG: Outline model lattice update called", mlen);
  return DLV_OK;
}

DLVreturn_type DLV::outline_threeD::update_lattice(render_parent *p,
						  const bool kspace,
						  char message[],
						  const int_g mlen)
{
  //strncpy(message, "BUG: Outline model lattice update called", mlen);
  return DLV_OK;
}

DLVreturn_type DLV::outline_model::update_atoms(const render_parent *p,
						char message[],
						const int_g mlen)
{
  strncpy(message, "BUG: Outline model atom update called", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::outline_model::update_bonds(const render_parent *p,
						char message[],
						const int_g mlen) const
{
  strncpy(message, "BUG: Outline model bond update called", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::outline_model::add_bond(render_parent *p,
					    char message[], const int_g mlen)
{
  strncpy(message, "BUG: Outline model bond update called", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::outline_model::del_bond(render_parent *p,
					    char message[], const int_g mlen)
{
  strncpy(message, "BUG: Outline model bond update called", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::outline_model::add_bond_type(render_parent *p,
						 char message[],
						 const int_g mlen)
{
  strncpy(message, "BUG: Outline model bond update called", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::outline_model::del_bond_type(render_parent *p,
						 char message[],
						 const int_g mlen)
{
  strncpy(message, "BUG: Outline model bond update called", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::outline_model::add_bond_all(render_parent *p,
						char message[],
						const int_g mlen)
{
  strncpy(message, "BUG: Outline model bond update called", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::outline_model::del_bond_all(render_parent *p,
						char message[],
						const int_g mlen)
{
  strncpy(message, "BUG: Outline model bond update called", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::outline_model::select_atom(render_parent *p,
					       const real_g coords[3],
					       const int_g status,
					       int_g &nselects,
					       char message[], const int_g mlen)
{
  nselects = 0;
  strncpy(message, "BUG: Outline model select atom called", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::outline_model::deselect_all_atoms(render_parent *p,
						      char message[],
						      const int_g mlen)
{
  strncpy(message, "BUG: Outline model deselect atom called", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::outline_model::update_outline(render_parent *p,
						  char message[],
						  const int_g mlen)
{
  int nvertices = 0;
  int nplanes = 0;
  float (*vertices)[3] = 0;
  int *planes = 0;
  if (polyhedra.size() > 0) {
    std::map<int, std::list<std::list<plane_vertex> > >::const_iterator p;
    for (p = polyhedra.begin(); p != polyhedra.end(); ++p) {
      nplanes += int(p->second.size());
      std::list<std::list<plane_vertex> >::const_iterator v;
      for (v = p->second.begin(); v != p->second.end(); ++v)
	nvertices += int(v->size());
    }
    vertices = new float[nvertices][3];
    planes = new int[nplanes];
    int i = 0;
    int j = 0;
    for (p = polyhedra.begin(); p != polyhedra.end(); ++p) {
      std::list<std::list<plane_vertex> >::const_iterator v;
      for (v = p->second.begin(); v != p->second.end(); ++v) {
	planes[i] = int(v->size());
	i++;
	std::list<plane_vertex>::const_iterator x;
	for (x = v->begin(); x != v->end(); ++x) {
	  vertices[j][0] = x->x;
	  vertices[j][1] = x->y;
	  vertices[j][2] = x->z;
	  j++;
	}
      }
    }
  }
  int ncylinders = int(cylinders.size());
  float *iradius = 0;
  float *oradius = 0;
  float *dmin = 0;
  float *dmax = 0;
  int *odir = 0;
  if (ncylinders > 0) {
    iradius = new float[ncylinders];
    oradius = new float[ncylinders];
    dmin = new float[ncylinders];
    dmax = new float[ncylinders];
    odir = new int[ncylinders];
    std::map<int, cylinder_data>::const_iterator c;
    int i = 0;
    for (c = cylinders.begin(); c != cylinders.end(); ++c) {
      iradius[i] = c->second.inner_radius;
      oradius[i] = c->second.outer_radius;
      dmin[i] = c->second.min_p;
      dmax[i] = c->second.max_p;
      odir[i] = c->second.orientation;
      i++;
    }
  }
  int nspheres = int(spheres.size());
  float *radii = 0;
  if (nspheres > 0) {
    radii = new float[nspheres];
    std::map<int, sphere_data>::const_iterator c;
    int i = 0;
    for (c = spheres.begin(); c != spheres.end(); ++c) {
      radii[i] = c->second.radius;
      i++;
    }
  }
  render_object->set_data(vertices, nvertices, planes, nplanes,
			  iradius, oradius, dmin, dmax, odir, ncylinders,
			  radii, nspheres);
  if (nspheres > 0)
    delete [] radii;
  if (ncylinders > 0) {
    delete [] odir;
    delete [] dmax;
    delete [] dmin;
    delete [] oradius;
    delete [] iradius;
  }
  if (polyhedra.size() > 0) {
    delete [] planes;
    delete [] vertices;
  }
  return update_cell(p, message, mlen);
}

#endif // ENABLE_DLV_GRAPHICS


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::outline_molecule *t,
				    const unsigned int file_version)
    {
      DLV::string n = t->get_model_name();
      ar << n;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::outline_molecule *t,
				    const unsigned int file_version)
    {
      DLV::string n;
      ar >> n;
      ::new(t)DLV::outline_molecule(n);
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::outline_oneD *t,
				    const unsigned int file_version)
    {
      DLV::string n = t->get_model_name();
      ar << n;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::outline_oneD *t,
				    const unsigned int file_version)
    {
      DLV::string n;
      ar >> n;
      ::new(t)DLV::outline_oneD(n, 0.0, 0);
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::outline_twoD *t,
				    const unsigned int file_version)
    {
      DLV::string n = t->get_model_name();
      ar << n;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::outline_twoD *t,
				    const unsigned int file_version)
    {
      DLV::string n;
      ar >> n;
      ::new(t)DLV::outline_twoD(n, 0.0, 0.0, 0, 0);
    }

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::outline_threeD *t,
				    const unsigned int file_version)
    {
      DLV::string n = t->get_model_name();
      ar << n;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::outline_threeD *t,
				    const unsigned int file_version)
    {
      DLV::string n;
      ar >> n;
      ::new(t)DLV::outline_threeD(n, 0.0, 0.0, 0.0, 0, 0);
    }

  }
}

template <class Archive>
void DLV::plane_vertex::serialize(Archive &ar, const unsigned int version)
{
  ar & x;
  ar & y;
  ar & z;
}

template <class Archive>
void DLV::cylinder_data::serialize(Archive &ar, const unsigned int version)
{
  ar & inner_radius;
  ar & outer_radius;
  ar & min_p;
  ar & max_p;
  ar & orientation;
}

template <class Archive>
void DLV::sphere_data::serialize(Archive &ar, const unsigned int version)
{
  ar & radius;
}

template <class Archive>
void DLV::outline_model::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<model>(*this);
  ar & polyhedra;
  ar & cylinders;
  ar & spheres;
#ifdef ENABLE_DLV_GRAPHICS
  ar & render_object;
#endif // ENABLE_DLV_GRAPHICS
}

template <class Archive>
void DLV::outline_molecule::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<outline_model>(*this);
}

template <class Archive>
void DLV::outline_oneD::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<outline_model>(*this);
  ar & vector;
}

template <class Archive>
void DLV::outline_twoD::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<outline_model>(*this);
  ar & vector;
}

template <class Archive>
void DLV::outline_threeD::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<outline_model>(*this);
  ar &vector;
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::outline_model)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::outline_molecule)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::outline_oneD)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::outline_twoD)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::outline_threeD)

DLV_SUPPRESS_TEMPLATES(DLV::model)
#ifdef ENABLE_DLV_GRAPHICS
DLV_SUPPRESS_TEMPLATES(DLV::model_outline_r)
#endif // ENABLE_DLV_GRAPHICS

DLV_NORMAL_EXPLICIT(DLV::outline_model)
DLV_NORMAL_EXPLICIT(DLV::outline_molecule)
DLV_NORMAL_EXPLICIT(DLV::outline_oneD)
DLV_NORMAL_EXPLICIT(DLV::outline_twoD)
DLV_NORMAL_EXPLICIT(DLV::outline_threeD)

#endif // DLV_USES_SERIALIZE
