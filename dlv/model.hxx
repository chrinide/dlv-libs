
#ifndef DLV_MODEL
#define DLV_MODEL

#include <vector>
#include <list>

namespace DLV {

  class model {
  public:
    virtual ~model();

    static DLVreturn_type configure_display_prefs(const char filename[],
						  char message[],
						  const int_g mlen);

    virtual bool is_shell() const;
    virtual bool is_crystal() const;
    virtual bool has_salvage() const;

    virtual void add_polyhedra();
    virtual bool add_polygon(coord_type c[][3], const int_g n,
			     char message[], const int_g mlen);
    virtual void add_cylinder(const coord_type r1, const coord_type r2,
			      const coord_type zmin, const coord_type zmax,
			      const int_g o);
    virtual void add_sphere(const coord_type radius);

    virtual bool set_primitive_lattice(const coord_type a[3],
				       const coord_type b[3],
				       const coord_type c[3]) = 0;
    virtual bool set_lattice_parameters(const coord_type a, const coord_type b,
					const coord_type c,
					const coord_type alpha,
					const coord_type beta,
					const coord_type gamma) = 0;
    virtual void set_lattice_and_centre(const int_g lattice,
					const int_g centre);
    virtual void set_crystal03_lattice_type(const int_g lattice,
					    const int_g centre) = 0;
    virtual bool set_group(const int_g gp) = 0;
    virtual bool set_point_group(const char gp[]);
    virtual bool set_group(const char gp[], const int_g setting = 0,
			   const int_g nsettings = 1,
			   const bool hex = false) = 0;
    virtual bool set_operators(const string ops[], const int_g nops,
			       const int_g gp, const bool rhomb,
			       const bool prim);
    virtual bool set_cartesian_sym_ops(const real_l rotations[][3][3],
				       const real_l translations[][3],
				       const int_g nops) = 0;
    virtual bool set_fractional_sym_ops(const real_l rotations[][3][3],
					const real_l translations[][3],
					const int_g nops) = 0;
    virtual void set_indexed_asym_cart_coords(const coord_type coords[3], 
					       const int_g n) const;
    virtual bool set_fractional_trans_ops(const real_l rotations[][3][3],
					  const real_l translations[][3],
					  const int_g nops);
    virtual bool add_cartesian_atom(int_g &index, const int_g atomic_number,
				    const coord_type coords[3]) = 0;
    virtual bool add_cartesian_ghost_atom(const coord_type coords[3]);
    virtual bool add_cartesian_point_charge(const coord_type coords[3]);
    virtual bool add_fractional_atom(int_g &index, const int_g atomic_number,
				     const coord_type coords[3],
				     const bool rhomb = false,
				     const bool prim = false) = 0;
    virtual bool add_cartesian_atom(int_g &index, const char symbol[],
				    const coord_type coords[3]) = 0;
    virtual bool add_fractional_atom(int_g &index, const char symbol[],
				     const coord_type coords[3],
				     const bool rhomb = false,
				     const bool prim = false) = 0;
    virtual bool set_atom_charge(const string symbol, const int_g charge,
				 char message[], const int_g mlen);
    virtual bool set_core_electrons(const string symbol, const int_g core,
				    char message[], const int_g mlen);
    virtual void set_point_charge(const int_g index, const real_l charge,
				  char message[], const int_g mlen);
    virtual void set_isotope(const int_g index, const int_g isotope,
			     char message[], const int_g mlen);
    virtual void add_bond(const int_g atom1, const int_g atom2);
    virtual void group_atoms(const string gp, const string label,
			     const bool atoms[]);
    virtual void add_shells(const int_g types[], const real_g numbers[],
			    const real_g radii[], const int_g nshells);
    virtual void convert_coords(const bool frac, real_l &x,
				real_l &y, real_l &z) const;
    virtual void set_atom_charge(const int_g index, const int_g charge);
    virtual void set_atom_radius(const int_g index, const real_g r);
    virtual void set_atom_crystal03_id(const int_g index, const int_g id);
    virtual void set_atoms_group(const int_g index, const string gpname,
				 const string label);

    virtual DLVreturn_type create_atom_group(const char label[],
					     const char group[],
					     const bool all,
					     const char remainder[],
					     char message[], const int_g mlen);
    virtual DLVreturn_type update_cluster_region(const real_g radii[],
						 const int_g nradii,
						 const char group[],
						 char message[],
						 const int_g mlen);
    virtual DLVreturn_type update_cluster_region(const int_g shells[],
						 const int_g nshells,
						 const char group[],
						 char message[],
						 const int_g mlen);
    virtual DLVreturn_type clear_cluster_region(const char group[],
						char message[],
						const int_g mlen);
    virtual DLVreturn_type set_supercell_region(const model *parent,
						const int_g shells[],
						const int_g nshells,
						const char group[],
						char message[],
						const int_g mlen);

    virtual model *duplicate_model(const string label) const = 0;
    virtual void copy_model(const model *m);
    virtual model *copy_model(const string name) const;
    virtual void replace_atom_cart_coords(const coord_type coords[][3]);
    virtual void update_model(const model *m);
    virtual bool supercell(const model *m, const int_g matrix[3][3],
			   const bool conv_cell);
    virtual bool delete_symmetry(const model *m);
    virtual bool view_to_molecule(const model *m);
    virtual bool view_to_molecule(const model *m, const bool conv,
				  const bool centred, const bool edges);
    virtual bool cell_to_molecule(const model *m, const int_g na,
				  const int_g nb, const int_g nc,
				  const bool conv, const bool centred,
				  const bool edges);
    virtual bool update_lattice_parameters(const model *m,
					   const coord_type params[6]);
    virtual bool update_origin(const model *m, const real_l x, const real_l y,
			       const real_l z, const bool frac);
    virtual bool update_origin_symm(const model *m, real_l &x, real_l &y,
				    real_l &z, const bool frac);
    virtual bool update_vacuum(const model *m, const real_l c, const real_l o);
    virtual bool update_cluster(model *m, const int_g n);
    virtual bool update_cluster(model *m, const int_g n, const real_l x,
				const real_l y, const real_l z);
    virtual bool update_cluster(model *m, const real_l r);
    virtual bool wulff(model *m, const real_l r, class data_object *data);
    virtual bool multi_layer(model *m, const real_l r, model *slab);
    virtual bool fill_geometry(model *m, const model *shape);
    virtual bool delete_atoms(model *m);
    virtual bool insert_atom(const model *m, const int_g atn, const real_l x,
			     const real_l y, const real_l z, const bool frac,
			     const bool keep);
    virtual bool insert_model(const model *m, const model *i, const real_l x,
			      const real_l y, const real_l z, const bool frac,
			      const real_l c_x, const real_l c_y,
			      const real_l c_z, const bool keep);
    virtual bool move_atoms(model *m, const real_l x, const real_l y,
			    const real_l z, const real_l c_x, const real_l c_y,
			    const real_l c_z, real_l shift[3],
			    const bool keep);
    virtual bool edit_atom(model *m, const int_g atn,
			   const bool all, const int_g index);
    virtual bool edit_atom_props(model *m, const int_g charge,
				 real_g &radius, const real_g red,
				 const real_g green, const real_g blue,
				 const int_g spin, const bool use_charge,
				 const bool use_radius, const bool use_colour,
				 const bool use_spin, const bool all,
				 const int_g index);
    virtual bool edit_atom_rod_props(const model *m, const int_g dw1,
				     const bool use_dw1, const int_g dw2,
				     const bool use_dw2, const int_g occ,
				     const bool use_occ, const bool all,
				     const int_g index);
    virtual bool update_slab(const model *m, model *threed, const int_g h,
			     const int_g k, const int_g l,
			     const bool conventional, const real_g tol,
			     int_g &layers, string * &labels, int_g &n,
			     real_l r[3][3]);
    virtual bool update_slab(const model *m, const real_g tol, int_g &layers,
			     string * &labels, int_g &n);
    virtual bool update_slab(const model *m, model *threed, const real_g tol,
			     const int_g term, const int_g nl);
    virtual bool surface_slab(const model *m, const real_g tol,
			      int_g &layers, string * &labels, int_g &n);
    virtual bool surface_slab(const model *m, const real_g tol, int_g layers);
    virtual bool update_surface(const model *m, model *threed, const int_g h,
				const int_g k, const int_g l,
				const bool conventional, const real_g tol,
				string * &labels, int_g &n, real_l r[3][3]);
    virtual bool update_surface(const model *m, const real_g tol,
				const int_g term);
    virtual bool update_salvage(const model *m, const real_g tol,
				const int_g nlayers);
    virtual void update_shells(const int_g types[], const real_g numbers[],
			       const real_g radii[], const int_g nshells);

    virtual void complete(const bool only_atoms = false) = 0;
    virtual void update_atom_list();
    virtual int_g get_model_type() const = 0;
    void get_model_name(char mname[], const int_g n) const;
    string get_model_name() const;
    virtual string get_formula();
    virtual void count_primitive_atom_types(int_g count[],
					    const int_g len) const;
    virtual void count_crystal03_ids(int_g count[], const int_g len) const;

    virtual int_g get_number_of_periodic_dims() const = 0;
    virtual int_g get_lattice_type() const = 0;
    virtual int_g get_lattice_centring() const = 0;
    virtual void get_primitive_lattice(coord_type a[3], coord_type b[3],
				       coord_type c[3]) const = 0;
    virtual void get_conventional_lattice(coord_type a[3], coord_type b[3],
					  coord_type c[3]) const = 0;
    virtual void get_lattice_parameters(coord_type &a, coord_type &b,
					coord_type &c, coord_type &alpha,
					coord_type &beta, coord_type &gamma,
					const bool conventional = true) const;
    virtual void use_which_lattice_parameters(bool usage[6]) const;
    virtual int_g get_hermann_mauguin_group(string &name) const = 0;
    virtual void get_point_group(string &base, int_g &n,
				 string &tail) const = 0;
    virtual int_g get_number_of_sym_ops() const = 0;
    virtual void get_cart_rotation_operators(real_l r[][3][3],
					     const int_g nops) const = 0;
    virtual void get_frac_rotation_operators(real_l r[][3][3],
					     const int_g nops) const = 0;
    virtual void get_cart_translation_operators(real_l t[][3],
						const int_g nops) const = 0;
    virtual void get_frac_translation_operators(real_l t[][3],
						const int_g nops) const = 0;
    virtual int_g get_number_of_asym_atoms() const = 0;
    virtual int_g get_number_of_primitive_atoms() const = 0;
    virtual void get_asym_atom_types(int_g atom_types[],
				     const int_g n) const = 0;
    virtual void get_asym_atom_regions(const char group[],
				       int_g regions[], const int_g n) const;
    virtual void get_asym_occupancy(real_g atom_occ[], const int_g n) const;
    virtual void set_asym_occupancy(real_g atom_occ[], const int_g n) const;
    virtual void get_asym_crystal03_ids(int_g atom_ids[], const int_g n) const;
    virtual void get_asym_core_electrons(int_g atom_ids[], const int_g n) const;
    virtual void get_asym_point_charge(real_l charge[], const int_g n) const;
    virtual void get_asym_isotope(int_g isotope[], const int_g n) const;
    virtual void get_asym_debye_waller1(real_g atom_ids[],
					const int_g n) const;  //not used
    virtual void get_asym_debye_waller2(real_g atom_ids[],
					const int_g n) const; //not used
    virtual void set_asym_debye_waller(real_g atom_dw1[], real_g atom_dw2[],
				       const int_g n) const; //not used
    virtual void get_asym_displacements(real_g atom_disps[][3],
					const int_g n) const;
    virtual void set_asym_displacements(real_g atom_disps[][3],
					const int_g n) const;
    virtual void get_asym_rod_dw1(int_g atom_ids[], const int_g n) const;
    virtual void get_asym_rod_dw2(int_g atom_ids[], const int_g n) const;
    virtual void get_asym_rod_noccup(int_g atom_ids[], const int_g n) const;
    virtual void set_asym_rod_dw1(int_g atom_ids[], const int_g n) const;
    virtual void set_asym_rod_dw2(int_g atom_ids[], const int_g n) const;
    virtual void set_asym_rod_noccup(int_g atom_ids[], const int_g n) const;
    virtual void get_asym_rod_xconst(real_g atom_ids[], const int_g n) const;
    virtual void get_asym_rod_nxdis(int_g atom_ids[], const int_g n) const;
    virtual void get_asym_rod_x2const(real_g atom_ids[], const int_g n) const;
    virtual void get_asym_rod_nx2dis(int_g atom_ids[], const int_g n) const;
    virtual void get_asym_rod_yconst(real_g atom_ids[], const int_g n) const;
    virtual void get_asym_rod_nydis(int_g atom_ids[], const int_g n) const;
    virtual void get_asym_rod_y2const(real_g atom_ids[], const int_g n) const;
    virtual void get_asym_rod_ny2dis(int_g atom_ids[], const int_g n) const;
    virtual void get_asym_rod_nzdis(int_g atom_ids[], const int_g n) const;
    virtual void get_asym_atom_spins(int_g atom_spins[], const int_g n) const;
    virtual void get_primitive_atom_types(int_g atom_types[],
					  const int_g n) const = 0;
    virtual void get_primitive_atom_regions(const char group[], int_g regions[],
					    const int_g n) const;
    virtual void set_indexed_asym_atom_type(const int_g id,
					    const int_g i) const;
    virtual void get_primitive_asym_indices(int_g indices[],
					    const int_g n) const;
    virtual void get_asym_to_primitive_indices(int_g indices[],
					       const int_g n) const;
    virtual void get_asym_atom_charges(int_g charges[], const int_g n) const;
    virtual void has_asym_atom_special_periodicity(bool special_periodicity[], 
						   const int_g n) const;
    virtual void get_asym_atom_cart_coords(coord_type coords[][3],
					   const int_g n) const = 0;
    virtual void get_asym_atom_frac_coords(coord_type coords[][3],
					   const int_g n) const = 0;
    virtual void get_primitive_atom_cart_coords(coord_type coords[][3],
						const int_g n) const = 0;
    virtual void get_primitive_atom_frac_coords(coord_type coords[][3],
						const int_g n) const = 0;
    virtual int_g get_atomic_number(const int_g index) const;
    virtual void get_atom_properties(const int_g index, int_g &c, real_g &r,
				     real_g &red, real_g &green, real_g &blue,
				     int_g &s, bool &use_charge,
				     bool &use_radius, bool &use_colour,
				     bool &use_spin) const;
    virtual void get_atom_shifts(const coord_type coords[][3],
				 int_g shifts[][3], int_g prim_id[],
				 int_g asym_id[], const int_g n) const;
    virtual void find_asym_pos(const int_g asym_idx, const coord_type a[3],
			       int_g &op, coord_type a_coords[3],
			       const coord_type b[][3],
			       const int_g b_asym_idx[],
			       coord_type b_coords[][3], int_g ab_shift[3],
			       const int_g nbatoms) const;
    virtual void map_atom_data(const real_g grid[][3], int_g **const data,
			       const int_g ndata, const int_g ngrid,
			       real_g (* &map_grid)[3], int_g ** &new_data,
			       int_g &n) const;
    virtual void map_atom_data(const real_g grid[][3], real_g **const data,
			       const int_g ndata, const int_g ngrid,
			       real_g (* &map_grid)[3], real_g ** &new_data,
			       int_g &n) const;
    virtual void map_atom_data(const real_g grid[][3], const real_g mags[][3],
			       const real_g phases[][3], const int_g ngrid,
			       real_g (* &map_grid)[3], real_g (* &new_data)[3],
			       int_g &n, const real_g ka, const real_g kb,
			       const real_g kc) const;
    virtual void map_atom_data(const real_g grid[][3], const real_g mags[][3],
			       const real_g phases[][3], const int_g ngrid,
			       std::vector<real_g (*)[3]> &new_data, int_g &n,
			       const int_g nframes, const int_g ncopies,
			       const real_g ka, const real_g kb,
			       const real_g kc, const real_g scale) const;
    virtual void map_atom_data(const real_g grid[][3], const int_g ngrid,
			       const std::vector<real_g (*)[3]> &traj,
			       const int_g nframes,
			       std::vector<real_g (*)[3]> &new_data) const;

    virtual void generate_transforms(const int_g na, const int_g nb,
				     const int_g nc, const int_g sa,
				     const int_g sb, const int_g sc,
				     const bool centre_cell,
				     const bool conventional,
				     int_g &ntransforms,
				     real_g (* &transforms)[3]) const = 0;

    virtual void get_reciprocal_lattice(coord_type a[3], coord_type b[3],
					coord_type c[3]) const = 0;
    virtual bool keep_symmetry() const;
    virtual void set_bond_all();
    virtual void add_atom_group(const string name);

#ifdef ENABLE_DLV_GRAPHICS
    virtual bool has_selected_atoms() const;
    virtual void list_atom_groupings(render_parent *p) const;

    virtual void set_edit_transform(const render_parent *parent,
				    const bool v, real_g m[4][4], real_g t[3],
				    real_g c[3], char message[],
				    const int_g mlen);
    virtual void activate_atom_flags(const string label, 
				     const bool reset_flags=true);
    virtual void deactivate_atom_flags();
    virtual void set_atom_flag(const int_g index, const atom_flag_type aft);
    virtual void toggle_atom_flags(const string label, 
				   const bool reset_flags=true);
    virtual bool set_atom_index_and_flags(int_g indices[], const int_g value,
					  const bool set_def, char message[],
					  const int_g mlen);
    virtual bool update_origin_atoms(const model *m, real_l &x, real_l &y,
				     real_l &z, const bool frac);

    virtual int_g get_atom_selection_info(real_l &x, real_l &y, real_l &z,
					real_l &length, real_l &x2, real_l &y2,
					real_l &z2, real_l &angle, char sym1[],
					char sym2[], char sym3[], real_g &r1,
					real_g &r2, real_g &r3, string &gp,
					string &label) const;
    virtual int_g get_number_selected_atoms() const;  
    virtual bool get_selected_positions(coord_type p[3]) const;
    virtual bool get_selected_positions(coord_type p1[3],
					coord_type p2[3]) const;
    virtual bool get_selected_positions(coord_type p1[3], coord_type p2[3],
					coord_type p3[3]) const;
    virtual bool get_average_selection_position(coord_type p[3]) const;
    virtual int_g locate_selected_atom() const;
    virtual int_g find_primitive_selection() const;
    virtual void get_displayed_cell(render_parent *p, int_g &na,
				    int_g &nb, int_g &nc, bool &conv,
				    bool &centre, bool &edges) const;
    virtual render_parent *
    create_render_parent(render_parent *p = 0,
			 const bool copy_cell = true,
			 const bool copy_wavefn = false) = 0;
    virtual DLVreturn_type render_r(render_parent *p, const model *m,
				    char message[], const int_g mlen) = 0;
    virtual DLVreturn_type render_k(render_parent *p,
				    char message[], const int_g mlen) = 0;
    virtual class toolkit_obj get_r_ui_obj() const = 0;
    virtual class toolkit_obj get_k_ui_obj() const;
    virtual int_g get_symmetry_selector() const;
    virtual void fix_symmetry_selector(const int_g s);
    virtual DLVreturn_type update_cell(render_parent *p,
				       char message[], const int_g mlen) = 0;
    virtual DLVreturn_type update_lattice(render_parent *p,
					  const bool kspace,
					  char message[], const int_g mlen) = 0;
    virtual DLVreturn_type update_atoms(const render_parent *p,
					char message[], const int_g mlen) = 0;
    virtual DLVreturn_type update_bonds(const render_parent *p,
					char message[],
					const int_g mlen) const = 0;
    virtual DLVreturn_type add_bond(render_parent *p,
				    char message[], const int_g mlen) = 0;
    virtual DLVreturn_type del_bond(render_parent *p,
				    char message[], const int_g mlen) = 0;
    virtual DLVreturn_type add_bond_type(render_parent *p,
					 char message[], const int_g mlen) = 0;
    virtual DLVreturn_type del_bond_type(render_parent *p,
					 char message[], const int_g mlen) = 0;
    virtual DLVreturn_type add_bond_all(render_parent *p,
					char message[], const int_g mlen) = 0;
    virtual DLVreturn_type del_bond_all(render_parent *p,
					char message[], const int_g mlen) = 0;
    virtual DLVreturn_type select_atom(render_parent *p,
				       const real_g coords[3],
				       const int_g status, int_g &nselects,
				       char message[], const int_g mlen) = 0;
    virtual DLVreturn_type deselect_all_atoms(render_parent *p,
					      char message[],
					      const int_g mlen) = 0;
    virtual DLVreturn_type update_outline(render_parent *p,
					  char message[], const int_g mlen);
    virtual DLVreturn_type update_shells(render_parent *p,
					 char message[], const int_g mlen);
    virtual void set_selected_atom_flags(const bool done);
    /*
    virtual bool map_atom_selections(int_g &n, int_g * &labels,
				     int_g * &atom_types, class atom * &parents,
				     const bool all_atoms,
				     const real_g grid[][3], int_g **const data,
				     const int_g ndata,
				     const int_g ngrid) const;
    */
    virtual bool map_selected_atoms(int_g &n, int_g * &labels,
				    int_g (* &shifts)[3],
				    const real_g grid[][3], int_g **const data,
				    const int_g ndata, const int_g ngrid) const;
    virtual void set_atom_data(const render_parent *p,
			       class data_object *d,
			       const bool undisplay = true);
    virtual void refresh_atom_data(const render_parent *p);
    virtual bool generate_wulff_plot(std::list<class wulff_data> &planes,
				     real_g (* &vertices)[3], int_g &nverts,
				     real_g (* &vert_cols)[3],
				     real_g (* &vert_norms)[3],
				     real_g (* &pos)[3], string * &names,
				     int_l * &connects, int_l &nconnects,
				     int_g * &nodes, int_g &nnodes,
				     real_g (* &line_verts)[3], int_g &nlv,
				     int_l * &lines, int_l &nlines) const;
    virtual void transform_atom(const model *m, const int_g atn, real_l &x,
				real_l &y, real_l &z, const bool keep,
				const real_g matrix[4][4],
				const real_g translate[3],
				const real_g centre[3]);
    virtual void transform_model(const model *m, const model *i,
				 real_l &x, real_l &y, real_l &z,
				 real_l &c_x, real_l &c_y, real_l &c_z,
				 const bool keep, const real_g matrix[4][4],
				 const real_g translate[3],
				 const real_g centre[3]);
    virtual void transform_move(model *m, real_l &x, real_l &y, real_l &z,
				real_l &c_x, real_l &c_y, real_l &c_z,
				real_l shift[3], const bool keep,
				const real_g matrix[4][4],
				const real_g translate[3],
				const real_g centre[3]);
#endif // ENABLE_DLV_GRAPHICS

  protected:
    model(const string n);


  private:
    string name;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_SERIALIZATION_ASSUME_ABSTRACT(DLV::model)
#endif // DLV_USES_SERIALIZE

inline void DLV::model::get_model_name(char mname[], const int_g n) const
{
  strncpy(mname, name.c_str(), n);
  mname[n - 1] = '\0';
}

inline DLV::string DLV::model::get_model_name() const
{
  return name;
}

#endif // DLV_MODEL
