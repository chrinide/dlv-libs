
#ifndef DLV_ATOM_DATA_EXPORTS
#define DLV_ATOM_DATA_EXPORTS

namespace DLV {

  DLVreturn_type wulff_crystal(const char name[], const real_l r,
			       const int selection, char message[],
			       const int_g mlen);
  DLVreturn_type wulff_update(const real_l r, char message[], const int_g mlen);

#ifdef ENABLE_DLV_GRAPHICS

  operation *edit_current_data(const int_g object, const int_g component,
			       const int_g method, char message[],
			       const int_g mlen);
  DLVreturn_type select_current_edit(const int_g object, char message[],
				     const int_g mlen);
  DLVreturn_type update_current_spectrum(const int_g object, const int_g type,
					 const real_g emin, const real_g emax,
					 const int_g np, const real_g w,
					 const real_g harmonics, char message[],
					 const int_g mlen);
  DLVreturn_type update_current_phonon(const int_g object, const int_g nframes,
				       const real_g scale, const bool use_temp,
				       const real_g temperature,
				       char message[], const int_g mlen);

#endif // ENABLE_DLV_GRAPHICS

}

#endif // DLV_ATOM_DATA_EXPORTS
