
#include <list>
#include <map>
#include <cstdio>
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/toolkit_obj.hxx"
#  include "../graphics/render_base.hxx"
#  include "../graphics/model_render.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "utils.hxx"
#include "symmetry.hxx"
#include "atom_model.hxx"
#include "atom_pairs.hxx"
#include "model.hxx"
#include "model_atoms.hxx"
#include "slab.hxx"
#include "surface.hxx"
#include "constants.hxx"  

// Kind of the dimensionality - for the User Interface menus.
DLV::int_g DLV::surface::get_model_type() const
{
  return 4;
}

DLV::model *DLV::create_surface(const string model_name)
{
  return new surface(model_name);
}

DLV::model_base *DLV::surface::duplicate(const string label) const
{
  surface *m = new surface(label);
  m->copy_atom_groups(this);
  return m;
}

void DLV::surface::get_frac_rotation_operators(real_l r[][3][3],
					       const int_g nops) const
{
  // Todo
  get_cart_rotation_operators(r, nops);
}

void DLV::surface::get_primitive_lattice(coord_type a[3], coord_type b[3],
					 coord_type c[3]) const
{
  coord_type l[3][3];
  get_primitive_vectors(l);
  a[0] = l[0][0];
  a[1] = l[0][1];
  a[2] = l[0][2];
  b[0] = l[1][0];
  b[1] = l[1][1];
  b[2] = l[1][2];
  c[0] = bulk_c[0];
  c[1] = bulk_c[1];
  c[2] = bulk_c[2];
}

void DLV::surface::get_conventional_lattice(coord_type a[3], coord_type b[3],
					    coord_type c[3]) const
{
  get_primitive_lattice(a, b, c);
  coord_type t[3][3];
  get_transform_to_conv_cell(t);
  coord_type conv[3][3];
  for (int_g i = 0; i < 2; i++)
    for (int_g j = 0; j < 2; j++)
      conv[i][j] = t[i][0] * a[j] + t[i][1] * b[j];
  for (int_g i = 0; i < 2; i++) {
    a[i] = conv[0][i];
    b[i] = conv[1][i];
  }
}

bool DLV::surface::set_primitive_lattice(const coord_type a[3],
					 const coord_type b[3],
					 const coord_type c[3])
{
  bulk_c[0] = c[0];
  bulk_c[1] = c[1];
  bulk_c[2] = c[2];
  return set_lattice(a, b);
}

bool DLV::surface::set_lattice_parameters(const coord_type a,
					  const coord_type b,
					  const coord_type c,
					  const coord_type alpha,
					  const coord_type beta,
					  const coord_type gamma)
{
  return set_parameters(a, b, c, alpha, beta, gamma);
}

void DLV::surface::get_lattice_parameters(coord_type &a, coord_type &b,
				       coord_type &c, coord_type &alpha,
					  coord_type &beta, coord_type &gamma,
					  const bool conventional) const
{
  if (conventional)
    get_parameters(a, b, c, alpha, beta, gamma);
  else
    create_primitive_params(a, b, c, alpha, beta, gamma);
}


void DLV::surface::complete_lattice(coord_type va[3], coord_type vb[3],
				    coord_type vc[3])
{
  const real_l tol = 0.01;
  if (has_valid_parameters() and !has_valid_vectors()) {
    coord_type a;
    coord_type b;
    coord_type c;
    coord_type alpha;
    coord_type beta;
    coord_type gamma;
    get_parameters(a, b, c, alpha, beta, gamma);
    switch (get_lattice_type()) {
    case 1:
      // We should a able to assume these are correct for the 'unique axis'
      // Do we only need > 90.0 + tol?
      if ((abs(alpha) > tol and abs(alpha) < real_l(90.0) - tol) or
	  abs(alpha) > real_l(90.0) + tol) { 
	beta = 90.0;
	gamma = 90.0;
      } else if ((abs(gamma) > tol and abs(gamma) < real_l(90.0) - tol) or
		 abs(gamma) > real_l(90.0) + tol) {
	alpha = 90.0;
	beta = 90.0;
      } else {
	alpha = 90.0;
	gamma = 90.0;
      }
      break;
    case 2:
      alpha = 90.0;
      beta = 90.0;
      gamma = 90.0;
      break;
    case 3:
      b = a;
      alpha = 90.0;
      beta = 90.0;
      gamma = 90.0;
      break;
    case 4:
      if ((abs(gamma) > real_l(120.0) - tol and
	   abs(gamma) < real_l(120.0) + tol) or abs(alpha) < tol) {
	// Should have been a hexagonal setting
	b = a;
	alpha = 90.0;
	beta = 90.0;
	gamma = 120.0;
      } else {
	b = a;
	c = a;
	beta = alpha;
	gamma = alpha;
      }
      break;
    case 5:
      b = a;
      alpha = 90.0;
      beta = 90.0;
      gamma = 120.0;
      break;
    case 6:
      b = a;
      c = a;
      alpha = 90.0;
      beta = 90.0;
      gamma = 90.0;
      break;
    default:
      break;
    }
    set_parameters(a, b, c, alpha, beta, gamma);
    //    create_lattice_vectors(a, b, c, alpha, beta, gamma, va, vb, vc);
    //    set_lattice(va, vb, vc);
    create_lattice_vectors(a, b, c, alpha, beta, gamma, va, vb, bulk_c);
    set_lattice(va, vb, bulk_c);
    set_valid_vectors();
    set_valid_parameters();
  } else if (has_valid_vectors() and !has_valid_parameters()) {
      //need to sort still
    coord_type al[3];
    coord_type bl[3];
    coord_type cl[3];
    get_conventional_lattice(al, bl, cl);
    coord_type a = sqrt(al[0] * al[0] + al[1] * al[1] + al[2] * al[2]);
    coord_type b = sqrt(bl[0] * bl[0] + bl[1] * bl[1] + bl[2] * bl[2]);
    coord_type c = sqrt(cl[0] * cl[0] + cl[1] * cl[1] + cl[2] * cl[2]);
    coord_type alpha = to_degrees(acos((bl[0] * cl[0] + bl[1] * cl[1]
					+ bl[2] * cl[2]) / (b * c)));
    coord_type beta  = to_degrees(acos((al[0] * cl[0] + al[1] * cl[1]
					+ al[2] * cl[2]) / (a * c)));
    coord_type gamma = to_degrees(acos((al[0] * bl[0] + al[1] * bl[1]
					+ al[2] * bl[2]) / (a * b)));
    set_parameters(a, b, c, alpha, beta, gamma);
    set_valid_parameters();
    set_valid_vectors();
    get_primitive_lattice(va, vb, bulk_c);
  }
  else if (has_valid_parameters() and has_valid_vectors())
    get_primitive_lattice(va, vb, bulk_c);
}

void DLV::surface::create_primitive_params(coord_type &a, coord_type &b,
					   coord_type &c, coord_type &alpha,
					   coord_type &beta,
					   coord_type &gamma) const
{
  coord_type al[3];
  coord_type bl[3];
  coord_type cl[3];
  get_primitive_lattice(al, bl, cl);
  a = sqrt(al[0] * al[0] + al[1] * al[1] + al[2] * al[2]);
  b = sqrt(bl[0] * bl[0] + bl[1] * bl[1] + bl[2] * bl[2]);
  c = sqrt(bulk_c[0] * bulk_c[0] + bulk_c[1] * bulk_c[1]
	   + bulk_c[2] * bulk_c[2]);
  alpha = to_degrees(acos((bl[0] * bulk_c[0] + bl[1] * bulk_c[1]
			   + bl[2] * bulk_c[2]) / (b * c)));
  beta  = to_degrees(acos((al[0] * bulk_c[0] + al[1] * bulk_c[1]
			   + al[2] * bulk_c[2]) / (a * c)));
  gamma = to_degrees(acos((al[0] * bl[0] + al[1] * bl[1]
			   + al[2] * bl[2]) / (a * b)));
}

void DLV::surface::create_lattice_vectors(const coord_type a,
					  const coord_type b,
					  const coord_type c,
					  const coord_type alpha,
					  const coord_type beta,
					  const coord_type gamma,
					  coord_type va[3],
					  coord_type vb[3],
					  coord_type vc[3]) const
{
  const real_l tol = 0.01;
  real_l scale[3];
  scale[0] = a;
  scale[1] = b;
  scale[2] = c;
  real_l calpha = alpha;
  real_l cbeta = beta;
  real_l cgamma = gamma;
  // Create unit conventional cell
  real_l ma[3][3] = {{ 1.0, 0.0, 0.0 }, { 0.0, 1.0, 0.0 }, { 0.0, 0.0, 1.0 }};
  real_l xalpha = cos(to_radians(calpha));
  real_l gg;
  switch (get_lattice_type()) {
  case 0:
  case 1:
    gg = to_radians(cgamma);
    ma[0][0] = sin(gg);
    ma[0][1] = cos(gg);
    ma[2][0] = (cos(to_radians(cbeta)) - ma[0][1] * xalpha) / ma[0][0];
    ma[2][1] = xalpha;
    ma[2][2] = sqrt(real_l(1.0) - ma[2][0] * ma[2][0] - xalpha * xalpha);
    break;
  case 4:
    if (abs(gamma) < real_l(120.0) - tol or abs(gamma) > real_l(120.0) + tol) {
      // calculate rhombohedral metric
      real_l gr[3][3];
      gg = scale[0] * scale[0];
      xalpha *= gg;
      for (int_g i = 0; i < 3; i++) {
        for (int_g j = 0; j < 3; j++)
          gr[i][j] = xalpha;
        gr[i][i] = gg;
      }
      // Transform to hex cell
      coord_type winv[3][3];
      get_transform_to_conv_cell(winv);
      real_l s[3][3];
      for (int_g i = 0; i < 3; i++)
	for (int_g j = 0; j < 3; j++)
	  s[i][j] = winv[i][0] * gr[0][j] + winv[i][1] * gr[1][j]
	    + winv[i][2] * gr[2][j];
      real_l gh[3][3];
      for (int_g i = 0; i < 3; i++)
	for (int_g j = 0; j < 3; j++)
	  gh[i][j] = s[i][0] * winv[j][0] + s[i][1] * winv[j][1]
	    + s[i][2] * winv[j][2];
      scale[0] = sqrt(gh[0][0]);
      scale[2] = sqrt(gh[2][2]);
      scale[1] = scale[0];
    }
    ma[0][1] = -0.5;
    ma[0][0] = std::sqrt(0.75);
    break;
  case 5:
    ma[0][1] = -0.5;
    ma[0][0] = std::sqrt(0.75);
    break;
  default:
    break;
  }
  // scale to actual conventional cell
  for (int_g i = 0; i < 3; i++) {
    real_l p = scale[i];
    for (int_g j = 0; j < 3; j++)
      ma[i][j] *= p;
  }
  coord_type w[3][3];
  // Convert cell to primitive
  get_transform_to_prim_cell(w);
  for (int_g i = 0; i < 3; i++) {
    va[i] = w[0][0] * ma[0][i] + w[0][1] * ma[1][i] + w[0][2] * ma[2][i];
    vb[i] = w[1][0] * ma[0][i] + w[1][1] * ma[1][i] + w[1][2] * ma[2][i];
    vc[i] = w[2][0] * ma[0][i] + w[2][1] * ma[1][i] + w[2][2] * ma[2][i];
  }
}


void DLV::surface::copy_lattice(const model_base *m)
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  m->get_primitive_lattice(a, b, c);
  set_primitive_lattice(a, b, c);
  coord_type a1;
  coord_type b1;
  coord_type c1;
  coord_type alpha;
  coord_type beta;
  coord_type gamma;
  m->get_lattice_parameters(a1, b1, c1, alpha, beta, gamma);
  set_parameters(a1, b1, c1, alpha, beta, gamma);
  set_valid_vectors();
}

DLV::int_g DLV::surface::get_salvage_layers() const
{
  return salvage_layers;
}

void DLV::surface::set_salvage_layers(const int_g l)
{
  salvage_layers = l;
}

/* I think the slab versions of these should be ok
void DLV::surface::generate_primitive_atoms(const bool tidy)
{
  fprintf(stderr, "surface Todo - check gen primitive atoms\n");
  slab::generate_primitive_atoms(tidy);
}

void DLV::surface::conventional_atoms(const atom_tree &primitive,
				      atom_tree &tree, const bool centre[3]) const
{
  fprintf(stderr, "surface Todo - check convetional atoms\n");
  slab::conventional_atoms(primitive, tree, centre);
}

void DLV::surface::centre_atoms(const atom_tree &primitive, atom_tree &tree,
				const bool centre[3]) const
{
  fprintf(stderr, "surface Todo - check centre atoms\n");
  slab::centre_atoms(primitive, tree, centre);
}
*/

void DLV::surface::update_display_atoms(atom_tree &tree, const real_l tol,
					const int_g na, const int_g nb,
					const int_g nc, const bool conventional,
					const bool centre, const bool edges) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  if (conventional)
    get_conventional_lattice(a, b, c);
  else
    get_primitive_lattice(a, b, c);
  tree.replicate_surface(a, b, bulk_c, tol, na, nb, nc, centre, edges);
}

void DLV::surface::generate_bonds(const atom_tree &tree, const int_g na,
				  const int_g nb, const int_g nc,
				  const bool conventional,
				  std::list<bond_list> info[],
				  const real_g overlap, const bool centre,
				  const bool invert_colours,
				  const bond_data &bond_info, const real_l tol,
				  const std::vector<real_g (*)[3]> *traj,
				  const int_g nframes, const bool gamma,
				  const bool edit) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  if (conventional)
    get_conventional_lattice(a, b, c);
  else
    get_primitive_lattice(a, b, c);
  tree.surface_bonds(a, b, bulk_c, na, nb, nc, info, nframes, overlap, centre,
		     invert_colours, bond_info, tol, traj, gamma, edit);
}

void DLV::surface::find_asym_pos(const int_g asym_idx, const coord_type a[3],
				 int_g &op, coord_type a_coords[3],
				 const coord_type b[][3],
				 const int_g b_asym_idx[],
				 coord_type b_coords[][3], int_g ab_shift[3],
				 const int_g nbatoms) const
{
  // only designed for i, j to i, j, k, l
  if (nbatoms < 1 or nbatoms > 3)
    exit(1);
  int_g nops = get_number_of_sym_ops();
  real_l (*rotations)[3][3] = new_local_array3(real_l, nops, 3, 3);
  real_l (*translations)[3] = new_local_array2(real_l, nops, 3);
  get_cartesian_ops(rotations, translations, nops);
  coord_type la[3];
  coord_type lb[3];
  coord_type lc[3];
  get_primitive_lattice(la, lb, lc);
  lc[0] = -bulk_c[0];
  lc[1] = -bulk_c[1];
  lc[2] = -bulk_c[2];
  map_asym_pos(asym_idx, a, op, a_coords, b, b_asym_idx, b_coords, ab_shift,
	       nbatoms, la, lb, lc, rotations, translations, nops, true);
  delete_local_array(translations);
  delete_local_array(rotations);
}

void DLV::surface::get_atom_shifts(const atom_tree &tree,
				   const coord_type coords[][3],
				   int_g shifts[][3], int_g prim_id[],
				   const int_g n) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  // periodic atoms are drawn below 0, so shift down
  c[0] = -bulk_c[0];
  c[1] = -bulk_c[1];
  c[2] = -bulk_c[2];
  // A salvage that hasn't been distorted or had atoms inserted will
  // probably identify the periodic atoms rather than the non-periodic ones.
  // Should be ok for shifted ones though.
  tree.get_atom_shifts(coords, shifts, prim_id, n, a, b, c, true);
  // change sign to compensate for vector direction change
  for (int_g i = 0; i < n; i++)
    shifts[i][2] = - shifts[i][2];
}

void DLV::surface::map_data(const atom_tree &tree, const real_g grid[][3],
			    int_g **const data, const int_g ndata,
			    const int_g ngrid, real_g (* &map_grid)[3],
			    int_g ** &map_data, int_g &n) const
{
  fprintf(stderr, "surface Todo - check map data\n");
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  // need to combine 2d and 3d methods
  // maybe add an extra periodicity value
  tree.map_data(grid, data, ndata, ngrid, map_grid, map_data, n, a, b);
}

void DLV::surface::map_data(const atom_tree &tree, const real_g grid[][3],
			    real_g **const data, const int_g ndata,
			    const int_g ngrid, real_g (* &map_grid)[3],
			    real_g ** &map_data, int_g &n) const
{
  fprintf(stderr, "surface Todo - check map data\n");
}

void DLV::surface::map_data(const atom_tree &tree, const real_g grid[][3],
			    const real_g mag[][3], const real_g phases[][3],
			    const int_g ngrid, real_g (* &map_grid)[3],
			    real_g (* &map_data)[3], int_g &n, const real_g ka,
			    const real_g kb, const real_g kc) const
{
  fprintf(stderr, "surface Todo - check map data\n");
}

void DLV::surface::map_data(const atom_tree &tree, const real_g grid[][3],
			    const real_g mags[][3], const real_g phases[][3],
			    const int_g ngrid,
			    std::vector<real_g (*)[3]> &new_data, int_g &n,
			    const int_g nframes, const int_g ncopies,
			    const real_g ka, const real_g kb, const real_g kc,
			    const real_g scale) const
{
  fprintf(stderr, "surface Todo - check map data\n");
}

bool DLV::surface::map_atom_selections(const atom_tree &display,
				       const atom_tree &prim,
				       const real_g grid[][3],
				       int_g **const data,
				       const int_g ndata, const int_g ngrid,
				       int_g * &labels, int_g * &atom_types,
				       atom * &parents, int_g &n,
				       const bool all_atoms) const
{
  fprintf(stderr, "surface Todo - check map atom selections\n");
  return false;
}

void DLV::surface::build_supercell_symmetry(const model *m,
					    const coord_type a[3],
					    const coord_type b[3],
					    const coord_type c[3],
					    const coord_type va[3],
					    const coord_type vb[3],
					    const coord_type vc[3],
					    const bool conv)
{
  // Seems ok, could remove this routine.
  //fprintf(stderr, "surface Todo - check build supercell\n");
  slab::build_supercell_symmetry(m, a, b, c, va, vb, vc, conv);
}

#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::surface *t,
				    const unsigned int file_version)
    {
      DLV::string n = t->get_model_name();
      ar << n;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::surface *t,
				    const unsigned int file_version)
    {
      DLV::string n;
      ar >> n;
      ::new(t)DLV::surface(n);
    }

  }
}

template <class Archive>
void DLV::surface::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<slab>(*this);
  ar & bulk_c;
  ar & salvage_layers;
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::surface)

DLV_SUPPRESS_TEMPLATES(DLV::slab)

DLV_NORMAL_EXPLICIT(DLV::surface)

#endif // DLV_USES_SERIALIZE
