
#ifndef DLV_OP_ATOM_EDITS
#define DLV_OP_ATOM_EDITS

namespace DLV {

  class supercell : public edit_geometry {
  public:
    supercell(class model *m, const int_g na, const int_g nb, const int_g nc,
	      const bool conv);
    static DLVreturn_type create(const char name[], int_g &na, int_g &nb,
				 int_g &nc, bool &conv, char message[],
				 const int_g mlen);
    DLVreturn_type update_supercell(const model *parent, const int_g m[3][3],
				    const bool conv, char message[],
				    const int_g mlen);

  protected:
    string get_name() const;
    bool is_supercell() const;
    bool copy_cell_replicate() const;
    void get_supercell(int_g supercell[2][2]) const;

  private:
    int_g matrix[3][3];
    bool conventional_cell;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class del_symmetry : public edit_geometry {
  public:
    del_symmetry(class model *m);
    static DLVreturn_type create(const char name[], char message[],
				 const int_g mlen);

  protected:
    string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class molecule_view : public edit_geometry {
  public:
    molecule_view(class model *m, const int_g a, const int_g b, const int_g c,
		  const bool conv, const bool centre, const bool cell_edges);
    static DLVreturn_type create(const char name[], char message[],
				 const int_g mlen);

  protected:
    string get_name() const;

  private:
    int_g na;
    int_g nb;
    int_g nc;
    bool conventional;
    bool centred;
    bool edges;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class remove_lattice : public edit_geometry {
  public:
    remove_lattice(class model *m, const int_g a, const int_g b, const int_g c,
		   const bool conv, const bool centre, const bool cell_edges);
    static DLVreturn_type create(const char name[], const int_g na,
				 const int_g nb, const int_g nc, const bool conv,
				 const bool centre, const bool edges,
				 char message[], const int_g mlen);

  protected:
    string get_name() const;

  private:
    int_g na;
    int_g nb;
    int_g nc;
    bool conventional;
    bool centred;
    bool edges;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class alter_lattice : public edit_geometry {
  public:
    alter_lattice(class model *m, const coord_type pa, const coord_type pb,
		  const coord_type pc, const coord_type palpha,
		  const coord_type pbeta, const coord_type pgamma);
    
    static DLVreturn_type create(const char name[], real_l values[6],
				 bool usage[6], char message[],
				 const int_g mlen);
    DLVreturn_type update_lattice(const model *parent, const real_l values[6],
				  char message[], const int_g mlen);

  protected:
    string get_name() const;

  private:
    coord_type a;
    coord_type b;
    coord_type c;
    coord_type alpha;
    coord_type beta;
    coord_type gamma;
    // Todo - do I need these, since they are included in the model?

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class origin_shift : public edit_geometry {
  public:
    origin_shift(class model *m);

    static DLVreturn_type create(const char name[], char message[],
				 const int_g mlen);
    DLVreturn_type update_origin(const model *parent, const real_l x,
				 const real_l y, const real_l z,
				 const bool frac, char message[],
				 const int_g mlen);
    DLVreturn_type update_origin_symm(const model *parent, real_l &x,
				      real_l &y, real_l &z, const bool frac,
				      char message[], const int_g mlen);
#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type update_origin_atoms(const model *parent, real_l &x,
				       real_l &y, real_l &z, const bool frac,
				       char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  protected:
    string get_name() const;

  private:
    real_l sx;
    real_l sy;
    real_l sz;
    bool fractional;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class vacuum_cell : public edit_geometry {
  public:
    vacuum_cell(class model *m);

    static DLVreturn_type create(const char name[], const real_l c,
				 const real_l origin, char message[],
				 const int_g mlen);
    DLVreturn_type update_vacuum(const model *parent, const real_l c,
				 const real_l origin, char message[],
				 const int_g mlen);

  protected:
    string get_name() const;

  private:
    real_l c_axis;
    real_l slab_origin;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class cluster : public edit_geometry {
  public:
    cluster(class model *m, const int_g n);

    static DLVreturn_type create(const char name[], int_g &n, char message[],
				 const int_g mlen);
    static DLVreturn_type create(const char name[], int_g &n, const real_l x,
				 const real_l y, const real_l z,
				 char message[], const int_g mlen);
    DLVreturn_type update_cluster(model *parent, const int_g n,
				  char message[], const int_g mlen);
    DLVreturn_type update_cluster(model *parent, const int_g n, const real_l x,
				  const real_l y, const real_l z,
				  char message[], const int_g mlen);
    DLVreturn_type update_cluster(model *parent, const real_l r,
				  char message[], const int_g mlen);

  protected:
    string get_name() const;
    bool get_lattice_rotation(real_l r[3][3]) const;

  private:
    int_g neighbours;
    real_l radius;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class slab_from_bulk : public edit_geometry {
  public:
    slab_from_bulk(class model *m);

    static DLVreturn_type create(const char name[], int_g &nlayers,
				 string * &names, int_g &n, char message[],
				 const int_g mlen);
    static DLVreturn_type create(const char name[], const int_g h,
				 const int_g k, const int_g l,
				 const bool conventional,
				 const real_g tol,int_g &nlayers,
				 string * &names, int_g &n,
				 char message[], const int_g mlen);
    DLVreturn_type update_slab(model *parent, const int_g h, const int_g k,
			       const int_g l, const bool conventional,
			       const real_g tol, int_g &nlayers,
			       string * &labels, int_g &n,
			       char message[], const int_g mlen);
    DLVreturn_type update_slab(model *parent, const real_g tol, int_g &nlayers,
			       string * &labels, int_g &n, char message[],
			       const int_g mlen);
    DLVreturn_type update_slab_term(model *parent, const int_g term,
				    char message[], const int_g mlen);
    DLVreturn_type update_slab(model *parent, const int_g nlayers,
			       char message[], const int_g mlen);

  protected:
    string get_name() const;
    bool get_lattice_rotation(real_l r[3][3]) const;

  private:
    model *crystal3d;
    int_g miller_h;
    int_g miller_k;
    int_g miller_l;
    bool conventional_cell;
    int_g termination;
    real_g layer_tolerance;
    int_g number_of_layers;
    int_g hkl_layers;
    real_l r[3][3];

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class slab_from_surface : public edit_geometry {
  public:
    slab_from_surface(class model *m);

    static DLVreturn_type create(const char name[], int_g &nlayers,
				 string * &names, int_g &n, char message[],
				 const int_g mlen);
    DLVreturn_type update_slab(model *parent, const int_g h, const int_g k,
			       const int_g l, const bool conventional,
			       const real_g tol, int_g &nlayers,
			       string * &labels, int_g &n,
			       char message[], const int_g mlen);
    DLVreturn_type update_slab(model *parent, const real_g tol, int_g &nlayers,
			       string * &labels, int_g &n, char message[],
			       const int_g mlen);
    DLVreturn_type update_slab(model *parent, const int_g nlayers,
			       char message[], const int_g mlen);

  protected:
    string get_name() const;

  private:
    real_g layer_tolerance;
    int_g number_of_layers;
    int_g hkl_layers;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class surface_edit : public edit_geometry {
  public:
    surface_edit(class model *m);

    static DLVreturn_type create(const char name[], string * &names, int_g &n,
				 char message[], const int_g mlen);
    static DLVreturn_type create(const char name[], const int_g h,
				 const int_g k, const int_g l,
				 const bool conventional, const real_g tol,
				 string * &names, int_g &n, char message[],
				 const int_g mlen);
    DLVreturn_type update_surface(model *parent, const int_g h, const int_g k,
				  const int_g l, const bool conventional,
				  const real_g tol, string * &labels,
				  int_g &n, char message[], const int_g mlen);
    DLVreturn_type update_surface(model *parent, const real_g tol,
				  string * &labels, int_g &n,
				  char message[], const int_g mlen);
    DLVreturn_type update_surface(model *parent, const int_g term,
				  char message[], const int_g mlen);

  protected:
    string get_name() const;
    bool is_cut_surface() const;
    bool get_lattice_rotation(real_l r[3][3]) const;

  private:
    model *crystal3d;
    int_g miller_h;
    int_g miller_k;
    int_g miller_l;
    bool conventional_cell;
    int_g termination;
    real_g layer_tolerance;
    real_l r[3][3];

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class salvage : public edit_geometry {
  public:
    salvage(class model *m);

    static DLVreturn_type create(const char name[], int_g &nlayers,
				 int_g &min_layers, char message[],
				 const int_g mlen);
    DLVreturn_type update_salvage(model *parent, const real_g tol,
				  char message[], const int_g mlen);
    DLVreturn_type update_salvage(model *parent, const int_g nlayers,
				  char message[], const int_g mlen);

  protected:
    string get_name() const;

  private:
    real_g layer_tolerance;
    int_g number_of_layers;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class wulff : public edit_geometry {
  public:
    wulff(class model *m, const real_l r, data_object *data);

    static DLVreturn_type create(const char name[], const real_l r,
				 const int selection, char message[],
				 const int_g mlen);
    DLVreturn_type wulff_update(model *parent, const real_l r,
				char message[], const int_g mlen);

  protected:
    string get_name() const;
    bool get_lattice_rotation(real_l r[3][3]) const;

  private:
    real_l scale;
    data_object *crystal;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class multi_layer : public edit_geometry {
  public:
    multi_layer(class model *m, const real_l r, class model *s);

    static DLVreturn_type create(const char name[], const real_l r,
				 const int selection, char message[],
				 const int_g mlen);
    DLVreturn_type multilayer_update(model *parent, const real_l r,
				     char message[], const int_g mlen);

  protected:
    string get_name() const;

  private:
    real_l space;
    model *slab;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class fill_geometry : public edit_geometry {
  public:
    fill_geometry(class model *m, class model *s);

    static DLVreturn_type create(const char name[], const int selection,
				 char message[], const int_g mlen);
    DLVreturn_type geometry_update(model *parent, char message[],
				   const int_g mlen);

  protected:
    string get_name() const;
    bool get_lattice_rotation(real_l r[3][3]) const;

  private:
    model *geometry;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

#ifdef ENABLE_DLV_GRAPHICS

  class atom_edit : public edit_geometry {
  public:
    atom_edit(class model *m, const int_g i);

    static DLVreturn_type create(const char name[], int_g &atn, const bool all,
				 char message[], const int_g mlen);
    DLVreturn_type update_atom(model *parent, const int_g atn, const bool all,
			       char message[], const int_g mlen);

  protected:
    string get_name() const;

  private:
    int_g index;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  // Todo - change inheritence?
  class atom_props : public edit_geometry {
  public:
    atom_props(class model *m, const int_g i);

    bool is_geometry() const;
    static DLVreturn_type create(const char name[], int_g &charge,
				 real_g &radius, real_g &red, real_g &green,
				 real_g &blue, int_g &spin, bool &use_charge,
				 bool &use_radius, bool &use_colour,
				 bool &use_spin, const bool all,
				 char message[], const int_g mlen);
    DLVreturn_type update_props(model *parent, const int_g charge,
				real_g &radius, const real_g red,
				const real_g green, const real_g blue,
				const int_g spin, const bool use_charge,
				const bool use_radius, const bool use_colour,
				const bool use_spin, const bool all,
				char message[], const int_g mlen);
    static DLVreturn_type update_rod_props(const int_g dw1, const bool use_dw1,
					   const int_g dw2, const bool use_dw2,
					   const int_g occ, const bool use_occ,
					   const bool all, char message[],
					   const int_g mlen);
    DLVreturn_type update_rod_props(model *parent, 
				    const int_g dw1, const bool use_dw1, 
				    const int_g dw2, const bool use_dw2,
				    const int_g occ, const bool use_occ,
				    const bool all,
				    char message[], const int_g mlen);
  protected:
    string get_name() const;
    bool is_property_change() const;

  private:
    int_g index;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class atom_delete : public edit_geometry {
  public:
    atom_delete(class model *m);

    static DLVreturn_type create(const char name[], char message[],
				 const int_g mlen);
    DLVreturn_type delete_atoms(model *parent, char message[],
				const int_g mlen);

  protected:
    string get_name() const;

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class atom_insert : public edit_geometry {
  public:
    atom_insert(class model *m, const bool sym);

    static DLVreturn_type create(const char name[], char message[],
				 const int_g mlen);
    DLVreturn_type insert_atom(const model *parent, const int_g atn,
			       const real_l x, const real_l y, const real_l z,
			       const bool frac, char message[],
			       const int_g mlen);
    DLVreturn_type switch_coords(const bool frac, const bool displace,
				 real_l &x, real_l &y, real_l &z,
				 char message[], const int_g mlen);
    DLVreturn_type use_selections(model *parent, real_l &x, real_l &y,
				  real_l &z, char message[], const int_g mlen);
    DLVreturn_type transform_editor(model *parent, const bool v,
				    real_l &x, real_l &y, real_l &z,
				    char message[], const int_g mlen);

  protected:
    string get_name() const;

  private:
    bool keep_sym;
    int_g atomic_number;
    real_l x_p;
    real_l y_p;
    real_l z_p;
    bool fractional;
    bool editor;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  // Todo - common code with atom_insert?
  class model_insert : public edit_geometry {
  public:
    model_insert(class model *m, class model *i, const bool sym);

    static DLVreturn_type create(const char name[], const int_g selection,
				 char message[], const int_g mlen);
    DLVreturn_type insert_model(const model *parent, const real_l x,
				const real_l y, const real_l z,
				const bool frac, char message[],
				const int_g mlen);
    DLVreturn_type switch_coords(const bool frac, const bool displace,
				 real_l &x, real_l &y, real_l &z,
				 char message[], const int_g mlen);
    DLVreturn_type use_selections(model *parent, real_l &x, real_l &y,
				  real_l &z, char message[], const int_g mlen);
    DLVreturn_type centre_from_selects(const model *parent, real_l &x,
				       real_l &y, real_l &z, char message[],
				       const int_g mlen);
    DLVreturn_type transform_editor(model *parent, const bool v,
				    real_l &x, real_l &y, real_l &z,
				    char message[], const int_g mlen);

  protected:
    string get_name() const;

  private:
    model *insert;
    bool keep_sym;
    real_l x_p;
    real_l y_p;
    real_l z_p;
    real_l x_c;
    real_l y_c;
    real_l z_c;
    bool fractional;
    bool editor;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class atom_move : public edit_geometry {
  public:
    atom_move(class model *m, const bool sym, const real_l ax,
	      const real_l ay, const real_l az);

    static DLVreturn_type create(const char name[], real_l &x, real_l &y,
				 real_l &z, char message[], const int_g mlen);
    DLVreturn_type move_atoms(model *parent, const real_l x,
			      const real_l y, const real_l z,
			      const bool frac, const bool displace,
			      char message[], const int_g mlen);
    DLVreturn_type switch_coords(const bool frac, const bool displace,
				 real_l &x, real_l &y, real_l &z,
				 char message[], const int_g mlen);
    DLVreturn_type use_selections(model *parent, real_l &x, real_l &y,
				  real_l &z, char message[], const int_g mlen);
    DLVreturn_type centre_from_selects(const model *parent, real_l &x,
				       real_l &y, real_l &z, char message[],
				       const int_g mlen);
    DLVreturn_type transform_editor(model *parent, const bool v,
				    real_l &x, real_l &y, real_l &z,
				    char message[], const int_g mlen);

  protected:
    string get_name() const;

  private:
    bool keep_sym;
    real_l x_p;
    real_l y_p;
    real_l z_p;
    real_l x_c;
    real_l y_c;
    real_l z_c;
    real_l shift[3];
    bool displacement;
    bool fractional;
    bool editor;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

#endif // ENABLE_DLV_GRAPHICS

}

#ifdef DLV_USES_SERIALIZE

BOOST_CLASS_EXPORT_KEY(DLV::supercell)
BOOST_CLASS_EXPORT_KEY(DLV::del_symmetry)
BOOST_CLASS_EXPORT_KEY(DLV::molecule_view)
BOOST_CLASS_EXPORT_KEY(DLV::remove_lattice)
BOOST_CLASS_EXPORT_KEY(DLV::alter_lattice)
BOOST_CLASS_EXPORT_KEY(DLV::origin_shift)
BOOST_CLASS_EXPORT_KEY(DLV::vacuum_cell)
BOOST_CLASS_EXPORT_KEY(DLV::cluster)
BOOST_CLASS_EXPORT_KEY(DLV::slab_from_bulk)
BOOST_CLASS_EXPORT_KEY(DLV::slab_from_surface)
BOOST_CLASS_EXPORT_KEY(DLV::surface_edit)
BOOST_CLASS_EXPORT_KEY(DLV::salvage)
BOOST_CLASS_EXPORT_KEY(DLV::wulff)
BOOST_CLASS_EXPORT_KEY(DLV::multi_layer)
BOOST_CLASS_EXPORT_KEY(DLV::fill_geometry)

#  ifdef ENABLE_DLV_GRAPHICS
BOOST_CLASS_EXPORT_KEY(DLV::atom_edit)
BOOST_CLASS_EXPORT_KEY(DLV::atom_props)
BOOST_CLASS_EXPORT_KEY(DLV::atom_delete)
BOOST_CLASS_EXPORT_KEY(DLV::atom_insert)
BOOST_CLASS_EXPORT_KEY(DLV::model_insert)
BOOST_CLASS_EXPORT_KEY(DLV::atom_move)
#  endif // ENABLE_DLV_GRAPHICS

#endif // DLV_USES_SERIALIZE

inline DLV::supercell::supercell(class model *m, const int_g na, const int_g nb,
				 const int_g nc, const bool conv)
  : edit_geometry(m), conventional_cell(conv)
{
  matrix[0][0] = na;
  matrix[0][1] = 0;
  matrix[0][2] = 0;
  matrix[1][0] = 0;
  matrix[1][1] = nb;
  matrix[1][2] = 0;
  matrix[2][0] = 0;
  matrix[2][1] = 0;
  matrix[2][2] = nc;
}

inline DLV::del_symmetry::del_symmetry(class model *m) : edit_geometry(m)
{
}

inline DLV::molecule_view::molecule_view(class model *m, const int_g a,
					 const int_g b, const int_g c,
					 const bool conv, const bool centre,
					 const bool cell_edges)
  : edit_geometry(m), na(a), nb(b), nc(c), conventional(conv), centred(centre),
    edges(cell_edges)
{
}

inline DLV::remove_lattice::remove_lattice(class model *m, const int_g a,
					   const int_g b, const int_g c,
					   const bool conv, const bool centre,
					   const bool cell_edges)
  : edit_geometry(m), na(a), nb(b), nc(c), conventional(conv), centred(centre),
    edges(cell_edges)
{
}

inline DLV::alter_lattice::alter_lattice(class model *m, const coord_type pa,
					 const coord_type pb,
					 const coord_type pc,
					 const coord_type palpha,
					 const coord_type pbeta,
					 const coord_type pgamma)
  : edit_geometry(m), a(pa), b(pb), c(pc),
    alpha(palpha), beta(pbeta), gamma(pgamma)
{
}

inline DLV::origin_shift::origin_shift(class model *m)
  : edit_geometry(m), sx(0.0), sy(0.0), sz(0.0), fractional(false)
{
}

inline DLV::vacuum_cell::vacuum_cell(class model *m)
  : edit_geometry(m), c_axis(0.0), slab_origin(0.0)
{
}

inline DLV::cluster::cluster(class model *m, const int_g n)
  : edit_geometry(m), neighbours(n), radius(0.0)
{
}

inline DLV::slab_from_bulk::slab_from_bulk(class model *m)
  : edit_geometry(m), crystal3d(0), miller_h(0), miller_k(0), miller_l(1),
    conventional_cell(true), termination(0), layer_tolerance(0.001),
    number_of_layers(1), hkl_layers(1)
{
}

inline DLV::slab_from_surface::slab_from_surface(class model *m)
  : edit_geometry(m), layer_tolerance(0.001), number_of_layers(1), hkl_layers(1)
{
}

inline DLV::surface_edit::surface_edit(class model *m)
  : edit_geometry(m), crystal3d(0), miller_h(0), miller_k(0), miller_l(1),
    conventional_cell(true), termination(0), layer_tolerance(0.001)
{
}

inline DLV::salvage::salvage(class model *m)
  : edit_geometry(m), layer_tolerance(0.001), number_of_layers(0)
{
}

inline DLV::wulff::wulff(class model *m, const real_l r, data_object *data)
  : edit_geometry(m), scale(r), crystal(data)
{
}

inline DLV::multi_layer::multi_layer(class model *m, const real_l r,
				     class model *s)
  : edit_geometry(m), space(r), slab(s)
{
}

inline DLV::fill_geometry::fill_geometry(class model *m, class model *s)
  : edit_geometry(m), geometry(s)
{
}

#ifdef ENABLE_DLV_GRAPHICS

inline DLV::atom_edit::atom_edit(class model *m, const int_g i)
  : edit_geometry(m), index(i)
{
}

inline DLV::atom_props::atom_props(class model *m, const int_g i)
  : edit_geometry(m), index(i)
{
}

inline DLV::atom_delete::atom_delete(class model *m)
  : edit_geometry(m)
{
}

inline DLV::atom_insert::atom_insert(class model *m, const bool sym)
  : edit_geometry(m), keep_sym(sym), atomic_number(1),
    x_p(0.0), y_p(0.0), z_p(0.0), fractional(false), editor(false)
{
}

inline DLV::model_insert::model_insert(class model *m, class model *i,
				       const bool sym)
  : edit_geometry(m), insert(i), keep_sym(sym), x_p(0.0), y_p(0.0), z_p(0.0),
    x_c(0.0), y_c(0.0), z_c(0.0), fractional(false), editor(false)
{
}

inline DLV::atom_move::atom_move(class model *m, const bool sym,
				 const real_l ax, const real_l ay,
				 const real_l az)
  : edit_geometry(m), keep_sym(sym), x_p(ax), y_p(ay), z_p(az),
    x_c(ax), y_c(ay), z_c(az), displacement(false), fractional(false),
    editor(false)
{
}

#endif // ENABLE_DLV_GRAPHICS

#endif // DLV_OP_ATOM_EDITS
