
#include <cmath>
#include <list>
#include <map>
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/toolkit_obj.hxx"
#  include "../graphics/render_base.hxx"
#  include "../graphics/model_render.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "utils.hxx"
#include "math_fns.hxx"
#include "symmetry.hxx"
#include "atom_model.hxx"
#include "atom_pairs.hxx"
#include "model.hxx"
#include "model_atoms.hxx"
#include "polymer.hxx"

DLV::model *DLV::create_polymer(const string model_name)
{
  return new polymer(model_name);
}

DLV::model_base *DLV::polymer::duplicate(const string label) const
{
  polymer *m = new polymer(label);
  m->copy_atom_groups(this);
  return m;
}

void DLV::polymer::use_which_lattice_parameters(bool usage[6]) const
{
  usage[0] = true;
  usage[1] = false;
  usage[2] = false;
  usage[3] = false;
  usage[4] = false;
  usage[5] = false;
}

bool DLV::polymer::set_primitive_lattice(const coord_type a[3],
					 const coord_type [3],
					 const coord_type [3])
{
  return set_lattice(a);
}

bool DLV::polymer::set_lattice_parameters(const coord_type a,
					  const coord_type,
					  const coord_type,
					  const coord_type,
					  const coord_type,
					  const coord_type)
{
  return set_parameters(a);
}

DLV::int_g DLV::polymer::get_number_of_periodic_dims() const
{
  return 1;
}

DLV::int_g DLV::polymer::get_lattice_type() const
{
  return 0;
}

DLV::int_g DLV::polymer::get_lattice_centring() const
{
  return 0;
}

// Kind of the dimensionality - for the User Interface menus.
DLV::int_g DLV::polymer::get_model_type() const
{
  return 1;
}

void DLV::polymer::get_frac_rotation_operators(real_l r[][3][3],
					       const int_g nops) const
{
  // Todo
  get_cart_rotation_operators(r, nops);
}

void DLV::polymer::set_crystal03_lattice_type(const int_g, const int_g)
{
  // Todo - a bug?
}

void DLV::polymer::get_primitive_lattice(coord_type a[3], coord_type b[3],
					 coord_type c[3]) const
{
  coord_type l[3][3];
  get_primitive_vectors(l);
  a[0] = l[0][0];
  a[1] = l[0][1];
  a[2] = l[0][2];
  b[0] = 0.0;
  b[1] = 0.0;
  b[2] = 0.0;
  c[0] = 0.0;
  c[1] = 0.0;
  c[2] = 0.0;
}

void DLV::polymer::get_conventional_lattice(coord_type a[3], coord_type b[3],
					    coord_type c[3]) const
{
  get_primitive_lattice(a, b, c);
}

void DLV::polymer::get_reciprocal_lattice(coord_type &l) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  l = real_l(1.0) / sqrt(a[0] * a[0] + a[1] * a[1] + a[2] * a[2]);
}

void DLV::polymer::get_reciprocal_lattice(coord_type a[3], coord_type b[3],
					  coord_type c[3]) const
{
  get_reciprocal_lattice(a[0]);
  a[1] = 0.0;
  a[2] = 0.0;
  b[0] = 0.0;
  b[1] = 0.0;
  b[2] = 0.0;
  c[0] = 0.0;
  c[1] = 0.0;
  c[2] = 0.0;
}

void DLV::polymer::generate_real_space_lattice(real_g (* &ipoints)[3],
					       real_g (* &fpoints)[3],
					       int_g &nlines, const int_g na,
					       const int_g, const int_g,
					       const bool centre_cell,
					       const bool, char [],
					       const int_g) const
{
  nlines = 1;
  ipoints = new real_g[nlines][3];
  fpoints = new real_g[nlines][3];
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  real_g origin[3] = { 0.0, 0.0, 0.0 };
  if (centre_cell) {
    origin[0] = real_g(- (real_l(na) * a[0]) / 2.0);
    origin[1] = real_g(- (real_l(na) * a[1]) / 2.0);
    origin[2] = real_g(- (real_l(na) * a[2]) / 2.0);
  }
  ipoints[0][0] = origin[0];
  ipoints[0][1] = origin[1];
  ipoints[0][2] = origin[2];
  fpoints[0][0] = real_g(real_l(na) * a[0]);
  fpoints[0][1] = real_g(real_l(na) * a[1]);
  fpoints[0][2] = real_g(real_l(na) * a[2]);
}

DLV::int_g DLV::polymer::get_real_space_lattice_labels(real_g points[][3],
						const int_g na, const int_g,
						const int_g,
						const bool centre_cell,
						const bool) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  real_g origin[3] = { 0.0, 0.0, 0.0 };
  if (centre_cell) {
    origin[0] = real_g(- (real_l(na) * a[0]) / 2.0);
    origin[1] = real_g(- (real_l(na) * a[1]) / 2.0);
    origin[2] = real_g(- (real_l(na) * a[2]) / 2.0);
  }
  points[0][0] = origin[0];
  points[0][1] = origin[1];
  points[0][2] = origin[2];
  points[1][0] = origin[0] + real_g(na) * real_g(a[0]);
  points[1][1] = origin[1] + real_g(na) * real_g(a[1]);
  points[1][2] = origin[2] + real_g(na) * real_g(a[2]);
  return 2;
}

void DLV::polymer::generate_k_space_vertices(real_g (* &vertices)[3],
					     int_g &nlines, int_l * &connects,
					     int_l &nconnects, char message[],
					     const int_g mlen) const
{
  strncpy(message, "Oops: Polymer generate reciprocal lattice Todo", mlen);
  nlines = 0;
}

void DLV::polymer::generate_transforms(const int_g na, const int_g, const int_g,
				       const int_g sa, const int_g, const int_g,
				       const bool centre_cell, const bool,
				       int_g &ntransforms,
				       real_g (* &transforms)[3]) const
{
  ntransforms = na;
  transforms = new real_g[na][3];
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  int_g min_a = 0;
  int_g max_a = na;
  if (centre_cell) {
    max_a = na / 2;
    min_a = - max_a;
    if (max_a * 2 != na)
      max_a++;
  }
  int_g j = 0;
  for (int_g i = min_a; i < max_a; i++) {
    transforms[j][0] = (real_g)(i * sa) * (real_g)a[0];
    transforms[j][1] = (real_g)(i * sa) * (real_g)a[1];
    transforms[j][2] = (real_g)(i * sa) * (real_g)a[2];
    j++;
  }
}

bool DLV::polymer::set_fractional_sym_ops(const real_l rotations[][3][3],
					  const real_l translations[][3],
					  const int_g nops)
{
  // Todo
  return false;
}

void DLV::polymer::generate_primitive_atoms(const bool tidy)
{
  int_g nops = get_number_of_sym_ops();
  real_l (*rotations)[3][3] = new_local_array3(real_l, nops, 3, 3);
  real_l (*translations)[3] = new_local_array2(real_l, nops, 3);
  get_cartesian_ops(rotations, translations, nops);
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  generate_primitive_copies(a, rotations, translations, nops, tidy);
  delete_local_array(translations);
  delete_local_array(rotations);
}

void DLV::polymer::find_asym_pos(const int_g asym_idx, const coord_type a[3],
				 int_g &op, coord_type a_coords[3],
				 const coord_type b[][3],
				 const int_g b_asym_idx[],
				 coord_type b_coords[][3], int_g ab_shift[3],
				 const int_g nbatoms) const
{
  // only designed for i, j to i, j, k, l
  if (nbatoms < 1 or nbatoms > 3)
    exit(1);
  int_g nops = get_number_of_sym_ops();
  real_l (*rotations)[3][3] = new_local_array3(real_l, nops, 3, 3);
  real_l (*translations)[3] = new_local_array2(real_l, nops, 3);
  get_cartesian_ops(rotations, translations, nops);
  coord_type la[3];
  coord_type lb[3];
  coord_type lc[3];
  get_primitive_lattice(la, lb, lc);
  map_asym_pos(asym_idx, a, op, a_coords, b, b_asym_idx, b_coords, ab_shift,
	       nbatoms, la, rotations, translations, nops);
  delete_local_array(translations);
  delete_local_array(rotations);
}

void DLV::polymer::conventional_atoms(const atom_tree &primitive, atom_tree &tree,
				      const bool centre[3]) const
{
  // I don't think we have a conventional cell
  if (centre[0])
    centre_atoms(primitive, tree, centre);
  else
    primitive.copy(tree);
}

void DLV::polymer::centre_atoms(const atom_tree &primitive, atom_tree &tree,
				const bool centre[3]) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  real_l cell[3][3];
  for (int_g i = 0; i < 3; i++) {
    cell[i][0] = a[i];
    cell[i][1] = 0.0;
    cell[i][2] = 0.0;
  }
  primitive.copy_and_centre(tree, cell, 1, centre);
}

void DLV::polymer::update_display_atoms(atom_tree &tree, const real_l tol,
					const int_g na, const int_g,
					const int_g, const bool,
					const bool centre, const bool edges) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  tree.replicate_atoms(a, tol, na, centre, edges);
}

void DLV::polymer::generate_bonds(const atom_tree &tree,
				  const int_g na, const int_g, const int_g,
				  const bool, std::list<bond_list> info[],
				  const real_g overlap, const bool centre,
				  const bool invert_colours,
				  const bond_data &bond_info,
				  const real_l tol,
				  const std::vector<real_g (*)[3]> *traj,
				  const int_g nframes, const bool gamma,
				  const bool edit) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  tree.generate_bonds(a, na, info, nframes, overlap, centre, invert_colours,
		      bond_info, tol, traj, gamma, edit);
}

void DLV::polymer::get_lattice_parameters(coord_type &a, coord_type &b,
					  coord_type &c, coord_type &alpha,
					  coord_type &beta, coord_type &gamma,
					  const bool conventional) const
{
  get_parameters(a);
  b = 0.0;
  c = 0.0;
  alpha = 0.0;
  beta = 0.0;
  gamma = 0.0;
}

void DLV::polymer::complete_lattice(coord_type va[3], coord_type [3],
				    coord_type [3])
{
  if (has_valid_parameters() and !has_valid_vectors()) {
    coord_type a;
    get_parameters(a);
    coord_type v[3];
    va[0] = a;
    va[1] = 0.0;
    va[2] = 0.0;
    set_lattice(v);
    set_valid_vectors();
    set_valid_parameters();
  } else if (has_valid_vectors() and !has_valid_parameters()) {
    coord_type l[3][3];
    get_primitive_vectors(l);
    coord_type a = sqrt(l[0][0] * l[0][0]
			+ l[0][1] * l[0][1] + l[0][2] * l[0][2]);
    set_parameters(a);
    set_valid_parameters();
    set_valid_vectors();
    va[0] = l[0][0];
    va[1] = l[0][1];
    va[2] = l[0][2];
  }
  else if (has_valid_parameters() and has_valid_vectors()){
    coord_type l[3][3];
    get_primitive_vectors(l);
    va[0] = l[0][0];
    va[1] = l[0][1];
    va[2] = l[0][2];
  }
}

void DLV::polymer::map_data(const atom_tree &tree, const real_g grid[][3],
			    int_g **const data, const int_g ndata,
			    const int_g ngrid, real_g (* &map_grid)[3],
			    int_g ** &map_data, int_g &n) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  tree.map_data(grid, data, ndata, ngrid, map_grid, map_data, n, a);
}

void DLV::polymer::map_data(const atom_tree &tree, const real_g grid[][3],
			    real_g **const data, const int_g ndata,
			    const int_g ngrid, real_g (* &map_grid)[3],
			    real_g ** &map_data, int_g &n) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  tree.map_data(grid, data, ndata, ngrid, map_grid, map_data, n, a);
}

void DLV::polymer::map_data(const atom_tree &tree, const real_g grid[][3],
			    const real_g mag[][3], const real_g phases[][3],
			    const int_g ngrid, real_g (* &map_grid)[3],
			    real_g (* &map_data)[3], int_g &n,
			    const real_g ka, const real_g, const real_g) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  tree.map_data(grid, mag, phases, ngrid, map_grid, map_data, n, ka, a);
}

void DLV::polymer::map_data(const atom_tree &tree, const real_g grid[][3],
			    const real_g mags[][3], const real_g phases[][3],
			    const int_g ngrid,
			    std::vector<real_g (*)[3]> &new_data, int_g &n,
			    const int_g nframes, const int_g ncopies,
			    const real_g ka, const real_g,
			    const real_g, const real_g scale) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  tree.map_data(grid, mags, phases, ngrid, new_data, n, scale,
		nframes, ncopies, ka, a);
}

void DLV::polymer::map_data(const atom_tree &tree, const real_g grid[][3],
			    const int_g ngrid,
			    const std::vector<real_g (*)[3]> &traj,
			    const int_g nframes,
			    std::vector<real_g (*)[3]> &new_data) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  tree.map_data(grid, ngrid, traj, new_data, nframes, a);
}

void DLV::polymer::build_supercell_symmetry(const model *m,
					    const coord_type a[3],
					    const coord_type b[3],
					    const coord_type c[3],
					    const coord_type va[3],
					    const coord_type vb[3],
					    const coord_type vc[3],
					    const bool)
{
  const polymer *sym = static_cast<const polymer *>(m);
  copy_symmetry(sym, false);
  build_supercell_operators(a, b, c, va, vb, vc, 1);
}

void DLV::polymer::copy_lattice(const model_base *m)
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  m->get_primitive_lattice(a, b, c);
  set_primitive_lattice(a, b, c);
  coord_type a1;
  coord_type b1;
  coord_type c1;
  coord_type alpha;
  coord_type beta;
  coord_type gamma;
  m->get_lattice_parameters(a1, b1, c1, alpha, beta, gamma);
  set_parameters(a1);
  set_valid_vectors();
}

void DLV::polymer::copy_bravais_lattice(const model_base *)
{
}

void DLV::polymer::copy_symmetry_ops(const model_base *m, const bool frac)
{
  const polymer *sym = static_cast<const polymer *>(m);
  copy_symmetry(sym, frac);
}

void DLV::polymer::rotate_symmetry_ops(const model_base *m,
				       const real_l r[3][3])
{
  const polymer *sym = static_cast<const polymer *>(m);
  rotate_symmetry(sym, r);
}

void DLV::polymer::shift_cartesian_operators(const model_base *m,
					     const real_l x, const real_l y,
					     const real_l z,
					     const coord_type a[3],
					     const coord_type b[3],
					     const coord_type c[3])
{
  const polymer *sym = static_cast<const polymer *>(m);
  shift_ops_cartesian(sym->get_symmetry(), x, y, z, a, b, c, 1);
}

void DLV::polymer::shift_fractional_operators(const model_base *m,
					      const real_l x, const real_l y,
					      const real_l z,
					      const coord_type a[3],
					      const coord_type b[3],
					      const coord_type c[3])
{
  const polymer *sym = static_cast<const polymer *>(m);
  shift_ops_fractional(sym->get_symmetry(), x, y, z, a, b, c, 1);
}

void DLV::polymer::cartesian_to_fractional_coords(real_l &x, real_l &,
						  real_l &) const
{
  // get x, y, z as fractional
  coord_type l[3][3];
  get_primitive_vectors(l);
  real_l inverse[3][3];
  matrix_invert(l, inverse, 1);
  x = inverse[0][0] * x;
}

void DLV::polymer::fractional_to_cartesian_coords(real_l &x, real_l &,
						  real_l &) const
{
  // get x, y, z as cartesian
  coord_type l[3][3];
  get_primitive_vectors(l);
  x = l[0][0] * x;
}

bool DLV::polymer::map_atom_selections(const atom_tree &display,
				       const atom_tree &prim,
				       const real_g grid[][3],
				       int_g **const data, const int_g ndata,
				       const int_g ngrid, int_g * &labels,
				       int_g * &atom_types, atom * &parents,
				       int_g &n, const bool all_atoms) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  return display.map_atom_selections(prim, n, labels, atom_types, parents,
				     all_atoms, grid, data, ndata, ngrid, a);
}

bool DLV::polymer::map_selected_atoms(const atom_tree &display, int_g &n,
				      int_g * &labels, int_g (* &shifts)[3],
				      const real_g grid[][3],
				      int_g **const data, const int_g ndata,
				      const int_g ngrid) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  return display.map_selected_atoms(n, labels, shifts, grid, data, ndata,
				    ngrid, a);
}

void DLV::polymer::get_atom_shifts(const atom_tree &tree,
				   const coord_type coords[][3],
				   int_g shifts[][3], int_g prim_id[],
				   const int_g n) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  tree.get_atom_shifts(coords, shifts, prim_id, n, a);
}

#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::polymer *t,
				    const unsigned int file_version)
    {
      DLV::string n = t->get_model_name();
      ar << n;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::polymer *t,
				    const unsigned int file_version)
    {
      DLV::string n;
      ar >> n;
      ::new(t)DLV::polymer(n);
    }

  }
}

template <class Archive>
void DLV::polymer::serialize(Archive &ar, const unsigned int version)
{
  ar &
    boost::serialization::base_object<periodic_model<line_symmetry> >(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::polymer)

DLV_SUPPRESS_TEMPLATES(DLV::model_impl<DLV::line_symmetry>)

DLV_NORMAL_EXPLICIT(DLV::polymer)

#endif // DLV_USES_SERIALIZE
