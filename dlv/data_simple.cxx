
#include <cmath>
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/toolkit_obj.hxx"
#  include "../graphics/render_base.hxx"
#  include "../graphics/display_objs.hxx"
#  include "../graphics/drawable.hxx"
#  include "../graphics/edit_objs.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "data_objs.hxx"
#include "operation.hxx"
// for update in point
#include "utils.hxx"
#include "math_fns.hxx"
#include "data_simple.hxx"
//#include "atom_model.hxx"
#include "model.hxx"

bool DLV::point::is_displayable() const
{
  return true;
}

bool DLV::point::is_editable() const
{
  return false;
}

bool DLV::point::is_edited() const
{
  return false;
}

DLV::string DLV::point::get_data_label() const
{
  return (name + " - point");
}
 
DLV::string DLV::point::get_edit_label() const
{
  return "";
}

DLV::int_g DLV::point::get_number_data_sets() const
{
  return 1;
}

DLV::string DLV::point::get_sub_data_label(const int_g) const
{
  return "";
}

DLV::int_g DLV::point::get_display_type() const
{
  return display_point;
}

DLV::int_g DLV::point::get_edit_type() const
{
  return no_edit;
}

bool DLV::point::get_sub_is_vector(const int_g) const
{
  return false;
}

bool DLV::k_space_point::is_kspace() const
{
  return true;
}

void DLV::point::unload_data()
{
  // Do nothing
}

DLV::reloadable_data::reloadable_data(const string code, const string src,
				      operation *p, const bool show)
  : data_object(code, src, show), loaded(true), parent_op(p)
{
}

void DLV::scalar_data::unload_data()
{
  // Do nothing
}

bool DLV::scalar_data::is_displayable() const
{
  return true;
}

bool DLV::scalar_data::is_editable() const
{
  return false;
}

DLV::string DLV::scalar_data::get_obj_label() const
{
  return label;
}

DLV::string DLV::scalar_data::get_data_label() const
{
  string tag = label + get_tags();
  return tag;
}

DLV::string DLV::scalar_data::get_edit_label() const
{
  return "";
}

DLV::int_g DLV::scalar_data::get_number_data_sets() const
{
  return 1;
}

DLV::string DLV::scalar_data::get_sub_data_label(const int_g) const
{
  return "";
}

DLV::int_g DLV::scalar_data::get_edit_type() const
{
  return no_edit;
}

bool DLV::scalar_data::get_sub_is_vector(const int_g) const
{
  return false;
}

bool DLV::boolean_data::is_displayable() const
{
  return false;
}

DLV::int_g DLV::boolean_data::get_display_type() const
{
  // Todo no_display?
  return display_text;
}

DLV::int_g DLV::integer_data::get_display_type() const
{
  return display_text;
}

bool DLV::real_data::is_displayable() const
{
  return displayable;
}

DLV::int_g DLV::real_data::get_display_type() const
{
  return display_real;
}

bool DLV::real_data::is_real_scalar() const
{
  return true;
}

bool DLV::real_data::get_real_value(real_l &v) const
{
  v = get_value();
  return true;;
}

void DLV::file_data::unload_data()
{
  // Do nothing
}

bool DLV::file_data::is_displayable() const
{
  return false;
}

bool DLV::file_data::is_editable() const
{
  return false;
}

DLV::string DLV::file_data::get_obj_label() const
{
  return label;
}

DLV::string DLV::file_data::get_data_label() const
{
  string tag = label + get_tags();
  return tag;
}

DLV::string DLV::file_data::get_edit_label() const
{
  return "";
}

DLV::int_g DLV::file_data::get_number_data_sets() const
{
  return 1;
}

DLV::string DLV::file_data::get_sub_data_label(const int_g) const
{
  return "";
}

DLV::int_g DLV::file_data::get_display_type() const
{
  return -1;
}

DLV::int_g DLV::file_data::get_edit_type() const
{
  return no_edit;
}

bool DLV::file_data::get_sub_is_vector(const int_g) const
{
  return false;
}

void DLV::text_file::unload_data()
{
  // Do nothing?
}

bool DLV::text_file::is_displayable() const
{
  return true;
}

bool DLV::text_file::is_editable() const
{
  return false;
}

DLV::string DLV::text_file::get_data_label() const
{
  string tag = label + get_tags();
  return tag;
}

DLV::string DLV::text_file::get_edit_label() const
{
  return "";
}

DLV::int_g DLV::text_file::get_number_data_sets() const
{
  return 1;
}

DLV::string DLV::text_file::get_sub_data_label(const int_g) const
{
  return "";
}

DLV::int_g DLV::text_file::get_display_type() const
{
  return display_file;
}

DLV::int_g DLV::text_file::get_edit_type() const
{
  return no_edit;
}

bool DLV::text_file::get_sub_is_vector(const int_g) const
{
  return false;
}

#ifdef ENABLE_DLV_GRAPHICS

DLV::edit_object::~edit_object()
{
  delete edit;
}

void DLV::edit_object::list_edit_object(const render_parent *parent) const
{
  edit->update_edit_list(parent, name, index);
}

bool DLV::edit_object::check_edit_index(const int_g id) const
{
  return (index == id);
}

bool DLV::reloadable_data::reload_data(reloadable_data *data, char message[],
				       const int_g mlen)
{
  if (loaded)
    return true;
  else {
    reset_data_sets();
    if (parent_op->reload_data(data, message, mlen))
      loaded = true;
    return false;
  }
}

DLVreturn_type DLV::text_file::render(const render_parent *parent,
				      class model *structure,
				      char message[], const int_g mlen)
{
  //return render(parent, structure, 0, 0, 0, message, mlen);
  return DLV_OK; // issue with crystal opt structure
}

DLVreturn_type DLV::text_file::render(const render_parent *parent,
				      class model *structure,
				      const int_g component, const int_g method,
				      const int_g index, char message[],
				      const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    obj = DLV::drawable_obj::create_text_file(parent, filename, message, mlen);
    if (obj == 0)
      return DLV_ERROR;
    else
      set_drawable(obj);
  } else
    obj->update_text_value(filename);
  display_obj *ptr = 0;
  string name = get_data_label();
  ptr = display_obj::create_file_view(parent, obj, name, index, message, mlen);
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

DLVreturn_type DLV::reloadable_data::render(const render_parent *parent,
					    class model *structure,
					    char message[], const int_g mlen)
{
  /*
  if (!loaded) {
    if (!parent_op->reload_data(this, message, mlen))
      return DLV_ERROR;
    loaded = true;
  }
  return render_data(structure, message, mlen);
  */
  return DLV_OK;
}

DLVreturn_type DLV::boolean_data::render(const render_parent *parent,
					 class model *structure,
					 char message[], const int_g mlen)
{
  strncpy(message, "BUG: data item is not renderable", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::boolean_data::render(const render_parent *parent,
					 class model *structure,
					 const int_g component,
					 const int_g method, const int_g index,
					 char message[], const int_g mlen)
{
  strncpy(message, "BUG: data item is not renderable", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::integer_data::render(const render_parent *parent,
					 class model *structure,
					 char message[], const int_g mlen)
{
  /* Todo - I suspect that as for real_data this might be a problem
  if (get_drawable() != 0)
    return render(parent, structure, 0, 0, 0, message, mlen);
  else
  */
  return DLV_OK;
}

DLVreturn_type DLV::integer_data::render(const render_parent *parent,
					 class model *structure,
					 const int_g component,
					 const int_g method, const int_g index,
					 char message[], const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    char stuff[32];
    sprintf(stuff, "%1d", value);
    obj = DLV::drawable_obj::create_text_value(parent, stuff, message, mlen);
    if (obj == 0)
      return DLV_ERROR;
    else
      set_drawable(obj);
  }
  display_obj *ptr = 0;
  string name = get_data_label();
  ptr = display_obj::create_text_display(parent, obj, name, index,
					 message, mlen);
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

DLVreturn_type DLV::real_data::render(const render_parent *parent,
				      class model *structure,
				      char message[], const int_g mlen)
{
  /* - Not sure why this was here but broke project recovery, Todo redundant?
  if (get_drawable() != 0)
    return render(parent, structure, 0, 0, 0, message, mlen);
  else
  */
  return DLV_OK;
}

DLVreturn_type DLV::real_data::render(const render_parent *parent,
				      class model *structure,
				      const int_g component, const int_g method,
				      const int_g index, char message[],
				      const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    obj = DLV::drawable_obj::create_real_value(parent, value, message, mlen);
    if (obj == 0)
      return DLV_ERROR;
    else
      set_drawable(obj);
  }
  display_obj *ptr = 0;
  string name = get_data_label();
  ptr = display_obj::create_real_display(parent, obj, name, index,
					 message, mlen);
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

DLVreturn_type DLV::file_data::render(const render_parent *parent,
				      class model *structure,
				      char message[], const int_g mlen)
{
  message[0] = '\0';
  return DLV_OK;
}

DLVreturn_type DLV::file_data::render(const render_parent *parent,
				      class model *structure,
				      const int_g component, const int_g method,
				      const int_g index, char message[],
				      const int_g mlen)
{
  message[0] = '\0';
  return DLV_OK;
}

bool DLV::edit_object::is_edited() const
{
  return true;
}

DLVreturn_type DLV::edit_object::select(char message[], const int_g mlen)
{
  edit->attach_params();
  return DLV_OK;
}

bool DLV::edit_object::is_kspace() const
{
  return parent->is_kspace();
}

bool DLV::edit_object::is_periodic() const
{
  return parent->is_periodic();
}

DLVreturn_type DLV::edit_object::render(const render_parent *parent,
					class model *structure,
					char message[], const int_g mlen)
{
  strncpy(message, "BUG: dummy routine called - Todo", mlen);
  return DLV_ERROR;
}

void DLV::edit_object::unload_data()
{
  // Todo?
}

DLVreturn_type DLV::point::render(const render_parent *parent,
				  class model *structure,
				  char message[], const int_g mlen)
{
  //return render(parent, structure, 0, 0, 0, message, mlen);
  return DLV_OK;
}

DLVreturn_type DLV::point::render(const render_parent *parent,
				  class model *structure,
				  const int_g component, const int_g method,
				  const int_g index, char message[],
				  const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    obj = DLV::drawable_obj::create_point_data(parent, is_kspace(),
					       message, mlen);
    if (obj == 0)
      return DLV_ERROR;
    else
      set_drawable(obj);
  }
  if (count_display_objects() == 0) {
    display_obj *ptr = 0;
    ptr = display_obj::create_point_obj(parent, obj, name, index, is_kspace(),
					message, mlen);
    if (ptr == 0) {
      if (strlen(message) == 0)
	strncpy(message, "Unable to allocate display object", mlen);
      return DLV_ERROR;
    } else {
      add_display_obj(ptr);
      ptr->update_display_list(parent, name);
      ptr->attach_params();
    }
  }
  return DLV_OK;
}

DLVreturn_type DLV::point::update0D(const int_g method, const real_g x,
				    const real_g y, const real_g z,
				    const bool conv, model *const m,
				    char message[], const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    strncpy(message, "BUG: missing drawable for point", mlen);
    ok = DLV_ERROR;
  } else {
    position[0] = x;
    position[1] = y;
    position[2] = z;
    if (method == 0) {
      coord_type va[3];
      coord_type vb[3];
      coord_type vc[3];
      if (is_kspace())
	m->get_reciprocal_lattice(va, vb, vc);
      else if (conv)
	m->get_conventional_lattice(va, vb, vc);
      else
	m->get_primitive_lattice(va, vb, vc);
      int_g dim = m->get_number_of_periodic_dims();
      for (int_g i = 0; i < dim; i++)
	position[i] = (real_g)(va[i] * x + vb[i] * y + vc[i] * z);
    }
    obj->update_point(position);
  }
  return ok;
}

#endif // ENABLE_DLV_GRAPHICS


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::boolean_data *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::boolean_data("recover", "temp", "", false);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::integer_data *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::integer_data("recover", "temp", "", 0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::real_data *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::real_data("recover", "temp", "", 0.0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::file_data *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::file_data("nofile", "recover", "");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::text_file *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::text_file("nofile", "recover", "temp", "");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::real_space_point *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::real_space_point("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::k_space_point *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::k_space_point("recover");
    }

  }
}

template <class Archive>
void DLV::wulff_data::serialize(Archive &ar, const unsigned int version)
{
  ar & h;
  ar & k;
  ar & l;
  ar & energy;
  ar & red;
  ar & green;
  ar & blue;
  ar & conventional;
}

template <class Archive>
void DLV::scalar_data::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<data_object>(*this);
  ar & label;
}

template <class Archive>
void DLV::boolean_data::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<scalar_data>(*this);
  ar & value;
}

template <class Archive>
void DLV::integer_data::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<scalar_data>(*this);
  ar & value;
#ifdef ENABLE_DLV_GRAPHICS
  reattach_objects();
#endif // ENABLE_DLV_GRAPHICS
}

template <class Archive>
void DLV::real_data::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<scalar_data>(*this);
  ar & value;
  ar & displayable;
#ifdef ENABLE_DLV_GRAPHICS
  reattach_objects();
#endif // ENABLE_DLV_GRAPHICS
}

template <class Archive>
void DLV::file_data::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<data_object>(*this);
  ar & filename;
  ar & label;
  if (version > 0)
    ar & binary;
  else
    binary = false;
}

template <class Archive>
void DLV::text_file::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<data_object>(*this);
  ar & filename;
  ar & label;
}

template <class Archive>
void DLV::reloadable_data::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<data_object>(*this);
  ar & parent_op;
}

template <class Archive>
void DLV::reloadable_data::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<data_object>(*this);
  ar & parent_op;
  loaded = false;
}

template <class Archive>
void DLV::point::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<data_object>(*this);
  ar & name;
  ar & position;
}

template <class Archive>
void DLV::real_space_point::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<point>(*this);
}

template <class Archive>
void DLV::k_space_point::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<point>(*this);
}

BOOST_CLASS_VERSION(DLV::file_data, 1)

//BOOST_CLASS_EXPORT_KEY(DLV::reloadable_data)
//BOOST_CLASS_EXPORT_IMPLEMENT(DLV::reloadable_data)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::boolean_data)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::integer_data)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::real_data)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::file_data)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::text_file)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::real_space_point)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::k_space_point)

DLV_NORMAL_EXPLICIT(DLV::wulff_data)
DLV_NORMAL_EXPLICIT(DLV::boolean_data)
DLV_NORMAL_EXPLICIT(DLV::integer_data)
DLV_NORMAL_EXPLICIT(DLV::real_data)
DLV_NORMAL_EXPLICIT(DLV::file_data)
DLV_NORMAL_EXPLICIT(DLV::text_file)
DLV_SPLIT_EXPLICIT(DLV::reloadable_data)
DLV_EXPORT_EXPLICIT(DLV::reloadable_data)
DLV_NORMAL_EXPLICIT(DLV::real_space_point)
DLV_NORMAL_EXPLICIT(DLV::k_space_point)

#  ifdef ENABLE_DLV_GRAPHICS

template <class Archive>
void DLV::edit_object::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<data_object>(*this);
  ar & parent;
  ar & edit;
  ar & index;
  ar & name;
}

template <class Archive>
void DLV::edit_object::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<data_object>(*this);
  ar & parent;
  ar & edit;
  ar & index;
  ar & name;
}

//BOOST_CLASS_EXPORT_IMPLEMENT(DLV::edit_object)

DLV_SPLIT_EXPLICIT(DLV::edit_object)
DLV_EXPORT_EXPLICIT(DLV::edit_object)

#  endif // ENABLE_DLV_GRAPHICS

#endif // DLV_USES_SERIALIZE
