
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "operation.hxx"
#include "extern_data_model.hxx"
#include "boost/python.hpp"

#define DLV_MODULE_NAME data_model

BOOST_PYTHON_MODULE(DLV_MODULE_NAME)
{
  using namespace boost::python;
  /*
  def("wulff_crystal", DLV::wulff_crystal);
  def("wulff_update", DLV::wulff_update);
  def("edit_current_data", DLV::edit_current_data);
  */

#ifdef ENABLE_DLV_GRAPHICS

  /*
  def("select_current_edit", DLV::select_current_edit);
  */

#endif // ENABLE_DLV_GRAPHICS
}

/*
DLVreturn_type DLV::update_current_spectrum(const int_g object,
					    const int_g type, const real_g emin,
					    const real_g emax, const int_g np,
					    const real_g w,
					    const real_g harmonics,
					    char message[], const int_g mlen)
{
  return operation::update_current_spectrum(object, type, emin, emax, np, w,
					    harmonics, message, mlen);
}

// definitely an atom based data object
DLVreturn_type DLV::update_current_phonon(const int_g object,
					  const int_g nframes,
					  const real_g scale,
					  const bool use_temp,
					  const real_g temperature,
					  char message[], const int_g mlen)
{
  return operation::update_current_phonon(object, nframes, scale, use_temp,
					  temperature, message, mlen);
}
*/
