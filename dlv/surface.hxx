
#ifndef DLV_MODEL_SURFACE
#define DLV_MODEL_SURFACE

namespace DLV {

  class surface : public slab {
  public:
    surface(const string model_name);
    //int get_number_of_periodic_dims() const;
    //int get_lattice_type() const;
    //int get_lattice_centring() const;
    int_g get_model_type() const;
    //void set_crystal03_lattice_type(const int_g lattice, const int_g centre);

    //void use_which_lattice_parameters(bool usage[6]) const;
    void get_primitive_lattice(coord_type a[3], coord_type b[3],
			       coord_type c[3]) const;
    void get_conventional_lattice(coord_type a[3], coord_type b[3],
    				  coord_type c[3]) const;
    void get_lattice_parameters(coord_type &a, coord_type &b,
				coord_type &c, coord_type &alpha,
				coord_type &beta, coord_type &gamma,
				const bool conventional) const;
    void get_frac_rotation_operators(real_l r[][3][3], const int_g nops) const;

    void find_asym_pos(const int_g asym_idx, const coord_type a[3],
		       int_g &op, coord_type a_coords[3],
		       const coord_type b[][3], const int_g b_asym_idx[],
		       coord_type b_coords[][3], int_g ab_shift[3],
		       const int_g nbatoms) const;

  protected:
    model_base *duplicate(const string label) const;

    bool set_primitive_lattice(const coord_type a[3], const coord_type b[3],
			       const coord_type c[3]);
    bool set_lattice_parameters(const coord_type a, const coord_type b,
				const coord_type, const coord_type,
				const coord_type, const coord_type gamma);
    void update_display_atoms(atom_tree &tree, const real_l tol, const int_g na,
			      const int_g nb, const int_g nc,
			      const bool conventional, const bool centre,
			      const bool edges) const;
    void generate_bonds(const atom_tree &tree, const int_g na,
			const int_g nb, const int_g nc, const bool conventional,
			std::list<bond_list> info[], const real_g overlap,
			const bool centre, const bool invert_colours,
			const bond_data &bond_info, const real_l tol,
			const std::vector<real_g (*)[3]> *traj,
			const int_g nframes, const bool gamma,
			const bool edit) const;
    void complete_lattice(coord_type va[3], coord_type vb[3],
			  coord_type vc[3]);
    void create_primitive_params(coord_type &a, coord_type &b, coord_type &c,
				 coord_type &alpha, coord_type &beta,
				 coord_type &gamma) const;
    void get_atom_shifts(const atom_tree &tree, const coord_type coords[][3],
			 int_g shifts[][3], int_g prim_id[],
			 const int_g n) const;
    void map_data(const atom_tree &tree, const real_g grid[][3],
		  int_g **const data, const int_g ndata, const int_g ngrid,
		  real_g (* &map_grid)[3], int_g ** &map_data, int_g &n) const;
    void map_data(const atom_tree &tree, const real_g grid[][3],
		  real_g **const data, const int_g ndata, const int_g ngrid,
		  real_g (* &map_grid)[3], real_g ** &map_data, int_g &n) const;
    void map_data(const atom_tree &tree, const real_g grid[][3],
		  const real_g mag[][3], const real_g phases[][3],
		  const int_g ngrid, real_g (* &map_grid)[3],
		  real_g (* &map_data)[3], int_g &n, const real_g ka,
		  const real_g kb, const real_g kc) const;
    void map_data(const atom_tree &tree, const real_g grid[][3],
		  const real_g mags[][3], const real_g phases[][3],
		  const int_g ngrid, std::vector<real_g (*)[3]> &new_data,
		  int_g &n, const int_g nframes, const int_g ncopies,
		  const real_g ka, const real_g kb, const real_g kc,
		  const real_g scale) const;
    bool map_atom_selections(const atom_tree &display, const atom_tree &prim,
			     const real_g grid[][3], int_g **const data,
			     const int_g ndata, const int_g ngrid,
			     int_g * &labels, int_g * &atom_types,
			     atom * &parents, int_g &n,
			     const bool all_atoms) const;

    void copy_lattice(const model_base *m);

    //void copy_bravais_lattice(const model_base *m);
    //void copy_symmetry_ops(const model_base *m, const bool frac);
    //void rotate_symmetry_ops(const model_base *m, const real_l r[3][3]);
    void build_supercell_symmetry(const model *m, const coord_type a[3],
				  const coord_type b[3], const coord_type c[3],
				  const coord_type va[3],
				  const coord_type vb[3],
				  const coord_type vc[3], const bool conv);

    int_g get_salvage_layers() const;
    void set_salvage_layers(const int_g l);

  private:
    void create_lattice_vectors(const coord_type a, const coord_type b,
				const coord_type c, const coord_type alpha,
				const coord_type beta, const coord_type gamma,
				coord_type va[3], coord_type vb[3],
				coord_type vc[3]) const;
    coord_type bulk_c[3];
    int_g salvage_layers;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(DLV::surface)
#endif // DLV_USES_SERIALIZE

inline DLV::surface::surface(const string model_name)
  : slab(model_name), salvage_layers(0)
{
}

#endif // DLV_MODEL_SURFACE
