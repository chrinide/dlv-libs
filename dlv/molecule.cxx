
#include <list>
#include <map>
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/toolkit_obj.hxx"
#  include "../graphics/render_base.hxx"
#  include "../graphics/model_render.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "utils.hxx"
#include "symmetry.hxx"
#include "atom_model.hxx"
#include "atom_pairs.hxx"
#include "model.hxx"
#include "model_atoms.hxx"
#include "molecule.hxx"

DLV::model *DLV::create_molecule(const string model_name)
{
  return new molecule(model_name);
}

DLV::model_base *DLV::molecule::duplicate(const string label) const
{
  molecule *m = new molecule(label);
  m->copy_atom_groups(this);
  return m;
}

bool DLV::molecule::set_primitive_lattice(const coord_type [3],
					  const coord_type [3],
					  const coord_type [3])
{
  return false;
}

bool DLV::molecule::set_lattice_parameters(const coord_type,
					   const coord_type,
					   const coord_type,
					   const coord_type,
					   const coord_type,
					   const coord_type)
{
  return false;
}

DLV::int_g DLV::molecule::get_number_of_periodic_dims() const
{
  return 0;
}

DLV::int_g DLV::molecule::get_lattice_type() const
{
  return 0;
}

DLV::int_g DLV::molecule::get_lattice_centring() const
{
  return 0;
}

// Kind of the dimensionality - for the User Interface menus.
DLV::int_g DLV::molecule::get_model_type() const
{
  return 0;
}

void DLV::molecule::set_crystal03_lattice_type(const int_g, const int_g)
{
  // Todo - a bug
}

bool DLV::molecule::set_point_group(const char gp[])
{
  return set_pt_group(gp);
}

void DLV::molecule::get_frac_rotation_operators(real_l r[][3][3],
						const int_g nops) const
{
  get_cart_rotation_operators(r, nops);
}

bool DLV::molecule::set_fractional_sym_ops(const real_l rotations[][3][3],
					   const real_l translations[][3],
					   const int_g nops)
{
  return set_cartesian_sym_ops(rotations, translations, nops);
}

void DLV::molecule::get_reciprocal_lattice(coord_type a[3], coord_type b[3],
					   coord_type c[3]) const
{
  // BUG
  a[0] = 0.0;
  a[1] = 0.0;
  a[2] = 0.0;
  b[0] = 0.0;
  b[1] = 0.0;
  b[2] = 0.0;
  c[0] = 0.0;
  c[1] = 0.0;
  c[2] = 0.0;
}

void DLV::molecule::generate_real_space_lattice(real_g (* &)[3],
						real_g (* &)[3],
						int_g &nlines, const int_g,
						const int_g, const int_g,
						const bool, const bool,
						char message[],
						const int_g mlen) const
{
  strncpy(message, "BUG: Molecule generate lattice called", mlen);
  nlines = 0;
}

void DLV::molecule::generate_k_space_vertices(real_g (* &)[3],
					      int_g &nlines, int_l * &,
					      int_l &, char message[],
					      const int_g mlen) const
{
  strncpy(message, "BUG: Molecule generate reciprocal lattice called", mlen);
  nlines = 0;
}

DLV::int_g
DLV::molecule::get_real_space_lattice_labels(real_g [][3], const int_g,
					     const int_g, const int_g,
					     const bool, const bool) const
{
  return 0;
}

void DLV::molecule::generate_transforms(const int_g, const int_g, const int_g,
					const int_g, const int_g, const int_g,
					const bool, const bool,
					int_g &ntransforms,
					real_g (* &transforms)[3]) const
{
  ntransforms = 1;
  transforms = new real_g[1][3];
  transforms[0][0] = 0.0;
  transforms[0][1] = 0.0;
  transforms[0][2] = 0.0;
}

void DLV::molecule::generate_primitive_atoms(const bool tidy)
{
  int_g nops = get_number_of_sym_ops();
  real_l (*rotations)[3][3] = new_local_array3(real_l, nops, 3, 3);
  real_l (*translations)[3] = new_local_array2(real_l, nops, 3);
  get_cartesian_ops(rotations, translations, nops);
  generate_primitive_copies(rotations, translations, nops, tidy);
  delete_local_array(translations);
  delete_local_array(rotations);
}

void DLV::molecule::find_asym_pos(const int_g asym_idx, const coord_type a[3],
				  int_g &op, coord_type a_coords[3],
				  const coord_type b[][3],
				  const int_g b_asym_idx[],
				  coord_type b_coords[][3], int_g ab_shift[3],
				  const int_g nbatoms) const
{
  // only designed for i, j to i, j, k, l
  if (nbatoms < 1 or nbatoms > 3)
    exit(1);
  int_g nops = get_number_of_sym_ops();
  real_l (*rotations)[3][3] = new_local_array3(real_l, nops, 3, 3);
  real_l (*translations)[3] = new_local_array2(real_l, nops, 3);
  get_cartesian_ops(rotations, translations, nops);
  map_asym_pos(asym_idx, a, op, a_coords, b, b_asym_idx, b_coords, ab_shift,
	       nbatoms, rotations, translations, nops);
  delete_local_array(translations);
  delete_local_array(rotations);
}

void DLV::molecule::conventional_atoms(const atom_tree &primitive, atom_tree &tree,
				       const bool [3]) const
{
  primitive.copy(tree);
}

void DLV::molecule::centre_atoms(const atom_tree &primitive, atom_tree &tree,
				 const bool [3]) const
{
  primitive.copy(tree);
}

void DLV::molecule::update_display_atoms(atom_tree &, const real_l,
					 const int_g, const int_g, const int_g,
					 const bool, const bool, const bool) const
{
  // Do nothing
}

void DLV::molecule::generate_bonds(const atom_tree &tree, const int_g,
				   const int_g, const int_g, const bool,
				   std::list<bond_list> info[],
				   const real_g overlap, const bool,
				   const bool invert_colours,
				   const bond_data &bond_info,
				   const real_l tol,
				   const std::vector<real_g (*)[3]> *traj,
				   const int_g nframes, const bool,
				   const bool edit) const
{
  tree.generate_bonds(info, nframes, overlap, invert_colours, bond_info, tol,
		      traj, edit);
}

void DLV::molecule::complete_lattice(coord_type [3], coord_type [3],
				     coord_type [3])
{
  // Do nothing
}

void DLV::molecule::map_data(const atom_tree &tree, const real_g grid[][3],
			     int_g **const data, const int_g ndata,
			     const int_g ngrid, real_g (* &map_grid)[3],
			     int_g ** &map_data, int_g &n) const
{
  tree.map_data(grid, data, ndata, ngrid, map_grid, map_data, n);
}

void DLV::molecule::map_data(const atom_tree &tree, const real_g grid[][3],
			     real_g **const data, const int_g ndata,
			     const int_g ngrid, real_g (* &map_grid)[3],
			     real_g ** &map_data, int_g &n) const
{
  tree.map_data(grid, data, ndata, ngrid, map_grid, map_data, n);
}

void DLV::molecule::map_data(const atom_tree &tree, const real_g grid[][3],
			     const real_g mag[][3], const real_g phases[][3],
			     const int_g ngrid, real_g (* &map_grid)[3],
			     real_g (* &map_data)[3], int_g &n,
			     const real_g, const real_g, const real_g) const
{
  tree.map_data(grid, mag, phases, ngrid, map_grid, map_data, n);
}

void DLV::molecule::map_data(const atom_tree &tree, const real_g grid[][3],
			     const real_g mags[][3], const real_g phases[][3],
			     const int_g ngrid,
			     std::vector<real_g (*)[3]> &new_data, int_g &n,
			     const int_g nframes, const int_g ncopies,
			     const real_g, const real_g, const real_g,
			     const real_g scale) const
{
  tree.map_data(grid, mags, phases, ngrid, new_data, n, scale,
		nframes, ncopies);
}

void DLV::molecule::map_data(const atom_tree &tree, const real_g grid[][3],
			     const int_g ngrid,
			     const std::vector<real_g (*)[3]> &traj,
			     const int_g nframes,
			     std::vector<real_g (*)[3]> &new_data) const
{
  tree.map_data(grid, ngrid, traj, new_data, nframes);
}

void DLV::molecule::build_supercell_symmetry(const model *,
					     const coord_type [3],
					     const coord_type [3],
					     const coord_type [3],
					     const coord_type [3],
					     const coord_type [3],
					     const coord_type [3],
					     const bool)
{
  // BUG
}

void DLV::molecule::copy_lattice(const model_base *)
{
}

void DLV::molecule::copy_bravais_lattice(const model_base *)
{
}

void DLV::molecule::copy_symmetry_ops(const model_base *m, const bool)
{
  const molecule *sym = static_cast<const molecule *>(m);
  copy_symmetry(sym, false);
}

void DLV::molecule::rotate_symmetry_ops(const model_base *m,
					const real_l r[3][3])
{
  const molecule *sym = static_cast<const molecule *>(m);
  rotate_symmetry(sym, r);
}

void DLV::molecule::shift_cartesian_operators(const model_base *m,
					     const real_l x, const real_l y,
					     const real_l z,
					     const coord_type a[3],
					     const coord_type b[3],
					     const coord_type c[3])
{
  const molecule *sym = static_cast<const molecule *>(m);
  shift_ops_cartesian(sym->get_symmetry(), x, y, z, a, b, c, 0);
}

void DLV::molecule::shift_fractional_operators(const model_base *m,
					      const real_l x, const real_l y,
					      const real_l z,
					      const coord_type a[3],
					      const coord_type b[3],
					      const coord_type c[3])
{
  const molecule *sym = static_cast<const molecule *>(m);
  shift_ops_fractional(sym->get_symmetry(), x, y, z, a, b, c, 0);
}

void DLV::molecule::cartesian_to_fractional_coords(real_l &, real_l &,
						   real_l &) const
{
}

void DLV::molecule::fractional_to_cartesian_coords(real_l &, real_l &,
						   real_l &) const
{
}

bool DLV::molecule::map_atom_selections(const atom_tree &display,
					const atom_tree &prim,
					const real_g grid[][3],
					int_g **const data, const int_g ndata,
					const int_g ngrid, int_g * &labels,
					int_g * &atom_types, atom * &parents,
					int_g &n, const bool all_atoms) const
{
  return display.map_atom_selections(prim, n, labels, atom_types, parents,
				     all_atoms, grid, data, ndata, ngrid);
}

bool DLV::molecule::map_selected_atoms(const atom_tree &display, int_g &n,
				       int_g * &labels, int_g (* &shifts)[3],
				       const real_g grid[][3],
				       int_g **const data, const int_g ndata,
				       const int_g ngrid) const
{
  return display.map_selected_atoms(n, labels, shifts, grid, data, ndata,
				    ngrid);
}

void DLV::molecule::get_atom_shifts(const atom_tree &tree,
				    const coord_type coords[][3],
				    int_g shifts[][3], int_g prim_id[],
				    const int_g n) const
{
  tree.get_atom_shifts(coords, shifts, prim_id, n);
}

#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::molecule *t,
				    const unsigned int file_version)
    {
      DLV::string n = t->get_model_name();
      ar << n;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::molecule *t,
				    const unsigned int file_version)
    {
      DLV::string n;
      ar >> n;
      ::new(t)DLV::molecule(n);
    }

  }
}

template <class Archive>
void DLV::molecule::serialize(Archive &ar, const unsigned int version)
{
  ar &
    boost::serialization::base_object<model_impl<point_symmetry> >(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::molecule)

DLV_SUPPRESS_TEMPLATES(DLV::model_impl<DLV::point_symmetry>)

DLV_NORMAL_EXPLICIT(DLV::molecule)

#endif // DLV_USES_SERIALIZE
