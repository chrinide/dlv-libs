
#ifndef DLV_OP_OBJECTS
#define DLV_OP_OBJECTS

namespace DLV {

  class op_volume : public operation {
  public:
    op_volume(const char label[]);
    static operation *create(const char name[], const bool kspace,
			     char message[], const int_g mlen);

    bool reload_data(class data_object *data, char message[], const int_g mlen);

  protected:
    string get_name() const;
    void add_standard_data_objects();

  private:
    string name;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class op_sphere : public operation {
  public:
    op_sphere(const char label[]);
    static operation *create(const char name[], const bool kspace,
			     char message[], const int_g mlen);

    bool reload_data(class data_object *data, char message[], const int_g mlen);

  protected:
    string get_name() const;
    void add_standard_data_objects();

  private:
    string name;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class op_plane : public operation {
  public:
    op_plane(const char label[]);
    static operation *create(const char name[], const bool kspace,
			     char message[], const int_g mlen);

    bool reload_data(class data_object *data, char message[], const int_g mlen);

  protected:
    string get_name() const;
    void add_standard_data_objects();

  private:
    string name;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE

  };

  class op_line : public operation {
  public:
    op_line(const char label[]);
    static operation *create(const char name[], const bool kspace,
			     char message[], const int_g mlen);

    void create_path(const real_g points[][3], const string labels[],
		     const int_g n, const bool kspace, char message[],
		     const int_g mlen);
    bool reload_data(class data_object *data, char message[], const int_g mlen);

  protected:
    string get_name() const;
    void add_standard_data_objects();

  private:
    string name;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE

  };

  class op_point : public operation {
  public:
    op_point(const char label[]);
    static operation *create(const char name[], const bool kspace,
			     char message[], const int_g mlen);

    bool reload_data(class data_object *data, char message[], const int_g mlen);

  protected:
    string get_name() const;
    void add_standard_data_objects();

  private:
    string name;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class op_wulff : public operation {
  public:
    op_wulff(const char label[]);
    static operation *create(const char name[], char message[],
			     const int_g mlen);

    bool reload_data(class data_object *data, char message[], const int_g mlen);

  protected:
    string get_name() const;
    void add_standard_data_objects();

  private:
    string name;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE

  };

  class op_leed : public operation {
  public:
    op_leed();
    static operation *create(char message[], const int_g mlen);

    bool reload_data(class data_object *data, char message[], const int_g mlen);

  protected:
    string get_name() const;
    void add_standard_data_objects();

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

#ifdef DLV_USES_SERIALIZE

BOOST_CLASS_EXPORT_KEY(DLV::op_volume)
BOOST_CLASS_EXPORT_KEY(DLV::op_sphere)
BOOST_CLASS_EXPORT_KEY(DLV::op_plane)
BOOST_CLASS_EXPORT_KEY(DLV::op_line)
BOOST_CLASS_EXPORT_KEY(DLV::op_point)
BOOST_CLASS_EXPORT_KEY(DLV::op_wulff)
BOOST_CLASS_EXPORT_KEY(DLV::op_leed)

#endif // DLV_USES_SERIALIZE

inline DLV::op_volume::op_volume(const char label[]) : name(label)
{
}

inline DLV::op_sphere::op_sphere(const char label[]) : name(label)
{
}

inline DLV::op_plane::op_plane(const char label[]) : name(label)
{
}

inline DLV::op_line::op_line(const char label[]) : name(label)
{
}

inline DLV::op_point::op_point(const char label[]) : name(label)
{
}

inline DLV::op_wulff::op_wulff(const char label[]) : name(label)
{
}

inline DLV::op_leed::op_leed()
{
}

#endif // DLV_OP_OBJECTS
