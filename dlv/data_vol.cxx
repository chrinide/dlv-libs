
#include <map>
#include <cstdio>
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/toolkit_obj.hxx"
#  include "../graphics/render_base.hxx"
#  include "../graphics/display_objs.hxx"
#  include "../graphics/drawable.hxx"
#  include "../graphics/edit_objs.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "constants.hxx"
#include "data_objs.hxx"
// for update in box
#include "utils.hxx"
//#include "atom_model.hxx"
#include "model.hxx"
#include "data_simple.hxx"
#include "data_vol.hxx"
#include "operation.hxx" // find_volume

static const DLV::int_g block_size = 16;

DLV::volume_data::~volume_data()
{
  for (int_g i = 0; i < n_data_sets; i++)
    delete [] data[i];
  delete [] data;
  delete [] labels;
  delete [] vector_dim;
}

DLV::rspace_wavefunction::~rspace_wavefunction()
{
  for (int_g i = 0; i < get_ndata_sets(); i++)
    delete [] phases[i];
  delete [] phases;
}

bool DLV::box::is_box() const
{
  return true;
}

DLV::string DLV::box::get_name() const
{
  return name;
}

bool DLV::box::is_displayable() const
{
  return true;
}

bool DLV::box::is_editable() const
{
  return false;
}

bool DLV::box::is_edited() const
{
  return false;
}

DLV::string DLV::box::get_data_label() const
{
  return (name + " - 3D region");
}
 
DLV::string DLV::box::get_edit_label() const
{
  return "";
}

DLV::int_g DLV::box::get_number_data_sets() const
{
  return 1;
}

DLV::string DLV::box::get_sub_data_label(const int_g) const
{
  return "";
}

DLV::int_g DLV::box::get_display_type() const
{
  return display_box;
}

DLV::int_g DLV::box::get_edit_type() const
{
  return no_edit;
}

bool DLV::box::get_sub_is_vector(const int_g) const
{
  return false;
}

bool DLV::k_space_box::is_kspace() const
{
  return true;
}

void DLV::box::unload_data()
{
  // Do nothing
}

void DLV::box::get_points(real_g o[3], real_g a[3], real_g b[3],
			  real_g c[3]) const
{
  o[0] = origin[0];
  o[1] = origin[1];
  o[2] = origin[2];
  a[0] = pointa[0];
  a[1] = pointa[1];
  a[2] = pointa[2];
  b[0] = pointb[0];
  b[1] = pointb[1];
  b[2] = pointb[2];
  c[0] = pointc[0];
  c[1] = pointc[1];
  c[2] = pointc[2];
}

DLV::string DLV::sphere::get_name() const
{
  return name;
}

bool DLV::sphere::is_displayable() const
{
  return true;
}

bool DLV::sphere::is_editable() const
{
  return false;
}

bool DLV::sphere::is_edited() const
{
  return false;
}

DLV::string DLV::sphere::get_data_label() const
{
  return (name + " - sphere");
}
 
DLV::string DLV::sphere::get_edit_label() const
{
  return "";
}

DLV::int_g DLV::sphere::get_number_data_sets() const
{
  return 1;
}

DLV::string DLV::sphere::get_sub_data_label(const int_g) const
{
  return "";
}

DLV::int_g DLV::sphere::get_display_type() const
{
  return display_box;
}

DLV::int_g DLV::sphere::get_edit_type() const
{
  return no_edit;
}

bool DLV::sphere::get_sub_is_vector(const int_g) const
{
  return false;
}

void DLV::sphere::unload_data()
{
  // Do nothing
}

DLV::string DLV::wulff_plot::get_name() const
{
  return name;
}

bool DLV::wulff_plot::is_displayable() const
{
  return true;
}

bool DLV::wulff_plot::is_editable() const
{
  return false;
}

bool DLV::wulff_plot::is_edited() const
{
  return false;
}

bool DLV::wulff_plot::is_wulff() const
{
  return true;
}

DLV::string DLV::wulff_plot::get_data_label() const
{
  return (name + " - Wulff plot");
}
 
DLV::string DLV::wulff_plot::get_edit_label() const
{
  return "";
}

DLV::int_g DLV::wulff_plot::get_number_data_sets() const
{
  return 1;
}

DLV::string DLV::wulff_plot::get_sub_data_label(const int_g) const
{
  return "";
}

DLV::int_g DLV::wulff_plot::get_display_type() const
{
  return display_box;
}

DLV::int_g DLV::wulff_plot::get_edit_type() const
{
  return no_edit;
}

bool DLV::wulff_plot::get_sub_is_vector(const int_g) const
{
  return false;
}

void DLV::wulff_plot::unload_data()
{
  // Do nothing
}

DLV::volume_data::volume_data(const char code[], const string src,
			      operation *p, const int_g na, const int_g nb,
			      const int_g nc, const real_g o[3],
			      const real_g as[3], const real_g bs[3],
			      const real_g cs[3])
  : reloadable_data(code, src, p), nx(na), ny(nb), nz(nc), n_data_sets(0),
    labels(0), data(0), vector_dim(0), size(0)
{
  origin[0] = o[0];
  origin[1] = o[1];
  origin[2] = o[2];
  a_step[0] = as[0];
  a_step[1] = as[1];
  a_step[2] = as[2];
  b_step[0] = bs[0];
  b_step[1] = bs[1];
  b_step[2] = bs[2];
  c_step[0] = cs[0];
  c_step[1] = cs[1];
  c_step[2] = cs[2];
}

void DLV::volume_data::add_data(real_g *array, const int_g vec,
				const char label[], const bool copy_data)
{
  if (n_data_sets == size) {
    real_g **new_data = new real_g*[size + block_size];
    string *new_str = new string[size + block_size];
    int_g *nvec = new int_g[size + block_size];
    for (int_g i = 0; i < size; i++) {
      new_data[i] = data[i];
      new_str[i] = labels[i];
      nvec[i] = vector_dim[i];
    }
    if (data != 0)
      delete [] data;
    data = new_data;
    if (labels != 0)
      delete [] labels;
    labels = new_str;
    if (vector_dim != 0)
      delete [] vector_dim;
    vector_dim = nvec;
    size += block_size;
  }
  labels[n_data_sets] = label;
  vector_dim[n_data_sets] = vec;
  if (copy_data) {
    data[n_data_sets] = new real_g[nx * ny * nz * vec];
    for (int_l i = 0; i < nx * ny * nz * vec; i++)
      data[n_data_sets][i] = array[i];
  } else
    data[n_data_sets] = array;
  n_data_sets++;
}

void DLV::volume_data::add_data(real_g *, real_g *, const char [],
				const bool)
{
  // BUG
}

void DLV::volume_data::set_k_point(const int_g, const int_g, const int_g,
				   const int_g, const int_g, const int_g)
{
  // dummy - BUG
}

void DLV::volume_data::unload_data()
{
  fprintf(stderr, "volume data unload not implemented - Todo\n");
}

bool DLV::volume_data::is_displayable() const
{
  return true;
}

bool DLV::volume_data::is_editable() const
{
  return true;
}

DLV::string DLV::volume_data::get_data_label() const
{
  string name = "3D" + get_tags();
  return name;
}

DLV::string DLV::volume_data::get_edit_label() const
{
  string name = "3D" + get_tags();
  return name;
}

DLV::int_g DLV::volume_data::get_number_data_sets() const
{
  return n_data_sets;
}

DLV::string DLV::volume_data::get_sub_data_label(const int_g n) const
{
  return labels[n];
}

DLV::int_g DLV::volume_data::get_display_type() const
{
  return (int)display_3D;
}

DLV::int_g DLV::volume_data::get_edit_type() const
{
  return (int)edit_3D;
}

bool DLV::volume_data::get_sub_is_vector(const int_g n) const
{
  return (vector_dim[n] > 1);
}

bool DLV::real_space_volume::is_kspace() const
{
  return false;
}

bool DLV::k_space_volume::is_kspace() const
{
  return true;
}

bool DLV::volume_data::is_periodic() const
{
  return false;
}

DLV::volume_data *DLV::real_space_volume::create(const char code[],
						 const string src,
						 operation *op) const
{
  real_g o[3];
  real_g a[3];
  real_g b[3];
  real_g c[3];
  get_origin(o);
  get_astep(a);
  get_bstep(b);
  get_cstep(c);
  return new real_space_volume(code, src, op, get_nx(), get_ny(), get_nz(),
			       o, a, b, c);
}

DLV::volume_data *DLV::rspace_periodic_volume::create(const char code[],
						      const string src,
						      operation *op) const
{
  real_g o[3];
  real_g a[3];
  real_g b[3];
  real_g c[3];
  get_origin(o);
  get_astep(a);
  get_bstep(b);
  get_cstep(c);
  return new rspace_periodic_volume(code, src, op, get_nx(), get_ny(),
				    get_nz(), o, a, b, c);
}

bool DLV::rspace_periodic_volume::is_periodic() const
{
  return true;
}

DLV::string DLV::k_space_volume::get_data_label() const
{
  string name = "3Dk" + get_tags();
  return name;
}

DLV::string DLV::k_space_volume::get_edit_label() const
{
  string name = "3Dk" + get_tags();
  return name;
}

DLV::volume_data *DLV::k_space_volume::create(const char code[],
					      const string src,
					      operation *op) const
{
  real_g o[3];
  real_g a[3];
  real_g b[3];
  real_g c[3];
  get_origin(o);
  get_astep(a);
  get_bstep(b);
  get_cstep(c);
  return new k_space_volume(code, src, op, get_nx(), get_ny(), get_nz(),
			    o, a, b, c);
}

DLV::volume_data *DLV::rspace_wavefunction::create(const char code[],
						   const string src,
						   operation *op) const
{
  real_g o[3];
  real_g a[3];
  real_g b[3];
  real_g c[3];
  get_origin(o);
  get_astep(a);
  get_bstep(b);
  get_cstep(c);
  return new rspace_wavefunction(code, src, op, get_nx(), get_ny(), get_nz(),
				 o, a, b, c);
}

void DLV::rspace_wavefunction::add_data(real_g *r, real_g *theta,
					const char label[],
					const bool copy_data)
{
  int_g n = get_ndata_sets();
  int_g s = get_data_size();
  volume_data::add_data(r, 1, label, copy_data);
  if (n == s) {
    real_g **new_phase = new real_g*[s + block_size];
    for (int_g i = 0; i < s; i++)
      new_phase[i] = phases[i];
    if (phases != 0)
      delete [] phases;
    phases = new_phase;
  }
  if (copy_data) {
    int_l l = get_data_length();
    phases[n] = new real_g[l];
    for (int_l i = 0; i < l; i++)
      phases[n][i] = theta[i];
  } else
    phases[n] = theta;
}

DLV::string DLV::rspace_wavefunction::get_data_label() const
{
  string name = "Local function" + get_tags();
  return name;
}

DLV::string DLV::rspace_wavefunction::get_edit_label() const
{
  string name = "Local function" + get_tags();
  return name;
}

DLV::int_g DLV::rspace_wavefunction::get_edit_type() const
{
  // Todo - need think about this, no_edit makes it safe for the moment.
  return no_edit;
}

DLV::volume_data *DLV::bloch_wavefunction::create(const char code[],
						  const string src,
						  operation *op) const
{
  real_g o[3];
  real_g a[3];
  real_g b[3];
  real_g c[3];
  get_origin(o);
  get_astep(a);
  get_bstep(b);
  get_cstep(c);
  return new bloch_wavefunction(code, src, op, get_nx(), get_ny(), get_nz(),
				o, a, b, c);
}

void DLV::bloch_wavefunction::set_k_point(const int_g k1, const int_g k2,
					  const int_g k3, const int_g s1,
					  const int_g s2, const int_g s3)
{
  // common denominators
  int_g j = 1;
  if (k1 == 0)
    j = s1;
  else {
    for (int_g i = 2; i <= k1; i++)
      if ((k1 % i) == 0 and (s1 % i) == 0)
	j = i;
  }
  k_num[0] = k1 / j;
  k_den[0] = s1 / j;
  j = 1;
  if (k2 == 0)
    j = s2;
  else {
    for (int_g i = 2; i <= k2; i++)
      if ((k2 % i) == 0 and (s2 % i) == 0)
	j = i;
  }
  k_num[1] = k2 / j;
  k_den[1] = s2 / j;
  j = 1;
  if (k3 == 0)
    j = s3;
  else {
    for (int_g i = 2; i <= k3; i++)
      if ((k3 % i) == 0 and (s3 % i) == 0)
	j = i;
  }
  k_num[2] = k3 / j;
  k_den[2] = s3 / j;
}

DLV::string DLV::bloch_wavefunction::get_data_label() const
{
  string name = "Bloch function, k =";
  char buff[128];
  if (k_num[0] != 0)
    snprintf(buff, 128, " %1d/%1d", k_num[0], k_den[0]);
  else
    snprintf(buff, 128, " 0");
  name += buff;
  if (k_num[1] != 0)
    snprintf(buff, 128, " %1d/%1d", k_num[1], k_den[1]);
  else
    snprintf(buff, 128, " 0");
  name += buff;
  if (k_num[2] != 0)
    snprintf(buff, 128, " %1d/%1d", k_num[2], k_den[2]);
  else
    snprintf(buff, 128, " 0");
  name += buff;
  name += "," + get_tags();
  return name;
}

DLV::string DLV::bloch_wavefunction::get_edit_label() const
{
  return get_data_label();
}

bool DLV::bloch_wavefunction::is_periodic() const
{
  return true;
}

#ifdef ENABLE_DLV_GRAPHICS

DLV::drawable_obj *
DLV::volume_data::create_drawable(const render_parent *parent,
				  char message[], const int_g mlen)
{
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    bool use_ignore = false;
    real_g ignore = 0.0;
    if (is_kspace()) {
      use_ignore = true;
      ignore = (real_g)(10000.0 * Hartree_to_eV);
    }
    obj = DLV::drawable_obj::create_volume(parent, origin, a_step, b_step,
					   c_step, data, nx, ny, nz,
					   vector_dim, labels, n_data_sets,
					   is_kspace(), use_ignore,
					   ignore, message, mlen);
    if (obj != 0)
      set_drawable(obj);
  }
  return obj;
}

DLVreturn_type DLV::box::render(const render_parent *parent,
				class model *structure,
				char message[], const int_g mlen)
{
  //return render(parent, structure, 0, 0, 0, message, mlen);
  return DLV_OK;
}

bool DLV::box::hide_render(const int_g object, const int_g visible,
			   const render_parent *parent,
			   class model *structure,
			   char message[], const int_g mlen)
{
  // This routine is now badly named
  if (visible == 1) {
    message[0] = '\0';
    drawable_obj *obj = get_drawable();
    obj->update_box(origin, pointa, pointb, pointc);
    reattach_objects(parent);
  }
  return true;
}

DLVreturn_type DLV::box::render(const render_parent *parent,
				class model *structure,
				const int_g component, const int_g method,
				const int_g index, char message[],
				const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    obj = DLV::drawable_obj::create_box_data(parent, is_kspace(),
					     message, mlen);
    if (obj == 0)
      return DLV_ERROR;
    else
      set_drawable(obj);
  }
  if (count_display_objects() == 0) {
    display_obj *ptr = 0;
    ptr = display_obj::create_3D_region(parent, obj, name, index, is_kspace(),
					message, mlen);
    if (ptr == 0) {
      if (strlen(message) == 0)
	strncpy(message, "Unable to allocate display object", mlen);
      return DLV_ERROR;
    } else {
      add_display_obj(ptr);
      ptr->update_display_list(parent, name);
      ptr->attach_params();
    }
  }
  return DLV_OK;
}

DLVreturn_type DLV::box::use_view_editor(const render_parent *parent,
					 const bool v, char message[],
					 const int_g mlen)
{
  display_obj *ptr = get_display_obj();
  if (ptr == 0) {
    strncpy(message, "3D region is not displayed", mlen);
    return DLV_ERROR;
  } else {
    //bool ok = true;
    real_g matrix[4][4];
    real_g translate[3];
    real_g centre[3];
    if (is_kspace())
      (void) ptr->attach_editor(v, parent->get_kedit(), matrix, translate,
				centre, message, mlen);
    else
      (void) ptr->attach_editor(v, parent->get_redit(), matrix, translate,
				centre, message, mlen);
    if (!v) {
      ptr->transform_point(origin, matrix, translate, centre);
      ptr->transform_point(pointa, matrix, translate, centre);
      ptr->transform_point(pointb, matrix, translate, centre);
      ptr->transform_point(pointc, matrix, translate, centre);
      drawable_obj *obj = get_drawable();
      if (obj == 0) {
	strncpy(message, "BUG: missing drawable for 3D region", mlen);
	return DLV_ERROR;
      } else
	obj->update_box(origin, pointa, pointb, pointc);
    }
    return DLV_OK;
  }
}

DLVreturn_type DLV::box::update3D(const int_g method, const int_g h,
				  const int_g k, const int_g l, const real_g x,
				  const real_g y, const real_g z,
				  const int_g object, const bool conv,
				  const bool frac, const bool p_obj,
				  model *const m, char message[],
				  const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    strncpy(message, "BUG: missing drawable for 3D region", mlen);
    ok = DLV_ERROR;
  } else {
    if (method < 6) {
      coord_type atom1[3];
      coord_type atom2[3];
      if (!m->get_selected_positions(atom1, atom2)) {
	strncpy(message, "At least two atoms must be selected", mlen);
	return DLV_WARNING;
      }
      if (method == 1) {
	pointa[0] = origin[0] + (real_g)(atom1[0] - atom2[0]);
	pointa[1] = origin[1] + (real_g)(atom1[1] - atom2[1]);
	pointa[2] = origin[2] + (real_g)(atom1[2] - atom2[2]);
      } else if (method == 3) {
	pointb[0] = origin[0] + (real_g)(atom1[0] - atom2[0]);
	pointb[1] = origin[1] + (real_g)(atom1[1] - atom2[1]);
	pointb[2] = origin[2] + (real_g)(atom1[2] - atom2[2]);
      } else if (method == 5) {
	pointc[0] = origin[0] + (real_g)(atom1[0] - atom2[0]);
	pointc[1] = origin[1] + (real_g)(atom1[1] - atom2[1]);
	pointc[2] = origin[2] + (real_g)(atom1[2] - atom2[2]);
      } else {
	real_g offset[3];
	for (int_g i = 0; i < 3; i++)
	  offset[i] = (real_g)atom2[i] - origin[i];
	origin[0] = (real_g)atom2[0];
	origin[1] = (real_g)atom2[1];
	origin[2] = (real_g)atom2[2];
	if (method == 0) {
	  pointa[0] = (real_g)atom1[0];
	  pointa[1] = (real_g)atom1[1];
	  pointa[2] = (real_g)atom1[2];
	  pointb[0] += offset[0];
	  pointb[1] += offset[1];
	  pointb[2] += offset[2];
	  pointc[0] += offset[0];
	  pointc[1] += offset[1];
	  pointc[2] += offset[2];
	} else if (method == 2) {
	  pointb[0] = (real_g)atom1[0];
	  pointb[1] = (real_g)atom1[1];
	  pointb[2] = (real_g)atom1[2];
	  pointa[0] += offset[0];
	  pointa[1] += offset[1];
	  pointa[2] += offset[2];
	  pointc[0] += offset[0];
	  pointc[1] += offset[1];
	  pointc[2] += offset[2];
	} else {
	  pointc[0] = (real_g)atom1[0];
	  pointc[1] = (real_g)atom1[1];
	  pointc[2] = (real_g)atom1[2];
	  pointa[0] += offset[0];
	  pointa[1] += offset[1];
	  pointa[2] += offset[2];
	  pointb[0] += offset[0];
	  pointb[1] += offset[1];
	  pointb[2] += offset[2];
	}
      }
    } else if (method == 6) {
      // average atom positions and set origin
      coord_type position[3];
      if (m->get_average_selection_position(position)) {
	real_g offset[3];
	for (int_g i = 0; i < 3; i++)
	  offset[i] = (real_g)position[i] - origin[i];
	origin[0] = (real_g)position[0];
	origin[1] = (real_g)position[1];
	origin[2] = (real_g)position[2];
	pointa[0] += offset[0];
	pointa[1] += offset[1];
	pointa[2] += offset[2];
	pointb[0] += offset[0];
	pointb[1] += offset[1];
	pointb[2] += offset[2];
	pointc[0] += offset[0];
	pointc[1] += offset[1];
	pointc[2] += offset[2];
      } else {
	strncpy(message, "At least one atom must be selected", mlen);
	return DLV_WARNING;
      }
    } else if (method == 7) {
      // average atom positions and set origin so this is the centre
      coord_type position[3];
      if (m->get_average_selection_position(position)) {
	real_g average[3];
	// Need average of 8 corners
	for (int_g i = 0; i < 3; i++)
	  average[i] = (pointa[i] + pointb[i] + pointc[i] - origin[i]) / 2.0f;
	/*average[i] = (origin[i] + pointa[i] + pointb[i] + pointc[i]
	  + pointa[i] + pointb[i] - origin[i]
	  + pointa[i] + pointc[i] - origin[i]
	  + pointb[i] + pointc[i] - origin[i]
	  + pointa[i] + pointb[i] + pointc[i] -  2.0 * origin[i])/8.0;*/
	real_g offset[3];
	for (int_g i = 0; i < 3; i++)
	  offset[i] = (real_g)position[i] - average[i];
	origin[0] += offset[0];
	origin[1] += offset[1];
	origin[2] += offset[2];
	pointa[0] += offset[0];
	pointa[1] += offset[1];
	pointa[2] += offset[2];
	pointb[0] += offset[0];
	pointb[1] += offset[1];
	pointb[2] += offset[2];
	pointc[0] += offset[0];
	pointc[1] += offset[1];
	pointc[2] += offset[2];
      } else {
	strncpy(message, "At least one atom must be selected", mlen);
	return DLV_WARNING;
      }
    } else if (method == 8) {
      // Use miller indices
      if (h == 0 and k == 0 and l == 0) {
	strncpy(message, "At least one miller index must be non-zero", mlen);
	return DLV_WARNING;
      }
      coord_type a[3];
      coord_type b[3];
      coord_type c[3];
      //int_g dim;
      bool has_rotation = true;
      if (p_obj) {
	if (is_kspace()) {
	  strncpy(message, "K space lattice mapping not implemented", mlen);
	  ok = DLV_WARNING;
	} else {
	  real_l r[3][3];
	  operation *op = operation::find_parent_geometry(r, has_rotation);
	  if (op == 0) {
	    strncpy(message, "Model doesn't have a parent lattice", mlen);
	    ok = DLV_WARNING;
	  } else if (!has_rotation) {
	    strncpy(message,
		    "Lattice mapping not implemented for geometry edit", mlen);
	    ok = DLV_WARNING;
	  } else {
	    coord_type aa[3];
	    coord_type bb[3];
	    coord_type cc[3];
	    if (is_kspace())
	      op->get_model()->get_reciprocal_lattice(aa, bb, cc);
	    else if (conv)
	      op->get_model()->get_conventional_lattice(aa, bb, cc);
	    else
	      op->get_model()->get_primitive_lattice(aa, bb, cc);
	    //dim = op->get_model()->get_number_of_periodic_dims();
	    for (int i = 0; i < 3; i++) {
	      a[i] = r[i][0] * aa[0] + r[i][1] * aa[1] + r[i][2] * aa[2];
	      b[i] = r[i][0] * bb[0] + r[i][1] * bb[1] + r[i][2] * bb[2];
	      c[i] = r[i][0] * cc[0] + r[i][1] * cc[1] + r[i][2] * cc[2];
	    }
	  }
	}
      } else {
	if (is_kspace())
	  m->get_reciprocal_lattice(a, b, c);
	else if (conv)
	  m->get_conventional_lattice(a, b, c);
	else
	  m->get_primitive_lattice(a, b, c);
	//dim = m->get_number_of_periodic_dims();
      }
      origin[0] = 0.0;
      origin[1] = 0.0;
      origin[2] = 0.0;
      real_g t;
      if (h == 0 and k == 0) {
	t = 1.0f / (real_g) l;
	for (int_g i = 0; i < 3; i++) {
	  origin[i] = t * (real_g)c[i];
	  pointa[i] = origin[i] + (real_g)a[i];
	  pointb[i] = origin[i] + (real_g)b[i];
	}
      } else if (h == 0 and l == 0) {
	t = 1.0f / (real_g) k;
	for (int_g i = 0; i < 3; i++) {
	  origin[i] = t * (real_g)b[i];
	  pointa[i] = origin[i] + (real_g)a[i];
	  pointb[i] = origin[i] + (real_g)c[i];
	}
      } else if (k == 0 and l == 0) {
	t = 1.0f / (real_g) h;
	for (int_g i = 0; i < 3; i++) {
	  origin[i] = t * (real_g)a[i];
	  pointa[i] = origin[i] + (real_g)b[i];
	  pointb[i] = origin[i] + (real_g)c[i];
	}
      } else if (h == 0) {
	t = 1.0f / (real_g) k;
	for (int_g i = 0; i < 3; i++) {
	  origin[i] = t * (real_g)b[i];
	  pointa[i] = origin[i] + (real_g)a[i];
	}
	t = 1.0f / (real_g) l;
	for (int_g i = 0; i < 3; i++)
	  pointb[i] = t * (real_g)c[i];
      } else if (k == 0) {
	t = 1.0f / (real_g) h;
	for (int_g i = 0; i < 3; i++) {
	  origin[i] = t * (real_g)a[i];
	  pointa[i] = origin[i] + (real_g)b[i];
	}
	t = 1.0f / (real_g) l;
	for (int_g i = 0; i < 3; i++)
	  pointb[i] = t * (real_g)c[i];
      } else if (l == 0) {
	t = 1.0f / (real_g) h;
	for (int_g i = 0; i < 3; i++) {
	  origin[i] = t * (real_g)a[i];
	  pointa[i] = origin[i] + (real_g)c[i];
	}
	t = 1.0f / (real_g) k;
	for (int_g i = 0; i < 3; i++)
	  pointb[i] = t * (real_g)b[i];
      } else {
	t = 1.0f / (real_g) h;
	for (int_g i = 0; i < 3; i++)
	  origin[i] = t * (real_g)a[i];
	t = 1.0f / (real_g) k;
	for (int_g i = 0; i < 3; i++)
	  pointa[i] = t * (real_g)b[i];
	t = 1.0f / (real_g) l;
	for (int_g i = 0; i < 3; i++)
	  pointb[i] = t * (real_g)c[i];
      }
      // point c needs to be perpendicular to OAB (I suppose)
      real_g oa[3];
      oa[0] = pointa[0] - origin[0];
      oa[1] = pointa[1] - origin[1];
      oa[2] = pointa[2] - origin[2];
      real_g ob[3];
      ob[0] = pointb[0] - origin[0];
      ob[1] = pointb[1] - origin[1];
      ob[2] = pointb[2] - origin[2];
      pointc[0] = oa[1] * ob[2] - oa[2] * ob[1];
      pointc[1] = oa[2] * ob[0] - oa[0] * ob[2];
      pointc[2] = oa[0] * ob[1] - oa[1] * ob[0];
      // nomalise to average length of lattice vectors
      real_g la = (real_g)std::sqrt(a[0] * a[0] + a[1] * a[1] + a[2] * a[2]);
      real_g lb = (real_g)std::sqrt(b[0] * b[0] + b[1] * b[1] + b[2] * b[2]);
      real_g lc = (real_g)std::sqrt(c[0] * c[0] + c[1] * c[1] + c[2] * c[2]);
      real_g length = (la + lb + lc) / 3.0f;
      lc = std::sqrt(pointc[0] * pointc[0] + pointc[1] * pointc[1]
		     + pointc[2] * pointc[2]);
      real_g scale = length / lc;
      for (int_g i = 0; i < 3; i++)
	pointc[i] = origin[i] + pointc[i] * scale;
    } else if (method == 9 or method == 11) {
      if (method == 11) {
	// set OABC to lattice
	origin[0] = 0.0;
	origin[1] = 0.0;
	origin[2] = 0.0;
      }
      coord_type va[3];
      coord_type vb[3];
      coord_type vc[3];
      //int_g dim;
      bool has_rotation = true;
      if (p_obj) {
	if (is_kspace()) {
	  strncpy(message, "K space lattice mapping not implemented", mlen);
	  ok = DLV_WARNING;
	} else {
	  real_l r[3][3];
	  operation *op = operation::find_parent_geometry(r, has_rotation);
	  if (op == 0) {
	    strncpy(message, "Model doesn't have a parent lattice", mlen);
	    ok = DLV_WARNING;
	  } else if (!has_rotation) {
	    strncpy(message,
		    "Lattice mapping not implemented for geometry edit", mlen);
	    ok = DLV_WARNING;
	  } else {
	    coord_type aa[3];
	    coord_type bb[3];
	    coord_type cc[3];
	    if (is_kspace())
	      op->get_model()->get_reciprocal_lattice(aa, bb, cc);
	    else if (conv)
	      op->get_model()->get_conventional_lattice(aa, bb, cc);
	    else
	      op->get_model()->get_primitive_lattice(aa, bb, cc);
	    //dim = op->get_model()->get_number_of_periodic_dims();
	    for (int i = 0; i < 3; i++) {
	      va[i] = r[i][0] * aa[0] + r[i][1] * aa[1] + r[i][2] * aa[2];
	      vb[i] = r[i][0] * bb[0] + r[i][1] * bb[1] + r[i][2] * bb[2];
	      vc[i] = r[i][0] * cc[0] + r[i][1] * cc[1] + r[i][2] * cc[2];
	    }
	  }
	}
      } else {
	if (is_kspace())
	  m->get_reciprocal_lattice(va, vb, vc);
	else if (conv)
	  m->get_conventional_lattice(va, vb, vc);
	else
	  m->get_primitive_lattice(va, vb, vc);
	//dim = m->get_number_of_periodic_dims();
      }
      // align ABC to lattice vectors
      switch (m->get_number_of_periodic_dims()) {
      case 0:
	if (method == 11) {
	  va[0] = 1.0;
	  va[1] = 0.0;
	  va[2] = 0.0;
	} else {
	  va[0] = pointa[0] - origin[0];
	  va[1] = pointa[1] - origin[1];
	  va[2] = pointa[2] - origin[2];
	}
      case 1:
	if (method == 11) {
	  vb[0] = 0.0;
	  vb[1] = 1.0;
	  vb[2] = 0.0;
	} else {
	  vb[0] = pointb[0] - origin[0];
	  vb[1] = pointb[1] - origin[1];
	  vb[2] = pointb[2] - origin[2];
	}
      case 2:
	if (method == 11) {
	  vc[0] = 0.0;
	  vc[1] = 0.0;
	  vc[2] = 1.0;
	} else {
	  vc[0] = pointc[0] - origin[0];
	  vc[1] = pointc[1] - origin[1];
	  vc[2] = pointc[2] - origin[2];
	}
	break;
      }
      pointa[0] = origin[0] + (real_g)va[0];
      pointa[1] = origin[1] + (real_g)va[1];
      pointa[2] = origin[2] + (real_g)va[2];
      pointb[0] = origin[0] + (real_g)vb[0];
      pointb[1] = origin[1] + (real_g)vb[1];
      pointb[2] = origin[2] + (real_g)vb[2];
      pointc[0] = origin[0] + (real_g)vc[0];
      pointc[1] = origin[1] + (real_g)vc[1];
      pointc[2] = origin[2] + (real_g)vc[2];
    } else if (method == 10 or method == 12) {
      if (method == 12) {
	// set OABC to XYZ
	origin[0] = 0.0;
	origin[1] = 0.0;
	origin[2] = 0.0;
      }
      // align ABC aint_l XYZ
      pointa[0] = origin[0] + 1.0f;
      pointa[1] = origin[1];
      pointa[2] = origin[2];
      pointb[0] = origin[0];
      pointb[1] = origin[1] + 1.0f;
      pointb[2] = origin[2];
      pointc[0] = origin[0];
      pointc[1] = origin[1];
      pointc[2] = origin[2] + 1.0f;
    } else if (method < 18) {
      coord_type position[3];
      position[0] = x;
      position[1] = y;
      position[2] = z;
      if (frac) {
	coord_type a[3];
	coord_type b[3];
	coord_type c[3];
	int_g dim;
	bool has_rotation = true;
	if (p_obj) {
	  if (is_kspace()) {
	    strncpy(message, "K space lattice mapping not implemented", mlen);
	    ok = DLV_WARNING;
	  } else {
	    real_l r[3][3];
	    operation *op = operation::find_parent_geometry(r, has_rotation);
	    if (op == 0) {
	      strncpy(message, "Model doesn't have a parent lattice", mlen);
	      ok = DLV_WARNING;
	    } else if (!has_rotation) {
	      strncpy(message,
		      "Lattice mapping not implemented for geometry edit",
		      mlen);
	      ok = DLV_WARNING;
	    } else {
	      coord_type aa[3];
	      coord_type bb[3];
	      coord_type cc[3];
	      if (is_kspace())
		op->get_model()->get_reciprocal_lattice(aa, bb, cc);
	      else if (conv)
		op->get_model()->get_conventional_lattice(aa, bb, cc);
	      else
		op->get_model()->get_primitive_lattice(aa, bb, cc);
	      dim = op->get_model()->get_number_of_periodic_dims();
	      for (int i = 0; i < 3; i++) {
		a[i] = r[i][0] * aa[0] + r[i][1] * aa[1] + r[i][2] * aa[2];
		b[i] = r[i][0] * bb[0] + r[i][1] * bb[1] + r[i][2] * bb[2];
		c[i] = r[i][0] * cc[0] + r[i][1] * cc[1] + r[i][2] * cc[2];
	      }
	    }
	  }
	} else {
	  dim = m->get_number_of_periodic_dims();
	  if (is_kspace())
	    m->get_reciprocal_lattice(a, b, c);
	  else if (conv)
	    m->get_conventional_lattice(a, b, c);
	  else
	    m->get_primitive_lattice(a, b, c);
	}
	if (ok == DLV_OK) {
	  // loop to dim should mean that non-periodic ones shouldn't change
	  // from original value set.
	  for (int_g i = 0; i < dim; i++)
	    position[i] = a[i] * x;
	  if (dim > 1) {
	    for (int_g i = 0; i < dim; i++)
	      position[i] += b[i] * y;
	    if (dim > 2) {
	      for (int_g i = 0; i < dim; i++)
		position[i] += c[i] * z;
	    }
	  }
	}
      }
      if (method == 13) {
	pointa[0] = position[0];
	pointa[1] = position[1];
	pointa[2] = position[2];
      } else if (method == 14) {
	pointb[0] = position[0];
	pointb[1] = position[1];
	pointb[2] = position[2];
      } else if (method == 15) {
	pointc[0] = position[0];
	pointc[1] = position[1];
	pointc[2] = position[2];
      } else if (method == 16) {
	real_g offset[3];
	for (int_g i = 0; i < 3; i++)
	  offset[i] = (real_g)position[i] - origin[i];
	origin[0] = (real_g)position[0];
	origin[1] = (real_g)position[1];
	origin[2] = (real_g)position[2];
	pointa[0] += offset[0];
	pointa[1] += offset[1];
	pointa[2] += offset[2];
	pointb[0] += offset[0];
	pointb[1] += offset[1];
	pointb[2] += offset[2];
	pointc[0] += offset[0];
	pointc[1] += offset[1];
	pointc[2] += offset[2];
      } else if (method == 17) {
	real_g average[3];
	// Need average of 8 corners
	for (int_g i = 0; i < 3; i++)
	  average[i] = (pointa[i] + pointb[i] + pointc[i] - origin[i]) / 2.0f;
	real_g offset[3];
	for (int_g i = 0; i < 3; i++)
	  offset[i] = (real_g)position[i] - average[i];
	origin[0] += offset[0];
	origin[1] += offset[1];
	origin[2] += offset[2];
	pointa[0] += offset[0];
	pointa[1] += offset[1];
	pointa[2] += offset[2];
	pointb[0] += offset[0];
	pointb[1] += offset[1];
	pointb[2] += offset[2];
	pointc[0] += offset[0];
	pointc[1] += offset[1];
	pointc[2] += offset[2];
      }
    } else {
      data_object *l = operation::find_3D_region(object);
      if (l == 0) {
	strncpy(message, "Failed to find selected 3D region", mlen);
	return DLV_WARNING;
      } else { // 18 - inherit
	box *ldata = dynamic_cast<box *>(l);
	if (ldata == 0) {
	  strncpy(message, "Selected object isn't 3D region", mlen);
	  return DLV_WARNING;
	} else {
	  origin[0] = ldata->origin[0];
	  origin[1] = ldata->origin[1];
	  origin[2] = ldata->origin[2];
	  pointa[0] = ldata->pointa[0];
	  pointa[1] = ldata->pointa[1];
	  pointa[2] = ldata->pointa[2];
	  pointb[0] = ldata->pointb[0];
	  pointb[1] = ldata->pointb[1];
	  pointb[2] = ldata->pointb[2];
	  pointc[0] = ldata->pointc[0];
	  pointc[1] = ldata->pointc[1];
	  pointc[2] = ldata->pointc[2];
	}
      }
    }
    obj->update_box(origin, pointa, pointb, pointc);
  }
  return ok;
}

DLVreturn_type DLV::sphere::render(const render_parent *parent,
				   class model *structure,
				   char message[], const int_g mlen)
{
  //return render(parent, structure, 0, 0, 0, message, mlen);
  return DLV_OK;
}

bool DLV::sphere::hide_render(const int_g object, const int_g visible,
			      const render_parent *parent,
			      class model *structure,
			      char message[], const int_g mlen)
{
  // This routine is now badly named
  if (visible == 1) {
    message[0] = '\0';
    drawable_obj *obj = get_drawable();
    obj->update_sphere(origin, radius);
    reattach_objects(parent);
  }
  return true;
}

DLVreturn_type DLV::sphere::render(const render_parent *parent,
				   class model *structure,
				   const int_g component, const int_g method,
				   const int_g index, char message[],
				   const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    obj = DLV::drawable_obj::create_sphere_data(parent, is_kspace(),
						message, mlen);
    if (obj == 0)
      return DLV_ERROR;
    else
      set_drawable(obj);
  }
  if (count_display_objects() == 0) {
    display_obj *ptr = 0;
    ptr = display_obj::create_sphere_obj(parent, obj, name, index, false,
					 message, mlen);
    if (ptr == 0) {
      if (strlen(message) == 0)
	strncpy(message, "Unable to allocate display object", mlen);
      return DLV_ERROR;
    } else {
      add_display_obj(ptr);
      ptr->update_display_list(parent, name);
      ptr->attach_params();
    }
  }
  return DLV_OK;
}

DLVreturn_type DLV::sphere::update3D(const int_g method, const real_g r,
				     model *const m, char message[],
				     const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    strncpy(message, "BUG: missing drawable for sphere", mlen);
    ok = DLV_ERROR;
  } else {
    if (method == 0) {
      coord_type centre[3];
      if (!m->get_average_selection_position(centre)) {
	strncpy(message, "At least one atom must be selected", mlen);
	return DLV_WARNING;
      }
      origin[0] = (real_g)centre[0];
      origin[1] = (real_g)centre[1];
      origin[2] = (real_g)centre[2];
    } else
      radius = r;
    obj->update_sphere(origin, radius);
  }
  return ok;
}

DLVreturn_type DLV::wulff_plot::render(const render_parent *parent,
				       class model *structure,
				       char message[], const int_g mlen)
{
  //return render(parent, structure, 0, 0, 0, message, mlen);
  return DLV_OK;
}

DLVreturn_type DLV::wulff_plot::render(const render_parent *parent,
				       class model *structure,
				       const int_g component,
				       const int_g method, const int_g index,
				       char message[], const int_g mlen)
{
  message[0] = '\0';
  if (planes.size() == 0) {
    strncpy(message, "No data to produce Wulff plot", mlen);
    return DLV_ERROR;
  }
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    int_g nverts;
    int_l *connects;
    int_l nconnects;
    real_g (*vertices)[3];
    real_g (*colours)[3];
    real_g (*normals)[3];
    real_g (*labelpos)[3];
    string *labels;
    int_g nnodes;
    int_g *nodes;
    real_g (*line_verts)[3];
    int_g nline_verts;
    int_l *lines;
    int_l nlines;
    structure->generate_wulff_plot(planes, vertices, nverts, colours, normals,
				   labelpos, labels, connects, nconnects,
				   nodes, nnodes, line_verts, nline_verts,
				   lines, nlines);
    if (nverts == 0) {
      // no arrays were allocated
      strncpy(message, "No vertices for Wulff plot", mlen);
      return DLV_ERROR;
    } 
    obj = DLV::drawable_obj::create_wulff_data(parent, vertices, nverts,
					       colours, normals, connects,
					       nconnects, labelpos, labels,
					       nodes, nnodes, line_verts,
					       nline_verts, lines, nlines,
					       message, mlen);
    delete [] lines;
    delete [] line_verts;
    delete [] nodes;
    delete [] labels;
    delete [] labelpos;
    delete [] normals;
    delete [] colours;
    delete [] vertices;
    delete [] connects;
    if (obj == 0)
      return DLV_ERROR;
    else
      set_drawable(obj);
    display_obj *ptr = 0;
    ptr = display_obj::create_wulff_region(parent, obj, name, index,
					   message, mlen);
    if (ptr == 0) {
      if (strlen(message) == 0)
	strncpy(message, "Unable to allocate display object", mlen);
      return DLV_ERROR;
    } else {
      add_display_obj(ptr);
      ptr->update_display_list(parent, name);
      ptr->attach_params();
    }
  }
  return DLV_OK;
}

DLVreturn_type DLV::wulff_plot::update_wulff(const int_g h, const int_g k,
					     const int_g l, const real_g e,
					     const real_g r, const real_g g,
					     const real_g b,const bool conv,
					     model *const m, char message[],
					     const int_g mlen)
{
  if ((h == 0 and k == 0 and l == 0) or (std::abs(e) < 0.0001)) {
    strncpy(message, "(0 0 0) or 0 energy not allowed", mlen);
    return DLV_ERROR;
  }
  wulff_data d;
  d.h = h;
  d.k = k;
  d.l = l;
  d.energy = e;
  d.red = r;
  d.green = g;
  d.blue = b;
  d.conventional = conv;
  d.termination = -1;
  planes.push_back(d);
  return DLV_OK;
}

DLVreturn_type DLV::wulff_plot::update_wulff(const char filename[],
					     model *const m, char message[],
					     const int_g mlen)
{
  DLVreturn_type ok = DLV_OK;
  if (check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (open_file_read(input, filename, message, mlen)) {
      bool conv = true;
      float r, g, b, e;
      wulff_data d;
      char line[128];
      while (!input.eof()) {
	input.getline(line, 128);
	if (line[0] == '\0')
	  break;
	if (strncmp(line, "prim", 4) == 0)
	  conv = false;
	else if (strncmp(line, "conv", 4) == 0)
	  conv = true;
	else {
	  int t;
	  int i = sscanf(line, "%d %d %d %f %f %f %f %d", &d.h, &d.k, &d.l,
			 &e, &r, &g, &b, &t);
	  if (i == 7 or i == 8) {
	    d.energy = e;
	    d.red = r;
	    d.green = g;
	    d.blue = b;
	    d.conventional = conv;
	    if (i == 8)
	      d.termination = t;
	    else
	      d.termination = -1;
	    planes.push_back(d);
	  } else {
	    strcpy(message, "Problem on line = ");
	    strncat(message, line, mlen - 30);
	    ok = DLV_ERROR;
	  }
	}
      }
      input.close();
    } else
      ok = DLV_ERROR;
  } else
    ok = DLV_ERROR;
  return ok;
}

void DLV::volume_data::reset_data_sets()
{
  n_data_sets = 0;
}

DLV::data_object *DLV::volume_data::edit(const render_parent *parent,
					 class model *structure,
					 const int_g component,
					 const int_g method, const int_g index,
					 char message[], const int_g mlen)
{
  message[0] = '\0';
  edit3D_object *ptr = 0;
  bool data_ok = reload_data(this, message, mlen);
  if (strlen(message) > 0)
    return ptr;
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    bool use_ignore = false;
    real_g ignore = 0.0;
    if (is_kspace()) {
      use_ignore = true;
      ignore = (real_g)(10000.0 * Hartree_to_eV);
    }
    obj = DLV::drawable_obj::create_volume(parent, origin, a_step, b_step,
					   c_step, data, nx, ny, nz,
					   vector_dim, labels, n_data_sets,
					   is_kspace(), use_ignore, ignore,
					   message, mlen);
    if (obj == 0)
      return ptr;
    else
      set_drawable(obj);
  } else if (!data_ok) {
    bool use_ignore = false;
    real_g ignore = 0.0;
    if (is_kspace()) {
      use_ignore = true;
      ignore = (real_g)(10000.0 * Hartree_to_eV);
    }
    obj->reload_data(parent, origin, a_step, b_step, c_step, data,
		     nx, ny, nz, vector_dim, labels, n_data_sets,
		     is_kspace(), use_ignore, ignore, message, mlen);
  }
  // Now setup the edited object, the drawable is the output obj
  string name = "Orthoslice of ";
  string basename = get_data_label();
  basename += " ";
  basename += labels[component];
  int_g m = method;
  // vectors don't use extend
  if (get_sub_is_vector(component) and method > 0)
    m++;
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  structure->get_primitive_lattice(a, b, c);
  int_g dim = structure->get_number_of_periodic_dims();
  switch (m) {
  case 0:
    name = "Orthoslice of ";
    name += basename;
    ptr = edit3D_ortho::create(parent, this, obj, is_kspace(), name,
			       index, message, mlen);
    break;
  case 1:
    name = "Extension of ";
    name += basename;
    ptr = edit3D_extend::create(parent, this, obj, component, is_kspace(),
				name, index, a, b, c, dim, message, mlen);
    break;
  case 2:
    name = "Slice of ";
    name += basename;
    ptr = edit3D_slice::create(parent, this, obj, component, is_kspace(), name,
			       index, message, mlen);
    break;
  case 3:
    name = "Clamp of ";
    name += basename;
    ptr = edit3D_clamp::create(parent, this, obj, component, is_kspace(), name,
			       index, message, mlen);
    break;
  case 4:
    name = "Downsize of ";
    name += basename;
    ptr = edit3D_downsize::create(parent, this, obj, component, is_kspace(),
				  name, index, message, mlen);
    break;
  case 5:
    name = "Crop of ";
    name += basename;
    ptr = edit3D_crop::create(parent, this, obj, component, is_kspace(), name,
			      index, message, mlen);
    break;
  case 6:
    name = "Cut of ";
    name += basename;
    ptr = edit3D_cut::create(parent, this, obj, component, is_kspace(), name,
			     index, message, mlen);
    break;
  case 7:
    name = "Math of ";
    name += basename;
    ptr = edit3D_math::create(parent, this, obj, component, is_kspace(), name,
			      index, message, mlen);
    break;
  case 8:
    name = "Vol of ";
    name += basename;
    ptr = edit3D_vol_render::create(parent, this, obj, component, is_kspace(),
				    name, index, message, mlen);
    break;
  default:
    strncpy(message, "BUG: Unrecognised volume display method", mlen);
    break;
  }
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate data edit object", mlen);
  }
  return ptr;
}

DLV::edit3D_ortho *DLV::edit3D_ortho::create(const render_parent *parent,
					     data_object *data,
					     const drawable_obj *obj,
					     const bool kspace,
					     const string name,
					     const int_g index,
					     char message[], const int_g mlen)
{
  edited_obj *ptr = edited_obj::create_orthoslice(parent, obj, kspace,
						  message, mlen);
  if (ptr == 0)
    return 0;
  else {
    edit3D_ortho *edit = new edit3D_ortho(data, ptr, index, name);
    if (edit == 0)
      strncpy(message, "Failed to allocate new data object", mlen);
    else {
      edit->set_drawable(ptr->get_drawable());
      data->add_edit(ptr);
      // Do these need to happen after the new op is added?
      ptr->update_edit_list(parent, name, index);
      ptr->attach_params();
    }
    return edit;
  }
}

bool DLV::edit3D_ortho::is_displayable() const
{
  return true;
}

bool DLV::edit3D_ortho::is_editable() const
{
  return false;
}

DLV::string DLV::edit3D_ortho::get_data_label() const
{
  string name = "2D orthoslice of " + get_base_data()->get_data_label();
  return name;
}

DLV::string DLV::edit3D_ortho::get_edit_label() const
{
  return get_data_label();
}

DLV::int_g DLV::edit3D_ortho::get_number_data_sets() const
{
  return get_base_data()->get_number_data_sets();
}

DLV::string DLV::edit3D_ortho::get_sub_data_label(const int_g n) const
{
  return get_base_data()->get_sub_data_label(n);
}

DLV::int_g DLV::edit3D_ortho::get_display_type() const
{
  return (int)display_2D;
}

DLV::int_g DLV::edit3D_ortho::get_edit_type() const
{
  return edit_2D;
}

bool DLV::edit3D_ortho::get_sub_is_vector(const int_g n) const
{
  return get_base_data()->get_sub_is_vector(n);
}

DLV::edit3D_slice *DLV::edit3D_slice::create(const render_parent *parent,
					     data_object *data,
					     const drawable_obj *obj,
					     const int_g component,
					     const bool kspace,
					     const string name,
					     const int_g index,
					     char message[], const int_g mlen)
{
  edited_obj *ptr = edited_obj::create_slice(parent, obj, kspace,
					     message, mlen);
  if (ptr == 0)
    return 0;
  else {
    edit3D_slice *edit = new edit3D_slice(data, ptr, index, name);
    if (edit == 0)
      strncpy(message, "Failed to allocate new data object", mlen);
    else {
      edit->set_drawable(ptr->get_drawable());
      data->add_edit(ptr);
      // Do these need to happen after the new op is added?
      ptr->update_edit_list(parent, name, index);
      ptr->attach_params();
    }
    return edit;
  }
}

bool DLV::edit3D_slice::is_displayable() const
{
  return true;
}

bool DLV::edit3D_slice::is_editable() const
{
  return false;
}

DLV::string DLV::edit3D_slice::get_data_label() const
{
  string name = "2D slice of " + get_base_data()->get_data_label();
  return name;
}

DLV::string DLV::edit3D_slice::get_edit_label() const
{
  return get_data_label();
}

DLV::int_g DLV::edit3D_slice::get_number_data_sets() const
{
  return get_base_data()->get_number_data_sets();
}

DLV::string DLV::edit3D_slice::get_sub_data_label(const int_g n) const
{
  return get_base_data()->get_sub_data_label(n);
}

DLV::int_g DLV::edit3D_slice::get_display_type() const
{
  return (int)display_2Dunstr;
}

DLV::int_g DLV::edit3D_slice::get_edit_type() const
{
  return edit_2D;
}

bool DLV::edit3D_slice::get_sub_is_vector(const int_g n) const
{
  return get_base_data()->get_sub_is_vector(n);
}

DLV::edit3D_cut *DLV::edit3D_cut::create(const render_parent *parent,
					 data_object *data,
					 const drawable_obj *obj,
					 const int_g component,
					 const bool kspace,
					 const string name, const int_g index,
					 char message[], const int_g mlen)
{
  edited_obj *ptr = edited_obj::create_cut(parent, obj, kspace,
					   message, mlen);
  if (ptr == 0)
    return 0;
  else {
    edit3D_cut *edit = new edit3D_cut(data, ptr, index, name);
    if (edit == 0)
      strncpy(message, "Failed to allocate new data object", mlen);
    else {
      edit->set_drawable(ptr->get_drawable());
      data->add_edit(ptr);
      // Do these need to happen after the new op is added?
      ptr->update_edit_list(parent, name, index);
      ptr->attach_params();
    }
    return edit;
  }
}

bool DLV::edit3D_cut::is_displayable() const
{
  return true;
}

bool DLV::edit3D_cut::is_editable() const
{
  return false;
}

DLV::string DLV::edit3D_cut::get_data_label() const
{
  string name = "3D cut of " + get_base_data()->get_data_label();
  return name;
}

DLV::string DLV::edit3D_cut::get_edit_label() const
{
  return get_data_label();
}

DLV::int_g DLV::edit3D_cut::get_number_data_sets() const
{
  return get_base_data()->get_number_data_sets();
}

DLV::string DLV::edit3D_cut::get_sub_data_label(const int_g n) const
{
  return get_base_data()->get_sub_data_label(n);
}

DLV::int_g DLV::edit3D_cut::get_display_type() const
{
  return (int)display_3D;
}

DLV::int_g DLV::edit3D_cut::get_edit_type() const
{
  return edit_3D;
}

bool DLV::edit3D_cut::get_sub_is_vector(const int_g n) const
{
  return get_base_data()->get_sub_is_vector(n);
}

DLV::edit3D_clamp *DLV::edit3D_clamp::create(const render_parent *parent,
					     data_object *data,
					     const drawable_obj *obj,
					     const int_g component,
					     const bool kspace,
					     const string name,
					     const int_g index,
					     char message[], const int_g mlen)
{
  edited_obj *ptr = edited_obj::create_clamp(parent, obj, component, kspace,
					     message, mlen);
  if (ptr == 0)
    return 0;
  else {
    edit3D_clamp *edit = new edit3D_clamp(data, ptr, index, name, component);
    if (edit == 0)
      strncpy(message, "Failed to allocate new data object", mlen);
    else {
      edit->set_drawable(ptr->get_drawable());
      data->add_edit(ptr);
      // Do these need to happen after the new op is added?
      ptr->update_edit_list(parent, name, index);
      ptr->attach_params();
    }
    return edit;
  }
}

bool DLV::edit3D_clamp::is_displayable() const
{
  return true;
}

bool DLV::edit3D_clamp::is_editable() const
{
  return true;
}

DLV::string DLV::edit3D_clamp::get_data_label() const
{
  string name = "3D clamp of ";
  name += get_base_data()->get_sub_data_label(component);
  name += " ";
  name += get_base_data()->get_data_label();
  return name;
}

DLV::string DLV::edit3D_clamp::get_edit_label() const
{
  return get_data_label();
}

DLV::int_g DLV::edit3D_clamp::get_number_data_sets() const
{
  return 1;
}

DLV::string DLV::edit3D_clamp::get_sub_data_label(const int_g n) const
{
  return get_base_data()->get_sub_data_label(component);
}

DLV::int_g DLV::edit3D_clamp::get_display_type() const
{
  return (int)display_3D;
}

DLV::int_g DLV::edit3D_clamp::get_edit_type() const
{
  return edit_3D;
}

bool DLV::edit3D_clamp::get_sub_is_vector(const int_g n) const
{
  return get_base_data()->get_sub_is_vector(component);
}

DLV::data_object *DLV::edit3D_clamp::edit(const render_parent *parent,
					  class model *structure,
					  const int_g component,
					  const int_g method, const int_g index,
					  char message[], const int_g mlen)
{
  message[0] = '\0';
  edit3D_object *ptr = 0;
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    strncpy(message, "BUG - no clamp object to edit", mlen);
    return 0;
  }
  // Now setup the edited object, the drawable is the output obj
  string name = "Orthoslice of ";
  string basename = get_data_label();
  // only one data object so no need to get name
  // vectors don't use extend
  int_g m = method;
  if (get_sub_is_vector(component) and method > 0)
    m++;
  //coord_type a[3];
  //coord_type b[3];
  //coord_type c[3];
  //structure->get_primitive_lattice(a, b, c);
  //int dim = structure->get_number_of_periodic_dims();
  switch (m) {
  case 0:
    name = "Orthoslice of ";
    name += basename;
    ptr = edit3D_ortho::create(parent, this, obj, is_kspace(), name,
			       index, message, mlen);
    break;
  case 1:
    //name = "Extension of ";
    //name += basename;
    //ptr = edit3D_extend::create(parent, this, obj, component, is_kspace(),
    //			name, index, a, b, c, dim, message, mlen);
    // Todo - not supported by current create/update code
    break;
  case 2:
    name = "Slice of ";
    name += basename;
    ptr = edit3D_slice::create(parent, this, obj, component, is_kspace(), name,
			       index, message, mlen);
    break;
  case 3:
    // This would be a strange thing to do.
    name = "Clamp of ";
    name += basename;
    ptr = edit3D_clamp::create(parent, this, obj, component, is_kspace(), name,
			       index, message, mlen);
    break;
  case 4:
    name = "Downsize of ";
    name += basename;
    ptr = edit3D_downsize::create(parent, this, obj, component, is_kspace(),
				  name, index, message, mlen);
    break;
  case 5:
    name = "Crop of ";
    name += basename;
    ptr = edit3D_crop::create(parent, this, obj, component, is_kspace(), name,
			      index, message, mlen);
    break;
  case 6:
    name = "Cut of ";
    name += basename;
    ptr = edit3D_cut::create(parent, this, obj, component, is_kspace(), name,
			     index, message, mlen);
    break;
  case 7:
    name = "Math of ";
    name += basename;
    ptr = edit3D_math::create(parent, this, obj, component, is_kspace(), name,
			      index, message, mlen);
    break;
  default:
    strncpy(message, "BUG: Unrecognised slice display method", mlen);
    break;
  }
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate data edit object", mlen);
  }
  return ptr;
}

DLV::edit3D_crop *DLV::edit3D_crop::create(const render_parent *parent,
					   data_object *data,
					   const drawable_obj *obj,
					   const int_g component,
					   const bool kspace,
					   const string name, const int_g index,
					   char message[], const int_g mlen)
{
  edited_obj *ptr = edited_obj::create_crop(parent, obj, kspace,
					    message, mlen);
  if (ptr == 0)
    return 0;
  else {
    edit3D_crop *edit = new edit3D_crop(data, ptr, index, name);
    if (edit == 0)
      strncpy(message, "Failed to allocate new data object", mlen);
    else {
      edit->set_drawable(ptr->get_drawable());
      data->add_edit(ptr);
      // Do these need to happen after the new op is added?
      ptr->update_edit_list(parent, name, index);
      ptr->attach_params();
    }
    return edit;
  }
}

bool DLV::edit3D_crop::is_displayable() const
{
  return true;
}

bool DLV::edit3D_crop::is_editable() const
{
  return false;
}

DLV::string DLV::edit3D_crop::get_data_label() const
{
  string name = "3D crop of " + get_base_data()->get_data_label();
  return name;
}

DLV::string DLV::edit3D_crop::get_edit_label() const
{
  return get_data_label();
}

DLV::int_g DLV::edit3D_crop::get_number_data_sets() const
{
  return get_base_data()->get_number_data_sets();
}

DLV::string DLV::edit3D_crop::get_sub_data_label(const int_g n) const
{
  return get_base_data()->get_sub_data_label(n);
}

DLV::int_g DLV::edit3D_crop::get_display_type() const
{
  return (int)display_3D;
}

DLV::int_g DLV::edit3D_crop::get_edit_type() const
{
  return edit_3D;
}

bool DLV::edit3D_crop::get_sub_is_vector(const int_g n) const
{
  return get_base_data()->get_sub_is_vector(n);
}

DLV::edit3D_extend *
DLV::edit3D_extend::create(const render_parent *parent,
			   data_object *data, const drawable_obj *obj,
			   const int_g component, const bool kspace,
			   const string name, const int_g index,
			   const coord_type a[3], const coord_type b[3],
			   const coord_type c[3], const int_g dim,
			   char message[], const int_g mlen)
{
  if (!data->is_periodic() and dim > 0) {
    strncpy(message, "Unable to extend non-periodic object", mlen);
    return 0;
  }
  // Todo - will need changing if we can edit an edit
  volume_data *v = dynamic_cast<volume_data *>(data);
  if (v == 0) {
    strncpy(message, "BUG creating extension", mlen);
    return 0;
  }
  int_g nx = v->get_nx();
  int_g ny = v->get_ny();
  int_g nz = v->get_nz();
  real_g as[3];
  v->get_astep(as);
  real_g bs[3];
  v->get_bstep(bs);
  real_g cs[3];
  v->get_cstep(cs);
  const real_l tol = 0.01;
  bool ok = (std::abs(a[0] - (nx - 1) * as[0])
	     + std::abs(a[1] - (nx - 1) * as[1])
	     + std::abs(a[2] - (nx - 1) * as[2])) < tol;
  if (ok and dim > 1) {
    ok = (std::abs(b[0] - (ny - 1) * bs[0])
	  + std::abs(b[1] - (ny - 1) * bs[1])
	  + std::abs(b[2] - (ny - 1) * bs[2])) < tol;
    if (ok and dim > 2)
      ok = (std::abs(c[0] - (nz - 1) * cs[0])
	    + std::abs(c[1] - (nz - 1) * cs[1])
	    + std::abs(c[2] - (nz - 1) * cs[2])) < tol;
  }
  if (!ok) {
    strncpy(message, "Volume dimensions don't match primitive cell", mlen);
    return 0;
  }
  edited_obj *ptr = edited_obj::create_extension(parent, obj, kspace,
						 nx, ny, nz, message, mlen);
  if (ptr == 0)
    return 0;
  else {
    edit3D_extend *edit = new edit3D_extend(data, ptr, index, name);
    if (edit == 0)
      strncpy(message, "Failed to allocate new data object", mlen);
    else {
      edit->set_drawable(ptr->get_drawable());
      data->add_edit(ptr);
      // Do these need to happen after the new op is added?
      ptr->update_edit_list(parent, name, index);
      ptr->attach_params();
    }
    return edit;
  }
}

bool DLV::edit3D_extend::is_displayable() const
{
  return true;
}

bool DLV::edit3D_extend::is_editable() const
{
  return false;
}

DLV::string DLV::edit3D_extend::get_data_label() const
{
  string name = "3D extension of " + get_base_data()->get_data_label();
  return name;
}

DLV::string DLV::edit3D_extend::get_edit_label() const
{
  return get_data_label();
}

DLV::int_g DLV::edit3D_extend::get_number_data_sets() const
{
  return get_base_data()->get_number_data_sets();
}

DLV::string DLV::edit3D_extend::get_sub_data_label(const int_g n) const
{
  return get_base_data()->get_sub_data_label(n);
}

DLV::int_g DLV::edit3D_extend::get_display_type() const
{
  return (int)display_3D;
}

DLV::int_g DLV::edit3D_extend::get_edit_type() const
{
  return edit_3D;
}

bool DLV::edit3D_extend::get_sub_is_vector(const int_g n) const
{
  return get_base_data()->get_sub_is_vector(n);
}

DLV::edit3D_downsize *
DLV::edit3D_downsize::create(const render_parent *parent,
			     data_object *data, const drawable_obj *obj,
			     const int_g component, const bool kspace,
			     const string name, const int_g index,
			     char message[], const int_g mlen)
{
  edited_obj *ptr = edited_obj::create_downsize(parent, obj, kspace,
						message, mlen);
  if (ptr == 0)
    return 0;
  else {
    edit3D_downsize *edit = new edit3D_downsize(data, ptr, index, name);
    if (edit == 0)
      strncpy(message, "Failed to allocate new data object", mlen);
    else {
      edit->set_drawable(ptr->get_drawable());
      data->add_edit(ptr);
      // Do these need to happen after the new op is added?
      ptr->update_edit_list(parent, name, index);
      ptr->attach_params();
    }
    return edit;
  }
}

bool DLV::edit3D_downsize::is_displayable() const
{
  return true;
}

bool DLV::edit3D_downsize::is_editable() const
{
  return false;
}

DLV::string DLV::edit3D_downsize::get_data_label() const
{
  string name = "3D downsize of " + get_base_data()->get_data_label();
  return name;
}

DLV::string DLV::edit3D_downsize::get_edit_label() const
{
  return get_data_label();
}

DLV::int_g DLV::edit3D_downsize::get_number_data_sets() const
{
  return get_base_data()->get_number_data_sets();
}

DLV::string DLV::edit3D_downsize::get_sub_data_label(const int_g n) const
{
  return get_base_data()->get_sub_data_label(n);
}

DLV::int_g DLV::edit3D_downsize::get_display_type() const
{
  return (int)display_3D;
}

DLV::int_g DLV::edit3D_downsize::get_edit_type() const
{
  return edit_3D;
}

bool DLV::edit3D_downsize::get_sub_is_vector(const int_g n) const
{
  return get_base_data()->get_sub_is_vector(n);
}

DLV::edit3D_math *DLV::edit3D_math::create(const render_parent *parent,
					   data_object *data,
					   const drawable_obj *obj,
					   const int_g component,
					   const bool kspace,
					   const string name, const int_g index,
					   char message[], const int_g mlen)
{
  edited_obj *ptr = edited_obj::create_math(parent, obj, component, kspace,
					    message, mlen);
  if (ptr == 0)
    return 0;
  else {
    edit3D_math *edit = new edit3D_math(data, component, ptr, index, name);
    if (edit == 0)
      strncpy(message, "Failed to allocate new data object", mlen);
    else {
      edit->set_drawable(ptr->get_drawable());
      data->add_edit(ptr);
      // Do these need to happen after the new op is added?
      ptr->update_edit_list(parent, name, index);
      ptr->attach_params();
    }
    return edit;
  }
}

bool DLV::edit3D_math::is_displayable() const
{
  return true;
}

bool DLV::edit3D_math::is_editable() const
{
  return false;
}

DLV::string DLV::edit3D_math::get_data_label() const
{
  string name = "3D math of " + get_base_data()->get_data_label();
  return name;
}

DLV::string DLV::edit3D_math::get_edit_label() const
{
  return get_data_label();
}

DLV::int_g DLV::edit3D_math::get_number_data_sets() const
{
  return 1;
}

DLV::string DLV::edit3D_math::get_sub_data_label(const int_g n) const
{
  return get_base_data()->get_sub_data_label(obj_component);
}

DLV::int_g DLV::edit3D_math::get_display_type() const
{
  return (int)display_3D;
}

DLV::int_g DLV::edit3D_math::get_edit_type() const
{
  return edit_3D;
}

bool DLV::edit3D_math::get_sub_is_vector(const int_g n) const
{
  return get_base_data()->get_sub_is_vector(obj_component);
}

DLV::edit3D_vol_render *
DLV::edit3D_vol_render::create(const render_parent *parent,
			       data_object *data,
			       const drawable_obj *obj,
			       const int_g component, const bool kspace,
			       const string name, const int_g index,
			       char message[], const int_g mlen)
{
  edited_obj *ptr = edited_obj::create_vol_render(parent, obj, kspace,
						  message, mlen);
  if (ptr == 0)
    return 0;
  else {
    edit3D_vol_render *edit = new edit3D_vol_render(data, ptr, index, name);
    if (edit == 0)
      strncpy(message, "Failed to allocate new data object", mlen);
    else {
      edit->update();
      edit->set_drawable(ptr->get_drawable());
      data->add_edit(ptr);
      // Do these need to happen after the new op is added?
      ptr->update_edit_list(parent, name, index);
      ptr->attach_params();
    }
    return edit;
  }
}

bool DLV::edit3D_vol_render::is_displayable() const
{
  return true;
}

bool DLV::edit3D_vol_render::is_editable() const
{
  return false;
}

DLV::string DLV::edit3D_vol_render::get_data_label() const
{
  string name = "3D volume of " + get_base_data()->get_data_label();
  return name;
}

DLV::string DLV::edit3D_vol_render::get_edit_label() const
{
  return get_data_label();
}

DLV::int_g DLV::edit3D_vol_render::get_number_data_sets() const
{
  return get_base_data()->get_number_data_sets();
}

DLV::string DLV::edit3D_vol_render::get_sub_data_label(const int_g n) const
{
  return get_base_data()->get_sub_data_label(n);
}

DLV::int_g DLV::edit3D_vol_render::get_display_type() const
{
  return (int)display_3Dvr;
}

DLV::int_g DLV::edit3D_vol_render::get_edit_type() const
{
  return edit_3D;
}

bool DLV::edit3D_vol_render::get_sub_is_vector(const int_g n) const
{
  return get_base_data()->get_sub_is_vector(n);
}

DLVreturn_type DLV::real_space_volume::render_data(const model *structure,
						   char message[],
						   const int_g mlen)
{
  // Todo
  return DLV_OK;
}

DLVreturn_type DLV::rspace_periodic_volume::render_data(const model *structure,
							char message[],
							const int_g mlen)
{
  // Todo
  return DLV_OK;
}

DLVreturn_type DLV::k_space_volume::render_data(const class model *structure,
						char message[],
						const int_g mlen)
{
  // Todo
  return DLV_OK;
}

bool DLV::volume_data::hide_render(const int_g object, const int_g visible,
				   const render_parent *parent,
				   class model *structure,
				   char message[], const int_g mlen)
{
  // This routine is now badly named
  if (visible == 1) {
    message[0] = '\0';
    if (!reload_data(this, message, mlen)) {
      drawable_obj *obj = get_drawable();
      bool use_ignore = false;
      real_g ignore = 0.0;
      if (is_kspace()) {
	use_ignore = true;
	ignore = (real_g)(10000.0 * Hartree_to_eV);
      }
      obj->reload_data(parent, origin, a_step, b_step, c_step, data,
		       nx, ny, nz, vector_dim, labels, n_data_sets,
		       is_kspace(), use_ignore, ignore, message, mlen);
      reattach_objects(parent);
      return false;
    }
  }
  return true;
}

DLVreturn_type DLV::volume_data::render(const render_parent *parent,
					class model *structure,
					const int_g component,
					const int_g method, const int_g index,
					char message[], const int_g mlen)
{
  message[0] = '\0';
  bool data_ok = reload_data(this, message, mlen);
  if (strlen(message) > 0)
    return DLV_ERROR;
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    bool use_ignore = false;
    real_g ignore = 0.0;
    if (is_kspace()) {
      use_ignore = true;
      ignore = (real_g)(10000.0 * Hartree_to_eV);
    }
    obj = DLV::drawable_obj::create_volume(parent, origin, a_step, b_step,
					   c_step, data, nx, ny, nz,
					   vector_dim, labels, n_data_sets,
					   is_kspace(), use_ignore,
					   ignore, message, mlen);
    if (obj == 0)
      return DLV_ERROR;
    else
      set_drawable(obj);
  } else if (!data_ok) {
    bool use_ignore = false;
    real_g ignore = 0.0;
    if (is_kspace()) {
      use_ignore = true;
      ignore = (real_g)(10000.0 * Hartree_to_eV);
    }
    obj->reload_data(parent, origin, a_step, b_step, c_step, data,
		     nx, ny, nz, vector_dim, labels, n_data_sets,
		     is_kspace(), use_ignore, ignore, message, mlen);
  }
  display_obj *ptr = 0;
  string name = get_data_label();
  name += " ";
  name += labels[component];
  char buff[32];
  snprintf(buff, 32, " - %1d", count_display_objects());
  name += buff;
  if (vector_dim[component] == 1) {
    switch (method) {
    case 0:
      ptr = display_obj::create_isosurface(parent, obj, is_periodic(),
					   is_kspace(), component, name, index,
					   message, mlen);
      name += " (isosurface)";
      break;
    case 1:
      ptr = display_obj::create_cells(parent, obj, is_periodic(),
				      is_kspace(), component, name, index,
				      message, mlen);
      name += " (cells)";
      break;
    case 2:
      ptr = display_obj::create_spheres(parent, obj, is_periodic(),
					is_kspace(), component, name, index,
					message, mlen);
      name += " (spheres)";
      break;
      /* Todocase 3:
	 ptr = display_obj::create_volume_render();
	 break;*/
    default:
      strncpy(message, "BUG: Unrecognised volume display method", mlen);
      break;
    }
  } else {
    // Todo - what about vectors?
    // vectors
    switch (method) {
    case 0:
      ptr = display_obj::create_hedgehog(parent, obj, is_periodic(),
					 is_kspace(), component, name, index,
					 message, mlen);
      name += " (hedgehog)";
      break;
    case 1:
      ptr = display_obj::create_streamlines(parent, obj, is_periodic(),
					    is_kspace(), component, name,
					    index, message, mlen);
      name += " (streamlines)";
      break;
    default:
      strncpy(message, "BUG: Unrecognised volume display method", mlen);
      break;
    }
  }
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

bool DLV::rspace_wavefunction::hide_render(const int_g object,
					   const int_g visible,
					   const render_parent *parent,
					   class model *structure,
					   char message[], const int_g mlen)
{
  // This routine is now badly named
  if (visible == 1) {
    message[0] = '\0';
    if (!reload_data(this, message, mlen)) {
      drawable_obj *obj = get_drawable();
      real_g o[3];
      get_origin(o);
      real_g a[3];
      get_astep(a);
      real_g b[3];
      get_bstep(b);
      real_g c[3];
      get_cstep(c);
      obj->reload_wavefn(parent, o, a, b, c, get_data(), phases, get_nx(),
			 get_ny(), get_nz(), get_labels(), get_ndata_sets(),
			 false, 0.0, message, mlen);
      reattach_objects(parent);
      return false;
    }
  }
  return true;
}

DLVreturn_type DLV::rspace_wavefunction::render(const render_parent *parent,
						class model *structure,
						const int_g component,
						const int_g method,
						const int_g index,
						char message[],
						const int_g mlen)
{
  message[0] = '\0';
  bool data_ok = reload_data(this, message, mlen);
  if (strlen(message) > 0)
    return DLV_ERROR;
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    real_g o[3];
    get_origin(o);
    real_g a[3];
    get_astep(a);
    real_g b[3];
    get_bstep(b);
    real_g c[3];
    get_cstep(c);
    obj = DLV::drawable_obj::create_real_wavefn(parent, o, a, b, c,
						get_data(), phases,
						get_nx(), get_ny(), get_nz(),
						get_labels(), get_ndata_sets(),
						false, 0.0, message, mlen);
    if (obj == 0)
      return DLV_ERROR;
    else
      set_drawable(obj);
  } else if (!data_ok) {
    real_g o[3];
    get_origin(o);
    real_g a[3];
    get_astep(a);
    real_g b[3];
    get_bstep(b);
    real_g c[3];
    get_cstep(c);
    obj->reload_wavefn(parent, o, a, b, c, get_data(), phases, get_nx(),
		       get_ny(), get_nz(), get_labels(), get_ndata_sets(),
		       false, 0.0, message, mlen);
  }
  display_obj *ptr = 0;
  string name = get_data_label();
  name += ", ";
  name += get_labels()[component];
  char buff[32];
  snprintf(buff, 32, " - %1d", count_display_objects());
  name += buff;
  switch (method) {
  case 0:
    ptr = display_obj::create_iso_wavefn(parent, obj, false, false, component,
					 get_ndata_sets(), name, index,
					 0, 0, 0, message, mlen);
    name += " (wavefn)";
    break;
    /*case 1:
      ptr = display_obj::create_cells(parent, obj, is_periodic(),
      is_kspace(), component, name, index,
      message, mlen);
      break;
      case 2:
      ptr = display_obj::create_spheres(parent, obj, is_periodic(),
      is_kspace(), component, name, index,
      message, mlen);
      break;*/
    /* Todocase 3:
       ptr = display_obj::create_volume_render();
       break;*/
  default:
    strncpy(message, "BUG: Unrecognised volume display method", mlen);
    break;
  }
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

bool DLV::bloch_wavefunction::hide_render(const int_g object,
					  const int_g visible,
					  const render_parent *parent,
					  class model *structure,
					  char message[], const int_g mlen)
{
  // This routine is now badly named
  if (visible == 1) {
    message[0] = '\0';
    if (!reload_data(this, message, mlen)) {
      drawable_obj *obj = get_drawable();
      real_g o[3];
      get_origin(o);
      real_g a[3];
      get_astep(a);
      real_g b[3];
      get_bstep(b);
      real_g c[3];
      get_cstep(c);
      obj->reload_bloch(parent, o, a, b, c, get_data(), get_phase(),
			get_nx(), get_ny(), get_nz(), get_labels(),
			get_ndata_sets(), k_num, k_den, false, 0.0,
			message, mlen);
      reattach_objects(parent);
      return false;
    }
  }
  return true;
}

DLVreturn_type DLV::bloch_wavefunction::render(const render_parent *parent,
					       class model *structure,
					       const int_g component,
					       const int_g method,
					       const int_g index,
					       char message[],
					       const int_g mlen)
{
  message[0] = '\0';
  bool data_ok = reload_data(this, message, mlen);
  if (strlen(message) > 0)
    return DLV_ERROR;
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    real_g o[3];
    get_origin(o);
    real_g a[3];
    get_astep(a);
    real_g b[3];
    get_bstep(b);
    real_g c[3];
    get_cstep(c);
    obj = DLV::drawable_obj::create_bloch_wavefn(parent, o, a, b, c,
						 get_data(), get_phase(),
						 get_nx(), get_ny(), get_nz(),
						 get_labels(),
						 get_ndata_sets(),
						 k_num, k_den, false, 0.0,
						 message, mlen);
    if (obj == 0)
      return DLV_ERROR;
    else
      set_drawable(obj);
  } else if (!data_ok) {
    real_g o[3];
    get_origin(o);
    real_g a[3];
    get_astep(a);
    real_g b[3];
    get_bstep(b);
    real_g c[3];
    get_cstep(c);
    obj->reload_bloch(parent, o, a, b, c, get_data(), get_phase(),
		      get_nx(), get_ny(), get_nz(), get_labels(),
		      get_ndata_sets(), k_num, k_den, false, 0.0,
		      message, mlen);
  }
  display_obj *ptr = 0;
  string name = get_data_label();
  name += ", ";
  name += get_labels()[component];
  char buff[32];
  snprintf(buff, 32, " - %1d", count_display_objects());
  name += buff;
  switch (method) {
  case 0:
    ptr = display_obj::create_iso_wavefn(parent, obj, true, false, component,
					 get_ndata_sets(), name, index,
					 k_den[0], k_den[1], k_den[2],
					 message, mlen);
    name += " (wavefn)";
    break;
    /*case 1:
      ptr = display_obj::create_cells(parent, obj, is_periodic(),
      is_kspace(), component, name, index,
      message, mlen);
      break;
      case 2:
      ptr = display_obj::create_spheres(parent, obj, is_periodic(),
      is_kspace(), component, name, index,
      message, mlen);
      break;*/
    /* Todocase 3:
       ptr = display_obj::create_volume_render();
       break;*/
  default:
    strncpy(message, "BUG: Unrecognised volume display method", mlen);
    break;
  }
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

bool DLV::edit3D_ortho::hide_render(const int_g object, const int_g visible,
				    const render_parent *parent,
				    class model *structure,
				    char message[], const int_g mlen)
{
  // This routine is now badly named
  if (visible == 1) {
    message[0] = '\0';
    if (!get_base_data()->hide_render(object, 1, parent, structure,
				      message, mlen)) {
      //get_edit_obj()->reload_data(get_base_data()->get_drawable());
      set_drawable(get_edit_obj()->get_drawable());
      reattach_objects(parent);
      return false;
    }
  }
  return true;
}

// Todo - seems to be a lot of copying here
DLVreturn_type DLV::edit3D_ortho::render(const render_parent *parent,
					 class model *structure,
					 const int_g component,
					 const int_g method, const int_g index,
					 char message[], const int_g mlen)
{
  //Todo - basically a copy of surface_data::render
  message[0] = '\0';
  bool data_ok = get_base_data()->hide_render(0, 1, parent, structure,
					      message, mlen);
  if (strlen(message) > 0)
    return DLV_ERROR;
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    strncpy(message, "Missing object for surface", mlen);
    return DLV_ERROR;
  } else if (!data_ok) {
    //get_edit_obj()->reload_data(get_base_data()->get_drawable());
    set_drawable(get_edit_obj()->get_drawable());
    reattach_objects(parent);
  }
  // Todo - what about vectors?
  display_obj *ptr = 0;
  string name = get_data_label();
  name += " ";
  name += get_sub_data_label(component);
  char buff[32];
  snprintf(buff, 32, " - %1d", count_display_objects());
  name += buff;
  // Can't use AG contours for unstructured slice.
  // Todo - this is always false for orthoslice?
  int_g m = method;
  if (get_display_type() == display_2Dunstr)
    m++;
  switch (m) {
  case 0:
    ptr = display_obj::create_ag_contour(parent, obj, is_periodic(),
					 is_kspace(), component, name, index,
					 message, mlen);
    name += " (contours)";
    break;
  case 1:
    ptr = display_obj::create_contour(parent, obj, is_periodic(),
				      is_kspace(), component, name, index,
				      message, mlen);
    name += " (avs contours)";
    break;
  case 4:
    ptr = display_obj::create_pn_contour(parent, obj, is_periodic(),
					 is_kspace(), component, name, index,
					 message, mlen);
    name += " (pn contours)";
    break;
  case 3:
    ptr = display_obj::create_solid_ct(parent, obj, is_periodic(),
				       is_kspace(), component, name, index,
				       message, mlen);
    name += " (solid contours)";
    break;
  case 2:
    ptr = display_obj::create_shaded_ct(parent, obj, is_periodic(),
					is_kspace(), component, name, index,
					message, mlen);
    name += " (shaded contours)";
    break;
  case 5:
    ptr = display_obj::create_surface_plot(parent, obj, is_periodic(),
					   is_kspace(), component, name, index,
					   message, mlen);
    name += " (surface plot)";
    break;
  default:
    strncpy(message, "BUG: Unrecognised surface display method", mlen);
    break;
  }
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

bool DLV::edit3D_slice::hide_render(const int_g object, const int_g visible,
				    const render_parent *parent,
				    class model *structure,
				    char message[], const int_g mlen)
{
  // This routine is now badly named
  if (visible == 1) {
    message[0] = '\0';
    if (!get_base_data()->hide_render(object, 1, parent, structure,
				      message, mlen)) {
      //get_edit_obj()->reload_data(get_base_data()->get_drawable());
      set_drawable(get_edit_obj()->get_drawable());
      (void) twod->hide_render(0, 1, parent, structure, message, mlen);
      get_edit_obj()->update_slice(twod->get_drawable(), message, mlen);
      reattach_objects(parent);
      return false;
    }
  }
  return true;
}

DLVreturn_type DLV::edit3D_slice::render(const render_parent *parent,
					 class model *structure,
					 const int_g component,
					 const int_g method, const int_g index,
					 char message[], const int_g mlen)
{
  //Todo - basically a copy of surface_data::render
  message[0] = '\0';
  bool data_ok = get_base_data()->hide_render(0, 1, parent, structure,
					      message, mlen);
  if (strlen(message) > 0)
    return DLV_ERROR;
  if (twod != 0)
    (void) twod->hide_render(0, 1, parent, structure, message, mlen);
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    strncpy(message, "Missing object for surface", mlen);
    return DLV_ERROR;
  } else if (!data_ok) {
    //get_edit_obj()->reload_data(get_base_data()->get_drawable());
    set_drawable(get_edit_obj()->get_drawable());
    reattach_objects(parent);
  }
  // Todo - what about vectors?
  display_obj *ptr = 0;
  string name = get_data_label();
  name += " ";
  name += get_sub_data_label(component);
  char buff[32];
  snprintf(buff, 32, " - %1d", count_display_objects());
  name += buff;
  // Can't use AG contours for unstructured slice.
  // Todo - this is always true for slice?
  int_g m = method;
  if (get_display_type() == display_2Dunstr)
    m++;
  switch (m) {
  case 0:
    ptr = display_obj::create_ag_contour(parent, obj, is_periodic(),
					 is_kspace(), component, name, index,
					 message, mlen);
    name += " (contours)";
    break;
  case 1:
    ptr = display_obj::create_contour(parent, obj, is_periodic(),
				      is_kspace(), component, name, index,
				      message, mlen);
    name += " (avs contours)";
    break;
  case 4:
    ptr = display_obj::create_pn_contour(parent, obj, is_periodic(),
					 is_kspace(), component, name, index,
					 message, mlen);
    name += " (pn contours)";
    break;
  case 3:
    ptr = display_obj::create_solid_ct(parent, obj, is_periodic(),
				       is_kspace(), component, name, index,
				       message, mlen);
    name += " (solid contours)";
    break;
  case 2:
    ptr = display_obj::create_shaded_ct(parent, obj, is_periodic(),
					is_kspace(), component, name, index,
					message, mlen);
    name += " (shaded contours)";
    break;
  case 5:
    ptr = display_obj::create_surface_plot(parent, obj, is_periodic(),
					   is_kspace(), component, name, index,
					   message, mlen);
    name += " (surface plot)";
    break;
  default:
    strncpy(message, "BUG: Unrecognised surface display method", mlen);
    break;
  }
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

DLVreturn_type DLV::edit3D_clamp::render(const render_parent *parent,
					 class model *structure,
					 const int_g component,
					 const int_g method, const int_g index,
					 char message[], const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    strncpy(message, "Missing object for volume", mlen);
    return DLV_ERROR;
  }
  display_obj *ptr = 0;
  string name = get_data_label();
  name += " ";
  name += get_sub_data_label(component);
  char buff[32];
  snprintf(buff, 32, " - %1d", count_display_objects());
  name += buff;
  if (!get_sub_is_vector(component)) {
    switch (method) {
    case 0:
      ptr = display_obj::create_isosurface(parent, obj, is_periodic(),
					   is_kspace(), component, name, index,
					   message, mlen);
      name += " (isosurface)";
      break;
    case 1:
      ptr = display_obj::create_cells(parent, obj, is_periodic(),
				      is_kspace(), component, name, index,
				      message, mlen);
      name += " (cells)";
      break;
    case 2:
      ptr = display_obj::create_spheres(parent, obj, is_periodic(),
					is_kspace(), component, name, index,
					message, mlen);
      name += " (spheres)";
      break;
    default:
      strncpy(message, "BUG: Unrecognised volume display method", mlen);
      break;
    }
  } else {
    // Todo - what about vectors?
    // vectors
    switch (method) {
    case 0:
      ptr = display_obj::create_hedgehog(parent, obj, is_periodic(),
					 is_kspace(), component, name, index,
					 message, mlen);
      name += " (hedgehog)";
      break;
    case 1:
      ptr = display_obj::create_streamlines(parent, obj, is_periodic(),
					    is_kspace(), component, name,
					    index, message, mlen);
      name += " (streamlines)";
      break;
    default:
      strncpy(message, "BUG: Unrecognised volume display method", mlen);
      break;
    }
  }
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

DLVreturn_type DLV::edit3D_crop::render(const render_parent *parent,
					class model *structure,
					const int_g component,
					const int_g method, const int_g index,
					char message[], const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    strncpy(message, "Missing object for volume", mlen);
    return DLV_ERROR;
  }
  display_obj *ptr = 0;
  string name = get_data_label();
  name += " ";
  name += get_sub_data_label(component);
  char buff[32];
  snprintf(buff, 32, " - %1d", count_display_objects());
  name += buff;
  if (!get_sub_is_vector(component)) {
    switch (method) {
    case 0:
      ptr = display_obj::create_isosurface(parent, obj, is_periodic(),
					   is_kspace(), component, name, index,
					   message, mlen);
      name += " (isosurface)";
      break;
    case 1:
      ptr = display_obj::create_cells(parent, obj, is_periodic(),
				      is_kspace(), component, name, index,
				      message, mlen);
      name += " (cells)";
      break;
    case 2:
      ptr = display_obj::create_spheres(parent, obj, is_periodic(),
					is_kspace(), component, name, index,
					message, mlen);
      name += " (spheres)";
      break;
    default:
      strncpy(message, "BUG: Unrecognised volume display method", mlen);
      break;
    }
  } else {
    // Todo - what about vectors?
    // vectors
    switch (method) {
    case 0:
      ptr = display_obj::create_hedgehog(parent, obj, is_periodic(),
					 is_kspace(), component, name, index,
					 message, mlen);
      name += " (hedgehog)";
      break;
    case 1:
      ptr = display_obj::create_streamlines(parent, obj, is_periodic(),
					    is_kspace(), component, name,
					    index, message, mlen);
      name += " (streamlines)";
      break;
    default:
      strncpy(message, "BUG: Unrecognised volume display method", mlen);
      break;
    }
  }
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

DLVreturn_type DLV::edit3D_cut::render(const render_parent *parent,
				       class model *structure,
				       const int_g component,
				       const int_g method, const int_g index,
				       char message[], const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    strncpy(message, "Missing object for volume", mlen);
    return DLV_ERROR;
  }
  display_obj *ptr = 0;
  string name = get_data_label();
  name += " ";
  name += get_sub_data_label(component);
  char buff[32];
  snprintf(buff, 32, " - %1d", count_display_objects());
  name += buff;
  if (!get_sub_is_vector(component)) {
    switch (method) {
    case 0:
      ptr = display_obj::create_isosurface(parent, obj, is_periodic(),
					   is_kspace(), component, name, index,
					   message, mlen);
      name += " (isosurface)";
      break;
    case 1:
      ptr = display_obj::create_cells(parent, obj, is_periodic(),
				      is_kspace(), component, name, index,
				      message, mlen);
      name += " (cells)";
      break;
    case 2:
      ptr = display_obj::create_spheres(parent, obj, is_periodic(),
					is_kspace(), component, name, index,
					message, mlen);
      name += " (streamlines)";
      break;
    default:
      strncpy(message, "BUG: Unrecognised volume display method", mlen);
      break;
    }
  } else {
    // Todo - what about vectors?
    // vectors
    switch (method) {
    case 0:
      ptr = display_obj::create_hedgehog(parent, obj, is_periodic(),
					 is_kspace(), component, name, index,
					 message, mlen);
      name += " (hedgehog)";
      break;
    case 1:
      ptr = display_obj::create_streamlines(parent, obj, is_periodic(),
					    is_kspace(), component, name,
					    index, message, mlen);
      name += " (streamlines)";
      break;
    default:
      strncpy(message, "BUG: Unrecognised volume display method", mlen);
      break;
    }
  }
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

DLVreturn_type DLV::edit3D_downsize::render(const render_parent *parent,
					    class model *structure,
					    const int_g component,
					    const int_g method,
					    const int_g index, char message[],
					    const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    strncpy(message, "Missing object for volume", mlen);
    return DLV_ERROR;
  }
  display_obj *ptr = 0;
  string name = get_data_label();
  name += " ";
  name += get_sub_data_label(component);
  char buff[32];
  snprintf(buff, 32, " - %1d", count_display_objects());
  name += buff;
  if (!get_sub_is_vector(component)) {
    switch (method) {
    case 0:
      ptr = display_obj::create_isosurface(parent, obj, is_periodic(),
					   is_kspace(), component, name, index,
					   message, mlen);
      name += " (isosurface)";
      break;
    case 1:
      ptr = display_obj::create_cells(parent, obj, is_periodic(),
				      is_kspace(), component, name, index,
				      message, mlen);
      name += " (cells)";
      break;
    case 2:
      ptr = display_obj::create_spheres(parent, obj, is_periodic(),
					is_kspace(), component, name, index,
					message, mlen);
      name += " (spheres)";
      break;
    default:
      strncpy(message, "BUG: Unrecognised volume display method", mlen);
      break;
    }
  } else {
    // Todo - what about vectors?
    // vectors
    switch (method) {
    case 0:
      ptr = display_obj::create_hedgehog(parent, obj, is_periodic(),
					 is_kspace(), component, name, index,
					 message, mlen);
      name += " (hedgehog)";
      break;
    case 1:
      ptr = display_obj::create_streamlines(parent, obj, is_periodic(),
					    is_kspace(), component, name,
					    index, message, mlen);
      name += " (streamlines)";
      break;
    default:
      strncpy(message, "BUG: Unrecognised volume display method", mlen);
      break;
    }
  }
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

DLVreturn_type DLV::edit3D_extend::render(const render_parent *parent,
					  class model *structure,
					  const int_g component,
					  const int_g method,
					  const int_g index, char message[],
					  const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    strncpy(message, "Missing object for volume", mlen);
    return DLV_ERROR;
  }
  display_obj *ptr = 0;
  string name = get_data_label();
  name += " ";
  name += get_sub_data_label(component);
  char buff[32];
  snprintf(buff, 32, " - %1d", count_display_objects());
  name += buff;
  if (!get_sub_is_vector(component)) {
    switch (method) {
    case 0:
      ptr = display_obj::create_isosurface(parent, obj, is_periodic(),
					   is_kspace(), component, name, index,
					   message, mlen);
      name += " (isosurface)";
      break;
    case 1:
      ptr = display_obj::create_cells(parent, obj, is_periodic(),
				      is_kspace(), component, name, index,
				      message, mlen);
      name += " (cells)";
     break;
    case 2:
      ptr = display_obj::create_spheres(parent, obj, is_periodic(),
					is_kspace(), component, name, index,
					message, mlen);
      name += " (spheres)";
      break;
    default:
      strncpy(message, "BUG: Unrecognised volume display method", mlen);
      break;
    }
  } else {
    // Todo - what about vectors?
    // vectors
    switch (method) {
    case 0:
      ptr = display_obj::create_hedgehog(parent, obj, is_periodic(),
					 is_kspace(), component, name, index,
					 message, mlen);
      name += " (hedgehog)";
      break;
    case 1:
      ptr = display_obj::create_streamlines(parent, obj, is_periodic(),
					    is_kspace(), component, name,
					    index, message, mlen);
      name += " (streamlines)";
      break;
    default:
      strncpy(message, "BUG: Unrecognised volume display method", mlen);
      break;
    }
  }
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

DLVreturn_type DLV::edit3D_math::render(const render_parent *parent,
					class model *structure,
					const int_g component,
					const int_g method, const int_g index,
					char message[], const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    strncpy(message, "Missing object for volume", mlen);
    return DLV_ERROR;
  }
  display_obj *ptr = 0;
  string name = get_data_label();
  name += " ";
  name += get_sub_data_label(component);
  char buff[32];
  snprintf(buff, 32, " - %1d", count_display_objects());
  name += buff;
  if (!get_sub_is_vector(component)) {
    switch (method) {
    case 0:
      ptr = display_obj::create_isosurface(parent, obj, is_periodic(),
					   is_kspace(), component, name, index,
					   message, mlen);
      name += " (isosurface)";
      break;
    case 1:
      ptr = display_obj::create_cells(parent, obj, is_periodic(),
				      is_kspace(), component, name, index,
				      message, mlen);
      name += " (cells)";
      break;
    case 2:
      ptr = display_obj::create_spheres(parent, obj, is_periodic(),
					is_kspace(), component, name, index,
					message, mlen);
      name += " (spheres)";
      break;
    default:
      strncpy(message, "BUG: Unrecognised volume display method", mlen);
      break;
    }
  } else {
    // Todo - what about vectors?
    // vectors
    switch (method) {
    case 0:
      ptr = display_obj::create_hedgehog(parent, obj, is_periodic(),
					 is_kspace(), component, name, index,
					 message, mlen);
      name += " (hedgehog)";
      break;
    case 1:
      ptr = display_obj::create_streamlines(parent, obj, is_periodic(),
					    is_kspace(), component, name,
					    index, message, mlen);
      name += " (streamlines)";
      break;
    default:
      strncpy(message, "BUG: Unrecognised volume display method", mlen);
      break;
    }
  }
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

DLVreturn_type DLV::edit3D_vol_render::render(const render_parent *parent,
					      class model *structure,
					      const int_g component,
					      const int_g method,
					      const int_g index, char message[],
					      const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    strncpy(message, "Missing object for volume", mlen);
    return DLV_ERROR;
  }
  display_obj *ptr = 0;
  string name = get_data_label();
  name += " ";
  name += get_sub_data_label(component);
  char buff[32];
  snprintf(buff, 32, " - %1d", count_display_objects());
  name += buff;
  if (!get_sub_is_vector(component)) {
    switch (method) {
    case 0:
      ptr = display_obj::create_isosurface(parent, obj, is_periodic(),
					   is_kspace(), component, name, index,
					   message, mlen);
      name += " (isosurface)";
      break;
    case 1:
      ptr = display_obj::create_cells(parent, obj, is_periodic(),
				      is_kspace(), component, name, index,
				      message, mlen);
      name += " (cells)";
      break;
    case 2:
      ptr = display_obj::create_spheres(parent, obj, is_periodic(),
					is_kspace(), component, name, index,
					message, mlen);
      name += " (spheres)";
      break;
    case 3:
      ptr = display_obj::create_volume(parent, obj, is_periodic(),
				       is_kspace(), component, name, index,
				       message, mlen);
      name += " (volume)";
      break;
    default:
      strncpy(message, "BUG: Unrecognised volume display method", mlen);
      break;
    }
  } else {
    // Todo - what about vectors?
    // vectors
    switch (method) {
    case 0:
      ptr = display_obj::create_hedgehog(parent, obj, is_periodic(),
					 is_kspace(), component, name, index,
					 message, mlen);
      name += " (hedgehog)";
      break;
    case 1:
      ptr = display_obj::create_streamlines(parent, obj, is_periodic(),
					    is_kspace(), component, name,
					    index, message, mlen);
      name += " (streamlines)";
      break;
    default:
      strncpy(message, "BUG: Unrecognised volume display method", mlen);
      break;
    }
  }
  if (ptr == 0) {
    if (strlen(message) == 0)
      strncpy(message, "Unable to allocate display object", mlen);
    return DLV_ERROR;
  } else {
    add_display_obj(ptr);
    ptr->update_display_list(parent, name);
    ptr->attach_params();
  }
  return DLV_OK;
}

DLVreturn_type DLV::edit3D_slice::update_slice(const render_parent *parent,
					       class model *,
					       data_object *plane,
					       char message[], const int_g mlen)
{
  message[0] = '\0';
  edited_obj *obj = get_edit_obj();
  if (obj == 0) {
    strncpy(message, "Missing object for slice", mlen);
    return DLV_ERROR;
  }
  twod = plane;
  drawable_obj *p = plane->get_drawable();
  if (p == 0) {
    strncpy(message, "Missing plane for slice", mlen);
    return DLV_ERROR;
  }
  if (obj->update_slice(p, message, mlen))
    return DLV_OK;
  else
    return DLV_ERROR;
}

DLVreturn_type DLV::edit3D_cut::update_cut(const render_parent *parent,
					   class model *,
					   data_object *plane,
					   char message[], const int_g mlen)
{
  message[0] = '\0';
  edited_obj *obj = get_edit_obj();
  if (obj == 0) {
    strncpy(message, "Missing object for cut", mlen);
    return DLV_ERROR;
  }
  drawable_obj *p = plane->get_drawable();
  if (p == 0) {
    strncpy(message, "Missing plane for cut", mlen);
    return DLV_ERROR;
  }
  if (obj->update_cut(p, message, mlen))
    return DLV_OK;
  else
    return DLV_ERROR;
}

DLVreturn_type
DLV::edit3D_extend::update_extension(const render_parent *parent,
				     const int_g na, const int_g nb,
				     const int_g nc, char message[],
				     const int_g mlen)
{
  message[0] = '\0';
  edited_obj *obj = get_edit_obj();
  if (obj == 0) {
    strncpy(message, "Missing object for extension", mlen);
    return DLV_ERROR;
  }
  // Todo - will need changing if we can edit an edit
  const data_object *p = get_base_data();
  const volume_data *v = dynamic_cast<const volume_data *>(p);
  if (v == 0) {
    strncpy(message, "BUG updating extension", mlen);
    return DLV_ERROR;
  }
  real_g o[3];
  v->get_origin(o);
  real_g a[3];
  v->get_astep(a);
  real_g b[3];
  v->get_bstep(b);
  real_g c[3];
  v->get_cstep(c);
  if (obj->update_extension(na, nb, nc, o, a, b, c, v->get_nx(), v->get_ny(),
			    v->get_nz(), v->get_data(), v->get_vector_data(),
			    v->get_ndata_sets(), message, mlen))
    return DLV_OK;
  else
    return DLV_ERROR;
}

void DLV::edit3D_math::data_math_parent(const data_object * &obj,
					int_g &component) const
{
  obj = get_base_data();
  component = obj_component;
}

void DLV::volume_data::match_data_math(const data_object *obj,
				       const int_g selection, const bool all,
				       const int_g cmpt, int_g &nmatch,
				       int_g &n) const
{
  const volume_data *data = dynamic_cast<const volume_data *>(obj);
  if (data != 0) {
    if (is_periodic() == data->is_periodic() and
	is_kspace() == data->is_kspace()) {
      if (get_nx() == data->get_nx() and get_ny() == data->get_ny() and
	  get_nz() == data->get_nz()) {
	// Todo - worry about vectors?
	if (all) {
	  nmatch = selection - n;
	  n += get_ndata_sets();
	} else {
	  real_g o1[3];
	  real_g o2[3];
	  get_origin(o1);
	  data->get_origin(o2);
	  const real_g tol = 0.0001;
	  bool ok = true;
	  for (int_g i = 0; i < 3; i++)
	    ok = ok and (std::abs(o1[i] - o2[i]) < tol);
	  if (ok) {
	    get_astep(o1);
	    data->get_astep(o2);
	    bool ok = true;
	    for (int_g i = 0; i < 3; i++)
	      ok = ok and (std::abs(o1[i] - o2[i]) < tol);
	    if (ok) {
	      get_bstep(o1);
	      data->get_bstep(o2);
	      bool ok = true;
	      for (int_g i = 0; i < 3; i++)
		ok = ok and (std::abs(o1[i] - o2[i]) < tol);
	      if (ok) {
		get_cstep(o1);
		data->get_cstep(o2);
		bool ok = true;
		for (int_g i = 0; i < 3; i++)
		  ok = ok and (std::abs(o1[i] - o2[i]) < tol);
		if (ok) {
		  string *labels1 = get_labels();
		  string *labels2 = data->get_labels();
		  for (int_g i = 0; i < get_ndata_sets(); i++) {
		    if (labels1[i] == labels2[cmpt]) {
		      if (selection == n)
			nmatch = i;
		      n++;
		    }
		  }
		}
	      }
	    }
	  }
	}
      }
    }
  }
}

// Todo - most of this is a copy of match
void DLV::volume_data::list_data_math(const data_object *obj, const bool all,
				      const int_g cmpt, string *names,
				      int_g &n) const
{
  const volume_data *data = dynamic_cast<const volume_data *>(obj);
  if (data != 0) {
    if (is_periodic() == data->is_periodic() and
	is_kspace() == data->is_kspace()) {
      if (get_nx() == data->get_nx() and get_ny() == data->get_ny() and
	  get_nz() == data->get_nz()) {
	// Todo - worry about vectors?
	string base = get_data_label() + " - ";
	if (all) {
	  for (int_g i = 0; i < get_ndata_sets(); i++) {
	    names[n] = base + get_sub_data_label(i);
	    n++;
	  }
	} else {
	  real_g o1[3];
	  real_g o2[3];
	  get_origin(o1);
	  data->get_origin(o2);
	  const real_g tol = 0.0001;
	  bool ok = true;
	  for (int_g i = 0; i < 3; i++)
	    ok = ok and (std::abs(o1[i] - o2[i]) < tol);
	  if (ok) {
	    get_astep(o1);
	    data->get_astep(o2);
	    bool ok = true;
	    for (int_g i = 0; i < 3; i++)
	      ok = ok and (std::abs(o1[i] - o2[i]) < tol);
	    if (ok) {
	      get_bstep(o1);
	      data->get_bstep(o2);
	      bool ok = true;
	      for (int_g i = 0; i < 3; i++)
		ok = ok and (std::abs(o1[i] - o2[i]) < tol);
	      if (ok) {
		get_cstep(o1);
		data->get_cstep(o2);
		bool ok = true;
		for (int_g i = 0; i < 3; i++)
		  ok = ok and (std::abs(o1[i] - o2[i]) < tol);
		if (ok) {
		  string *labels1 = get_labels();
		  string *labels2 = data->get_labels();
		  for (int_g i = 0; i < get_ndata_sets(); i++) {
		    if (labels1[i] == labels2[cmpt]) {
		      names[n] = base + get_sub_data_label(i);
		      n++;
		    }
		  }
		}
	      }
	    }
	  }
	}
      }
    }
  }
}

DLVreturn_type DLV::edit3D_math::update_data_math(const render_parent *parent,
						  data_object *obj,
						  const int_g cmpt,
						  const int_g index,
						  char message[],
						  const int_g mlen)
{
  switch (index) {
  case 0:
    obj1 = obj;
    component1 = cmpt;
    break;
  case 1:
    obj2 = obj;
    component2 = cmpt;
    break;
  case 2:
    obj3 = obj;
    component3 = cmpt;
    break;
  default:
    strncpy(message, "BUG: invalid data math field index", mlen);
    return DLV_ERROR;
  }
  message[0] = '\0';
  edited_obj *myedit = get_edit_obj();
  if (myedit == 0) {
    strncpy(message, "Missing object for data math", mlen);
    return DLV_ERROR;
  }
  drawable_obj *p = obj->create_drawable(parent, message, mlen);
  if (p == 0) {
    strncpy(message, "Missing field for data math", mlen);
    return DLV_ERROR;
  }
  if (myedit->update_math(p, cmpt, index, message, mlen))
    return DLV_OK;
  else
    return DLV_ERROR;
}

void DLV::edit3D_vol_render::update()
{
  char message[256];
  int mlen = 256;
  message[0] = '\0';
  edited_obj *obj = get_edit_obj();
  if (obj == 0) {
    strncpy(message, "Missing object for volume", mlen);
    //return DLV_ERROR;
  }
  // Todo - will need changing if we can edit an edit
  const data_object *p = get_base_data();
  const volume_data *v = dynamic_cast<const volume_data *>(p);
  if (v == 0) {
    strncpy(message, "BUG updating volume", mlen);
    //return DLV_ERROR;
  }
  real_g o[3];
  v->get_origin(o);
  real_g a[3];
  v->get_astep(a);
  real_g b[3];
  v->get_bstep(b);
  real_g c[3];
  v->get_cstep(c);
  (void) obj->update_volr(o, a, b, c, v->get_nx(), v->get_ny(),
			  v->get_nz(), v->get_data(), v->get_vector_data(),
			  v->get_ndata_sets(), message, mlen);
}

#endif // ENABLE_DLV_GRAPHICS


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::real_space_box *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::real_space_box("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::k_space_box *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::k_space_box("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::wulff_plot *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::wulff_plot("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar,
				    DLV::rspace_periodic_volume *t,
				    const unsigned int file_version)
    {
      float o[3];
      float a[3];
      float b[3];
      float c[3];
      ::new(t)DLV::rspace_periodic_volume("recover", "temp",
					  0, 0, 0, 0, o, a, b, c);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::k_space_volume *t,
				    const unsigned int file_version)
    {
      float o[3];
      float a[3];
      float b[3];
      float c[3];
      ::new(t)DLV::k_space_volume("recover", "temp", 0, 0, 0, 0, o, a, b, c);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::rspace_wavefunction *t,
				    const unsigned int file_version)
    {
      float o[3];
      float a[3];
      float b[3];
      float c[3];
      ::new(t)DLV::rspace_wavefunction("recover", "temp",
				       0, 0, 0, 0, o, a, b, c);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::bloch_wavefunction *t,
				    const unsigned int file_version)
    {
      float o[3];
      float a[3];
      float b[3];
      float c[3];
      ::new(t)DLV::bloch_wavefunction("recover", "temp",
				      0, 0, 0, 0, o, a, b, c);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::sphere *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::sphere("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::real_space_sphere *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::real_space_sphere("recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::box *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::box("recover");
    }

  }
}

template <class Archive>
void DLV::box::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<data_object>(*this);
  ar & name;
  ar & origin;
  ar & pointa;
  ar & pointb;
  ar & pointc;
}

template <class Archive>
void DLV::real_space_box::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<box>(*this);
}

template <class Archive>
void DLV::k_space_box::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<box>(*this);
}

template <class Archive>
void DLV::sphere::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<data_object>(*this);
  ar & name;
  ar & origin;
  ar & radius;
}

template <class Archive>
void DLV::real_space_sphere::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<sphere>(*this);
}

template <class Archive>
void DLV::wulff_plot::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<data_object>(*this);
  ar & name;
  ar & planes;
}

template <class Archive>
void DLV::volume_data::save(Archive &ar, const unsigned int version) const
{
  ar & boost::serialization::base_object<reloadable_data>(*this);
  ar & nx;
  ar & ny;
  ar & nz;
  ar & n_data_sets;
  for (int_g i = 0; i < n_data_sets; i++) {
    ar & labels[i];
    ar & vector_dim[i];
  }
  for (int_g i = 0; i < 3; i++) {
    ar & origin[i];
    ar & a_step[i];
    ar & b_step[i];
    ar & c_step[i];
  }
}

template <class Archive>
void DLV::volume_data::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<reloadable_data>(*this);
  ar & nx;
  ar & ny;
  ar & nz;
  ar & n_data_sets;
  size = n_data_sets;
  labels = new string[n_data_sets];
  vector_dim = new int_g[n_data_sets];
  data = new real_g *[n_data_sets];
  for (int_g i = 0; i < n_data_sets; i++) {
    ar & labels[i];
    ar & vector_dim[i];
  }
  for (int_g i = 0; i < 3; i++) {
    ar & origin[i];
    ar & a_step[i];
    ar & b_step[i];
    ar & c_step[i];
  }
}

template <class Archive>
void DLV::real_space_volume::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<volume_data>(*this);
}

template <class Archive>
void DLV::rspace_periodic_volume::serialize(Archive &ar,
					    const unsigned int version)
{
  ar & boost::serialization::base_object<real_space_volume>(*this);
}

template <class Archive>
void DLV::k_space_volume::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<volume_data>(*this);
}

template <class Archive>
void DLV::rspace_wavefunction::save(Archive &ar,
				    const unsigned int version) const
{
  ar & boost::serialization::base_object<real_space_volume>(*this);
}

template <class Archive>
void DLV::rspace_wavefunction::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<real_space_volume>(*this);
  phases = new real_g *[get_ndata_sets()];
}

template <class Archive>
void DLV::bloch_wavefunction::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<rspace_wavefunction>(*this);
  ar & k_num;
  ar & k_den;
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::box)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::real_space_box)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::k_space_box)
//BOOST_CLASS_EXPORT_IMPLEMENT(DLV::sphere)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::real_space_sphere)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::wulff_plot)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::rspace_periodic_volume)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::k_space_volume)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::rspace_wavefunction)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::bloch_wavefunction)

DLV_SUPPRESS_TEMPLATES(DLV::data_object)
DLV_SUPPRESS_TEMPLATES(DLV::reloadable_data)

DLV_NORMAL_EXPLICIT(DLV::box)
DLV_NORMAL_EXPLICIT(DLV::real_space_box)
DLV_NORMAL_EXPLICIT(DLV::k_space_box)
DLV_NORMAL_EXPLICIT(DLV::sphere)
DLV_NORMAL_EXPLICIT(DLV::real_space_sphere)
DLV_NORMAL_EXPLICIT(DLV::wulff_plot)
DLV_NORMAL_EXPLICIT(DLV::rspace_periodic_volume)
DLV_NORMAL_EXPLICIT(DLV::k_space_volume)
DLV_SPLIT_EXPLICIT(DLV::rspace_wavefunction)
DLV_NORMAL_EXPLICIT(DLV::bloch_wavefunction)

#  ifdef ENABLE_DLV_GRAPHICS

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::edit3D_ortho *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::edit3D_ortho(0, 0, 0, "");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::edit3D_slice *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::edit3D_slice(0, 0, 0, "");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::edit3D_cut *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::edit3D_cut(0, 0, 0, "");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::edit3D_crop *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::edit3D_crop(0, 0, 0, "");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::edit3D_clamp *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::edit3D_clamp(0, 0, 0, "", 0);
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::edit3D_extend *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::edit3D_extend(0, 0, 0, "");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::edit3D_downsize *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::edit3D_downsize(0, 0, 0, "");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::edit3D_math *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::edit3D_math(0, 0, 0, 0, "");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::edit3D_vol_render *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::edit3D_vol_render(0, 0, 0, "");
    }

  }
}

template <class Archive>
void DLV::edit3D_object::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit_object>(*this);
}

template <class Archive>
void DLV::edit3D_ortho::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit3D_object>(*this);
}

template <class Archive>
void DLV::edit3D_slice::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit3D_object>(*this);
  ar & twod;
}

template <class Archive>
void DLV::edit3D_cut::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit3D_object>(*this);
}

template <class Archive>
void DLV::edit3D_clamp::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit3D_object>(*this);
  ar & component;
}

template <class Archive>
void DLV::edit3D_crop::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit3D_object>(*this);
}

template <class Archive>
void DLV::edit3D_extend::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit3D_object>(*this);
}

template <class Archive>
void DLV::edit3D_downsize::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit3D_object>(*this);
}

template <class Archive>
void DLV::edit3D_math::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit3D_object>(*this);
  ar & obj_component;
  ar & obj1;
  ar & obj2;
  ar & obj3;
  ar & component1;
  ar & component2;
  ar & component3;
}

template <class Archive>
void DLV::edit3D_vol_render::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<edit3D_object>(*this);
}

//BOOST_CLASS_EXPORT_IMPLEMENT(DLV::edit3D_object)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::edit3D_ortho)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::edit3D_slice)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::edit3D_cut)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::edit3D_clamp)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::edit3D_crop)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::edit3D_extend)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::edit3D_downsize)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::edit3D_math)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::edit3D_vol_render)

DLV_SUPPRESS_TEMPLATES(DLV::edit_object)

DLV_NORMAL_EXPLICIT(DLV::edit3D_ortho)
DLV_NORMAL_EXPLICIT(DLV::edit3D_slice)
DLV_NORMAL_EXPLICIT(DLV::edit3D_cut)
DLV_NORMAL_EXPLICIT(DLV::edit3D_clamp)
DLV_NORMAL_EXPLICIT(DLV::edit3D_crop)
DLV_NORMAL_EXPLICIT(DLV::edit3D_extend)
DLV_NORMAL_EXPLICIT(DLV::edit3D_downsize)
DLV_NORMAL_EXPLICIT(DLV::edit3D_math)
DLV_NORMAL_EXPLICIT(DLV::edit3D_vol_render)

#  endif // ENABLE_DLV_GRAPHICS

#endif // DLV_USES_SERIALIZE
