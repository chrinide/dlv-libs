
#include <cmath>
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/toolkit_obj.hxx"
#  include "../graphics/render_base.hxx"
#  include "../graphics/display_objs.hxx"
#  include "../graphics/drawable.hxx"
#  include "../graphics/edit_objs.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "data_objs.hxx"
//#include "operation.hxx"
// for update in point
#include "utils.hxx"
#include "math_fns.hxx"
//#include "atom_model.hxx"
#include "model.hxx"

DLV::data_object::~data_object()
{
#ifdef ENABLE_DLV_GRAPHICS
  std::list<display_obj *>::iterator ptr;
  for (ptr = display.begin(); ptr != display.end(); ++ptr )
    delete (*ptr);
  if (draw != 0)
    delete draw;
#endif // ENABLE_DLV_GRAPHICS
}

bool DLV::data_object::is_edited() const
{
  return false;
}

DLV::string DLV::data_object::get_program_name() const
{
  return program_name;
}

DLV::string DLV::data_object::get_obj_label() const
{
  return get_data_label();
}

bool DLV::data_object::match_type(const string type,
				  const string program) const
{
  return (type == get_obj_label() and program == program_name);
}

bool DLV::data_object::is_atom_bond_data() const
{
  // Todo - use typeid rather than functions like this?
  return false;
}

bool DLV::data_object::is_kspace() const
{
  return false;
}

bool DLV::data_object::is_periodic() const
{
  return false;
}

bool DLV::data_object::is_line() const
{
  return false;
}

bool DLV::data_object::is_plane() const
{
  return false;
}

bool DLV::data_object::is_box() const
{
  return false;
}

bool DLV::data_object::is_wulff() const
{
  return false;
}

bool DLV::data_object::is_real_scalar() const
{
  return false;
}

bool DLV::data_object::is_lattice_direction() const
{
  return false;
}

DLV::string DLV::data_object::get_name() const
{
  return get_data_label();
}

bool DLV::data_object::get_real_value(real_l &v) const
{
  v = 0.0;
  return false;
}

const std::list<DLV::wulff_data> &DLV::data_object::get_wulff_data() const
{
  // potential memory leak if this is ever called (its an error to be here)
  std::list<wulff_data> *p = new std::list<wulff_data>;
  return *p;
}

void DLV::text_buffer::unload_data()
{
  // Do nothing?
}

bool DLV::text_buffer::is_displayable() const
{
  return true;
}

bool DLV::text_buffer::is_editable() const
{
  return false;
}

DLV::string DLV::text_buffer::get_data_label() const
{
  string tag = label + get_tags();
  return tag;
}

DLV::string DLV::text_buffer::get_edit_label() const
{
  return "";
}

DLV::int_g DLV::text_buffer::get_number_data_sets() const
{
  return 1;
}

DLV::string DLV::text_buffer::get_sub_data_label(const int_g) const
{
  return "";
}

DLV::int_g DLV::text_buffer::get_display_type() const
{
  return display_file;
}

DLV::int_g DLV::text_buffer::get_edit_type() const
{
  return no_edit;
}

bool DLV::text_buffer::get_sub_is_vector(const int_g) const
{
  return false;
}

bool DLV::atom_and_bond_data::is_displayable() const
{
  return true;
}

bool DLV::atom_and_bond_data::is_editable() const
{
  return false;
}

DLV::string DLV::atom_and_bond_data::get_data_label() const
{
  return "Selected atom positions, bond lengths and angles";
}

DLV::string DLV::atom_and_bond_data::get_edit_label() const
{
  string x;
  return x;
}

DLV::int_g DLV::atom_and_bond_data::get_number_data_sets() const
{
  return 1;
}

DLV::string DLV::atom_and_bond_data::get_sub_data_label(const int_g) const
{
  string x;
  return x;
}

DLV::int_g DLV::atom_and_bond_data::get_display_type() const
{
  return display_atoms_bonds;
}

DLV::int_g DLV::atom_and_bond_data::get_edit_type() const
{
  return no_edit;
}

bool DLV::atom_and_bond_data::get_sub_is_vector(const int_g) const
{
  return false;
}

bool DLV::atom_and_bond_data::is_atom_bond_data() const
{
  return true;
}

void DLV::atom_and_bond_data::unload_data()
{
  // Do nothing
}

bool DLV::lattice_direction_data::is_displayable() const
{
  return true;
}

bool DLV::lattice_direction_data::is_editable() const
{
  return false;
}

bool DLV::lattice_direction_data::is_lattice_direction() const
{
  return true;
}

DLV::string DLV::lattice_direction_data::get_data_label() const
{
  return "Lattice directions from atom selections";
}

DLV::string DLV::lattice_direction_data::get_edit_label() const
{
  string x;
  return x;
}

DLV::int_g DLV::lattice_direction_data::get_number_data_sets() const
{
  return 1;
}

DLV::string DLV::lattice_direction_data::get_sub_data_label(const int_g) const
{
  string x;
  return x;
}

DLV::int_g DLV::lattice_direction_data::get_display_type() const
{
  return display_direction;
}

DLV::int_g DLV::lattice_direction_data::get_edit_type() const
{
  return no_edit;
}

bool DLV::lattice_direction_data::get_sub_is_vector(const int_g) const
{
  return false;
}

void DLV::lattice_direction_data::unload_data()
{
  // Do nothing
}

#ifdef ENABLE_DLV_GRAPHICS

void DLV::data_object::reattach_objects(const render_parent *parent)
{
  if (draw != 0) {
    if (display.size() > 0) {
      std::list<display_obj *>::iterator x;
      for (x = display.begin(); x != display.end(); x++ )
	(*x)->reattach_data(draw, parent);
    }
    if (modified.size() > 0) {
      std::list<edited_obj *>::iterator x;
      for (x = modified.begin(); x != modified.end(); x++ )
	(*x)->reload_data(draw);
    }
  }
}

void DLV::data_object::list_display_objects(const render_parent *parent) const
{
  if (display.size() > 0) {
    std::list<display_obj *>::const_iterator x;
    for (x = display.begin(); x != display.end(); x++ )
      (*x)->update_display_list(parent);
  }
}

bool DLV::data_object::check_display_index(const int_g id) const
{
  bool found = false;
  if (display.size() > 0) {
    std::list<display_obj *>::const_iterator x;
    for (x = display.begin(); x != display.end(); x++ ) {
      if ((*x)->get_index() == id) {
	found = true;
	break;
      }
    }
  }
  return found;
}

void DLV::data_object::list_edit_object(const render_parent *parent) const
{
  // Dummy
}

bool DLV::data_object::check_edit_index(const int_g id) const
{
  return false;
}

// Todo - common find code for select/remove
DLVreturn_type DLV::data_object::select(const int_g object, char message[],
					const int_g mlen)
{
  // Find display object and attach params
  std::list<display_obj *>::iterator x;
  bool found = false;
  int_g count = 0;
  if (display.size() > 0) {
    for (x = display.begin(); x != display.end(); x++ ) {
      if ((*x)->get_index() == object) { 
	found = true;
	break;
      }
      count++;
    }
  }
  if (found) {
    (*x)->attach_params();
    return DLV_OK;
  } else {
    strncpy(message, "BUG: display object not selected", mlen);
    return DLV_ERROR;
  }
}

DLVreturn_type DLV::data_object::remove(const int_g object, char message[],
					const int_g mlen)
{
  std::list<display_obj *>::iterator x;
  bool found = false;
  if (display.size() > 0) {
    for (x = display.begin(); x != display.end(); x++ ) {
      if ((*x)->get_index() == object) { 
	found = true;
	break;
      }
    }
  }
  if (found) {
    display_obj *ptr = *x;
    delete ptr;
    display.erase(x);
    return DLV_OK;
  } else {
    strncpy(message, "BUG: display object not removed", mlen);
    return DLV_ERROR;
  }
}

DLVreturn_type DLV::data_object::remove_display(const render_parent *parent,
						class model *structure,
						int_g &first, int_g &last,
						char message[],
						const int_g mlen)
{
  std::list<display_obj *>::iterator x;
  first = -1;
  last = -1;
  if (display.size() > 0) {
    for (x = display.begin(); x != display.end(); x++ ) {
      last = (*x)->get_index();
      if (first < 0)
	first = last;
      display_obj *ptr = *x;
      delete ptr;
    }
    display.clear();
  }
  return DLV_OK;
}

DLV::int_g DLV::data_object::get_nframes() const
{
  // BUG
  return 1;
}

const std::vector<DLV::real_g (*)[3]> *DLV::data_object::get_trajectory() const
{
  // BUG
  return 0;
}

bool DLV::data_object::is_gamma_point() const
{
  // BUG
  return false;
}

void DLV::data_object::get_kpoint_cell(int_g kc[3]) const
{
  // BUG
  kc[0] = 1;
  kc[1] = 1;
  kc[2] = 1;
}

void DLV::data_object::update_trajectory(class model *)
{
  // BUG
}

DLVreturn_type DLV::data_object::use_view_editor(const render_parent *parent,
						 const bool v, char message[],
						 const int_g mlen)
{
  strncpy(message, "BUG: Data object does not support editor", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::data_object::update3D(const int_g method, const int_g h,
					  const int_g k, const int_g l,
					  const real_g x, const real_g y,
					  const real_g z, const int_g object,
					  const bool conv, const bool frac,
					  const bool p_obj, model *const m,
					  char message[], const int_g mlen)
{
  strncpy(message, "BUG: Not 3D region to update", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::data_object::update3D(const int_g method, const real_g r,
					  model *const m,
					  char message[], const int_g mlen)
{
  strncpy(message, "BUG: Not sphere to update", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::data_object::update2D(const int_g method, const int_g h,
					  const int_g k, const int_g l,
					  const real_g x, const real_g y,
					  const real_g z, const int_g obj,
					  const bool conv, const bool frac,
					  const bool parent, model *const m,
					  char message[], const int_g mlen)
{
  strncpy(message, "BUG: Not plane to update", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::data_object::update1D(const int_g method, const int_g h,
					  const int_g k, const int_g l,
					  const real_g x, const real_g y,
					  const real_g z, const int_g obj,
					  const bool conv, const bool frac,
					  const bool parent, model *const m,
					  char message[], const int_g mlen)
{
  strncpy(message, "BUG: Not line to update", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::data_object::update0D(const int_g method, const real_g x,
					  const real_g y, const real_g z,
					  const bool conv, model *const m,
					  char message[], const int_g mlen)
{
  strncpy(message, "BUG: Not point to update", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::data_object::update_wulff(const int_g, const int_g,
					      const int_g, const real_g,
					      const real_g, const real_g,
					      const real_g, const bool,
					      model *const,
					      char message[], const int_g mlen)
{
  strncpy(message, "BUG: Not wulff plot to update", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::data_object::update_wulff(const char [], model *const,
					      char message[], const int_g mlen)
{
  strncpy(message, "BUG: Not wulff plot to update", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::data_object::update_leed(const bool, model *const,
					     char message[], const int_g mlen)
{
  strncpy(message, "BUG: Not LEED pattern to update", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::data_object::update_spectrum(const int_g type,
						 const real_g emin,
						 const real_g emax,
						 const int_g np,
						 const real_g w,
						 const real_g harmonic,
						 char message[],
						 const int_g mlen)
{
  strncpy(message, "BUG: Not a spectrum to update", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::data_object::update_phonon(const render_parent *parent,
					       class model *structure,
					       const int_g nframes,
					       const real_g scale,
					       const bool use_temp,
					       const real_g temperature,
					       char message[], const int_g mlen)
{
  strncpy(message, "BUG: Not a phonon trajectory to update", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::data_object::update_slice(const render_parent *,
					      class model *, data_object *,
					      char message[], const int_g mlen)
{
  strncpy(message, "BUG: Not a slice to update", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::data_object::update_cut(const render_parent *,
					    class model *, data_object *,
					    char message[], const int_g mlen)
{
  strncpy(message, "BUG: Not a cut to update", mlen);
  return DLV_ERROR;
}

DLVreturn_type
DLV::data_object::update_extension(const render_parent *parent, const int_g na,
				   const int_g nb, const int_g nc,
				   char message[], const int_g mlen)
{
  strncpy(message, "BUG: Not an extension to update", mlen);
  return DLV_ERROR;
}

bool DLV::data_object::hide_render(const int_g object, const int_g visible,
				   const render_parent *parent,
				   class model *structure,
				   char message[], const int_g mlen)
{
  // Dummy
  return true;
}

DLVreturn_type 
DLV::data_object::transfer_and_render(class data_object *old_data,
				      const render_parent *parent,
				      char message[], const int_g mlen)
{
  //Dummy
  return DLV_ERROR;
}

void DLV::data_object::update_streamlines(const int_g object,
					  data_object *plane,
					  const render_parent *parent,
					  char message[], const int_g mlen)
{
  bool found = false;
  if (display.size() > 0) {
    std::list<display_obj *>::const_iterator x;
    for (x = display.begin(); x != display.end(); x++ ) {
      if ((*x)->get_index() == object) {
	found = true;
	break;
      }
    }
    if (found) {
      drawable_obj *p = plane->get_drawable();
      if (p == 0)
	strncpy(message, "Missing plane for streamlines", mlen);
      else
	(*x)->update_streamlines(p, message, mlen);
    } else
      strncpy(message, "BUG locating streamline object", mlen);
  }
}

DLVreturn_type DLV::data_object::select(char message[], const int_g mlen)
{
  strncpy(message, "BUG: attach edit for no-edit data object", mlen);
  return DLV_ERROR;
}

DLV::data_object *DLV::data_object::edit(const render_parent *parent,
					 class model *structure,
					 const int_g component,
					 const int_g method, const int_g index,
					 char message[], const int_g mlen)
{
  strncpy(message, "BUG: object has no edit operation", mlen);
  return 0;
}

void DLV::data_object::undisplay() const
{
  // Todo - not very general
  display_obj *obj = *(display.begin());
  obj->turn_off_visible();
}

void DLV::data_object::match_data_math(const data_object *, const int_g,
				       const bool, const int_g,
				       int_g &, int_g &) const
{
  // dummy
}

void DLV::data_object::list_data_math(const data_object *obj, const bool all,
				      const int_g cmpt, string *names,
				      int_g &n) const
{
  // dummy
}

void DLV::data_object::data_math_parent(const data_object * &obj,
					int_g &component) const
{
  // bug
  obj = 0;
  component = 0;
}

DLVreturn_type DLV::data_object::update_data_math(const render_parent *parent,
						  data_object *obj,
						  const int_g cmpt,
						  const int_g index,
						  char message[],
						  const int_g mlen)
{
  strncpy(message, "BUG: not a data math object", mlen);
  return DLV_ERROR;
}

DLVreturn_type DLV::data_object::update_shift(const real_g shift,
					      char message[], const int_g mlen)
{
  strncpy(message, "BUG: not a plot edit object", mlen);
  return DLV_ERROR;
}  

DLV::drawable_obj *DLV::data_object::get_drawable()
{
  return draw;
}

DLV::drawable_obj *
DLV::data_object::create_drawable(const render_parent *parent,
				  char message[], const int_g mlen)
{
  return draw;
}

void DLV::data_object::set_drawable(drawable_obj *obj)
{
  draw = obj;
}

void DLV::data_object::add_display_obj(display_obj *obj)
{
  display.push_back(obj);
}

DLV::display_obj *DLV::data_object::remove_display_obj()
{
  DLV::display_obj *obj = display.back();
  display.pop_back();
  return obj;
}

void DLV::data_object::delete_drawable()
{
  delete draw;
  draw = 0;
}

void DLV::data_object::add_edit(edited_obj *obj)
{
  modified.push_back(obj);
}

void DLV::data_object::attach_updatable()
{
}

bool DLV::data_object::update_atom_selection_info(const class model *,
						  char message[],
						  const int_g mlen)
{
  // Do nothing
  return true;
}

bool DLV::data_object::update_cell_info(model *m, char message[],
					const int_g mlen)
{
  // Do nothing
  return true;
}

DLVreturn_type DLV::text_buffer::render(const render_parent *parent,
					class model *structure,
					char message[], const int_g mlen)
{
  return render(parent, structure, 0, 0, 0, message, mlen);
  //return DLV_OK; // issue with crystal opt structure
}

DLVreturn_type DLV::text_buffer::render(const render_parent *parent,
					class model *structure,
					const int_g component,
					const int_g method, const int_g index,
					char message[], const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    obj = DLV::drawable_obj::create_text_buff(parent, message, mlen);
    if (obj == 0)
      return DLV_ERROR;
    else
      set_drawable(obj);
  }
  if (!object_is_displayed()) {
    display_obj *ptr = 0;
    string name = get_data_label();
    ptr = display_obj::create_text_view(parent, obj, name,
					index, message, mlen);
    if (ptr == 0) {
      if (strlen(message) == 0)
	strncpy(message, "Unable to allocate display object", mlen);
      return DLV_ERROR;
    } else {
      add_display_obj(ptr);
      ptr->update_display_list(parent, name);
      ptr->attach_params();
    }
  }
  return DLV_OK;
}

void DLV::text_buffer::expand_lines(const int_g n)
{
  int_g new_lines = nlines + n;
  if (pending_eol)
    new_lines--;
  drawable_obj *obj = get_drawable();
  obj->expand_text_list(new_lines);
}

void DLV::text_buffer::add_line(const char line[], const bool no_eol)
{
  drawable_obj *obj = get_drawable();
  obj->add_text_line(line, nlines, pending_eol);
  nlines++;
  pending_eol = no_eol;
}

bool DLV::atom_and_bond_data::update_cell_info(model *m, char message[],
					       const int_g mlen)
{
  string formula = m->get_formula();
  drawable_obj *d = get_drawable();
  d->update_formula(formula);
  return update_atom_selection_info(m, message, mlen);
}

DLVreturn_type DLV::atom_and_bond_data::render(const render_parent *parent,
					       class model *structure,
					       char message[], const int_g mlen)
{
  return render(parent, structure, 0, 0, 0, message, mlen);
}

DLVreturn_type
DLV::atom_and_bond_data::render(const render_parent *parent,
				class model *structure,
				const int_g component, const int_g method,
				const int_g index, char message[],
				const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    string formula = structure->get_formula();
    obj = DLV::drawable_obj::create_lengths_angles(parent, formula,
						   message, mlen);
    if (obj == 0)
      return DLV_ERROR;
    else
      set_drawable(obj);
    display_obj *ptr = 0;
    string name = get_data_label();
    ptr = display_obj::create_bond_text(parent, obj, name,
					index, message, mlen);
    if (ptr == 0) {
      if (strlen(message) == 0)
	strncpy(message, "Unable to allocate display object", mlen);
      return DLV_ERROR;
    } else {
      add_display_obj(ptr);
      ptr->update_display_list(parent, name);
      ptr->attach_params();
    }
  }
  return DLV_OK;
}

void DLV::atom_and_bond_data::attach_updatable()
{
  display_obj *ptr = get_display_obj();
  ptr->attach_params();
}

bool DLV::atom_and_bond_data::update_atom_selection_info(const model *m,
							 char message[],
							 const int_g mlen)
{
  int_g natoms;
  real_l x1, y1, z1, length, angle;
  real_l x2, y2, z2;
  char sym1[16], sym2[16], sym3[16];
  real_g r1, r2, r3;
  string gp;
  string label;
  natoms = m->get_atom_selection_info(x1, y1, z1, length, x2, y2, z2, angle,
				      sym1, sym2, sym3, r1, r2, r3, gp, label);
  drawable_obj *d = get_drawable();
  char group_label[128];
  if (label == "")
    group_label[0] = '\0';
  else
    snprintf(group_label, 128, "%s (%s)", label.c_str(), gp.c_str());
  d->update_atoms_and_bonds(natoms, x1, y1, z1, length, x2, y2, z2, angle,
			    sym1, sym2, sym3, r1, r2, r3, group_label);
  return true;
}

DLVreturn_type DLV::lattice_direction_data::render(const render_parent *parent,
						   class model *structure,
						   char message[],
						   const int_g mlen)
{
  //return render(parent, structure, 0, 0, 0, message, mlen);
  return DLV_OK;
}

DLVreturn_type
DLV::lattice_direction_data::render(const render_parent *parent,
				    class model *structure,
				    const int_g component, const int_g method,
				    const int_g index, char message[],
				    const int_g mlen)
{
  message[0] = '\0';
  drawable_obj *obj = get_drawable();
  if (obj == 0) {
    obj = DLV::drawable_obj::create_text_value(parent, "", message, mlen);
    if (obj == 0)
      return DLV_ERROR;
    else
      set_drawable(obj);
    display_obj *ptr = 0;
    string name = get_data_label();
    ptr = display_obj::create_text_display(parent, obj, name, index,
					   message, mlen);
    if (ptr == 0) {
      if (strlen(message) == 0)
	strncpy(message, "Unable to allocate display object", mlen);
      return DLV_ERROR;
    } else {
      add_display_obj(ptr);
      update_atom_selection_info(structure, message, mlen);
      ptr->update_display_list(parent, name);
      ptr->attach_params();
    }
  }
  return DLV_OK;
}

bool DLV::lattice_direction_data::update_atom_selection_info(const model *m,
							     char message[],
							     const int_g mlen)
{
  string text;
  if (get_lattice_info(text, m, message, mlen)) {
    drawable_obj *d = get_drawable();
    if (d != 0) // It hasn't been displayed yet
      d->update_text_value(text);
    return true;
  } else
    return false;
}

bool DLV::lattice_direction_data::get_lattice_info(string &text,
						   const model *m,
						   char message[],
						   const int_g mlen)
{
  bool ok = true;
  coord_type p1[3];
  coord_type p2[3];
  coord_type p3[3];
  int_g n = 1;
  if (m->get_selected_positions(p1, p2, p3))
    n = 3;
  else if (m->get_selected_positions(p1, p2))
    n = 2;
  if (n > 1) {
    int_g dim = m->get_number_of_periodic_dims();
    //if (dim == 2)
    //  parent = model->find_bulk_parent();
    // I could let dim == 1 do something but it doesn't seem worth it.
    if (dim < 2)
      text = "";
    else {
      coord_type a[3];
      coord_type b[3];
      coord_type c[3];
      m->get_conventional_lattice(a, b, c);
      coord_type lattice[3][3];
      for (int_g i = 0; i < 3; i++) {
	lattice[0][i] = a[i];
	lattice[1][i] = b[i];
	lattice[2][i] = c[i];
      }
      ok = set_vector_info(lattice, p1, p2, dim, text, false);
      /*if (ok == DLV_STAT_SUCCESS && parent != NULL) {
	parent->get_lattice_vectors(platt, cell_conventional, true);
	model->rotate_lattice(platt);
	ok = set_vector_info(platt, p1, p2, 3, buff, true);
	}*/
      if (ok and n > 2) {
	ok = set_plane_info(lattice, p1, p2, p3, dim, text, false);
	/*if (ok == DLV_STAT_SUCCESS && parent != NULL)
	  ok = set_plane_info(platt, p1, p2, p3, 3, buff, true);*/
      }
    }
  } else
    text = "";  
  return ok;
}

// A general vector x = LT . a, where a are the coefficients and LT the
// transpose of the matrix where the rows are the lattice vectors.
// So a = LT^-1 . x, and to get miller indices we need to turn a into ints.

bool
DLV::lattice_direction_data::set_vector_info(const coord_type lattice[3][3],
					     const coord_type p1[3],
					     const coord_type p2[3],
					     const int_g dim, string &text,
					     const bool bulk)
{
  coord_type x[3];
  coord_type sum = 0.0;
  for (int_g i = 0; i < 3; i++) {
    x[i] = p1[i] - p2[i];
    sum += std::abs(x[i]);
  }
  const coord_type tol = 0.0001;
  if (sum < tol) {
    if (!bulk)
      text += "No line (identical atoms)";
    return true;
  }
  coord_type lt[3][3];
  matrix_transpose(lattice, lt, dim);
  coord_type ltinv[3][3];
  matrix_invert(lt, ltinv, dim);
  coord_type a[3];
  for (int_g i = 0; i < dim; i++) {
    a[i] = 0.0;
    for (int_g j = 0; j < dim; j++)
      a[i] += ltinv[i][j] * x[j];
  }
  for (int_g i = dim; i < 3; i++)
    a[i] = 0.0;
  int_g h;
  int_g k;
  int_g l;
  bool failed = false;
  miller_to_integer(a[0], a[1], a[2], h, k, l, dim, failed);
  // Now write the info.
  if (failed) {
    if (bulk)
      text += "(unable to identify bulk direction)";
    else
      text += "Unable to identify direction";
  } else {
    char buff[128];
    if (dim == 2)
      snprintf(buff, 128, "direction is [%1d %1d]", h, k);
    else if (bulk)
      snprintf(buff, 128, " (bulk direction is [%1d %1d %1d])", h, k, l);
    else
      snprintf(buff, 128, "direction is [%1d %1d %1d]", h, k, l);
    text += buff;
  }
  return true;
}

bool
DLV::lattice_direction_data::set_plane_info(const coord_type lattice[3][3],
					    const coord_type p1[3],
					    const coord_type p2[3],
					    const coord_type p3[3],
					    const int_g dim, string &text,
					    const bool bulk)
{
  // Make sure the 3 points aren't in a line!
  // x = p1 + t (p3 - p1). See if p2 is a solution of this line equation
  coord_type lp[3];
  coord_type ld[3];
  for (int_g i = 0; i < 3; i++) {
    lp[i] = p1[i];
    ld[i] = p3[i] - p1[i];
  }
  coord_type t;
  coord_type a;
  coord_type b;
  coord_type c;
  bool is_line = false;
  const coord_type tol = 0.0001;
  if (dim == 2) {
    if (std::abs(ld[0]) > tol) {
      t = (p2[0] - lp[0]) / ld[0];
      b = lp[1] + t * ld[1];
      if (std::abs(b - p2[1]) < tol)
	is_line = true;
    } else if (std::abs(ld[1]) > tol) {
      t = (p2[1] - lp[1]) / ld[1];
      a = lp[0] + t * ld[0];
      if (std::abs(a - p2[0]) < tol)
	is_line = true;
    }
  } else {
    if (std::abs(ld[0]) > tol) {
      t = (p2[0] - lp[0]) / ld[0];
      b = lp[1] + t * ld[1];
      c = lp[2] + t * ld[2];
      if ((std::abs(b - p2[1]) < tol) and (std::abs(c - p2[2]) < tol))
	is_line = true;
    } else if (fabs(ld[1]) > tol) {
      t = (p2[1] - lp[1]) / ld[1];
      a = lp[0] + t * ld[0];
      c = lp[2] + t * ld[2];
      if ((std::abs(a - p2[0]) < tol) and (std::abs(c - p2[2]) < tol))
	is_line = true;
    } else  if (std::abs(ld[2]) > tol) {
      t = (p2[2] - lp[2]) / ld[2];
      a = lp[0] + t * ld[0];
      b = lp[1] + t * ld[1];
      if ((std::abs(a - p2[0]) < tol) and (std::abs(b - p2[1]) < tol))
	is_line = true;
    }
  }
  if (is_line) {
    if (!bulk)
      text += ", no plane (atoms form line)";
    return true;
  }
  // Not in a line so search for plane.
  // Plane equation is ax + by + cz = d, and can be solved based on 3 points
  // in the plane which we have. Vector version with 3 points is
  // (r - p1) . ((p2 - p1) x (p3 - p1)) = 0. In expanded form
  a = (p2[1] * p3[2] - p2[1] * p1[2] - p1[1] * p3[2] - p2[2] * p3[1]
       + p2[2] * p1[1] + p1[2] * p3[1]);
  b = (p2[2] * p3[0] - p2[2] * p1[0] - p1[2] * p3[0] - p2[0] * p3[2]
       + p2[0] * p1[2] + p1[0] * p3[2]);
  c = (p2[0] * p3[1] - p2[0] * p1[1] - p1[0] * p3[1] - p2[1] * p3[0]
       + p2[1] * p1[0] + p1[1] * p3[0]);
  coord_type d = p1[0] * a + p1[1] * b + p1[2] * c;
  if (std::abs(d) < tol) {
    // Intercept is at origin, so shift points by one of each lattice vector
    // and try again.
    coord_type q1[3];
    coord_type q2[3];
    coord_type q3[3];
    for (int_g i = 0; i < 3; i++) {
      q1[i] = p1[i] + lattice[0][i] + lattice[1][i];
      q2[i] = p2[i] + lattice[0][i] + lattice[1][i];
      q3[i] = p3[i] + lattice[0][i] + lattice[1][i];
    }
    if (dim == 3) {
      for (int_g i = 0; i < 3; i++) {
	q1[i] = q1[i] + lattice[2][i];
	q2[i] = q2[i] + lattice[2][i];
	q3[i] = q3[i] + lattice[2][i];
      }
    }
    a = (q2[1] * q3[2] - q2[1] * q1[2] - q1[1] * q3[2] - q2[2] * q3[1]
	 + q2[2] * q1[1] + q1[2] * q3[1]);
    b = (q2[2] * q3[0] - q2[2] * q1[0] - q1[2] * q3[0] - q2[0] * q3[2]
	 + q2[0] * q1[2] + q1[0] * q3[2]);
    c = (q2[0] * q3[1] - q2[0] * q1[1] - q1[0] * q3[1] - q2[1] * q3[0]
	 + q2[1] * q1[0] + q1[1] * q3[0]);
    d = q1[0] * a + q1[1] * b + q1[2] * c;
  }
  int_g h = 0;
  int_g k = 0;
  int_g l = 0;
  bool failed = false;
  if (std::abs(d) < tol)
    failed = true;
  //printf("Bulk identification failure %f %f %f %f\n", a, b, c, d);
  else {
    // Solve intercepts for lattice vectors (sligtly harder than if cartesian).
    // Just substitute a coeff x the vector into the equation and solve for
    // the single coeff. We use t / d since we want the reciprocals
    coord_type fh = 0.0;
    t = (a * lattice[0][0] + b * lattice[0][1] + c * lattice[0][2]);
    if (std::abs(t) > tol)
      fh = t / d;
    coord_type fk = 0.0;
    t = (a * lattice[1][0] + b * lattice[1][1] + c * lattice[1][2]);
    if (std::abs(t) > tol)
      fk = t / d;
    coord_type fl = 0.0;
    if (dim == 3) {
      t = (a * lattice[2][0] + b * lattice[2][1] + c * lattice[2][2]);
      if (std::abs(t) > tol)
	fl = t / d;
    }
    // Turn these into integer plane indices.
    miller_to_integer(fh, fk, fl, h, k, l, dim, failed);
  }
  // Now write the info.
  if (failed) {
    if (bulk)
      text += ", (unable to identify bulk plane)";
    else
      text += ", unable to identify plane";
  } else {
    char buff[128];
    if (dim == 2)
      snprintf(buff, 128, ", plane is (%1d %1d)", h, k);
    else if (bulk)
      snprintf(buff, 128, " (bulk plane is (%1d %1d %1d))", h, k, l);
    else
      snprintf(buff, 128, ", plane is (%1d %1d %1d)", h, k, l);
    text += buff;
  }
  return true;
}

void DLV::lattice_direction_data::miller_to_integer(const coord_type fh,
						    const coord_type fk,
						    const coord_type fl,
						    int_g &h, int_g &k,
						    int_g &l, const int_g dim,
						    bool &failed)
{
  const coord_type tol = 1e-4;
  // Now convert a to integer values - the tricky bit.
  coord_type scale;
  if (std::abs(fh) < 1.0 and std::abs(fk) < 1.0 and std::abs(fl) < 1.0) {
    // If all < 1 then just divide by their product, its easy
    scale = 1.0;
    if (std::abs(fh) > tol)
      scale *= std::abs(fh);
    if (std::abs(fk) > tol)
      scale *= std::abs(fk);
    if (dim == 3) {
      if (std::abs(fl) > tol)
	scale *= std::abs(fl);
    }
    h = (int) floor(fh / scale + 0.01);
    k = (int) floor(fk / scale + 0.01);
    l = (int) floor(fl / scale + 0.01);
  } else {
    // Ideally we ought to generate limit based on the values in a[].
    bool failed = true;
    for (int_g i = 1; i <= 1000; i++) {
      scale = (coord_type) i;
      coord_type total = 0.0;
      coord_type mh = scale * std::abs(fh);
      if (mh > tol)
	total += (mh - floor(mh + 0.1)) / mh;
      coord_type mk = scale * std::abs(fk);
      if (mk > tol)
	total += (mk - floor(mk + 0.1)) / mk;
      if (dim == 3) {
	coord_type ml = scale * std::abs(fl);
	if (ml > tol)
	  total += (ml - floor(ml + 0.1)) / ml;
      }
      if (total < 3.0 * tol) {
	failed = false;
	break;
      }
    }
    if (!failed) {
      h = (int) floor(fh * scale + 0.1);
      k = (int) floor(fk * scale + 0.1);
      l = (int) floor(fl * scale + 0.1);
    }
  }
  if (!failed) {
    int_g gcd = 1;
    int_g mhkl = std::abs(h) + std::abs(k) + std::abs(l);
    // Get smallest integer values
    if (h != 0 and std::abs(h) < mhkl)
      mhkl = std::abs(h);
    if (k != 0 and std::abs(k) < mhkl)
      mhkl = std::abs(k);
    if (l != 0 and std::abs(l) < mhkl)
      mhkl = std::abs(l);
    for (int_g i = 2; i <= mhkl; i++)
      if ((h % i) == 0 and (k % i) == 0 and (l % i) == 0)
	gcd = i;
    h = h / gcd;
    k = k / gcd;
    l = l / gcd;
  }
}

#endif // ENABLE_DLV_GRAPHICS


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::text_buffer *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::text_buffer("recover", "temp", "");
    }

  }
}

template <class Archive>
void DLV::text_buffer::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<data_object>(*this);
  ar & label;
}

template <class Archive>
void DLV::data_object::serialize(Archive &ar, const unsigned int version)
{
  ar & program_name;
  ar & source;
#ifdef ENABLE_DLV_GRAPHICS
  ar & draw;
  ar & display;
  ar & modified;
#endif // ENABLE_DLV_GRAPHICS
}

template <class Archive>
void DLV::atom_and_bond_data::save(Archive &ar,
				   const unsigned int version) const
{
  ar & boost::serialization::base_object<data_object>(*this);
}

template <class Archive>
void DLV::atom_and_bond_data::load(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<data_object>(*this);
#ifdef ENABLE_DLV_GRAPHICS
  reattach_objects();
#endif // ENABLE_DLV_GRAPHICS
}

template <class Archive>
void DLV::lattice_direction_data::serialize(Archive &ar,
					    const unsigned int version)
{
  ar & boost::serialization::base_object<data_object>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::text_buffer)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::atom_and_bond_data)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::lattice_direction_data)

DLV_NORMAL_EXPLICIT(DLV::text_buffer)
DLV_SPLIT_EXPLICIT(DLV::atom_and_bond_data)
DLV_NORMAL_EXPLICIT(DLV::lattice_direction_data)
DLV_EXPORT_EXPLICIT(DLV::data_object)

#endif // DLV_USES_SERIALIZE
