
#include <cmath>
#include <list>
#include <map>
#include <cctype>
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "utils.hxx"
#include "constants.hxx"
#include "atom_prefs.hxx"
#include "atom_model.hxx"
#include "model.hxx"
#include "operation.hxx"
#include "op_model.hxx"
#include "load_model.hxx"
#include "settings.hxx"
#include "extern_model.hxx"

DLV::string DLV::load_xyz_file::get_name() const
{
  return (get_model_name() + " - Load XYZ");
}

DLV::operation *DLV::load_xyz_file::create(const char name[],
					   const char filename[],
					   const bool bonds, char message[],
					   const int_g mlen)
{
  message[0] = '\0';
  load_xyz_file *op = 0;
  DLVreturn_type ok = DLV_OK;
  if (check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (open_file_read(input, filename, message, mlen)) {
      model *structure = read(input, name_from_file(name, filename),
			      message, mlen);
      if (structure == 0) {
	strncpy(message, "Create model failed", mlen - 1);
	ok = DLV_ERROR;
      } else {
	if (ok == DLV_OK) {
	  structure->complete();
	  op = new load_xyz_file(structure, filename);
	  attach_base(op);
	  if (bonds)
	    op->set_bond_all();
	  //Todo? if (strlen(message) > 0)
	  //  ok = DLV_WARNING;
	} else
	  delete structure;
      }
      if (ok != DLV_OK)
	message[mlen - 1] = '\0';
      input.close();
    }
  }
  return op;
}

DLV::string DLV::load_xtl_file::get_name() const
{
  return (get_model_name() + " - Load XTL");
}

DLV::operation *DLV::load_xtl_file::create(const char name[],
					   const char filename[],
					   const bool bonds, char message[],
					   const int_g mlen)
{
  message[0] = '\0';
  load_xtl_file *op = 0;
  DLVreturn_type ok = DLV_OK;
  if (check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (open_file_read(input, filename, message, mlen)) {
      model *structure = read(input, name_from_file(name, filename),
			      message, mlen);
      if (structure == 0) {
	strncpy(message, "Create model failed", mlen - 1);
	ok = DLV_ERROR;
      } else {
	if (ok == DLV_OK) {
	  structure->complete();
	  op = new load_xtl_file(structure, filename);
	  attach_base(op);
	  if (bonds)
	    op->set_bond_all();
	  //Todo? if (strlen(message) > 0)
	  //  ok = DLV_WARNING;
	} else
	  delete structure;
      }
      if (ok != DLV_OK)
	message[mlen - 1] = '\0';
      input.close();
    }
  }
  return op;
}

DLV::string DLV::load_cssr_file::get_name() const
{
  return (get_model_name() + " - Load XR/CSSR");
}

DLV::operation *DLV::load_cssr_file::create(const char name[],
					    const char filename[],
					    const bool bonds, char message[],
					    const int_g mlen)
{
  message[0] = '\0';
  load_cssr_file *op = 0;
  DLVreturn_type ok = DLV_OK;
  if (check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (open_file_read(input, filename, message, mlen)) {
      model *structure = read(input, name_from_file(name, filename),
			      message, mlen);
      if (structure == 0) {
	strncpy(message, "Create model failed", mlen - 1);
	ok = DLV_ERROR;
      } else {
	if (ok == DLV_OK) {
	  structure->complete();
	  op = new load_cssr_file(structure, filename);
	  attach_base(op);
	  if (bonds)
	    op->set_bond_all();
	  //Todo? if (strlen(message) > 0)
	  //  ok = DLV_WARNING;
	} else
	  delete structure;
      }
      if (ok != DLV_OK)
	message[mlen - 1] = '\0';
      input.close();
    }
  }
  return op;
}

DLV::string DLV::load_shelx_file::get_name() const
{
  return (get_model_name() + " - Load ShelX");
}

DLV::operation *DLV::load_shelx_file::create(const char name[],
					     const char filename[],
					     const bool bonds, char message[],
					     const int_g mlen)
{
  message[0] = '\0';
  load_shelx_file *op = 0;
  DLVreturn_type ok = DLV_OK;
  if (check_filename(filename, message, mlen)) {
    std::ifstream input;
    if (open_file_read(input, filename, message, mlen)) {
      model *structure = read(input, name_from_file(name, filename),
			      message, mlen);
      if (structure == 0) {
	strncpy(message, "Create model failed", mlen - 1);
	ok = DLV_ERROR;
      } else {
	if (ok == DLV_OK) {
	  structure->complete();
	  op = new load_shelx_file(structure, filename);
	  attach_base(op);
	  if (bonds)
	    op->set_bond_all();
	  //Todo? if (strlen(message) > 0)
	  //  ok = DLV_WARNING;
	} else
	  delete structure;
      }
      if (ok != DLV_OK)
	message[mlen - 1] = '\0';
      input.close();
    }
  }
  return op;
}

// All of this is basically straight from DLV v2

static void expand_label(char label[], char string[], const int reverse);
static void gen_vectors(const double w[3][3], double prim[3][3],
			const double a, const double b, const double c,
			const double alpha, const double beta,
			const double gamma, const int spgr,
			double rot[48][3][3], double trans[48][3],
			const int nops);

static double wdat[7][3][3] = {
  { {1.0,  0.0,  0.0},  {0.0,  1.0,  0.0},  {0.0,  0.0,  1.0} }, /* P */
  { {1.0,  0.0,  0.0},  {0.0,  0.5, -0.5},  {0.0,  0.5,  0.5} }, /* A */
  { {0.5,  0.0,  0.5},  {0.0,  1.0,  0.0}, {-0.5,  0.0,  0.5} }, /* B */
  { {0.5, -0.5,  0.0},  {0.5,  0.5,  0.0},  {0.0,  0.0,  1.0} }, /* C */
  { {0.0,  0.5,  0.5},  {0.5,  0.0,  0.5},  {0.5,  0.5,  0.0} }, /* F */
  { {-0.5,  0.5,  0.5},  {0.5, -0.5,  0.5},  {0.5,  0.5, -0.5} }, /* I */
  { {2.0, -1.0, -1.0},  {1.0,  1.0, -2.0},  {1.0,  1.0,  1.0} }  /* R */
};

// Declares for Keiths files
#define TSIZE 8
#define TITLE_SIZE 81
#define BUFSIZE 512
#define MAXATOMS 16384

static int fgetxyz(std::ifstream &f, int maxnatoms, float (*apos)[3],
		   char (*atype)[TSIZE], char *title);
static int fgetcssr(std::ifstream &f, int maxnatoms, float (*apos)[3],
		    char (*atype)[TSIZE], char *title,
		    float *cell, int *ispgr, char *spgr, bool &fractional);
static int fgetshelx(std::ifstream &f, int maxnatoms, float (*apos)[3],
		     char (*atype)[TSIZE], char *title, float *cell,
		     int &nops, double rotations[][3][3],
		     double translations[][3], int &centre, char spgr[]);

DLV::model *DLV::load_xyz_file::read(std::ifstream &input, string def_name,
				     char message[], const int_g mlen)
{
  real_g pos[MAXATOMS][3];
  char atomids[MAXATOMS][TSIZE], title[BUFSIZE];

  model *structure = 0;
  int_g n = fgetxyz(input, MAXATOMS, pos, atomids, title);
  if (n > 0) {
    string name = def_name;
    if (strlen(title) != 0)
      name = title;
    structure = create_atoms(name, 0);
    structure->set_group(1);
    for (int_g i = 0; i < n; i++) {
      coord_type coords[3];
      coords[0] = pos[i][0];
      coords[1] = pos[i][1];
      coords[2] = pos[i][2];
      int_g index;
      if (isalpha(atomids[i][0])) {
	atomids[i][1] = tolower(atomids[i][1]);
	structure->add_cartesian_atom(index, atomids[i], coords);
      } else {
	int_g atn = atoi(atomids[i]);
	structure->add_cartesian_atom(index, atn, coords);
      }
    }
  }
  return structure;
}

DLV::model *DLV::load_xtl_file::read(std::ifstream &input, string def_name,
				     char message[], const int_g mlen)
{
  real_l t, coord[1024][3];
  int_g cell = 0, sym = 0, isatom = 0, gp = 1, m, n = 0, frac = 1, flags[20];
  int_g charge[1024], atn[1024], dimen = 3;
  const int_g unset_ch = -1000;
  char str[128], label[128];

  real_l a, b, c, alpha, beta, gamma;
  for (int_g i = 0; i < 1024; i++) {
    atn[i] = 0;
    charge[i] = unset_ch;
  }
  string name = def_name;
  string space_group;
  int_g setting = 1;
  do {
    input >> str;
    if (strncmp(str, "title", 3) == 0 || strncmp(str, "TITLE", 3) == 0) {
      input.getline(str, 128);
      if (str[strlen(str) - 1] == '\n')
	str[strlen(str) - 1] = '\0';
      int_g i = 0;
      while (str[i] == ' ')
	i++;
      //input >> str;
      name = (str + i);
      snprintf(label, 128, "%s", str);
      isatom = 0;
      i = strlen(label) + 1;
      while (str[i] == ' ')
	i++;
      //input >> str;
      if (strncmp(str + i, "type ", 5) == 0 ||
	  strncmp(str + i, "TYPE ", 5) == 0) {
	//input >> str;
	i += 5;
	while (str[i] == ' ')
	  i++;
	name = (str + i);
      }
    } else if (strncmp(str, "dim", 3) == 0 || strncmp(str, "DIM", 3) == 0) {
      input >> dimen;
    } else if (strncmp(str, "cell", 3) == 0 ||
	       strncmp(str, "CELL", 3) == 0) {
      cell = 1;
      // input >> str; ?
      isatom = 0;
      input >> t;
      a = t;
      input >> t;
      b = t;
      input >> t;
      c = t;
      input >> t;
      alpha = t;
      input >> t;
      beta = t;
      input >> t;
      gamma = t;
      input.getline(str, 128);
    } else if (strncmp(str, "symmetry", 3) == 0 ||
	       strncmp(str, "SYMMETRY", 3) == 0) {
      input >> str;
      isatom = 0;
      sym = 1;
      // Todo - problem if dim != 3 and group != 1 ?
      //input >> str;
      setting = 1;
      if (strncmp(str, "number", 3) == 0 ||
	  strncmp(str, "NUMBER", 3) == 0) {
	input >> gp;
      } else if (strncmp(str, "label", 3) == 0 ||
		 strncmp(str, "LABEL", 3) == 0) {
	input >> str;
	expand_label(label, str, 0);
	space_group = label;
      } else if (strncmp(str, "qualifier", 4) == 0 ||
		 strncmp(str, "QUALIFIER", 4) == 0) {
	input >> str;
	if (strcmp(str, "origin_1") == 0 ||
	    strcmp(str, "ORIGIN_1") == 0)
	  setting = 1;
	else if (strcmp(str, "origin_2") == 0 ||
		 strcmp(str, "ORIGIN_2") == 0)
	  setting = 0;
	else {
	  strncpy(message, "Can't handle space group setting in XTL file",
		  mlen);
	  return 0;
	}
      } else if (strncmp(str, "matrix", 3) == 0 ||
		 strncmp(str, "MATRIX", 3) == 0 ||
		 strncmp(str, "operator", 2) == 0 ||
		 strncmp(str, "OPERATOR", 2) == 0) {
	strncpy(message, "Ignoring matrices/operators in XTL file", mlen);
	input.getline(str, 128);
	//return OM_STAT_ERROR;
      }
    } else if (strncmp(str, "atoms", 3) == 0 ||
	       strncmp(str, "ATOMS", 3) == 0) {
      if (!cell) {
	strncpy(message, "XTL files only work if atoms is the last keyword",
		mlen);
	return 0;
      }
      /* Read the order of data */
      m = 0;
      do {
	input >> str;
	for (int_g i = 0; i < (int)strlen(str); i++)
	  str[i] = tolower(str[i]);
	if (strncmp(str, "name", 3) == 0)
	  flags[m] = 1;
	else if (strcmp(str, "x") == 0)
	  flags[m] = 3;
	else if (strcmp(str, "y") == 0)
	  flags[m] = 4;
	else if (strcmp(str, "z") == 0)
	  flags[m] = 5;
	else if (strcmp(str, "carx") == 0) {
	  frac = 0;
	  flags[m] = 3;
	} else if (strcmp(str, "cary") == 0) {
	  frac = 0;
	  flags[m] = 4;
	} else if (strcmp(str, "carz") == 0) {
	  frac = 0;
	  flags[m] = 5;
	} else if (strncmp(str, "charge", 3) == 0)
	  flags[m] = 2;
	else
	  flags[m] = 0;
	m++;
      } while (input.peek() != '\n');
      isatom = 1;
    } else if (strncmp(str, "skip", 3) == 0 ||
	       strncmp(str, "SKIP", 3) == 0)
      input.getline(str, 128);
    else if (strncmp(str, "eof", 3) == 0 ||
	     strncmp(str, "EOF", 3) == 0)
      break;
    else if (isatom) {
      /* Read a line of atom data */
      if (n == 1024) {
	strncpy(message,
		"Buffer overflow reading XTL atoms - truncating list", mlen);
	break;
      }
      for (int_g i = 0; i < m; i++) {
	switch (flags[i]) {
	case 1:
	  if (isalpha(str[1])) {
	    str[2] = '\0';
	    str[1] = tolower(str[1]);
	  } else
	    str[1] = '\0';
	  atn[n] = DLV::atomic_prefs::lookup_symbol(str);
	  break;
	case 2:
	  charge[n] = atoi(str);
	  break;
	case 3:
	  coord[n][0] = atof(str);
	  break;
	case 4:
	  coord[n][1] = atof(str);
	  break;
	case 5:
	  coord[n][2] = atof(str);
	  break;
	default:
	  break;
	}
	if (i < m - 1)
	  input >> str;
      }
      input.getline(str, 128);
      n++;
    }
  } while (!input.eof());
  // Possible problems with mixed coords?, what if no group found for sym?
  model *structure = create_atoms(name, dimen);
  if (space_group.length() > 0) {
    int_g nsettings = 1;
    if (gp > 1)
      nsettings = get_number_of_origins(gp);
    else
      nsettings = get_number_of_origins(space_group);
    structure->set_group(space_group.c_str(), setting, nsettings);
  } else
    structure->set_group(gp);
  structure->set_lattice_parameters(a, b, c, alpha, beta, gamma);
  structure->complete();
  int_g index;
  for (int_g i = 0; i < n; i++) {
    if (frac)
      structure->add_fractional_atom(index, atn[i], coord[i]);
    else
      structure->add_cartesian_atom(index, atn[i], coord[i]);
    // Todo - should be based on index not type
    if (charge[i] != unset_ch)
      structure->set_atom_charge(DLV::atomic_prefs::get_symbol(atn[i]),
				 charge[i], message, mlen);
  }
  return structure;
}

DLV::model *DLV::load_cssr_file::read(std::ifstream &input, string def_name,
				      char message[], const int_g mlen)
{
  real_l va[3], vb[3], vc[3];
  real_g pos[MAXATOMS][3], cell[6];
  int_g ispgr;
  bool fractional, use_group;
  char atomids[MAXATOMS][TSIZE], title[BUFSIZE];
  char label[128], spgr[BUFSIZE];

  int_g n = fgetcssr(input, MAXATOMS, pos, atomids, title, cell, &ispgr, spgr,
		   fractional);
  if (n < 0) {
    strncpy(message, "Error reading file", mlen);
    return 0;
  }
  string name = title;
  model *structure = 0;
  if (cell[0] > 0.0)
    structure = create_atoms(name, 3);
  else
    structure = create_atoms(name, 0);
  if (ispgr > 0) {
    if (spgr[0] != '\0') {
      char x = spgr[strlen(spgr) - 1];
      if (x == 'H' || x == 'Z')
	spgr[strlen(spgr) - 1] = '\0';
      expand_label(label, spgr, 1);
      int_g nsettings = 1;
      if (ispgr > 1)
	nsettings = get_number_of_origins(ispgr);
      else
	nsettings = get_number_of_origins(label);
      structure->set_group(label, 1, nsettings);
    } else
      structure->set_group(ispgr);
    use_group = true;
  } else
    structure->set_group(1);
  input >> va[0];
  // Todo - warning or deal with case of group and lattice vectors.
  // Now I've included Keith's stuff does this still work?
  if (!input.eof() && (cell[0] == 0.0 || ispgr == 0)) {
    // Is this safe?
    // GULP version writes lattice vectors here, read first set
    // we better not have read a group though
    input >> va[1];
    input >> va[2];
    input >> vb[0];
    input >> vb[1];
    input >> vb[2];
    input >> vc[0];
    input >> vc[1];
    input >> vc[2];
    structure->set_primitive_lattice(va, vb, vc);
  } else
    structure->set_lattice_parameters(cell[0], cell[1], cell[2],
				      cell[3], cell[4], cell[5]);
  structure->complete();
  for (int_g i = 0; i < n; i++) {
    atomids[i][2] = '\0';
    if (!isalpha(atomids[i][1]))
      atomids[i][1] = '\0';
    else
      atomids[i][1] = tolower(atomids[i][1]);
    coord_type coords[3];
    coords[0] = pos[i][0];
    coords[1] = pos[i][1];
    coords[2] = pos[i][2];
    int_g index;
    if (fractional)
      structure->add_fractional_atom(index, atomids[i], coords);
    else
      structure->add_cartesian_atom(index, atomids[i], coords);
  }
  return structure;
}

DLV::model *DLV::load_shelx_file::read(std::ifstream &input, string def_name,
				       char message[], const int_g mlen)
{
  int_g i, j, k, n, nops, centre;
  real_g pos[MAXATOMS][3], cell[7];
  real_l rotations[48][3][3], translations[48][3];
  char atomids[MAXATOMS][TSIZE], title[BUFSIZE];
  char spgr[128];

  n = fgetshelx(input, MAXATOMS, pos, atomids, title, cell,
		nops, rotations, translations, centre, spgr);
  if (n < 0) {
    strncpy(message, "Errror reading file", mlen);
    return 0;
  }
  string name = title;
  // Copied from my CDS import routine.
  if (centre < 0) {
    centre = - centre;
    for (i = 0; i < nops; i++) {
      for (j = 0; j < 3; j++) {
	for (k = 0; k < 3; k++)
	  rotations[i + nops][j][k] = - rotations[i][j][k];
	// Is this correct?
	translations[i + nops][j] = translations[i][j];
      }
    }
    nops *= 2;
  }
  model *structure = 0;
  if (cell[0] < 0.0001) {
    structure = create_atoms(name, 0);
  } else {
    structure = create_atoms(name, 3);
    int_g latt = 0;
    switch (centre) {
    case 1:
      centre = 0;
      break;
    case 2:
      centre = 5;
      break;
    case 3:
      centre = 6;
      break;
    case 4:
      centre = 4;
      break;
    case 5:
      centre = 1;
      break;
    case 6:
      centre = 2;
      break;
    case 7:
      centre = 3;
      break;
    default:
      centre = 0;
      break;
    }
    // Todo - check
    structure->set_crystal03_lattice_type(latt + 1, centre + 1);
    structure->set_lattice_parameters(cell[0], cell[1], cell[2],
				      cell[3], cell[4], cell[5]);
    real_l ww[3][3], prim[3][3];
    int_g cc = centre;
    if (spgr[0] == 'R') {
      // gen_vectors needs R cell transformation to get lattice correct.
      if (cc == 0 && fabs(cell[3] - 90.0) > 0.001)
	cc = 6;
    }
    real_l dum = 1.0;
    if (cc == 6)
      dum = 3.0;
    for (i = 0; i < 3; i++) {
      for (j = 0; j < 3; j++)
	ww[i][j] = wdat[cc][i][j] / dum;
    }
    gen_vectors(ww, prim, cell[0], cell[1], cell[2], cell[3], cell[4],
		cell[5], 1, rotations, translations, nops);
  }
  structure->set_cartesian_sym_ops(rotations, translations, nops);
  structure->complete();
  for (int_g i = 0; i < n; i++) {
    coord_type coords[3];
    coords[0] = pos[i][0];
    coords[1] = pos[i][1];
    coords[2] = pos[i][2];
    int_g index;
    if (cell[0] > 0.0)
      structure->add_fractional_atom(index, atomids[i], coords);
    else
      structure->add_cartesian_atom(index, atomids[i], coords);
  }
  return structure;
}

static void expand_label(char label[], char string[], const int reverse)
{
  int i = 0, j = 0, l;

  l = strlen(string);
  while (i < l) {
    label[j] = string[i];
    if (isalpha(string[i])) {
      label[j + 1] = ' ';
      j += 2;
    } else if (isdigit(string[i])) {
      if (string[i + 1] == '/' || isdigit(string[i + 1]))
	j++;
      else if (reverse && string[i + 1] == '-') {
	label[j + 1] = string[i];
	label[j] = string[i + 1];
	label[j + 2] = ' ';
	i++;
	j += 3;
      } else {
	label[j + 1] = ' ';
	j += 2;
      }
    } else if (string[i] != ' ')
      j++;
    i++;
  }
  if (label[j - 1] == ' ')
    j--;
  label[j] = '\0';
}

static void matrix_init(double [3][3], const int);
static void matrix_copy(double [3][3], const double [3][3], const int);
static void matrix_transpose(double [3][3], const double [3][3], const int);
static void matrix_multnn(double [3][3], const double [3][3],
			  const double [3][3], const int, const int);
static double matrix_invert(const double mat[3][3], double inv[3][3],
			    const int dim, const char module[]);
static void matrix_multtn(double [3][3], const double [3][3],
			  const double [3][3], const int, const int);
static void vector_init(double [3], const int);
static void vector_copy(double x[3], const double y[3], const int n);
static void mvector_multn(double v[3], const double m[3][3],
			  const double x[3], const int n, const int sum);

static void gen_vectors(const double w[3][3], double prim[3][3],
			const double a, const double b, const double c,
			const double alpha, const double beta,
			const double gamma, const int spgr,
			double rot[48][3][3], double trans[48][3],
			const int nops)
{
  const double degrees_to_radians = DLV::pi / 180.0;
  double cell[3][3], lengths[3], winv[3][3], gg;
  int i, j;
  bool rhomb = false;

  lengths[0] = a;
  lengths[1] = b;
  lengths[2] = c;
  matrix_init(cell, 3);
  for (i = 0; i < 3; i++)
    cell[i][i] = 1.0;
  double calpha = std::cos(alpha * degrees_to_radians);
  if (spgr < 16) {
    gg = gamma * degrees_to_radians;
    cell[0][0] = std::sin(gg);
    cell[0][1] = std::cos(gg);
    cell[2][0] = (std::cos(beta * degrees_to_radians)
		  - cell[0][1] * calpha) / cell[0][0];
    cell[2][1] = calpha;
    cell[2][2] = std::sqrt(1.0 - cell[2][0] * cell[2][0] - calpha * calpha);
  } else if (spgr > 142 && spgr < 195) {
    if (spgr < 168 && std::abs(alpha - 90.0) > 0.001) {
      double gh[3][3], gr[3][3], s[3][3];
      (void) matrix_invert(w, winv, 3, "import CDS");
      /* calculate rhombohedral metric */
      gg = lengths[0] * lengths[0];
      calpha *= gg;
      for (i = 0; i < 3; i++) {
	for (j = 0; j < 3; j++)
	  gr[i][j] = calpha;
	gr[i][i] = gg;
      }
      /* Transform to hex cell */
      matrix_multtn(s, winv, gr, 3, 0);
      matrix_multnn(gh, s, winv, 3, 0);
      lengths[0] = sqrt(gh[0][0]);
      lengths[2] = sqrt(gh[2][2]);
      lengths[1] = lengths[0];
      rhomb = true;
    }
    cell[0][1] = -0.5;
    cell[0][0] = sqrt(0.75);
  }
  for (i = 0; i < 3; i++) {
    double p = lengths[i];
    for (j = 0; j < 3; j++)
      cell[i][j] *= p;
  }
  /* Transform xstal cell into primitive */
  matrix_multtn(prim, w, cell, 3, 0);

  double uxyz[3][3], uxyzi[3][3]; 
  // Convert operators to cartesian
  if (rhomb) // operators defined in primitive setting
    matrix_transpose(uxyz, prim, 3);
  else
    matrix_transpose(uxyz, cell, 3);
  (void) matrix_invert(uxyz, uxyzi, 3, "import CDS");

  double zr[3][3];
  double tras[3];
  for (i = 0; i < nops; i++) {
    matrix_copy(cell, (double (*)[3])(&rot[i][0][0]), 3);
    matrix_multnn(zr, cell, uxyzi, 3, 0);
    matrix_multnn((double (*)[3])(&rot[i][0][0]), uxyz, zr, 3, 0);
    vector_copy(tras, trans[i], 3);
    mvector_multn(trans[i], uxyz, tras, 3, 0);
  }
}

double matrix_invert(const double mat[3][3], double inv[3][3],
		     const int dim, const char module[])
{
  const double toll = 1e-15;
  double f1, f2, f3, det, deti = 1.0;

  if (dim < 1)
    return(0.0);
  else if (dim == 1) {
    inv[0][0] = 1.0 / mat[0][0];
    return mat[0][0];
  } else if (dim == 2) {
    det = mat[0][0] * mat[1][1] - mat[0][1] * mat[1][0];
    deti = 1.0 / (det);
    inv[0][0] = mat[1][1] * deti;
    inv[1][0] = -mat[1][0] * deti;
    inv[0][1] = -mat[0][1] * deti;
    inv[1][1] = mat[0][0] * deti;
    return(det);
  } else {
    f1 = mat[1][1] * mat[2][2] - mat[1][2] * mat[2][1];
    f2 = mat[1][2] * mat[2][0] - mat[1][0] * mat[2][2];
    f3 = mat[1][0] * mat[2][1] - mat[1][1] * mat[2][0];
    det = mat[0][0] * f1 + mat[0][1] * f2 + mat[0][2] * f3;
    //if (std::abs(det) < toll)
      //ERRverror(module, ERR_WARNING,
      //	"Invert - Determinat of direct lattice vectors is very small");
    //else {
      deti = 1.0 / det;
      inv[0][0] = f1 * deti;
      inv[1][0] = f2 * deti;
      inv[2][0] = f3 * deti;
      f1 = mat[0][0] * deti;
      f2 = mat[0][1] * deti;
      f3 = mat[0][2] * deti;
      inv[0][1] = f3 * mat[2][1] - f2 * mat[2][2];
      inv[1][1] = f1 * mat[2][2] - f3 * mat[2][0];
      inv[2][1] = f2 * mat[2][0] - f1 * mat[2][1];
      inv[0][2] = f2 * mat[1][2] - f3 * mat[1][1];
      inv[1][2] = f3 * mat[1][0] - f1 * mat[1][2];
      inv[2][2] = f1 * mat[1][1] - f2 * mat[1][0];
    //}
    return(det);
  }
}

void matrix_init(double a[3][3], const int n)
{
  int i, j;

  for (i = 0; i < n; i++)
    for (j = 0; j < n; j++)
      a[i][j] = 0.0;
}

void matrix_copy(double a[3][3], const double b[3][3], const int n)
/* a = b */
{
  int i, j;

  for (i = 0; i < n; i++)
    for (j = 0; j < n; j++)
      a[i][j] = b[i][j];
}

void matrix_transpose(double a[3][3], const double b[3][3], const int n)
/* a = bT */
{
  int i, j;

  for (i = 0; i < n; i++)
    for (j = 0; j < n; j++)
      a[i][j] = b[j][i];
}

void matrix_multnn(double c[3][3], const double a[3][3],
		   const double b[3][3], const int n, const int sum)
/* if sum c = c + a * b  else c = a * b */
{
  int i, j, k;

  if (!sum)
    matrix_init(c, n);
  for (i = 0; i < n; i++)
    for (j = 0; j < n; j++)
      for (k = 0; k < n; k++)
	c[i][j] += a[i][k] * b[k][j];
}

void matrix_multtn(double c[3][3], const double a[3][3],
		   const double b[3][3], const int n, const int sum)
/* if sum c = c + a * b else c = a * b, a stored in transposed form */
{
  int i, j, k;

  if (!sum)
    matrix_init(c, n);
  for (i = 0; i < n; i++)
    for (j = 0; j < n; j++)
      for (k = 0; k < n; k++)
	c[i][j] += a[k][i] * b[k][j];
}

void vector_init(double v[3], const int n)
{
  int i;

  for (i = 0; i < n; i++)
    v[i] = 0.0;
}

void vector_copy(double x[3], const double y[3], const int n)
{
  int i;

  for (i = 0; i < n; i++)
    x[i] = y[i];
}

void mvector_multn(double v[3], const double m[3][3], const double x[3],
		   const int n, const int sum)
/* if sum v = v + m * x else v = m * x */
{
  int i, j;

  if (!sum)
    vector_init(v, n);
  for (i = 0; i < n; i++)
    for (j = 0; j < n; j++)
      v[i] += m[i][j] * x[j];
}

// From Keith's readers.c file.

struct symm_t {
  struct symm_t *next;
  float trans_matrix[3][4];
};

static int sscanff(char *s, char *fmt, ...);

static int strNcasecmp(const char *a, const char *b, const int n);
static char *strcasestr(char *cs, char *ct);
static void trail(char *s, size_t len);
static int fgetschk(char *buf, size_t size, std::ifstream &f, char *label,
		    char *errmsg);
static void space_minus(char *str, int n);
static int tokenize(char *buf, char **linev);
static int transformation_matrix(char *buf, double rotations[][3][3],
				 double translations[][3], const int nops);

static int strNcasecmp(const char *a, const char *b, const int n)
{
  int i = 0;
  while (i < n && a[i] != '\0' && b[i] != '\0' &&
	 (tolower(a[i]) == tolower(b[i])))
    i++;
  if (i == n)
    return 0;
  else
    return (tolower(a[i]) - tolower(b[i]));
}

static char *strcasestr(char *cs, char *ct)
{
   int  i,
        n  = strlen(ct),
        sl = strlen(cs)-n;


   for(i = 0; i <= sl; i++)
      if( !strNcasecmp(cs+i, ct, n) )
	 return (char*)cs+i;
   return 0;
}
/*
 * Remove trailing blanks and add a null character to a string.
 */
static void trail(char *s, size_t len)
{
   char *p = s+len;

   *p = '\0';
   while (p > s && !isalnum(*p))
      *p-- = '\0';
}

/******************************************************************************
 * fgetshelx().  The main SHELXL93 file reader.				      *
 ******************************************************************************/
static int fgetshelx(std::ifstream &f, int maxnatoms, float (*apos)[3],
		     char (*atype)[TSIZE], char *title, float *cell,
		     int &nops, double rotations[][3][3],
		     double translations[][3], int &centre, char spgr[])
{
   char buf[BUFSIZE], linebuf[BUFSIZE], *bufend, codebuf[8];
   char (*species_ct)[3] = 0;
   int linec, linep; char *linev[32];
   int natoms;		 		/* number of atoms */
   int ispecies, nspecies= 0;
   int i; //, label=0;
   int csymm, ilatt; //, il;

   nops = 1;
   rotations[0][0][0] = 1.0;
   rotations[0][0][1] = 0.0;
   rotations[0][0][2] = 0.0;
   rotations[0][1][0] = 0.0;
   rotations[0][1][1] = 1.0;
   rotations[0][1][2] = 0.0;
   rotations[0][2][0] = 0.0;
   rotations[0][2][1] = 0.0;
   rotations[0][2][2] = 1.0;
   translations[0][0] = 0.0;
   translations[0][1] = 0.0;
   translations[0][2] = 0.0;
   natoms=0;
   title[0] = 0;
   while(1)
   {
      /*
       * Read line into linebuf, joining lines upon continuation char "="
       */
      bufend = linebuf;
      do
      {
	 if( !f.getline(bufend, sizeof linebuf-(bufend-linebuf)) )
	    strcpy(linebuf,"END"); /* Force end on EOF	*/

	 bufend = strchr(bufend,'\n');
	 if( bufend == NULL)
	    bufend = linebuf+strlen(linebuf);
	 *bufend = '\0';
      } while( *--bufend == '=' );
      space_minus(linebuf, sizeof linebuf);
      /*
       * Scrub comments
       */
      strcpy(buf, linebuf);
      if( (bufend = strchr(buf,'!')) != 0)
	 *bufend = '\0';
      linec = tokenize(buf, linev);
      if( linec < 1 )
	 continue;
      linep = 0;
      for (i = 0; (i < (int)strlen(linev[0])) && (i < 4); i++)
	codebuf[i] = tolower(linev[0][i]);
      codebuf[i] = '\0';
      linec--;
      linep++;
      if (codebuf[0] == '\0')
	/* Blank line				*/;
      else if (strncmp(codebuf, "titl", 4) == 0) {
  	/* Title 				*/
	strcpy(title,linev[1]);
	if (linev[linec][0] == 'P' || linev[linec][0] == 'A' ||
	    linev[linec][0] == 'B' || linev[linec][0] == 'C' ||
	    linev[linec][0] == 'I' || linev[linec][0] == 'F')
	  strcpy(spgr, linev[linec]);
	else {
	  strcpy(spgr, linev[linec - 1]);
	  strcat(spgr, linev[linec]);
	}
      } else if (strncmp(codebuf, "cell", 4) == 0) {
	/* Cell - Specify unit cell		*/
	 i = 0;
	 linep++;
	 if( linec == 7 )
	 {
	    while(linep <= linec)
	       cell[i++] = atof(linev[linep++]);
	 }
	 else
	 {
	   //ERRverror("fgetshelx", ERR_ERROR, 
	   //      " Error in CELL card -- should have 7 parameters");
	    goto error_return;
	 }

	 if( ! (cell[0] > 0 && cell[1] > 0 && cell[2] > 0 &&
		cell[3] > 0 && cell[3] < 180.0 &&
		cell[4] > 0 && cell[4] < 180.0 &&
		cell[5] > 0 && cell[5] < 180.0))
	 {
	   //ERRverror("fgetshelx", ERR_ERROR,
	   //	      " Invalid unit cell %f %f %f %f %f %f",
	   //      cell[0], cell[1], cell[2], cell[3], cell[4], cell[5]);
	    goto error_return;
	 }
      } else if (strncmp(codebuf, "latt", 4) == 0) {
	/* Don't (BGS) Generate lattice symmetry ops        */
	 csymm = atoi(linev[1]);
	 ilatt = abs(csymm);
	 if( ilatt < 1 || ilatt > 7)
	 {
	   //ERRverror("fgetshlx", ERR_ERROR,
	   //      "Invalid LATT card \"%s\"", buf);
	    goto error_return;
	 }
	 centre = csymm;
      } else if (strncmp(codebuf, "sfac", 4) == 0) {
	/* Create Atom name list              */
	 nspecies = linec;
	 species_ct = new char[nspecies+1][3];
	 for(i=1; i<=nspecies; i++)
	    strncpy(species_ct[i],linev[i],3);
      } else if (strncmp(codebuf, "symm", 4) == 0) {
	/* Parse and store symmetry ops       */
	 if( transformation_matrix(linebuf+4, rotations, translations, nops))
	 {
	   nops++;
	 }
	 else
	 {
	   //ERRverror("fgetshelx", ERR_ERROR,
	   //      "Syntax Error on SYMM card\n%s",linebuf);
	    goto error_return;
	 }
      } else if (strncmp(codebuf, "end", 4) == 0) {
	/* End - Add unit cell, etc		  */
	 if( natoms == 0  )
	    goto error_return;

	 goto normal_return;	/* Finished reading file   */
      } else {
	bool found = false;
	if (strncmp(codebuf, "acta", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "afix", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "anis", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "basf", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "bind", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "bloc", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "bond", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "bump", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "chiv", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "cgls", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "conf", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "conn", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "damp", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "defs", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "dfix", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "disp", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "delu", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "eadp", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "exti", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "eqiv", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "exyz", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "fend", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "flat", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "fmap", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "frag", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "free", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "fvar", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "grid", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "hfix", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "hklf", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "isor", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "l.s.", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "laue", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "list", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "merg", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "mole", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "more", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "move", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "mpla", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "omit", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "part", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "plan", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "rem", 3) == 0)
	  found = true;
	else if (strncmp(codebuf, "resi", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "rtab", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "sadi", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "same", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "shel", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "simu", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "size", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "slim", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "spec", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "sump", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "swat", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "time", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "twin", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "unit", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "wght", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "wpdb", 4) == 0)
	  found = true;
	else if (strncmp(codebuf, "zerr", 4) == 0)
	  found = true;
	else {
	  /* Atom - add atom to list		*/
	  linep--; linec++;
	  if( natoms >= maxnatoms )
	    {
	      //ERRverror("fgetshelx", ERR_ERROR,
	      //		"Too many atoms! (%d)", natoms);
	      goto normal_return;
	    }
	  if( linec < 5 )
	    {
	      //ERRverror("fgetshelx", ERR_ERROR,
	      //	"Insufficient items on ATOM card - %s", buf);
	      goto error_return;
	    }
	  ispecies = atoi(linev[linep+1]);
	  if( ispecies < 1 || ispecies > nspecies )
	    {
	      //ERRverror("fgetshelx", ERR_ERROR,
	      //	"ATOM not on SFAC card \"%s\" (%d of %d)",
	      //	buf, ispecies, nspecies);
	      goto error_return;
	    }

	  if( strcasestr(linev[linep], species_ct[ispecies]) )
	    {
	      strncpy(atype[natoms],linev[linep],TSIZE);
	      atype[natoms][TSIZE-1] = '\0';
	    }
	  else
	    {
	      //strcpy(atype[natoms],species_ct[ispecies]);
	      //strcat(atype[natoms],":");
	      atype[natoms][0] = '\0';
	      strncat(atype[natoms],linev[linep],TSIZE);
	      atype[natoms][TSIZE-1] = '\0';
	    }
	  if (isalpha(atype[natoms][1])) {
	    atype[natoms][1] = tolower(atype[natoms][1]);
	    atype[natoms][2] = '\0';
	  } else {
	    atype[natoms][1] = '\0';
	  }
	  linep += 2;
	  for(i = 0; i < 3; i++)
	    apos[natoms][i] = atof(linev[linep++]);

	  natoms++;
	}
      }
   }
 normal_return:
   return natoms;		/* Should normally return from 'end' label   */
 unsupported:
   //ERRverror("fgetshelx", ERR_ERROR, "Unimplemented label %s", linev[0]);
   goto error_return;
 error_return:
   return 0;
}

static int fgetschk(char *buf, size_t size, std::ifstream &f, char *label,
		    char *errmsg)
{
   if( !f.getline(buf, size) )
   {
     //ERRverror(label, ERR_ERROR, errmsg);
      return -1;
   }
   return 0;
}
/******************************************************************************
 * fgetcssr().  The CSSR and XR file reader.				      *
 ******************************************************************************/

static int fgetcssr(std::ifstream &f, int maxnatoms, float (*apos)[3],
		    char (*atype)[TSIZE], char *title,
		    float *cell, int *ispgr, char *spgr, bool &fractional)
{
   /*
    * READ a CSSR input file
    */
   char linebuf[BUFSIZE], buff[128];
   int natoms;		 		/* number of atoms */
   int iat,serno,orthflag, nocell = 0;
   int conn[8];
   float chg;
   //float h[3][3], hinv[3][3];

   fractional = true;
   cell[0] = cell[1] = cell[2] = 0.0;
   cell[3] = cell[4] = cell[5] = 90.0;
   *ispgr = 0;
   spgr[0] = '\0';
   natoms=0;
   title[0] = 0;

   f.getline(buff, 128);
   if (sscanff(buff,"%*38c%8f%8f%8f", &cell[0],&cell[1],&cell[2]) < 3)
      nocell++;

   f.getline(buff, 128);
   if (sscanff(buff,"%*21c%8f%8f%8f    SPGR%*2[ =]%3d %11c",
	      &cell[3],&cell[4],&cell[5],ispgr,spgr) < 5)
      nocell++;
   trail(spgr,11);

   f.getline(buff, 128);
   if (sscanff(buff,"%4d%4d%*1c%60s",&natoms,&orthflag, title) < 2)
   {
     //ERRverror("fgetcssr", ERR_ERROR, "EOF or unexpected format on line 3");
      return -1;
   }
   trail(title,60);

   if( nocell && ! orthflag )
   {
     //ERRverror("fgetcssr", ERR_ERROR,
     //       "No unit cell parameters supplied but orthflag = 0");
     return -1;
   }


   f.getline(buff, 128);
   if(sscanff(buff,"%s",linebuf) == -1)
   {
     //ERRverror("fgetcssr", ERR_ERROR, "Unexpected end of input on line 4");
      return -1;
   }
   if( natoms > maxnatoms)
   {
     //ERRverror("fgetcssr", ERR_WARNING,
     //       "Too many atoms: only %d will be read", maxnatoms);
     natoms = maxnatoms;
   }

   for( iat=0; iat < natoms; iat++)
   {
      /* Parse columns 5-11 as label to cope with dodgy BABEL-generated cssr */
     f.getline(buff, 128);
      if(sscanff(buff,"%4d%7s%9f %9f %9f %4d%4d%4d%4d%4d%4d%4d%4d %7f",
		 &serno, &atype[iat], &apos[iat][0], &apos[iat][1],
		 &apos[iat][2],&conn[0],&conn[0],&conn[0],&conn[0],&conn[0],
		 &conn[0],&conn[0],&conn[0],&chg) < 5)
      {
	//ERRverror("fgetcssr", ERR_ERROR,
	//  "EOF or unexpected format in atom definitions line %d",iat);
	 return -1;
      }
   }

   if (orthflag)
     fractional = false;

   return natoms;
}
/******************************************************************************
 * fgetxyz().  The XYZ file reader.				      *
 ******************************************************************************/

static int fgetxyz(std::ifstream &f, int maxnatoms, float (*apos)[3],
		   char (*atype)[TSIZE], char *title)
{
   /*
    * READ a CSSR input file
    */
   char linebuf[BUFSIZE];
   int iat;
   int natoms;		 		/* number of atoms */
   char *p;

   natoms=0;
   title[0] = 0;
   //*ispgr = 1;
   //cell[0] = cell[1] = cell[2] = 1.0;
   //cell[3] = cell[4] = cell[5] = 90.0;
   //strcpy(spgr,"P 1");

   if (fgetschk(linebuf, sizeof linebuf, f,
		"fgetxyz", "Unexpected end of input on line 1"))
      return -1;
   if (sscanf(linebuf,"%d",&natoms) < 1)
   {
     //ERRverror("fgetxyz", ERR_ERROR, "Unexpected format on line 1");
      return -1;
   }

   if( natoms > maxnatoms)
   {
     //ERRverror("fgetxyz", ERR_WARNING, "Too many atoms: only %d will be read",
     //	maxnatoms);
      natoms = maxnatoms;
   }


   if (fgetschk(title, sizeof linebuf, f,
		"fgetxyz", "Unexpected end of input on line 1"))
      return -1;

   for( iat=0; iat < natoms; iat++)
   {
      if( fgetschk(linebuf, sizeof linebuf, f,
		   "fgetxyz", "Unexpected end of input"))
	 return -1;

      if(sscanf(linebuf,"%s%f%f%f",
		atype[iat], &apos[iat][0], &apos[iat][1], &apos[iat][2]) < 4)
      {
	//ERRverror("fgetxyz", ERR_ERROR,
	//   "Unexpected format in atom definitions");
	 return -1;
      }

      p = &atype[iat][TSIZE-1];
      do {
	 *p--='\0';
      } while(p > atype[iat] && ! isalnum(*p));

   }
   return natoms;
}

// Keith's scanff file

#include <stdarg.h>

static int vscanff(char *inbuf, char *format, va_list ap);

static int vscanff(char *inbuf, char *format, va_list ap)
{
   int fwidth, pos, charsin, fpos, error, modflag, bufsave;
   int nchars, nitems, nitot;
   // bit dangerous - don't dynamically allocate BGS
   char fmt[1024], mybuf[1024];
   char    *buf, *bufend;
   char    ch;
   void	   *argp; //, *dummy;

   buf = mybuf;
   strcpy(buf, inbuf);

    pos = charsin = nitot = nchars = nitems = error = 0;
    bufsave = -1;

    while (format[pos] != 0 && error == 0)
    {
        modflag = 0;
        if (format[pos] != '%' || format[pos+1] == '%')
        {
	   if (format[pos] == '%')
	      pos++;
	   if (format[pos++] != *(buf++))
	       break;
	   charsin++;
	}
        else
        {
	    fwidth = 0;
            fmt[0] = '%';
            pos++;
            fpos = 1;
	    if(format[pos] == '*'  )
	    {
	       modflag = format[pos];
	       fmt[fpos++] = format[pos++];
	    }
	    if( isdigit(format[pos]) )
	    {
	       fwidth = strtol(&format[pos], NULL, 10);
	       fmt[fpos++] = format[pos++];
	    }
            while (format[pos] != 0 && isdigit(format[pos]) )
                fmt[fpos++] = format[pos++];
	    if( format[pos] == 'l' || format[pos] == 'L' || format[pos] == 'h')
	    {
	       if( modflag != '*' )
		  modflag = format[pos];
	       fmt[fpos++] = format[pos++];
	    }
            if (format[pos] == 0)
            {
            /* error in format string */
                error++;
            }
            ch = fmt[fpos++] = format[pos++];

	    if ( ch == '[' )
	    {
	       if( format[pos] == '^')
		  fmt[fpos++] = format[pos++];
	       if( format[pos] != 0  )
		  fmt[fpos++] = format[pos++];
	       while (format[pos] != 0 && format[pos] != ']')
		  fmt[fpos++] = format[pos++];
	       if( format[pos] == ']' )
		  fmt[fpos++] = format[pos++];
	    }
	    fmt[fpos] = 0;
	    strcat(fmt,"%n");
        /* Now fmt contains the format for this part of the output , fwidth
	   contains the field width and ch contains the conversion code. */
	    if( fwidth > 0 && (int)strlen(buf) > fwidth)
	    {
	       bufend = buf+fwidth;
	       bufsave = bufend[0];
	       bufend[0] = 0;
	    } else {
	       bufsave = -1;
	    }
	    /* This default applies only if the "all-blank" code is active*/
	    nitems = 1;
	    nchars = 0;
	    if( modflag == '*')
	       ch = modflag;
            switch (ch)
            {
              case 'n':
		if( modflag == 'l')
		   *va_arg (ap, long *) = charsin;
		else if( modflag == 'h')
		   *va_arg (ap, short *) = charsin;
		else
		   *va_arg (ap, int *) = charsin;
                break;
              case 'c':
              case 's':
              case '[':
		 argp = (void*)va_arg (ap, char *);
		 *(char*)argp = 0;
		 nitems = sscanf (buf, fmt, (char *)argp, &nchars);
                break;
              case 'p':
		 argp = (void*)va_arg (ap, void **);
		  if( fwidth > 0 && (int)strspn(buf," \t\n") == fwidth)
		     *(void**)argp = 0;
		  else
		     nitems = sscanf (buf, fmt, (void **)argp, &nchars);
                break;
              case 'd':
              case 'i':
              case 'o':
              case 'u':
              case 'x':
              case 'X':
		if( modflag == 'l')
		{
		  argp = (void*)va_arg (ap, long *);
		  if( fwidth > 0 && (int)strspn(buf," \t\n") == fwidth)
		     *(long*)argp = 0;
		  else
		     nitems = sscanf (buf, fmt, (long *)argp, &nchars);
		}
		else if( modflag == 'h')
		{
		  argp = (void*)va_arg (ap, short *);
		  if( fwidth > 0 && (int)strspn(buf," \t\n") == fwidth)
		     *(short*)argp = 0;
		  else
		     nitems = sscanf (buf, fmt, (short *)argp, &nchars);
		}
		else
		{
		  argp = (void*)va_arg (ap, int *);
		  if( fwidth > 0 && (int)strspn(buf," \t\n") == fwidth)
		     *(int*)argp = 0;
		  else
		     nitems = sscanf (buf, fmt, (int *)argp, &nchars);
		}
                break;
              case 'e':
              case 'E':
              case 'f':
              case 'g':
              case 'G':
		if( modflag == 'L')
		{
		  argp = (void*)va_arg (ap, long double *);
		  if( fwidth > 0 && (int)strspn(buf," \t\n") == fwidth)
		     *(long double*)argp = 0.0;
		  else
		     nitems = sscanf (buf, fmt, (long double *)argp, &nchars);
		}
		else if( modflag == 'l')
		{
		  argp = (void*)va_arg (ap, double *);
		  if( fwidth > 0 && (int)strspn(buf," \t\n") == fwidth)
		     *(double*)argp = 0.0;
		  else
		     nitems = sscanf (buf, fmt, (double *)argp, &nchars);
		}
  		else
		{
		  argp = (void*)va_arg (ap, float *);
		  if( fwidth > 0 && (int)strspn(buf," \t\n") == fwidth)
		     *(float*)argp = 0.0;
		  else
		     nitems = sscanf (buf, fmt, (float *)argp, &nchars);
		}
                break;
	      case '*':
		 nitems = sscanf (buf, fmt, &nchars);
		 break;
              default:
                error++;
            }
	    if( nitems >= 0 )
	       nitot += nitems;
	    charsin += nchars;
	    if( bufsave > 0 )
	    {
	       buf = bufend;
	       buf[0] = bufsave;
	    }
	    else
	    {
	       while (*buf++ && nchars-- > 0)
		  ;
	    }
        }
    }

    //delete [] mybuf;
    //delete [] fmt;
    if (error)
        return (-1);
    return (nitot);
}

static int sscanff(char *buf, char *format, ...)
{
   va_list ap;

   va_start(ap, format);
   return vscanff(buf, format, ap);
   va_end(ap);
}

static int fscanff(std::ifstream &fp, char *format, ...)
{
   va_list ap;
   char buf[BUFSIZE];
   int i;

   if ( !fp.getline(buf, BUFSIZE) )
      return -1;
   i = strlen(buf)-1;
   if(buf[i] == '\n')
      buf[i] = '\0';
   for(; i < BUFSIZE; i++)
      buf[i] = '\0';

   va_start(ap, format);
   return vscanff(buf, format, ap);
   va_end(ap);
}

/******************************************************************************
 * space_minus().  Insert spaces before every '-' in s string except exponent.*
 ******************************************************************************/
static void space_minus(char *str, int n)
{
   char *in = strdup(str), *savein = in, *strbegin = str;
   
   do
   {
      if( *in == '-' &&(
        (in > savein && in[-1] != 'E' && in[-1] != 'e') || in == savein))
	 *str++ = ' ';
      *str++ = *in++;
   } while( *(in-1) && str-strbegin < n-1);
   free(savein);
}

static int tokenize(char *buf, char **linev)
{
   char *p;
   char *sep = " +*/=:;,'\"<>()[]!?@\\^_`{}~";
   int linec = 0;
   
   while( (p = strtok(buf,sep) ) != NULL)
   {
      *(linev++) = p;
      linec++;
      buf = NULL;
   }
   
   return linec;
}

// Keith's parse_trans.c
/*****************************************************************************
 * Parser for transformation expressions a la International tables{ eg       *
 * "x-y,y,3*Z+1/3".							     *
 * BNF form is								     *
 * [trans] :== [const],...,[const]|[sexpr],[sexpr],[sexpr] ;		     *
 * [sexpr] :== [aop][expr]|[expr] ;					     *
 * [expr]  :== [el][aop][expr]|[el] ;					     *
 * [el]    :== [cexpr]*[var]|[cexpr]|[var] ;				     *
 * [aop]   :== +|- ;							     *
 * [cexpr] :== [const]/[const]|[const] ;				     *
 * [var]   :== x|y|z ;							     *
 *  where [const] is built in and detected by lexer.			     *
 *****************************************************************************/

#define MAXCONST 127

static float constant[MAXCONST];

#define LITERAL 128
#define ISEXP	LITERAL+4
#define IEXPR	LITERAL+7
#define IEL  	LITERAL+10
#define IAOP 	LITERAL+14
#define ICEXP	LITERAL+17
#define IVAR 	LITERAL+20
#define ICONST	LITERAL

#define JTRANS1 1
#define JTRANS2 25
#define JSEXP1	31
#define JSEXP2	34
#define JEXP1	36
#define JEXP2	40
#define JEL1	42
#define JEL2	46
#define JEL3	48
#define JAOP1	50
#define JAOP2	52
#define JCEXP1	54
#define JCEXP2	58
#define JVAR1	60
#define JVAR2	62
#define JVAR3	64

/*
 * Parse tables.  "ps" contains match text and "pstab" master alternatives.
 */

static int ps[] =       {0,ICONST,',',ICONST,',',ICONST,',',ICONST,',',
			ICONST,',',ICONST,',',ICONST,',',ICONST,',',
			ICONST,',',ICONST,',',ICONST,',',ICONST,0,
			ISEXP,',',ISEXP,',',ISEXP,0,
			IAOP,IEXPR,0,IEXPR,0,
			IEL,IAOP,IEXPR,0,IEL,0,
			ICEXP,'*',IVAR,0,ICEXP,0,IVAR,0,
			'+',0,'-',0,
			ICONST,'/',ICONST,0,ICONST,0,
			'x',0,'y',0,'z',0};

int pstab[] =	         {0,JTRANS1,JTRANS2,0,
			 JSEXP1,JSEXP2,0,
			 JEXP1,JEXP2,0,
			 JEL1,JEL2,JEL3,0,
			 JAOP1,JAOP2,0,
			 JCEXP1,JCEXP2,0,
			 JVAR1,JVAR2,JVAR3,0};

#define MATCH   1
#define NOMATCH 0

#define STBF 12

static int parse_trans(char **str, int *pstp, int **parse_rec);
static char *process_line(char *line);
static int make_trans_matrix(double rotations[][3][3],
			     double translations[][3],
			     const int nops, int *parse_rec);

/*
 * The parser
 */
static int parse_trans(char **str, int *pstp, int **parse_rec)
{
   char *strp;
   int  *prec, *pst;

   prec = *parse_rec;
   for(; *pstp; pstp++)			/* Loop over possible alternatives */
   {
      strp = *str;
      *parse_rec=prec;
      *prec = pstp-pstab;
      *(prec+1) = 0;
      for(pst=ps+(*pstp); *pst; pst++)  /* Loop over elements of phrase */
      {
	 if( *pst < LITERAL)
	 {
	    if( *pst != *strp )
	       break;
	    strp++;
	 }
	 else if( *pst == LITERAL )
	 {
	    if( ! (*strp & LITERAL) )
	       break;
	    strp++;
	 }
	 else
	 {
	    (*parse_rec)++;
	    if( ! parse_trans(&strp, pstab+(*pst-LITERAL), parse_rec ))
	       break;
	 }
      }
      if( ! *pst ) 		/* pst==0 if matched whole phrase     */
      {
	 *str = strp;		/* Mark end of text scanned so far    */
	 return MATCH;
      }
   }
   *str = strp;		/* Mark end of text scanned so far    */
   return NOMATCH;
}

/*
 * Lexical analyzer
 */

static char *process_line(char *line)
{
   int n = strlen(line);
   char *outline = new char[n+1];
   char *ip = line, *op = outline;
   int iconst=0;

   if( !outline )
   {
     //ERRverror("fgetshelx", ERR_ERROR,
     //	"Process_line: Failed to allocate %d bytes of memory\n", n+1);
      return 0;
   }
   
   while( ip < line+n)
   {
      while(*ip == ' ' || *ip == '\t' )
	 ip++;
      
      if(isdigit(*ip) || (*ip == '.' && isdigit(*(ip+1))))
      {
	 constant[iconst++] = strtod(ip, &ip);
	 *op++ = LITERAL;
      }
      else
      {
	 *op = *ip;
	 if( isupper(*op) )
	    *op = tolower(*op);
	 op++; ip++;
      }
   }
   *op = 0;
   return outline;
}

/****************************************************************************
 * make_trans_matrix().  Rease parse record and construct transf. matrix.   *
 ****************************************************************************/
static int make_trans_matrix(double rotations[][3][3],
			     double translations[][3],
			     const int nops, int *parse_rec)
{
   int	coeff;
   int		irow = -1, i, j, aflg, iconst = 0;

   for( i=0; i<3; i++) {
     for (j = 0; j < 3; j++)
       rotations[nops][i][j] = 0.0;
     translations[nops][i] = 0.0;
   }

   while( *parse_rec )
   {
      switch( pstab[*parse_rec++] )
      {
       case JTRANS1:
	 for(i=0; i<3; i++) {
	    for(j=0; j<3; j++)
	       rotations[nops][i][j] = STBF*constant[iconst++]+0.5;
	    translations[nops][i] = STBF*constant[iconst++]+0.5;
	 }
	 break;
       case JTRANS2:
	 break;
       case JSEXP1:
	 irow++;
	 break;
       case JSEXP2:
	 coeff = STBF;
	 irow++;
	 break;
       case JEXP1:
       case JEXP2:
	 break;
       case JEL1:
       case JEL3:
	 aflg = 0;
	 break;
       case JEL2:
	 aflg = 1;
	 break;
       case JAOP1:
	 coeff = STBF;
	 break;
       case JAOP2:
	 coeff = -STBF;
	 break;
       case JCEXP1:
	 coeff = (int)(coeff * constant[iconst++]);
	 coeff = (int)(coeff / constant[iconst++]);
	 // BGS change
	 if( aflg )
	    translations[nops][irow] += ((double)coeff) / ((double)STBF);
	 break;
       case JCEXP2:
	 coeff *= (int)(STBF*constant[iconst++]+0.5);
	 coeff /= STBF;
	 if( aflg )
	    translations[nops][irow] += coeff;
	 break;
       case JVAR1:
	 rotations[nops][irow][0] += coeff/STBF;
	 break;
       case JVAR2:
	 rotations[nops][irow][1] += coeff/STBF;;
	 break;
       case JVAR3:
	 rotations[nops][irow][2] += coeff/STBF;;
	 break;
       default:
	 //ERRverror("fgetshelx", ERR_ERROR,
	 //   "Unknown entry in parse record, %d\n", pstab[*--parse_rec]);
	 return 0;
      }
   }
   return 1;
}

/****************************************************************************
 * transformation_matrix()   Driver function for above			    *
 ****************************************************************************/
static int transformation_matrix(char *buf, double rotations[][3][3],
				 double translations[][3], const int nops)
{
   int parse_rec[80],*prp;
   char	*tfbuf, *tfp;

   tfbuf = tfp = process_line(buf);
   if( tfbuf == 0 )
      return NOMATCH;
   int ok = MATCH;
   prp = parse_rec;
   if( parse_trans(&tfp,pstab+1,&prp) && 
      (*tfp == 0 || *tfp == ':' || *tfp == ';' || *tfp == ',') )
   {
      if( !make_trans_matrix(rotations, translations, nops, parse_rec) )
	 ok = NOMATCH;
   }
   else
      ok = NOMATCH;
   delete tfbuf;
   return ok;
}


#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::load_xyz_file *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::load_xyz_file(0, "recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::load_xtl_file *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::load_xtl_file(0, "recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::load_cssr_file *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::load_cssr_file(0, "recover");
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::load_shelx_file *t,
				    const unsigned int file_version)
    {
      ::new(t)DLV::load_shelx_file(0, "recover");
    }

  }
}

template <class Archive>
void DLV::load_xyz_file::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<load_atom_model_op>(*this);
}

template <class Archive>
void DLV::load_xtl_file::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<load_atom_model_op>(*this);
}

template <class Archive>
void DLV::load_cssr_file::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<load_atom_model_op>(*this);
}

template <class Archive>
void DLV::load_shelx_file::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<load_atom_model_op>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::load_xyz_file)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::load_xtl_file)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::load_cssr_file)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::load_shelx_file)

template void DLV::load_xyz_file::serialize<boost::archive::text_oarchive>(boost::archive::text_oarchive &ar, const unsigned int version);
template void DLV::load_xyz_file::serialize<boost::archive::text_iarchive>(boost::archive::text_iarchive &ar, const unsigned int version);
template void DLV::load_xtl_file::serialize<boost::archive::text_oarchive>(boost::archive::text_oarchive &ar, const unsigned int version);
template void DLV::load_xtl_file::serialize<boost::archive::text_iarchive>(boost::archive::text_iarchive &ar, const unsigned int version);
template void DLV::load_cssr_file::serialize<boost::archive::text_oarchive>(boost::archive::text_oarchive &ar, const unsigned int version);
template void DLV::load_cssr_file::serialize<boost::archive::text_iarchive>(boost::archive::text_iarchive &ar, const unsigned int version);
template void DLV::load_shelx_file::serialize<boost::archive::text_oarchive>(boost::archive::text_oarchive &ar, const unsigned int version);
template void DLV::load_shelx_file::serialize<boost::archive::text_iarchive>(boost::archive::text_iarchive &ar, const unsigned int version);

#endif // DLV_USES_SERIALIZE
