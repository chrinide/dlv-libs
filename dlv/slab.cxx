
#include <list>
#include <map>
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/toolkit_obj.hxx"
#  include "../graphics/render_base.hxx"
#  include "../graphics/model_render.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "utils.hxx"
#include "constants.hxx"
#include "math_fns.hxx"
#include "symmetry.hxx"
#include "atom_model.hxx"
#include "atom_pairs.hxx"
#include "model.hxx"
#include "model_atoms.hxx"
#include "slab.hxx"

DLV::model *DLV::create_slab(const string model_name)
{
  return new slab(model_name);
}

DLV::model_base *DLV::slab::duplicate(const string label) const
{
  slab *m = new slab(label);
  m->copy_atom_groups(this);
  return m;
}

void DLV::slab::use_which_lattice_parameters(bool usage[6]) const
{
  usage[0] = true;
  usage[1] = false;
  usage[2] = false;
  usage[3] = false;
  usage[4] = false;
  usage[5] = false;
  switch (get_lattice_type()) {
  case 0:
    usage[5] = true;
  case 1:
    usage[1] = true;
    break;
  default:
    break;
  }
}

bool DLV::slab::set_primitive_lattice(const coord_type a[3],
				      const coord_type b[3],
				      const coord_type [3])
{
  return set_lattice(a, b);
}

bool DLV::slab::set_lattice_parameters(const coord_type a, const coord_type b,
				       const coord_type, const coord_type,
				       const coord_type,
				       const coord_type gamma)
{
  return set_parameters(a, b, gamma);
}

DLV::int_g DLV::slab::get_number_of_periodic_dims() const
{
  return 2;
}

DLV::int_g DLV::slab::get_lattice_type() const
{
  return lattice_type();
}

DLV::int_g DLV::slab::get_lattice_centring() const
{
  return get_centre_type();
}

// Kind of the dimensionality - for the User Interface menus.
DLV::int_g DLV::slab::get_model_type() const
{
  return 2;
}

void DLV::slab::get_frac_rotation_operators(real_l r[][3][3],
					    const int_g nops) const
{
  // Todo
  get_cart_rotation_operators(r, nops);
}

void DLV::slab::set_crystal03_lattice_type(const int_g lattice,
					   const int_g centre)
{
  set_crystal03_lattice(lattice, centre);
}

void DLV::slab::get_primitive_lattice(coord_type a[3], coord_type b[3],
				      coord_type c[3]) const
{
  coord_type l[3][3];
  get_primitive_vectors(l);
  a[0] = l[0][0];
  a[1] = l[0][1];
  a[2] = l[0][2];
  b[0] = l[1][0];
  b[1] = l[1][1];
  b[2] = l[1][2];
  c[0] = 0.0;
  c[1] = 0.0;
  c[2] = 0.0;
}

void DLV::slab::get_conventional_lattice(coord_type a[3], coord_type b[3],
					 coord_type c[3]) const
{
  get_primitive_lattice(a, b, c);
  coord_type t[3][3];
  get_transform_to_conv_cell(t);
  coord_type conv[3][3];
  for (int_g i = 0; i < 2; i++)
    for (int_g j = 0; j < 2; j++)
      conv[i][j] = t[i][0] * a[j] + t[i][1] * b[j];
  for (int_g i = 0; i < 2; i++) {
    a[i] = conv[0][i];
    b[i] = conv[1][i];
    c[i] = 0.0;
  }
}

void DLV::slab::get_reciprocal_lattice(coord_type a[3], coord_type b[3]) const
{
  coord_type pa[3];
  coord_type pb[3];
  coord_type pc[3];
  get_primitive_lattice(pa, pb, pc);
  real_l vol = pa[0] * pb[1] - pa[1] * pb[0];
  a[0] = real_l(2.0 * pi) * pb[1] / vol;
  a[1] = real_l(-2.0 * pi) * pb[0] / vol;
  a[2] = 0.0;
  b[0] = real_l(-2.0 * pi) * pa[1] / vol;
  b[1] = real_l(2.0 * pi) * pa[0] / vol;
  b[2] = 0.0;
}

void DLV::slab::get_reciprocal_lattice(coord_type a[3], coord_type b[3],
				       coord_type c[3]) const
{
  get_reciprocal_lattice(a, b);
  c[0] = 0.0;
  c[1] = 0.0;
  c[2] = 0.0;
}

void DLV::slab::generate_real_space_lattice(real_g (* &ipoints)[3],
					    real_g (* &fpoints)[3],
					    int_g &nlines, const int_g na,
					    const int_g nb, const int_g,
					    const bool centre_cell,
					    const bool conventional,
					    char [], const int_g) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  if (conventional)
    get_conventional_lattice(a, b, c);
  else
    get_primitive_lattice(a, b, c);
  real_g origin[3] = { 0.0, 0.0, 0.0 };
  if (centre_cell) {
    origin[0] = real_g(- (real_l(na) * a[0] + real_l(nb) * b[0]) / 2.0);
    origin[1] = real_g(- (real_l(na) * a[1] + real_l(nb) * b[1]) / 2.0);
    origin[2] = real_g(- (real_l(na) * a[2] + real_l(nb) * b[2]) / 2.0);
  }
  int_g size = (na + 1) + (nb + 1);
  ipoints = new real_g[size][3];
  fpoints = new real_g[size][3];
  // Generate (nb + 1) lines in 'a' direction.
  for (int_g i = 0; i <= nb; i++) {
    ipoints[i][0] = origin[0] + real_g(b[0]) * real_g(i);
    ipoints[i][1] = origin[1] + real_g(b[1]) * real_g(i);
    ipoints[i][2] = origin[2] + real_g(b[2]) * real_g(i);
    fpoints[i][0] = ipoints[i][0] + real_g(a[0]) * real_g(na);
    fpoints[i][1] = ipoints[i][1] + real_g(a[1]) * real_g(na);
    fpoints[i][2] = ipoints[i][2] + real_g(a[2]) * real_g(na);
  }
  // Generate (na + 1) lines in 'b' direction.
  int_g j = nb + 1;
  for (int_g i = 0; i <= na; i++) {
    ipoints[j + i][0] = origin[0] + real_g(a[0]) * real_g(i);
    ipoints[j + i][1] = origin[1] + real_g(a[1]) * real_g(i);
    ipoints[j + i][2] = origin[2] + real_g(a[2]) * real_g(i);
    fpoints[j + i][0] = ipoints[j + i][0] + real_g(b[0]) * real_g(nb);
    fpoints[j + i][1] = ipoints[j + i][1] + real_g(b[1]) * real_g(nb);
    fpoints[j + i][2] = ipoints[j + i][2] + real_g(b[2]) * real_g(nb);
  }
  nlines = size;
}

DLV::int_g
DLV::slab::get_real_space_lattice_labels(real_g points[][3], const int_g na,
					 const int_g nb, const int_g,
					 const bool centre_cell,
					 const bool conventional) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  if (conventional)
    get_conventional_lattice(a, b, c);
  else
    get_primitive_lattice(a, b, c);
  real_g origin[3] = { 0.0, 0.0, 0.0 };
  if (centre_cell) {
    origin[0] = real_g(- (real_l(na) * a[0] + real_l(nb) * b[0]) / 2.0);
    origin[1] = real_g(- (real_l(na) * a[1] + real_l(nb) * b[1]) / 2.0);
    origin[2] = real_g(- (real_l(na) * a[2] + real_l(nb) * b[2]) / 2.0);
  }
  points[0][0] = origin[0];
  points[0][1] = origin[1];
  points[0][2] = origin[2];
  points[1][0] = origin[0] + real_g(na) * real_g(a[0]);
  points[1][1] = origin[1] + real_g(na) * real_g(a[1]);
  points[1][2] = origin[2] + real_g(na) * real_g(a[2]);
  points[2][0] = origin[0] + real_g(nb) * real_g(b[0]);
  points[2][1] = origin[1] + real_g(nb) * real_g(b[1]);
  points[2][2] = origin[2] + real_g(nb) * real_g(b[2]);
  return 3;
}

void DLV::slab::generate_k_space_vertices(real_g (* &vertices)[3],
					  int_g &nlines, int_l * &connects,
					  int_l &nconnects, char [],
					  const int_g) const
{
  coord_type a[3];
  coord_type b[3];
  get_reciprocal_lattice(a, b);
  // 8 would probably be enough
  real_g verts[16][2];
  int_g nverts = 0;
  // Start by finding intercepts of (1,0), (0,1) etc.
  real_g vectors[4][2];
  // For x = a + t m
  real_g direction[4][2];
  real_g points[4][2];
  real_g intercept1[4]; //, intercept2[4];
  int_g i, j;
  for (i = 0; i < 2; i++) {
    // (1,0)
    vectors[0][i] = real_g(a[i]);
    // (0,1)
    vectors[1][i] = real_g(b[i]);
    // (-1,0)
    vectors[2][i] = real_g(- a[i]);
    // (0,-1)
    vectors[3][i] = real_g(- b[i]);
  }
  // Setup points and slopes (-vy, vx) rotate by 90
  for (i = 0; i < 4; i++) {
    direction[i][0] = - vectors[i][1];
    direction[i][1] = vectors[i][0];
    points[i][0] = real_g(0.5) * vectors[i][0];
    points[i][1] = real_g(0.5) * vectors[i][1];
  }
  // Find intercepts between lines, if slope[1] is very small then
  // don't need to solve simultaneous equations
  const real_g tol = 0.001f;
  real_g x, y;
  for (i = 0; i < 4; i++) {
    j = (i + 1) % 4;
    if (abs(direction[j][1]) < tol) {
      intercept1[i] = (points[j][1] - points[i][1]) / direction[i][1];
      x = points[i][0] + intercept1[i] * direction[i][0];
      //intercept2[i] = (x - points[j][0]) / direction[j][0];
    } else {
      intercept1[i] = ((points[i][0] - points[j][0]) - direction[j][0]
		       * (points[i][1] - points[j][1]) / direction[j][1]) /
	(direction[i][1] * direction[j][0] / direction[j][1]
	 - direction[i][0]);
      y = points[i][1] + intercept1[i] * direction[i][1];
      //intercept2[i] = (y - points[j][1]) / direction[j][1];
    }
  }
  // (1,1) etc vectors
  real_g vector2[4][2];
  real_g directio2[4][2];
  real_g point2[4][2];
  for (i = 0; i < 2; i++) {
    // (1,1)
    vector2[0][i] = real_g(a[i] + b[i]);
    // (-1,1)
    vector2[1][i] = real_g(- a[i] + b[i]);
    // (-1,-1)
    vector2[2][i] = real_g(- a[i] - b[i]);
    // (1,-1)
    vector2[3][i] = real_g(a[i] - b[i]);
  }
  // Setup points and slopes (-vy, vx) rotate by 90
  for (i = 0; i < 4; i++) {
    directio2[i][0] = - vector2[i][1];
    directio2[i][1] = vector2[i][0];
    point2[i][0] = real_g(0.5) * vector2[i][0];
    point2[i][1] = real_g(0.5) * vector2[i][1];
  }
  nverts = 0;
  for (i = 0; i < 4; i++) {
    // Look for (1,1) and related intercepts.
    x = points[i][0] + intercept1[i] * direction[i][0];
    y = points[i][1] + intercept1[i] * direction[i][1];
    real_g il = (x * x + y * y);
    real_g diagl = (point2[i][0] * point2[i][0] + point2[i][1] * point2[i][1]);
    if (il > (diagl + tol)) {
      // Midpoint of (1,1) is inside intercept so it will also intersect.
      real_g t;
      j = (i + 1) % 4;
      // (1,0) to (1,1) etc
      if (abs(directio2[i][1]) < tol) {
	t = (point2[i][1] - points[i][1]) / direction[i][1];
      } else {
	t = ((points[i][0] - point2[i][0]) - directio2[i][0]
	     * (points[i][1] - point2[i][1]) / directio2[i][1]) /
	  (direction[i][1] * directio2[i][0] / directio2[i][1]
	   - direction[i][0]);
      }
      verts[nverts][0] = points[i][0] + t * direction[i][0];
      verts[nverts][1] = points[i][1] + t * direction[i][1];
      nverts++;
      // (1,1) to (0,1) etc
      if (abs(direction[j][1]) < tol) {
	t = (points[j][1] - point2[i][1]) / directio2[i][1];
      } else {
	t = ((point2[i][0] - points[j][0]) - direction[j][0]
	     * (point2[i][1] - points[j][1]) / direction[j][1]) /
	  (directio2[i][1] * direction[j][0] / direction[j][1]
	   - directio2[i][0]);
      }
      verts[nverts][0] = point2[i][0] + t * directio2[i][0];
      verts[nverts][1] = point2[i][1] + t * directio2[i][1];
      nverts++;
    } else {
      verts[nverts][0] = x;
      verts[nverts][1] = y;
      nverts++;
    }
  }
  nlines = nverts;
  vertices = new real_g[nverts][3];
  for (i = 0; i < nverts; i++) {
    vertices[i][0] = verts[i][0];
    vertices[i][1] = verts[i][1];
    vertices[i][2] = 0.0;
  }
  nconnects = 2 * nverts;
  connects = new int_l[nconnects];
  int_g count = 0;
  for (j = 0; j < nverts; j++) {
    connects[count] = j;
    connects[count + 1] = (j + 1) % nverts;
    count += 2;
  }
}

void DLV::slab::generate_transforms(const int_g na, const int_g nb,
				    const int_g, const int_g sa, const int_g sb,
				    const int_g, const bool centre_cell,
				    const bool conventional, int_g &ntransforms,
				    real_g (* &transforms)[3]) const
{
  int_g size = na * nb;
  ntransforms = size;
  transforms = new real_g[size][3];
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  if (conventional)
    get_conventional_lattice(a, b, c);
  else
    get_primitive_lattice(a, b, c);
  int_g min_a = 0;
  int_g max_a = na;
  int_g min_b = 0;
  int_g max_b = nb;
  if (centre_cell) {
    max_a = na / 2;
    min_a = - max_a;
    if (max_a * 2 != na)
      max_a++;
    max_b = nb / 2;
    min_b = - max_b;
    if (max_b * 2 != nb)
      max_b++;
  }
  int_g k = 0;
  for (int_g i = min_a; i < max_a; i++) {
    real_g x[3];
    x[0] = (real_g)(i * sa) * (real_g)a[0];
    x[1] = (real_g)(i * sa) * (real_g)a[1];
    x[2] = (real_g)(i * sa) * (real_g)a[2];
    for (int_g j = min_b; j < max_b; j++) {
      transforms[k][0] = x[0] + (real_g)(j * sb) * (real_g)b[0];
      transforms[k][1] = x[1] + (real_g)(j * sb) * (real_g)b[1];
      transforms[k][2] = x[2] + (real_g)(j * sb) * (real_g)b[2];
      k++;
    }
  }
}

bool DLV::slab::set_fractional_sym_ops(const real_l rotations[][3][3],
				       const real_l translations[][3],
				       const int_g nops)
{
  // Todo
  return false;
}

void DLV::slab::generate_primitive_atoms(const bool tidy)
{
  int_g nops = get_number_of_sym_ops();
  real_l (*rotations)[3][3] = new_local_array3(real_l, nops, 3, 3);
  real_l (*translations)[3] = new_local_array2(real_l, nops, 3);
  get_cartesian_ops(rotations, translations, nops);
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  generate_primitive_copies(a, b, rotations, translations, nops, tidy);
  delete_local_array(translations);
  delete_local_array(rotations);
}

void DLV::slab::find_asym_pos(const int_g asym_idx, const coord_type a[3],
			      int_g &op, coord_type a_coords[3],
			      const coord_type b[][3], const int_g b_asym_idx[],
			      coord_type b_coords[][3], int_g ab_shift[3],
			      const int_g nbatoms) const
{
  // only designed for i, j to i, j, k, l
  if (nbatoms < 1 or nbatoms > 3)
    exit(1);
  int_g nops = get_number_of_sym_ops();
  real_l (*rotations)[3][3] = new_local_array3(real_l, nops, 3, 3);
  real_l (*translations)[3] = new_local_array2(real_l, nops, 3);
  get_cartesian_ops(rotations, translations, nops);
  coord_type la[3];
  coord_type lb[3];
  coord_type lc[3];
  get_primitive_lattice(la, lb, lc);
  map_asym_pos(asym_idx, a, op, a_coords, b, b_asym_idx, b_coords, ab_shift,
	       nbatoms, la, lb, rotations, translations, nops);
  delete_local_array(translations);
  delete_local_array(rotations);
}

void DLV::slab::conventional_atoms(const atom_tree &primitive, atom_tree &tree,
				   const bool centre[3]) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_conventional_lattice(a, b, c);
  int_g n = get_number_conv_cell_translators();
  real_l (*translators)[3] = new_local_array2(real_l, n, 3);
  get_conv_cell_translators(translators);
  primitive.add_conv_cell_atoms(tree, a, b, translators, n, centre);
  delete_local_array(translators);
}

void DLV::slab::centre_atoms(const atom_tree &primitive, atom_tree &tree,
			     const bool centre[3]) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  real_l cell[3][3];
  for (int_g i = 0; i < 3; i++) {
    cell[i][0] = a[i];
    cell[i][1] = b[i];
    cell[i][2] = 0.0;
  }
  primitive.copy_and_centre(tree, cell, 2, centre);
}

void DLV::slab::update_display_atoms(atom_tree &tree, const real_l tol,
				     const int_g na, const int_g nb,
				     const int_g, const bool conventional,
				     const bool centre, const bool edges) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  if (conventional)
    get_conventional_lattice(a, b, c);
  else
    get_primitive_lattice(a, b, c);
  tree.replicate_atoms(a, b, tol, na, nb, centre, edges);
}

void DLV::slab::generate_bonds(const atom_tree &tree,
			       const int_g na, const int_g nb, const int_g,
			       const bool conventional,
			       std::list<bond_list> info[],
			       const real_g overlap, const bool centre,
			       const bool invert_colours,
			       const bond_data &bond_info,
			       const real_l tol,
			       const std::vector<real_g (*)[3]> *traj,
			       const int_g nframes, const bool gamma,
			       const bool edit) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  if (conventional)
    get_conventional_lattice(a, b, c);
  else
    get_primitive_lattice(a, b, c);
  tree.generate_bonds(a, b, na, nb, info, nframes, overlap, centre,
		      invert_colours, bond_info, tol, traj, gamma, edit);
}

void DLV::slab::get_lattice_parameters(coord_type &a, coord_type &b,
				       coord_type &c, coord_type &alpha,
				       coord_type &beta, coord_type &gamma,
				       const bool conventional) const
{
  if (conventional)
    get_parameters(a, b, gamma);
  else
    create_primitive_params(a, b, gamma);
  c = 0.0;
  alpha = 0.0;
  beta = 0.0;
}

void DLV::slab::complete_lattice(coord_type va[3], coord_type vb[3],
				 coord_type [3])
{
  if (has_valid_parameters() and !has_valid_vectors()) {
    coord_type a;
    coord_type b;
    coord_type gamma;
    get_parameters(a, b, gamma);
    switch (get_lattice_type()) {
    case 1:
      gamma = 90.0;
      break;
    case 3:
      b = a;
      gamma = 90.0;
      break;
    case 2:
    case 4:
      b = a;
      gamma = 120.0;
      break;
    default:
      break;
    }
    // store all parameter values.
    set_parameters(a, b, gamma);
    va[0] = a;
    va[1] = 0.0;
    va[2] = 0.0;
    vb[0] = b * cos(to_radians(gamma));
    vb[1] = b * sin(to_radians(gamma));
    vb[2] = 0.0;
    set_lattice(va, vb);
    set_valid_vectors();
    set_valid_parameters();
  } else if (has_valid_vectors() and !has_valid_parameters()) {
    coord_type al[3];
    coord_type bl[3];
    coord_type cl[3];
    get_conventional_lattice(al, bl, cl);
    coord_type a = sqrt(al[0] * al[0] + al[1] * al[1] + al[2] * al[2]);
    coord_type b = sqrt(bl[0] * bl[0] + bl[1] * bl[1] + bl[2] * bl[2]);
    // get cos(alpha) from cos rule
    coord_type gamma = to_degrees(acos((al[0] * bl[0]
					+ al[1] * bl[1]
					+ al[2] * bl[2]) / (a * b)));
    set_parameters(a, b, gamma);
    set_valid_parameters();
    set_valid_vectors();
    get_primitive_lattice(va, vb, cl);
  }
  else if (has_valid_parameters() and has_valid_vectors()){
    coord_type cl[3];
    get_primitive_lattice(va, vb, cl);
  }
}

void DLV::slab::create_primitive_params(coord_type &a, coord_type &b,
					coord_type &gamma) const
{
  coord_type al[3];
  coord_type bl[3];
  coord_type cl[3];
  get_primitive_lattice(al, bl, cl);
  a = sqrt(al[0] * al[0] + al[1] * al[1] + al[2] * al[2]);
  b = sqrt(bl[0] * bl[0] + bl[1] * bl[1] + bl[2] * bl[2]);
  // get cos(alpha) from cos rule
  gamma = to_degrees(acos((al[0] * bl[0] + al[1] * bl[1]
			   + al[2] * bl[2]) / (a * b)));
}
void DLV::slab::map_data(const atom_tree &tree, const real_g grid[][3],
			 int_g **const data, const int_g ndata,
			 const int_g ngrid, real_g (* &map_grid)[3],
			 int_g ** &map_data, int_g &n) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  tree.map_data(grid, data, ndata, ngrid, map_grid, map_data, n, a, b);
}

void DLV::slab::map_data(const atom_tree &tree, const real_g grid[][3],
			 real_g **const data, const int_g ndata,
			 const int_g ngrid, real_g (* &map_grid)[3],
			 real_g ** &map_data, int_g &n) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  tree.map_data(grid, data, ndata, ngrid, map_grid, map_data, n, a, b);
}

void DLV::slab::map_data(const atom_tree &tree, const real_g grid[][3],
			 const real_g mag[][3], const real_g phases[][3],
			 const int_g ngrid, real_g (* &map_grid)[3],
			 real_g (* &map_data)[3], int_g &n, const real_g ka,
			 const real_g kb, const real_g) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  tree.map_data(grid, mag, phases, ngrid, map_grid, map_data, n, ka, kb, a, b);
}

void DLV::slab::map_data(const atom_tree &tree, const real_g grid[][3],
			 const real_g mags[][3], const real_g phases[][3],
			 const int_g ngrid,
			 std::vector<real_g (*)[3]> &new_data,
			 int_g &n, const int_g nframes, const int_g ncopies,
			 const real_g ka, const real_g kb, const real_g,
			 const real_g scale) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  tree.map_data(grid, mags, phases, ngrid, new_data, n, scale,
		nframes, ncopies, ka, kb, a, b);
}

void DLV::slab::map_data(const atom_tree &tree, const real_g grid[][3],
			 const int_g ngrid,
			 const std::vector<real_g (*)[3]> &traj,
			 const int_g nframes,
			 std::vector<real_g (*)[3]> &new_data) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  tree.map_data(grid, ngrid, traj, new_data, nframes, a, b);
}

void DLV::slab::build_supercell_symmetry(const model *m,
					 const coord_type a[3],
					 const coord_type b[3],
					 const coord_type c[3],
					 const coord_type va[3],
					 const coord_type vb[3],
					 const coord_type vc[3],
					 const bool conv)
{
  const slab *sym = static_cast<const slab *>(m);
  copy_symmetry(sym, false);
  if (conv)
    add_conv_cell_operators(sym, a, b, c, 2);
  build_supercell_operators(a, b, c, va, vb, vc, 2);
}

void DLV::slab::copy_lattice(const model_base *m)
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  m->get_primitive_lattice(a, b, c);
  set_primitive_lattice(a, b, c);
  coord_type a1;
  coord_type b1;
  coord_type c1;
  coord_type alpha;
  coord_type beta;
  coord_type gamma;
  m->get_lattice_parameters(a1, b1, c1, alpha, beta, gamma);
  set_parameters(a1, b1, gamma);
  set_valid_vectors();
}

void DLV::slab::copy_bravais_lattice(const model_base *m)
{
  const slab *sym = static_cast<const slab *>(m);
  copy_lattice_type(sym);
}

void DLV::slab::copy_symmetry_ops(const model_base *m, const bool frac)
{
  const slab *sym = static_cast<const slab *>(m);
  copy_symmetry(sym, frac);
}

void DLV::slab::rotate_symmetry_ops(const model_base *m,
				    const real_l r[3][3])
{
  const slab *sym = static_cast<const slab *>(m);
  rotate_symmetry(sym, r);
}

void DLV::slab::shift_cartesian_operators(const model_base *m,
					  const real_l x, const real_l y,
					  const real_l z,
					  const coord_type a[3],
					  const coord_type b[3],
					  const coord_type c[3])
{
  const slab *sym = static_cast<const slab *>(m);
  shift_ops_cartesian(sym->get_symmetry(), x, y, z, a, b, c, 2);
}

void DLV::slab::shift_cartesian_operators(const model_base *m, const real_l x,
					  const real_l y, const real_l z,
					  const coord_type a[3],
					  const coord_type b[3],
					  const coord_type c[3],
					  const int_g dim)
{
  const slab *sym = static_cast<const slab *>(m);
  shift_ops_cartesian(sym->get_symmetry(), x, y, z, a, b, c, dim);
}

void DLV::slab::shift_fractional_operators(const model_base *m,
					   const real_l x, const real_l y,
					   const real_l z,
					   const coord_type a[3],
					   const coord_type b[3],
					   const coord_type c[3])
{
  const slab *sym = static_cast<const slab *>(m);
  shift_ops_fractional(sym->get_symmetry(), x, y, z, a, b, c, 2);
}

void DLV::slab::cartesian_to_fractional_coords(real_l &x, real_l &y,
					       real_l &) const
{
  // get x, y, z as fractional
  coord_type l[3][3];
  get_primitive_vectors(l);
  real_l inverse[3][3];
  matrix_invert(l, inverse, 2);
  real_l t;
  t = inverse[0][0] * x + inverse[1][0] * y;
  y = inverse[0][1] * x + inverse[1][1] * y;
  x = t;
}

void DLV::slab::fractional_to_cartesian_coords(real_l &x, real_l &y,
					       real_l &) const
{
  // get x, y, z as cartesian
  coord_type l[3][3];
  get_primitive_vectors(l);
  real_l t = l[0][0] * x + l[0][1] * y;
  y = l[1][0] * x + l[1][1] * y;
  x = t;
}

bool DLV::slab::map_atom_selections(const atom_tree &display,
				    const atom_tree &prim,
				    const real_g grid[][3],
				    int_g **const data, const int_g ndata,
				    const int_g ngrid, int_g * &labels,
				    int_g * &atom_types, atom * &parents,
				    int_g &n, const bool all_atoms) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  return display.map_atom_selections(prim, n, labels, atom_types, parents,
				     all_atoms, grid, data, ndata, ngrid,
				     a, b);
}

bool DLV::slab::map_selected_atoms(const atom_tree &display, int_g &n,
				   int_g * &labels, int_g (* &shifts)[3],
				   const real_g grid[][3], int_g **const data,
				   const int_g ndata, const int_g ngrid) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  return display.map_selected_atoms(n, labels, shifts, grid, data, ndata,
				    ngrid, a, b);
}

void DLV::slab::get_atom_shifts(const atom_tree &tree,
				const coord_type coords[][3],
				int_g shifts[][3], int_g prim_id[],
				const int_g n) const
{
  coord_type a[3];
  coord_type b[3];
  coord_type c[3];
  get_primitive_lattice(a, b, c);
  tree.get_atom_shifts(coords, shifts, prim_id, n, a, b);
}  

#ifdef DLV_USES_SERIALIZE

namespace boost {
  namespace serialization {

    template <class Archive>
    inline void save_construct_data(Archive &ar, const DLV::slab *t,
				    const unsigned int file_version)
    {
      DLV::string n = t->get_model_name();
      ar << n;
    }

    template <class Archive>
    inline void load_construct_data(Archive &ar, DLV::slab *t,
				    const unsigned int file_version)
    {
      DLV::string n;
      ar >> n;
      ::new(t)DLV::slab(n);
    }

  }
}

template <class Archive>
void DLV::slab::serialize(Archive &ar, const unsigned int version)
{
  ar &
    boost::serialization::base_object<periodic_model<plane_symmetry> >(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(DLV::slab)

DLV_SUPPRESS_TEMPLATES(DLV::model_impl<DLV::plane_symmetry>)

DLV_NORMAL_EXPLICIT(DLV::slab)
DLV_EXPORT_EXPLICIT(DLV::slab)

#endif // DLV_USES_SERIALIZE
