
#ifndef DLV_SYMMETRY_IMPL
#define DLV_SYMMETRY_IMPL

namespace DLV {

  class symmetry_operator {
  public:
    friend class symmetry_data;

    void get_cartesian(real_l r[3][3], real_l t[3]) const;
    void get_fractional_trans(real_l t[3]) const;

  protected:
    // These are unsafe used on their own, so protect them
    symmetry_operator();
    void set_cartesian(const real_l r[3][3], const real_l t[3]);
    void set_cartesian(const real_l r[3][3], const rotation_type rot_id,
		       const reflection_type ref_id);
    void set_fractional(const real_l r[3][3], const real_l t[3],
			const rotation_type rot_id,
			const reflection_type ref_id);
    void set_fractional_translation(const real_l t[3]);
    void add_cartesian_translation(const real_l t[3]);
    void generate_cartesian_translation(const coord_type va[3],
					const coord_type vb[3],
					const coord_type vc[3],
					const int_g dim);
    void generate_fractional_translation(const real_l inv[3][3],
					 const int_g dim);

  private:
    real_l rotation[3][3];
    real_l cart_translation[3];
    real_l frac_translation[3];
    rotation_type rotation_id;
    reflection_type reflect_id;
    bool has_translation;
    // Todo - do we use these?
    bool generator;
    bool glide;
    symmetry_operator *inverse;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

}

inline DLV::symmetry_operator::symmetry_operator() :
  rotation_id(identity), reflect_id(no_reflection), has_translation(false),
  generator(false), glide(false), inverse(0)
{
}

#endif // DLV_SYMMETRY_IMPL
