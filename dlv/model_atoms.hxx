
#ifndef DLV_MODEL_IMPLEMENTATIONS
#define DLV_MODEL_IMPLEMENTATIONS

#include <map>
#ifdef DLV_USES_SERIALIZE
  #include <boost/serialization/map.hpp>
#endif // DLV_USES_SERIALIZE

namespace DLV {

  class triple {
  public:
    int_g i;
    int_g j;
    int_g k;
  };

  class polyhedron {
  public:
    std::list<int_l> indices;
    std::list<triple> triangles;
  };

  // Todo - this seems rather too complicated
  class model_base : public model {
  public:
    model_base(const string model_name);
    ~model_base();

    bool has_salvage() const;

    void count_primitive_atom_types(int_g count[], const int_g len) const;
    void count_crystal03_ids(int_g count[], const int_g len) const;
    string get_formula();
    void complete(const bool only_atoms = false);
    void update_atom_list();
    void get_primitive_lattice(coord_type a[3], coord_type b[3],
			       coord_type c[3]) const;
    void get_conventional_lattice(coord_type a[3], coord_type b[3],
				  coord_type c[3]) const;
    int_g get_hermann_mauguin_group(string &name) const;
    int_g get_number_of_asym_atoms() const;
    int_g get_number_of_primitive_atoms() const;
    void get_asym_atom_types(int_g atom_types[], const int_g n) const;
    void get_asym_atom_regions(const char group[], int_g regions[],
			       const int_g n) const;
    void get_asym_crystal03_ids(int_g atom_ids[], const int_g n) const;
    void get_asym_occupancy(real_g atom_occ[], const int_g n) const;
    void set_asym_occupancy(real_g atom_occ[], const int_g n) const;
    void get_asym_core_electrons(int_g atom_ids[], const int_g n) const;
    void get_asym_point_charge(real_l charge[], const int_g n) const;
    void get_asym_isotope(int_g isotope[], const int_g n) const;
    void get_asym_debye_waller1(real_g atom_ids[], const int_g n) const;
    void get_asym_debye_waller2(real_g atom_ids[], const int_g n) const;
    void set_asym_debye_waller(real_g atom_dw1[], real_g atom_dw2[],
			       const int_g n) const;
    void get_asym_displacements(real_g atom_disps[][3], const int_g n) const;
    void set_asym_displacements(real_g atom_disps[][3], const int_g n) const;
    void get_asym_rod_dw1(int_g atom_ids[], const int_g n) const;
    void get_asym_rod_dw2(int_g atom_ids[], const int_g n) const;
    void get_asym_rod_noccup(int_g atom_ids[], const int_g n) const;
    void set_asym_rod_dw1(int_g atom_ids[], const int_g n) const;
    void set_asym_rod_dw2(int_g atom_ids[], const int_g n) const;
    void set_asym_rod_noccup(int_g atom_ids[], const int_g n) const;
    void get_asym_rod_xconst(real_g atom_ids[], const int_g n) const;
    void get_asym_rod_nxdis(int_g atom_ids[], const int_g n) const;
    void get_asym_rod_x2const(real_g atom_ids[], const int_g n) const;
    void get_asym_rod_nx2dis(int_g atom_ids[], const int_g n) const;
    void get_asym_rod_yconst(real_g atom_ids[], const int_g n) const;
    void get_asym_rod_nydis(int_g atom_ids[], const int_g n) const;
    void get_asym_rod_y2const(real_g atom_ids[], const int_g n) const;
    void get_asym_rod_ny2dis(int_g atom_ids[], const int_g n) const;
    void get_asym_rod_nzdis(int_g atom_ids[], const int_g n) const;
    void get_asym_atom_spins(int_g atom_spins[], const int_g n) const;
    void get_primitive_atom_types(int_g atom_types[], const int_g n) const;
    void get_primitive_atom_regions(const char group[], int_g regions[],
				    const int_g n) const;
    void get_primitive_asym_indices(int_g indices[], const int_g n) const;
    void get_asym_to_primitive_indices(int_g indices[], const int_g n) const;
    void get_asym_atom_charges(int_g charges[], const int_g n) const;
    void has_asym_atom_special_periodicity(bool special_periodicity[], 
					   const int_g n) const;
    void get_asym_atom_cart_coords(coord_type coords[][3], const int_g n) const;
    void get_asym_atom_frac_coords(coord_type coords[][3], const int_g n) const;
    void get_primitive_atom_cart_coords(coord_type coords[][3],
					const int_g n) const;
    void get_primitive_atom_frac_coords(coord_type coords[][3],
					const int_g n) const;
    int_g get_atomic_number(const int_g index) const;
    void get_atom_properties(const int_g index, int_g &c, real_g &r,
			     real_g &red, real_g &green, real_g &blue,
			     int_g &s, bool &use_charge, bool &use_radius,
			     bool &use_colour, bool &use_spin) const;
    void get_atom_shifts(const coord_type coords[][3], int_g shifts[][3],
			 int_g prim_id[], int_g asym_id[], const int_g n) const;
    void map_atom_data(const real_g grid[][3], int_g **const data,
		       const int_g ndata, const int_g ngrid,
		       real_g (* &map_grid)[3], int_g ** &new_data,
		       int_g &n) const;
    void map_atom_data(const real_g grid[][3], real_g **const data,
		       const int_g ndata, const int_g ngrid,
		       real_g (* &map_grid)[3], real_g ** &new_data,
		       int_g &n) const;
    void map_atom_data(const real_g grid[][3], const real_g mags[][3],
		       const real_g phases[][3], const int_g ngrid,
		       real_g (* &map_grid)[3], real_g (* &new_data)[3],
		       int_g &n, const real_g ka, const real_g kb,
		       const real_g kc) const;
    void map_atom_data(const real_g grid[][3], const real_g mags[][3],
		       const real_g phases[][3], const int_g ngrid,
		       std::vector<real_g (*)[3]> &new_data, int_g &n,
		       const int_g nframes, const int_g ncopies,
		       const real_g ka, const real_g kb, const real_g kc,
		       const real_g scale) const;
    void map_atom_data(const real_g grid[][3], const int_g ngrid,
		       const std::vector<real_g (*)[3]> &traj,
		       const int_g nframes,
		       std::vector<real_g (*)[3]> &new_data) const;

    bool set_atom_charge(const string symbol, const int_g charge,
			 char message[], const int_g mlen);
    bool set_core_electrons(const string symbol, const int_g core,
			    char message[], const int_g mlen);
    void set_point_charge(const int_g index, const real_l charge,
			  char message[], const int_g mlen);
    void set_isotope(const int_g index, const int_g isotope,
		     char message[], const int_g mlen);
    void add_bond(const int_g atom1, const int_g atom2);
    void group_atoms(const string gp, const string label, const bool atoms[]);

    DLVreturn_type create_atom_group(const char label[], const char group[],
				     const bool all, const char remainder[],
				     char message[], const int_g mlen);
    DLVreturn_type update_cluster_region(const real_g radii[],
					 const int_g nradii, const char group[],
					 char message[], const int_g mlen);
    DLVreturn_type update_cluster_region(const int_g shells[],
					 const int_g nshells,
					 const char group[],
					 char message[], const int_g mlen);
    DLVreturn_type clear_cluster_region(const char group[], char message[],
					const int_g mlen);
    DLVreturn_type set_supercell_region(const model *parent,
					const int_g shells[],
					const int_g nshells,
					const char group[],
					char message[], const int_g mlen);

    void convert_coords(const bool frac, real_l &x,
			real_l &y, real_l &z) const;

    model *duplicate_model(const string label) const;
    void copy_model(const model *m);
    model *copy_model(const string name) const;
    void replace_atom_cart_coords(const coord_type coords[][3]);
    void update_model(const model *m);
    bool supercell(const model *m, const int_g matrix[3][3],
		   const bool conv_cell);
    bool delete_symmetry(const model *m);
    bool view_to_molecule(const model *m);
    bool cell_to_molecule(const model *m, const int_g na, const int_g nb,
			  const int_g nc, const bool conv, const bool centred,
			  const bool edges);
    bool update_lattice_parameters(const model *m, const coord_type params[6]);
    bool update_origin(const model *m, const real_l x, const real_l y,
		       const real_l z, const bool frac);
    bool update_origin_symm(const model *m, real_l &x, real_l &y,
			    real_l &z, const bool frac);
    bool update_vacuum(const model *m, const real_l c, const real_l o);
    bool update_cluster(model *m, const int_g n);
    bool update_cluster(model *m, const int_g n, const real_l x, const real_l y,
			const real_l z);
    bool update_cluster(model *m, const real_l r);
    bool delete_atoms(model *m);
    bool insert_atom(const model *m, const int_g atn, const real_l x,
		     const real_l y, const real_l z, const bool frac,
		     const bool keep);
    bool insert_model(const model *m, const model *i, const real_l x,
		      const real_l y, const real_l z, const bool frac,
		      const real_l c_x, const real_l c_y,
		      const real_l c_z, const bool keep);
    bool move_atoms(model *m, const real_l x, const real_l y,
		    const real_l z, const real_l c_x, const real_l c_y,
		    const real_l c_z, real_l shift[3], const bool keep);
    bool edit_atom(model *m, const int_g atn, const bool all,
		   const int_g index);
    bool edit_atom_props(model *m, const int_g charge, real_g &radius,
			 const real_g red, const real_g green,
			 const real_g blue, const int_g spin,
			 const bool use_charge, const bool use_radius,
			 const bool use_colour, const bool use_spin,
			 const bool all, const int_g index);
    bool edit_atom_rod_props(const model *m, const int_g dw1,
			     const bool use_dw1, const int_g dw2,
			     const bool use_dw2, const int_g occ,
			     const bool use_occ, const bool all,
			     const int_g index);
    bool update_slab(const model *m, model *threed, const int_g h,
		     const int_g k, const int_g l, const bool conventional,
		     const real_g tol, int_g &layers, string * &labels,
		     int_g &n, real_l r[3][3]);
    bool update_slab(const model *m, const real_g tol, int_g &layers,
		     string * &labels, int_g &n);
    bool update_slab(const model *m, model *threed, const real_g tol,
		     const int_g term, const int_g nl);
    bool surface_slab(const model *m, const real_g tol,
		      int_g &layers, string * &labels, int_g &n);
    bool surface_slab(const model *m, const real_g tol, int_g layers);
    bool update_surface(const model *m, model *threed, const int_g h,
			const int_g k, const int_g l, const bool conventional,
			const real_g tol, string * &labels, int_g &n,
			real_l r[3][3]);
    bool update_surface(const model *m, const real_g tol, const int_g term);
    bool update_salvage(const model *m, const real_g tol, const int_g nlayers);
    void update_shells(const int_g types[], const real_g numbers[],
		       const real_g radii[], const int_g nshells);
    bool wulff(model *m, const real_l r, class data_object *data);
    bool multi_layer(model *m, const real_l r, model *slab);
    bool fill_geometry(model *m, const model *shape);
    bool keep_symmetry() const;
    void set_bond_all();
    void add_atom_group(const string name);

#ifdef ENABLE_DLV_GRAPHICS
    bool has_selected_atoms() const;
    void list_atom_groupings(render_parent *p) const;

    void set_edit_transform(const render_parent *parent, const bool v,
			    real_g m[4][4], real_g t[3], real_g c[3],
			    char message[], const int_g mlen);
    void activate_atom_flags(const string label, const bool reset_flags);
    void deactivate_atom_flags();
    void set_atom_flag(const int_g index, const atom_flag_type aft);
    void toggle_atom_flags(const string label, const bool reset_flags);
    bool set_atom_index_and_flags(int_g indices[], const int_g value,
				  const bool set_def, char message[],
       				  const int_g mlen);
    int_g locate_selected_atom() const;
    int_g find_primitive_selection() const;
    render_parent *create_render_parent(render_parent *p,
					const bool copy_cell,
					const bool copy_wavefn);
    DLVreturn_type render_r(render_parent *p, const model *m,
			    char message[], const int_g mlen);
    DLVreturn_type render_k(render_parent *p,
			    char message[], const int_g mlen);
    class toolkit_obj get_r_ui_obj() const;
    class toolkit_obj get_k_ui_obj() const;
    int_g get_symmetry_selector() const;
    void fix_symmetry_selector(const int_g s);
    DLVreturn_type update_cell(render_parent *p,
			       char message[], const int_g mlen);
    DLVreturn_type update_atoms(const render_parent *p,
				char message[], const int_g mlen);
    DLVreturn_type update_lattice(render_parent *p,
				  const bool kspace, char message[],
				  const int_g mlen);
    DLVreturn_type update_bonds(const render_parent *p,
				char message[], const int_g mlen) const;
    DLVreturn_type add_bond(render_parent *p,
			    char message[], const int_g mlen);
    DLVreturn_type del_bond(render_parent *p,
			    char message[], const int_g mlen);
    DLVreturn_type add_bond_type(render_parent *p,
				 char message[], const int_g mlen);
    DLVreturn_type del_bond_type(render_parent *p,
				 char message[], const int_g mlen);
    DLVreturn_type add_bond_all(render_parent *p,
				char message[], const int_g mlen);
    DLVreturn_type del_bond_all(render_parent *p,
				char message[], const int_g mlen);
    DLVreturn_type select_atom(render_parent *p, const real_g coords[3],
			       const int_g status, int_g &nselects,
			       char message[], const int_g mlen);
    DLVreturn_type deselect_all_atoms(render_parent *p,
				      char message[], const int_g mlen);
    void set_selected_atom_flags(const bool done);

    bool update_origin_atoms(const model *m, real_l &x, real_l &y,
			     real_l &z, const bool frac);
    void get_displayed_cell(render_parent *p, int_g &na, int_g &nb, int_g &nc,
			    bool &conv, bool &centre, bool &edges) const;
    bool map_atom_selections(int_g &n, int_g * &labels, int_g * &atom_types,
			     atom * &parents, const bool all_atoms,
			     const real_g grid[][3], int_g **const data,
			     const int_g ndata, const int_g ngrid) const;
    bool map_selected_atoms(int_g &n, int_g * &labels, int_g (* &shifts)[3],
			    const real_g grid[][3], int_g **const data,
			    const int_g ndata, const int_g ngrid) const;
    void set_atom_data(const render_parent *p,
		       class data_object *d, const bool undisplay);
    void refresh_atom_data(const render_parent *p);
    void transform_atom(const model *m, const int_g atn, real_l &x, real_l &y,
			real_l &z, const bool keep, const real_g matrix[4][4],
			const real_g translate[3], const real_g centre[3]);
    void transform_model(const model *m, const model *i, real_l &x, real_l &y,
			 real_l &z, real_l &c_x, real_l &c_y, real_l &c_z,
			 const bool keep, const real_g matrix[4][4],
			 const real_g translate[3], const real_g centre[3]);
    void transform_move(model *m, real_l &x, real_l &y, real_l &z, real_l &c_x,
			real_l &c_y, real_l &c_z, real_l shift[3],
			const bool keep, const real_g matrix[4][4],
			const real_g translate[3], const real_g centre[3]);
#endif // ENABLE_DLV_GRAPHICS

  protected:
    void set_indexed_asym_cart_coords(const coord_type coords[3], 
				       const int_g n) const;
    void set_indexed_asym_atom_type(const int_g id, const int_g i) const;
    virtual model_base *duplicate(const string label) const = 0;
    void copy_atom_groups(const model_base *m);

    bool add_cartesian_atom(int_g &index, const int_g atomic_number,
			    const coord_type coords[3]);
    bool add_cartesian_ghost_atom(const coord_type coords[3]);
    bool add_cartesian_point_charge(const coord_type coords[3]);
    bool add_fractional_atom(int_g &index, const int_g atomic_number,
			     const coord_type coords[3], const bool rhomb,
			     const bool prim);
    bool add_cartesian_atom(int_g &index, const char symbol[],
			    const coord_type coords[3]);
    bool add_fractional_atom(int_g &index, const char symbol[],
			     const coord_type coords[3], const bool rhomb,
			     const bool prim);
    void set_atom_charge(const int_g index, const int_g charge);
    void set_atom_radius(const int_g index, const real_g r);
    void set_atom_crystal03_id(const int_g index, const int_g id);
    void set_atoms_group(const int_g index, const string gpname,
			 const string label);
    void copy_cartesian_atoms(const model_base *m);
    void copy_cartesian_atoms(const model_base *m, atom *parents);
    void copy_fractional_atoms(const model_base *m);
    void reset_fractional_atoms(const model_base *m);
    void shift_cartesian_atoms(const model_base *m, const real_l z_shift);
    void shift_cartesian_atoms(const real_l x, const real_l y, const real_l z);
    void shift_cartesian_atoms(const model_base *m, const real_l x,
			       const real_l y, const real_l z);
    void shift_fractional_atoms(const model_base *m, const real_l x,
				const real_l y, const real_l z);
    void rotate_cartesian_atoms(const model_base *m, const real_l r[3][3]);
    void centre_slab_atoms(const coord_type vc[3], real_l shift[3]);
    void shift_surface_atoms(const coord_type vc[3], real_l shift[3]);

    virtual int_g get_number_no_trans_ops() const = 0;
    virtual void get_cart_no_trans_ops(real_l r[][3][3],
				       const int_g nops) const = 0;
    virtual void copy_lattice(const model_base *m) = 0;
    virtual void copy_bravais_lattice(const model_base *m) = 0;
    virtual void copy_symmetry_ops(const model_base *m, const bool frac) = 0;
    virtual void rotate_symmetry_ops(const model_base *m,
				     const real_l r[3][3]) = 0;
    void check_point_symmetry(const model_base *m, const atom *parent);
    void check_symmetry(const atom parent[], const coord_type a[3],
			const coord_type b[3], const coord_type c[3]);
    void check_symmetry_no_z(const atom parent[], const coord_type a[3],
			     const coord_type b[3], const coord_type c[3]);
    virtual void identify_bravais_lattice(const int_g old_l, const int old_c);
    virtual void build_bravais_lattice_from_slab(const int_g l, const int_g c);
    virtual void build_supercell_symmetry(const model *m,
					  const coord_type a[3],
					  const coord_type b[3],
					  const coord_type c[3],
					  const coord_type va[3],
					  const coord_type vb[3],
					  const coord_type vc[3],
					  const bool conv) = 0;
    virtual void cartesian_to_fractional_coords(real_l &x, real_l &y,
						real_l &z) const = 0;
    virtual void fractional_to_cartesian_coords(real_l &x, real_l &y,
						real_l &z) const = 0;
    virtual void shift_cartesian_operators(const model_base *m, const real_l x,
					   const real_l y, const real_l z,
					   const coord_type a[3],
					   const coord_type b[3],
					   const coord_type c[3]) = 0;
    virtual void shift_cartesian_operators(const model_base *m, const real_l x,
					   const real_l y, const real_l z,
					   const coord_type a[3],
					   const coord_type b[3],
					   const coord_type c[3],
					   const int_g dim);
    virtual void shift_fractional_operators(const model_base *m,
					    const real_l x, const real_l y,
					    const real_l z,
					    const coord_type a[3],
					    const coord_type b[3],
					    const coord_type c[3]) = 0;
    virtual bool get_min_trans_origin(real_l p[3], const coord_type a[3],
				      const coord_type b[3],
				      const coord_type c[3],
				      const int_g dim) const = 0;

    virtual void get_transform_to_prim_cell(coord_type t[3][3]) const = 0;
    virtual void get_transform_to_prim_cell(int_g t[3][3]) const = 0;
    virtual void generate_real_space_lattice(real_g (* &ipoints)[3],
					     real_g (* &fpoints)[3],
					     int_g &nlines, const int_g na,
					     const int_g nb, const int_g nc,
					     const bool centre_cell,
					     const bool conventional,
					     char message[],
					     const int_g mlen) const = 0;
    virtual void generate_k_space_vertices(real_g (* &vertices)[3],
					   int_g &nlines, int_l * &connects,
					   int_l &nconnects, char message[],
					   const int_g mlen) const = 0;
    virtual int_g get_real_space_lattice_labels(real_g points[][3],
						const int_g na, const int_g nb,
						const int_g nc,
						const bool centre_cell,
					    const bool conventional) const = 0;
    void count_atom_display_list(int_g &natoms1, int_g &natoms2,
				 int_g &natoms3, const int_g select_type,
				 const bool use_flags,
				 const int_g flag_type) const;
    void get_opaque_atoms(real_g (* &coords)[3], real_g (* &colours)[3],
			  real_g * &radii, const real_g scale,
			  const int_g natoms, const int_g select_type,
			  const bool use_flags, const int_g flag_type,
			  const std::vector<real_g (*)[3]> *traj,
			  const int_g nframes);
    void get_transparent_atoms(real_g (* &coords)[3], real_g (* &colours)[3],
			       real_g * &radii, const real_g scale,
			       const int_g natoms, const int_g select_type,
			       const bool use_flags, const int_g flag_type,
			       const std::vector<real_g (*)[3]> *traj,
			       const int_g nframes);
    void get_edit_op_atoms(real_g (* &coords)[3], real_g (* &colours)[3],
			   real_g * &radii, const real_g scale,
			   const int_g natoms,
			   const std::vector<real_g (*)[3]> *traj,
			   const int_g nframes);
    void map_asym_pos(const int_g asym_idx, const coord_type a[3],
		      int_g &op, coord_type a_coords[3],
		      const coord_type b[][3], const int_g b_asym_idx[],
		      coord_type b_coords[][3], int_g ab_shift[3],
		      const int_g nbatoms, const real_l rotations[][3][3],
		      const real_l translations[][3], const int_g nops) const;
    void map_asym_pos(const int_g asym_idx, const coord_type a[3],
		      int_g &op, coord_type a_coords[3],
		      const coord_type b[][3], const int_g b_asym_idx[],
		      coord_type b_coords[][3], int_g ab_shift[3],
		      const int_g nbatoms, const coord_type la[3],
		      const real_l rotations[][3][3],
		      const real_l translations[][3], const int_g nops) const;
    void map_asym_pos(const int_g asym_idx, const coord_type a[3],
		      int_g &op, coord_type a_coords[3],
		      const coord_type b[][3], const int_g b_asym_idx[],
		      coord_type b_coords[][3], int_g ab_shift[3],
		      const int_g nbatoms, const coord_type la[3],
		      const coord_type lb[3], const real_l rotations[][3][3],
		      const real_l translations[][3], const int_g nops) const;
    void map_asym_pos(const int_g asym_idx, const coord_type a[3],
		      int_g &op, coord_type a_coords[3],
		      const coord_type b[][3], const int_g b_asym_idx[],
		      coord_type b_coords[][3], int_g ab_shift[3],
		      const int_g nbatoms, const coord_type la[3],
		      const coord_type lb[3], const coord_type lc[3],
		      const real_l rotations[][3][3],
		      const real_l translations[][3], const int_g nops) const;
    void map_asym_pos(const int_g asym_idx, const coord_type a[3],
		      int_g &op, coord_type a_coords[3],
		      const coord_type b[][3], const int_g b_asym_idx[],
		      coord_type b_coords[][3], int_g ab_shift[3],
		      const int_g nbatoms, const coord_type la[3],
		      const coord_type lb[3], const coord_type lc[3],
		      const real_l rotations[][3][3],
		      const real_l translations[][3], const int_g nops,
		      const bool surface) const;
    void generate_primitive_copies(const real_l rotations[][3][3],
				   const real_l translations[][3],
				   const int_g nops, const bool tidy);
    void generate_primitive_copies(const real_l a[3],
				   const real_l rotations[][3][3],
				   const real_l translations[][3],
				   const int_g nops, const bool tidy);
    void generate_primitive_copies(const real_l a[3], const real_l b[3],
				   const real_l rotations[][3][3],
				   const real_l translations[][3],
				   const int_g nops, const bool tidy);
    void generate_primitive_copies(const real_l a[3], const real_l b[3],
				   const real_l c[3],
				   const real_l rotations[][3][3],
				   const real_l translations[][3],
				   const int_g nops, const bool tidy);
    virtual void generate_primitive_atoms(const bool tidy = false) = 0;
    virtual void conventional_atoms(const atom_tree &primitive, atom_tree &tree,
				    const bool centre[3]) const = 0;
    virtual void centre_atoms(const atom_tree &primitive, atom_tree &tree,
			      const bool centre[3]) const = 0;
    virtual void update_display_atoms(atom_tree &tree, const real_l tol,
				      const int_g na, const int_g nb,
				      const int_g nc, const bool conventional,
				      const bool centre,
				      const bool edges) const = 0;
    virtual void generate_bonds(const atom_tree &tree, const int_g na,
				const int_g nb, const int_g nc,
				const bool conventional,
				std::list<bond_list> info[],
				const real_g overlap, const bool centre,
				const bool invert_colours,
				const bond_data &bond_info, const real_l tol, 
				const std::vector<real_g (*)[3]> *traj,
				const int_g nframes, const bool gamma,
				const bool edit) const = 0;
    void count_bond_list(const std::list<bond_list> &info, int_l &nbonds1,
			 int_l &nbonds2, int_l &nbonds3, int_l &nvertices,
			 const bool polyh) const;
    void generate_line_bonds(const std::list<bond_list> &info,
			     real_g (* &pos)[3], real_g (* &rgb)[3],
			     int_l &nbonds, int_l * &connects, int_l &nconnects,
			     const int_g btype) const;
    void generate_tube_bonds(const std::list<bond_list> &info,
			     real_g (* &pos)[3], real_g (* &rgb)[3],
			     int_l &nbonds, int_l * &connects, int_l &nconnects,
			     const int_g btype, const real_g radius,
			     const int_g subdiv) const;
    bool generate_polyhedra(const std::list<bond_list> &info,
			    real_g (* &pos)[3], real_g (* &rgb)[3],
			    int_l &nverts, int_l * &connects,
			    int_l &nconnects) const;
    static void transform_coordinates(coord_type cnew[3],
				      const coord_type cold[3],
				      const real_l r[3][3], const real_l t[3]);
    virtual void complete_lattice(coord_type va[3], coord_type vb[3],
				  coord_type vc[3]) = 0;
    virtual void complete_symmetry() = 0;
    virtual void complete_translators(const coord_type va[3],
				      const coord_type vb[3],
				      const coord_type vc[3]) = 0;
    virtual void complete_translators(const coord_type va[3],
				      const coord_type vb[3],
				      const coord_type vc[3],
				      const int_g dim) = 0;
    virtual void get_atom_shifts(const atom_tree &tree,
				 const coord_type coords[][3],
				 int_g shifts[][3], int_g prim_id[],
				 const int_g n) const = 0;
    virtual void map_data(const atom_tree &tree, const real_g grid[][3],
			  int_g **const data, const int_g ndata,
			  const int_g ngrid, real_g (* &map_grid)[3],
			  int_g ** &map_data, int_g &n) const = 0;
    virtual void map_data(const atom_tree &tree, const real_g grid[][3],
			  real_g **const data, const int_g ndata,
			  const int_g ngrid, real_g (* &map_grid)[3],
			  real_g ** &map_data, int_g &n) const = 0;
    virtual void map_data(const atom_tree &tree, const real_g grid[][3],
			  const real_g mag[][3], const real_g phases[][3],
			  const int_g ngrid, real_g (* &map_grid)[3],
			  real_g (* &map_data)[3], int_g &n, const real_g ka,
			  const real_g kb, const real_g kc) const = 0;
    virtual void map_data(const atom_tree &tree, const real_g grid[][3],
			  const real_g mags[][3], const real_g phases[][3],
			  const int_g ngrid,
			  std::vector<real_g (*)[3]> &new_data,
			  int_g &n, const int_g nframes, const int_g ncopies,
			  const real_g ka, const real_g kb,
			  const real_g kc, const real_g scale) const = 0;
    virtual void map_data(const atom_tree &tree, const real_g grid[][3],
			  const int_g ngrid,
			  const std::vector<real_g (*)[3]> &traj,
			  const int_g nframes,
			  std::vector<real_g (*)[3]> &new_data) const = 0;
    virtual bool map_atom_selections(const atom_tree &display,
				     const atom_tree &prim,
				     const real_g grid[][3], int_g **const data,
				     const int_g ndata, const int_g ngrid,
				     int_g * &labels, int_g * &atom_types,
				     atom * &parents, int_g &n,
				     const bool all_atoms) const = 0;
    virtual bool map_selected_atoms(const atom_tree &display, int_g &n,
				    int_g * &labels, int_g (* &shifts)[3],
				    const real_g grid[][3], int_g **const data,
				    const int_g ndata,
				    const int_g ngrid) const = 0;
    int_g get_atom_selection_info(real_l &x, real_l &y, real_l &z,
				real_l &length, real_l &x2, real_l &y2,
				real_l &z2, real_l &angle, char sym1[],
				char sym2[], char sym3[], real_g &r1,
				real_g &r2, real_g &r3, string &gp,
				string &label) const;
    int_g get_number_selected_atoms() const; 
    bool get_selected_positions(coord_type p[3]) const;
    bool get_selected_positions(coord_type p1[3], coord_type p2[3]) const;
    bool get_selected_positions(coord_type p1[3], coord_type p2[3],
				coord_type p3[3]) const;
    bool get_average_selection_position(coord_type p[3]) const;

    void generate_slab_layers(const model *threed, const real_g tol,
			      const int_g term, const int_g numl,
			      int_g &layers);
    void generate_slab_layers(const model *threed, const real_g tol,
			      const int_g numl, int_g &layers);
    void add_to_slab(const model *threed, const real_g tol, const int_g numl,
		     int_g &layers);
    int_g sort_slab(const real_g tol, int_g layer_index[], int_g order[],
		  real_l zbuff[], const int_g natoms) const;
    bool generate_surface(const model *m, const int_g h, const int_g k,
			  const int_g l, const bool conventional,
			  const real_g tol, string * &labels, int_g &n,
			  real_l r[3][3]);
    void generate_surface_layers(const model *threed, const real_g tol,
				 const int_g term);
    void generate_salvage(const model *m, const real_g tol, const int_g numl);
    void add_to_salvage(const model *m, const real_g tol, const int_g numl,
			const int_g prevl);
    int_g sort_salvage(const real_g tol, int_g layer_index[], int_g order[],
		     real_l zbuff[], const int_g natoms) const;
    virtual int_g get_salvage_layers() const;
    virtual void set_salvage_layers(const int_g l);
    void surface_vectors(const model_base *sym, const int_g h, const int_g k,
			 const int_g l, const bool conventional,
			 coord_type rs1[3], coord_type rs2[3],
			 coord_type rs3[3]) const;
    static void rotate_lattice(coord_type a[3], coord_type b[3],
			       coord_type c[3], coord_type r[3][3]);
    int_g identify_layers(const real_g tol, const real_g zshift,
			  const int_g term, real_l &shift, string * &labels,
			  int_g &n, const bool str = true) const;
    int_g sort_layers(const real_g tol, const real_g zshift, const int_g term,
		    real_l &shift, int_g layer_index[], int_g order[],
		    real_l zbuff[], const int_g natoms) const;
    virtual const symmetry_data &get_symmetry() const = 0;
    virtual void generate_wulff_data(const std::list<class wulff_data> &planes,
				     string labels[], real_g rgb[][3],
				     real_l normals[][3], int_g &nnormals,
				     real_l (* &myverts)[3],
				     int_g ** &face_vertices,
				     int_g * &nface_vertices, int_g &nfaces,
				     int_g &nvertices) const;
    real_g calc_plane_offset(const model *m, const wulff_data &data,
			     const real_l verts[][3],
			     const int_g face_vertices[],
			     const int_g nface_vertices) const;

#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type redraw_atoms(const render_parent *p,
				char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS
    void generate_atom_display_list(const int_g na, const int_g nb,
				    const int_g nc, const bool centre_cell,
				    const bool conv_cell, const bool edges,
				    const real_l tol, int_g &natoms1,
				    int_g &natoms2, int_g &natoms3,
				    const int_g select_type,
				    const int_g sym_select,
				    const bool use_flags,
				    const int_g flag_type);

  private:
    // Todo - ?
    std::map<int, atom> atom_basis;
    atom_tree primitive_atoms;
    bond_data bonds;
    atom_tree display_atoms;
    // Todo - pair info
    std::list<string> group_names;
#ifdef ENABLE_DLV_GRAPHICS
    model_display_r *render_rspace;
    model_display_k *render_kspace;
    class data_object *atom_data;
#endif // ENABLE_DLV_GRAPHICS
    bool use_edit_flags;

    string find_atom_group(const int_g index) const;
    void generate_conventional_atom_display(atom_tree &tree,
					    const bool centre[3]);
    void generate_primitive_atom_display(atom_tree &tree,
					 const bool centre[3]);
    static void build_triangles(polyhedron &data, real_g pos[][3],
				const real_g centre[3]);

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void load(Archive &ar, const unsigned int version);
    template <class Archive>
    void save(Archive &ar, const unsigned int version) const;

    BOOST_SERIALIZATION_SPLIT_MEMBER()
#endif // DLV_USES_SERIALIZE
  };

  template <class sym> class model_impl : public model_base {
  public:
    model_impl(const string model_name);

    int_g get_number_of_sym_ops() const;
    void get_cart_rotation_operators(real_l r[][3][3], const int_g nops) const;
    void get_cart_translation_operators(real_l t[][3], const int_g nops) const;
    void get_frac_translation_operators(real_l t[][3], const int_g nops) const;
    void get_point_group(string &base, int_g &n, string &tail) const;
    void set_lattice_and_centre(const int_g l, const int_g c);

  protected:
    void set_crystal03_lattice(const int_g lattice, const int_g centre);
    bool set_group(const int_g gp);
    bool set_pt_group(const char gp[]);
    bool set_group(const char gp[], const int_g setting = 0,
		   const int_g nsettings = 1, const bool hex = false);
    int_g get_number_no_trans_ops() const;
    void get_cart_no_trans_ops(real_l r[][3][3], const int_g nops) const;
    bool set_operators(const string ops[], const int_g nops,
		       const int_g gp, const bool rhomb, const bool prim);
    bool set_cartesian_sym_ops(const real_l rotations[][3][3],
			       const real_l translations[][3],
			       const int_g nops);
    bool set_fractional_trans_ops(const real_l rotations[][3][3],
				  const real_l translations[][3],
				  const int_g nops);
    void copy_symmetry(const model_impl *m, const bool frac);
    void rotate_symmetry(const model_impl *m, const real_l r[3][3]);
    void copy_lattice_type(const model_impl *m);
    void add_conv_cell_operators(const model_impl *m, const coord_type va[3],
				 const coord_type vb[3],
				 const coord_type vc[3], const int_g dim);
    void build_supercell_operators(const coord_type opa[3],
				   const coord_type opb[3],
				   const coord_type opc[3],
				   const coord_type va[3],
				   const coord_type vb[3],
				   const coord_type vc[3], const int_g dim);
    void shift_ops_cartesian(const symmetry_data &sym_ops, const real_l x,
			     const real_l y, const real_l z,
			     const coord_type a[3], const coord_type b[3],
			     const coord_type c[3], const int_g dim);
    void shift_ops_fractional(const symmetry_data &sym_ops, const real_l x,
			      const real_l y, const real_l z,
			      const coord_type a[3], const coord_type b[3],
			      const coord_type c[3], const int_g dim);
    bool get_min_trans_origin(real_l p[3], const coord_type a[3],
			      const coord_type b[3], const coord_type c[3],
			      const int_g dim) const;

    void get_cartesian_ops(real_l rotations[][3][3],
			   real_l translations[][3], const int_g nops) const;
    void get_transform_to_prim_cell(coord_type t[3][3]) const;
    void get_transform_to_prim_cell(int_g t[3][3]) const;
    void get_transform_to_conv_cell(coord_type t[3][3]) const;
    int_g get_number_conv_cell_translators() const;
    void get_conv_cell_translators(real_l translators[][3]) const;
    int_g lattice_type() const;
    int_g get_centre_type() const;
    void complete_symmetry();
    void complete_translators(const coord_type va[3], const coord_type vb[3],
			      const coord_type vc[3]);
    void complete_translators(const coord_type va[3], const coord_type vb[3],
			      const coord_type vc[3], const int_g dim);
    const symmetry_data &get_symmetry() const;

  private:
    sym symmetry;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  // Todo - do we need this?
  template <class sym> class periodic_model : public model_impl<sym> {
  public:
    periodic_model(const string model_name);

  protected:
    bool has_valid_vectors() const;
    bool has_valid_parameters() const;
    void set_valid_vectors();
    void set_valid_parameters();

    bool set_lattice(const coord_type a[3]);
    bool set_lattice(const coord_type a[3], const coord_type b[3]);
    bool set_lattice(const coord_type a[3], const coord_type b[3],
		     const coord_type c[3]);
    bool set_parameters(const coord_type a);
    bool set_parameters(const coord_type a, const coord_type b,
			const coord_type alpha);
    bool set_parameters(const coord_type a, const coord_type b,
			const coord_type c, const coord_type alpha,
			const coord_type beta, const coord_type gamma);
    void get_primitive_vectors(coord_type lattice[3][3]) const;
    void get_parameters(coord_type &a) const;
    void get_parameters(coord_type &a, coord_type &b, coord_type &alpha) const;
    void get_parameters(coord_type &a, coord_type &b, coord_type &c,
			coord_type &alpha, coord_type &beta,
			coord_type &gamma) const;
    
  private:
    // each vector is row of matrix
    coord_type primitive_lattice[3][3];
    coord_type param_a;
    coord_type param_b;
    coord_type param_c;
    coord_type param_alpha;
    coord_type param_beta;
    coord_type param_gamma;
    bool valid_vectors;
    bool valid_parameters;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  model *create_molecule(const string model_name);
  model *create_polymer(const string model_name);
  model *create_slab(const string model_name);
  model *create_crystal(const string model_name);
  model *create_surface(const string model_name);

}

#ifdef DLV_USES_SERIALIZE
BOOST_SERIALIZATION_ASSUME_ABSTRACT(DLV::model_base)
#endif // DLV_USES_SERIALIZE

inline DLV::model_base::model_base(const string model_name)
  : model(model_name),
#ifdef ENABLE_DLV_GRAPHICS
    render_rspace(0), render_kspace(0), atom_data(0),
#endif // ENABLE_DLV_GRAPHICS
    use_edit_flags(false){
}

inline void DLV::model_base::copy_atom_groups(const model_base *m)
{
  group_names = m->group_names;
}

template <class sym> DLV::model_impl<sym>::model_impl(const string model_name)
  : model_base(model_name)
{
}

template <class sym>
void DLV::model_impl<sym>::get_point_group(string &base, int_g &n,
					   string &tail) const
{
  symmetry.find_point_group(base, n, tail);
}

// Since we aren't using a pointer to access the virtual these can't specialize
template <class sym> bool DLV::model_impl<sym>::set_group(const int_g gp)
{
  return symmetry.set_group(gp);
}

template <class sym> bool DLV::model_impl<sym>::set_pt_group(const char gp[])
{
  return symmetry.set_point_group(gp);
}

template <class sym> bool DLV::model_impl<sym>::set_group(const char gp[],
							  const int_g setting,
							  const int_g nsettings,
							  const bool)
{
  char label[64];
  strcpy(label, gp);
  if (nsettings > 1) {
    if (setting == 1)
      strcat(label, ":1");
    else
      strcat(label, ":2");
  } else {
    if (gp[0] == 'R')
      strcat(label, ":r");
  }
  return symmetry.set_group(label);
}

template <class sym> bool
DLV::model_impl<sym>::set_operators(const string ops[], const int_g nops,
				    const int_g gp, const bool rhomb,
				    const bool prim)
{
  return symmetry.parse_operators(ops, nops, gp, rhomb, prim);
}

template <class sym> bool
DLV::model_impl<sym>::set_cartesian_sym_ops(const real_l rotations[][3][3],
					    const real_l translations[][3],
					    const int_g nops)
{
  return symmetry.set_cartesian_ops(rotations, translations, nops);
}

template <class sym> bool
DLV::model_impl<sym>::set_fractional_trans_ops(const real_l rotations[][3][3],
					       const real_l translations[][3],
					       const int_g nops)
{
  return symmetry.set_fractional_trans_ops(rotations, translations, nops);
}

template <class sym>
inline const DLV::symmetry_data &DLV::model_impl<sym>::get_symmetry() const
{
  return symmetry;
}

template <class sym> int
DLV::model_impl<sym>::get_number_of_sym_ops() const
{
  return symmetry.get_number_of_sym_ops();
}

template <class sym> void
DLV::model_impl<sym>::get_cartesian_ops(real_l rotations[][3][3],
					real_l translations[][3],
					const int_g nops) const
{
  symmetry.get_cartesian_ops(rotations, translations, nops);
}

template <class sym> void
DLV::model_impl<sym>::get_cart_rotation_operators(real_l r[][3][3],
						  const int_g nops) const
{
  symmetry.get_cartesian_rotations(r, nops);
}

template <class sym> void
DLV::model_impl<sym>::get_cart_translation_operators(real_l t[][3],
						     const int_g nops) const
{
  symmetry.get_cartesian_translations(t, nops);
}

template <class sym> void
DLV::model_impl<sym>::get_frac_translation_operators(real_l t[][3],
						     const int_g nops) const
{
  symmetry.get_fractional_translations(t, nops);
}

template <class sym> int
DLV::model_impl<sym>::get_number_no_trans_ops() const
{
  return symmetry.get_number_no_trans_ops();
}

template <class sym> void
DLV::model_impl<sym>::get_cart_no_trans_ops(real_l r[][3][3],
					    const int_g nops) const
{
  symmetry.get_cartesian_no_trans_ops(r, nops);
}

template <class sym>
void DLV::model_impl<sym>::set_crystal03_lattice(const int_g lattice,
						 const int_g centre)
{
  symmetry.set_crystal03_lattice(lattice, centre);
}

template <class sym> void
DLV::model_impl<sym>::get_transform_to_prim_cell(coord_type t[3][3]) const
{
  symmetry.get_transform_to_prim_cell(t);
}

template <class sym> void
DLV::model_impl<sym>::get_transform_to_prim_cell(int_g t[3][3]) const
{
  symmetry.get_transform_to_prim_cell(t);
}

template <class sym> void
DLV::model_impl<sym>::get_transform_to_conv_cell(coord_type t[3][3]) const
{
  symmetry.get_transform_to_conv_cell(t);
}

template <class sym>
DLV::int_g DLV::model_impl<sym>::get_number_conv_cell_translators() const
{
  return symmetry.get_number_conv_cell_translators();
}

template <class sym> void
DLV::model_impl<sym>::get_conv_cell_translators(real_l ops[][3]) const
{
  symmetry.get_conv_cell_translators(ops);
}

template <class sym> DLV::int_g DLV::model_impl<sym>::lattice_type() const
{
  return symmetry.get_lattice_type();
}

template <class sym> DLV::int_g DLV::model_impl<sym>::get_centre_type() const
{
  return symmetry.get_centre_type();
}

template <class sym>
void DLV::model_impl<sym>::copy_symmetry(const model_impl *m, const bool frac)
{
  if (frac)
    symmetry.copy_fractional_ops(m->symmetry);
  else
    symmetry.copy_cartesian_ops(m->symmetry);
}

template <class sym>
void DLV::model_impl<sym>::rotate_symmetry(const model_impl *m,
					   const real_l r[3][3])
{
  symmetry.rotate_cartesian_ops(m->symmetry, r);
}

template <class sym>
void DLV::model_impl<sym>::add_conv_cell_operators(const model_impl *m,
						   const coord_type va[3],
						   const coord_type vb[3],
						   const coord_type vc[3],
						   const int_g dim)
{
  symmetry.add_conv_cell_ops(m->symmetry, va, vb, vc, dim);
}

template <class sym>
void DLV::model_impl<sym>::build_supercell_operators(const coord_type opa[3],
						     const coord_type opb[3],
						     const coord_type opc[3],
						     const coord_type va[3],
						     const coord_type vb[3],
						     const coord_type vc[3],
						     const int_g dim)
{
  symmetry.build_supercell_ops(opa, opb, opc, va, vb, vc, dim);
}

template <class sym>
void DLV::model_impl<sym>::shift_ops_cartesian(const symmetry_data &sym_ops,
					       const real_l x, const real_l y,
					       const real_l z,
					       const coord_type a[3],
					       const coord_type b[3],
					       const coord_type c[3],
					       const int_g dim)
{
  symmetry.shift_translations(sym_ops, x, y, z, a, b, c, dim, false);
}

template <class sym>
void DLV::model_impl<sym>::shift_ops_fractional(const symmetry_data &sym_ops,
						const real_l x, const real_l y,
						const real_l z,
						const coord_type a[3],
						const coord_type b[3],
						const coord_type c[3],
						const int_g dim)
{
  symmetry.shift_translations(sym_ops, x, y, z, a, b, c, dim, true);
}

template <class sym>
bool DLV::model_impl<sym>::get_min_trans_origin(real_l p[3],
						const coord_type a[3],
						const coord_type b[3],
						const coord_type c[3],
						const int_g dim) const
{
  return symmetry.minimise_translation_ops(p, a, b, c, dim);
}

template <class sym>
void DLV::model_impl<sym>::set_lattice_and_centre(const int_g l, const int_g c)
{
  symmetry.set_lattice_and_centre(l, c);
}

template <class sym>
void DLV::model_impl<sym>::copy_lattice_type(const model_impl *m)
{
  symmetry.copy_lattice_type(m->symmetry);
}

template <class sym> void DLV::model_impl<sym>::complete_symmetry()
{
  symmetry.complete();
}

template <class sym>
void DLV::model_impl<sym>::complete_translators(const coord_type va[3],
						const coord_type vb[3],
						const coord_type vc[3])
{
  symmetry.complete_translators(va, vb, vc);
}

template <class sym>
void DLV::model_impl<sym>::complete_translators(const coord_type va[3],
						const coord_type vb[3],
						const coord_type vc[3],
						const int_g dim)
{
  symmetry.generate_translations(va, vb, vc, dim);
}

template <class sym>
DLV::periodic_model<sym>::periodic_model(const string model_name)
  : model_impl<sym>(model_name), valid_vectors(false), valid_parameters(false)
{
}

template <class sym> void
DLV::periodic_model<sym>::get_primitive_vectors(coord_type lattice[3][3]) const
{
  for (int_g i = 0; i < 3; i++)
    for (int_g j = 0; j < 3; j++)
      lattice[i][j] = primitive_lattice[i][j];
}

template <class sym>
bool DLV::periodic_model<sym>::set_lattice(const coord_type a[3])
{
  for (int_g i = 0; i < 3; i++)
    primitive_lattice[0][i] = a[i];
  valid_vectors = true;
  valid_parameters = false;
  return true;
}

template <class sym>
bool DLV::periodic_model<sym>::set_parameters(const coord_type a)
{
  param_a = a;
  valid_vectors = false;
  valid_parameters = true;
  return true;
}

template <class sym>
bool DLV::periodic_model<sym>::set_lattice(const coord_type a[3],
					   const coord_type b[3])
{
  for (int_g i = 0; i < 3; i++) {
    primitive_lattice[0][i] = a[i];
    primitive_lattice[1][i] = b[i];
  }
  valid_vectors = true;
  valid_parameters = false;
  return true;
}

template <class sym>
bool DLV::periodic_model<sym>::set_parameters(const coord_type a,
					      const coord_type b,
					      const coord_type alpha)
{
  param_a = a;
  param_b = b;
  param_alpha = alpha;
  valid_vectors = false;
  valid_parameters = true;
  return true;
}

template <class sym>
bool DLV::periodic_model<sym>::set_lattice(const coord_type a[3],
					   const coord_type b[3],
					   const coord_type c[3])
{
  for (int_g i = 0; i < 3; i++) {
    primitive_lattice[0][i] = a[i];
    primitive_lattice[1][i] = b[i];
    primitive_lattice[2][i] = c[i];
  }
  valid_vectors = true;
  valid_parameters = false;
  return true;
}

template <class sym>
bool DLV::periodic_model<sym>::set_parameters(const coord_type a,
					      const coord_type b,
					      const coord_type c,
					      const coord_type alpha,
					      const coord_type beta,
					      const coord_type gamma)
{
  param_a = a;
  param_b = b;
  param_c = c;
  param_alpha = alpha;
  param_beta = beta;
  param_gamma = gamma;
  valid_vectors = false;
  valid_parameters = true;
  return true;
}

template <class sym> bool DLV::periodic_model<sym>::has_valid_vectors() const
{
  return valid_vectors;
}

template <class sym>
bool DLV::periodic_model<sym>::has_valid_parameters() const
{
  return valid_parameters;
}

template <class sym> void DLV::periodic_model<sym>::set_valid_vectors()
{
  valid_vectors = true;
}

template <class sym> void DLV::periodic_model<sym>::set_valid_parameters()
{
  valid_parameters = true;
}

template <class sym>
void DLV::periodic_model<sym>::get_parameters(coord_type &a) const
{
  a = param_a;
}

template <class sym>
void DLV::periodic_model<sym>::get_parameters(coord_type &a, coord_type &b,
					      coord_type &alpha) const
{
  a = param_a;
  b = param_b;
  alpha = param_alpha;
}

template <class sym>
void DLV::periodic_model<sym>::get_parameters(coord_type &a, coord_type &b,
					      coord_type &c, coord_type &alpha,
					      coord_type &beta,
					      coord_type &gamma) const
{
  a = param_a;
  b = param_b;
  c = param_c;
  alpha = param_alpha;
  beta = param_beta;
  gamma = param_gamma;
}

#endif // DLV_MODEL_IMPLEMENTATIONS
