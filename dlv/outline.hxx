
#ifndef DLV_OUTLINE_SHELL
#define DLV_OUTLINE_SHELL

namespace DLV {

  struct plane_vertex {
    coord_type x;
    coord_type y;
    coord_type z;

    plane_vertex();
    plane_vertex(const coord_type c[3]);

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  struct cylinder_data {
    coord_type inner_radius;
    coord_type outer_radius;
    coord_type min_p;
    coord_type max_p;
    int_g orientation;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  struct sphere_data {
    coord_type radius;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class outline_model : public model {
  public:
    outline_model(const string model_name);
    ~outline_model();

    void add_polyhedra();
    bool add_polygon(coord_type c[][3], const int_g n,
		     char message[], const int_g mlen);
    void add_cylinder(const coord_type r1, const coord_type r2,
		      const coord_type zmin, const coord_type zmax,
		      const int_g o);
    void add_sphere(const coord_type radius);
    void complete(const bool only_atoms = false);

    int_g get_lattice_type() const;
    int_g get_lattice_centring() const;
    int_g get_hermann_mauguin_group(string &name) const;
    void get_point_group(string &base, int_g &n, string &tail) const;
    int_g get_number_of_sym_ops() const;
    void get_cart_rotation_operators(real_l r[][3][3], const int_g nops) const;
    void get_frac_rotation_operators(real_l r[][3][3], const int_g nops) const;
    void get_cart_translation_operators(real_l t[][3], const int_g nops) const;
    void get_frac_translation_operators(real_l t[][3], const int_g nops) const;
    int_g get_number_of_asym_atoms() const;
    int_g get_number_of_primitive_atoms() const;
    void get_asym_atom_types(int_g atom_types[], const int_g n) const;
    void get_primitive_atom_types(int_g atom_types[], const int_g n) const;
    void get_asym_atom_cart_coords(coord_type coords[][3], const int_g n) const;
    void get_asym_atom_frac_coords(coord_type coords[][3], const int_g n) const;
    void get_primitive_atom_cart_coords(coord_type coords[][3],
					const int_g n) const;
    void get_primitive_atom_frac_coords(coord_type coords[][3],
					const int_g n) const;

    void set_crystal03_lattice_type(const int_g lattice, const int_g centre);

    void get_conventional_lattice(coord_type a[3], coord_type b[3],
				  coord_type c[3]) const;
    void get_reciprocal_lattice(coord_type a[3], coord_type b[3],
				coord_type c[3]) const;

    const std::map<int, std::list<std::list<plane_vertex> > >
    &get_polyhedra() const;
    const std::map<int, cylinder_data> &get_cylinders() const;
    const std::map<int, sphere_data> &get_spheres() const;

#ifdef ENABLE_DLV_GRAPHICS
    render_parent *create_render_parent(render_parent *p, const bool copy_cell,
					const bool copy_wavefn);
    DLVreturn_type render_r(render_parent *p, const model *m,
			    char message[], const int_g mlen);
    DLVreturn_type render_k(render_parent *p, char message[], const int_g mlen);
    class toolkit_obj get_r_ui_obj() const;

    DLVreturn_type update_atoms(const render_parent *p,
				char message[], const int_g mlen);
    DLVreturn_type update_bonds(const render_parent *p,
				char message[], const int_g mlen) const;
    DLVreturn_type add_bond(render_parent *p, char message[], const int_g mlen);
    DLVreturn_type del_bond(render_parent *p, char message[], const int_g mlen);
    DLVreturn_type add_bond_type(render_parent *p,
				 char message[], const int_g mlen);
    DLVreturn_type del_bond_type(render_parent *p,
				 char message[], const int_g mlen);
    DLVreturn_type add_bond_all(render_parent *p,
				char message[], const int_g mlen);
    DLVreturn_type del_bond_all(render_parent *p,
				char message[], const int_g mlen);
    DLVreturn_type select_atom(render_parent *p, const real_g coords[3],
			       const int_g status, int_g &nselects,
			       char message[], const int_g mlen);
    DLVreturn_type deselect_all_atoms(render_parent *p,
				      char message[], const int_g mlen);
    DLVreturn_type update_outline(render_parent *p,
				  char message[], const int_g mlen);
    DLVreturn_type update_cell(render_parent *p,
			       char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  protected:
    void duplicate(outline_model *m) const;
    bool set_group(const int_g gp);
    bool set_group(const char gp[], const int_g setting = 0,
		   const int_g nsettings = 1, const bool hex = false);
    bool set_cartesian_sym_ops(const real_l rotations[][3][3],
			       const real_l translations[][3],
			       const int_g nops);
    bool set_fractional_sym_ops(const real_l rotations[][3][3],
				const real_l translations[][3],
				const int_g nops);
    bool set_primitive_lattice(const coord_type [3], const coord_type [3],
			       const coord_type [3]);
    bool set_lattice_parameters(const coord_type a, const coord_type b,
				const coord_type c, const coord_type alpha,
				const coord_type beta, const coord_type gamma);
    bool add_cartesian_atom(int_g &index, const int_g atomic_number,
			    const coord_type coords[3]);
    bool add_fractional_atom(int_g &index, const int_g atomic_number,
			     const coord_type coords[3], const bool rhomb,
			     const bool prim);
    bool add_cartesian_atom(int_g &index, const char symbol[],
			    const coord_type coords[3]);
    bool add_fractional_atom(int_g &index, const char symbol[],
			     const coord_type coords[3], const bool rhomb,
			     const bool prim);

  private:
    std::map<int, std::list<std::list<plane_vertex> > > polyhedra;
    std::map<int, cylinder_data> cylinders;
    std::map<int, sphere_data> spheres;
    // cone?

#ifdef ENABLE_DLV_GRAPHICS
    model_outline_r *render_object;
#endif // ENABLE_DLV_GRAPHICS

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class outline_molecule : public outline_model {
  public:
    outline_molecule(const string model_name);
    model *duplicate_model(const string label) const;

    int_g get_number_of_periodic_dims() const;
    void get_primitive_lattice(coord_type a[3], coord_type b[3],
			       coord_type c[3]) const;

    int_g get_model_type() const;
    void generate_transforms(const int_g na, const int_g nb, const int_g nc,
			     const int_g sa, const int_g sb, const int_g sc,
			     const bool centre_cell, const bool conventional,
			     int_g &ntransforms,
			     real_g (* &transforms)[3]) const;

#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type update_lattice(render_parent *p, const bool kspace,
				  char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  private:
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class outline_oneD : public outline_model {
  public:
    outline_oneD(const string model_name, float a1, int dir);
    model *duplicate_model(const string label) const;

    int_g get_number_of_periodic_dims() const;
    void get_primitive_lattice(coord_type a[3], coord_type b[3],
			       coord_type c[3]) const;

    int_g get_model_type() const;
    void generate_transforms(const int_g na, const int_g nb, const int_g nc,
			     const int_g sa, const int_g sb, const int_g sc,
			     const bool centre_cell, const bool conventional,
			     int_g &ntransforms,
			     real_g (* &transforms)[3]) const;

#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type update_lattice(render_parent *p, const bool kspace,
				  char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  private:
    outline_oneD(const string label, const float vector[3]);
    float vector[3];

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  
  class outline_twoD : public outline_model {
  public:
    outline_twoD(const string model_name, float a1, float a2, int dir,
		 int Lat_type);
    model *duplicate_model(const string label) const;

    int_g get_number_of_periodic_dims() const;
    void get_primitive_lattice(coord_type a[3], coord_type b[3],
			       coord_type c[3]) const;
   
    int_g get_model_type() const;
    void generate_transforms(const int_g na, const int_g nb, const int_g nc,
			     const int_g sa, const int_g sb, const int_g sc,
			     const bool centre_cell, const bool conventional,
			     int_g &ntransforms,
			     real_g (* &transforms)[3]) const;
   
#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type update_lattice(render_parent *p, const bool kspace,
				  char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS
   
 private:
    outline_twoD(const string label, const float vector[2][3]);
    float vector[2][3]; 

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };
 
  class outline_threeD : public outline_model {
  public:
    outline_threeD(const string model_name, float a1, float a2, float a3,
		   int dir, int Lat_type);
    model *duplicate_model(const string label) const;

    int_g get_number_of_periodic_dims() const;
    void get_primitive_lattice(coord_type a[3], coord_type b[3],
			       coord_type c[3]) const;

    int_g get_model_type() const;
    void generate_transforms(const int_g na, const int_g nb, const int_g nc,
			     const int_g sa, const int_g sb, const int_g sc,
			     const bool centre_cell, const bool conventional,
			     int_g &ntransforms,
			     real_g (* &transforms)[3]) const;

#ifdef ENABLE_DLV_GRAPHICS
    DLVreturn_type update_lattice(render_parent *p, const bool kspace,
				  char message[], const int_g mlen);
#endif // ENABLE_DLV_GRAPHICS

  private:
    outline_threeD(const string label, const float vector[3][3]);
    float vector[3][3];

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };
}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(DLV::outline_model)
BOOST_CLASS_EXPORT_KEY(DLV::outline_molecule)
BOOST_CLASS_EXPORT_KEY(DLV::outline_oneD)
BOOST_CLASS_EXPORT_KEY(DLV::outline_twoD)
BOOST_CLASS_EXPORT_KEY(DLV::outline_threeD)
#endif // DLV_USES_SERIALIZE

inline DLV::plane_vertex::plane_vertex()
  : x(0.0), y(0.0), z(0.0)
{
}

inline DLV::plane_vertex::plane_vertex(const coord_type c[3])
  : x(c[0]), y(c[1]), z(c[2])
{
}

inline DLV::outline_model::outline_model(const string model_name)
  : model(model_name)
#ifdef ENABLE_DLV_GRAPHICS
  , render_object(0)
#endif // ENABLE_DLV_GRAPHICS
{
}

inline const std::map<int, std::list<std::list<DLV::plane_vertex> > > &
DLV::outline_model::get_polyhedra() const
{
  return polyhedra;
}

inline const std::map<int, DLV::cylinder_data> &
DLV::outline_model::get_cylinders() const
{
  return cylinders;
}

inline const std::map<int, DLV::sphere_data> &
DLV::outline_model::get_spheres() const
{
  return spheres;
}

inline DLV::outline_molecule::outline_molecule(const string model_name)
  : outline_model(model_name)
{
}

inline DLV::outline_oneD::outline_oneD(const string label, const float v[3])
  : outline_model(label)
{
  vector[0] = v[0];
  vector[1] = v[1];
  vector[2] = v[2];
}


inline DLV::outline_twoD::outline_twoD(const string label, const float v[2][3])
  : outline_model(label)
{
  vector[0][0] = v[0][0];
  vector[0][1] = v[0][1];
  vector[0][2] = v[0][2];
  vector[1][0] = v[1][0];
  vector[1][1] = v[1][1];
  vector[1][2] = v[1][2];
}

inline DLV::outline_threeD::outline_threeD(const string label,
					   const float v[3][3])
  : outline_model(label)
{
  vector[0][0] = v[0][0];
  vector[0][1] = v[0][1];
  vector[0][2] = v[0][2];
  vector[1][0] = v[1][0];
  vector[1][1] = v[1][1];
  vector[1][2] = v[1][2];
  vector[2][0] = v[2][0];
  vector[2][1] = v[2][1];
  vector[2][2] = v[2][2];
}

#endif // DLV_OUTLINE_SHELL
