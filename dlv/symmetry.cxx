
#include "types.hxx"
#include "boost_lib.hxx"
#include "constants.hxx"
#include "math_fns.hxx"
#include "cctbx/sgtbx/rot_mx.h"
#include "cctbx/sgtbx/space_group.h"
#include "cctbx/sgtbx/space_group_type.h"
#include "cctbx/sgtbx/symbols.h"
#include "symmetry.hxx"
#include "symmetry_impl.hxx"

namespace {
  DLV::real_l trans_tol = 0.00001;
  DLV::real_l rot_tol = 0.001;
}

static DLV::int_g get_one_or_zero(const DLV::real_l r);
static bool is_identity(const DLV::real_l r[3][3]);

static DLV::int_g get_one_or_zero(const DLV::real_l r)
{
  DLV::int_g v = 0;
  if (r > DLV::real_l(1.0) - rot_tol)
    v = 1;
  else if (r < DLV::real_l(-1.0) + rot_tol)
    v = -1;  
  return v;
}

static bool is_identity(const DLV::real_l r[3][3])
{
  bool ok = true;
  for (DLV::int_g i = 0; i < 3; i++) {
    for (DLV::int_g j = 0; j < 3; j++) {
      if (i == j) {
	if (DLV::abs(r[i][i]) > DLV::real_l(1.0) - rot_tol and
	    DLV::abs(r[i][i]) < DLV::real_l(1.0) + rot_tol)
	  ok = false;
      } else {
	if (DLV::abs(r[i][j]) > rot_tol)
	  ok = false;
      }
    }
  }
  return ok;
}

DLV::symmetry_data::~symmetry_data()
{
  delete [] sym_ops;
}

DLV::int_g DLV::symmetry_data::get_number_of_sym_ops() const
{
  return number_of_sym_ops;
}

void DLV::symmetry_data::init_number_of_ops(const int_g nops)
{
  if (sym_ops != 0)
    delete [] sym_ops;
  number_of_sym_ops = nops;
  sym_ops = new symmetry_operator[nops];
}

bool DLV::symmetry_data::set_cartesian_ops(const real_l rotations[][3][3],
					   const real_l translations[][3],
					   const int_g nops)
{
  if (number_of_sym_ops != nops)
    init_number_of_ops(nops);
  for (int_g i = 0; i < nops; i++)
    sym_ops[i].set_cartesian(rotations[i], translations[i]);
  cartesian_translators = true;
  fractional_translators = false;
  return true;
}

bool
DLV::symmetry_data::set_fractional_trans_ops(const real_l rotations[][3][3],
					     const real_l translations[][3],
					     const int_g nops)
{
  if (number_of_sym_ops != nops)
    init_number_of_ops(nops);
  for (int_g i = 0; i < nops; i++) {
    sym_ops[i].set_cartesian(rotations[i], translations[i]);
    sym_ops[i].set_fractional_translation(translations[i]);
  }
  cartesian_translators = false;
  fractional_translators = true;
  return true;
}

DLV::int_g DLV::symmetry_data::get_number_no_trans_ops() const
{
  int_g nops = 0;
  for (int_g i = 0; i < number_of_sym_ops; i++)
    if (!sym_ops[i].has_translation)
      nops++;
  return nops;
}

void DLV::symmetry_data::get_cartesian_no_trans_ops(real_l r[][3][3],
						    const int_g nops) const
{
  int_g n = 0;
  for (int_g i = 0; i < number_of_sym_ops; i++) {
    if (!sym_ops[i].has_translation) {
      r[n][0][0] = sym_ops[i].rotation[0][0];
      r[n][0][1] = sym_ops[i].rotation[0][1];
      r[n][0][2] = sym_ops[i].rotation[0][2];
      r[n][1][0] = sym_ops[i].rotation[1][0];
      r[n][1][1] = sym_ops[i].rotation[1][1];
      r[n][1][2] = sym_ops[i].rotation[1][2];
      r[n][2][0] = sym_ops[i].rotation[2][0];
      r[n][2][1] = sym_ops[i].rotation[2][1];
      r[n][2][2] = sym_ops[i].rotation[2][2];
      n++;
    }
    if (n == nops)
      break;
  }
}

void DLV::symmetry_data::set_fractional_operator(const real_l r[3][3],
						 const real_l t[3],
						 const rotation_type rot_id,
						 const reflection_type ref_id,
						 const int_g index)
{
  sym_ops[index].set_fractional(r, t, rot_id, ref_id);
  //if (cartesian_translators)
  //  return false;
  fractional_translators = true;
  cartesian_translators = false;
  //return true;
}

void DLV::symmetry_data::copy_cartesian_ops(const symmetry_data &sym)
{
  init_number_of_ops(sym.number_of_sym_ops);
  for (int_g i = 0; i < number_of_sym_ops; i++)
    sym_ops[i] = sym.sym_ops[i];
  cartesian_translators = true;
  fractional_translators = false;
}

void DLV::symmetry_data::copy_fractional_ops(const symmetry_data &sym)
{
  init_number_of_ops(sym.number_of_sym_ops);
  for (int_g i = 0; i < number_of_sym_ops; i++)
    sym_ops[i] = sym.sym_ops[i];
  cartesian_translators = false;
  fractional_translators = true;
}

void DLV::symmetry_data::rotate_cartesian_ops(const symmetry_data &sym,
					      const real_l r[3][3])
{
  // If we define y' = R'y and x' = Rx, where y = Tx, then R'=TRT^-1
  // T^-1 is the transpose since its a rotation.
  init_number_of_ops(sym.number_of_sym_ops);
  for (int_g i = 0; i < number_of_sym_ops; i++) {
    real_l t[3][3];
    mat_mult(r, sym.sym_ops[i].rotation, t);
    mat_nt_mult(t, r, sym_ops[i].rotation, 3);
    for (int_g j = 0; j < 3; j++)
      sym_ops[i].cart_translation[j] = 0.0;
    sym_ops[i].has_translation = sym.sym_ops[i].has_translation;
    if (sym_ops[i].has_translation) {
      for (int_g j = 0; j < 3; j++)
	for (int_g k = 0; k < 3; k++)
	  sym_ops[i].cart_translation[j] +=
	    r[j][k] * sym.sym_ops[i].cart_translation[k];
    }
  }
  cartesian_translators = true;
  fractional_translators = false;
}

void DLV::symmetry_data::add_conv_cell_ops(const symmetry_data &sym,
					   const coord_type va[3],
					   const coord_type vb[3],
					   const coord_type vc[3],
					   const int_g dim)
{
  int_g n = sym.get_number_conv_cell_translators();
  if (n > 1) {
    real_l ops[4][3];
    sym.get_conv_cell_translators(ops);
    init_number_of_ops(n * sym.number_of_sym_ops);
    int_g nops = 0;
    for (int_g i = 0; i < sym.number_of_sym_ops; i++) {
      sym_ops[nops] = sym.sym_ops[i];
      nops++;
      for (int_g j = 1; j < n; j++) {
	real_l translation[3];
	translation[0] = va[0] * ops[j][0];
	translation[1] = va[1] * ops[j][0];
	translation[2] = va[2] * ops[j][0];
	if (dim > 1) {
	  translation[0] += vb[0] * ops[j][1];
	  translation[1] += vb[1] * ops[j][1];
	  translation[2] += vb[2] * ops[j][1];
	  if (dim > 2) {
	    translation[0] += vc[0] * ops[j][2];
	    translation[1] += vc[1] * ops[j][2];
	    translation[2] += vc[2] * ops[j][2];
	  }
	}
	sym_ops[nops] = sym.sym_ops[i];
	sym_ops[nops].add_cartesian_translation(translation);
	nops++;
      }
    }
    cartesian_translators = true;
    fractional_translators = false;
  }
}

bool DLV::symmetry_data::check_ops(const symmetry_operator ops[],
				   const int_g start, const int_g nops,
				   const int_g dim)
{
  const real_l tol = 0.001;
  for (int_g i = start; i < nops; i++) {
    bool found = true;
    for (int_g j = 0; j < dim; j++)
      found = found and
	((ops[nops].cart_translation[j] > ops[i].cart_translation[j] - tol) and
	 (ops[nops].cart_translation[j] < ops[i].cart_translation[j] + tol));
    if (found)
      return false;
  }
  return true;
}
				   
void DLV::symmetry_data::build_supercell_ops(const coord_type opa[3],
					     const coord_type opb[3],
					     const coord_type opc[3],
					     const coord_type va[3],
					     const coord_type vb[3],
					     const coord_type vc[3],
					     const int_g dim)
{
  // build the cell transformation matrix
  real_l cell[3][3] = { {1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0} };
  for (int_g i = 0; i < dim; i++)
    cell[0][i] = va[i];
  if (dim > 1) {
    for (int_g i = 0; i < dim; i++)
      cell[1][i] = vb[i];
    if (dim > 2) {
    for (int_g i = 0; i < dim; i++)
      cell[2][i] = vc[i];
    }
  }
  real_l inverse[3][3];
  matrix_invert(cell, inverse, dim);
  // convert basic ops to fractional (were old lattice vectors)
  // Todo - should we build translator closest to 0.0, so cell 0 is correct?
  real_l fraca[3] = { 0.0, 0.0, 0.0 };
  for (int_g i = 0; i < dim; i++)
    for (int_g j = 0; j < dim; j++)
      fraca[i] += inverse[j][i] * opa[j];
  real_l fracb[3] = { 0.0, 0.0, 0.0 };
  if (dim > 1) {
    for (int_g i = 0; i < dim; i++)
      for (int_g j = 0; j < dim; j++)
	fracb[i] += inverse[j][i] * opb[j];
  }
  real_l fracc[3] = { 0.0, 0.0, 0.0 };
  if (dim > 2) {
    for (int_g i = 0; i < dim; i++)
      for (int_g j = 0; j < dim; j++)
	fracc[i] += inverse[j][i] * opc[j];
  }
  // Now need to work out how many ops fit within 1 cell
  const real_l tol = 0.001;
  real_l vector[3] = { 0.0, 0.0, 0.0 };
  const int_g max_step = 100;
  int_g ca = 1;
  for (int_g j = 1; j < max_step; j++) {
    for (int_g i = 0; i < dim; i++)
      vector[i] = (real_l)j * fraca[i];
    bool outside = true;
    for (int_g i = 0; i < dim; i++)
      outside = outside and ((abs(vector[i]) < tol) or
			     (abs(vector[i]) > real_l(1.0) - tol));
    if (outside)
      break;
    ca++;
  }
  int_g cb = 1;
  int_g cc = 1;
  if (dim > 1) {
    for (int_g j = 1; j < max_step; j++) {
      for (int_g i = 0; i < dim; i++)
	vector[i] = (real_l)j * fracb[i];
      bool outside = true;
      for (int_g i = 0; i < dim; i++)
	outside = outside and ((abs(vector[i]) < tol) or
			       (abs(vector[i]) > real_l(1.0) - tol));
      if (outside)
	break;
      cb++;
    }
    if (dim > 2) {
      for (int_g j = 1; j < max_step; j++) {
	for (int_g i = 0; i < dim; i++)
	  vector[i] = (real_l)j * fracc[i];
	bool outside = true;
	for (int_g i = 0; i < dim; i++)
	  outside = outside and ((abs(vector[i]) < tol) or
				 (abs(vector[i]) > real_l(1.0) - tol));
	if (outside)
	  break;
	cc++;
      }
    }
  }
  // generate possible operators
  int_g max_ops = ca * cb * cc;
  if (max_ops > 1) {
    real_l (*op_list)[3] = new_local_array2(real_l, max_ops, 3);
    int_g count = 0;
    for (int_g i = 0; i < ca; i++) {
      for (int_g j = 0; j < cb; j++) {
	for (int_g k = 0; k < cc; k++) {
	  real_l t[3];
	  for (int_g l = 0; l < dim; l++) {
	    t[l] = (real_l)i * fraca[l] + (real_l)j * fracb[l]
	      + (real_l)k * fracc[l];
	    t[l] -= truncate(t[l]);
	  }
	  // make cartesian
	  op_list[count][0] = va[0] * t[0];
	  op_list[count][1] = va[1] * t[0];
	  op_list[count][2] = va[2] * t[0];
	  if (dim > 1) {
	    op_list[count][0] += vb[0] * t[1];
	    op_list[count][1] += vb[1] * t[1];
	    op_list[count][2] += vb[2] * t[1];
	    if (dim > 2) {
	      op_list[count][0] += vc[0] * t[2];
	      op_list[count][1] += vc[1] * t[2];
	      op_list[count][2] += vc[2] * t[2];
	    }
	  }
	  count++;
	}
      }
    }
    // multiply up operator table
    symmetry_operator *new_ops =
      new symmetry_operator[count * number_of_sym_ops];
    int_g nops = 0;
    for (int_g i = 0; i < number_of_sym_ops; i++) {
      int_g pos = nops;
      new_ops[nops] = sym_ops[i];
      nops++;
      for (int_g j = 1; j < count; j++) {
	new_ops[nops] = sym_ops[i];
	new_ops[nops].add_cartesian_translation(op_list[j]);
	// normalise the op
	real_l frac[3] = { 0.0, 0.0, 0.0 };
	for (int_g k = 0; k < dim; k++)
	  for (int_g l = 0; l < dim; l++)
	    frac[k] += inverse[l][k] * new_ops[nops].cart_translation[l];
	for (int_g k = 0; k < dim; k++)
	  frac[k] -= truncate(frac[k]);
	// make cartesian
	new_ops[nops].cart_translation[0] = va[0] * frac[0];
	new_ops[nops].cart_translation[1] = va[1] * frac[0];
	new_ops[nops].cart_translation[2] = va[2] * frac[0];
	if (dim > 1) {
	  new_ops[nops].cart_translation[0] += vb[0] * frac[1];
	  new_ops[nops].cart_translation[1] += vb[1] * frac[1];
	  new_ops[nops].cart_translation[2] += vb[2] * frac[1];
	  if (dim > 2) {
	    new_ops[nops].cart_translation[0] += vc[0] * frac[2];
	    new_ops[nops].cart_translation[1] += vc[1] * frac[2];
	    new_ops[nops].cart_translation[2] += vc[2] * frac[2];
	  }
	}
	if (check_ops(new_ops, pos, nops, dim)) {
	  nops++;
	}
      }
    }
    delete_local_array(op_list);
    delete [] sym_ops;
    sym_ops = new_ops;
    number_of_sym_ops = nops;
  }
}

void DLV::symmetry_data::shift_translations(const symmetry_data &sym,
					    const real_l x, const real_l y,
					    const real_l z,
					    const coord_type a[3],
					    const coord_type b[3],
					    const coord_type c[3],
					    const int_g dim,
					    const bool fractional)
{
  real_l prim[3][3] = {{1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0}};
  for (int_g i = 0; i < dim; i++) {
    prim[0][i] = a[i];
    prim[1][i] = b[i];
    prim[2][i] = c[i];
  }
  real_l inv[3][3];
  matrix_invert(prim, inv, dim);
  real_l best_shift[3];
  if (fractional) {
    best_shift[0] = x;
    best_shift[1] = y;
    best_shift[2] = z;
  } else {
    real_l shift[3];
    shift[0] = x;
    shift[1] = y;
    shift[2] = z;
    for (int_g i = 0; i < dim; i++) {
      best_shift[i] = 0.0;
      for (int_g j = 0; j < dim; j++)
	best_shift[i] += inv[j][i] * shift[j];
    }
  }
  // Convert ops to fractional coords. Ignore initial identity
  for (int_g i = 1; i < number_of_sym_ops; i++) {
    // Generate (R - I) in fractional system.
    real_l frot[3][3] = {{-1.0, 0.0, 0.0}, {0.0, -1.0, 0.0}, {0.0, 0.0, -1.0}};
    real_l temp[3][3];
    mat_nt_mult(sym.sym_ops[i].rotation, prim, temp, dim);
    mat_tn_mult_sum(inv, temp, frot, dim);
    for (int_g k = 0; k < dim; k++) {
      sym_ops[i].frac_translation[k] = sym.sym_ops[i].frac_translation[k];
      for (int_g j = 0; j < dim; j++)
	sym_ops[i].frac_translation[k] += frot[k][j] * best_shift[j];
    }
    bool has_trans = false;
    for (int_g j = 0; j < dim; j++) {
      sym_ops[i].frac_translation[j] -=
	truncate(sym_ops[i].frac_translation[j]);
      if (abs(sym_ops[i].frac_translation[j]) > trans_tol)
	has_trans = true;
    }
    sym_ops[i].has_translation = has_trans;
    for (int_g k = dim; k < 3; k++)
      sym_ops[i].frac_translation[k] = 0.0;
  }
  fractional_translators = true;
  cartesian_translators = false;
}

bool DLV::symmetry_data::minimise_translation_ops(real_l p[3],
						  const coord_type a[3],
						  const coord_type b[3],
						  const coord_type c[3],
						  const int_g dim) const
{
  p[0] = 0.0;
  p[1] = 0.0;
  p[2] = 0.0;
  int_g ntranslations = 0;
  for (int_g i = 0; i < number_of_sym_ops; i++)
    if (sym_ops[i].has_translation)
      ntranslations++;
  // Molecules have no translations.
  if (ntranslations > 0) {
    real_l prim[3][3] = {{1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0}};
    for (int_g i = 0; i < dim; i++) {
      prim[0][i] = a[i];
      prim[1][i] = b[i];
      prim[2][i] = c[i];
    }
    real_l inv[3][3];
    matrix_invert(prim, inv, dim);
    real_l shift[3];
    // loop over each op with a translation and see if it would reduce ntrans
    for (int_g i = 1; i < number_of_sym_ops; i++) {
      if (sym_ops[i].has_translation) {
	// work out which shift would 0 the translation
	// Generate (R - I) in fractional system.
	real_l fr[3][3] = { {-1.0, 0.0, 0.0},
			    {0.0, -1.0, 0.0},
			    {0.0, 0.0, -1.0} };
	real_l temp[3][3];
	mat_nt_mult(temp, sym_ops[i].rotation, prim, dim);
	mat_tn_mult_sum(fr, inv, temp, dim);
	// solve FR . shift = -ft, requires normal linear equation solver.
	if (dim == 1) {
	  if (abs(fr[0][0]) > rot_tol)
	    shift[0] = -sym_ops[i].frac_translation[0] / fr[0][0];
	} else if (dim == 2) {
	  // Todo linear_solve(fr, -ft, 2);
	  if (abs(fr[1][1]) > rot_tol) {
	    shift[0] = ((fr[0][1] * sym_ops[i].frac_translation[1]) / fr[1][1]
			- sym_ops[i].frac_translation[0]) /
	      (fr[0][0] - fr[0][1] * fr[1][0] / fr[1][1]);
	    shift[1] = -(sym_ops[i].frac_translation[1] + fr[1][0] * shift[0])
	      / fr[1][1];
	  } else if (abs(fr[0][0]) > rot_tol) {
	    shift[1] = ((fr[1][0] * sym_ops[i].frac_translation[0]) / fr[0][0]
			- sym_ops[i].frac_translation[1]) /
	      (fr[1][1] - fr[0][1] * fr[1][0] / fr[0][0]);
	    shift[0] = -(sym_ops[i].frac_translation[0] + fr[0][1] * shift[1])
	      / fr[0][0];
	  } else
	    fprintf(stderr, "2D equation solver failed\n");
	} else {
	  shift[0] = - sym_ops[i].frac_translation[0];
	  shift[1] = - sym_ops[i].frac_translation[1];
	  shift[2] = - sym_ops[i].frac_translation[2];
	  if (!linear_solve(fr, shift, 3))
	    fprintf(stderr, "3D equation solver failed\n");
	}
	// recalc the ops and see how many translations we would have
	// this is a copy of shift_trans
	int_g ntrans = 0;
	real_l ftrans[3];
	for (int_g l = 1; l < number_of_sym_ops; l++) {
	  // Generate (R - I) in fractional system.
	  real_l frot[3][3] = { {-1.0, 0.0, 0.0},
				{0.0, -1.0, 0.0},
				{0.0, 0.0, -1.0} };
	  real_l tempx[3][3];
	  mat_nt_mult(tempx, sym_ops[l].rotation, prim, dim);
	  mat_tn_mult_sum(frot, inv, tempx, dim);
	  for (int_g k = 0; k < dim; k++) {
	    ftrans[k] = sym_ops[l].frac_translation[k];
	    for (int_g j = 0; j < dim; j++)
	      ftrans[k] += frot[k][j] * shift[j];
	  }
	  bool has_trans = false;
	  for (int_g j = 0; j < dim; j++) {
	    ftrans[j] -= truncate(ftrans[j]);
	    if (abs(ftrans[j]) > trans_tol)
	      has_trans = true;
	  }
	  if (has_trans)
	    ntrans++;
	}
	if (ntrans < ntranslations) {
	  ntranslations = ntrans;
	  p[0] = shift[0];
	  p[1] = shift[1];
	  p[2] = shift[2];
	}
      }
    }
  }
  return true;
}

void DLV::symmetry_data::complete()
{
  if (number_of_sym_ops == 0) {
    init_number_of_ops(1);
    real_l r[3][3] = { {1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0} };
    real_l t[3] = { 0.0, 0.0, 0.0 };
    sym_ops[0].set_cartesian(r, t);
    cartesian_translators = true;
  }
  //if (number_of_sym_ops > 1)
    // Todo - find lattice type?
}

void DLV::symmetry_data::find_point_group(string &basename, int_g &n,
					  string &suffix) const
{
  // attempt to work out point group - Todo use operator id info
  // count non-translation ops
  int_g count = 0;
  for (int_g i = 0; i < number_of_sym_ops; i++)
    if (!sym_ops[i].has_translation)
      count++;
  std::vector<real_l> dets(count);
  std::vector<real_l> angs(count);
  std::vector<int> index(count);
  real_l tang;
  const real_l ang_tol = 0.001;
  bool reflections = false;
  int_g j = 0;
  for (int_g i = 0; i < number_of_sym_ops; i++) {
    if (!sym_ops[i].has_translation) {
      index[j] = i;
      dets[j] = determinant(sym_ops[i].rotation);
      tang = (sym_ops[i].rotation[0][0] + sym_ops[i].rotation[1][1]
	      + sym_ops[i].rotation[2][2]);
      tang -= dets[j];
      tang /= 2.0;
      if (tang > (real_l(1.0) - ang_tol))
	angs[j] = 0.0;
      else if (tang < ang_tol - 1.0)
	angs[j] = 180.0;
      else
	angs[j] = acos(tang) * real_l(180.0 / pi);
      if (dets[j] < real_l(0.0))
	reflections = true;
      j++;
    }
  }
  bool improper = false;
  bool proper = false;
  bool sixty = false;
  bool ninety = false;
  bool reflect_z = false;
  basename = "C";
  n = 1;
  suffix = "";
  switch (count) {
  case 1:
    basename = "C";
    n = 1;
    break;
  case 2:
    // Cs(8/7/6), C2(5/4/3), Ci(2)
    if (reflections) {
      if (sym_ops[index[1]].rotation[0][0] < -ang_tol) {
	if (sym_ops[index[1]].rotation[1][1] < -ang_tol) {
	  basename = "C";
	  suffix = "i";
	} else {
	  basename = "C";
	  suffix = "x";
	}
      } else if (sym_ops[index[1]].rotation[1][1] < -ang_tol) {
	basename = "C";
	suffix = "y";
      } else {
	basename = "C";
	suffix = "s";
      }
    } else {
      // Todo x or y problems?
      basename = "C";
      n = 2;
    }
    break;
  case 3:
    // C3(25)
    basename = "C";
    n = 3;
    break;
  case 4:
    // S4(18), C4(17), C2v(15/14/13), D2(12), C2h(11/10/9)
    ninety = false;
    proper = false;
    improper = false;
    for (int_g i = 0; i < 4; i++) {
      if (angs[i] < real_l(95.0) and angs[i] > real_l(85.0)) {
	// 90 degrees
	ninety = true;
	if (dets[i] > 0)
	  proper = true;
	else
	  improper = true;
      }
      if (sym_ops[index[i]].rotation[2][2] < (real_l(-1.0) + ang_tol))
	reflect_z = true;
    }
    if (ninety) {
      if (reflections) {
	basename = "S";
	n = 4;
      } else {
	basename = "C";
	n = 4;
      }
    } else {
      if (reflections) {
	basename = "C";
	n = 2;
	if (reflect_z) {
	  suffix = "h";
	} else {
	  suffix = "v";
	}
      } else {
	basename = "D";
	n = 2;
      }
    }
    break;
  case 5:
    basename = "C";
    n = 5;
    break;
  case 6:
    // C3h(34), C6(33), C3v(30/29), D3(28/27), C3i(26)
    sixty = false;
    improper = false;
    proper = false;
    reflect_z = false;
    for (int_g i = 0; i < 6; i++) {
      if (angs[i] < real_l(65.0) and angs[i] > real_l(55.0)) {
	// 60 degrees
	sixty = true;
	if (dets[j] > 0)
	  proper = true;
	else
	  improper = true;
      }
      if (sym_ops[index[i]].rotation[2][2] < (real_l(-1.0) + ang_tol))
	reflect_z = true;
    }
    if (sixty) {
      if (reflections) {
	basename = "C";
	n = 3;
	suffix = "i";
      } else {
	basename = "C";
	n = 6;
      }
    } else {
      if (reflections) {
	basename = "C";
	n = 3;
	if (reflect_z) {
	  suffix = "h";
	} else {
	  suffix = "v";
	}
      } else {
	basename = "D";
	n = 3;
      }
    }
    break;
  case 8:
    // D2d(23/22), C4v(21), D4(20), C4h(19), D2h(16)
    ninety = false;
    proper = false;
    improper = false;
    for (int_g i = 0; i < 8; i++) {
      if (angs[i] < real_l(95.0) and angs[i] > real_l(85.0)) {
	// 90 degrees
	ninety = true;
	if (dets[i] > 0)
	  proper = true;
	else
	  improper = true;
      }
    }
    if (ninety) {
      if (reflections) {
	if (proper) {
	  basename = "C";
	  n = 4;
	  if (improper)
	    suffix = "h";
	  else
	    suffix = "v";
	} else {
	  basename = "D";
	  n = 2;
	  suffix = "d";
	}
      } else {
	basename = "D";
	n = 4;
      }
    } else {
      basename = "D";
      n = 2;
      suffix = "h";
    }
    break;
  case 10:
    // Todo - check
    improper = false;
    proper = false;
    reflect_z = false;
    for (int_g i = 0; i < 10; i++) {
      if (dets[j] > 0)
	proper = true;
      else
	improper = true;
      if (sym_ops[index[i]].rotation[2][2] < (real_l(-1.0) + ang_tol))
	reflect_z = true;
    }
    if (reflections) {
      basename = "C";
      n = 5;
      if (reflect_z) {
	suffix = "h";
      } else {
	suffix = "v";
      }
    } else {
      basename = "D";
      n = 5;
    }
    break;
  case 12:
    // T(41), D3h(39/38), C6v(37), D6(36), C6h(35), D3d(32/31)
    sixty = false;
    improper = false;
    proper = false;
    for (int_g i = 0; i < 12; i++) {
      if (angs[i] < real_l(65.0) and angs[i] > real_l(55.0)) {
	// 60 degrees
	sixty = true;
	if (dets[i] > 0)
	  proper = true;
	else
	  improper = true;
      }
    }
    if (sixty) {
      if (reflections) {
	if (proper) {
	  basename = "C";
	  n = 6;
	  if (improper)
	    suffix = "h";
	  else
	    suffix = "v";
	} else {
	  basename = "D";
	  n = 3;
	  suffix = "d";
	}
      } else { // improper will be true
	basename = "D";
	n = 6;
      }
    } else {
      if (reflections) {
	basename = "D";
	n = 3;
	suffix = "h";
      } else
	basename = "T";
    }
    break;
  case 16: // D4h(24)
    basename = "D";
    n = 4;
    suffix = "h";
    break;
  case 20:
    // Todo - check
    improper = false;
    proper = false;
    for (int_g i = 0; i < 20; i++) {
      if (dets[i] > 0)
	proper = true;
      else
	improper = true;
    }
    basename = "D";
    n = 5;
    if (improper)
      suffix = "d";
    else
      suffix = "h";
    break;
  case 24: // Td, O or Th
    if (reflections) {
      proper = false;
      improper = false;
      sixty = false;
      for (int_g i = 0; i < 24; i++) {
	if (angs[i] < real_l(65.0) and angs[i] > real_l(55.0)) {
	  // 60 degrees
	  sixty = true;
	  if (dets[i] > 0)
	    proper = true;
	  else
	    improper = true;
	}
      }
      if (!sixty) {
	basename = "T";
	suffix = "d";
      } else {
	// D6h(40) or Th(42)
	if (proper) {
	  basename = "D";
	  n = 6;
	  suffix = "h";
	} else {
	  basename = "T";
	  suffix = "h";
	}
      }
    } else
      basename = "O";
    break;
  case 48:
    basename = "O";
    suffix = "h";
    break;
  case 60:
    basename = "I";
    break;
  case 120:
    basename = "I";
    suffix = "h";
    break;
  default:
    break;
  }
}

void DLV::symmetry_data::get_cartesian_ops(real_l rotations[][3][3],
					   real_l translations[][3],
					   const int_g nops) const
{
  for (int_g i = 0; i < nops; i++)
    sym_ops[i].get_cartesian(rotations[i], translations[i]);
}

void DLV::symmetry_data::get_cartesian_rotations(real_l rotations[][3][3],
						 const int_g nops) const
{
  coord_type t[3];
  for (int_g i = 0; i < nops; i++)
    sym_ops[i].get_cartesian(rotations[i], t);
}

void DLV::symmetry_data::get_cartesian_translations(real_l translations[][3],
						    const int_g nops) const
{
  coord_type r[3][3];
  for (int_g i = 0; i < nops; i++)
    sym_ops[i].get_cartesian(r, translations[i]);
}

void DLV::symmetry_data::get_fractional_translations(real_l translations[][3],
						     const int_g nops) const
{
  for (int_g i = 0; i < nops; i++)
    sym_ops[i].get_fractional_trans(translations[i]);
}

void DLV::symmetry_operator::set_cartesian(const real_l r[3][3],
					   const real_l t[3])
{
  for (int_g i = 0; i < 3; i++) {
    for (int_g j = 0; j < 3; j++)
      rotation[i][j] = r[i][j];
    cart_translation[i] = t[i];
  }
  has_translation = (abs(t[0]) > trans_tol or abs(t[1]) > trans_tol
		     or abs(t[2]) > trans_tol);
  if (!has_translation) {
    cart_translation[0] = 0.0;
    cart_translation[1] = 0.0;
    cart_translation[2] = 0.0;
    frac_translation[0] = 0.0;
    frac_translation[1] = 0.0;
    frac_translation[2] = 0.0;
  }
  bool safe = true;
  for (int_g i = 0; i < 3; i++) {
    for (int_g j = 0; j < 3; j++) {
      // see if it contains != 0.0 or != 1.0 values
      if (abs(r[i][j]) > rot_tol and abs(r[i][j]) < real_l(1.0) - rot_tol) {
	safe = false;
	break;
      }
    }
  }
  if (safe) {
    int_g m00 = get_one_or_zero(r[0][0]);
    int_g m01 = get_one_or_zero(r[0][1]);
    int_g m02 = get_one_or_zero(r[0][2]);
    int_g m10 = get_one_or_zero(r[1][0]);
    int_g m11 = get_one_or_zero(r[1][1]);
    int_g m12 = get_one_or_zero(r[1][2]);
    int_g m20 = get_one_or_zero(r[2][0]);
    int_g m21 = get_one_or_zero(r[2][1]);
    int_g m22 = get_one_or_zero(r[2][2]);
    cctbx::sgtbx::rot_mx rm(m00, m01, m02, m10, m11, m12, m20, m21, m22);
    switch (rm.type()) {
    case -4:
      rotation_id = R90;
      reflect_id = improper_rotation;
      break;
    case -2:
      rotation_id = identity;
      reflect_id = mirror;
      break;
    case -1:
      rotation_id = identity;
      reflect_id = inversion;
      break;
    case 2:
      rotation_id = R180;
      reflect_id = no_reflection;
      break;
    case 4:
      rotation_id = R90;
      reflect_id = no_reflection;
      break;
    case 1:
      rotation_id = identity;
      reflect_id = no_reflection;
    default:
      rotation_id = unknown_rotation;
      reflect_id = unknown_mirror;
      break;
    }
  } else {
    // expect 3, 5 or 6 fold axis
    real_l rx[3][3];
    for (int_g i = 0; i < 3; i++)
      for (int_g j = 0; j < 3; j++)
	rx[i][j] = r[i][j];
    real_l det = determinant(r);
    if (det > real_l(1.0) - rot_tol and det < real_l(1.0) + rot_tol)
      reflect_id = no_reflection;
    else if (det > real_l(-1.0) - rot_tol and det < real_l(-1.0) + rot_tol) {
      reflect_id = improper_rotation;
      // multiply by inversion to form +ve determinant - should then be
      // a proper rotation? Todo - check
      for (int_g i = 0; i < 3; i++)
	for (int_g j = 0; j < 3; j++)
	  rx[i][j] = - r[i][j];
    } else
      reflect_id = unknown_mirror;
    // multiplying n times should give identity (or mirror?) - 3, 5 or 6 fold
    real_l a[3][3];
    mat_mult(rx, rx, a);
    real_l b[3][3];
    mat_mult(rx, a, b);
    if (is_identity(b))
      rotation_id = R120;
    else {
      mat_mult(rx, b, a);
      mat_mult(rx, a, b);
      if (is_identity(b))
	rotation_id = R72;
      else {
	mat_mult(rx, b, a);
	if (is_identity(a))
	  rotation_id = R60;
	else
	  rotation_id = unknown_rotation;
      }
    }
  }
}

void DLV::symmetry_operator::set_cartesian(const real_l r[3][3],
					   const rotation_type rot_id,
					   const reflection_type ref_id)
{
  for (int_g i = 0; i < 3; i++) {
    for (int_g j = 0; j < 3; j++)
      rotation[i][j] = r[i][j];
    frac_translation[i] = 0.0;
    cart_translation[i] = 0.0;
  }
  has_translation = false;
  rotation_id = rot_id;
  reflect_id = ref_id;
}

void DLV::symmetry_operator::set_fractional(const real_l r[3][3],
					    const real_l t[3],
					    const rotation_type rot_id,
					    const reflection_type ref_id)
{
  for (int_g i = 0; i < 3; i++) {
    for (int_g j = 0; j < 3; j++)
      rotation[i][j] = r[i][j];
    frac_translation[i] = t[i];
  }
  has_translation = (abs(t[0]) > trans_tol or abs(t[1]) > trans_tol
		     or abs(t[2]) > trans_tol);
  if (!has_translation) {
    cart_translation[0] = 0.0;
    cart_translation[1] = 0.0;
    cart_translation[2] = 0.0;
    frac_translation[0] = 0.0;
    frac_translation[1] = 0.0;
    frac_translation[2] = 0.0;
  }
  rotation_id = rot_id;
  reflect_id = ref_id;
}

void DLV::symmetry_operator::set_fractional_translation(const real_l t[3])
{
  for (int_g i = 0; i < 3; i++)
    frac_translation[i] = t[i];
  has_translation = (abs(t[0]) > trans_tol or abs(t[1]) > trans_tol
		     or abs(t[2]) > trans_tol);
  if (!has_translation) {
    cart_translation[0] = 0.0;
    cart_translation[1] = 0.0;
    cart_translation[2] = 0.0;
    frac_translation[0] = 0.0;
    frac_translation[1] = 0.0;
    frac_translation[2] = 0.0;
  }
}

void DLV::symmetry_operator::add_cartesian_translation(const real_l t[3])
{
  cart_translation[0] += t[0];
  cart_translation[1] += t[1];
  cart_translation[2] += t[2];
  frac_translation[0] = 0.0;
  frac_translation[1] = 0.0;
  frac_translation[2] = 0.0;
  has_translation = true;  
}

void DLV::symmetry_operator::get_cartesian(real_l r[3][3], real_l t[3]) const
{
  for (int_g i = 0; i < 3; i++) {
    for (int_g j = 0; j < 3; j++)
      r[i][j] = rotation[i][j];
    t[i] = cart_translation[i];
  }
}

void DLV::symmetry_operator::get_fractional_trans(real_l t[3]) const
{
  for (int_g i = 0; i < 3; i++) {
    t[i] = frac_translation[i];
  }
}

void
DLV::symmetry_operator::generate_cartesian_translation(const coord_type va[3],
						       const coord_type vb[3],
						       const coord_type vc[3],
						       const int_g dim)
{
  if (has_translation) {
    cart_translation[0] = va[0] * frac_translation[0];
    cart_translation[1] = va[1] * frac_translation[0];
    cart_translation[2] = va[2] * frac_translation[0];
    if (dim > 1) {
      cart_translation[0] += vb[0] * frac_translation[1];
      cart_translation[1] += vb[1] * frac_translation[1];
      cart_translation[2] += vb[2] * frac_translation[1];
      if (dim > 2) {
	cart_translation[0] += vc[0] * frac_translation[2];
	cart_translation[1] += vc[1] * frac_translation[2];
	cart_translation[2] += vc[2] * frac_translation[2];
      }
    }
  } else {
    cart_translation[0] = 0.0;
    cart_translation[1] = 0.0;
    cart_translation[2] = 0.0;
  }
}

void
DLV::symmetry_operator::generate_fractional_translation(const real_l inv[3][3],
							const int_g dim)
{
  if (has_translation) {
    real_l frac[3] = { 0.0, 0.0, 0.0 };
    for (int_g i = 0; i < dim; i++)
      for (int_g j = 0; j < dim; j++)
	frac[i] += inv[i][j] * cart_translation[j];
    for (int_g i = 0; i < dim; i++)
      frac_translation[i] = frac[i] - truncate(frac[i]);
    for (int_g i = dim; i < 3; i++)
      frac_translation[i] = 0.0;
  } else {
    frac_translation[0] = 0.0;
    frac_translation[1] = 0.0;
    frac_translation[2] = 0.0;
  }
}

void DLV::symmetry_data::set_crystal03_lattice(const int_g, const int_g)
{
  // probably a bug
}

void DLV::plane_symmetry::set_crystal03_lattice(const int_g l, const int_g c)
{
  switch (l) {
  case 2:
    lattice = rectangular;
    break;
  case 3:
    lattice = square;
    break;
  case 4:
    //Todo - improve
    lattice = triangular;
    break;
  default:
    lattice = parallelpiped;
    break;
  }
  switch (c) {
  case 4:
    centre = c_face;
    break;
  default:
    centre = primitive;
    break;
  }
}

void DLV::volume_symmetry::set_crystal03_lattice(const int_g l, const int_g c)
{
  switch (l) {
  case 2:
    lattice = monoclinic;
    break;
  case 3:
    lattice = orthorhombic;
    break;
  case 4:
    lattice = tetragonal;
    break;
  case 5:
    //Todo - improve
    lattice = trigonal;
    break;
  case 6:
    lattice = cubic;
    break;
  default:
    lattice = triclinic;
    break;
  }
  switch (c) {
  case 2:
    centre = a_face;
    break;
  case 3:
    centre = b_face;
    break;
  case 4:
    centre = c_face;
    break;
  case 5:
    centre = face_centred;
    break;
  case 6:
    centre = body_centred;
    break;
  case 7:
    centre = rhombohedral;
    break;
  default:
    centre = primitive;
    break;
  }
}

void DLV::symmetry_data::set_lattice_and_centre(const int_g, const int_g)
{
  // probably a bug
}

void DLV::plane_symmetry::set_lattice_and_centre(const int_g l, const int_g c)
{
  lattice = (surface_class) l;
  centre = (lattice_centering) c;
}

void DLV::volume_symmetry::set_lattice_and_centre(const int_g l, const int_g c)
{
  lattice = (crystal_class) l;
  centre = (lattice_centering) c;
}

bool DLV::symmetry_data::set_space_group(const string gp, int_g &number)
{
  cctbx::sgtbx::space_group_type sgtype(gp);
  cctbx::sgtbx::space_group sg = sgtype.group();
  number = sgtype.number();
  bool ok = true;
  // operators
  init_number_of_ops(sg.f_inv() * sg.n_smx());
  int_g i = 0;
  for (nat_g j = 0; j < sg.f_inv(); j++) {
    for (nat_g k = 0; k < sg.n_smx(); k++) {
      cctbx::sgtbx::rt_mx mat = sg(0, j, k);
      scitbx::af::tiny<real_l, 12> elements = mat.as_double_array();
      real_l r[3][3];
      r[0][0] = elements[0];
      r[0][1] = elements[1];
      r[0][2] = elements[2];
      r[1][0] = elements[3];
      r[1][1] = elements[4];
      r[1][2] = elements[5];
      r[2][0] = elements[6];
      r[2][1] = elements[7];
      r[2][2] = elements[8];
      real_l t[3];
      t[0] = elements[9];
      t[1] = elements[10];
      t[2] = elements[11];
      // Todo - check if there are any operator transform issues with
      // monoclinic. Don't think triclinic has enough sym to be relevant.
      // Don't seem to be able to rely on op type, so always do this here
      if (number < 195 and number > 142) {
	// Trig or hex
	if (number < 168 and gp[0] == 'R' and gp[gp.length() - 1] != 'H') {
	  // Rhombohedral setting
	  // matrix is  a/sqrt(3) -a/2sqrt(3) -a/2sqrt(3)
	  //              0        a/2        -a/2
	  //             c/3       c/3         c/3
	  // The actual values of a and c shouldn't matter for the
	  // operators we get, but its hard to do in general
	  // so set a = sqrt(3), c = 3 (if it matters we have a bug)
	  real_l cell[3][3] = { {1.0,-0.5,-0.5},
				{0.0, 0.0, 0.0},
				{1.0, 1.0, 1.0} };
	  cell[1][1] = std::sqrt(3.0) / 2.0;
	  cell[1][2] = -cell[1][1];
	  real_l inv[3][3];
	  matrix_invert(cell, inv, 3);
	  real_l temp[3][3];
	  mat_mult(r, inv, temp);
	  mat_mult(cell, temp, r);
	} else {
	  // Hex setting
	  real_l cell[3][3] = { {0.0, 0.0, 0.0},
				{-0.5, 1.0, 0.0},
				{0.0, 0.0, 1.0} };
	  cell[0][0] = std::sqrt(3.0) / 2.0;
	  real_l inv[3][3];
	  matrix_invert(cell, inv, 3);
	  real_l temp[3][3];
	  mat_mult(r, inv, temp);
	  mat_mult(cell, temp, r);
	  if (number < 168 and gp[0] == 'R') {
	    // convert into rhomb cell fractional translations (x 1/3?)
	    t[0] = (real_l(2.0) * elements[9] + elements[10] + elements[11]);
	    t[1] = (- elements[9] + elements[10] + elements[11]);
	    t[2] = (- elements[9] - real_l(2.0) * elements[10] + elements[11]);
	  }
	}
      }
      cctbx::sgtbx::rot_mx rmat = mat.r();
      int_g optype = rmat.type();
      switch (optype) {
      case -6:
	// Its hexagonal
	/*{
	  real_l cell[3][3] = { 0.0, -0.5, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 };
	  cell[0][0] = sqrt(3.0)/2.0;
	  real_l inv[3][3];
	  matrix_invert(cell, inv, 3);
	  real_l temp[3][3];
	  mat_mult(r, inv, temp);
	  mat_mult(cell, temp, r);
	  set_fractional_operator(r, t, R60, improper_rotation, i);
	  }*/
	set_fractional_operator(r, t, R60, improper_rotation, i);
	break;
      case -4:
	set_fractional_operator(r, t, R90, improper_rotation, i);
	break;
      case -3:
	/*if (number > 194)
	  set_fractional_operator(r, t, R120, improper_rotation, i);
	else if (number < 168 and gp[0] == 'R') {
	  // Rhombohedral setting
	  // see comments for case 3
	  real_l cell[3][3] = { 1.0,-0.5,-0.5, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0 };
	  cell[1][1] = sqrt(3.0)/2.0;
	  cell[1][2] = -cell[1][1];
	  real_l inv[3][3];
	  matrix_invert(cell, inv, 3);
	  real_l temp[3][3];
	  mat_mult(r, inv, temp);
	  mat_mult(cell, temp, r);
	  set_fractional_operator(r, t, R120, improper_rotation, i);
	} else {
	  // Hex setting
	  real_l cell[3][3] = { 0.0, -0.5, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 };
	  cell[0][0] = sqrt(3.0)/2.0;
	  real_l inv[3][3];
	  matrix_invert(cell, inv, 3);
	  real_l temp[3][3];
	  mat_mult(r, inv, temp);
	  mat_mult(cell, temp, r);
	  set_fractional_operator(r, t, R120, improper_rotation, i);
	  }*/
	set_fractional_operator(r, t, R120, improper_rotation, i);
	break;
      case -2:
	set_fractional_operator(r, t, identity, mirror, i);
	break;
      case -1:
	set_fractional_operator(r, t, identity, inversion, i);
	break;
      case 1:
	set_fractional_operator(r, t, identity, no_reflection, i);
	break;
      case 2:
	set_fractional_operator(r, t, R180, no_reflection, i);
	break;
      case 3:
	/*if (number > 194)
	  set_fractional_operator(r, t, R120, no_reflection, i);
	else if (number < 168 and gp[0] == 'R') {
	  // Rhombohedral setting
	  // matrix is  a/sqrt(3) -a/2sqrt(3) -a/2sqrt(3)
	  //              0        a/2        -a/2
	  //             c/3       c/3         c/3
	  // The actual values of a and c shouldn't matter for the
	  // operators we get, but its hard to do in general
	  // so set a = sqrt(3), c = 3 (if it matters we have a bug)
	  real_l cell[3][3] = { 1.0,-0.5,-0.5, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0 };
	  cell[1][1] = sqrt(3.0)/2.0;
	  cell[1][2] = -cell[1][1];
	  real_l inv[3][3];
	  matrix_invert(cell, inv, 3);
	  real_l temp[3][3];
	  mat_mult(r, inv, temp);
	  mat_mult(cell, temp, r);
	  set_fractional_operator(r, t, R120, no_reflection, i);
	} else {
	  // Hex setting
	  real_l cell[3][3] = { 0.0, -0.5, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 };
	  cell[0][0] = sqrt(3.0)/2.0;
	  real_l inv[3][3];
	  matrix_invert(cell, inv, 3);
	  real_l temp[3][3];
	  mat_mult(r, inv, temp);
	  mat_mult(cell, temp, r);
	  set_fractional_operator(r, t, R120, no_reflection, i);
	  }*/
	set_fractional_operator(r, t, R120, no_reflection, i);
	break;
      case 4:
	set_fractional_operator(r, t, R90, no_reflection, i);
	break;
      case 6:
	// Its hexagonal
	/*{
	  real_l cell[3][3] = { 0.0, -0.5, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 };
	  cell[0][0] = sqrt(3.0)/2.0;
	  real_l inv[3][3];
	  matrix_invert(cell, inv, 3);
	  real_l temp[3][3];
	  mat_mult(r, inv, temp);
	  mat_mult(cell, temp, r);
	  set_fractional_operator(r, t, R60, no_reflection, i);
	  }*/
	set_fractional_operator(r, t, R60, no_reflection, i);
	break;
      default:
	ok = false;
	break;
      }
      i++;
    }
  }
  return ok;
}

void DLV::symmetry_data::set_cartesian(const real_l r[3][3],
				       const rotation_type rot_id,
				       const reflection_type ref_id,
				       const int_g i)
{
  sym_ops[i].set_cartesian(r, rot_id, ref_id);
}

void DLV::symmetry_data::mult_cartesian(const real_l r[3][3],
				       const rotation_type rot_id,
				       const reflection_type ref_id,
				       const int_g i, const int_g j)
{
  real_l mat[3][3];
  mat_mult(r, sym_ops[i].rotation, mat);
  rotation_type roid = rot_id;
  reflection_type rfid = ref_id;
  if (sym_ops[i].rotation_id > rot_id)
    roid = sym_ops[i].rotation_id;
  if (ref_id != no_reflection) {
    if (roid > identity)
      rfid = improper_rotation;
  } else if (sym_ops[i].reflect_id != no_reflection) {
    if (roid > identity)
      rfid = improper_rotation;
    else
      rfid = sym_ops[i].reflect_id;
  }
  sym_ops[j].set_cartesian(mat, roid, rfid);
}

void DLV::point_symmetry::add_5fold_axis(const int_g start,
					 const real_l reflect[3][3],
					 const reflection_type ref_id)
{
  // Do I want to R144, R270 etc or just use it as an indication of axis type?
  real_l r[3][3] = { {1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0} };
  set_cartesian(reflect, identity, ref_id, start);
  r[0][0] = std::cos(to_radians(72.0));
  r[1][1] = r[0][0];
  r[1][0] = std::sin(to_radians(72.0));
  r[0][1] = -r[1][0];
  real_l matrix[3][3];
  mat_mult(reflect, r, matrix);
  reflection_type id = ref_id;
  if (ref_id != no_reflection)
    id = improper_rotation;
  set_cartesian(matrix, R72, id, start + 1);
  r[0][0] = std::cos(to_radians(144.0));
  r[1][1] = r[0][0];
  r[1][0] = std::sin(to_radians(144.0));
  r[0][1] = -r[1][0];
  mat_mult(reflect, r, matrix);
  set_cartesian(matrix, R72, id, start + 2);
  r[0][0] = std::cos(to_radians(216.0));
  r[1][1] = r[0][0];
  r[1][0] = std::sin(to_radians(216.0));
  r[0][1] = -r[1][0];
  mat_mult(reflect, r, matrix);
  set_cartesian(matrix, R72, id, start + 3);
  r[0][0] = std::cos(to_radians(288.0));
  r[1][1] = r[0][0];
  r[1][0] = std::sin(to_radians(288.0));
  r[0][1] = -r[1][0];
  mat_mult(reflect, r, matrix);
  set_cartesian(matrix, R72, id, start + 4);
}

void DLV::point_symmetry::add_6fold_axis(const int_g start,
					 const real_l reflect[3][3],
					 const reflection_type ref_id)
{
  real_l r[3][3] = { {1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0} };
  set_cartesian(reflect, identity, ref_id, start);
  r[0][0] = std::cos(to_radians(60.0));
  r[1][1] = r[0][0];
  r[1][0] = std::sin(to_radians(60.0));
  r[0][1] = -r[1][0];
  real_l matrix[3][3];
  mat_mult(reflect, r, matrix);
  reflection_type id = ref_id;
  if (ref_id != no_reflection)
    id = improper_rotation;
  set_cartesian(matrix, R60, id, start + 1);
  r[0][0] = std::cos(to_radians(120.0));
  r[1][1] = r[0][0];
  r[1][0] = std::sin(to_radians(120.0));
  r[0][1] = -r[1][0];
  mat_mult(reflect, r, matrix);
  set_cartesian(matrix, R120, id, start + 2);
  r[0][0] = std::cos(to_radians(180.0));
  r[1][1] = r[0][0];
  r[1][0] = std::sin(to_radians(180.0));
  r[0][1] = -r[1][0];
  mat_mult(reflect, r, matrix);
  set_cartesian(matrix, R180, id, start + 3);
  r[0][0] = std::cos(to_radians(240.0));
  r[1][1] = r[0][0];
  r[1][0] = std::sin(to_radians(240.0));
  r[0][1] = -r[1][0];
  mat_mult(reflect, r, matrix);
  set_cartesian(matrix, R120, id, start + 4);
  r[0][0] = std::cos(to_radians(300.0));
  r[1][1] = r[0][0];
  r[1][0] = std::sin(to_radians(300.0));
  r[0][1] = -r[1][0];
  mat_mult(reflect, r, matrix);
  set_cartesian(matrix, R60, id, start + 5);
}

void DLV::symmetry_data::build_table(const int_g n, const int_g size,
				     const bool translate)
{
  if (translate)
    fprintf(stderr, "Translations not implemented - Todo\n");
  else {
    int_g nops = n;
    while (nops < size) {
      int_g cops = nops;
      for (int_g i = 1; (i < cops and nops < size); i++) {
	for (int_g j = 1; (j < cops and nops < size); j++) {
	  real_l mat[3][3];
	  mat_mult(sym_ops[i].rotation, sym_ops[j].rotation, mat);
	  bool found = false;
	  for (int_g k = 0; k < nops; k++) {
	    real_l diff = 0.0;
	    for (int_g l = 0; l < 3; l++)
	      for (int_g m = 0; m < 3; m++)
		diff += abs(sym_ops[k].rotation[l][m] - mat[l][m]);
	    const real_l tol = 0.001;
	    if (diff < tol) {
	      found = true;
	      break;
	    }
	  }
	  if (!found) {
	    rotation_type roid = sym_ops[i].rotation_id;
	    reflection_type rfid = sym_ops[i].reflect_id;
	    if (sym_ops[j].rotation_id > roid)
	      roid = sym_ops[j].rotation_id;
	    if (rfid != no_reflection) {
	      if (roid > identity)
		rfid = improper_rotation;
	    } else if (sym_ops[j].reflect_id != no_reflection) {
	      if (roid > identity)
		rfid = improper_rotation;
	      else
		rfid = sym_ops[j].reflect_id;
	    }
	    sym_ops[nops].set_cartesian(mat, roid, rfid);
	    nops++;
	  }
	}
      }
    }
  }
}

bool DLV::point_symmetry::set_group(const int_g gp)
{
  int_g number;
  cctbx::sgtbx::space_group_symbols symbol(gp);
  string label = symbol.universal_hermann_mauguin();
  bool ok = set_space_group(label, number);
  return ok;
}

bool DLV::point_symmetry::set_group(const char gp[])
{
  int_g number;
  bool ok = set_space_group(gp, number);
  return ok;
}

bool DLV::point_symmetry::set_point_group(const char gp[])
{
  bool ok = true;
  real_l m[3][3] = { {1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0} };
  if (strcmp(gp, "C5") == 0) {
    init_number_of_ops(5);
    add_5fold_axis(0, m, no_reflection);
  } else if (strcmp(gp, "C5v") == 0) {
    init_number_of_ops(10);
    add_5fold_axis(0, m, no_reflection);
    m[1][1] = -1.0; // reflect in xz
    add_5fold_axis(5, m, mirror);
  } else if (strcmp(gp, "C5h") == 0) {
    init_number_of_ops(10);
    add_5fold_axis(0, m, no_reflection);
    m[2][2] = -1.0;
    add_5fold_axis(5, m, mirror);
  } else if (strcmp(gp, "C5i") == 0) {
    init_number_of_ops(10);
    add_5fold_axis(0, m, no_reflection);
    m[0][0] = -1.0;
    m[1][1] = -1.0;
    m[2][2] = -1.0;
    add_5fold_axis(5, m, inversion);
  } else if (strcmp(gp, "D5") == 0) {
    init_number_of_ops(10);
    add_5fold_axis(0, m, no_reflection);
    m[1][1] = -1.0; // 2-fold rotation about x
    m[2][2] = -1.0;
    add_5fold_axis(5, m, no_reflection);
  } else if (strcmp(gp, "D5d") == 0) {
    init_number_of_ops(20);
    add_5fold_axis(0, m, no_reflection);
    m[1][1] = -1.0; // 2-fold rotation about x
    m[2][2] = -1.0;
    add_5fold_axis(5, m, no_reflection);
    //m[0][0] = - sin(to_radians(18.0));
    //m[0][1] = cos(to_radians(18.0));
    //m[1][0] = m[0][1];
    //m[1][1] = -m[0][0];
    m[0][0] = -1.0;
    m[1][1] = 1.0;
    m[2][2] = 1.0;  // reflect plane between 2-fold axes (y)
    add_5fold_axis(10, m, mirror);
    //m[0][0] = -1.0; // 2-fold rotation about x + reflection
    m[1][1] = -1.0;
    m[2][2] = -1.0;
    add_5fold_axis(15, m, improper_rotation);
  } else if (strcmp(gp, "D5h") == 0) {
    init_number_of_ops(20);
    add_5fold_axis(0, m, no_reflection);
    m[1][1] = -1.0; // 2-fold rotation about x
    m[2][2] = -1.0;
    add_5fold_axis(5, m, no_reflection);
    m[1][1] = 1.0; // reflect in z
    add_5fold_axis(10, m, mirror);
    m[1][1] = -1.0; // 2-fold rotation about x + reflect in z
    m[2][2] = 1.0;
    add_5fold_axis(15, m, improper_rotation);
  } else if (strcmp(gp, "D6d") == 0) {
    init_number_of_ops(24);
    add_6fold_axis(0, m, no_reflection);
    m[1][1] = -1.0; // 2-fold rotation about x
    m[2][2] = -1.0;
    add_6fold_axis(6, m, no_reflection);
    m[1][1] = -1.0; // 2-fold rotation + reflection = inversion
    add_6fold_axis(12, m, inversion);
    m[1][1] = 1.0;  // reflect plane between 2-fold axes, e.g. x-axis
    m[2][2] = 1.0;
    add_6fold_axis(18, m, mirror);
  } else if (gp[0] == 'I') {
    int_g nops = 60;
    if (strcmp(gp, "Ih") == 0)
      nops = 120;
    init_number_of_ops(nops);
    /*
    // Start by generating D5d subgroup
    add_5fold_axis(0, m, no_reflection);
    m[1][1] = -1.0; // 2-fold rotation about x
    m[2][2] = -1.0;
    add_5fold_axis(5, m, no_reflection);
    //m[0][0] = sin(to_radians(18.0));
    //m[0][1] = cos(to_radians(18.0));
    //m[1][0] = m[0][1];
    //m[1][1] = -m[0][0];
    m[0][0] = -1.0;
    m[1][1] = 1.0;
    m[2][2] = 1.0;  // reflect plane between 2-fold axes
    add_5fold_axis(10, m, mirror);
    m[1][1] = -1.0; // 2-fold rotation about xy + reflection
    m[2][2] = -1.0;
    add_5fold_axis(15, m, improper_rotation);
    // generate R120 at angle to vertical 5-fold axis
    real_l r3[3][3] = { {1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0} };
    r3[0][0] = cos(to_radians(120.0));
    r3[1][1] = r3[0][0];
    r3[1][0] = sin(to_radians(120.0));
    r3[0][1] = -r3[1][0];
    // rotate off z axis in -y direction, value from Kim
    real_l angle[3][3] = { {1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0} };
    real_l tau = (1.0 + sqrt(5.0)) / 2.0;
    real_l ctheta35 = sqrt((3.0 + 4.0 * tau) / 15.0);
    real_l stheta35 = sqrt(1.0 - ctheta35 * ctheta35);
    angle[1][1] = ctheta35;
    angle[2][2] = ctheta35;
    angle[1][2] = -stheta35;
    angle[2][1] = stheta35;
    real_l r[3][3];
    mat_mult(angle, r3, r);
    set_cartesian(r, R120, no_reflection, 20);
    // multiply onto existing operators
    for (int_g i = 21; i < 40; i++)
      mult_cartesian(r, R120, no_reflection, i - 20, i);
    // generate R240 (R120 * R120)
    mat_mult(r, r, r3);
    set_cartesian(r3, R120, no_reflection, 40);
    // multiply onto existing operators
    for (int_g i = 41; i < 60; i++)
      mult_cartesian(r3, R120, no_reflection, i - 40, i);
    */
    // generators from Acta Cryst A47, 70 (1991), 2 fold axis on z
    set_cartesian(m, identity, no_reflection, 0);
    m[0][0] = -1.0;
    m[1][1] = -1.0;
    set_cartesian(m, R180, no_reflection, 1);
    m[0][0] = 0.0;
    m[1][1] = 0.0;
    m[0][2] = 1.0;
    m[1][0] = 1.0;
    m[2][1] = 1.0;
    m[2][2] = 0.0;
    set_cartesian(m, R120, no_reflection, 2);
    real_l r[3][3];
    mat_mult(m, m, r);
    set_cartesian(r, R120, no_reflection, 3);
    real_l tau = (1.0 + std::sqrt(5.0)) / 2.0;
    real_l g5[3][3];
    g5[0][0] = 0.5;
    g5[0][1] = real_l(-0.5) * tau;
    g5[0][2] = real_l(0.5) / tau;
    g5[1][0] = -g5[0][1];
    g5[1][1] = g5[0][2];
    g5[1][2] = -0.5;
    g5[2][0] = g5[0][2];
    g5[2][1] = 0.5;
    g5[2][2] = g5[1][0];
    set_cartesian(g5, R72, no_reflection, 4);
    mat_mult(g5, g5, m);
    set_cartesian(m, R72, no_reflection, 5);
    mat_mult(g5, m, r);
    set_cartesian(r, R72, no_reflection, 6);
    mat_mult(g5, r, m);
    set_cartesian(m, R72, no_reflection, 7);
    // 5th is the identity
    build_table(8, 60);
    if (strcmp(gp, "Ih") == 0) {
      // Horizontal mirror plane will generate an inversion, so we use that
      m[0][0] = -1.0;
      m[0][1] = 0.0;
      m[0][2] = 0.0;
      m[1][0] = 0.0;
      m[1][1] = -1.0;
      m[1][2] = 0.0;
      m[2][0] = 0.0;
      m[2][1] = 0.0;
      m[2][2] = -1.0;
      set_cartesian(m, identity, inversion, 60);
      // and multiply onto previous operators.
      for (int_g i = 61; i < 120; i++)
	mult_cartesian(m, identity, inversion, i - 60, i);
    }
  } else {
    // Should be able to use space groups as gp^1 in Schoeflies notation
    char name[32];
    strcpy(name, gp);
    strcat(name, "^1");
    int_g number;
    ok = set_space_group(name, number);
  }
  return ok;
}

bool DLV::line_symmetry::set_group(const int_g gp)
{
  int_g number;
  cctbx::sgtbx::space_group_symbols symbol(gp);
  string label = symbol.universal_hermann_mauguin();
  bool ok = set_space_group(label, number);
  return ok;
}

bool DLV::line_symmetry::set_group(const char gp[])
{
  int_g number;
  bool ok = set_space_group(gp, number);
  return ok;
}

bool DLV::plane_symmetry::set_group(const int_g gp)
{
  // Todo
  return false;
}

bool DLV::plane_symmetry::set_group(const char gp[])
{
  int_g number;
  bool ok = set_space_group(gp, number);
  // Todo - assumes a space in the symbol, needs checking
  set_lattice_data(number, gp[0], gp[2]);
  return ok;
}

void DLV::plane_symmetry::set_lattice_data(const int_g num, const char c1,
					   const char c2)
{
  if (num < 3)
    lattice = parallelpiped;
  else if (num < 16) {
    if (c1 == 'C')
      lattice = rectangular;
    else {
      if (c2 == '1')
	lattice = parallelpiped;
      else
	lattice = rectangular;
    }
  } else if (num < 75)
    lattice = rectangular;
  else if (num < 143)
    lattice = square;
  else if (num < 168)
    lattice = triangular;
  else
    lattice = six_fold;
  switch (c1) {
  case 'C':
    centre = c_face;
    break;
  case 'P':
  default:
    centre = primitive;
    break;
  }
}

bool DLV::volume_symmetry::set_group(const int_g gp)
{
  int_g number;
  cctbx::sgtbx::space_group_symbols symbol(gp);
  string label = symbol.universal_hermann_mauguin();
  bool ok = set_space_group(label, number);
  set_lattice_data(number, label[0]);
  return ok;
}

bool DLV::volume_symmetry::set_group(const char gp[])
{
  int_g number;
  bool ok = set_space_group(gp, number);
  set_lattice_data(number, gp[0]);
  return ok;
}

void DLV::volume_symmetry::set_lattice_data(const int_g num, const char c)
{
  if (num < 3)
    lattice = triclinic;
  else if (num < 16)
    lattice = monoclinic;
  else if (num < 75)
    lattice = orthorhombic;
  else if (num < 143)
    lattice = tetragonal;
  else if (num < 168)
    lattice = trigonal;
  else if (num < 195)
    lattice = hexagonal;
  else
    lattice = cubic;
  switch (c) {
  case 'A':
    centre = a_face;
    break;
  case 'B':
    centre = b_face;
    break;
  case 'C':
    centre = c_face;
    break;
  case 'F':
    centre = face_centred;
    break;
  case 'I':
    centre = body_centred;
    break;
  case 'R':
    centre = rhombohedral;
    break;
  case 'P':
  default:
    centre = primitive;
    break;
  }
}

void DLV::symmetry_data::get_transform_to_prim_cell(coord_type t[3][3]) const
{
  t[0][0] = 1.0;
  t[0][1] = 0.0;
  t[0][2] = 0.0;
  t[1][0] = 0.0;
  t[1][1] = 1.0;
  t[1][2] = 0.0;
  t[2][0] = 0.0;
  t[2][1] = 0.0;
  t[2][2] = 1.0;
}

void DLV::plane_symmetry::get_transform_to_prim_cell(coord_type t[3][3]) const
{
  switch (centre) {
  case c_face:
    t[0][0] = 0.5;
    t[0][1] = 0.5;
    t[0][2] = 0.0;
    t[1][0] = -0.5;
    t[1][1] = 0.5;
    t[1][2] = 0.0;
    t[2][0] = 0.0;
    t[2][1] = 0.0;
    t[2][2] = 1.0;
    break;
  case primitive:
  default:
    t[0][0] = 1.0;
    t[0][1] = 0.0;
    t[0][2] = 0.0;
    t[1][0] = 0.0;
    t[1][1] = 1.0;
    t[1][2] = 0.0;
    t[2][0] = 0.0;
    t[2][1] = 0.0;
    t[2][2] = 1.0;
    break;
  }
}

void DLV::volume_symmetry::get_transform_to_prim_cell(coord_type t[3][3]) const
{
  switch (centre) {
  case primitive:
    t[0][0] = 1.0;
    t[0][1] = 0.0;
    t[0][2] = 0.0;
    t[1][0] = 0.0;
    t[1][1] = 1.0;
    t[1][2] = 0.0;
    t[2][0] = 0.0;
    t[2][1] = 0.0;
    t[2][2] = 1.0;
    break;
  case a_face:
    t[0][0] = 1.0;
    t[0][1] = 0.0;
    t[0][2] = 0.0;
    t[1][0] = 0.0;
    t[1][1] = 0.5;
    t[1][2] = 0.5;
    t[2][0] = 0.0;
    t[2][1] = -0.5;
    t[2][2] = 0.5;
    break;
  case b_face:
    t[0][0] = 0.5;
    t[0][1] = 0.0;
    t[0][2] = -0.5;
    t[1][0] = 0.0;
    t[1][1] = 1.0;
    t[1][2] = 0.0;
    t[2][0] = 0.5;
    t[2][1] = 0.0;
    t[2][2] = 0.5;
    break;
  case c_face:
    t[0][0] = 0.5;
    t[0][1] = 0.5;
    t[0][2] = 0.0;
    t[1][0] = -0.5;
    t[1][1] = 0.5;
    t[1][2] = 0.0;
    t[2][0] = 0.0;
    t[2][1] = 0.0;
    t[2][2] = 1.0;
    break;
  case face_centred:
    t[0][0] = 0.0;
    t[0][1] = 0.5;
    t[0][2] = 0.5;
    t[1][0] = 0.5;
    t[1][1] = 0.0;
    t[1][2] = 0.5;
    t[2][0] = 0.5;
    t[2][1] = 0.5;
    t[2][2] = 0.0;
    break;
  case body_centred:
    t[0][0] = -0.5;
    t[0][1] = 0.5;
    t[0][2] = 0.5;
    t[1][0] = 0.5;
    t[1][1] = -0.5;
    t[1][2] = 0.5;
    t[2][0] = 0.5;
    t[2][1] = 0.5;
    t[2][2] = -0.5;
    break;
  case rhombohedral:
    t[0][0] = 2.0 / 3.0;
    t[0][1] = 1.0 / 3.0;
    t[0][2] = 1.0 / 3.0;
    t[1][0] = -1.0 / 3.0;
    t[1][1] = 1.0 / 3.0;
    t[1][2] = 1.0 / 3.0;
    t[2][0] = -1.0 / 3.0;
    t[2][1] = -2.0 / 3.0;
    t[2][2] = 1.0 / 3.0;
    break;
  }
}

void DLV::symmetry_data::get_transform_to_prim_cell(int_g t[3][3]) const
{
  t[0][0] = 1;
  t[0][1] = 0;
  t[0][2] = 0;
  t[1][0] = 0;
  t[1][1] = 1;
  t[1][2] = 0;
  t[2][0] = 0;
  t[2][1] = 0;
  t[2][2] = 1;
}

void DLV::plane_symmetry::get_transform_to_prim_cell(int_g t[3][3]) const
{
  switch (centre) {
  case c_face:
    t[0][0] = 1;
    t[0][1] = 1;
    t[0][2] = 0;
    t[1][0] = -1;
    t[1][1] = 1;
    t[1][2] = 0;
    t[2][0] = 0;
    t[2][1] = 0;
    t[2][2] = 2;
    break;
  case primitive:
  default:
    t[0][0] = 1;
    t[0][1] = 0;
    t[0][2] = 0;
    t[1][0] = 0;
    t[1][1] = 1;
    t[1][2] = 0;
    t[2][0] = 0;
    t[2][1] = 0;
    t[2][2] = 1;
    break;
  }
}

void DLV::volume_symmetry::get_transform_to_prim_cell(int_g t[3][3]) const
{
  switch (centre) {
  case primitive:
    t[0][0] = 1;
    t[0][1] = 0;
    t[0][2] = 0;
    t[1][0] = 0;
    t[1][1] = 1;
    t[1][2] = 0;
    t[2][0] = 0;
    t[2][1] = 0;
    t[2][2] = 1;
    break;
  case a_face:
    t[0][0] = 2;
    t[0][1] = 0;
    t[0][2] = 0;
    t[1][0] = 0;
    t[1][1] = 1;
    t[1][2] = 1;
    t[2][0] = 0;
    t[2][1] = -1;
    t[2][2] = 1;
    break;
  case b_face:
    t[0][0] = 1;
    t[0][1] = 0;
    t[0][2] = -1;
    t[1][0] = 0;
    t[1][1] = 2;
    t[1][2] = 0;
    t[2][0] = 1;
    t[2][1] = 0;
    t[2][2] = 1;
    break;
  case c_face:
    t[0][0] = 1;
    t[0][1] = 1;
    t[0][2] = 0;
    t[1][0] = -1;
    t[1][1] = 1;
    t[1][2] = 0;
    t[2][0] = 0;
    t[2][1] = 0;
    t[2][2] = 2;
    break;
  case face_centred:
    t[0][0] = 0;
    t[0][1] = 1;
    t[0][2] = 1;
    t[1][0] = 1;
    t[1][1] = 0;
    t[1][2] = 1;
    t[2][0] = 1;
    t[2][1] = 1;
    t[2][2] = 0;
    break;
  case body_centred:
    t[0][0] = -1;
    t[0][1] = 1;
    t[0][2] = 1;
    t[1][0] = 1;
    t[1][1] = -1;
    t[1][2] = 1;
    t[2][0] = 1;
    t[2][1] = 1;
    t[2][2] = -1;
    break;
  case rhombohedral:
    t[0][0] = 2;
    t[0][1] = 1;
    t[0][2] = 1;
    t[1][0] = -1;
    t[1][1] = 1;
    t[1][2] = 1;
    t[2][0] = -1;
    t[2][1] = -2;
    t[2][2] = 1;
    break;
  }
}

void DLV::symmetry_data::get_transform_to_conv_cell(coord_type t[3][3]) const
{
  t[0][0] = 1.0;
  t[0][1] = 0.0;
  t[0][2] = 0.0;
  t[1][0] = 0.0;
  t[1][1] = 1.0;
  t[1][2] = 0.0;
  t[2][0] = 0.0;
  t[2][1] = 0.0;
  t[2][2] = 1.0;
}

void DLV::plane_symmetry::get_transform_to_conv_cell(coord_type t[3][3]) const
{
  switch (centre) {
  case c_face:
    t[0][0] = 1.0;
    t[0][1] = -1.0;
    t[0][2] = 0.0;
    t[1][0] = 1.0;
    t[1][1] = 1.0;
    t[1][2] = 0.0;
    t[2][0] = 0.0;
    t[2][1] = 0.0;
    t[2][2] = 1.0;
    break;
  case primitive:
  default:
    t[0][0] = 1.0;
    t[0][1] = 0.0;
    t[0][2] = 0.0;
    t[1][0] = 0.0;
    t[1][1] = 1.0;
    t[1][2] = 0.0;
    t[2][0] = 0.0;
    t[2][1] = 0.0;
    t[2][2] = 1.0;
    break;
  }
}

void DLV::volume_symmetry::get_transform_to_conv_cell(coord_type t[3][3]) const
{
  switch (centre) {
  case primitive:
    t[0][0] = 1.0;
    t[0][1] = 0.0;
    t[0][2] = 0.0;
    t[1][0] = 0.0;
    t[1][1] = 1.0;
    t[1][2] = 0.0;
    t[2][0] = 0.0;
    t[2][1] = 0.0;
    t[2][2] = 1.0;
    break;
  case a_face:
    t[0][0] = 1.0;
    t[0][1] = 0.0;
    t[0][2] = 0.0;
    t[1][0] = 0.0;
    t[1][1] = 1.0;
    t[1][2] = -1.0;
    t[2][0] = 0.0;
    t[2][1] = 1.0;
    t[2][2] = 1.0;
    break;
  case b_face:
    t[0][0] = 1.0;
    t[0][1] = 0.0;
    t[0][2] = 1.0;
    t[1][0] = 0.0;
    t[1][1] = 1.0;
    t[1][2] = 0.0;
    t[2][0] = -1.0;
    t[2][1] = 0.0;
    t[2][2] = 1.0;
    break;
  case c_face:
    t[0][0] = 1.0;
    t[0][1] = -1.0;
    t[0][2] = 0.0;
    t[1][0] = 1.0;
    t[1][1] = 1.0;
    t[1][2] = 0.0;
    t[2][0] = 0.0;
    t[2][1] = 0.0;
    t[2][2] = 1.0;
    break;
  case face_centred:
    t[0][0] = -1.0;
    t[0][1] = 1.0;
    t[0][2] = 1.0;
    t[1][0] = 1.0;
    t[1][1] = -1.0;
    t[1][2] = 1.0;
    t[2][0] = 1.0;
    t[2][1] = 1.0;
    t[2][2] = -1.0;
    break;
  case body_centred:
    t[0][0] = 0.0;
    t[0][1] = 1.0;
    t[0][2] = 1.0;
    t[1][0] = 1.0;
    t[1][1] = 0.0;
    t[1][2] = 1.0;
    t[2][0] = 1.0;
    t[2][1] = 1.0;
    t[2][2] = 0.0;
    break;
  case rhombohedral:
    t[0][0] = 1.0;
    t[0][1] = -1.0;
    t[0][2] = 0.0;
    t[1][0] = 0.0;
    t[1][1] = 1.0;
    t[1][2] = -1.0;
    t[2][0] = 1.0;
    t[2][1] = 1.0;
    t[2][2] = 1.0;
    break;
  }
}

DLV::int_g DLV::symmetry_data::get_number_conv_cell_translators() const
{
  return 1;
}

DLV::int_g DLV::plane_symmetry::get_number_conv_cell_translators() const
{
  switch (centre) {
  case c_face:
    return 2;
  case primitive:
  default:
    return 1;
  }
}

DLV::int_g DLV::volume_symmetry::get_number_conv_cell_translators() const
{
  switch (centre) {
  case primitive:
    return 1;
  case a_face:
    return 2;
  case b_face:
    return 2;
  case c_face:
    return 2;
  case face_centred:
    return 4;
  case body_centred:
    return 2;
  case rhombohedral:
    return 3;
  }
  return 1;
}

void DLV::symmetry_data::get_conv_cell_translators(real_l ops[][3]) const
{
  ops[0][0] = 0.0;
  ops[0][1] = 0.0;
  ops[0][2] = 0.0;
}

void DLV::plane_symmetry::get_conv_cell_translators(real_l ops[][3]) const
{
  switch (centre) {
  case c_face:
    ops[0][0] = 0.0;
    ops[0][1] = 0.0;
    ops[0][2] = 0.0;
    ops[1][0] = 0.5;
    ops[1][1] = 0.5;
    ops[1][2] = 0.0;
    break;
  case primitive:
  default:
    ops[0][0] = 0.0;
    ops[0][1] = 0.0;
    ops[0][2] = 0.0;
    break;
  }
}

void DLV::volume_symmetry::get_conv_cell_translators(real_l ops[][3]) const
{
  switch (centre) {
  case primitive:
    ops[0][0] = 0.0;
    ops[0][1] = 0.0;
    ops[0][2] = 0.0;
    break;
  case a_face:
    ops[0][0] = 0.0;
    ops[0][1] = 0.0;
    ops[0][2] = 0.0;
    ops[1][0] = 0.0;
    ops[1][1] = 0.5;
    ops[1][2] = 0.5;
    break;
  case b_face:
    ops[0][0] = 0.0;
    ops[0][1] = 0.0;
    ops[0][2] = 0.0;
    ops[1][0] = 0.5;
    ops[1][1] = 0.0;
    ops[1][2] = 0.5;
    break;
  case c_face:
    ops[0][0] = 0.0;
    ops[0][1] = 0.0;
    ops[0][2] = 0.0;
    ops[1][0] = 0.5;
    ops[1][1] = 0.5;
    ops[1][2] = 0.0;
    break;
  case face_centred:
    ops[0][0] = 0.0;
    ops[0][1] = 0.0;
    ops[0][2] = 0.0;
    ops[1][0] = 0.5;
    ops[1][1] = 0.5;
    ops[1][2] = 0.0;
    ops[2][0] = 0.5;
    ops[2][1] = 0.0;
    ops[2][2] = 0.5;
    ops[3][0] = 0.0;
    ops[3][1] = 0.5;
    ops[3][2] = 0.5;
    break;
  case body_centred:
    ops[0][0] = 0.0;
    ops[0][1] = 0.0;
    ops[0][2] = 0.0;
    ops[1][0] = 0.5;
    ops[1][1] = 0.5;
    ops[1][2] = 0.5;
    break;
  case rhombohedral:
    ops[0][0] = 0.0;
    ops[0][1] = 0.0;
    ops[0][2] = 0.0;
    ops[1][0] = 2.0 / 3.0;
    ops[1][1] = 1.0 / 3.0;
    ops[1][2] = 1.0 / 3.0;
    ops[2][0] = 1.0 / 3.0;
    ops[2][1] = 2.0 / 3.0;
    ops[2][2] = 2.0 / 3.0;
    break;
  }
}

DLV::int_g DLV::symmetry_data::get_lattice_type() const
{
  return 0;
}

DLV::int_g DLV::plane_symmetry::get_lattice_type() const
{
  return (int)lattice;
}

DLV::int_g DLV::volume_symmetry::get_lattice_type() const
{
  return (int)lattice;
}

DLV::int_g DLV::symmetry_data::get_centre_type() const
{
  return 0;
}

DLV::int_g DLV::plane_symmetry::get_centre_type() const
{
  return (int)centre;
}

DLV::int_g DLV::volume_symmetry::get_centre_type() const
{
  return (int) centre;
}

void DLV::symmetry_data::generate_translations(const coord_type va[3],
					       const coord_type vb[3],
					       const coord_type vc[3],
					       const int_g dim)
{
  if (fractional_translators and !cartesian_translators) {
    real_l t[3][3] = { {1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0} };
    if (get_centre_type() != rhombohedral)
      get_transform_to_conv_cell(t);
    real_l conv[3][3];
    for (int_g i = 0; i < 3; i++)
      for (int_g j = 0; j < 3; j++)
	conv[i][j] = t[i][0] * va[j] + t[i][1] * vb[j] + t[i][2] * vc[j];
    real_l ca[3], cb[3], cc[3];
    for (int_g i = 0; i < 3; i++) {
      ca[i] = conv[0][i];
      cb[i] = conv[1][i];
      cc[i] = conv[2][i];
    }
    for (int_g i = 0; i < number_of_sym_ops; i++)
      sym_ops[i].generate_cartesian_translation(ca, cb, cc, dim);
    cartesian_translators = true;
  } else if (cartesian_translators and !fractional_translators) {
    real_l cell[3][3] = { {1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0} };
    cell[0][0] = va[0];
    cell[1][0] = va[1];
    cell[2][0] = va[2];
    if (dim > 1) {
      cell[0][1] = vb[0];
      cell[1][1] = vb[1];
      cell[2][1] = vb[2];
      if (dim > 2) {
	cell[0][2] = vc[0];
	cell[1][2] = vc[1];
	cell[2][2] = vc[2];
      }
    }
    real_l inverse[3][3];
    matrix_invert(cell, inverse, dim);
    for (int_g i = 0; i < number_of_sym_ops; i++)
      sym_ops[i].generate_fractional_translation(inverse, dim);
    fractional_translators = true;
  }
}

void DLV::point_symmetry::complete_translators(const coord_type [3],
					       const coord_type [3],
					       const coord_type [3])
{
  // Do nothing - they should have been set to zero
}

void DLV::line_symmetry::complete_translators(const coord_type va[3],
					      const coord_type vb[3],
					      const coord_type vc[3])
{
  generate_translations(va, vb, vc, 1);
}

void DLV::plane_symmetry::complete_translators(const coord_type va[3],
					       const coord_type vb[3],
					       const coord_type vc[3])
{
  generate_translations(va, vb, vc, 2);
}

void DLV::volume_symmetry::complete_translators(const coord_type va[3],
						const coord_type vb[3],
						const coord_type vc[3])
{
  generate_translations(va, vb, vc, 3);
}

void DLV::plane_symmetry::copy_lattice_type(const symmetry_data &sym)
{
  const plane_symmetry &s = static_cast<const plane_symmetry &>(sym);
  lattice = s.lattice;
  centre = s.centre;
}

void DLV::volume_symmetry::copy_lattice_type(const symmetry_data &sym)
{
  const volume_symmetry &s = static_cast<const volume_symmetry &>(sym);
  lattice = s.lattice;
  centre = s.centre;
}

bool DLV::point_symmetry::parse_operators(const string ops[],
					  const int_g nops, const int_g gp,
					  const bool rhomb, const bool prim)
{
  fprintf(stderr, "BUG: parse operators for molecule\n");
  return false;
}

bool DLV::line_symmetry::parse_operators(const string ops[],
					 const int_g nops, const int_g gp,
					 const bool rhomb, const bool prim)
{
  fprintf(stderr, "BUG: parse operators for polymer\n");
  return false;
}

bool DLV::plane_symmetry::parse_operators(const string ops[],
					  const int_g nops, const int_g gp,
					  const bool rhomb, const bool prim)
{
  fprintf(stderr, "BUG: parse operators for slab\n");
  return false;
}

bool DLV::volume_symmetry::parse_operators(const string ops[],
					   const int_g nops, const int_g gp,
					   const bool rhomb, const bool prim)
{
  int_g prim_ops = nops;
  if (gp > 1) {
    // See how many operators it should have
    cctbx::sgtbx::space_group_symbols symbol(gp);
    string label = symbol.universal_hermann_mauguin();
    cctbx::sgtbx::space_group_type sgtype(label);
    cctbx::sgtbx::space_group sg = sgtype.group();
    if (rhomb)
      set_lattice_data(gp, 'R');
    else
      set_lattice_data(gp, label[0]);
    if (!prim)
      prim_ops = sg.f_inv() * sg.n_smx();
  }
  return parse_vol_operators(ops, nops, gp, rhomb, prim, prim_ops);
}

// copied and then modified from set space group
bool DLV::symmetry_data::parse_vol_operators(const string ops[],
					     const int_g nops, const int_g gp,
					     const bool rhomb, const bool prim,
					     const int_g prim_ops)
{
  bool strip = false;
  int_g ncentres = 1;
  real_l centres[4][3] = { { 0.0, 0.0, 0.0 },
			   { 0.0, 0.0, 0.0 },
			   { 0.0, 0.0, 0.0 },
			   { 0.0, 0.0, 0.0 } };
  if (gp > 1) {
    if (prim_ops < nops) {
      strip = true;
      ncentres = get_number_conv_cell_translators();
      get_conv_cell_translators(centres);
    }
  }
  bool ok = true;
  // operators
  init_number_of_ops(prim_ops);
  int_g i = 0;
  // try to guess the type
  bool sixf = false;
  //bool threef = false;
  //bool fourf = false;
  if (gp == 0) {
    for (int_g j = 0; j < nops; j++) {
      cctbx::sgtbx::rt_mx mat(ops[j]);
      cctbx::sgtbx::rot_mx rmat = mat.r();
      int_g optype = rmat.type();
      if (optype == 6 or optype == -6)
	sixf = true;
      //else if (optype == 4 or optype == -4)
      //	fourf = true;
      //else if (optype == 3 or optype == -3)
      //threef = true;
    }
  }
  coord_type c_to_p[3][3];
  coord_type p_to_c[3][3];
  if (prim) {
    get_transform_to_prim_cell(c_to_p);
    get_transform_to_conv_cell(p_to_c);
  }
  for (int_g j = 0; j < nops; j++) {
    cctbx::sgtbx::rt_mx mat(ops[j]);
    scitbx::af::tiny<real_l, 12> elements = mat.as_double_array();
    real_l r[3][3];
    r[0][0] = elements[0];
    r[0][1] = elements[1];
    r[0][2] = elements[2];
    r[1][0] = elements[3];
    r[1][1] = elements[4];
    r[1][2] = elements[5];
    r[2][0] = elements[6];
    r[2][1] = elements[7];
    r[2][2] = elements[8];
    if (prim and !rhomb) {
      real_l temp[3][3];
      mat_mult(r, p_to_c, temp);
      mat_mult(c_to_p, temp, r);
    }
    real_l t[3];
    if (prim and !rhomb) {
      t[0] = p_to_c[0][0] * elements[9] + p_to_c[0][1] * elements[10]
	+ p_to_c[0][2] * elements[11];
      t[0] = p_to_c[1][0] * elements[9] + p_to_c[1][1] * elements[10]
	+ p_to_c[1][2] * elements[11];
      t[0] = p_to_c[2][0] * elements[9] + p_to_c[2][1] * elements[10]
	+ p_to_c[2][2] * elements[11];
    } else {
      t[0] = elements[9];
      t[1] = elements[10];
      t[2] = elements[11];
    }
    // Todo - check if there are any operator transform issues with
    // monoclinic. Don't think triclinic has enough sym to be relevant.
    // Don't seem to be able to rely on op type, so always do this here
    if (rhomb /* or (threef and !fourf)*/) {
      // it might be trigonal, R or H - Todo, how to tell?
      // Rhombohedral setting
      // matrix is  a/sqrt(3) -a/2sqrt(3) -a/2sqrt(3)
      //              0        a/2        -a/2
      //             c/3       c/3         c/3
      // The actual values of a and c shouldn't matter for the
      // operators we get, but its hard to do in general
      // so set a = sqrt(3), c = 3 (if it matters we have a bug)
      real_l cell[3][3] = { {1.0,-0.5,-0.5},
			    {0.0, 0.0, 0.0},
			    {1.0, 1.0, 1.0} };
      cell[1][1] = std::sqrt(3.0)/2.0;
      cell[1][2] = -cell[1][1];
      real_l inv[3][3];
      matrix_invert(cell, inv, 3);
      real_l temp[3][3];
      mat_mult(r, inv, temp);
      mat_mult(cell, temp, r);
    } else if ((gp > 142 and gp < 195) or sixf) {
      // Hex setting
      real_l cell[3][3] = { {0.0, 0.0, 0.0},
			    {-0.5, 1.0, 0.0},
			    {0.0, 0.0, 1.0} };
      cell[0][0] = std::sqrt(3.0)/2.0;
      real_l inv[3][3];
      matrix_invert(cell, inv, 3);
      real_l temp[3][3];
      mat_mult(r, inv, temp);
      mat_mult(cell, temp, r);
      if (gp == 146 or gp == 148 or gp == 155 or gp == 160 or gp == 161 or
	  gp == 166 or gp == 167) {
	// convert into rhomb cell fractional translations (x 1/3?)
	t[0] = (real_l(2.0) * elements[9] + elements[10] + elements[11]);
	t[1] = (- elements[9] + elements[10] + elements[11]);
	t[2] = (- elements[9] - real_l(2.0) * elements[10] + elements[11]);
	if (strip) { // our centres are now wrong
	  t[0] -= truncate(t[0]);
	  t[1] -= truncate(t[1]);
	  t[2] -= truncate(t[2]);
	}
      }
    }
    //fprintf(stderr, "%d %f %f %f\n", j, t[0], t[1], t[2]);
    bool op_copy = false;
    if (strip) {
      const real_l tol = 0.001;
      // check for copy, if op[o] + centre == this one then its a copy
      for (int_g o = 0; o < i; o++) {
	op_copy = true;
	for (int_g i1 = 0; i1 < 3; i1++)
	  for (int_g j1 = 0; j1 < 3; j1++)
	    if (sym_ops[o].rotation[i1][j1] > (r[i1][j1] + tol) or
		sym_ops[o].rotation[i1][j1] < (r[i1][j1] - tol))
	      op_copy = false;
	// Todo - does this work if centre 0 0 0 isn't before others
	// would need to subtract to compare in that case
	if (op_copy) {
	  for (int_g c = 0; c < ncentres; c++) {
	    op_copy = true;
	    for (int_g i1 = 0; i1 < 3; i1++) {
	      real_l tt = sym_ops[o].frac_translation[i1] + centres[c][i1];
	      tt -= truncate(tt);
	      if (tt > t[i1] + tol or tt < t[i1] - tol)
		op_copy = false;
	    }
	    if (op_copy)
	      break;
	  }
	}
	if (op_copy)
	  break;
      }
    }
    if (!op_copy) {
      if (i == prim_ops) {
	//fprintf(stderr, "%f %f %f\n", r[0][0], r[0][1], r[0][2]);
	//fprintf(stderr, "%f %f %f\n", r[1][0], r[1][1], r[1][2]);
	//fprintf(stderr, "%f %f %f\n", r[2][0], r[2][1], r[2][2]);
	fprintf(stderr, "Problem parsing operators\n");
	ok = false;
	break;
      }
      // Todo - minimise translators
      // (be careful with R set 167 etc as they're wrong now).
      //fprintf(stderr, "Accept %d\n", j);
      cctbx::sgtbx::rot_mx rmat = mat.r();
      int_g optype = rmat.type();
      switch (optype) {
      case -6:
	// Its hexagonal
	set_fractional_operator(r, t, R60, improper_rotation, i);
	break;
      case -4:
	set_fractional_operator(r, t, R90, improper_rotation, i);
	break;
      case -3:
	set_fractional_operator(r, t, R120, improper_rotation, i);
	break;
      case -2:
	set_fractional_operator(r, t, identity, mirror, i);
	break;
      case -1:
	set_fractional_operator(r, t, identity, inversion, i);
	break;
      case 1:
	set_fractional_operator(r, t, identity, no_reflection, i);
	break;
      case 2:
	set_fractional_operator(r, t, R180, no_reflection, i);
	break;
      case 3:
	set_fractional_operator(r, t, R120, no_reflection, i);
	break;
      case 4:
	set_fractional_operator(r, t, R90, no_reflection, i);
	break;
      case 6:
	// Its hexagonal
	set_fractional_operator(r, t, R60, no_reflection, i);
	break;
      default:
	ok = false;
	break;
      }
      i++;
    }
  }
  return ok;
}


#ifdef DLV_USES_SERIALIZE

template <class Archive>
void DLV::symmetry_operator::serialize(Archive &ar, const unsigned int version)
{
  ar & rotation;
  ar & cart_translation;
  ar & frac_translation;
  ar & rotation_id;
  ar & reflect_id;
  ar & has_translation;
  ar & generator;
  ar & glide;
  ar & inverse;
}

template <class Archive>
void DLV::symmetry_data::load(Archive &ar, const unsigned int version)
{
  ar & number_of_sym_ops;
  sym_ops = new symmetry_operator[number_of_sym_ops];
  for (int i = 0; i < number_of_sym_ops; i++)
    ar & sym_ops[i];
  ar & cartesian_translators;
  ar & fractional_translators;
}

template <class Archive>
void DLV::symmetry_data::save(Archive &ar, const unsigned int version) const
{
  ar & number_of_sym_ops;
  for (int i = 0; i < number_of_sym_ops; i++)
    ar & sym_ops[i];
  ar & cartesian_translators;
  ar & fractional_translators;
}

template <class Archive>
void DLV::point_symmetry::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<symmetry_data>(*this);
}

template <class Archive>
void DLV::line_symmetry::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<symmetry_data>(*this);
}

template <class Archive>
void DLV::plane_symmetry::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<symmetry_data>(*this);
  ar & lattice;
  ar & centre;
}

template <class Archive>
void DLV::volume_symmetry::serialize(Archive &ar, const unsigned int version)
{
  ar & boost::serialization::base_object<symmetry_data>(*this);
  ar & lattice;
  ar & centre;
}

BOOST_CLASS_EXPORT_KEY(DLV::point_symmetry)
BOOST_CLASS_EXPORT_KEY(DLV::line_symmetry)
BOOST_CLASS_EXPORT_KEY(DLV::plane_symmetry)
BOOST_CLASS_EXPORT_KEY(DLV::volume_symmetry)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::point_symmetry)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::line_symmetry)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::plane_symmetry)
BOOST_CLASS_EXPORT_IMPLEMENT(DLV::volume_symmetry)

DLV_SPLIT_EXPLICIT(DLV::symmetry_data)
DLV_NORMAL_EXPLICIT(DLV::point_symmetry)
DLV_EXPORT_EXPLICIT(DLV::point_symmetry)
DLV_NORMAL_EXPLICIT(DLV::line_symmetry)
DLV_EXPORT_EXPLICIT(DLV::line_symmetry)
DLV_NORMAL_EXPLICIT(DLV::plane_symmetry)
DLV_EXPORT_EXPLICIT(DLV::plane_symmetry)
DLV_NORMAL_EXPLICIT(DLV::volume_symmetry)
DLV_EXPORT_EXPLICIT(DLV::volume_symmetry)

#endif // DLV_USES_SERIALIZE
