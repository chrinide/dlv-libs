
#ifndef DLV_BASE_TYPES
#define DLV_BASE_TYPES

#include <string>
#include <cstdlib>
#include <cmath>

#include <boost/shared_ptr.hpp>

#ifdef WIN32
#  define or ||
#  define and &&
#endif // WIN32

enum DLVreturn_type { DLV_ERROR = -1, DLV_WARNING = 0, DLV_OK = 1 };

namespace DLV {

  // g - general, s small, l large

  // integers -x,..., 0, ..., x
  typedef int int_g;
  typedef long int_l;
  // typedef long long int_l;

  // natural numbers N^0, 0,... x
  typedef unsigned int nat_g;
  typedef unsigned long nat_l;

  //typedef char char_g;

  typedef float real_g;
  typedef double real_l;

  typedef real_l coord_type;

  using std::string;

  inline int to_integer(const real_l r)
  {
    return int(r);
  }

  inline double to_double(const real_l r)
  {
    return r;
  }

  using std::abs;

  using boost::shared_ptr;

  enum atom_flag_type { flag_unset = 0, flag_partial = 1, flag_done = 2 };

}

#ifndef DLV_LOCAL_ARRAYS

#  ifdef USE_ALLOCA

#    include <alloca.h>
#    define new_local_array1(t, n) (t *)alloca(sizeof(t) * n);
#    define new_local_array2(t, n, m) (t (*)[m])alloca(sizeof(t) * n * m);
#    define new_local_array3(t, n, m, l) (t (*)[m][l])alloca(sizeof(t) * n*m*l);
#    define delete_local_array(p)

#  else

#    define new_local_array1(t, n) new t[n]
#    define new_local_array2(t, n, m) new t[n][m]
#    define new_local_array3(t, n, m, l) new t[n][m][l]
#    define delete_local_array(p) delete [] p

#  endif // USE_ALLOCA

#  define DLV_LOCAL_ARRAYS

#endif // DLV_LOCAL_ARRAYS

#endif // DLV_BASE_TYPES
