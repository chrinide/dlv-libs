
#include "types.hxx"
#include "job_setup.hxx"
#include "boost_lib.hxx"
#include "utils.hxx"

DLV::job_setup_data::job_setup_data(const char *host, const char *wd,
				    const char *sd, const int_g cpus,
				    const int_g nodes, const int_g hours,
				    const int_g memory, const char *project,
				    const char *q_id, const bool parallel)
  : scratch_directory(sd)
{
  if (strcmp(host, "localhost") == 0) {
    n_processors = 0;
    n_nodes = 0;
    walltime = 0;
    wd = "";
    local = true;
    hostname = get_host_name();
    project_id = "";
    queue = "";
    memory_per_proc = 0;
  } else {
    work_directory = wd;
    walltime = hours;
    memory_per_proc = 1024 * memory;
    local = false;
    hostname = host;
    project_id = project;
    queue = q_id;
    if (parallel) {
      n_processors = cpus;
      n_nodes = nodes;
    } else {
      n_processors = 0;
      n_nodes = 0;
    }
  }
  if (scratch_directory.length() > 0)
    if (scratch_directory[scratch_directory.length() - 1] != DIR_SEP_CHR)
      scratch_directory += DIR_SEP_CHR;
}
