
#ifndef DLV_ATOM_IMPL
#define DLV_ATOM_IMPL

//#include <vector>

namespace DLV {

  enum selection_type {
    not_selected, is_selected, dragging_on, dragging_off,
    group_copy, symmetry_copy, group_sym_copy, trans_copy, group_trans_copy,
    group_drag, symmetry_drag, group_sym_drag, trans_drag, group_trans_drag
  };

  struct bond_info {
  public:
    atom_tree_node *node;
    coord_type cartesian[3];
    coord_type fractional[3];
    colour_data colour;
    real_g radius;
    int_g atomic_number;
    bool *bonds;
    int_g min_a;
    int_g max_a;
    int_g min_b;
    int_g max_b;
    int_g min_c;
    int_g max_c;
    int_g data_index;
  };
    
  class atom_tree_node {
  public:
    atom_tree_node(const atom_position &item, const atom_tree_node *ptr,
		   atom_tree_node *l, atom_tree_node *r);
    const atom_tree_node *find(const atom_position &atm) const;
    void insert(atom_tree_node * &node, const atom_position &newitem,
		const atom_tree_node *ptr, bool &taller, int_g &natoms,
		const bool cell);
    void remove();
    void copy(atom_tree &tree) const;
    void centre(atom_tree &tree, const real_l matrix[3][3],
		const int_g dim, const bool shift[3]) const;

    void get_atom_types(int_g atom_types[], const int_g n, int_g &i) const;
    void get_atom_regions(const char group[], int_g regions[],
			  const int_g n, int_g &i) const;
    void get_asym_indices(int_g indices[], const int_g n, int_g &i,
			  const std::map<int, atom> &basis) const;
    void get_cartesian_coords(coord_type coords[][3],
			      const int_g n, int_g &i) const;
    void get_cartesian_coords(coord_type coords[][3], bool p[],
			      const int_g n, int_g &i) const;
    void get_fractional_coords(coord_type coords[][3],
			       const int_g n, int_g &i) const;
    void map_data(const real_g grid[][3], int_g **const elem, const int_g ndata,
		  const int_g ngrid, real_g (*map_grid)[3], int_g **map_data,
		  int_g &count, const real_l inverse[3][3],
		  const int_g dim) const;
    void map_data(const real_g grid[][3], real_g **const elem,
		  const int_g ndata, const int_g ngrid, real_g (*map_grid)[3],
		  real_g **map_data, int_g &count, const real_l inverse[3][3],
		  const int_g dim) const;
    void map_data(const real_g grid[][3], const int_g shifts[][3],
		  const real_g mag[][3], const real_g phases[][3],
		  const int_g ngrid, real_g map_grid[][3], real_g map_data[][3],
		  int_g &count, const real_g ka, const real_g kb,
		  const real_g kc, const real_l inv[3][3],
		  const int_g dim) const;
    void map_data(const real_g grid[][3], const int_g shifts[][3],
		  const real_g mag[][3], const real_g phases[][3],
		  const int_g ngrid, std::vector<real_g (*)[3]> &map_data,
		  int_g &count, const real_g scale, const int_g nframes,
		  const int_g ncopies, const real_g ka, const real_g kb,
		  const real_g kc, const real_l inv[3][3],
		  const int_g dim) const;
    void map_data(const real_g grid[][3], const int_g ngrid,
		  const std::vector<real_g (*)[3]> &traj,
		  std::vector<real_g (*)[3]> &map_data, int_g &count,
		  const int_g nframes, const real_l inv[3][3],
		  const int_g dim) const;
    void locate_atom(const atom_position &p, int_g &count, bool &found) const;
    void locate_selected_atom(const atom_tree_node *s, int_g &count,
			      bool &found) const;
    void map_atom_count(const atom &myatom, const real_g coords[][3],
			const int_g ngrid, const real_l inv[3][3],
			const int_g dim, int_g &count,
			const bool all_atoms) const;
    void map_atom_selection(const atom &myatom, const real_g coords[][3],
			    const int_g ngrid, int_g **const info,
			    const int_g ninfo, const real_l inv[3][3],
			    const int_g dim, int_g labels[], int_g atom_types[],
			    atom parents[], int_g &count,
			    const bool all_atoms) const;
    void set_atom_flag(const atom_flag_type flag);

    void count_atom_types(int_g types[], const int_g n) const;
    void count_crystal03_ids(int_g types[]) const;
    void count_atoms(int_g &count, const bool only_selections,
		     const bool ignore_selections,
		     const bool transparent_flags, const bool use_edit_flags,
		     const bool edit) const;
    void fill_arrays(real_g coords[][3], real_g colours[][3], real_g radii[],
		     int_g &count, const bool only_selected,
		     const bool ignore_selections, const bool use_flags,
		     const int_g flag_type,
		     const std::vector<real_g (*)[3]> *traj,
		     const int_g nframes, const int_g n,
		     const bool use_edit_flags, const bool edit) const;
    void get_z_range(real_l &zmin, real_l &zmax) const;

    void set_base_cell();
    void clear_base_cell();
    void count_origin_cell_atoms(int_g &count) const;
    void get_origin_cell_atoms(atom_tree_node *atoms[], int_g &index);
    void get_origin_cell_atoms(bond_info atoms[], int_g &index,
			       real_g &max_radius, const real_g overlap,
			       const bond_data &bonds, int_g &count);
    void get_all_cell_atoms(bond_info atoms[], int_g &index, real_g &max_radius,
			    const real_g overlap, const bond_data &bonds,
			    int_g &count, const int_g na, const int_g nb,
			    const int_g nc);
    const atom_position &get_data() const;
    void get_fractional_coords(coord_type coords[3]) const;
    void conv_origin_cell_atoms(atom_tree_node *atomlist[], int_g &index,
				const int_g dim, const real_l cell[3][3],
				const real_l inverse[3][3]);
    bool atom_is_selected() const;
    bool atom_is_being_edited() const;

    void generate_bonds(std::list<bond_list> &info, const bond_info data[],
			const int_g no, const bool invert_colours,
			const real_l tol, const real_g map_data[][3],
			const bool edit) const;
    void generate_bonds(std::list<bond_list> &info, const bond_info data[],
			const int_g no, const coord_type a[3], const int_g na,
			const bool centre, const bool invert_colours,
			const real_l tol, const real_g map_data[][3],
			const bool edit) const;
    void generate_bonds(std::list<bond_list> &info, const bond_info data[],
			const int_g no, const coord_type a[3],
			const coord_type b[3], const int_g na, const int_g nb,
			const bool centre, const bool invert_colours,
			const real_l tol, const real_g map_data[][3],
			const bool edit) const;
    void generate_bonds(std::list<bond_list> &info, const bond_info data[],
			const int_g no, const coord_type a[3],
			const coord_type b[3], const coord_type c[3],
			const int_g na, const int_g nb, const int_g nc,
			const bool centre, const bool invert_colours,
			const real_l tol, const real_g map_data[][3],
			const bool edit) const;
    void surface_bonds(std::list<bond_list> &info, const bond_info data[],
		       const int_g no, const coord_type a[3],
		       const coord_type b[3], const coord_type c[3],
		       const int_g na, const int_g nb, const int_g nc,
		       const bool centre, const bool invert_colours,
		       const real_l tol, const real_g map_data[][3],
		       const bool edit) const;
    static void set_atom_overlaps(bond_info data[], const int_g n,
				  const real_g fla, const int_g na,
				  const bool centre, const real_l tol);
    static void set_atom_overlaps(bond_info data[], const int_g n,
				  const real_g fla, const real_g flb,
				  const int_g na, const int_g nb,
				  const bool centre, const real_l tol);
    static void set_atom_overlaps(bond_info data[], const int_g n,
				  const real_g fla, const real_g flb,
				  const real_g flc, const int_g na,
				  const int_g nb, const int_g nc,
				  const bool centre, const real_l tol);
    static void set_surface_overlaps(bond_info data[], const int_g n,
				     const real_g fla, const real_g flb,
				     const real_g flc, const int_g na,
				     const int_g nb, const int_g nc,
				     const bool centre, const real_l tol);
    void select_copies(const int_g symmetry_select, const atom_tree_node *ptr);
    atom_tree_node *select(const real_g origin[3], const real_g offset,
			   const bool active, char message[], const int_g mlen);
    bool set_atom_selection(const atom_position &atom, const selection_type s);
    bool set_translated_selection(const atom_position &atm, const int_g dim);
    void select_groups(const int_g symmetry_select, const string group,
		       const atom_tree_node *ptr);
    void copy_selected_atoms(atom_tree_node *tree, const int_g symmetry_select,
			     const bool force, const int_g dim) const;
    bool update_selection_list(const atom_position &atm,
			       std::list<atom_tree_node *> &s);
    void deselect();
    atom get_atom() const;

    void copy_atoms(std::map<int, atom> &basis,
		    atom prim[], const real_l shift[3], int_g &count) const;
    void copy_atoms(std::map<int, atom> &basis,
		    atom prim[], const int_g atn, int_g &count) const;
    void shift_atoms(const coord_type centre[3], real_l distance[],
		     int_g &count) const;
    void copy_atoms(std::map<int, atom> &basis,
		    atom prim[], const real_l shift[3], int_g &count,
		    const string label, const string group) const;
    void copy_atoms(std::map<int, atom> &basis,
		    atom prim[], const real_l zshift, int_g &count,
		    const string label, const string group) const;
    void delete_atoms(std::map<int, atom> &basis,
		      atom prim[], int_g &count) const;
    void edit_atoms(std::map<int, atom> &basis,
		    atom prim[], int_g &count, const bool keep_sym) const;
    void copy_atom_layers(std::map<int, atom> &basis, atom prim[],
			  const real_l caxis[3], int_g &index, int_g &count,
			  const int_g term, const int_g lcopies[],
			  const int_g lindex[], const string group,
			  const bool slab, const int_g salvage) const;
    void build_cluster_atoms(std::map<int, atom> &basis,
			     const coord_type centre[3], const int_g flags[],
			     atom prim[], int_g &index, int_g &count,
			     const string gpname) const;
    void find_cell0_atom(const atom &a, coord_type centre[3], real_l &l) const;
    void build_wulff_atoms(std::map<int, atom> &basis, atom prim[],
			   int_g &count, const real_g points[][3],
			   const real_g normals[][3],
			   const int_g nplanes, const real_g offsets[]) const;
    void build_box_atoms(std::map<int, atom> &basis, atom prim[], int_g &count,
			 const real_g points[][3], const real_g normals[][3],
			 const int_g nplanes, const int_g planes_per_obj[],
			 const int_g nobjs) const;
    void build_cyl_atoms(std::map<int, atom> &basis, atom prim[], int_g &count,
			 const real_g iradius[], const real_g oradius[],
			 const real_g zmin[], const real_g zmax[],
			 const int orient[], const int ncylinders) const;
    void layer_groups(const int_g lindex[], const string group, 
		      int_g &index) const;

  private:
    atom_tree_node *left_child;
    atom_tree_node *right_child;
    atom_position data;
    enum { TREE_equal, TREE_right, TREE_left } balance;
    selection_type selected;
    bool base_cell;
    const atom_tree_node *parent;

    static void add_basis(std::map<int, atom> &basis, atom &p, const int_g n);
    bool atom_is_selected_sym() const;
    bool atom_is_selected_trans() const;
    bool atom_being_selected() const;
    bool is_bonded_pair(const atom_tree_node *node,
			const bond_data &bonds) const;
    static bool atoms_are_bonded(const bond_info data[], const int_g i,
				 const int_g j, const coord_type pos[3],
				 const real_g map_data[][3]);
    void select_colour(colour_data &rgb, int_g &btype, const atom_position &atm,
		       const colour_data &col, const bool invert_col,
		       const bool edit) const;
    void store_bond(std::list<bond_list> &info, const bond_info data[],
		    const int_g i1, const int_g i2, const coord_type pos2[3],
		    const real_l tol, const bool invert_colours,
		    const real_g map_data[][3], const bool edit) const;
    void gen_bond_second_atom(std::list<bond_list> &info,
			      const bond_info data[], const int_g index,
			      const int_g no, const bool invert_colours,
			      const real_l tol, const real_g map_data[][3],
			      const bool edit) const;
    void edge_bond(std::list<bond_list> &info, const bond_info &data,
		   const coord_type pos1[3], const coord_type pos2[3],
		   const coord_type diff[3], const real_l cella,
		   const coord_type a[3], const bool invert_colours,
		   const bool edit) const;
    void store_bond(std::list<bond_list> &info, const bond_info data[],
		    const int_g i1, const int_g i2, const coord_type pos2[3],
		    const bool inside_cell, const coord_type a[3],
		    const int_g na, const real_l tol, const bool centre,
		    const bool invert_colours,
		    const real_g map_data[][3], const bool edit) const;
    void gen_bond_second_atom(std::list<bond_list> &info,
			      const bond_info data[], const int_g index,
			      const int_g no, const coord_type a[3],
			      const int_g na, const bool centre,
			      const bool invert_colours, const real_l tol,
			      const real_g map_data[][3],
			      const bool edit) const;
    void edge_bond(std::list<bond_list> &info, const bond_info &data,
		   const coord_type pos1[3], const coord_type pos2[3],
		   const coord_type diff[3], const real_l cella,
		   const real_l cellb, const coord_type a[3],
		   const coord_type b[3], const bool invert_colours,
		   const bool edit) const;
    void store_bond(std::list<bond_list> &info, const bond_info data[],
		    const int_g i1, const int_g i2, const coord_type pos2[3],
		    const bool inside_cell, const coord_type a[3],
		    const coord_type b[3], const int_g na, const int_g nb,
		    const real_l tol, const bool centre,
		    const bool invert_colours, const real_g map_data[][3],
		    const bool edit) const;
    void gen_bond_second_atom(std::list<bond_list> &info,
			      const bond_info data[], const int_g index,
			      const int_g no, const coord_type a[3],
			      const coord_type b[3], const int_g na,
			      const int_g nb, const bool centre,
			      const bool invert_colours, const real_l tol,
			      const real_g map_data[][3],
			      const bool edit) const;
    void edge_bond(std::list<bond_list> &info, const bond_info &data,
		   const coord_type pos1[3], const coord_type pos2[3],
		   const coord_type diff[3], const real_l cella,
		   const real_l cellb, const real_l cellc,
		   const coord_type a[3], const coord_type b[3],
		   const coord_type c[3], const bool invert_colours,
		   const bool edit) const;
    void store_bond(std::list<bond_list> &info, const bond_info data[],
		    const int_g i1, const int_g i2, const coord_type pos2[3],
		    const bool inside_cell, const coord_type a[3],
		    const coord_type b[3], const coord_type c[3],
		    const int_g na, const int_g nb, const int_g nc,
		    const real_l tol, const bool centre,
		    const bool invert_colours, const real_g map_data[][3],
		    const bool edit) const;
    void gen_bond_second_atom(std::list<bond_list> &info,
			      const bond_info data[], const int_g index,
			      const int_g no, const coord_type a[3],
			      const coord_type b[3], const coord_type c[3],
			      const int_g na, const int_g nb, const int_g nc,
			      const bool centre, const bool invert_colours,
			      const real_l tol, const real_g map_data[][3],
			      const bool edit) const;
    void edge_surf(std::list<bond_list> &info, const bond_info &data,
		   const coord_type pos1[3], const coord_type pos2[3],
		   const coord_type diff[3], const real_l cella,
		   const real_l cellb, const real_l cellc,
		   const coord_type a[3], const coord_type b[3],
		   const coord_type c[3], const bool invert_colours,
		   const bool edit) const;
    void store_surf(std::list<bond_list> &info, const bond_info data[],
		    const int_g i1, const int_g i2, const coord_type pos2[3],
		    const bool inside_cell, const coord_type a[3],
		    const coord_type b[3], const coord_type c[3],
		    const int_g na, const int_g nb, const int_g nc,
		    const real_l tol, const bool centre,
		    const bool invert_colours, const real_g map_data[][3],
		    const bool edit) const;
    void surf_bond_second_atom(std::list<bond_list> &info,
			       const bond_info data[], const int_g index,
			       const int_g no, const coord_type a[3],
			       const coord_type b[3], const coord_type c[3],
			       const int_g na, const int_g nb, const int_g nc,
			       const bool centre, const bool invert_colours,
			       const real_l tol, const real_g map_data[][3],
			       const bool edit) const;
    bool atom_flag_is_ok() const;
    atom_flag_type get_atom_flag() const;
    selection_type search_inequivalent_atoms(const atom_position &p) const;
    selection_type search_parents(const atom_tree_node *p) const;
    selection_type search_symmetry_groups(const string group,
					  const string label) const;
    selection_type search_parent_groups(const atom_tree_node *p,
					const string group,
					const string label) const;
    selection_type search_cell(const atom_tree_node *p, const string group,
			       const string label) const;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  //typedef std::vector<pair<atom_tree_node *, atom_tree_node *> > atom_pair;

  class crystal03_id_type : public atom_info_type {
  public:
    crystal03_id_type(const int_g i);
    int_g get_id() const;

    static const string label;

  private:
    int_g id;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class core_electrons_type : public atom_info_type {
  public:
    core_electrons_type(const int_g i);
    int_g get_electrons() const;

    static const string label;

  private:
    int_g electrons;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  // for charge on point charge sites, primarily
  class charge_type : public atom_info_type {
  public:
    charge_type(const real_l i);
    real_l get_charge() const;

    static const string label;

  private:
    real_l charge;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class isotope_type : public atom_info_type {
  public:
    isotope_type(const int_g i);
    int_g get_isotope() const;

    static const string label;

  private:
    int_g isotope;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class debye_waller_type : public atom_info_type {
  public:
    debye_waller_type(const real_g i, const real_g j=0.0);
    real_g get_debye_waller1() const;
    real_g get_debye_waller2() const;

    static const string label;

  private:
    real_g dw1;
    real_g dw2;

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class displacements_type : public atom_info_type {
  public:
    displacements_type(const real_g d[3]);
    void get_displacements(real_g d[3]) const;
 
    static const string label;

  private:
    real_g disps[3];

#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };

  class rod_type : public atom_info_type {
  public:
    rod_type(const int_g d, const int_g d2=0, const int_g nocc=0, 
	     const real_g xc1=0, const int_g nx1=0, 
	     const real_g xc2=0, const int_g nx2=0, 
	     const real_g yc1=0, const int_g ny1=0, 
	     const real_g yc2=0, const int_g ny2=0,
	     const int_g nz=0);
    int_g get_dw1() const;
    int_g get_dw2() const;
    int_g get_noccup() const;
    real_g get_xconst() const;
    int_g get_nxdis() const;
    real_g get_x2const() const;
    int_g get_nx2dis() const;
    real_g get_yconst() const;
    int_g get_nydis() const;
    real_g get_y2const() const;
    int_g get_ny2dis() const;
    int_g get_nzdis() const;
    void set_dw1(const int_g d);
    void set_dw2(const int_g d);
    void set_occ(const int_g d);
    static const string label;

  private:
    int_g dw1;	   
    int_g dw2;	   
    int_g noccup;	   
    real_g xconst;  
    int_g nxdis;	   
    real_g x2const; 
    int_g nx2dis;	   
    real_g yconst;  
    int_g nydis;	   
    real_g y2const; 
    int_g ny2dis;	   
    int_g nzdis;     
#ifdef DLV_USES_SERIALIZE
    friend class boost::serialization::access;
    template <class Archive>
    void serialize(Archive &ar, const unsigned int version);
#endif // DLV_USES_SERIALIZE
  };
}

#ifdef DLV_USES_SERIALIZE
BOOST_CLASS_EXPORT_KEY(DLV::atom_tree_node)
BOOST_CLASS_EXPORT_KEY(DLV::crystal03_id_type)
BOOST_CLASS_EXPORT_KEY(DLV::core_electrons_type)
BOOST_CLASS_EXPORT_KEY(DLV::charge_type)
BOOST_CLASS_EXPORT_KEY(DLV::isotope_type)
BOOST_CLASS_EXPORT_KEY(DLV::debye_waller_type)
BOOST_CLASS_EXPORT_KEY(DLV::displacements_type)
BOOST_CLASS_EXPORT_KEY(DLV::rod_type)
#endif // DLV_USES_SERIALIZE

inline DLV::atom_tree_node::atom_tree_node(const atom_position &item,
					   const atom_tree_node *ptr,
					   atom_tree_node *l,
					   atom_tree_node *r)
  : left_child(l), right_child(r), data(item),
    balance(TREE_equal), selected(not_selected), base_cell(false),
    parent(ptr)
{
}

inline void DLV::atom_tree_node::set_base_cell()
{
  base_cell = true;
}

inline void DLV::atom_tree_node::clear_base_cell()
{
  base_cell = false;
}

inline bool DLV::atom_tree_node::atom_is_being_edited() const
{
  return data.get_atom()->get_edit_flag();
}

inline bool DLV::atom_tree_node::atom_is_selected() const
{
  if (selected == not_selected or selected == dragging_off)
    return false;
  else
    return true;
}

inline bool DLV::atom_tree_node::atom_is_selected_sym() const
{
  return (selected == symmetry_copy or selected == group_sym_copy);
}

inline bool DLV::atom_tree_node::atom_is_selected_trans() const
{
  return (selected == trans_copy or selected == group_trans_copy);
}

inline bool DLV::atom_tree_node::atom_being_selected() const
{
  if (selected == dragging_on or selected == symmetry_drag or
      selected == trans_drag or selected == group_drag or
      selected == group_sym_drag or selected == group_trans_drag)
    return true;
  else
    return false;
}

inline const DLV::atom_position &DLV::atom_tree_node::get_data() const
{
  return data;
}

inline
void DLV::atom_tree_node::get_fractional_coords(coord_type coords[3]) const
{
  data.get_fractional_coords(coords);
}

inline DLV::atom DLV::atom_tree_node::get_atom() const
{
  return data.get_atom();
}

inline bool DLV::atom_tree_node::atom_flag_is_ok() const
{
  return data.atom_flag_is_ok();
}

inline DLV::atom_flag_type DLV::atom_tree_node::get_atom_flag() const
{
  return data.get_atom_flag();
}

inline void DLV::atom_tree_node::set_atom_flag(const atom_flag_type flag)
{
  data.set_atom_flag(flag);
}

inline DLV::atom_info_type::atom_info_type()
{
}

inline DLV::crystal03_id_type::crystal03_id_type(const int_g i) : id(i)
{
}

inline DLV::int_g DLV::crystal03_id_type::get_id() const
{
  return id;
}

inline DLV::core_electrons_type::core_electrons_type(const int_g i)
  : electrons(i)
{
}

inline DLV::int_g DLV::core_electrons_type::get_electrons() const
{
  return electrons;
}

inline DLV::charge_type::charge_type(const real_l i) : charge(i)
{
}

inline DLV::real_l DLV::charge_type::get_charge() const
{
  return charge;
}

inline DLV::isotope_type::isotope_type(const int_g i) : isotope(i)
{
}

inline DLV::int_g DLV::isotope_type::get_isotope() const
{
  return isotope;
}

inline DLV::debye_waller_type::debye_waller_type(const real_g i,
						 const real_g j) 
  : dw1(i), dw2(j)
{
}

inline DLV::real_g DLV::debye_waller_type::get_debye_waller1() const
{
  return dw1;
}

inline DLV::real_g DLV::debye_waller_type::get_debye_waller2() const
{
  return dw2;
}

inline DLV::displacements_type::displacements_type(const real_g d[3]) 
{
  for (int_g i = 0; i<3; i++)
    disps[i] = d[i];
}

inline void DLV::displacements_type::get_displacements(real_g d[3]) const
{
  for (int_g i = 0; i<3; i++)
    d[i] = disps[i];
}

inline DLV::rod_type::rod_type(const int_g d, const int_g d2, const int_g nocc, 
			       const real_g xc1, const int_g nx1, 
			       const real_g xc2, const int_g nx2, 
			       const real_g yc1, const int_g ny1, 
			       const real_g yc2, const int_g ny2,
			       const int_g nz)
  : dw1(d), dw2(d2), noccup(nocc), 
    xconst(xc1), nxdis(nx1), x2const(xc2), nx2dis(nx2), 
    yconst(yc1), nydis(ny1), y2const(yc2), ny2dis(ny2), 
    nzdis(nz)
{
}

inline DLV::int_g DLV::rod_type::get_dw1() const
{
  return dw1;
}

inline DLV::int_g DLV::rod_type::get_dw2() const
{
  return dw2;
}

inline DLV::int_g DLV::rod_type::get_noccup() const
{
  return noccup;
}

inline DLV::real_g DLV::rod_type::get_xconst() const
{
  return xconst;
}

inline DLV::int_g DLV::rod_type::get_nxdis() const
{
  return nxdis;
}

inline DLV::real_g DLV::rod_type::get_x2const() const
{
  return x2const;
}

inline DLV::int_g DLV::rod_type::get_nx2dis() const
{
  return nx2dis;
}

inline DLV::real_g DLV::rod_type::get_yconst() const
{
  return yconst;
}

inline DLV::int_g DLV::rod_type::get_nydis() const
{
  return nydis;
}

inline DLV::real_g DLV::rod_type::get_y2const() const
{
  return y2const;
}

inline DLV::int_g DLV::rod_type::get_ny2dis() const
{
  return ny2dis;
}

inline DLV::int_g DLV::rod_type::get_nzdis() const
{
  return nzdis;
}

inline void DLV::rod_type::set_dw1(const int_g d)
{
  dw1 = d;
}

inline void DLV::rod_type::set_dw2(const int_g d)
{
  dw2 = d;
}

inline void DLV::rod_type::set_occ(const int_g d)
{
  noccup = d;
}


inline void DLV::atom_tree_node::add_basis(std::map<int, atom> &basis,
					   atom &p, const int_g n)
{
  p->set_id(n);
  basis[n] = p;
}

#endif // DLV_ATOM_IMPL
