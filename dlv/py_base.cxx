
#include "types.hxx"
#include "boost_lib.hxx"
#ifdef ENABLE_DLV_GRAPHICS
#  include "../graphics/render_base.hxx"
#endif // ENABLE_DLV_GRAPHICS
#include "operation.hxx"
//#include "model.hxx"
#include "extern_base.hxx"
#include "boost/python.hpp"

#define DLV_MODULE_NAME base

BOOST_PYTHON_MODULE(DLV_MODULE_NAME)
{
  using namespace boost::python;
  def("initialise", DLV::initialise);
  def("init_project", DLV::init_project);
}
