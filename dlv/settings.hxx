
#ifndef DLV_GROUP_SETTINGS
#define DLV_GROUP_SETTINGS

namespace DLV {

  extern int_g get_number_of_origins(const int_g spgp);
  extern int_g get_number_of_origins(const string spgp);

}

#endif // DLV_GROUP_SETTINGS
